<?php
set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
include('Net/SSH2.php');

function pingAddress($ip) {
    $pingresult = exec("ping -c 2 $ip", $outcome, $status);
    if (0 == $status) {
        $status = "alive";
    } else {
        $status = "dead";
    }
    echo "Ping to $ip is  ".$status;
}

function checkService($ip,$t=60){

    $ctx = stream_context_create(array('http'=>
        array(
            'timeout' => $t
        )
    ));

    $url = 'http://'.$ip.':8080/api/ping';
    $content = json_decode(file_get_contents($url,false, $ctx),true);
    if ($content['code']==1)
        return "running";
    else
        return null;
}

function testPrintSus($ip){

    $url = "http://".$ip.":8080/printsusinv";
    $date = date('Y-m-d H:i:s');
    $data = '{"Token":"111","Vid":1208,"Pid":514,"copies":1,"data":{"basket_id":"THIS IS A TEST PRINT OF THE PRINTER.         IT IS WORKING & YOU CAN THROW THIS AWAY.","invoice_num":"REMOTE TESTING OF PRINTER","trans_date":"'.$date.'","loc_id":"THROW","pos_id":"AWAY","cashier_name":"MIS ADMIN"}}';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $headers[] = "X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

if (isset($_GET['get-app'])) {
    $status = checkService($_GET['get-app'],2);
    echo ($status) ? $status : '';
    exit;
}

if (isset($_GET['eodCheck'])) {
    $url = 'https://pos.api.valueclub.asia/api/report/unposted?trans_date=' . date('Y-m-d', strtotime('today'));
    $content = json_decode(file_get_contents($url,false),true);
    if ($content['data'] == null) {
        $rtn = [
                "code"=>0,
                "msg" => 'No unposted transaction',
                "url" => $url
            ];
    }
    else {
        $rtn = [
            "code"=>1,
            "msg" => count($content['data']) .' unposted transaction',
            "url" => $url,
            "data" => $content['data']
        ];
    }
    echo json_encode($rtn);
    exit;
}

?>
<h1>CHPOS2.0 - Raspberry Mgmt
    <span style="font-size:12px;"><br>(To be move into POS CMS later)
        [<a href="?allService=1" style="font-size:12px;">Get all Service</a>]
    </span>
</h1>
<hr>
<?php

if (isset($_GET['get'])) {
    $status = checkService($_GET['get']);
    echo "Local service in ".$_GET['get'].": ";
    echo ($status) ? $status : 'error';
}

if (isset($_GET['sshin'])) {
    $ssh = new Net_SSH2($_GET['sshin']);
    if (!$ssh->login('pi', 'ch@ll3nger')) {
        echo "Shell login to ".$_GET['sshin']." failed";
    }
    else {
        echo $ssh->exec('sudo reboot');
        echo "Rebooting ".$_GET['sshin']."...";
    }
}

if (isset($_GET['ping'])) {
    $out = pingAddress($_GET['ping']);
    print_r($out);
}

if (isset($_GET['print'])) {
    $out = testPrintSus($_GET['print']);
    print_r($out);
}
?>
<hr>

<table border="0" cellpadding="3">
    <thead><tr>
        <th>LOC_ID</th>
        <th>POS_ID</th>
        <th>Raspberry IP</th>
        <th>&nbsp;&nbsp;</th>
        <th></th>
        <th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
        <th>CHPOS2.0 Version</th>
        <th>Last Login</th>
    </tr></thead>
    <?php
    $url = "http://pos.api.valueclub.asia/api/function/device";
    $content = json_decode(file_get_contents($url),true);
    $x=0;

    $serviceSet = array();
    foreach ($content['devices'] as $dev) {
        if ($dev['loc_id']!='') {
            $x++;
            $srvcpane = 'x'.$x;
            ?>
            <tr>
                <td><?php echo $dev['loc_id']; ?></td>
                <td><?php echo $dev['pos_id']; ?></td>
                <td><?php $thisip = $dev['terminal_ip']; echo $thisip; ?></td>
                <td>
                    <?php
                        if (isset($_GET['allService'])):
                        //if ( in_array($dev['loc_id'], ['313','313-R','AMK'] )):
                    ?>
                    <?php $serviceSet[] = $srvcpane; ?>
                    <span class="srvc-pane" id="<?php echo $srvcpane; ?>" data-ip="<?php echo $thisip; ?>">...</span>
                    <?php endif; ?>
                </td>
                <td>
                    <a href="index.php?ping=<?php echo $thisip; ?>">PING</a>
                    <a href="index.php?get=<?php echo $thisip; ?>">SRVC</a>
                    <a href="index.php?print=<?php echo $thisip; ?>">PRINT</a>
                    <a href="index.php?sshin=<?php echo $thisip; ?>">REBOOT</a>
                </td>
                <td> </td>
                <td><?php echo $dev['last_version']; ?></td>
                <td><?php echo (strtotime($dev['last_login']) > strtotime('-24 hours')) ? $dev['last_login'] : '<s>'.$dev['last_login'].'</s>'; ?></td>
            </tr>
            <?php
        }
    }
    ?>
</table>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(function(){
        <?php if (isset($_GET['allService'])): ?>
        $.ajaxSetup({ async: false });
        /*
        $('.srvc-pane').each(function() {
            var ip = $(this).attr('data-ip');
            var x = $(this).attr('id');

            $.get( "index.php?get-app=" + ip, function( data ) {
                $('#'+x).text(data)
            });
        });
        */
        var panes = ["<?php echo implode('","', $serviceSet); ?>"];

        var i;
        for (i = 0; i < panes.length; i++) {
            var ip = $('#'+panes[i]).attr('data-ip');

            $.ajax({
                async: false,
                type: "GET",
                url: "index.php?get-app=" + ip,
                dataType: 'html',
                timeout: 2000,
                success: function(data){
                    console.log(ip+' '+data);
                    var pp = panes[i];
                    $('#'+pp).html(data)
                }
            });

        };

        <?php endif; ?>
    });

</script>