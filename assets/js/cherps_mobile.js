
/* YS
 * 20-07-2016
 */

$(function () {

    var web_url = $("#web_url").val();
    var LoadingCircle = "<center><img src='" + web_url + "assets/dist/img/ajax-loading.gif' width='50px'><br><br>Loading data</center>";

    // USER
    if ($("#cherps_user").length) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: web_url + "api/challenger_mis/"+USR_ID+"/"+COY_ID,
            success: function (msg) {
                var data = JSON.parse(msg);

                var HTML = '';
                $.each(data.sys, function (k, dt) {
                    HTML += '<div class="col-xs-4 text-grey">'+k+':</div><div class="col-xs-8 no-padding text-bold pickcontnt">'+dt+'&nbsp;</div>';
                })
                $("#usr_sys").html(HTML);

                var HTML = '';
                $.each(data.emp, function (k, dt) {
                    HTML += '<div class="col-xs-4 text-grey">'+k+':</div><div class="col-xs-8 no-padding text-bold pickcontnt">'+dt+'&nbsp;</div>';
                })
                $("#usr_emp").html(HTML);

                var HTML = '';
                $.each(data.coy, function (k, row) {
                    HTML += '<div class="col-xs-12 no-padding clearfix" style="padding:5px 0px !important;border-top:1px solid #CCC;border-bottom:1px solid #CCC;">';
                    $.each(row, function (k, dt) {
                        HTML += '<div class="col-xs-4 text-grey">'+k+':</div><div class="col-xs-8 no-padding text-bold pickcontnt">'+dt+'&nbsp;</div>';
                    })
                    HTML += '<a href="?usr='+USR_ID+'&coy='+row.coy_id+'" class="btn btn-grey">VIEW BY</a> ';
                    HTML += '<a href="#" data-toggle="modal" data-target="#modal_coy" data-datapoint="'+k+'" class="btn btn-grey">AMEND</a>';
                    HTML += '</div>';
                })
                $("#usr_coy").html(HTML);

                var HTML = '';
                $.each(data.grp, function (k, row) {
                    if (row.status_level == 'Y') {
                        HTML += '<div class="col-xs-12 no-padding clearfix" style="padding:5px 0px !important;border-top:1px solid #CCC;border-bottom:0px solid #CCC;">';
                        $.each(row, function (k, dt) {
                            HTML += '<div class="col-xs-4 text-grey">' + k + ':</div><div class="col-xs-8 no-padding text-bold pickcontnt">' + dt + '&nbsp;</div>';
                        })
                        HTML += '</div>';
                    }
                    else {
                        $('#sel_grp').append($('<option>', {
                            value: row.grp_id,
                            text: row.grp_id +' - '+ row.grp_desc
                        }));
                    }
                })
                $("#usr_grp").html(HTML);

                var HTML = '', prevMenu='', prevFunc='0';
                $.each(data.menu, function (k, row) {
                    HTML += (prevMenu!='' && row.sys_id!=prevMenu) ? '<hr>' : '';
                    HTML += (row.sys_id!=prevMenu) ? row.sys_id + ':<br>' : '';
                    HTML += '<div class="col-xs-7 text-grey">' + row.func_num + '.' + row.menu_num + ': ' + row.menu_desc + '</div>';
                    HTML += '<div class="col-xs-5 no-padding text-bold pickcontnt">'+ row.grant_coy + '<span class="pull-right">' + row.grant_type + '('+row.grant_ref+')</span>&nbsp;</div>';

                    prevMenu = row.sys_id;
                    prevFunc = row.func_num;
                })
                $("#usr_menu").html(HTML);

                var HTML = '';
                $.each(data.types, function (k, row) {
                    HTML += '<div class="col-xs-12 no-padding clearfix" style="padding:5px 0px !important;border-top:1px solid #CCC;border-bottom:1px solid #CCC;">';
                    HTML += '<div class="col-xs-2 text-grey">'+ row.sys_id +'</div>';
                    HTML += '<div class="col-xs-5 text-grey">'+ row.type_code + ' ('+row.type_data+')' +'</div>';
                    HTML += '<div class="col-xs-5 text-grey">'+ row.grant_coy + '<span class="pull-right">' + row.grant_type + '('+row.grant_ref+')</span></div>';
                    HTML += '</div>';
                })
                $("#usr_types").html(HTML);

                var HTML = '';
                $.each(data.settings, function (k, row) {
                    HTML += '<div class="col-xs-12 no-padding clearfix" style="padding:5px 0px !important;border-top:1px solid #CCC;border-bottom:1px solid #CCC;">';
                    HTML += '<div class="col-xs-2 text-grey">'+ row.sys_id +'</div>';
                    HTML += '<div class="col-xs-5 text-grey">'+ row.set_code + ' ('+row.set_data+')' +'</div>';
                    HTML += '<div class="col-xs-5 text-grey">'+ row.grant_coy + '<span class="pull-right">' + row.grant_type + '('+row.grant_ref+')</span></div>';
                    HTML += '</div>';
                })
                $("#usr_settings").html(HTML);



                // console.log(data.sys)
                // console.log(data.emp)
                // console.log(data.coy)
                // console.log(data.grp)
                // console.log(data.menu)
                // console.log(data.types)
                // console.log(data.settings)


            }
        });


    }


});