
/* YS
 * 20-07-2016
 */

$(function () {
    
    function MobileHiding(){
        $(".h-mobile").each(function() {
            $( this ).addClass("hidden-sm").addClass("hidden-xs");
        });
    }
    $( document ).ajaxComplete(function() {
        MobileHiding();
    });
    MobileHiding();
        
    function ActivityLogging(msg) { 
        if (msg=='SHOW') { 
            $("#UserActivityLog").modal('show');
        }
        else if (msg=='HIDE') { 
            $("#UserActivityLog").modal('hide');
        }
        else if (msg=='RESTORE') { 
            var TEXT = localStorage.getItem('chvoiceslog');
            if (TEXT != null ) {
                var TEXTlines = TEXT.split('@');
                var TEXTcut = TEXTlines.length - 10;
                if (TEXTcut>0) {
                    TEXT = '@' + TEXTlines.slice(TEXTcut).join('@');
                }
            }
            $("#UserActivityLog_TEXT").html( TEXT )
        }
        else {
            var currentdate = new Date(); 
            var datetime = "@" + currentdate.getDate() + "/" + (currentdate.getMonth()+1)  + "/" + currentdate.getFullYear() + " "  
                                + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        
            $("#UserActivityLog_TEXT").append(datetime + ' - ' + msg.replace(/(<([^>]+)>)/ig,"").replace(/","/ig,'", "') + "\n");
            localStorage.setItem('chvoiceslog', $("#UserActivityLog_TEXT").html() );
        }
    }
    $("body").delegate(".OpenActivityLog", "click", function () {
        ActivityLogging('SHOW');
    });
    $("body").delegate(".MailActivityLog", "click", function () {
        var data = $("#UserActivityLog_TEXT").html()

        $.ajax({
            type: "POST",
            //contentType: "application/json; charset=utf-8",
            url: web_url + "home/UserActivityLog",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (msg) {
                $(".MailActivityLog").hide();
            }
        });
    });
    ActivityLogging('RESTORE');
    
    $('.toggle-arrow-menu').click(function (e) {
        if ( $('#toggle-arrow-menu').hasClass("fa-arrow-left") ) {
            $('#toggle-arrow-menu').removeClass("fa-arrow-left");
            $('#toggle-arrow-menu').addClass("fa-arrow-right");
        }
        else {
            $('#toggle-arrow-menu').removeClass("fa-arrow-right");
            $('#toggle-arrow-menu').addClass("fa-arrow-left");
        }
    });

    var web_url = $("#web_url").val();
    var LoadingCircle = "<center><img src='" + web_url + "assets/dist/img/ajax-loading.gif' width='50px'><br><br>Loading data</center>";

    function FormatSmDatatable(table) {
        if (table === undefined)
            table = "#smdatatable";
        var tb = $(table).DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "pageLength": 10,
            "responsive": true,
            "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
        });
        return tb;
    }
    var JS_SmDatatable = FormatSmDatatable();

    function FormatDatatable(table, pglen) {
        if (table === undefined)
            table = "#datatable";
        if (pglen === undefined)
            pglen = "50";
        var tb = $(table).DataTable({ 
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "pageLength": pglen,
            "responsive": true,
            "lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
            "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
        });


        $("#datatable_filter label").css("color", "#FFF");
        $("#datatable_filter label input").attr("placeholder", "Search all");
//        $span=$("#GRNitemstable_search").clone();
//        //$("#votes").remove();
//        $("#datatable_filter label").append($span);
        return tb;
    }
    var JS_Datatable = FormatDatatable();

    function ScrollTo(div) {
        $('html,body').animate({
            scrollTop: $("#" + div).offset().top},
                'slow');
    }

    $("input[type=file]").each(function () {
        var Placeholder = $(this).attr("placeholder");
        $(this).nicefileinput({
            label: Placeholder
        });
    });
    
    var dateToday = new Date(); 
    $('.datepicker').datepicker({autoclose: true});
    $('.datepicker_future').datepicker({autoclose: true, startDate:dateToday });
    $('.datepicker_past').datepicker({autoclose: true, endDate:dateToday });
    
    $(document).delegate(".popuplink", "click", function () {
    //$('.popuplink').click(function (e) {
        var url = $(this).attr("href");
        window.open(url, 'PopUp', 'window settings');
        return false;
    });

    if ($("#Invoice_listing").length) {

        function listingInvGRN(term, pagination) {
            if (pagination === undefined)
                pagination = true;

            $("#listing_grn_div").show();
            $("#datatable_div").html(LoadingCircle);
            ScrollTo('listing_grn_div');
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: web_url + "invoice/get_grn_items/1?search=" + term,
                success: function (msg) {
                    $("#datatable_div").html(msg);
                    //FormatDatatable('#listing_grn_div #datatable'); 
                    $("#listing_grn_div #datatable").DataTable({
                        "paging": pagination,
                        "lengthChange": true,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "pageLength": 50,
                        "lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
                        "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
                    });
                }
            });
        }

        $("#listing_grn_div").hide();
        $("#listing_info_div").hide();
        $("#ViewGRNFullCloseSpan").hide();

//        var datatableX = 0;
//        $(document).on('scroll', function () {
//            if (datatableX == 0) {
//                datatableX = 1;
//                $("#datatableIL").DataTable({
//                    "paging": true,
//                    "lengthChange": true,
//                    "searching": true,
//                    "ordering": true,
//                    "info": true,
//                    "autoWidth": true,
//                    "pageLength": 50,
//                    "lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
//                    "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
//                });
//            }
//        });

        $("body").delegate(".ViewGRN", "click", function () {
        //$(".ViewGRN").click(function () {
            var search = $(this).attr("id");
            listingInvGRN(search);
            ScrollTo('listing_grn_div');
            return false;
        });

        $("body").delegate(".ViewGRNFull", "click", function () {
        //$(".ViewGRNFull").click(function () {
            var sid = $(this).attr("id");
            listingInvGRN(sid, false);
            $("#listing_list_div").hide();
            $("#ViewGRNFullCloseSpan").show();

            var inv = $("#L_inv_" + sid).val();
            $("#listing_info_div").show();
            $("#listing_info_div #inv_no").val(inv);
            $("#listing_info_div #inv_date").val($("#L_date_" + sid).val());
            $("#listing_info_div #inv_amt").val($("#L_amt_" + sid).val());

            $("#listing_info_div #myUploadFiles").html('');
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: web_url + "invoice/get_grn_items_files/" + inv,
                success: function (msg) {
                    $("#listing_info_div #myUploadFiles").html(msg);
                }
            });
            return false;
        });

        $("body").delegate("#ViewGRNFullClose", "click", function () {
        //$("#ViewGRNFullClose").click(function () {
            $("#listing_list_div").show();
            $("#listing_info_div").hide();
            $("#listing_grn_div").hide();
            $("#ViewGRNFullCloseSpan").hide();
            return false;
        });

        function EmailGRN(term) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: web_url + "invoice/email_matched_page/" + term,
                success: function (msg) {
                    alert(msg);
                }
            });
        }

        $("body").delegate(".EmailGRN", "click", function () {
        //$(".EmailGRN").click(function () {
            var search = $(this).attr("id");
            EmailGRN(search);
            return false;
        });

    }

    if ($("#Invoice_submit_new_match_DIV").length) {
        // Invoice matching (single) & Invoice matching (batch)

        $('#inv_no').change(function () {
            var inv = $("#inv_no").val();
            if (inv != '') {
                $("#myUploadFiles").html('');
                var data = new FormData();
                data.append('inv_no', inv);
                $.ajax({
                    type: "POST",
                    //contentType: "application/json; charset=utf-8",
                    url: web_url + "invoice/submit_new_match_upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (msg) {
                        $("#myUploadFiles").html(msg);
                    }
                });
                $("#submit_inv_btn").prop('disabled',false);
            } else {
                $("#myUploadFiles").html('');
                $("#submit_inv_btn").prop('disabled',true);
            }
        });

        $('#inv_amt').change(function () {
            var inv_amt = parseFloat($("#inv_amt").val());
            if (inv_amt != '') {
                $("#inv_amt").val(inv_amt.toFixed(2));
            }
        });

        function LoadGRNitems() {
            $(".dataTables_empty").html(LoadingCircle);
            // $("#datatable_div").html(LoadingCircle);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: web_url + "invoice/get_grn_items?export=1",
                success: function (msg) {
                    $("#datatable_div").html(msg);
                    JS_Datatable = FormatDatatable();
                    $("#datatable_filter").parent().attr("class", "col-sm-8");
                    $("#GRNitemstable_search").appendTo("#datatable_filter label"); 
                    $("#datatable_length").parent().attr("class", "col-sm-4");
                    $("#ItembtnSpan").appendTo("#datatable_length label");

                }
            });
        }
        $('#LoadGRNitems').click(function () {
            LoadGRNitems();
        });
        // submit_new_match page default:
        $("#submit_inv_btn").prop('disabled',true);
        //$(".dataTables_empty").html('<center><br><a href="#" id="LoadGRNitems" class="btn btn-sm btn-warning">Retrieve Outstanding Items</a><br>&nbsp;</center>');
        $(".dataTables_empty").html(LoadingCircle);
        setTimeout(function () {
            LoadGRNitems();
            //$(".dataTables_empty").html('<center><br><a href="#" id="LoadGRNitems" class="btn btn-sm btn-warning">Retrieve Outstanding Items</a><br>&nbsp;</center>');
        }, 1500);
        

        
        $('#GRNitemstable_search_po').on('keyup', function () {
            JS_Datatable
                    .columns(1)
                    .search(this.value)
                    .draw();
        });
        $('#GRNitemstable_search_do').on('keyup', function () {
            JS_Datatable
                    .columns(9)
                    .search(this.value)
                    .draw();
        });

        $('#myUploadModalBtn').click(function (e) {
            e.preventDefault();
            if (LOADCOPYDO==1) {
                copydo();
            }
            if ($("#inv_no").val() == "") {
                alert("Please enter the invoice number to continue");
                return false;
            }
            else {
                return $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: web_url + "invoice/quickcheck/invoice_id/" + $("#inv_no").val() ,
                    success: function (msg) {
                        var msgdata = JSON.parse(msg);
                        if (msgdata.code<=0) {
                            alert(msgdata.msg);
                            return false;
                        }
                        else {
                            $('#myUploadModal').modal('show');
                        }
                    }
                });
            }
        });

        $('#myMatchModalSender').click(function () {
            $('#myMatchModalSender').prop('disabled', true);
            $('#myMatchModalCloser').trigger("click");
            $("#GRNitemstable_search").appendTo("#hidden_div");
            $("#datatable_div").html(LoadingCircle);

            var data = new FormData();
            data.append('upload_type', "match");
            data.append('upload_file', $('#upload_match')[0].files[0]);

            $.ajax({
                type: "POST",
                //contentType: "application/json; charset=utf-8",
                url: web_url + "invoice/get_grn_items",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (msg) {
                    $("#datatable_div").html(msg);
                    JS_Datatable = FormatDatatable("#datatable", "500");
                    $("#GRNitemstable_search").appendTo("#datatable_filter label");
                    $('#myBatchModalSender').prop('disabled', false);
                }
            });
        });

        $('#myUploadModalSender_Com').click(function () {
            if ($("#inv_no").val() == "") {
                alert("Please enter the invoice number to continue");
                $('#myUploadModalCloser').trigger("click");
            } else {

                if ($('#upload_inv_do')[0].files[0] === undefined) {
                    alert("Please select the file to continue");
                    return false;
                }

                $('#myUploadModalSender_Com').prop('disabled', true);

                var data = new FormData();
                data.append('upload_file_invdo', $('#upload_inv_do')[0].files[0]);
                data.append('inv_no', $("#inv_no").val());

                $.ajax({
                    type: "POST",
                    //contentType: "application/json; charset=utf-8",
                    url: web_url + "invoice/submit_new_match_upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (msg) {
                        $("#myUploadFiles").html(msg);

                        $('#myUploadModalSender_Com').prop('disabled', false);
                        $('#myUploadModalSender_ComFm').trigger('reset');
                        $('#myUploadModalCloser').trigger("click");

                    }
                });
            }
        });

        $('#myUploadModalSender_Sep').click(function () {
            if ($("#inv_no").val() == "") {
                alert("Please enter the invoice number to continue");
                $('#myUploadModalCloser').trigger("click");
            } else {

                if ($('#upload_inv')[0].files[0] === undefined || ($('#upload_do')[0].files[0] === undefined && $("#copy_do_input").val()=='') ) {
                    alert("Please select the file to continue");
                    return false;
                }

                $('#myUploadModalSender_Sep').prop('disabled', true);

                var data = new FormData();
                data.append('upload_file_inv', $('#upload_inv')[0].files[0]);
                data.append('upload_file_do', $('#upload_do')[0].files[0]);
                data.append('upload_copy_do', $('#copy_do_input').val());
                data.append('inv_no', $("#inv_no").val());

                $.ajax({
                    type: "POST",
                    //contentType: "application/json; charset=utf-8",
                    url: web_url + "invoice/submit_new_match_upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (msg) {
                        $("#myUploadFiles").html(msg);

                        $('#myUploadModalSender_Sep').prop('disabled', false);
                        $('#myUploadModalSender_SepFm').trigger('reset');
                        $('#myUploadModalCloser').trigger("click");

                    }
                });
            }
        });

        //$('.myUploadModalSender_Delete').click(function () { 
        $("#Invoice_submit_new_match_DIV").delegate(".myUploadModalSender_Delete", "click", function () {
            var file = $(this).data("target");
            $("#"+file).remove();
        });

        $('.myBatchModalCaller').click(function () {
            $("#myBatchModal .modal-footer").show();
            $("#submit_new_save_batch_info").show();
            $("#myBatchModalFiles").html('');
            $("#submit_new_save_batch_div").html('');
            $('#Invoice_submit_batch1').trigger('reset');
            $('#Invoice_submit_batch2').trigger('reset');
            $("#Invoice_submit_batch2_CloseDiv").show();
        });

        $('#myBatchModalSp2Btn').click(function (e) {
            $('#myBatchModalSp2').trigger("click");
        });

        $('#myBatchModalSp1Btn').click(function (e) {
            $('#myBatchModalSp1').trigger("click");
        });

        $('#myBatchModal #myBatchModalSp2').click(function (e) {
            $("#submit_new_save_batch_info").hide();
            if ($('#myBatchModalFiles #uploads_0').length) {
                $("#submit_new_save_batch_div").html('');
                $('#Invoice_submit_batch2').trigger('reset');
                $("#Invoice_submit_batch2_CloseDiv").show();
            } else {
                $("#myBatchModal #myBatchModalSp1").trigger("click");
                $("#myBatchModalFiles").show();
                $("#myBatchModalFiles").html('<font color=red>Please upload Supporting Documents before continuing.</font>');
                return false;
            }
        });

        var myBatchReload_JSON = "";
        var myBatchReload_FILES = [];
        function myBatchReload() { 
            // Run through json and show table. 
            myBatchReload_FILES = [];
            $("#datatable8 tbody").html('<tr></tr>');
            $.each(myBatchCSV, function (id, val) { 
                myBatchReload_FILES.push( id.toUpperCase() + '.PDF' );
                myBatchReload_FILES.push( id.toUpperCase() + '-DO.PDF' );
                myBatchReload_FILES.push( id.toUpperCase() + '-INV.PDF' );
                $('#datatable8 tr:last').after('<tr><td>'+val.po_id+'</td><td>'+id+'</td><td>'+val.inv_date+'</td><td>'+parseFloat(val.inv_amt).toFixed(2)+'</td><td><div id="F_'+id+'"></div></td></tr>');
            });
            
            if (myBatchReload_JSON!=""){
                
                $("#myBatchModalFiles").hide();
                $("#myBatchModalFiles").html('Some uploaded documents encountered problem when matching to respective invoices (please check the filename before uploading):<br><br>');
                var obj = JSON.parse(myBatchReload_JSON) 
                $.each(obj, function (k, file) {
                    if ( file.slice(-3).toUpperCase()=="PDF" ){
                        var fileinv = file.toUpperCase().replace(/-INV|-DO|.PDF/gi, '').replace(myBatchCSV_prefix,'').trim(); 
                        if (myBatchCSV.hasOwnProperty(fileinv)){
                            // Matched files
                            $("#F_"+fileinv).append( myBatchReload_FileBtn(file) );
                        }
                        else {
                            // Files not matched
                            $("#myBatchModalFiles").show();
                            $("#myBatchModalFiles").append( myBatchReload_FileBtn(file) ); 
                        }
                    }
                })
            } 
            
        }
        function myBatchReload_FileBtn(file){ 
            var filename = file.replace(myBatchCSV_prefix,'').trim();
            
            var html = '<div class="col-xs-4"><div class="label label-default" style="margin-bottom:5px;display:block;width:100%;text-align:left"><div style="padding:5px 3px 3px 3px">';
            html += '<a title="'+file+'" href="' + web_url + 'assets/uploads/'+file+'" class="popuplink" style="font-weight:normal;color:#000"><i class="fa fa-file-pdf-o"></i>&nbsp; '+filename+'</a>';
            html += '<a href="#" id="'+file+'" title="Remove File" class="myBatchModalFilesDelete" style="float:right;color:#F00">X</a>';
            html += '</div></div></div>';
            return html;
        }
        if (typeof myBatchCSV!="undefined"){
            myBatchReload();
            
            if ( $.browser.chrome || $.browser.msedge ) {
                $("#upload_dir_txt").html("<font color=red>Select the directory containing the PDF documents. Relevant documents will be pulled automatically.</font>");
                ActivityLogging('Browser supports Directory upload')
            }
            else {
                ActivityLogging('Browser support File upload only')
            }
        }
        
        $('#myBatchModalSp1_UploadBtn').click(function (e) { 
            
            $("#upload_dir_header").html(' <a href="#" class="OpenActivityLog">(Show Log)</a>');
            ActivityLogging('CLICK: Uploading supporting documents (myBatchModalSp1_UploadBtn)')
            
            $("#submit_new_save_batch_info").hide();
            $("#myBatchModalFiles").show();
            $("#myBatchModalFiles").html(LoadingCircle);

            var data = new FormData();
            data.append('upload_type', "batch");
            var u = 0;
            /*for (var i = 0; i < $('#upload_batch')[0].files.length; ++i) {
                var ifile = $('#upload_batch')[0].files[i];
                if ($.inArray( ifile.name.toUpperCase() , myBatchReload_FILES ) != -1 ) {
                    data.append('upload_file' + u, ifile);
                    u++;
                }
            }*/
            for (var i = 0; i < $('#upload_dir')[0].files.length; ++i) {
                var ifile = $('#upload_dir')[0].files[i];
                var ifilemsg = 'Found ' + ifile.name;
                if ($.inArray( ifile.name.toUpperCase() , myBatchReload_FILES ) != -1 ) {
                    ifilemsg += ' (matched)';
                    data.append('upload_file' + u, ifile);
                    u++;
                }
                ActivityLogging(ifilemsg)
            }
            
            var web_url2 = '';
            var uploadedcsv_name = $("#uploadedcsv_name").val();
            if (typeof uploadedcsv_name!="undefined" && uploadedcsv_name!=''){
                //data.append('uploadedcsv_name',uploadedcsv_name)
                web_url2 = '?uploadedcsv_name='+uploadedcsv_name;
            } 
            
            var BatchURL = web_url + "invoice/submit_new_save_batch_upload" + web_url2;
            ActivityLogging('Posting ' + BatchURL)

            $.ajax({
                type: "POST",
                //contentType: "application/json; charset=utf-8",
                url: BatchURL,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (msg) {
                    
                    ActivityLogging('Reply: '+msg)
                    
                    if (typeof uploadedcsv_name!="undefined" && uploadedcsv_name!=''){
                        // Do client-side matching for display on own page
                        myBatchReload_JSON = msg;
                        myBatchReload();
                    }
                    else {
                        $("#myBatchModalFiles").html(msg);
                    }
                    $('#myBatchModalSender').prop('disabled', false);
                    $('#Invoice_submit_batch1').trigger('reset');
                }
            });

        });

        //$('.myBatchModalFilesDelete').click(function (e) {
        $("#myBatchModal").delegate(".myBatchModalFilesDelete", "click", function () {
            $("#submit_new_save_batch_info").hide();
            $("#myBatchModalFiles").show();
            $("#myBatchModalFiles").html(LoadingCircle);
            
            var del_filename = $(this).attr("id");
            
            ActivityLogging('CLICK: Delete myBatchModalFilesDelete ' + del_filename)
            
            var web_url2 = '';
            var uploadedcsv_name = $("#uploadedcsv_name").val();
            if (typeof uploadedcsv_name!="undefined" && uploadedcsv_name!=''){
                //data.append('uploadedcsv_name',uploadedcsv_name)
                web_url2 = '&uploadedcsv_name='+uploadedcsv_name;
            } 

            var BatchURL = web_url + "invoice/submit_new_save_batch_upload_delete?filename=" + del_filename + web_url2;
            ActivityLogging('Posting ' + BatchURL)
            
            $.ajax({
                type: "GET",
                //contentType: "application/json; charset=utf-8",
                url: BatchURL,
                //data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (msg) { 
                    
                    ActivityLogging('Reply: '+msg)
                    
                    if (typeof uploadedcsv_name!="undefined" && uploadedcsv_name!=''){
                        // Do client-side matching for display on own page
                        myBatchReload_JSON = msg;
                        myBatchReload();
                    }
                    else {
                        $("#myBatchModalFiles").html(msg);
                    } 
                    $('#myBatchModalSender').prop('disabled', false);
                    $('#Invoice_submit_batch1').trigger('reset');
                }
            });
        });

        $('#myBatchModalSender').click(function (e) {
            $('#myBatchModalSender').prop('disabled', true);
            $("#submit_new_save_batch_div").html(LoadingCircle);
            ActivityLogging('CLICK: Submiting Invoice Matching (Batch)')

            var data = new FormData();
            
            var uploadedcsv_name = $("#uploadedcsv_name").val();
            if (typeof uploadedcsv_name!="undefined" && uploadedcsv_name!=''){
                data.append('uploadedcsv_name',uploadedcsv_name)
            }
            else {
                data.append('upload_file', $('#upload_batch_csv')[0].files[0]);
            }
            
            var BatchURL = web_url + "invoice/submit_new_save_batch";
            ActivityLogging('Posting ' + BatchURL)

            $.ajax({
                type: "POST",
                //contentType: "application/json; charset=utf-8",
                url: BatchURL,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (msg) {
                    
                    ActivityLogging('Reply: '+msg)
                    
                    $("#submit_new_save_batch_div").html(msg);
                    $('#myBatchModalSender').prop('disabled', false);
                    $("#Invoice_submit_batch2_CloseDiv").hide();
                }
            });
        });

        function tally_selected() {
            var inv_amt = parseFloat($("#inv_amt").val());
            var tally_amt = 0;
            var grnqty = 0;
            $('.cb').each(function () {
                if ($(this).prop('checked')) {
                    var sel_id = $(this).attr("id").substring(3);
                    var amt = parseFloat($("#amt_" + sel_id).val());
                    var tax = parseFloat($("#tax_" + sel_id).val());
                    grnqty += parseFloat($("#grnqty_" + sel_id).val());
                    tally_amt += amt + tax; 
                }
            });
            $("#inv_amt_tally").val(tally_amt.toFixed(2));
            $("#inv_amt_diff").val((inv_amt - tally_amt).toFixed(2));
            $("#inv_qty_tally_cp").html('Selected Qty: <b>' + grnqty.toFixed(0) + '</b> ');
            $("#inv_amt_tally_cp").html('Total Amount (w/ Tax): <b>$' + tally_amt.toFixed(2) + '</b> ');
            return tally_amt;
        }

        var cbChecked = 0;
        $("#sel_all_btn").click(function () {
            cbChecked = (cbChecked == 1) ? 0 : 1;
            $('input:checkbox.cb').each(function () {
                if (cbChecked == 1) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
            tally_selected();
        });
        $("#inv_amt_tally").click(function () {
            tally_selected();
        });
        $('#Invoice_submit_new_match').submit(function (event) {
            var tally_amt = tally_selected();
            var inv_amt = parseFloat($("#inv_amt").val());
            if ( $(":input[name='uploads[]']").length==0 ) {
                alert('Please upload supporting documents');
                return false;
            }
            else {
                if (inv_amt > 0 && (inv_amt + 0.1) > tally_amt && (inv_amt - 0.1) < tally_amt) {
                    return confirm("Are you sure to save this invoice?");
                } else {
                    alert("Selected items $" + tally_amt.toFixed(2) + " do not match the invoice amount ($" + inv_amt.toFixed(2) + ").");
                    return false;
                }
            }
        });

        $("#copy_do").hide();
        function copydo(){
            $("#upload_do_div").show();
            $("#copy_do").show();
            $("#copy_do_input").val('');
            var po_num = '', po_this='';
            var do_num = '', do_this='';
            $('input:checkbox.cb:checked').each(function () {
                po_this = $(this).data('po');
                do_this = $(this).data('do');
                if (po_num=='' && do_num=='') {
                    po_num = po_this;
                    do_num = do_this;
                }
                else if (po_num!=po_this || do_num!=do_this) {
                    po_num = 'MULTI';
                }
            });
            if (po_num=='') {
                $("#copy_do").html("Select GRN items from a PO/DO to find uploaded DO from system.");
            }
            else if (po_num=='MULTI') {
                $("#copy_do").html("Selected GRN items has multiple PO/DO. Please select GRN items from a PO/DO to find the uploaded DO from system.");
            }
            else {
                // Load for DO
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: web_url + "invoice/get_do_files/" + po_num + '-' + do_num ,
                    success: function (msg) {
                        var msgdata = JSON.parse(msg);
                        if (msgdata.code>0) {
                            $("#upload_do_div").hide();
                            $("#copy_do_input").val(msgdata.filepath);
                            $("#copy_do").html('<div style="float:left;text-align:center;padding:0px 15px"><a title="' + msgdata.file + '" href="' + msgdata.url + '" class="popuplink"><span class="fa-2x fa fa-file-pdf-o"></a></div><div style="padding:3px">1 DO found for ' + po_num + ' found</div>')
                        }
                        else {
                            $("#copy_do").html("No uploaded DO found for the selected PO/GRN items.");
                        }
                    }
                });
            }
        }

    }

    if ($("#Return_Note_Coll").length) {
        
        var viewSingle = '';
        
        function showRNexportbtn(rn,cnt) {
            var PDF = '';
//            if ( cnt==1 ){ 
//                var rn_dn = rn.substr(0, rn.length-1);
//                if ( $("#Debitnote_" + rn_dn).length > 0 )
//                    PDF += '<a href="' + web_url + 'rn/dn_pdf/' + $("#Debitnote_" + rn_dn).val() + '" target="_blank" class="popuplink btn btn-warning">Print DN</a> &nbsp; ';
//            } 
            PDF += '<a href="' + web_url + 'rn/rn_export/rn/' + rn + '" target="_blank" class="popuplink btn btn-warning"><i class="fa fa-print"></i> Print RN w/ cost</a> &nbsp; ';
            PDF += '<a href="' + web_url + 'rn/rn_export/rndo/' + rn + '" target="_blank" class="popuplink btn btn-warning"><i class="fa fa-print"></i> Print RN w/o cost</a> &nbsp; ';
            PDF += '<a href="' + web_url + 'rn/rn_export/csv/' + rn + '" target="_blank" class="popuplink btn btn-warning"><i class="fa fa-arrow-up"></i> Export CSV</a> &nbsp; ';
            $("#rn_pdf_div").html(PDF);
        }

        function showRNdetails(rn) {
            $("#UpdateCollectionDate_Div").show();
            $("#RMAfiles_Resp").show();
            $("#myUploadFiles").html("<img src='" + web_url + "assets/dist/img/ajax-loading.gif' width='38px'>");
            
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: web_url + "rn/get_rn_files/" + rn,
                success: function (msg) {
                    $("#myUploadFiles").html(msg);
                }
            });
            
            T = '# ' + rn;
            $("#UpdateCollectionDate_Div #sel_num").text("(" + T + ")");

            if ($("#UpdateCollectionDate_Div #debitnote_div").length) {
                $("#debitnote_field").val($("#Debitnote_" + rn).val());
            }
            if ($("#UpdateCollectionDate_Div #newcodate_div").length) {
                $("#new_codate").val($("#CodateOld_" + rn).val());
            }
        }

        function showUpdateCollection() {
            var cbOpen = 0;
            var rns = '';
            $('input:checkbox.cb:checked').each(function () {
                cbOpen++;
                rns += $(this).val() + ',';
            });
            
            if ($("#UpdateCollectionDate_Div #debitnote_div").length) {
                $("#debitnote_field").val('');
            }
            if ($("#UpdateCollectionDate_Div #newcodate_div").length) {
                $("#new_codate").val('');
            }

            $("#RMAfiles_Resp").hide();
            $("#rn_pdf_div").html('');
            $("#myUploadFiles").html('');
            $("#UpdateCollectionDate_Div #sel_num").text("");

            if (cbOpen > 0) {
                var T = (cbOpen > 1) ? cbOpen + ' RNs Selected' : '1 RN Selected';
                $("#UpdateCollectionDate_Div #sel_num").text("(" + T + ")");

                if (cbOpen == 1) {
                    var rn = rns.substr(0, rns.length-1);
                    showRNdetails(rn);
                }
                showRNexportbtn(rns,cbOpen);

                //$('#UpdateCollectionDate_Btn').prop('disabled', false);
                $("#UpdateCollectionDate_Div").show();
            } else {
                //$("#UpdateCollectionDate_Div").hide();
                //$('#UpdateCollectionDate_Btn').prop('disabled', true);
            }
        }

        $("#RMAfiles_Resp").hide();
        //$("#UpdateCollectionDate_Div").hide();
        //$('#UpdateCollectionDate_Btn').prop('disabled', true);

        $("body").delegate("#UpdateCollectionDate_Btn", "click", function () {
        //$("#UpdateCollectionDate_Btn").click(function () {
            //$('#UpdateCollectionDate_Btn').prop('disabled', true);
            
            var trans_id_cnt = 0;
            var date = $('#UpdateCollectionDate_Div #new_codate').val();
            var data = new FormData();
            data.append('new_codate', date);
            if (viewSingle!=''){
                data.append('new_trans_id[]', $("#cb_"+viewSingle).val());
                trans_id_cnt++;
            }
            else {
                $('input:checkbox.cb:checked').each(function () {
                    data.append('new_trans_id[]', $(this).val());
                    trans_id_cnt++;
                });
            }

            if ( date == "" ) {
                alert("Please select collection date");
                //$('#UpdateCollectionDate_Btn').prop('disabled', true);
                return false;
            }
            if ( trans_id_cnt == 0) {
                alert("Please select at least one item");
                //$('#UpdateCollectionDate_Btn').prop('disabled', true);
                return false;
            }

            $.ajax({
                type: "POST",
                //contentType: "application/json; charset=utf-8",
                url: web_url + "rn/updatedate",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (msg) {
                    if (viewSingle!=''){
                        $('#codate_' + viewSingle).html(date);
                    }
                    else {
                        $('input:checkbox.cb:checked').each(function () {
                            var sid = $(this).attr("id").substring(3);
                            $('#codate_' + sid).html(date);
                            $(this).prop('checked', false);
                        });
                    
                        $('#UpdateCollectionDate_Div #new_codate').val('');
                        showUpdateCollection();
                    }
                }
            });
        });

        var cbChecked = 0;
        $("body").delegate("#sel_all_btn", "click", function () {
        //$("#sel_all_btn").click(function () {
            cbChecked = (cbChecked == 1) ? 0 : 1;
            $('input:checkbox.cb').each(function () {
                if (cbChecked == 1) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
            showUpdateCollection();
        });

        $("body").delegate(".cb", "click", function () {
        //$('.cb').click(function () {
            showUpdateCollection();
        });
        $("body").delegate(".cbdate", "click", function () {
        //$('.cbdate').click(function () {
            var cid = $(this).attr("id").substring(7);
            if ($("#cb_"+cid).prop('checked')==true)
                $("#cb_"+cid).prop('checked', false);
            else
                $("#cb_"+cid).prop('checked', true);
            showUpdateCollection();
        });

        function listingGRN(term) {
            $("#listing_rn_div").show();
            $("#datatable_div").html(LoadingCircle);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: web_url + "rn/items/" + term,
                success: function (msg) {
                    $("#datatable_div").html(msg);
                    //FormatDatatable('#listing_grn_div #datatable'); 
                    $("#listing_grn_div #datatable").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "pageLength": 50,
                        "lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
                        "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
                    });
                }
            });
        }

        $("#listing_rn_div").hide();
        $("#ViewGRNFullCloseSpan").hide();

//        $(".ViewGRN").click(function () {
//            var search = $(this).attr("id");
//            listingGRN(search);
//            ScrollTo('listing_rn_div');
//            return false;
//        });

        $("body").delegate(".ViewGRNFull", "click", function () {
        //$(".ViewGRNFull").click(function () {
            
            viewSingle = $(this).attr("id2").substring(3); 
            
            var sid = $(this).attr("id");
            listingGRN(sid);
            $("#listing_rn_div #rn_no").html("(# " + sid + ")");
            $("#listing_rn").hide();
            $("#ViewGRNFullCloseSpan").show();

            $("#UpdateCollectionDate_Div").hide();

            showRNdetails(sid);
            showRNexportbtn(sid);
            //$('#UpdateCollectionDate_Btn').prop('disabled', true);

            return false;
        });

        $("body").delegate("#ViewGRNFullClose", "click", function () {
        //$("#ViewGRNFullClose").click(function () {
            
            viewSingle='';
            
            $("#listing_rn").show();
            $("#listing_rn_div").hide();
            $("#ViewGRNFullCloseSpan").hide();
            showUpdateCollection();
            return false;
        });

        $("body").delegate(".ViewNotebuyer", "click", function () {
        //$(".ViewNotebuyer").click(function () {
            var sid = $(this).attr("id");
            $("#myNotebuyerModal #rn_no_span").html("" + sid + "");
            $("#myNotebuyerModal #rn_no").val(sid);
            $('#myNotebuyerModal #seeremarks').html($('#RemarkShow_' + sid).html());
            $('#myNotebuyerModal #buyernotes').val($('#Note_' + sid).val());
            $('#myNotebuyerModal #buyer_id').val($('#Buyerid_' + sid).val());
            $('#myNotebuyerModal #created_by').val($('#Createdby_' + sid).val());
            $("#myNotebuyerModal #loc_id").val($('#Locid_' + sid).val());
            $("#submit_response").html("Your message will be saved and sent to the Buyer.");
        });

        $("body").delegate("#myNotebuyerSender", "click", function () {
        //$("#myNotebuyerSender").click(function () {
            $('#myNotebuyerSender').prop('disabled', true);
            
            var sid = $('#myNotebuyerModal #rn_no').val();
            var Txt = $('#myNotebuyerModal #buyernotes').val();

            var data = new FormData();
            data.append('buyernotes', Txt);
            data.append('rn_no', sid);
            data.append('buyer_id', $('#myNotebuyerModal #buyer_id').val());
            data.append('loc_id', $('#myNotebuyerModal #loc_id').val());
            data.append('created_by', $('#myNotebuyerModal #created_by').val());

            $.ajax({
                type: "POST",
                //contentType: "application/json; charset=utf-8",
                url: web_url + "rn/buyernotes",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (msg) {
                    $("#NoteShow_"+sid).html('<i>Notes:</i> ' + Txt );
                    $("#submit_response").html(msg);
                    $('#myNotebuyerSender').prop('disabled', false);
                }
            });
        }); 

    }

    if ($("#Po2_Reprint").length) {
        // New PO Confirmation & Reprint
        
        var cbChecked = 0;
        $("#sel_all_btn").click(function () {
            cbChecked = (cbChecked == 1) ? 0 : 1;
            $('input:checkbox.cb').each(function () {
                if (cbChecked == 1) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
        });
        
        function listingPoitems(sid,state){ 
            $("#datatable_div").html(LoadingCircle);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: web_url + "po2/items/" + sid + "/"+ state,
                success: function (msg) {
                    $("#datatable_div").html(msg);
                    //FormatDatatable('#listing_grn_div #datatable'); 
                    $("#listing_porpt_items #datatable").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "pageLength": 50,
                        "lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
                        "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
                    });
                }
            });
        }

        $("body").delegate(".ViewPoItems", "click", function () {
        //$(".ViewGRNFull").click(function () {
            
            var sid = $(this).attr("id");
            listingPoitems(sid,'reprint');
            
            $("#listing_porpt").hide();
            $("#listing_porpt_items").show();
            $("#ViewPoItemsCloseSpan").show();
            
            $(".content-header h1").text("Reprint PO - " + sid);
            return false;
        });

        $("body").delegate("#ViewPoItemsClose", "click", function () { 
            
            $("#listing_porpt").show();
            $("#listing_porpt_items").hide();
            $("#ViewPoItemsCloseSpan").hide(); 
            
            $(".content-header h1").text("Reprint PO");
            return false;
        });

        $("body").delegate(".InvbtnSpanDiv_Export", "click", function () {
            var ExportType = $(this).data("type");
            var Checked='';
            $('input:checkbox[name=po_checkbox]:checked').each(function () {
                Checked += $(this).attr("id2") +',';
            });
            if (Checked=='') {
                alert('Please select at least 1 PO.');
            }
            else {
                var url = web_url + 'po2/export/'+ExportType+'/'+Checked;
                window.open(url, 'PopUp', 'window settings');
            } 
            return false;
        }); 
        
        $("#ViewPoItemsCloseSpan").hide(); 
        
        $("#listing_porpt").show();
        $("#listing_porpt_items").hide();
        
        // PO Confirmation 
        
        function showUpdateDelvdate(){
            var cbOpen = 0;
            var selid = '';
            $('input:checkbox.cb:checked').each(function () {
                cbOpen++;
                selid += $(this).attr("id").substring(3);
            });
            
            if (cbOpen == 1) {  
                $('#CfmPoItems_Dlvrdate_field').val( $("#cbdate_"+selid).text() )
            } else {
                $('#CfmPoItems_Dlvrdate_field').val('')
            }
        }

        $("body").delegate(".cb", "click", function () {
        //$(".ViewGRNFull").click(function () {
            showUpdateDelvdate(); 
        });
        $("body").delegate(".cbdate", "click", function () {
        //$(".ViewGRNFull").click(function () {
            var cid = $(this).attr("id").substring(7);
            if ($("#cb_"+cid).prop('checked')==true)
                $("#cb_"+cid).prop('checked', false);
            else
                $("#cb_"+cid).prop('checked', true);
            showUpdateDelvdate();
            return false;
        });

        $("body").delegate("#CfmPoItems_Dlvrdate", "click", function () { 
            var trans_id_cnt = 0;
            var date = $('#CfmPoItems_Dlvrdate_field').val();
            var data = new FormData();
            data.append('new_codate', date);
            $('input:checkbox.cb:checked').each(function () {
                data.append('new_trans_id[]', $(this).attr('id2'));
                trans_id_cnt++;
            });

            if ( date == "" ) {
                alert("Please select delivery date"); 
                return false;
            }
            if ( trans_id_cnt == 0) {
                alert("Please select at least one item"); 
                return false;
            }

            $.ajax({
                type: "POST",
                //contentType: "application/json; charset=utf-8",
                url: web_url + "po2/updatedate",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (msg) { 
                    $('input:checkbox.cb:checked').each(function () {
                        var k = $(this).attr("id").substr(3);
                        $('#cbdate_' + k).html(date);
                        $('#cbitembtn_' + k).data('dlvrdate',date);
                        $(this).prop('checked', false);
                    });

                    $('#CfmPoItems_Dlvrdate_field').val(''); 
                }
            });
            return false;
        });

        $("body").delegate(".CfmPoItems", "click", function () {
        //$(".ViewGRNFull").click(function () {
            
            var sid = $(this).attr("id2");
            listingPoitems(sid,'confirmation');
            
            $("#CfmPoItems_Dlvrdate_field").val( $(this).data("dlvrdate") );
            $("#CfmPoItems_Dlvrdate").hide();
            // Tick only selected PO (for print/export fx)
            var k = $(this).attr("id").substring(10);
            $(".cb").prop('checked', false);
            $("#cb_"+k).prop('checked', true);
            
            
            $("#listing_porpt").hide();
            $("#listing_porpt_items").show();
            $("#CfmPoItemsCloseSpan").show();
            
            $(".content-header h1").text("PO Confirmation - " + sid);
            return false;
        });

        $("body").delegate(".po_item_newqty", "change", function () {
        //$(".ViewGRNFull").click(function () {
        
            var tq = parseFloat($(this).val())
            if (!(tq <= parseFloat($(this).attr("max")) && tq >= parseFloat($(this).attr("min")) )) {
                $(this).val( $(this).attr("max") );
                alert("New Qty cannot be higher than Qty Order");
            }
            
            var tally_amt = 0;
            $('.po_item_newqty').each(function () {
                var pq = parseFloat($(this).val());
                var pu = parseFloat($(this).data('unitprice')); 
                tally_amt += (pq * pu);  
            });            
            $("#po_item_newqty_total").html('<b>$' + tally_amt.toFixed(2) + '</b>'); 
            return false;
        });

        $("body").delegate("#CfmPoItemsSubmit", "click", function () { 
            
            $("#CfmPoItemsSubmit").attr('disabled','true');
            
            var date = $('#CfmPoItems_Dlvrdate_field').val();
            var data = new FormData();
            data.append('new_codate', date);
            $('input.po_item_oldqty').each(function () {
                data.append($(this).attr('name'), $(this).val()); 
            });
            $('input.po_item_newqty').each(function () {
                data.append($(this).attr('name'), $(this).val()); 
            });

            $.ajax({
                type: "POST",
                //contentType: "application/json; charset=utf-8",
                url: web_url + "po2/confirmpo",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (msg) {
                    
                    $("#CfmPoItemsSubmit").removeAttr('disabled');
                    
                    var jsonreply = JSON.parse(msg);
                    if (jsonreply.return==0)
                        alert(jsonreply.message)
                    else {
                        
                        $("#datatable_tr_" + jsonreply.po_id).remove();

                        $("#CfmPoItems_Dlvrdate_field").val('');
                        $("#CfmPoItems_Dlvrdate").show();
                        $('input:checkbox.cb').each(function () {
                            $(this).prop('checked', false);
                        });
                        
                        $("#listing_porpt").show();
                        $("#listing_porpt_items").hide();
                        $("#CfmPoItemsCloseSpan").hide(); 
                        
                        $(".content-header h1").text("PO Confirmation");

                    }
                }
            });
            return false; 
        });

        $("body").delegate("#CfmPoItemsClose", "click", function () { 
            
            $("#CfmPoItems_Dlvrdate_field").val('');
            $("#CfmPoItems_Dlvrdate").show();
            $('input:checkbox.cb').each(function () {
                $(this).prop('checked', false);
            });
            
            $("#listing_porpt").show();
            $("#listing_porpt_items").hide();
            $("#CfmPoItemsCloseSpan").hide(); 
            
            $(".content-header h1").text("PO Confirmation");
            return false;
        });
        
        $("#CfmPoItemsCloseSpan").hide();

        // PR

        $("body").delegate(".InvbtnSpanDiv_PR_Export", "click", function () {
            var ExportType = $(this).data("type");
            var Checked='';
            $('input:checkbox[name=po_checkbox]:checked').each(function () {
                Checked += $(this).attr("id2") +',';
            });
            if (Checked=='') {
                alert('Please select at least 1 PR.');
            }
            else {
                var url = web_url + 'po2/pr_export/'+ExportType+'/'+Checked;
                window.open(url, 'PopUp', 'window settings');
            }
            return false;
        });

    }
    
    if ($("#Inventory_DIV").length) {

        function listingInvItems(snid) { 
            $("#datatable_div").html(LoadingCircle);
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: web_url + "inventory/items/" + snid,
                success: function (msg) {
                    $("#datatable_div").html(msg);
                    //FormatDatatable('#listing_grn_div #datatable'); 
                    $("#inv_items #datatable").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "pageLength": 50,
                        "lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
                        "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
                    });
                }
            });
        }

        $("#inv_items").hide();
        $("#inv_items_closer").hide();

        $("body").delegate(".ViewItems", "click", function () {         
            var sid = $(this).attr("id"); 
            listingInvItems(sid);
            
            $("#inv_items #sn_no").html("(Sn # EIL-" + sid + ")");
            $("#inv_items").show();
            $("#inv_items_closer").show();
            $("#inv_list").hide();
            $("#inv_upload").hide();
            return false;
        });

        $("body").delegate("#ViewItemsClose", "click", function () {
        //$("#ViewItemsClose").click(function () {
            $("#inv_items").hide();
            $("#inv_items_closer").hide();
            $("#inv_list").show();
            $("#inv_upload").show();
            return false;
        });

        // Inventory quick update Fx
        $("#upload_csv_btn").hide();
        $("#upload_csv").click(function () {
            $("#upload_csv_btn").show();
        });
        
        var invqty_chg=0;
        $("#update_inv_btn").prop('disabled',true);
        $("body").delegate(".inv_qty", "change", function () {
        //$(".inv_qty").change(function () {
            invqty_chg=1;
            $("#update_inv_btn").prop('disabled',false);
        });
        
        $("#update_inv_btn").click(function () {
            if (invqty_chg==0){
                alert("There is no change in Qty of the Inventory Listing.");
                return false;
            }
            else {
                $( "form#update_inv_form" ).submit(); 
            }
        }); 
        
        // ADD NEW ITEM
        $("#new_inv_btn").click(function () {
            $("#modal_newinventory").modal('show');
            
            $.ajax({
                type: "GET",
                url: web_url + "inventory/newitem",
                success: function (msg) { 
                    $("#modal_newinventory .modal-body").html(msg);
                }
            });
            
        });
        
        $("#modal_newinventory_add").click(function () {
            $("form#newinventory_form").submit();             
        });
        
    }
    
    if ($("#po_items").length) {
        
        function item_total_price(updfield) {
            var tally_amt = 0;
            $('.po_item_newqty').each(function () {
                var pq = parseFloat($(this).val());
                var pu = parseFloat($(this).data('unitprice')); 
                tally_amt += (pq * pu);  
            });
            if (updfield=='old')
                $("#po_item_total_old").html('<b>$' + tally_amt.toFixed(2) + '</b>');
            
            $("#po_item_total_new").html('<b>$' + tally_amt.toFixed(2) + '</b>');
            return tally_amt;
        }

        $('.po_item_newqty').change(function () {
            item_total_price('new'); 
        });
        item_total_price('old');
        
    }
    
    if ($("#po_listing").length) {

        if ($("#po_listing").length) {
            var cbChecked = 0;
            $("#sel_all_btn").click(function () {
                cbChecked = (cbChecked == 1) ? 0 : 1;
                $('input:checkbox.cb').each(function () {
                    if (cbChecked == 1) {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);
                    }
                });
            });
        }

        $("#apply_btn").click(function () {
            var sel = $('input:checkbox[name=po_checkbox]:checked').map(function (_, el) {
                return $(el).val();
            }).get();
            var delv_date = $("#delv_date").val();
            $("#data").val(sel);
            $("#action").val('applydelv');
            if (delv_date == "") {
                alert("Please select delivery date");
                return false;
            } else if (sel == "") {
                alert("Please select at least one PO");
                return false;
            } else {
                $("#po_deli_apply").submit();
            }
        });


        $("#print_btn").click(function () {
            var sel = $('input:checkbox[name=po_checkbox]:checked').map(function (_, el) {
                return $(el).val();
            }).get();
            $("#data").val(sel);
            if (sel == "") {
                alert("Please select at least one PO");
                return false;
            } else {
                
                var sel = $('input:checkbox[name=po_checkbox]:checked').map(function (_, el) {
                    var s0 = $(el).val();
                    var s1 = s0.split("---");
                    return s1[1];
                }).get();
                
                var url = web_url + 'po/po_pdf/'+sel;
                window.open(url, 'PopUp', 'window settings');
                return false;
            }
        });

        $("#email_btn").click(function () {
            var sel = $('input:checkbox[name=po_checkbox]:checked').map(function (_, el) {
                return $(el).val();
            }).get();
            $("#data").val(sel);
            $("#action").val('email');
            if (sel == "") {
                alert("Please select at least one PO");
                return false;
            } else {
                $("#po_deli_apply").submit();
            }
        });

//        $("#qty_submit").click(function () {
//            if (confirm("Do you want confirm?")) {
//                $("#po_qty_apply").submit();
//            } else {
//                return false;
//            }
//        });

    }

});