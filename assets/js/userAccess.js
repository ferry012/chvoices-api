
$(document).ready(function(){ 
    //by default hide those forms
    $('#showForm').hide();
    $('#revokeAccess').hide();
    $('#assignAccess').hide();
    // $('#errMsg').hide();

    $('#btnChangeLoc').click(function(){
        $('#newAccess').slideToggle(200);
        $('#newStaff').slideToggle(200);
        $('#resignStaff').slideToggle(200);
        $('#resetPwd').slideToggle(200);
        $('#showForm').slideToggle(200);
        // $('#errMsg').slideToggle(200);
    });

    $('#btnNewStaff').click(function(){
        $('#changeLoc').slideToggle(200);
        $('#newAccess').slideToggle(200);
        $('#resignStaff').slideToggle(200);
        $('#resetPwd').slideToggle(200);
        $('#showForm').slideToggle(200);
        // $('#errMsg').slideToggle(200);
    });    

    $('#btnResignStaff').click(function(){
        $('#changeLoc').slideToggle(200);
        $('#newAccess').slideToggle(200);
        $('#newStaff').slideToggle(200);
        $('#resetPwd').slideToggle(200);
        $('#showForm').slideToggle(200);
        // $('#errMsg').slideToggle(200);
    });   
    
    $('#btnResetPwd').click(function(){
        $('#changeLoc').slideToggle(200);
        $('#newAccess').slideToggle(200);
        $('#newStaff').slideToggle(200);
        $('#resignStaff').slideToggle(200);
        $('#showForm').slideToggle(200);
        // $('#errMsg').slideToggle(200);
        
    });  

    $('#btnNewAccess').click(function(){
        $('#changeLoc').slideToggle(200);
        $('#resetPwd').slideToggle(200);
        $('#newStaff').slideToggle(200);
        $('#resignStaff').slideToggle(200);
        $('#showForm').slideToggle(200);
        // $('#errMsg').slideToggle(200);
    });  

    $('#btnRevokeAccess').click(function(){
        $('#revokeAccess').slideToggle(200);
        $('#assignAccess').hide(200);
    });

    $('#btnAssignAccess').click(function(){
        $('#assignAccess').slideToggle(200);
        $('#revokeAccess').hide(200);
    });
});

function checkUser($type){
    var security_level = document.getElementById("security_level").innerText;
    console.log("security_level: ", security_level);
    let form = "<div id='searchStaff'> " +
    "<!-- <form method='post' action='<?=base_url();?>'> -->"+
    "<form method='post' action='/cherps/searchStaff'>"+
        "<div><input id='usr_id' type = 'text' name = 'usr_id' placeholder='enter staff 4D' required /><br/><br/>"+
        "<input id='type' name='type' type='text' style=display:none value='"+$type+"'/>"+
        "<input id='btnSubmit' type = 'submit' value = 'Validate'></div>"+
    "</form></div>";

    let resetForm = document.getElementById("showForm").innerHTML = "<div id='resetPwd'><br/>"+
    "<!-- <form method='post' action='<?=base_url();?>'> -->"+
    "<form method='post' action='/cherps/setNewPwd'>"+
    "<label>Please enter your challenger email <br/>Phone number for verification:</label><br/>"+
    "<input id='email_verification' name='email_verification' type='email' placeholder='Email Address' required/><br/>"+
    "<input id='sms' name='sms' type=number placeholder='91234567' required /><br/><br/>"+
    "<input id='btnSubmit' name='btnSubmit' type='submit' value='Validate' /></form></div>";

    $warningMsg = "<div id='warningMsg' style=color:red;>"+
    "You are not allowed to perform this action.<br/>"+
    "Please contact your IC for this request.</div>";

    if(security_level == 'Supervisor' || security_level == 'Staff/Cashier'){
        if($type == 'P'){ // p = password
            document.getElementById('showForm').innerHTML = resetForm;
        }else{
            document.getElementById('errMsg').innerHTML = $warningMsg;
        }
    }else if($type != 'P'){
        document.getElementById('showForm').innerHTML = form;
    }else{
            document.getElementById('showForm').innerHTML = resetForm;
    }
}