<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_pagination')) {

    function get_pagination($total_results, $limit, $page) {
        // Check for valid and limit
        $page = $page < 1 ? 1 : $page;
        $limit = $limit < 1 ? 1 : $limit;

        $total_pages = ceil($total_results / $limit);
        $page = $page > $total_pages ? $total_pages : $page;
        $offset = (($page * $limit) - $limit);

        return array(
            'limit' => $limit,
            'offset' => $offset < 1 ? 0 : $offset,
            'page' => $page,
        );
    }

}

if (!function_exists('is_valid_date')) {

    function is_valid_date($date) {
        // yyyy-mm-dd 
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $date, $matches)) {
            if (checkdate($matches[2], $matches[3], $matches[1]))
                return TRUE;
        }
    }

}

if (!function_exists('doCurl')) {

    function doCurl($url, $param = NULL, $method = NULL) {
        // Pass the access Token - Prasanna
        $CI = &get_instance();
        $CI->load->library('session');
        $user_session_data = $CI->session->userdata('admin_logged_in');
        $access_token = $user_session_data['access_token'];
        // Admin Login Venue ID
        $pattern_login = "/login/";

        $count_url_login_result = preg_match($pattern_login, $url, $matches_login);

        if ($count_url_login_result == 0) {
            $admin_venue_id = $CI->session->userdata('admin_venue_id');
            if (isset($admin_venue_id) && $admin_venue_id != '') {
                if (is_array($param) && !empty($param)) {
                    $param['login_venue_id'] = $admin_venue_id;
                } else {
                    $param = array('login_venue_id' => $admin_venue_id);
                }
            }
            if ($method != 'POST') {
                $params['login_venue_id'] = $admin_venue_id;
                $pattern = '/\?/';
                $count_url_result = preg_match($pattern, $url, $matches);
                if ($count_url_result > 0) {
                    $url = $url . '&' . http_build_query($params);
                } else {
                    $url = $url . '?' . http_build_query($params);
                }
                //echo $url;exit;
            }
        }

        log_message('debug', 'SNB-curl: Call to ' . $url . ($method == 'POST' ? ' with ' . preg_replace('/\n/', '', print_r($param, TRUE)) : ''));

        $process = curl_init($url);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        if ($method == 'POST') {
            curl_setopt($process, CURLOPT_POST, TRUE);
            curl_setopt($process, CURLOPT_POSTFIELDS, $param);
            curl_setopt($process, CURLOPT_CUSTOMREQUEST, $method);
        }
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        if (trim($access_token) != "") {
            $headerArray = array(
                'X-authorization: ' . $access_token,
                'Cache-Control: no-cache',
            );
            curl_setopt($process, CURLOPT_HTTPHEADER, $headerArray);
        }

        $content = array();
        $content['output'] = curl_exec($process);
        $content['http_status_code'] = curl_getinfo($process, CURLINFO_HTTP_CODE);

        return $content;
    }

}

if (!function_exists('active_link')) {

    function activate_menu($controller) {
        // Getting CI class instance.
        $CI = get_instance();
        // Getting router class to active.
        $class = $CI->router->fetch_class();

        return ($class == $controller) ? 'active' : '';
    }

}

if (!function_exists('limit_word')) {

    function limit_word($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }

        return $text;
    }

}


if (!function_exists('limit_char')) {

    function limit_char($string, $limit, $sep) {
        $text = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $limit));
        $result_text = $text . $sep;

        return $result_text;
    }

}

if (!function_exists('limit_string')) {

    function limit_string($string, $limit, $sep) {
        $text = substr($string, 0, $limit);
        $result_text = $text . $sep;

        return $result_text;
    }

}

function array_double_single_quotes($input) {
    if (!is_array($input)) {
        $input = (array) $input;
    }

    $input = array_map(function ($v) {
        $v = trim($v);

        return "''{$v}''";
    }, $input);

    return $input;
}

function debug() {
    echo '<pre>';
    print_r(func_get_args());
}

if (!function_exists('is_empty')) {

    function is_empty($value, $default_value = '') {
        if (isset($value) && !empty($value)) {
            return $value;
        }

        return $default_value;
    }

}

if (!function_exists('get_option')) {

    function get_option($process, $apply_on, $id) {
        $CI = & get_instance();

        $sql = "SELECT constant_id, constant_desc, datatype, intvalue, ascvalue
                FROM o2o_constants
                WHERE is_active = 'Y' AND constant_id = ? AND process = ? AND apply_to = ?";

        $result = $CI->db->query($sql, [$id, $process, $apply_on])->row_array();

        if ($result) {
            switch ($result['datatype']) {
                case 'I':
                    $result['value'] = (int) $result['intvalue'];
                    break;
                case 'C':
                    $result['value'] = $result['ascvalue'];
                    break;
            }

            unset($result['datatype']);
            unset($result['intvalue']);
            unset($result['ascvalue']);
        }

        return $result;
    }

}

/*
 * Record error log
 */
if (!function_exists('record_to_log')) {

    function record_to_log($processes, $remarks, $mbr_id = '') {
        $CI = & get_instance();

        $CI->db->set('coy_id', HACHI_COY_ID);
        $CI->db->set('process_id', $processes);
        $CI->db->set('status', 'XOK');
        $CI->db->set('remarks', $remarks);
        $CI->db->set('mbr_id', $mbr_id);
        $CI->db->set('created_on', 'getdate()', FALSE);
        $CI->db->set('created_by', 'Sys');
        $CI->db->set('modified_on', 'getdate()', FALSE);
        $CI->db->set('modified_by', 'Sys');

        $CI->db->insert('o2o_log_live_processes');
    }

}

function get_mssql_date() {
    return date('Y-d-m H:i:s');
}

function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
    return base64_decode(strtr($data, '-_', '+/'));
}

function encrypt_one_time_token($key, $time_seconds = 30) {
    $CI = &get_instance();
    $CI->load->library('encryption');

    $encrypt_key = bin2hex($CI->encryption->create_key(16));
    $CI->encryption->initialize(['key' => $encrypt_key]);

    $CI->session->set_tempdata($key, $encrypt_key, $time_seconds);

    $timestamp = time();
    $one_time_token = $CI->encryption->encrypt($timestamp);

    return base64url_encode($one_time_token);
}

function decrypt_one_time_token($key, $encrypted_token) {
    $CI = &get_instance();
    $CI->load->library('encryption');

    $decrypt_key = $CI->session->tempdata($key);
    $CI->encryption->initialize(['key' => $decrypt_key]);

    $one_time_token = $CI->encryption->decrypt(base64url_decode($encrypted_token));

    return $one_time_token !== FALSE;
}

function objectToArray($object) {
    if (!is_object($object) && !is_array($object)) {
        return $object;
    }
    if (is_object($object)) {
        $object = get_object_vars($object);
    }

    return array_map('objectToArray', $object);
}

function getTransactionJs(&$trans) {

    $trans['shipping'] = $trans['free_shipping'] == 0 ? $trans['total_delivery_fee'] : 0;

    return <<<HTML
ga('ecommerce:addTransaction', {
  'id': '{$trans['trans_id']}',
  'revenue': '{$trans['total_net_amt']}',
  'shipping': '{$trans['shipping']}'
});
HTML;
}

// Function to return the JavaScript representation of an ItemData object.
function getTransactionItemsJs(&$transId, &$item) {

    $item['item_desc'] = addslashes($item['item_desc']);
    $item['final_price'] = floatval($item['final_price']);
    $item['qty'] = floor($item['qty']);

    return <<<HTML
ga('ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['item_desc']}',
  'sku': '{$item['item_id']}',
  'price': '{$item['final_price']}',
  'quantity': '{$item['qty']}'
});
HTML;
} 


/**
 * Create a Random String
 *
 * Useful for generating passwords or hashes.
 *
 * @access	public
 * @param	string	type of random string.  basic, alpha, alunum, numeric, nozero, unique, md5, encrypt and sha1
 * @param	integer	number of characters
 * @return	string
 */
if ( ! function_exists('random_string'))
{
	function random_string($type = 'alnum', $len = 8)
	{
		switch($type)
		{
			case 'basic'	: return mt_rand();
				break;
			case 'alnum'	:
			case 'upperalnum'	:
			case 'loweralnum'	:
			case 'numeric'	:
			case 'nozero'	:
			case 'alpha'	:

					switch ($type)
					{
						case 'alpha'	:	$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
							break;
						case 'alnum'	:	$pool = '23456789ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz';
							break;
						case 'upperalnum':	$pool = '23456789ABCDEFGHJKMNPQRSTUVWXYZ';
							break;
						case 'loweralnum':	$pool = '23456789abcdefghjkmnopqrstuvwxyz';
							break;
						case 'numeric'	:	$pool = '0123456789';
							break;
						case 'nozero'	:	$pool = '123456789';
							break;
					}

					$str = '';
					for ($i=0; $i < $len; $i++)
					{
						$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
					}
					return $str;
				break;
			case 'unique'	:
			case 'md5'		:

						return md5(uniqid(mt_rand()));
				break;
			case 'encrypt'	:
			case 'sha1'	:

						$CI =& get_instance();
						$CI->load->helper('security');

						return do_hash(uniqid(mt_rand(), TRUE), 'sha1');
				break;
		}
	}
}

if ( ! function_exists('trim_all'))
{
    function trim_all( $str ){
        $what = "\\x00-\\x20";
        return trim( preg_replace( "/[".$what."]+/" , ' ' , $str ) , $what );
    }
}

if ( ! function_exists('implode_email'))
{
    function implode_email( $emails_input ){
        $emails_set = array();
        foreach ($emails_input as $input) {
            //if (filter_var($input, FILTER_VALIDATE_EMAIL) && !in_array($input,$emails_set)) {
            if ( !in_array($input,$emails_set) ) {
                $emails_set[] = $input;
            }
        }
        return implode(';',$emails_set);
    }
}

function getRealIpAddr($chars=45)
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return substr($ip,0,$chars);
}

function challengerIpAddr(){
    $ip = getRealIpAddr(15);
    $whitelist = array(
        '52.76.179.193', '52.77.103.117', '52.221.100.194', // CHERPS01, CHERPS02, CHERPS91
        '13.250.64.186', // HACHI2-DEV
        '52.76.8.27', // HACHI2-PROD
        '52.76.6.141', // HACHI-DEV-WEB (HY-DEV)
        '52.74.207.7', // HY-PROD
        '13.229.185.150' // WEBCTL-PROD
    );
    if ( substr($ip,0,4)=="10.0" || substr($ip,0,7)=="192.168" || substr($ip,0,11)=="203.127.118" || in_array($ip,$whitelist) ) {
        return true;
    }
    else {
        return false;
    }
}

function isBase64($string) {
    if (base64_decode($string, true) === false) {
        return false;
    }
    else {
        if (base64_encode(base64_decode($string))==$string) {
            return true;
        }
        else {
            return false;
        }
    }
}

function mssql_escape($data) {
    if(is_numeric($data))
        return $data;
    $unpacked = unpack('H*hex', $data);
    return '0x' . $unpacked['hex'];
}
