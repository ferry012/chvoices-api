<?php
/**
 * hachi-web
 *
 * @package    hachi-web
 * @author     GUO ZHIMING
 * @filesource MY_url_helper.php
 */
function product_url($id, $name) {

    $name = empty($name) ? 'missing name' : $name;
    $slug = url_title($name, '-', TRUE);
    $pathname = sprintf('product/%s/%s', trim($id), $slug);
    $url = base_url($pathname);

    return $url;
}

function base_url2($uri = '', $protocol = NULL) {
    $new_url = get_instance()->config->base_url($uri, $protocol);
    //$new_url = "http://ebsazve.cloudimg.io/s/cdn/x/" . $new_url; //pgd: comment out till cdn is ready
    return $new_url;
}
