<?php

function title($title = '', $prefix = '', $separator = '|')
{

    if (!empty($title) && !empty($prefix)) {
        $title = $prefix . ' ' . $separator . ' ' . $title;
    }

    echo "<title>{$title}</title>";
}