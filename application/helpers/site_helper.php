<?php

if (!function_exists('filterstring')) {

    function filterstring($string) {
        $output = $string;
        $output = str_replace("'", "", $output);
        $output = str_replace(";", "", $output);
        $output = str_replace("*", "", $output);
        $output = str_ireplace("insert", "", $output);
        $output = str_ireplace("update", "", $output);
        $output = str_ireplace("delete", "", $output);
        $output = str_ireplace("select", "", $output);
        $output = str_ireplace("wait", "", $output);
        $output = str_replace("truncate", "", $output);
        $output = str_replace("--", "", $output);
        $output = str_replace("=", "", $output);

        return $output;
    }

}
if (!function_exists('validate_phone')) {

    function validate_phone($phone) {
        if (strlen($phone) == 8 && is_numeric($phone)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

if (!function_exists('validate_postal_code')) {

    function validate_postal_code($postal_code) {
        if (strlen($postal_code) == 6 && is_numeric($postal_code)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

if (!function_exists('validate_quantity')) {

    function validate_quantity($qty) {
        if (strlen($qty) != 0 && is_numeric($qty)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

if (!function_exists('validate_price')) {

    function validate_price($price) {
        if (strlen($price) != 0 && is_numeric($price)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

if (!function_exists('GUID')) {

    function GUID() {
        if (function_exists('com_create_guid') === TRUE) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

}


if (!function_exists('current_url_query_string')) {

    function current_url_query_string($add = [], $keep = [], $remove = []) {
        $CI = &get_instance();
        $url = $CI->config->site_url($CI->uri->uri_string());

        $query_string = $_SERVER['QUERY_STRING'];

        if ($add || $keep || $remove) {

            parse_str($_SERVER['QUERY_STRING'], $query_string);

            if (!empty($remove) && is_array($remove)) {
                $query_string = array_diff_key($query_string, array_flip($remove));
            }

            if (!empty($keep) && is_array($keep)) {
                $query_string = array_intersect_key($query_string, array_flip($keep));
            }

            if (!empty($add) && is_array($add)) {
                $query_string = array_merge($query_string, $add);
            }

            $query_string = http_build_query($query_string);
        }

        return $query_string ? $url . '?' . $query_string : $url;
    }

}

if (!function_exists('convert_to_mysql_time')) {

    function convert_to_mysql_time($timestamp) {
        if (isset($timestamp) && $timestamp != '') {
            $timestamp = DateTime::createFromFormat('d-m-Y', $timestamp);
            if ($timestamp != '')
                return $timestamp->format('Y-m-d');
            else
                return '';
        }
        return '';
    }

}
?>