<?php

if (!function_exists('display_number')) {

    function display_number($price) {
        return sprintf('%01.0f', $price);
    }

}

if (!function_exists('price_no_dec')) {

    function price_no_dec($price) {
        return sprintf('$%01.0f', $price);
    }

}

if (!function_exists('price_no_symbol')) {

    function price_no_symbol($price) {
        //return sprintf('%01.2f', $price);
        //return number_format($price, '2', '.', ',');
        if ($price != "") {
            return number_format($price, '2', '.', ',');
        } else {
            return "---";
        }
        
    }

}

if (!function_exists('price_two_dec')) {

    function price_two_dec($price) {
        return ("S$") . number_format($price, '2', '.', ',');
    }

}

if (!function_exists('display_date_format')) {

    function display_date_format($date) {
        $date = str_replace('/', '-', $date);
        if ($date != "") {
            return date("d-m-Y", strtotime($date));
        } else {
            return "---";
        }
    }

}

if (!function_exists('display_time_format')) {

    function display_time_format($date) {
        return date('g:i a', strtotime($date));
    }

}

if (!function_exists('display_date_time_format')) {

    function display_date_time_format($date) {
        return date('d M Y, g:i a', strtotime($date));
    }

}

if (!function_exists('validateDate')) {

    function validateDate($myDateString) {
        if ((bool) strtotime($myDateString)) {
            return strtotime($myDateString);
        } else {
            list($d, $m, $y) = explode("/", $myDateString);
            if (checkdate($m, $d, $y)) {
                return mktime(0, 0, 0, $m, $d, $y);
            } else {
                return false;
            }
        }
    }

}

if (!function_exists('cleanString')) {

    function cleanString($str) {
        $rtn = preg_replace('/\s+/', '', $str);
        //$rtn = str_replace('/','',$rtn);
        return $rtn;
    }

}

if (!function_exists('get_encrypt_key')) {

    function get_encrypt_key($param = '') {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $key = "a16byteslongkeyaa16rajaslongkeya";

        $plaintext = date('H:i:s');
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext, MCRYPT_MODE_ECB, $iv);
        $ciphertext = base64_encode($ciphertext);

        return $ciphertext;
    }

}

if (!function_exists('get_page_limit')) {

    function get_page_limit() {
        return array("20", "40", "60", "80", "100");
    }

}


if (!function_exists('current_full_url')) {

    function current_full_url() {
        $CI = & get_instance();
        $url = $CI->config->site_url($CI->uri->uri_string());
        return $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;
    }

}



if (!function_exists('debug_mode')) {

    function debug_mode($content) {
        if (isset($_GET["debug"]) && $_GET["debug"]=="1"){
            echo "<pre>"; print_r($content); exit;
        }
    }

}



?>