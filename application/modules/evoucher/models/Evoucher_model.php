<?php

!defined('BASEPATH') OR ( 'No direct script access allowed');

class Evoucher_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function crm_member_list($params) {
        $ic = empty($params['ic']) ? "" : "or mbr_id = '" . $params['ic'] ."'";
        $sql = "SELECT mbr_id
                FROM crm_member_list 
                WHERE email_addr = '" . $params['emailaddr']  ."' 
                    or contact_num = '" . $params['contactnum']  ."'
                    $ic";
        $result = CI::db()->query($sql)->result_array();
        if (count($result) > 0) {
            return $result;
        }

        return FALSE;
    }

    public function crm_voucher_list($username)
    {
        $sql = "SELECT *
                FROM crm_voucher_list 
                WHERE mbr_id = '" . $username  ."' 
                    and coupon_id = '!VCH-VCTP30'";
        $result = CI::db()->query($sql)->result_array();
        if (count($result) > 0) {
            return $result;
        }

        return FALSE;
    }

    public function add_voucher($mbr_id, $supp_id, $coupon, $promocart, $vctp, $amount)
    {
        $serial_sql = "EXEC [sp_SysGetNextDocId] 'CTL', 'POS', 'VOUCHER', " . $vctp . ", '2019-02-27'";
        $serialno = CI::db()->query($serial_sql)->result_array();
        $sql = "INSERT INTO crm_voucher_list
	      (coy_id, coupon_id, coupon_serialno, promocart_id, mbr_id, trans_date, status_level, issue_date, sale_date, utilize_date, [expiry_date], voucher_amount, receipt_id1, created_by, created_on, modified_by, modified_on, [guid])
          VALUES('CTL', ?, ?, ?, ?, getdate(), 0, getdate(), getdate(), CONVERT(varchar, '1900-01-01', 21), CONVERT(varchar(10), DATEADD(MONTH, 3, getdate()), 120) + ' 23:59:59', ?, '', ?, getdate(), ?, getdate(), NEWID())
        ";

        $result = CI::db()->query($sql,[$coupon, $serialno[0]['doc_id'], $promocart, $mbr_id, $amount, $supp_id, $supp_id]);
        if ($result) {
            return $result;
        }
        return FALSE;
    }
    
}

?>
