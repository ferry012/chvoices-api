<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header hidden">
                    <h3 class="box-title">PO Details </h3>
                </div>
                <div class="box-body">

                    <div class="card" style="margin-top: 30px;">
                        <div class="card-block">
                            <!-- jQuery Validation (.js-validation-bootstrap class is initialized in js/pages/base_forms_validation.js) -->
                            <!-- For more examples please check https://github.com/jzaefferer/jquery-validation -->
                            <form class="js-validation-bootstrap form-horizontal"
                                  action="<?php echo base_url("evoucher/check") ?>" method="post">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="val-username">Contact Number <span
                                                class="text-orange">*</span></label>
                                    <div class="col-md-7">
                                        <input class="form-control" type="text" id="contactnum" name="contactnum"
                                               placeholder="Enter your contact number.."/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="val-email">Email <span
                                                class="text-orange">*</span></label>
                                    <div class="col-md-7">
                                        <input class="form-control" type="text" id="emailaddr" name="emailaddr"
                                               placeholder="Enter your valid email.."/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="val-password">IC Number </label>
                                    <div class="col-md-7">
                                        <input class="form-control" type="text" id="ic" name="ic"
                                               placeholder="Enter your ic number.."/>
                                    </div>
                                </div>
                                <div class="form-group m-b-0">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button class="btn btn-app" type="submit">Check</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- .card-block -->
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>