<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Evoucher extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('evoucher/evoucher_model');
        $this->load->model('login/Login_model');
        $this->load->library('session');
        $this->_checkLogged();
    }

    public function check_voucher()
    {
        $data["page_title"] = 'Issue VC Voucher';
        $data["sidebar"] = 'Promotion';
        Template::set($data);
        Template::render();
    }

    public function check()
    {
        $params["emailaddr"] = $this->input->post('emailaddr');
        $params["contactnum"] = $this->input->post('contactnum');
        $params["ic"] = $this->input->post('ic');
        $supp_id = $this->session->userdata('cm_user_id');
        $user_id = $this->session->userdata('cm_supp_id');
        if ($user_id == 'ZZ9998') {
            $coupon = '!VCH-VCTP30';
            $promocart = '!VCH-VCTP30';
            $vctp = 'VCTP30';

            if (!empty($params['emailaddr']) && !empty($params['contactnum'])) {
                $ic = $this->evoucher_model->crm_member_list($params);
                if ($ic) {
                    $result = $this->evoucher_model->crm_voucher_list($ic[0]["mbr_id"]);
                    if ($result) {
                        $data["success_message"] = 'Congratulation, the evoucher has been credited to your ValueClub account.';
                        $this->session->set_flashdata('success_message', $data["success_message"]);
                        redirect('evoucher/check_voucher');
                    } else {
                        $date_now = date("Y-m-d H:i:s");
                        if ($date_now < '2019-03-03 23:59:59') {
                            $amount = '50';
                        } else {
                            $amount = '30';
                        }
                        $add = $this->evoucher_model->add_voucher($ic[0]["mbr_id"], $supp_id, $coupon, $promocart, $vctp, $amount);
                        if ($add) {
                            $data["success_message"] = 'Congratulation, the evoucher has been credited to your ValueClub account.';
                            $this->session->set_flashdata('success_message', $data["success_message"]);
                            redirect('evoucher/check_voucher');
                        } else {
                            $data["error_message"] = "Sorry, our server busy now, please try again later.";
                            $this->session->set_flashdata('error_message', $data["error_message"]);
                            redirect('evoucher/check_voucher');
                        }
                    }
                } else {
                    $data["error_message"] = "Sorry, you are not our member.";
                    $this->session->set_flashdata('error_message', $data["error_message"]);
                    redirect('evoucher/check_voucher');
                }
            } else {
                $data["error_message"] = "Please enter your email address and contact number!";
                $this->session->set_flashdata('error_message', $data["error_message"]);
                redirect('evoucher/check_voucher');
            }
        } else {
            $data["error_message"] = "Sorry, you do not have the permission to operate";
            $this->session->set_flashdata('error_message', $data["error_message"]);
            redirect('evoucher/check_voucher');
        }

    }
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
