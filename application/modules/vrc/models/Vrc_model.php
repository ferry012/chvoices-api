<?php

!defined('BASEPATH') OR ( 'No direct script access allowed');

class Vrc_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }

    public function pdf_list($params){
        $vrcinfo = $this->pdf_info($params);

        $vrclists = array(); $last_id='';
        foreach ($vrcinfo as $info) {
            if ($last_id!=$info["trans_id"]) {
                $last_id = trim($info["trans_id"]);
            }
            $vrclists[$last_id][] = $info;
        }
        return $vrclists;
    }

    public function pdf_info($params) {
        $sql_po = implode("','",$params["ids"]);
        $sql_supp = ($params["supp_id"]=='CHERPS') ? "" : " AND l.supp_id='".$params["supp_id"]."'";
        $sql = "select	l.trans_id,l.trans_date trans_date,l.curr_id,
                        l.claim_type, (select type_desc from coy_type_list where coy_id=l.coy_id and type_code='CLAIM_TYPE' and type_data=l.claim_type) as claim_type_desc,
                        l.remarks,
                        i.line_num,i.item_id,i.item_desc,i.trans_qty,i.unit_cost,i.new_cost,(i.unit_cost-i.new_cost) as unit_rebate,((i.unit_cost-i.new_cost)*i.trans_qty) as total_rebate,
                        l.supp_id, (select supp_name from pms_supplier_list where coy_id=l.coy_id and supp_id=l.supp_id) as supp_name,
                        l.contact_person,l.tel_code,l.fax_code,l.email_addr,
                        (select usr_name from sys_usr_list where usr_id=l.issued_by) issued_by,
                        (select usr_name from sys_usr_list where usr_id=l.approved_by) approved_by,
                        (select email_addr from sys_usr_list where usr_id=l.issued_by) issued_email_addr
                from pms_claim_list l, pms_claim_item i
                where l.coy_id=i.coy_id and l.trans_id=i.trans_id
                    --and claim_type='D4'
                    and status_level >= 1
                    and l.trans_id IN ('$sql_po') $sql_supp
                    ORDER BY l.trans_id,i.line_num";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            foreach($result as $key => $value)
                $result[$key]['trans_date'] = date('d-m-Y', strtotime($value['trans_date']));
            return $result;
        }
        return FALSE;
    }

    public function get_all_claims(array $params) {
        $status_level = (isset($params["status_level"]) && $params["status_level"] != NULL) ? $params["status_level"] : "1,2";
        $claim_type = (isset($params["claim_type"]) && $params["claim_type"] != NULL) ? " AND l.claim_type='".$params["claim_type"]."' " : "";
        $sql_search = '';
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (l.trans_date LIKE '%" . $params["search"] . "%' OR l.remarks LIKE '%" . $params["search"] . "%')" : "";
        $sql_search.= (isset($params["search_to"]) && $params["search_to"] != NULL) ? " AND l.trans_date <= '" . date('Y-m-d H:i:s',strtotime($params["search_to"] . "+1 days")) . "'" : "";
        $sql_search.= (isset($params["search_from"]) && $params["search_from"] != NULL) ? " AND l.trans_date >= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_from"]))) . "'" : "";
        $sql = "select l.trans_id,l.trans_date,l.curr_id,
                        (SELECT usr_name FROM sys_usr_list where usr_id=l.issued_by LIMIT 1) as issued_by,
                        l.remarks,
                        (SELECT SUM((i.unit_cost-i.new_cost)*i.trans_qty) FROM pms_claim_item i WHERE i.trans_id=l.trans_id GROUP BY trans_id) as rebates,
		                (SELECT SUM(i.trans_qty) FROM pms_claim_item i WHERE i.trans_id=l.trans_id GROUP BY trans_id) as items,
						(select trans_id from fin_journal where coy_id=l.coy_id and left(doc_ref,15)=l.trans_id order by trans_date desc limit 1) dn_no
                FROM pms_claim_list l
                WHERE l.coy_id ='" . Base_Common_Model::COMPANY_ID . "' AND l.supp_id = '" . trim($params["supp_id"]) . "'
                AND l.status_level IN (" . $status_level . ") " . $sql_search . $claim_type;
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }
        return FALSE;
    }
}

?>
