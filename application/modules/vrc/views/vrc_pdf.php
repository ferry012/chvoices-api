<?php
ini_set('display_errors', 1);
ob_start();
tcpdf();

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        $this->SetFont('helvetica', '', 9);
        $headerData = $this->getHeaderData();
        $this->writeHTML($headerData['string']);
    }

    // Page footer
    public function Footer() {
        $this->SetY(-66);
        $this->SetFont('helvetica', '', 9);
        $footer = $this->getFooterString();
        $footer .= ''; //'<table style="padding: 5px;"><tr><td align=left></td><td align=center></td><td align="right">Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages() . '</td></tr></table>';
        
        $this->writeHTML($footer);
    }

    public $isLastPage = false;
    public function lastPage($resetmargins = false) {
        $this->setPage($this->getNumPages(), $resetmargins);
        $this->isLastPage = true;
    }

}

$obj_pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, 52, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(true, 65);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);

$obj_pdf->SetHeaderMargin(5);
$obj_pdf->setPrintHeader(true);
$obj_pdf->SetFooterMargin(20);
$obj_pdf->setPrintFooter(true);

foreach ($vrcinfos as $vrcinfo) {
    
    $CustomHeader = HEADERP($vrcinfo,$ctl_addr,$obj_pdf);
    $CustomFooter = FOOTERP($vrcinfo,$ctl_addr);
    $content = CONTENTP($vrcinfo);

    $obj_pdf->setHeaderData($ln = '', $lw = 0, $ht = '', $CustomHeader, $tc = array(0, 0, 0), $lc = array(0, 0, 0));
    $obj_pdf->setFooterData(array(0, 0, 0), array(0, 0, 0), $CustomFooter);
    $obj_pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->AddPage(); 
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    if (ceil($obj_pdf->GetY()) >= 60) {
        $obj_pdf->AddPage();
    }
    $lastPage = $obj_pdf->getPage();
    $obj_pdf->deletePage($lastPage);
}

if (isset($save_path)) {
    $obj_pdf->Output($save_path, 'F');
}
else {
    $obj_pdf->Output('output.pdf', 'I');
}


function HEADERP($info,$ctl_addr,$obj_pdf){
    $logo = 'http://chvoices.challenger.sg/assets/images/logo.png'; // base_url('assets/images/logo.png');
    return '<table style="font-size:9px;">
    <tr>
        <td style="width: 20%; text-align: left;">
            <div>
                <img style="width: 130px; height: auto;" src="' . $logo . '"/><br/>
                <span><br>Co. Reg. No.: '.$ctl_addr["coy_reg_code"].'</span> 
            </div>
        </td>
        <td style="width: 55%; text-align: left; margin-left:10px;">
            <table width="100%">
                <tr><td>&nbsp;<br><span style="font-size:10px"><b>'.$ctl_addr["coy_name"].'</b></span></td></tr>
                <tr><td>'.$ctl_addr["street_line1"].'</td></tr>
                <tr><td>'.$ctl_addr["street_line2"].'</td></tr>
                <tr><td>'.$ctl_addr["country_name"].' '.$ctl_addr["postal_code"].'</td></tr>
                <tr><td>Tel: '.$ctl_addr["tel_code"].' Fax: '.$ctl_addr["fax_code"].'</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
        <td style="width: 25%;" align="right">
            <table width="100%">
                <tr><td>
                    <table width="100%" cellspacing="1" cellpadding="1">
                        <tr><td align="right"><b>VRC NO.:</b></td><td style="border:1px solid #888" align="left"><span> '.$info[0]['trans_id'].'</span></td></tr>
                        <tr><td align="right"><b>DATE:</b></td><td style="border:1px solid #888;" align="left"><span> '.$info[0]['trans_date'].'</span></td></tr>
                        <tr><td align="right"><b>Currency:</b></td><td style="border:1px solid #888" align="left"><span> '.$info[0]['curr_id'].'</span></td></tr>
                    </table>
                </td></tr>
                <tr><td align="right">&nbsp;<br><span style="font-weight:bold;"><!-- Vendor Claims --></span></td></tr> 
            </table>
        </td>
    </tr>
</table>

<br><b>Type:</b> '.$info[0]['claim_type_desc'].'<br><br>

<table>
  <tr>
    <td width="5%"><b>Num</b></td>
    <td width="12%"><b>UPC</b></td>
    <td width="35%"><b>Description</b></td>
    <td width="8%" align="right"><b>Qty</b></td>
    <td width="10%" align="right"><b>Current Cost</b></td>
    <td width="10%" align="right"><b>New Cost</b></td>
    <td width="10%" align="right"><b>Unit Rebate</b></td>
    <td width="10%" align="right"><b>Total Rebate</b></td>
  </tr>
</table>';
}

function FOOTERP($info,$ctl_addr){
    return '<table>
                <tr><td colspan="2"></td></tr>
                <tr>
                    <td valign="bottom" style="border: 0px solid white; width: 450px; height: auto; font-size: 9px;">
                        Please credit us within 10 days. Otherwise, our Debit Note will be issued.<br><br>
                        Please approve and reply your confirmation fo the above via fax 6318 9867 or email.<br>
                        <table>
                            <tr><td width="74">Supplier:</td><td>'.$info[0]['supp_name'].'</td></tr>
                            <tr><td>Contact Person:</td><td>'.$info[0]['contact_person'].'</td></tr>
                            <tr><td>Tel Num:</td><td>'.$info[0]['tel_code'].'</td></tr>
                            <tr><td>Fax Num:</td><td>'.$info[0]['fax_code'].'</td></tr>
                            <tr><td>Email Address:</td><td>'.$info[0]['email_addr'].'</td></tr>
                        </table>
                        <br><br><br><br><br>
                        <table style="border-top:1px solid #888; width:180px"><tr><td><b>Acknowledged By:</b></td></tr></table>
                    </td>
                    <td valign="bottom" style="border: 0px solid white; width: 300px; height: auto; font-size: 9px;">
                        <br><br><br>On Behalf of: <b>'.$ctl_addr["coy_name"].'</b><br>
                        <table>
                            <tr><td width="70">Issued By:</td><td>'.$info[0]['issued_by'].'</td></tr>
                            <tr><td>Email Address:</td><td>'.$info[0]['issued_email_addr'].'</td></tr>
                            <tr><td>Approved By:</td><td>'.$info[0]['approved_by'].'</td></tr>
                            <tr><td>Approved On:</td><td></td></tr> 
                        </table>
                        <br><br><br><br><br>
                        <table style="border-top:1px solid #888; width:180px"><tr><td><b>Approved By:</b></td></tr></table>
                    </td>
                </tr>
            </table>';
}

function CONTENTP($info){
    $vrcamount = 0;
    $c = '<table>';
    foreach($info as $item) {
        $c.= '<tr>
            <td width="5%">'.$item['line_num'].'</td>
            <td width="12%">'.$item['item_id'].'</td>
            <td width="35%">'.$item['item_desc'].'</td>
            <td width="8%" align="right">'.intval($item['trans_qty']).'</td>
            <td width="10%" align="right">'.price_no_symbol($item['unit_cost']).'</td>
            <td width="10%" align="right">'.price_no_symbol($item['new_cost']).'</td>
            <td width="10%" align="right">'.price_no_symbol($item['unit_rebate']).'</td>
            <td width="10%" align="right">'.price_no_symbol($item['total_rebate']).'</td>
        </tr>';
        $vrcamount += $item['total_rebate'];
    }
    $c.= '<tr><td colspan="8">&nbsp;</td></tr>
        <tr>
            <td colspan="3">Remarks: '.htmlspecialchars($item['remarks']).'</td>
            <td colspan="4" align="right"><b>*Subject to GST* TOTAL REBATE: </b></td>
            <td align="right"><b>'.price_no_symbol($vrcamount).'</b></td>
        </tr>
    </table>';
    return $c;
}

?>