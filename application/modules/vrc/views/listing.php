<!-- Main content -->
<section class="clearfix content">
    <?php echo theme_view('general/search_bar_filter'); ?>
    
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header hidden">
                    <h3 class="box-title">Listing</h3> 
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <table id="datatableIL" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>VRC No.</th>
                                <th>Date</th>
                                <th class="h-mobile">Issue By</th>
                                <th class="h-mobile">Remarks</th>
                                <th class="h-mobile">Curr.</th>
                                <th>Rebate Amt</th>
                                <th>Debit Note</th>
                                <th>Items</th>
                                <th class="no-sort"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($results):
                                foreach ($results as $result):
                                    ?>
                                    <tr>
                                        <td><?php echo $result["trans_id"]; ?></td>
                                        <td><?php echo display_date_format($result["trans_date"]); ?></td>
                                        <td class="h-mobile"><?php echo $result["issued_by"]; ?></td>
                                        <td class="h-mobile"><div style="width:300px"><?php echo $result["remarks"]; ?></div></td>
                                        <td class="h-mobile"><?php echo $result["curr_id"]; ?></td>
                                        <td class="h-mobile"><?php echo price_no_symbol($result["rebates"]); ?></td>
                                        <td><a href="<?php echo base_url("rn/dn_pdf/".$result["dn_no"]) ; ?>" class="popuplink"><?php echo $result["dn_no"]; ?></a></td>
                                        <td class="h-mobile"><?php echo intval($result["items"]); ?></td>
                                        <td class="pull-right">
                                            <a href="<?php echo base_url("vrc/export/pdf/".$result["trans_id"]) ; ?>" class="popuplink btn btn-info btn-sm">PDF</a>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr></td><td colspan="8" class="dataTables_empty"><br>No data available in table<br>&nbsp;</td></tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
</section>
<!-- /.content -->

<?php if ($results): ?>
<script>
    document.onreadystatechange = function () {
        if (document.readyState == "interactive") {
            $("#datatableIL").DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 50,
                "lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
                "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
            });
            $("#datatableIL_filter label").css("color", "#FFF");
            $("#datatableIL_filter label input").attr("placeholder", "Search all");
            $("#InvbtnSpanDiv").appendTo("#datatableIL_length label");
        }
    }
</script>
<?php endif; ?>