<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Vrc extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('vrc/Vrc_model');
        $this->load->model('login/Login_model');
        $this->load->library('session');

        if ($this->input->get('cherps')>(time()-30) && $this->input->get('cherps')<(time()+5)) {
            // Skip check for login
            // http://chvoices-test.challenger.sg/cherps/595335B3-CAAF-43B5-A0DD-E316C39EEBBE
        }
        else {
            $this->_checkLogged();
        }
    }

    public function listing() {
        set_time_limit(180);

        $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : date('d-m-Y',strtotime("-1 month"));
        $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : date('d-m-Y');
        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
            $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : '';
            $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : '';
        }
        if (isset($data["search_from"])) {
            $params["search_from"] = $data["search_from"];
        }
        if (isset($data["search_to"])) {
            $params["search_to"] = $data["search_to"];
        }
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        //$params["claim_type"] = 'D4';
        $output = $this->Vrc_model->get_all_claims($params);
        //echo "<pre>";print_r($output);exit;
        $data["results"] = $output;
        $data["page_title"] = 'Claims (VRC)';
        $data["sidebar"] = 'Claims';
        Template::set($data);
        Template::render();
    }

    public function export($export_type='pdf',$doc_id,$supp_id='') {
        ini_set('max_execution_time',300);

        $doc_id = (isBase64($doc_id)) ? base64_decode($doc_id) : $doc_id ;

        $params["ids"] = isset($doc_id) ? explode(',',$doc_id) : '';
        $params["supp_id"] = ($supp_id=='') ? $this->session->userdata('cm_supp_id') : urldecode($supp_id);
        $vrclists = $this->Vrc_model->pdf_list($params);
        $data["vrcinfos"] = $vrclists;

        $data["ctl_addr"] = $this->Login_model->get_coy_address("CTL","CTL","full");

        if ($this->input->get("debug")==1) {
            print_r($data);
            exit;
        }

        $this->load->helper('pdf_helper');
        $this->load->view('vrc/vrc_pdf', $data);
    }

}
