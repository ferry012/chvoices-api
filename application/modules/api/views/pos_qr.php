<html>
    
<head>
<title>Challenger :: POS QR Scanner</title>
<style type="text/css">
    #v { 
        width: 320px;
        height: 240px;
    }
    #outdiv {
        width: 320px;
        height: 240px;
        border: solid 3px #555;
    }
    #webcamimg {
        padding: 3px;
    }
    #result-div {
        padding:8px 2px;
        font-family: "Segoe UI",Arial,sans-serif;
        font-size: 14px;
        background-color:#555;
        color:#FFF;
        width:320px;
        border: solid 1px #555;
    }
    #button-div {
        margin-top:20px;
    }
    .btn {
        display: inline-block;
        padding: 6px 12px !important;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
        color: #333;
        background-color: #fff;
        border-color: #ccc;
    }
</style>

<?php $this->load->view('api/pos_qr_js'); ?>
<script>
    var cnst_SCANNING = 'Scanning QR Code...';
    var dots = '.';

    function performtask(a) {
        var url = window.location.href.split("/");
        var baseUrl = url[0] + "//" + url[2] + "/";
        var params = "CTL---POS---" + getUrlParameter("loc_id") + "---" + getUrlParameter("pos_id") + "---" + a;
        var APIURL = baseUrl + 'api/qrscan/scan/' + params;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText == "null") {
                    ui_result('Member ID not found');
                    setTimeout(setwebcam, 3000);
                }
                else {
                    var data = JSON.parse(this.responseText);
                    ui_result('Welcome, ' + data.name);
                }
            }
        };
        xhttp.open("POST", APIURL, true);
        xhttp.send(params);
    }
    
    function ui_result(message){
        if (typeof (message) != "undefined") {
            document.getElementById("result").innerHTML = message;
            document.getElementById("button-div").style.display = 'block';
            dots = '';
        }
        else {
            if (dots.length > 0) {
                dots += '.';
                if (dots.length > 5)
                    dots = '.';
                document.getElementById("result").innerHTML = cnst_SCANNING + dots;
            }
        }
    }
    
    function ui_reset(){
        dots = '.';
        document.getElementById("button-div").style.display = 'none';
    }

</script>

</head>

<body>
    <div id="main">
        <div id="mainbody">
            <center>
                <div id="outdiv"></div>
                <canvas id="qr-canvas" width="400" height="300" style="display: none;"></canvas>

                <div id="result-div"><span id="result"></span></div>

                <div id="button-div" style="display:none">
                    <button class="btn" id="webcamimg" onclick="setwebcam()">SCAN AGAIN</button>
                    <button id="qrimg" onclick="setimg()" style="display:none">UPLOAD</button>
                </div>
            </center>
        </div>
    </div>

    <script type="text/javascript">
        load();
    </script>
</body>

</html>
