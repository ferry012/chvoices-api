<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Lazada_model extends Base_Common_Model {

    private $lazacct;
    private $apiurl;
    private $api_key;
    private $user_id;

    public function __construct() {
        parent::__construct();

        // DECLARE const in this API
        $this->apiurl = 'https://api.sellercenter.lazada.sg/';

        // Challenger store
        $this->lazacct = 'CTL';
        $this->api_key = 'BPWS0t8Jp_T7FDA84CtvA1zrPmpX9PcRQ2Z8_t7WSwsNneMiSTrUx1rY';
        $this->user_id = 'yongsheng@challenger.sg';

        // Fitbit Official
        if ($this->input->get('acct')=='fitbit') {
            $this->lazacct = 'FITBIT';
            $this->api_key = 'Ujb6YZxIIs5udEQzF5DP_RzcCbV72_-pICSY3eWo_2v5P-eWiEh9wVVv';
            $this->user_id = 'ys.challenger@gmail.com';
        }

    }
    
    public function import_orders(){

        //date_default_timezone_set("UTC");
        $date = new DateTime();
        $date->add(DateInterval::createFromDateString('yesterday'));
        $datestamp = $date->format(DateTime::ISO8601);

        $now = new DateTime();
        $timestamp = $now->format(DateTime::ISO8601);
        $api_key = $this->api_key;
        $parameters = array(
            'UserID' => $this->user_id,
            'Version' => '1.0',
            'Format' => 'JSON',
            'Timestamp' => $timestamp,
            'Action' => 'GetOrders',
            'CreatedAfter' => $datestamp //'2017-05-01T00:00:00+08:00'
        );
        ksort($parameters);
        $encoded = array();
        foreach ($parameters as $name => $value) {
            $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
        }
        $concatenated = implode('&', $encoded);
        $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

        $url = $this->apiurl;
        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

        // Open cURL connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url."?".$queryString);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $dataraw = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($dataraw);
        $orders = $data->SuccessResponse->Body->Orders;
        foreach ($orders as $order) {
            $order = (array) $order;
            $captured = array();
            $captured['order_id'] = $order["OrderId"];
            $captured['order_no'] = $order["OrderNumber"];
            $captured['order_date'] = $order["CreatedAt"];
            $captured['items_count'] = $order["ItemsCount"];
            $captured['items_price'] = floatval(str_replace(',','',$order["Price"]));
            $captured['created_at'] = $order["CreatedAt"];
            $captured['updated_at'] = $order["UpdatedAt"];
            $captured['first_name'] = $order["CustomerFirstName"];
            $captured['last_name'] = $order["CustomerLastName"];
            $captured['billing_first_name'] = $order["AddressBilling"]->FirstName;
            $captured['billing_last_name'] = $order["AddressBilling"]->LastName;
            $captured['billing_phone1'] = $order["AddressBilling"]->Phone;
            $captured['billing_phone2'] = $order["AddressBilling"]->Phone2;
            $captured['billing_addr1'] = $order["AddressBilling"]->Address1;
            $captured['billing_addr2'] = $order["AddressBilling"]->Address2;
            $captured['billing_addr3'] = $order["AddressBilling"]->Address3;
            $captured['billing_addr4'] = $order["AddressBilling"]->Address4;
            $captured['billing_addr5'] = $order["AddressBilling"]->Address5;
            $captured['billing_email_addr'] = $order["AddressBilling"]->CustomerEmail;
            $captured['billing_city'] = $order["AddressBilling"]->City;
            $captured['billing_postal'] = $order["AddressBilling"]->PostCode;
            $captured['billing_country'] = $order["AddressBilling"]->Country;
            $captured['shipping_first_name'] = $order["AddressShipping"]->FirstName;
            $captured['shipping_last_name'] = $order["AddressShipping"]->LastName;
            $captured['shipping_phone1'] = $order["AddressShipping"]->Phone;
            $captured['shipping_phone2'] = $order["AddressShipping"]->Phone2;
            $captured['shipping_addr1'] = $order["AddressShipping"]->Address1;
            $captured['shipping_addr2'] = $order["AddressShipping"]->Address2;
            $captured['shipping_addr3'] = $order["AddressShipping"]->Address3;
            $captured['shipping_addr4'] = $order["AddressShipping"]->Address4;
            $captured['shipping_addr5'] = $order["AddressShipping"]->Address5;
            $captured['shipping_email_addr'] = $order["AddressShipping"]->CustomerEmail;
            $captured['shipping_city'] = $order["AddressShipping"]->City;
            $captured['shipping_postal'] = $order["AddressShipping"]->PostCode;
            $captured['shipping_country'] = $order["AddressShipping"]->Country;
            $captured['remarks'] = $order["Remarks"];
            $captured['paymentmethod'] = $order["PaymentMethod"];
            $captured['voucher'] = $order["Voucher"];
            $captured['vouchercode'] = $order["VoucherCode"];
            $captured['shippingfee'] = $order["ShippingFee"];
            $captured['deliveryinfo'] = $order["DeliveryInfo"];
            $captured['giftmessage'] = $order["GiftMessage"];
            $captured['national_regno'] = $order["NationalRegistrationNumber"];
            $captured['ship_time'] = $order["PromisedShippingTimes"];
            $captured['attr'] = $order["ExtraAttributes"];
            $captured['order_status'] = $order["Statuses"];
            $captured['laz_account'] = $this->lazacct;
            $neworder = $this->b2b_import_order_list($captured, "b2b_lazada_order_list");

            // Get items
            if ($neworder){
                $parameters = array(
                    'UserID' => $this->user_id,
                    'Version' => '1.0',
                    'Format' => 'JSON',
                    'Timestamp' => $timestamp,
                    'Action' => 'GetOrderItems',
                    'OrderId' => $captured['order_id']
                );
                ksort($parameters);
                $encoded = array();
                foreach ($parameters as $name => $value) {
                    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
                }
                $concatenated = implode('&', $encoded);
                $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

                $url = $this->apiurl;
                $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

                // Open cURL connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url."?".$queryString);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                $dataraw = curl_exec($ch);
                curl_close($ch);

                $data = json_decode($dataraw);
                if (isset($data->ErrorResponse)) {
                    // Fail to get items. Delete header & email to YS.
                    $captureditem["order_id"] = $captured['order_id'];
                    $this->b2b_import_order_delete($captureditem, "b2b_lazada_order_list","b2b_lazada_order_item");

                    $subj = "Lazada Import Fail - " . $captured['order_id'];
                    $msg = 'Fail to import this order from Lazada. Please try manually.';
                    $sql = "exec sp_SysSendDBmail @from_address = 'chvoices@challenger.sg', @recipients = 'yongsheng@challenger.sg', @copy_recipients = '', @subject = '".$subj."', @body = '".$msg."', @file_attachments = '' ";
                    $sendmail = CI::db()->query($sql);
                    $return['message'] = 'Fail to import Lazada Order ' . $captured['order_id'];
                }
                $orderitems = $data->SuccessResponse->Body->OrderItems;
                foreach ($orderitems as $orderitem) {
                    $orderitem = (array) $orderitem;
                    $itemsku = $this->trimItemSku($orderitem["Sku"]);

                    $captureditem = array();
                    $captureditem["order_id"] = $orderitem["OrderId"];
                    $captureditem["shop_id"] = $orderitem["ShopId"];
                    $captureditem["item_id"] = $orderitem["OrderItemId"];
                    $captureditem["item_name"] = $orderitem["Name"];
                    $captureditem["item_sku"] = $itemsku;
                    $captureditem["shop_sku"] = $orderitem["ShopSku"];
                    $captureditem["shipping_type"] = $orderitem["ShippingType"];
                    $captureditem["item_price"] = floatval(str_replace(',','',$orderitem["ItemPrice"]));
                    $captureditem["paid_price"] = $orderitem["PaidPrice"];
                    $captureditem["curr_id"] = $orderitem["Currency"];
                    $captureditem["wallet_credits"] = $orderitem["WalletCredits"];
                    $captureditem["tax_amt"] = $orderitem["TaxAmount"];
                    $captureditem["ship_amt"] = $orderitem["ShippingAmount"];
                    $captureditem["ship_svc_cost"] = $orderitem["ShippingServiceCost"];
                    $captureditem["voucher_amt"] = $orderitem["VoucherAmount"];
                    $captureditem["voucher_code"] = $orderitem["VoucherCode"];
                    $captureditem["order_status"] = $orderitem["Status"];
                    $captureditem["ship_provider"] = $orderitem["ShipmentProvider"];
                    $captureditem["is_digital"] = $orderitem["IsDigital"];
                    $captureditem["digital_delivery_info"] = $orderitem["DigitalDeliveryInfo"];
                    $captureditem["tracking_code"] = $orderitem["TrackingCode"];
                    $captureditem["tracking_codepre"] = $orderitem["TrackingCodePre"];
                    $captureditem["reason"] = $orderitem["Reason"];
                    $captureditem["reason_detail"] = $orderitem["ReasonDetail"];
                    $captureditem["po_id"] = $orderitem["PurchaseOrderId"];
                    $captureditem["po_no"] = $orderitem["PurchaseOrderNumber"];
                    $captureditem["pkg_id"] = $orderitem["PackageId"];
                    $captureditem["ship_time"] = $orderitem["PromisedShippingTime"];
                    $captureditem["attr"] = $orderitem["ExtraAttributes"];
                    $captureditem["ship_provider_type"] = $orderitem["ShippingProviderType"];
                    $captureditem["created_at"] = $orderitem["CreatedAt"];
                    $captureditem["updated_at"] = $orderitem["UpdatedAt"];
                    $captureditem["return_status"] = $orderitem["ReturnStatus"];
                    $captureditem["image_url"] = $orderitem["productMainImage"];
                    $captureditem["variation"] = $orderitem["Variation"];
                    $captureditem["item_ref_url"] = $orderitem["ProductDetailUrl"];
                    $captureditem["invoice_no"] = $orderitem["invoiceNumber"];
                    $this->b2b_import_order_item($captureditem, "b2b_lazada_order_item");
                }
            }
        }
        // Exec o2o_sp_import_exorder
        $sql = "EXEC sp_b2b_lazada_import;";
        $result = $this->db->query($sql)->result_array();
        $result[0]['acct'] = $this->lazacct;

        return $result;
    }

    public function order_products_sync(){
        //date_default_timezone_set("UTC");
        $date = new DateTime();
        $date->add(DateInterval::createFromDateString('3 days ago'));
        $datestamp = $date->format(DateTime::ISO8601);

        $now = new DateTime();
        $timestamp = $now->format(DateTime::ISO8601);
        $api_key = $this->api_key;
        $parameters = array(
            'UserID' => $this->user_id,
            'Version' => '1.0',
            'Format' => 'JSON',
            'Timestamp' => $timestamp,
            'Action' => 'GetProducts',
            'Limit' => '500',
            'Filter' => 'live'
            //,'UpdatedAfter'=>$datestamp
        );
        ksort($parameters);
        $encoded = array();
        foreach ($parameters as $name => $value) {
            $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
        }
        $concatenated = implode('&', $encoded);
        $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

        $url = $this->apiurl;
        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
        //echo $url."?".$queryString .'<hr>';

        // Open cURL connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url."?".$queryString);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $dataraw = curl_exec($ch);
        curl_close($ch);

        //echo $dataraw .'<hr>';
        $data = json_decode($dataraw);

        $lazada_products=[]; $shop_sku=[];
        $orders = $data->SuccessResponse->Body->Products;
        foreach ($orders as $order) {
            foreach ($order->Skus as $orderitem) {
                if ($orderitem->ShopSku!='') {
                    // Only care of approved products (with shopsku)
                    $shop_sku[] = $orderitem->ShopSku;
                    $lazada_products[] = array(
                        "shop_sku" => $orderitem->ShopSku,
                        "seller_sku" => $orderitem->SellerSku,
                        "status" => $orderitem->Status,
                        "price" => $orderitem->price,
                        "special_price" => $orderitem->special_price,
                        "available" => $orderitem->Available,
                        "quantity" => $orderitem->quantity,
                        "item_id" => $this->trimItemSku($orderitem->SellerSku)
                    );
                }
            }
        }

        // STEP 2: Truncate, save in DB and count qty
        $this->b2b_products_clean();
        foreach ($lazada_products as $k=>$row) {
            $this->b2b_products_insert($row);
        }
        $this->b2b_products_syncqty();

        // STEP 3: Update & Email results
        $msg = '';
        $product_unlink = $this->b2b_products_get_empty();
        if ($product_unlink) {
            $msg.= "Invalid SKU:";
            foreach ($product_unlink as $r) {
                $msg.= "\n  ".$r['seller_sku']." [Qty: ".$r['quantity']."]";
            }
            $msg.= "\n\n";
        }

        $product_synced = $this->b2b_products_get_sync();
        if ($product_synced) {
            $sync = false; //$this->order_products_send($product_synced);
            $msg .= ($sync) ? "Updated following items:" : "SIMULATED results of following items:";
            foreach ($product_synced as $r) {
                $msg.= "\n\t".$r['seller_sku']."\t[Qty: ".$r['qty_on_hand'].", Old: ".$r['quantity']."]\t".trim($r['item_desc'])." (Buffer: ".$r['buffer_qty'].")";
            }
        }

        $msgresult = 'Updated ' . count($product_synced) . ' products to Lazada ';
        $msgresult.= (count($product_unlink)>0) ? '(' . count($product_unlink) . ' invalid)' : '';

        if ($msg!='') {
            $cherps_email_subject = 'Lazada '.$this->lazacct.' Product Sync - '.date('d M Y').'';
            $cherps_email_message = $msgresult."\n\n".$msg;
            $cherps_email_addr = ($this->input->get('email')) ? $this->input->get('email') : 'dongwei@challenger.sg; zulaiha@challenger.sg; yongsheng@challenger.sg; ';
            $this->cherps_email($cherps_email_subject,$cherps_email_message,$cherps_email_addr);
        }

        return array("message"=> $msgresult);
    }

    public function order_products_send($product_list){

        $request_body = '<?xml version="1.0" encoding="UTF-8"?> <Request><Product><Skus>';
        foreach ($product_list as $r) {
            $request_body.= '<Sku><SellerSku>'.$r['seller_sku'].'</SellerSku><Quantity>'.$r['qty_on_hand'].'</Quantity></Sku>';
        }
        $request_body.= '</Skus></Product></Request>';

        $now = new DateTime();
        $timestamp = $now->format(DateTime::ISO8601);
        $api_key = $this->api_key;
        $parameters = array(
            'UserID' => $this->user_id,
            'Version' => '1.0',
            'Format' => 'JSON',
            'Timestamp' => $timestamp,
            'Action' => 'UpdatePriceQuantity'
        );
        ksort($parameters);
        $encoded = array();
        foreach ($parameters as $name => $value) {
            $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
        }
        $concatenated = implode('&', $encoded);
        $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

        $url = $this->apiurl;
        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
        //echo $url."?".$queryString .'<hr>';

        // Open cURL connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url."?".$queryString);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $request_body);
        $dataraw = curl_exec($ch);
        curl_close($ch);

        //echo $dataraw .'<hr>';
        $data = json_decode($dataraw);

        return ($data->SuccessResponse) ? true : false;
    }

    private function b2b_products_get_sync(){
        $sql = "SELECT b.seller_sku,i.item_desc,b.quantity,
                    (CASE WHEN b.qty_on_hand>0 THEN b.qty_on_hand ELSE 0 END) [qty_on_hand] ,b.buffer_qty
                FROM b2b_lazada_products b
                  JOIN ims_item_list i ON i.item_id=b.item_id
                WHERE b.quantity != (CASE WHEN b.qty_on_hand>0 THEN b.qty_on_hand ELSE 0 END) AND qty_on_hand IS NOT NULL
                ORDER BY [qty_on_hand]";
        $result = $this->db->query($sql)->result_array();
        return ($result) ? $result : NULL;
    }

    private function b2b_products_get_empty(){
        $sql = "SELECT seller_sku,quantity FROM b2b_lazada_products WHERE inv_dim4 IS NULL AND buffer_qty IS NULL AND qty_on_hand IS NULL; ";
        $result = $this->db->query($sql)->result_array();
        return ($result) ? $result : NULL;
    }

    private function b2b_products_clean(){
        $sql = "TRUNCATE TABLE b2b_lazada_products;";
        $result = $this->db->query($sql);
        return $result;
    }

    private function b2b_products_syncqty(){
        $sql = "UPDATE b2b_lazada_products
                    SET item_id = i.item_id,
                        inv_dim4 = i.inv_dim4,
                        buffer_qty = i.buffer_qty,
                        qty_on_hand = ( 
							CASE WHEN i.inv_dim2 IN ('DESKTOP','MOBILE COM','PRINTING') THEN
								a.quantity
							ELSE
								(SELECT ISNULL(SUM(qty_on_hand-qty_reserved),0) FROM ims_inv_physical WHERE item_id= i.item_id AND loc_id IN ('HCL')) - i.buffer_qty
							END 
						) 
                FROM b2b_lazada_products a, ims_item_list i
                WHERE a.item_id=i.item_id; ";
        $result = $this->db->query($sql);
        return $result;
    }

    private function b2b_products_insert($params){
        $sql = "INSERT INTO b2b_lazada_products (shop_sku,seller_sku,status,price,special_price,available,quantity,item_id)
                  VALUES ('".$params['shop_sku']."','".$params['seller_sku']."','".$params['status']."',
                                '".$params['price']."','".$params['special_price']."','".$params['available']."','".$params['quantity']."',
                                '".$params['item_id']."');";
        $result = $this->db->query($sql);
        return $result;
    }

    private function b2b_import_order_list($params, $table){
        $sql = "SELECT * FROM $table WHERE order_id= '".$params['order_id']."' ";
        $query = $this->db->query($sql);
        if($query !== FALSE && $query->num_rows() > 0){
            return FALSE;
        }
        else {
            $params = (object) $params;
            $result = $this->db->insert($table, $params);
            return TRUE;
        }
    }

    private function b2b_import_order_item($params, $table){
        $params = (object) $params;
        $result = $this->db->insert($table, $params);
        return $result;
    }

    private function b2b_import_order_delete($params, $table_order, $table_item){
        foreach($params as $key=>$value) {
            $result = $this->db->delete([$table_order,$table_item], "$key='$value'");
        }
        return $result;
    }

    private function trimItemSku($itemsku){
        $itemsku = trim($itemsku);
        if (strlen($itemsku)==16 && strpos('-'.$itemsku , 'CTL')==1) {
            // Remove first 3 letters (CTL)
            $itemsku = substr($itemsku,3);
        } else if (strlen($itemsku)<13) {
            // Add leading zero
            $itemsku = str_pad($itemsku, 13, '0', STR_PAD_LEFT);
        } else if (strpos('Â',$itemsku)>0) {
            // Replace unknown char
            $itemsku = str_replace('Â','',$itemsku);
        }
        return trim($itemsku);
    }

    public function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {
        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = '';
        $params['subject'] = $subject;
        $params['message'] = $message;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }

}

?>
