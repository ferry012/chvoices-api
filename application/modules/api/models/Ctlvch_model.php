<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Ctlvch_model extends Base_Common_Model
{

    public function __construct()
    {
        parent::__construct();

        // DECLARE const in this API
        if (ENVIRONMENT == "production") {
            $this->partnerkey = 'obh0hqcxz81upu2ixyokn9t49na1ak3a';
        } else {
            $this->partnerkey = 'obh0hqcxz81upu2ixyokn9t49na1ak3a';
        }

    }

    // sms_voucher_list
    public function sms_voucher($mode,$id){
        if ($this->input->get('PKEY') != $this->partnerkey) {
            return ['code' => -1, 'msg' => 'Partner Key does not exist.'];
        }

        if ($mode=="validate") {

            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody,1);

            $id = $postarr['voucher_id'][0];
            $item_id = $postarr['item_id'];

            //1. Check sms_voucher_list with voucher_id=$id, if the status=2 and expiry is OK
            $sql = "SELECT rtrim(sms_voucher_list.voucher_id) voucher_id,sms_voucher_list.expiry_date,
                        CASE WHEN i.item_type='C' THEN sms_voucher_list.status_level ELSE ( CASE WHEN sms_voucher_list.status_level IN (1,2) THEN 2 ELSE sms_voucher_list.status_level END ) END status_level,
                        CASE WHEN voucher_amount=0 THEN (select unit_price from ims_item_price where item_id=sms_voucher_list.item_id and price_type='REGULAR' and 
                                eff_from<current_timestamp and eff_to>current_timestamp) ELSE sms_voucher_list.voucher_amount END voucher_amount
                       FROM sms_voucher_list
                       JOIN ims_item_list i ON i.item_id=sms_voucher_list.item_id and i.coy_id=sms_voucher_list.coy_id
                       WHERE sms_voucher_list.coy_id='CTL' and sms_voucher_list.item_id='$item_id' and sms_voucher_list.voucher_id='$id' ";
            $result = $this->db->query($sql)->row_array();

            if ($result==null) {
                $code = 0;
                $msg = 'No voucher found ('.$id.')';
            }
            else if ( strtotime($result['expiry_date'])<time() ) {
                $code = 0;
                $msg = 'Voucher has has expired on '. date('Y-m-d',strtotime($result['expiry_date']));
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else if ( $result['status_level']<2 ) {
                $code = 0;
                $msg = 'Voucher is not activated';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else if ( $result['status_level']>2 ) {
                $code = 0;
                $msg = 'Voucher is utilized';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
                $result = null;
            }
            else {
                $code = 1;
                $msg = 'Voucher is valid';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }

            return [
                'code' => $code,
                'msg' => $msg,
                'voucher' => $result
            ];
        }
        else if ($mode=="utilize") {

            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody,1);

            $id = $postarr['voucher_id'][0];
            $item_id = $postarr['item_id'];
            $trans_id = $postarr['trans_id'];
            $usr_id = $postarr['usr_id'];
            if ($trans_id==null || $usr_id==null) {
                return ['code' => -1, 'msg' => 'Missing required paramters.', "param"=>$postarr];
            }

            $sql = "SELECT rtrim(sms_voucher_list.voucher_id) voucher_id,sms_voucher_list.expiry_date,
                        CASE WHEN i.item_type='C' THEN sms_voucher_list.status_level ELSE ( CASE WHEN sms_voucher_list.status_level IN (1,2) THEN 2 ELSE sms_voucher_list.status_level END ) END status_level,
                        CASE WHEN voucher_amount=0 THEN (select unit_price from ims_item_price where item_id=sms_voucher_list.item_id and price_type='REGULAR') ELSE sms_voucher_list.voucher_amount END voucher_amount
                       FROM sms_voucher_list
                       JOIN ims_item_list i ON i.item_id=sms_voucher_list.item_id and i.coy_id=sms_voucher_list.coy_id
                       WHERE sms_voucher_list.coy_id='CTL' and sms_voucher_list.item_id='$item_id' and sms_voucher_list.voucher_id='$id' ";
            $result = $this->db->query($sql)->row_array();

            if ($result==null) {
                $code = 0;
                $msg = 'No voucher found ('.$id.').';
            }
            else if ( strtotime($result['expiry_date'])<time() ) {
                $code = 0;
                $msg = 'Voucher has expired.';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else if ( $result['status_level']<2 ) {
                $code = 0;
                $msg = 'Voucher is not activated.';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else if ( $result['status_level']>2 ) {
                $code = 0;
                $msg = 'Voucher is utilized.';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
                $result = null;
            }
            else {
                $sql = "UPDATE sms_voucher_list
                            SET status_level=3,modified_by='$usr_id',modified_on=current_timestamp ,utilize_date=current_timestamp,receipt_id2='$trans_id'
                            WHERE coy_id='CTL' and item_id='$item_id' and voucher_id='$id'";
                $res = $this->db->query($sql);
                if ($res){
                    $code = 1;
                    $msg = 'Voucher successfully utilized.';
                    $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
                }
                else {
                    $code = 1;
                    $msg = 'Voucher update failed.';
                    $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
                }
            }

            return [
                'code' => $code,
                'msg' => $msg,
                'voucher' => $result
            ];
        }
        else if ($mode=="activate") {

            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody,1);

            $id = $postarr['voucher_id'][0];
            $item_id = $postarr['item_id'];
            $trans_id = $postarr['trans_id'];
            $usr_id = $postarr['usr_id'];
            $loc_id = $postarr['loc_id'];
            if ($trans_id==null || $usr_id==null || $loc_id==null) {
                return ['code' => -1, 'msg' => 'Missing required paramters.'];
            }

            $sql = "SELECT rtrim(voucher_id) voucher_id,expiry_date,status_level,
                        CASE WHEN voucher_amount=0 THEN (select unit_price from ims_item_price where item_id=sms_voucher_list.item_id and price_type='REGULAR') ELSE voucher_amount END voucher_amount
                       FROM sms_voucher_list
                       WHERE coy_id='CTL' and item_id='$item_id' and voucher_id='$id'
                        AND status_level=1";
            $result = $this->db->query($sql)->row_array();

            if ($result!=null) {
                $sql_expiry = date('Y-m-d', strtotime('+180 days')) . ' 23:59:59';
                $sql = "UPDATE sms_voucher_list
                            SET status_level=2,modified_by='$usr_id',modified_on=current_timestamp , sale_date=current_timestamp,receipt_id1='$trans_id',loc_id='$loc_id',expiry_date='$sql_expiry'
                            WHERE coy_id='CTL' and item_id='$item_id' and voucher_id='$id'";
                $res = $this->db->query($sql);
                if ($res){
                    $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
                    return ['code' => 1, 'msg' => 'Voucher activated.','voucher'=>$result];
                }
                else {
                    return ['code' => 0, 'msg' => 'Voucher update fail.'];
                }
            }
            else {
                return ['code' => 0, 'msg' => 'Voucher is not valid.'];
            }
        }


    }

    public function sms_activate_trans($trans_id){
        // Get trans_id and do GV activation
        $item_id = "!VCH-50";
        $sql = "select l.loc_id,l.trans_id,l.created_by,v.voucher_id 
                from pos_transaction_list l
                join pos_transaction_item i on l.coy_id=i.coy_id and l.trans_id=i.trans_id
                join pos_transaction_lot o on o.coy_id=i.coy_id and o.trans_id=i.trans_id and o.line_num=i.line_num
                join sms_voucher_list v on v.coy_id=o.coy_id and v.voucher_id=o.lot_id
                where l.coy_id='CTL' and l.status_level>0 and i.status_level>0 and l.trans_id='$trans_id'
                  and i.item_id='$item_id' and v.status_level=1";
        $result = $this->db->query($sql)->result_array();
        if ($result!=null) {
            $vch = [];
            $usr_id = $result[0]['created_by'];
            $trans_id = $result[0]['trans_id'];
            $loc_id = $result[0]['loc_id'];
            foreach ($result as $res) {
                $vch[] = $res['voucher_id'];
            }
            $sql_vch = implode("','",$vch);
            $sql_expiry = date('Y-m-d', strtotime('+180 days')) . ' 23:59:59';
            $sql = "UPDATE sms_voucher_list
                    SET status_level=2,modified_by='$usr_id',modified_on=current_timestamp , sale_date=current_timestamp,receipt_id1='$trans_id',loc_id='$loc_id',expiry_date='$sql_expiry'
                    WHERE coy_id='CTL' and item_id='$item_id' and voucher_id IN ('$sql_vch')";
            $res = $this->db->query($sql);
            return ['code' => 1, 'msg' => 'Activated '.(count($result)).' vouchers.'];
        }
        else {
            return ['code' => 0, 'msg' => 'No transaction found.'];
        }
    }

    // crm_voucher_list
    public function crm_voucher($mode,$id) {

        $cherps_hachi = $this->load->database('cherps_hachi', TRUE);

        if ($this->input->get('PKEY') != $this->partnerkey) {
            return ['code' => 0, 'msg' => 'Partner Key does not exist.'];
        }

        if ($mode=="validate") {

            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody,1);

            $id = $postarr['voucher_id'][0];
            $coupon_id = $postarr['item_id'];
            $trans_id = $postarr['trans_id'];
            $usr_id = $postarr['usr_id'];
            if ($trans_id==null || $usr_id==null) {
                return ['code' => -1, 'msg' => 'Missing required parameters.', "param"=>$postarr];
            }

            $sql = "SELECT rtrim(coupon_serialno) voucher_id,expiry_date,status_level, voucher_amount
                       FROM crm_voucher_list
                       WHERE coy_id='CTL' and coupon_id='$coupon_id' and coupon_serialno='$id' ";
            // $sql .= " AND status_level=0 AND expiry_date>=GETDATE() ";
            $result = $cherps_hachi->query($sql)->row_array();

            if ($result==null) {
                $code = 0;
                $msg = 'No voucher found ('.$id.')';
            }
            else if ( strtotime($result['expiry_date'])<time() ) {
                $code = 0;
                $msg = 'Voucher has has expired on '. date('Y-m-d',strtotime($result['expiry_date']));
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else if ( $result['status_level']<0 ) {
                $code = 0;
                $msg = 'Voucher is not active';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else if ( $result['status_level']>0 ) {
                $code = 0;
                $msg = 'Voucher is utilized';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else {
                $code = 1;
                $msg = 'Voucher is valid';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }

            return [
                'code' => $code,
                'msg' => $msg,
                'voucher'=>['voucher_id'=>$result['voucher_id'],'expiry_date'=>$result['expiry_date'],'voucher_amount'=>$result['voucher_amount']]
            ];

        }
        else if ($mode=="utilize") {

            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody,1);

            $id = $postarr['voucher_id'][0];
            $coupon_id = $postarr['item_id'];
            $trans_id = $postarr['trans_id'];
            $usr_id = $postarr['usr_id'];
            if ($trans_id==null || $usr_id==null) {
                return ['code' => -1, 'msg' => 'Missing required parameters', "param"=>$postarr];
            }

            $sql = "SELECT rtrim(coupon_serialno) voucher_id,expiry_date,status_level, voucher_amount
                       FROM crm_voucher_list
                       WHERE coy_id='CTL' and coupon_id='$coupon_id' and coupon_serialno='$id' ";
            //$sql = " AND status_level=0 AND expiry_date>=GETDATE() ";
            $result = $cherps_hachi->query($sql)->row_array();

            if ($result==null) {
                $code = 0;
                $msg = 'No voucher found ('.$id.')';
            }
            else if ( strtotime($result['expiry_date'])<time() ) {
                $code = 0;
                $msg = 'Voucher has has expired on '. date('Y-m-d',strtotime($result['expiry_date']));
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else if ( $result['status_level']<0 ) {
                $code = 0;
                $msg = 'Voucher is not active';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else if ( $result['status_level']>0 ) {
                $code = 0;
                $msg = 'Voucher is utilized';
                $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
            }
            else {
                $sql = "UPDATE crm_voucher_list
                            SET status_level=1,modified_by='$usr_id',modified_on=GETDATE(),utilize_date=GETDATE(),receipt_id2='$trans_id'
                            WHERE coy_id='CTL' and coupon_id='$coupon_id' and coupon_serialno='$id'";
                $res = $cherps_hachi->query($sql);
                if ($res){
                    $code = 1;
                    $msg = 'Voucher successfully utilized.';
                    $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
                }
                else {
                    $code = 1;
                    $msg = 'Voucher update failed.';
                    $result['expiry_date'] = date('Y-m-d',strtotime($result['expiry_date']));
                }
            }

            return [
                'code' => $code,
                'msg' => $msg,
                'voucher'=>['voucher_id'=>$result['voucher_id'],'expiry_date'=>$result['expiry_date'],'voucher_amount'=>$result['voucher_amount']]
            ];

        }
        else if ($mode=="activate") {

            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody,1);

            $coupon_id = $postarr['item_id'];
            $trans_id = $postarr['trans_id'];
            $trans_amount = floatval(str_replace([',','$'],'',$postarr['trans_amount']));
            $usr_id = $postarr['usr_id'];
            $loc_id = $postarr['loc_id'];
            if ($trans_id==null || $usr_id==null || $loc_id==null || $trans_amount==null) {
                return ['code' => -1, 'msg' => 'Missing required parameters.'];
            }

            $coupon_no = $this->getRefId($loc_id.'RFV');
            $expiry_date = date('Y-m-d', strtotime(' +30 days'));

            $sql = "INSERT INTO crm_voucher_list (coy_id,coupon_id,coupon_serialno, promocart_id,mbr_id,trans_id,ho_ref,
                        trans_date,ho_date,issue_date,sale_date,print_date, expiry_date, utilize_date,
                        loc_id,receipt_id1,receipt_id2,voucher_amount,redeemed_amount,expired_amount,
                        issue_type,issue_reason,status_level,created_by,created_on,modified_by,modified_on)
                      VALUES ('CTL','$coupon_id','$coupon_no', '','','','', GETDATE(),GETDATE(),GETDATE(),GETDATE(),GETDATE(), CAST( '$expiry_date' as DATE), '1900-01-01',
                        '$loc_id','$trans_id','','$trans_amount',0,0, 'CREDIT','POS_REFUND',0, '$usr_id',GETDATE(),'$usr_id',GETDATE() )";
            $res = $cherps_hachi->query($sql);
            if ($res){
                return ['code' => 1, 'msg' => 'Voucher activated.','voucher'=>['voucher_id'=>$coupon_no,'expiry_date'=>$expiry_date,'voucher_amount'=>$trans_amount]];
            }
            else {
                return ['code' => 0, 'msg' => 'Voucher update fail.'];
            }

        }

    }

    // Private functions
    private function getRefId($trans_prefix){
//        $sql = "select next_num from sys_trans_list where sys_id='ADS' and trans_prefix='$trans_prefix'";
//        $res = $this->db->query($sql)->row_array();
//        return $res['next_num'];
        $cherps_hachi = $this->load->database('cherps_hachi', TRUE);
        $sql = "EXEC [sp_SysGetNextDocId] 'CTL','POS','VOUCHER','$trans_prefix' ";
        $res = $cherps_hachi->query($sql)->row_array();
        return $res['doc_id'];
    }

    public function saveGatewayLog($params)
    {
        $cherps_hachi = $this->load->database('cherps_hachi', TRUE);
        $coy_id = 'CTL';
        $gateway_id = 'CTLVCH';
        $gateway_code = md5($params['trans_id'] . $params['loc_id'] . $params['pos_id'] . $params['buyer_code'] . $params['tx_time']);
        $gateway_request = $params['trans_id'] . '/' . $params['loc_id'] . '/' . $params['pos_id'] . '/' . $params['buyer_code'] . '?t=' . $params['tx_time'];
        if ($params['status'] == "NEW") {
            $sql = "INSERT INTO b2b_gateway_message 
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id,request_link,request_msg,status_level,created_by,created_on)
                        VALUES ('$coy_id','$gateway_id','" . $gateway_code . "','" . trim($params['trans_id']) . "','" . trim($params['loc_id']) . "','" . trim($params['pos_id']) . "' ,
                        '" . trim($params['url']) . "','" . $gateway_request . "','0','" . getRealIpAddr(15) . "',GETDATE())";
            $cherps_hachi->query($sql);
        } else {
            $sql = "UPDATE b2b_gateway_message
                        SET status_level=1, return_status='" . $params['status'] . "', return_msg='" . $params['response'] . "'
                        WHERE coy_id='$coy_id' AND gateway_id='$gateway_id' AND gateway_code='$gateway_code' ";
            $cherps_hachi->query($sql);
        }
    }
}
?>
