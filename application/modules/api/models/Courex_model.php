<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Courex_model extends Base_Common_Model {

    private $apiurl;
    private $user;
    private $pass;

    public function __construct() {
        parent::__construct();

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->apiurl = 'https://www.courex.com.sg/xml/index.php';
            $this->user = 'challenger-test';
            $this->pass = 'password123';
        }
        else {
            $this->apiurl = 'https://www.courex.com.sg/xml/index.php';
            $this->user = 'challenger-test';
            $this->pass = 'password123';
        }
    }

    public function check_delivery(){
        $invoices = array();

        $apiUrl = $this->apiurl;
        $apiUser = [$this->user,$this->pass];

        $results = $this->o2o_delivery_log_get();
        foreach ($results as $result) {

            // Convert strtotime
            $result["act_neworder_on"] = strtotime($result["act_neworder_on"]);
            $result["act_collected_on"] = strtotime($result["act_collected_on"]);
            $result["act_intransit_on"] = strtotime($result["act_intransit_on"]);
            $result["act_delivered_on"] = strtotime($result["act_delivered_on"]);
            $result["act_undelivered_on"] = strtotime($result["act_undelivered_on"]);

            $trackno = $result["tracking_id"];
            $xmlcontent = '<RequestCall><AppType>Tracking</AppType><UserName>'.$apiUser[0].'</UserName><Password>'.$apiUser[1].'</Password><Parameters><TrackNo>'.$trackno.'</TrackNo></Parameters></RequestCall>';
            $headers = array(
                "Content-type: application/xml;"
            );

            //setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlcontent);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            $data = curl_exec($ch);
            curl_close($ch);

            //monitor for updates
            $resultchanges = 0;
            $xml = new SimpleXMLElement($data);
            foreach ($xml->Data as $row) {
                $val = intval($row->Date);
                if (in_array($row->Activity, ['New Order','Order Processing']) && empty($result["act_neworder_on"])) {
                    $resultchanges = ($resultchanges>1) ? $resultchanges : 1;
                    $result["act_neworder_on"]=$val;
                }
                if (in_array($row->Activity, ['Picked Up','Collected','Arrived Hub']) && empty($result["act_collected_on"])) {
                    $resultchanges= ($resultchanges>1) ? $resultchanges : 1;
                    $result["act_collected_on"]=$val;
                }
                if (in_array($row->Activity, ['In Warehouse','In Transit','On the way to deliver','Departed Hub']) && empty($result["act_intransit_on"])) {
                    $resultchanges= ($resultchanges>2) ? $resultchanges : 2;
                    $result["act_intransit_on"]=$val;
                }
                if (in_array($row->Activity, ['Delivered']) && empty($result["act_delivered_on"])) {
                    $resultchanges= ($resultchanges>3) ? $resultchanges : 3;
                    $result["act_delivered_on"]=$val;
                }
                if (strpos('-'.$row->Activity,'Undelivered') && empty($result["act_undelivered_on"])) {
                    $resultchanges= ($resultchanges>4) ? $resultchanges : 4;
                    $result["act_undelivered_on"]=$val;
                }

            }
            if ($resultchanges>0) {

                if ($resultchanges>=2) {
                    // Trigger email to Logistics for Receiving,Delivery,Posting.
                    $email_tracking_status = ($resultchanges==2) ? 'In Transit' : ($resultchanges==3) ? 'Delivered' : 'Undelivered';
                    $result["status_level"] = 1;
                    $email_param["from"] = "chvoices@challenger.sg";
                    $email_param["to"] = "yongsheng@challenger.sg; ";
                    $email_param["subject"] = "Delivery Notice - Couriex - " . trim($result["invoice_id"]);
                    $email_param["message"] = "Following delivery has been processed by Couriex (NQX)"
                        . "\n- Invoice: ".trim($result["invoice_id"])."\n- PO: ".trim($result["po_id"])."\n- Tracking No.: ".trim($result["tracking_id"])."\n- Latest Status: ".trim($email_tracking_status).""
                        . "\n\n Pls proceed to do receiving, delivery & posting.";
                    $results = $this->o2o_delivery_email($email_param);
                }

                // Save to DB
                $invoices[] = $result["invoice_id"];
                $results = $this->o2o_delivery_log_save($result);
            }

        }
        return $invoices;
    }

    public function o2o_delivery_log_get(){
        $sql = "SELECT po_id,invoice_id,tracking_id,status_level, 
                        act_neworder_on,act_collected_on,act_intransit_on,act_delivered_on,act_undelivered_on 
                    FROM o2o_delivery_log
                    WHERE status_level=0";
        return $query = $this->db->query($sql)->result_array();
    }

    public function o2o_delivery_log_save($params){
        $sql_time = date('Y-m-d', strtotime('-7 days', time()));
        $sql = "UPDATE o2o_delivery_log SET status_level=".$params["status_level"].", ";
        $sql .= ($params["act_neworder_on"]!='') ? "act_neworder_on='".date('Y-m-d H:i:s',$params["act_neworder_on"])."', " : '';
        $sql .= ($params["act_collected_on"]!='') ? "act_collected_on='".date('Y-m-d H:i:s',$params["act_collected_on"])."', " : '';
        $sql .= ($params["act_intransit_on"]!='') ? "act_intransit_on='".date('Y-m-d H:i:s',$params["act_intransit_on"])."', " : '';
        $sql .= ($params["act_delivered_on"]!='') ? "act_delivered_on='".date('Y-m-d H:i:s',$params["act_delivered_on"])."', " : '';
        $sql .= ($params["act_undelivered_on"]!='') ? "act_undelivered_on='".date('Y-m-d H:i:s',$params["act_undelivered_on"])."', " : '';
        $sql .= "tracking_count=tracking_count+1 ";
        $sql .= "WHERE po_id='".$params["po_id"]."' AND invoice_id='".$params["invoice_id"]."' AND tracking_id='".$params["tracking_id"]."' ";
        return $query = $this->db->query($sql);
    }

    public function o2o_delivery_email($params){
        $result = $this->_SendEmail($params);
        return $result;
    }

}

?>
