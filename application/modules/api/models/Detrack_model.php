<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Detrack_model extends Base_Common_Model {

    private $apiurl;
    private $token;

    public function __construct() {
        parent::__construct();

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->apiurl = 'https://app.detrack.com/api/v1/';
            $this->token = 'U510cbafaed90e1c2a42191398df76063154cbd3eef0b5d2f'; //'U510cbafaed90e1c2a42191398df76063154cbd3eef0b5d2f';
        }
        else {
            $this->apiurl = 'https://app.detrack.com/api/v1/';
            $this->token = '6f28c95b3c3482e5f2e0eb22f48b84a6e7a626ee9b814da4'; //'6f28c95b3c3482e5f2e0eb22f48b84a6e7a626ee9b814da4';
        }
    }

    public function getParcel($invoice_id='') {
        $sql_invoice = (strlen($invoice_id)>3) ? "and p.invoice_id='$invoice_id'" : "";
        $sql = "select 
                    p.coy_id,p.invoice_id,p.parcel_id,p.delv_mode_id,
                    --CASE WHEN (p.parcel_id>1) THEN rtrim(i.invoice_id)+'-'+CAST(p.parcel_id as nvarchar(3)) ELSE rtrim(i.invoice_id) END as [external_track_no], 
                    rtrim(i.invoice_id)+'-'+RTRIM(p.delv_mode_id)+'-'+CAST(p.parcel_id as nvarchar(3)) as [external_track_no], 
                    CASE WHEN (rtrim(i.first_name)+' '+rtrim(i.last_name)!='') THEN rtrim(i.first_name)+' '+rtrim(i.last_name)
                        ELSE (cust_name) END as [dst_name],
                    i.tel_code as [dst_contact],
                    CASE WHEN (i.email_addr!='') THEN i.email_addr
                        ELSE (select email_addr from crm_member_list where coy_id='CTL' and mbr_id=i.cust_id) END as [dst_email],
                    REPLACE(d.addr_text,'''','') as [dst_addr],
                    d.street_line1 dst_addr_line1, case when d.street_line2<>'' then '#'+d.street_line2+'-'+d.street_line3+' '+d.street_line4 else isnull(d.street_line4,'-') end dst_addr_line2,
                    d.postal_code as [dst_postcode], 
                    CASE WHEN d.country_id IN ('SG','') THEN 'Singapore' ELSE d.country_id END as [dst_country],
                    p.volumeL as [vol_l],
                    p.volumeW as [vol_w],
                    p.volumeH as [vol_h],
                    p.weight as [weight],
                    CAST(p.delivery_date as date) as [delivr_date],
                    CASE p.delivery_window WHEN -3 THEN '1800'
										   WHEN -2 THEN '0900'
										   WHEN -1 THEN '2201'
										   WHEN 0 THEN '0900'
										   WHEN 1 THEN '1200'
										   WHEN 2 THEN '1500'
										   WHEN 3 THEN '1800' END as [delivr_time],
                    CASE WHEN EXISTS(SELECT sms_invoice_item.loc_id FROM b2b_parcel_item,sms_invoice_item 
							WHERE sms_invoice_item.invoice_id=b2b_parcel_item.invoice_id
								AND sms_invoice_item.item_id=b2b_parcel_item.item_id
								AND b2b_parcel_item.invoice_id= p.invoice_id
								AND b2b_parcel_item.parcel_id= p.parcel_id 
						) 
						THEN (SELECT TOP 1 sms_invoice_item.loc_id FROM b2b_parcel_item,sms_invoice_item 
							WHERE sms_invoice_item.invoice_id=b2b_parcel_item.invoice_id
								AND sms_invoice_item.item_id=b2b_parcel_item.item_id
								AND b2b_parcel_item.invoice_id= p.invoice_id
								AND b2b_parcel_item.parcel_id= p.parcel_id ) 
						ELSE i.loc_id END as loc_id
                from b2b_parcel_list p
                left join sms_invoice_list i on i.invoice_id=p.invoice_id and i.coy_id=p.coy_id
                left join coy_address_book d on d.ref_id=i.invoice_id and d.addr_type=i.delv_addr
                where p.status_level =0 $sql_invoice 
                order by p.created_on desc";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    public function getLocation($coy_id,$loc_id) {
        $sql = "select 
                    l.loc_name as [src_company],
                    l.tel_code as [src_contact],
                    l.email_ims as [src_email],
                    d.addr_text as [src_addr],
                    d.street_line1 src_addr_line1, d.street_line2 src_addr_line2,
                    d.postal_code as [src_postcode], 
                CASE WHEN d.country_id='SG' THEN 'Singapore' ELSE d.country_id END as [src_country]
                from ims_location_list l
                left join coy_address_book d on d.ref_id=l.loc_id and d.coy_id=l.coy_id and d.ref_type='LOCATION'
                where l.coy_id='$coy_id' and l.loc_id='$loc_id'";
        $res = $this->db->query($sql)->row_array();
        if ($loc_id=='BF'){
            $res['src_email'] = 'bj.om@challenger.sg';
        }
        return $res;
    }

    public function saveGatewayLog($params){
        $coy_id='CTL';
        $gateway_id='RIVERWOOD';
        $gateway_code = md5($params['trans_id'].$params['loc_id'].$params['pos_id'].$params['buyer_code'].$params['tx_time']);
        $gateway_request = $params['trans_id'].'/'.$params['loc_id'].'/'.$params['buyer_code'].'?t='.$params['tx_time'] ;
        if ($params['status']=="NEW") {
            $sql = "INSERT INTO b2b_gateway_message 
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id,request_link,request_msg,status_level,created_by,created_on)
                        VALUES ('$coy_id','$gateway_id','".$gateway_code."','".trim($params['trans_id'])."','".trim($params['loc_id'])."','".trim($params['pos_id'])."' ,
                        '".trim($params['url'])."','".$params['request']."','0','".getRealIpAddr(15)."',GETDATE())";
            $this->db->query($sql);
        }
        else {
            $sql = "UPDATE b2b_gateway_message
                        SET status_level=1, return_status='".$params['status']."', return_msg='".str_replace("'",'',$params['response'])."'
                        WHERE coy_id='$coy_id' AND gateway_id='$gateway_id' AND gateway_code='$gateway_code' ";
            $this->db->query($sql);
        }
    }

    public function updateParcel($params){
        $sql = "UPDATE b2b_parcel_list
                    SET transaction_id='".$params['transaction_id']."', tracking_id='".$params['tracking_id']."', status_level='".$params['status_level']."',
                        remarks='".$params['remarks']."',quick_ref='".$params['quick_ref']."',
                        modified_by='Riverwood',modified_on=GETDATE()
                    WHERE coy_id='".$params['coy_id']."' AND invoice_id='".$params['invoice_id']."' AND parcel_id='".$params['parcel_id']."' AND delv_mode_id='".$params['delv_mode_id']."' ";
        $this->db->query($sql);
    }

    public function query($id=''){

        date_default_timezone_set('Asia/Singapore');

        $invoice_id = ($id=='') ? $this->input->get('invoice_id') : $id;
        $invoice = $this->getParcel($invoice_id);

        if (!$invoice){
            $return["status"] = 'FAILED';
            $return["message"] = 'No invoice found.';
            return $return;
        }

        foreach ($invoice as $i=>$inv){
            // Fix contact_no
            $inv_contact = str_replace(' ','',$inv['dst_contact']);
            $invoice[$i]['dst_contact'] = (substr($inv_contact,0,2)=='65' && strlen($inv_contact)<=8) ? '65'.$inv_contact : $inv_contact;
        }

        if (empty($invoice[0]['dst_contact']) || empty($invoice[0]['dst_postcode']) || empty($invoice[0]['dst_country'])){
            $return["status"] = 'FAILED';
            $return["message"] = 'Invoice found does not have full details (contact/postcode/country).';
            return $return;
        }
        else if (strlen($invoice[0]['dst_contact'])<8) {
            // Invalid phone
            $return["status"] = 'FAILED';
            $return["message"] = 'Please check the phone number, do not have 8 digits ('.$invoice[0]['dst_contact'].').';
            return $return;
        }
        else if ( !($this->input->get('delivr_date')) && strtotime($invoice[0]['delivr_date'])<=time()-86400 ){
            $return["status"] = 'FAILED';
            $return["message"] = 'Please check the delivery window ('.$invoice[0]['delivr_date'].').';
            return $return;
        }

        $loc_id = $invoice[0]['loc_id']; //($this->input->get('loc_id')) ? $this->input->get('loc_id') : 'BF';
        $loc_id2 = ($invoice[0]['loc_id']=='VEN-C' || $invoice[0]['loc_id']=='DSH-C') ? 'HCL' : $loc_id;
        $loc = $this->getLocation($invoice[0]['coy_id'],$loc_id2);

        $dates['pickup_date'] = ($this->input->get('pickup_date')) ? strtotime($this->input->get('pickup_date')) : strtotime('today 17:00');
        $dates['delivr_date'] = ($this->input->get('delivr_date')) ? strtotime($this->input->get('delivr_date')) : strtotime($invoice[0]['delivr_date'].' '.$invoice[0]['delivr_time']);


        $batchcode=1; $batchreturn = array(); $batchoutput = array(); $invoice_complete = array();
        foreach ($invoice as $key=>$invoice1) {

            /*
            if ( in_array($invoice1['invoice_id'],$invoice_complete) ) {
                return;
            }
            $invoice_complete[] = $invoice1['invoice_id'];
            */

            $tx_time = date("YmdHis") . '-' . $key;
            $query = $this->_doQuery($invoice, $loc, $dates, $key);

            $this->saveGatewayLog(array(
                "status" => 'NEW',
                "trans_id" => trim($invoice1['invoice_id']),
                "loc_id" => $loc_id,
                "pos_id" => '',
                "buyer_code" => $dates['pickup_date'] . '>' . $dates['delivr_date'],
                "url" => $this->apiurl,
                "tx_time" => $tx_time,
                "request" => urldecode($query),
                "response" => ''
            ));

            $output = $this->_doCurl($query);
            $response = json_decode($output,1);


            $response_trackingtest = $response['results'][0]['status'];
            if ($response_trackingtest=="ok") {
                $return["status"] = 'SUCCESS';
                $response_tracking = $response['results'][0]['do'];
                if ($response_tracking) {
                    $this->updateParcel(array(
                        "coy_id" => $invoice1['coy_id'],
                        "invoice_id" => $invoice1['invoice_id'],
                        "parcel_id" => $invoice1['parcel_id'],
                        "delv_mode_id" => $invoice1['delv_mode_id'],
                        "transaction_id" => $response_tracking,
                        "tracking_id" => $response_tracking,
                        "status_level" => 3,
                        "quick_ref" => substr(trim($invoice1['invoice_id']), -5),
                        "remarks" => 'SUCCESSFUL' //FAILED
                    ));
                }
                $return["results"] = array(
                    "invoice_id" => trim($invoice1['invoice_id']),
                    "parcel_id" => trim($invoice1['parcel_id']),
                    "tracking_id" => $response_tracking
                );
            } else {
                $batchcode = 0;
                $return["status"] = 'FAILED';
                $return["message"] = $response->error;
            }

            $this->saveGatewayLog(array(
                "status" => ($response_trackingtest) ? json_encode($return) : 'ERROR',
                "trans_id" => trim($invoice1['invoice_id']),
                "loc_id" => $loc_id,
                "pos_id" => '',
                "buyer_code" => $dates['pickup_date'] . '>' . $dates['delivr_date'],
                "url" => $this->apiurl,
                "tx_time" => $tx_time,
                "response" => $output
            ));

            $batchreturn[] = $return;
            $batchoutput[] = $output;

        }

        if ( $this->input->get('debug')==1 ) {
            echo '<hr>RESPONSES: ';
            print_r($batchoutput);
            exit;
        }

        return array(
            "status" => ($batchcode>0) ? "SUCCESS" : "FAILED",
            "results" => $batchreturn
        );
    }

    private function _doCurl($data_string) {

        $headers = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Content-length:'.strlen($data_string),
            'X-API-KEY: '.$this->token
        );

        $connection = curl_init();
        curl_setopt($connection, CURLOPT_URL, $this->apiurl . "deliveries/create.json");
        curl_setopt ($connection, CURLOPT_POST, true); //Mark requests as POST
        curl_setopt($connection, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($connection, CURLOPT_POSTFIELDS, $data_string); //Set the POST body
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($connection, CURLINFO_HEADER_OUT, 1); // enable tracking of request header for debugging purpose
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($connection);
        if ($response === false) {
            return array("error"=>curl_error($connection));
        }
        curl_close ($connection);


        if ( $this->input->get('debug')==1 ) {
            echo '<hr>URL: ';
            print_r($this->apiurl . "deliveries/create.json");
            //$headerSent = curl_getinfo($connection, CURLINFO_HEADER_OUT ); // request headers
            //print_r($headerSent);
            echo '<hr>HEADER: ';
            print_r($headers);
            echo '<hr>REQUEST: ';
            print_r($data_string);
            echo '<hr>RESPONSE: ';
            print_r($response);
        }

        return $response; //Get Resposne as JSON Array
    }

    private function _doQuery($invoices,$loc,$dates,$key){
        $invoice = $invoices[$key];
        $data = array(
            array(
                "date" => date('Y-m-d', $dates['delivr_date']),
                "do" => trim($invoice['external_track_no']), //substr(trim($invoice['invoice_id']), -4) . trim($invoice['parcel_id']) . '-' . date('His'),
                "track_no"=> trim($invoice['invoice_id']),
                "address" => $invoice['dst_addr_line1'] . ' ' . $invoice['dst_addr_line2'] . ' ' . $invoice['dst_country'] . ' ' . $invoice['dst_postcode'],
                "delivery_time" => $invoice['delivr_date'] . ' ' . $invoice['delivr_time'],
                "deliver_to" => $invoice['dst_name'],
                "phone" => $invoice['dst_contact'],
                "notify_email" => $invoice['dst_email'],
                "items" => [array("sku" => 'CHALLENGER', "desc" => 'Challenger/Hachi - ' . $invoice['invoice_id'], "qty" => 1)],
                "group_name" => 'CTL-'. trim($invoice['loc_id']),
                "start_date" => date('Y-m-d'),
                "wt" => $invoice['weight'],
                "parcel_height" => $invoice['vol_h'],
                "parcel_length" => $invoice['vol_l'],
                "parcel_width" => $invoice['vol_w']
            )
        );
        return json_encode($data);
    }

}
?>