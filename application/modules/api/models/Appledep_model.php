<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

!defined('BASEPATH') OR ('No direct script access allowed');

class Appledep_model extends Base_Common_Model {

    private $apiUrl;
    private $shipTo;
    private $ctlDep;
    private $receipients;

    // http://chvoices-test.challenger.sg/api/appledep/XXXX

    /***
     * (1) Customer DEP number must be input in "Tax-Reg Code" of SMS customer master (Format: DEP-XXXXXX)
     * (2) Invoice must be from IC/IB (CR/BR for refund).
     * (3) Item must be from acceptable categories, lot_id must be scanned in.
     * (4) Daily job will take valid, unprocessed, past 7 days invoices to submit.
     */

    public function __construct()
    {
        parent::__construct();

        if (ENVIRONMENT=="production") {
            $this->apiUrl = 'https://api-applecareconnect2.apple.com/enroll-service/1.0';
            $this->shipTo = '0000798057';
            $this->ctlDep = '1CE8DFF0';
            $this->receipients = 'loke.karfatt@challenger.sg;';
        }
        else {
//            $this->apiUrl = 'https://acc-ipt.apple.com/enroll-service/1.0';
            $this->apiUrl = 'https://api-applecareconnect-ept2.apple.com/enroll-service/1.0';
            $this->shipTo = '0000798057';
            $this->ctlDep = 'CF18220';
            $this->receipients = 'yongsheng@challenger.sg;';
        }
    }

    public function process($id = '', $mode = ''){

        ini_set('max_execution_time', '300');

        //1- Get orders with DEP to process
        if ($id!='') {
            if ($mode != '' && $mode == 'show'){
                return $this->show_perinvoice($id, $mode);
            }
            $r = $this->process_perinvoice($id, $mode);
        }
        else {
            $invoices = $this->querySQL();

            $r_inv = array();
            foreach ($invoices as $id){
                $r_inv[] = $this->process_perinvoice($id['invoice_id'], $mode);
            }

            $r = array("total_invoices"=>count($invoices),"invoice"=>$r_inv);
        }
        return $r;

    }

    public function process_perinvoice($id, $mode){

        $invoice = $this->querySQL($id);

        $unprocessed=0;
        foreach ($invoice as $d1) {
            if ($d1['string_udf2']=='')
                $unprocessed++;
        }
        if ($unprocessed>0 && $mode == '') {

            $request = $this->_buildRequest($invoice, $mode);
            $response = $this->_acc_credentials('bulk-enroll-devices', $request, $logging = array('trans_id' => $invoice[0]['invoice_id']));

            if ($response['status_code'] == 200) {

                $tx = 'DEP' . time();
                $enrollment = $response['message']['deviceEnrollmentTransactionId'];
                $dep_status = 'PENDING';

                foreach ($invoice as $d1) {
                    $this->save_log($tx, $enrollment, $d1);
                    $this->updTxnSuccess($d1['sys_id'], $d1['invoice_id'], $d1['line_num'], $tx, $dep_status);
                }

                $rtn = array(
                    "status" => 'ENROLLMENT',
                    "status_code" => 200,
                    "invoice" => $id,
                    "message" => $response["message"]["enrollDevicesResponse"]["statusMessage"]
                );
                $this->_doSuccessmail($invoice, $rtn);

            } else {
                $rtn = array(
                    "status" => 'BAD REQUEST',
                    "status_code" => 400,
                    "invoice" => $id,
                    "message" => $response["message"]["error_message"]
                );
                $this->_doErrormail($invoice, $rtn);
            }

        }
        else if ($mode == '' && count($invoice)>0){
            // Get enrollment status
            $rtn = array(
                "status" => 'CHECK',
                "status_code" => 200,
                "invoice" => $id,
                "message" => $this->check_perinvoice($invoice)
            );
        }
        else if ( $mode == 'OV' ){
            $request = $this->_buildRequest($invoice, $mode);
            $response = $this->_acc_credentials('bulk-enroll-devices', $request, $logging = array('trans_id' => $invoice[0]['invoice_id']));

            if ($response['status_code'] == 200) {

                $tx = 'DEP' . time();
                $enrollment = $response['message']['deviceEnrollmentTransactionId'];
                $dep_status = 'PENDING';

                foreach ($invoice as $d1) {
                    $this->save_log($tx, $enrollment, $d1);
                    $this->updTxnSuccess($d1['sys_id'], $d1['invoice_id'], $d1['line_num'], $tx, $dep_status);
                }

                $rtn = array(
                    "status" => 'ENROLLMENT',
                    "status_code" => 200,
                    "invoice" => $id,
                    "message" => $response["message"]["enrollDevicesResponse"]["statusMessage"]
                );
                $this->_doSuccessmail($invoice, $rtn);

            } else {
                $rtn = array(
                    "status" => 'BAD REQUEST',
                    "status_code" => 400,
                    "invoice" => $id,
                    "message" => $response["message"]["error_message"]
                );
                $this->_doErrormail($invoice, $rtn);
            }
        }
        else if ( $mode == 'VD' ){
            $request = $this->_buildRequest($invoice, $mode);
            $response = $this->_acc_credentials('bulk-enroll-devices', $request, $logging = array('trans_id' => $invoice[0]['invoice_id']));

            if ($response['status_code'] == 200) {

                $tx = 'DEP' . time();
                $enrollment = $response['message']['deviceEnrollmentTransactionId'];
                $dep_status = 'PENDING';

                foreach ($invoice as $d1) {
                    $this->save_log($tx, $enrollment, $d1);
                    $this->updTxnSuccess($d1['sys_id'], $d1['invoice_id'], $d1['line_num'], $tx, $dep_status);
                }

                $rtn = array(
                    "status" => 'ENROLLMENT',
                    "status_code" => 200,
                    "invoice" => $id,
                    "message" => $response["message"]["enrollDevicesResponse"]["statusMessage"]
                );
                $this->_doSuccessmail($invoice, $rtn);

            } else {
                $rtn = array(
                    "status" => 'BAD REQUEST',
                    "status_code" => 400,
                    "invoice" => $id,
                    "message" => $response["message"]["error_message"]
                );
                $this->_doErrormail($invoice, $rtn);
            }
        }
        else {
            $rtn = array(
                "status" => 'NO CONTENT',
                "status_code" => 204,
                "invoice" => $id,
                "message" => "No valid items found"
            );
        }

        return $rtn;
    }

    public function show_perinvoice($id)
    {
        $request = array(
            'requestContext' => array('shipTo' => $this->shipTo, "langCode" => 'en', "timeZone" => '-480'),
            'depResellerId' => $this->ctlDep,
            'orderNumbers' => $id
        );
        $response = $this->_acc_credentials('show-order-details', $request, $logging = array('trans_id' => $id));
        return $response;
    }

    public function check_perinvoice($invoice){

        $trans = [];
        foreach ($invoice as $d1) {
            if (!in_array($d1['string_udf2'],$trans))
                $trans[] = $d1['string_udf2'];
        }

        $depitems = [];
        foreach ($trans as $tran) {
            $sql = "select distinct string_udf1 from b2b_gateway_lot where coy_id='CTL' and gateway_id='APPLEDEP' and trans_id='$tran'; ";
            $results = $this->db->query($sql)->result_array();

            foreach ($results as $result) {
                $request = array(
                    'requestContext' => array('shipTo' => $this->shipTo, "langCode" => 'en', "timeZone" => '-480'),
                    'depResellerId' => $this->ctlDep,
                    'deviceEnrollmentTransactionId' => $result['string_udf1']
                );
                $response = $this->_acc_credentials('check-transaction-status', $request, $logging = array('trans_id' => $invoice[0]['invoice_id']));
                if ($response['status_code']==200 ) {
                    if ($response['message']['statusCode'] !== 'ERROR') {
                        foreach ($response['message']['orders'] as $d2) {
                            foreach ($d2['deliveries'] as $d3) {
                                foreach ($d3['devices'] as $d4) {
                                    $depitems[$d4['deviceId']] = $d4['devicePostStatus'];
                                }
                            }
                        }
                    } else {
                        return array(
                            "enrollment_transaction" => $tran,
                            "status_code" => 'ERROR',
                            "message" => isset($response['message']['orders'][0]['orderPostStatusMessage']) ? $response['message']['orders'][0]['orderPostStatusMessage'] : $response['message']['orders'][0]['deliveries']['deliveryPostStatusMessage']
                        );
                    }
                }
            }
        }

        foreach ($invoice as $d1) {
            $dep_status = (isset($depitems[$d1['lot_id']])) ? $depitems[$d1['lot_id']] : "PENDING";
            $rtn_items[] = array(
                'item_id' => $d1['item_id'],
                'lot_id' => $d1['lot_id'],
                'dep_status' => $dep_status
            );
            $this->updTxnSuccess($d1['sys_id'], $d1['invoice_id'], $d1['line_num'], str_replace('"', '', implode(',', $trans)), $dep_status);
        }
        $rtn = array(
            'enrollment_transaction' => $trans,
            'item_status' => $rtn_items
        );
        return $rtn;
    }

    public function querySQL($id=''){

        $query = "from sms_invoice_list l
                join sms_invoice_item i on i.coy_id=l.coy_id and i.invoice_id=l.invoice_id and i.status_level>=0
                join sms_invoice_lot o on o.coy_id=i.coy_id and o.invoice_id=i.invoice_id and o.line_num=i.line_num
                join sms_customer_list c on c.coy_id=l.coy_id and c.cust_id=l.cust_id
                where l.coy_id='CTL' and inv_type in ('IC','IB','BR','CR','ID','RF') and left(c.tax_reg_code,3)='DEP' and l.status_level>=0 
                  and i.item_id in (select item_id from ims_item_list where 
                  inv_dim3 in ('APPLE-IPHONE','IPAD CELLULAR','IPAD WIFI',
                    'MAC SYS-IMAC','MAC SYS-MAC MINI','MAC SYS-MAC PRO','MAC SYS-MACBOOK','MAC SYS-MB AIR','MAC SYS-MB PRO','MAC SYS-MBP RETINA')
                  )";

        if ($id=='') {
            $sql = "select DISTINCT rtrim(l.invoice_id) invoice_id 
                  $query and o.string_udf2='' and (l.invoice_date >= now() + interval '-7 day') 
                  order by invoice_id ";
        }
        else {
            $sql = "select 'SMS' sys_id, rtrim(l.invoice_id) invoice_id,
                    l.invoice_date AT time zone 'Asia/Singapore' AT TIME ZONE 'UTC' invoice_date,
                    case when inv_type in ('CR','BR','RF') then 'RE' else 'OR' end cust_ord_type,
                    NULLIF(regexp_replace(c.tax_reg_code, '\D','','g'), '') cust_dep_id,
                    concat_ws(rtrim(l.invoice_id), '-1') delv_num, 
                    (l.invoice_date + (interval '1 second')) AT time zone 'Asia/Singapore' AT TIME ZONE 'UTC' delv_date,
                    rtrim(i.item_id) item_id, i.line_num, o.lot_line_num, rtrim(o.lot_id) lot_id, rtrim(o.string_udf2) string_udf2
                $query and l.invoice_id='$id' 
                order by l.invoice_date,l.invoice_id,i.line_num; ";
        }

        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    public function _buildRequest($data, $mode){

        $r_item = array();
        foreach($data as $d1){
            $r_item[] = array('deviceId'=>$d1['lot_id']);
        }

        $inv_date = strtotime($data[0]['invoice_date']);
        $delv_date = strtotime($data[0]['delv_date']);
//        if (date('Ymd',$inv_date)==date('Ymd',$delv_date)) {
//            $delv_date += 60;
//        }
        $r = array(
            'requestContext' => array('shipTo' => $this->shipTo, "langCode" => 'en', "timeZone" => '-480'),
            'depResellerId' => $this->ctlDep,
            'transactionId' => $data[0]['invoice_id'],
            'orders' => array(
                array(
                    'orderNumber' => $data[0]['invoice_id'],
                    'orderDate' => date('Y-m-d\TH:i:s\Z',$inv_date), //'2014-08-28T10:10:10Z',
                    'orderType' => $mode ? $mode : $data[0]['cust_ord_type'],
                    'customerId' => $data[0]['cust_dep_id'],
                    'deliveries' => ($mode !== 'VD') ? array(
                        array(
                            'deliveryNumber' => $data[0]['delv_num'],
                            'shipDate' => date('Y-m-d\TH:i:s\Z',$delv_date),
                            'devices' => $r_item
                        )
                    ) : [],
                ),
            ),
        );
        return ($r);
    }

    private function save_log($tx,$enrollment,$item){
        $ins["coy_id"] = 'CTL';
        $ins["gateway_id"] = 'APPLEDEP';
        $ins["loc_id"] = '';
        $ins['trans_id'] = $tx;
        $ins['lot_id'] = $item['invoice_id'].'-'.$item['line_num'].'-'.$item['lot_line_num'].'-'.date('ymdHi');
        $ins['lot_id2'] = $item['lot_id'];
        $ins['unit_price'] = 0;
        $ins['string_udf1'] = $enrollment;
        $ins['string_udf2'] = '';
        $ins['string_udf3'] = '';
        $ins['datetime_udf'] = date('Y-m-d H:i:s');
        $ins['gateway_meta'] = '';
        $ins['gateway_num'] = 0;
        $ins["created_by"] = isset($item["created_by"]) ? $item["created_by"] : 'HACHI';
        $ins["created_on"] = date('Y-m-d H:i:s');
        $ins["modified_by"] = isset($item["created_by"]) ? $item["created_by"] : 'HACHI';
        $ins["modified_on"] = date('Y-m-d H:i:s');
        $result1 = $this->db->insert('b2b_gateway_lot', $ins);
    }

    private function updTxnSuccess($sys_id,$trans_id,$line_num,$tx,$dep_status) {
        if ($sys_id=='SMS') {
            $sql = "UPDATE sms_invoice_lot SET string_udf2='$tx',datetime_udf2=now(),string_udf3='$dep_status' WHERE coy_id='CTL' and invoice_id='$trans_id' and line_num=$line_num ";
            $query = $this->db->query($sql);
        }
        if ($sys_id=='POS') {
            $sql = "UPDATE pos_transaction_lot SET string_udf2='$tx',datetime_udf2=now(),string_udf3='$dep_status' WHERE coy_id='CTL' and trans_id='$trans_id' and line_num=$line_num ";
            $query = $this->db->query($sql);
        }
    }

    /**
     * @param $json_array String variable to be validated if it is JSON
     * @return bool True if json_array is valid, False if not
     */
    public function _is_json($json_array)
    {
        json_decode($json_array);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @param $full_response JSON response from the DEP API
     * @return mixed Full API response and any API errors and their messages
     */
    public function _response_handler($full_response)
    {
        $valid_return = $this->_is_json($full_response);

        if ($valid_return) {
            $json_response = json_decode($full_response, true);

            // Verify/Create/Cancel Order Error
            if (isset($json_response['orderErrorResponse'])) {
                $api_errors = $json_response['orderErrorResponse'];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Device Enrollment Error
            elseif (isset($json_response["orderDetailsResponses"]["deviceEligibility"]["deviceErrorResponse"])) {
                $api_errors = $json_response["orderDetailsResponses"]["deviceEligibility"]["deviceErrorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // POC Content Error
            elseif (isset($json_response["pocErrorResponse"])) {
                $api_errors = $json_response["pocErrorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Device Configuration Error
            elseif (isset($json_response["deviceConfigErrorResponse"])) {
                $api_errors = $json_response["deviceConfigErrorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Failed Auth Error
            elseif (isset($json_response["failedAuthErrorResponse"])) {
                $api_errors = $json_response["failedAuthErrorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Consolidated POC Error
            elseif (isset($json_response["errorResponse"])) {
                $api_errors = $json_response["errorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Ship-to Error
            elseif (isset($json_response['error_code'])) {
                $api_errors = $json_response["error_code"];
                $error['error_code'] = $api_errors['errorCode'];
                $error['error_message'] = $api_errors['errorMessage'];
            } // checkTransactionErrorResponse
            elseif (isset($json_response['checkTransactionErrorResponse'])) {
                $api_errors = $json_response["checkTransactionErrorResponse"][0];
                $error['error_code'] = $api_errors['errorCode'];
                $error['error_message'] = $api_errors['errorMessage'];
            } // Other Errors
            elseif (isset($json_response['errorCode'])) {
                $error['error_code'] = $json_response['errorCode'];
                $error['error_message'] = $json_response['errorMessage'];
            } // No Error
            else {
                return array("json_response"=>$json_response, "error"=> '');
            }
        } else {
            $error['error_code'] = 'ACC_ERR_0001';
            $error['error_message'] = 'JSON is invalid - Inspect full response for errors';
        }
        return array("json_response"=>$json_response, "error"=> $error);
    }

    /**
     * Usage Defines the AppleCare Settings for the API calls
     * @param $type Options: create_order, cancel_order or verify_order
     * @param $vars Request date
     * @param int $second
     * @return Reposible Data
     */
    public function _acc_credentials($type, $vars, $logging, $second=300){

        $post_url = $this->apiUrl . '/' . $type;
        $headers[] = "Content-Type:application/json";
        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_VERBOSE, 1);
//        curl_setopt($ch, CURLOPT_STDERR, fopen(getcwd().'/assets/errorlog.txt', 'w'));
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
//        curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $post_url);
//        curl_setopt($ch, CURLOPT_SSLVERSION,6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'/www/appledep20/GRX-0000798057.pem');
        curl_setopt($ch, CURLOPT_SSLKEY, getcwd().'/www/appledep20/privatekeynopass.pem');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($vars));

        $data = curl_exec($ch);
        curl_close($ch);

        if ($this->input->get('debug')==1) {
            echo file_get_contents(getcwd().'/assets/errorlog.txt');
            echo '

';
            echo $data;
            echo '

';
            print_r($this->_response_handler($data));
        }

        if ($data) {

            $res = $this->_response_handler($data);
            if (!is_array($res['error']) && $res['error']=='') {
                $rr= array(
                    "status" => $type . ' successful',
                    "status_code" => 200,
                    "url" => $post_url,
                    "message" => $res['json_response']
                );
            } else {
                $rr= array(
                    "status" => $type . ' failed',
                    "status_code" => 400,
                    "url" => $post_url,
                    "message" => $res['error']
                );
            }

            $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('CTL','APPLEDEP','".md5('APPLEDEP'.time())."','".trim($logging['trans_id'])."','','' ,
                        '".$post_url."','".json_encode($vars)."','". $rr['status_code'] ."','". $rr['status'] ."','". $data ."','".getRealIpAddr(15)."',now())";
            $this->db->query($sql);
            return $rr;

        } else {
            return false;
        }
    }

    private function _doSuccessmail($invoice,$rtn) {

        if (ENVIRONMENT != 'production') {
            // If not production, do not trigger email
            //return ;
        }

        $msg_item = '';
        foreach ($invoice as $i)
            $msg_item .= "<br>".trim($i['item_id'])." / ".trim($i['lot_id']);

        $subj = 'Apple Device Enrollment - ' . $invoice[0]['invoice_id'];
        $msg = "Trans ID: ".trim($invoice[0]['sys_id'])." ".trim($invoice[0]['invoice_id'])."<br>Item/SN: ".$msg_item;
        $msg.= "<br><br>".$rtn["message"];
        $email = 'yongsheng@challenger.sg;'; //'MIS.Team@challenger.sg;';
        $this->cherps_email($subj,$msg,$email);
    }

    private function _doErrormail($invoice,$rtn) {

        if (ENVIRONMENT != 'production') {
            // If not production, do not trigger email
            return ;
        }

        $msg_item = '';
        foreach ($invoice as $i)
            $msg_item .= "<br>".trim($i['item_id'])." / ".trim($i['lot_id']);

        $subj = 'Apple Device Enrollment ERROR - ' . $invoice[0]['invoice_id'];
        $msg = "Trans ID: ".trim($invoice[0]['sys_id'])." ".trim($invoice[0]['invoice_id'])."<br>Item/SN: ".$msg_item;
        $msg.= "<br><br>".$rtn["message"];
        $email = $this->receipients;
        $this->cherps_email($subj,$msg,$email);
    }

    private function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {

        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = 'li.liangze@challenger.sg; yongsheng@challenger.sg; ';
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }

}

?>