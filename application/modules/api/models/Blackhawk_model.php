<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Blackhawk_model extends Base_Common_Model {

    /*  CHECKING URL
     *      Recommended to prepare transactions for test. If need customized testing, can overwrite using following parameters:
     *      http://chvoices-test.challenger.sg/api/blackhawk/GHxHIA04148?debug=1&primaryAccountNumber=9877890000000059&donotsave=1&productId=505164400264&referenceTransactionId=H4501AX1463RZ4K5W9LNCVL0YM
     */

    private $apiurl;
    private $notification;

    private $default_acct;
    private $default_onlineitem;

    private $batch_id;
    private $batch_count;
    private $debugger;

    private $hachitriggerurl;
    private $activaterely;

    public function __construct() {
        parent::__construct();

        $this->activaterely = 0; // Set to '1' to send call via rely at 192.168.10.15:88/blackhawk.php
        $this->batch_count = 0;
        $this->batch_id = $this->getRefId('iTuneBatch');
        $this->debugger = array();
        $this->apiurl = '';
        if (ENVIRONMENT=="production" || $this->input->get('env')=="production" ) {

            $sql = "select remarks from b2b_setting_list where sys_id='B2B' and set_code='BLACKHAWK' and set_default='LIVE' order by modified_on desc limit 1";
            $url = $this->db->query($sql)->row_array();
            $this->apiurl = $url['remarks'];

            if ($this->apiurl=='') {
                $env = ($this->input->get('env')) ? $this->input->get('env') : 'blastapp'; // blastapp (DFW), webpos (SC)
                if ($env == 'blastapp')
                    $this->apiurl = 'https://blastapp.blackhawk-net.com:8443/transactionManagement/v2/transaction';
                else
                    $this->apiurl = 'https://webpos.blackhawk-net.com:8443/transactionManagement/v2/transaction';
            }

            $this->default_onlineitem = '505164405245';
            $this->default_acct = '6039534201000000024';
            $this->notification = 'MIS.Team@challenger.sg; ';

            $this->hachitriggerurl = 'https://www.hachi.tech/api/vc2/emailgplay';

            // New variable card
            $this->default_onlineitem = '505164408137';
            $this->default_acct = '6039534201000000066';
        }
        else {
            $this->apiurl = 'https://blast.preprod.blackhawk-net.com:8443/transactionManagement/v2/transaction';
            $this->default_onlineitem = '505164408137';
            $this->default_acct = '9877890000000000';
            $this->notification = 'yongsheng@challenger.sg; ';

            $this->hachitriggerurl = 'https://web3.sghachi.com/api/vc2/emailgplay';

            // Overwrite for live testing
//            $this->activaterely = 1;
//            $this->apiurl = 'https://webpos.blackhawk-net.com:8443/transactionManagement/v2/transaction';
//            $this->default_onlineitem = '505164405245';
//            $this->default_acct = '6039534201000000024';
        }
    }

    public function process($id=''){

//        if (!challengerIpAddr()) {
//            return array(
//                "code" => '-1',
//                "messages" => 'Unauthorized access location. Please make sure you are at a whitelisted IP location.'
//            );
//        }

        ini_set('max_execution_time', '300');

        if ($id=='network') {
            // Do network test
            return $this->processNetworktest();
        }
        if ($id=='check') {
            // Check status of Txn
            return $this->checkTx();
        }
        if ($id=='void-hi') {
            // Check status of Txn
            return $this->voidTx();
        }

        $return = array();
        $return_stats = array("Received"=>0, "Processed OK"=>0, "Processed Fail"=>0, "No Process"=>0);

        // 1. Get records from DB
        $records = $this->getPending($id);
        //$this->logwritegw( array("gateway_id"=>'BHK-PRECALL',"trans_id"=>$id,"request_link"=>'api/blackhawk/process',"request_msg"=>json_encode($records)) );
        $return_stats['Received'] = count($records);
        $this->debugger[] = 'Found '.count($records).' pending records.';

        $recamounts = array();
        foreach ($records as $rec) {
            $processOne = $this->processOne($rec);
            $return[] = $processOne;
            if ($processOne['p1_status'] == "1") {
                $return_stats['Processed OK'] ++;

                if ($rec['sys_id']=='SMS'){
                    // Call HACHI
                    $hachi = $this->HachiTriggerEmail($rec['trans_id'], $rec['email_addr'], $rec['regular_price'], $processOne['card_no']);
                }
            }
            else if ($processOne['p1_status'] == "-1") {
                $return_stats['Processed Fail'] ++;
            }
            else {
                $return_stats['No Process'] ++;
            }

            if ($rec['fraud_alert']=='Y') {
                $recamounts[$rec['trans_id']]['subtotal_amt'] += $rec['regular_price']; //$rec['trans_qty'] * $rec['regular_price'];
                $recamounts[$rec['trans_id']]['loc_id'] = $rec['loc_id'];
                $recamounts[$rec['trans_id']]['trans_id'] = $rec['trans_id'];
                $recamounts[$rec['trans_id']]['sys_id'] = $rec['sys_id'];
                $recamounts[$rec['trans_id']]['process_cards'][] = array(
                    'item_desc' => trim($rec['item_desc']),
                    'card_no' => trim($processOne['card_no']),
                    'amount' => $rec['trans_qty'] * $rec['regular_price'],
                    'unit_price' => $rec['regular_price']
                );
            }
        }

        if ($this->input->get('debug')) {
            foreach($this->debugger as $l)
                echo "<br> - " . $l;
            echo "<hr>";
        }

        if (ENVIRONMENT=="production") {
            foreach ($recamounts as $recamt) {
                if ($recamt['subtotal_amt']>=500) {
                    // Alert for tx more than $500
                    $addemail = trim($recamt['loc_id']) .'.ic@challenger.sg; HQ.CustServiceGroup@challenger.sg; ';
                    $subj = 'Warning - Gift Card Activation over $500 [' . trim($recamt['trans_id']) . ']';
                    $msg = "Trans ID: ".trim($recamt['sys_id'])." ".$recamt['trans_id'];
                    $msg.= "<br>Total Amount: ".$recamt['subtotal_amt'];
                    $msg.= "<br><br>FYI, this transaction has total value of more than $500 of Giftcards.";
                    $msg.= "<br><br>Items:";
                    foreach ($recamt['process_cards'] as $pcard){
                        $msg.= "<br>- " . $pcard['item_desc'] . " (" . $pcard['card_no'] . ") $" . $pcard['unit_price'];
                    }
                    $email = $this->notification . ' ' . $addemail ;
                    $this->cherps_email($subj,$msg,$email);
                }
            }
        }

        return array(
            "code" => ($return_stats['Processed Fail']>0) ? '0' : '1',
            "stats" => $return_stats,
            "messages" => $return
        );
    }

    public function voidTx(){

        $txn = $this->input->get("trans_id");
        $lnn = ($this->input->get("line_num")) ? " AND sms_invoice_item.line_num=" . $this->input->get("line_num") : '' ;

        $sql = "SELECT 
                    'SMS' as sys_id,
                    concat('000',LEFT(ims_location_list.fin_dim1,2),'     000   ') loc_pos_ref,
                    sms_invoice_list.loc_id, ims_location_list.fin_dim1, '' as pos_id, 
                    sms_invoice_item.item_id, sms_invoice_item.item_desc, sms_invoice_item.qty_invoiced, 
                    sms_invoice_item.unit_price as regular_price, 
                    sms_invoice_item.coy_id, sms_invoice_item.invoice_id, sms_invoice_item.line_num, coalesce(sms_invoice_lot.lot_line_num,0) lot_line_num, 
                    sms_invoice_lot.lot_id, 
                      sms_invoice_lot.lot_id2, 
						NULL as status_level , sms_invoice_lot.string_udf1, sms_invoice_lot.string_udf2, sms_invoice_lot.string_udf3, sms_invoice_lot.datetime_udf1, sms_invoice_lot.datetime_udf2, sms_invoice_lot.datetime_udf3,   
                    sms_invoice_item.created_by, sms_invoice_item.created_on, sms_invoice_item.modified_by, sms_invoice_item.modified_on,
					sms_invoice_list.email_addr,
					(case when ims_item_list.inv_dim4 in ('OFFICE CARD') then 'N' else 'Y' end) fraud_alert
                FROM sms_invoice_item
                    JOIN sms_invoice_list ON sms_invoice_list.coy_id=sms_invoice_item.coy_id AND sms_invoice_list.invoice_id=sms_invoice_item.invoice_id
                    LEFT JOIN sms_invoice_lot ON sms_invoice_lot.coy_id=sms_invoice_item.coy_id AND sms_invoice_lot.invoice_id=sms_invoice_item.invoice_id AND sms_invoice_lot.line_num=sms_invoice_item.line_num
                    JOIN ims_item_list ON ims_item_list.coy_id=sms_invoice_item.coy_id AND ims_item_list.item_id=sms_invoice_item.item_id
                    JOIN ims_location_list ON ims_location_list.coy_id=sms_invoice_list.coy_id AND ims_location_list.loc_id=sms_invoice_list.loc_id   
                WHERE
                    sms_invoice_list.coy_id = 'CTL' AND sms_invoice_list.inv_type IN ('HI','HR') AND 
                        ims_item_list.item_type in ('G','H') AND 
                    sms_invoice_item.status_level > 0 AND sms_invoice_list.invoice_id='".strtoupper($txn)."' $lnn ";
        $res = $this->db->query($sql)->result_array();

        if (count($res)==1) {
            $rec = $res[0];

            $param['mode'] = '';
            $param['sys_id'] = $rec['sys_id'];
            $param['lot_id'] = $this->default_acct; //trim($rec['lot_id']);
            $param['item_id'] = substr(trim($rec['item_id']), 0, 12);
            $param['unit_price'] = $rec['regular_price'];
            $param['udf1'] = (strlen($rec['string_udf1']) > 1) ? $rec['string_udf1'] : $this->getRefId('iTunesRef'); //$retrievalReferenceNumber
            $param['loc_id'] = $rec['loc_id']; // Using loc_pos_ref
            $param['fin_dim'] = $rec['fin_dim1']; // Using loc_pos_ref
            $param['pos_id'] = $rec['pos_id']; // Using loc_pos_ref
            $param['loc_pos_ref'] = $rec['loc_pos_ref'];
            $param['created_on'] = strtotime($rec['created_on']);
            $trans['trans_id'] = strtoupper($txn); //trim($rec['trans_id']);
            $trans['lot_id'] = trim($rec['lot_id']);
            $param['mode'] = 'void';
            $param['lot_id2'] = $rec['lot_id2'];

            $query = $this->_doParams($param);

            $qcurl_uri = '';
            $qcurl = $this->_doCurl($qcurl_uri, $query, $trans);
            $reply = $this->_doReadcurl($qcurl_uri, $qcurl);

            // Alert for hard-void
            $subj = 'HACHI GIFTCARD VOID TXN - '.strtoupper($txn);
            $msg = "Trans ID: SMS ".strtoupper($txn);
            $msg.= "<br>Card No: ". trim($rec['lot_id']) ;
            $msg.= "<br>Amount: ". $rec['regular_price'] ;
            $msg.= "<br><br>" . json_encode($reply) ;
            $email = 'yongsheng@challenger.sg' ;
            $this->cherps_email($subj,$msg,$email);

        }
        else {
            $reply = array("error" => "More than 1 item in transaction");
        }

        echo json_encode($reply);
        exit;
        return true;
    }

    public function checkTx(){

        $txn = $this->input->get("trans_id");

        if ($this->input->get("reset")) {
            // Reset Tx if needed
            $line_num = $this->input->get("line_num");
            $lot_line_num = $this->input->get("lot_line_num");
            $sql = "UPDATE pos_transaction_lot SET status_level=0 WHERE trans_id='$txn' and line_num='$line_num' and lot_line_num='$lot_line_num'; ";
            $res = $this->db->query($sql);
        }

        $sql = "select
                 rtrim(i.trans_id) trans_id,
                 i.item_id,i.item_desc,i.trans_qty,
                 n.line_num,n.lot_line_num,n.status_level,n.lot_id,n.string_udf2,n.string_udf3,n.datetime_udf1,
                 (
                  select SUBSTRING(msg_response,position('responseCode' in msg_response),20)
                  from b2b_chitunes_message
                  where trans_id = i.trans_id
                  order by created_on desc limit 1
                 ) as partial_response
                from pos_transaction_item i
                join pos_transaction_lot n on n.trans_id=i.trans_id and n.line_num = i.line_num
                where i.trans_id = '".trim($txn)."' and i.status_level>=0
                 and i.item_id in (select item_id from ims_item_list where item_type='G') ";
        //echo "<pre>" . $sql;exit;
        $res = $this->db->query($sql)->result_array();

        foreach ($res as $k=>$r){
            if ( strpos($r['partial_response'],'00') != true ) {
                if ($r['status_level'] == "0")
                    $res[$k]['action'] = '<a href="' . base_url('api/blackhawk/check?trans_id=' . $r['trans_id']) .'">Activate in 1 min</a>';
                else
                    $res[$k]['action'] = '<a href="' . base_url('api/blackhawk/check?trans_id=' . $r['trans_id'] . '&reset=y&line_num=' . $r['line_num'] . '&lot_line_num=' . $r['lot_line_num']) . '">Reset Tx</a>';
            }
            $res[$k]['partial_response'] = str_replace('<','&lt;',$r['partial_response']);
        }

        if ($this->input->get("json")) {
            return $res;
        } else {
            echo "<pre>";
            print_r($res);
            exit;
        }
        return true;
    }

    public function processNetworktest(){
        $this->debugger[] = '-';
        $this->debugger[] = 'Perform for network test';

        $systemTraceAuditNumber = $this->getRefId('iTuneTrace');
        $transmissionDateTime = date('ymdHis');
        $query['request']['header']['signature'] = 'BHNUMS';
        $query['request']['header']['details']['productCategoryCode'] = '01';
        $query['request']['header']['details']['specVersion'] = '43';
        $query['request']['transaction']['transmissionDateTime'] = $transmissionDateTime; //-C
        $query['request']['transaction']['systemTraceAuditNumber'] = "$systemTraceAuditNumber"; //-X
        $query['request']['transaction']['networkManagementCode'] = "301"; //-C
        $trans['trans_id'] = 'NETWORK-'.rand(1111,9999);
        $qcurl = $this->_doCurl('network', $query,$trans);
        $resp = json_decode($qcurl);
        $code = $resp->response->transaction->responseCode;

        $this->debugger[] = 'First set params: ' . print_r($query,true);
        $this->debugger[] = 'First call to Blackhawk: ' . print_r($trans,true);
        $this->debugger[] = 'First reply from Blackhawk: ' . $qcurl;

        if ($this->input->get('debug')) {
            foreach($this->debugger as $l)
                echo "<br> - " . $l;
            echo "<hr>";
        }

        return array(
            "code" => $code,
            "stats" => ($code=="00") ? 'network test completed' : 'network test return error',
            "messages" => $qcurl
        );
    }

    public function processOne($rec){

        $this->debugger[] = '-';
        $this->debugger[] = 'Perform for tx: '.$rec['trans_id'].'.';

        $addemail = trim($rec['loc_id']) .'.ic@challenger.sg; ';

        if ( in_array($rec['string_udf2'], ['04','21'] ) ){
            // Already redeemed or Card already active
            $setlot["trans_id"] = $rec['trans_id'];
            $setlot["line_num"] = $rec['line_num'];
            $setlot["lot_line_num"] = $rec['lot_line_num'];
            $setlot["status_level"] = 2;
            $setlot["string_udf2"] = '';
            $setlot["string_udf3"] = ($rec['trans_qty']<0) ? 'Card redeemed' : 'Card activated';
            $this->setLotstatus($rec['sys_id'], $setlot);

            $this->debugger[] = 'This tx has response 04 or 21 in previous call. Updated as completed.';

            if (($rec['trans_qty']<0)) {
                // Send email - fail to void the card
                //$subj = 'iTunes VOID Error - ' . $rec['trans_id'];
                //$msg = "Trans ID: ".trim($rec['trans_id'])."<br>Card No: ".trim($rec['lot_id'])."<br><br>IMPORTANT! FAIL TO VOID - This card has been redeemed.<br>(Previous Msg)";
                //$this->cherps_email($subj,$msg,$this->notification);
                $this->_doErrormail($rec,$setlot,$reply,'FAIL TO VOID - This card has been redeemed.<br>(Previous Msg)',$addemail);

                return array(
                    "p1_status" => "-1",
                    "trans_ref" => trim($rec['trans_id']).'-'.$rec['line_num'].'-'.trim($rec['lot_line_num']),
                    "card_no" => trim($rec['lot_id']),
                    "code" => $rec['string_udf2'],
                    "txn" => '',
                    "message" => 'Card redeemed - fail to void card'
                );
            }
            else {
                // Update as success
                return array(
                    "p1_status" => "0",
                    "trans_ref" => trim($rec['trans_id']).'-'.$rec['line_num'].'-'.trim($rec['lot_line_num']),
                    "card_no" => trim($rec['lot_id']),
                    "code" => "00",
                    "txn" => '',
                    "message" => 'Card activated'
                );
            }
        }
        else {
            // Calling to Blackhawk
            $param['mode'] = '';
            $param['sys_id'] = $rec['sys_id'];
            $param['lot_id'] = trim($rec['lot_id']);
            $param['item_id'] = substr(trim($rec['item_id']),0,12);
            $param['unit_price'] = $rec['regular_price'];
            $param['udf1'] = (strlen($rec['string_udf1'])>1) ? $rec['string_udf1'] : $this->getRefId('iTunesRef'); //$retrievalReferenceNumber
            $param['loc_id'] = $rec['loc_id']; // Using loc_pos_ref
            $param['fin_dim'] = $rec['fin_dim1']; // Using loc_pos_ref
            $param['pos_id'] = $rec['pos_id']; // Using loc_pos_ref
            $param['loc_pos_ref'] = $rec['loc_pos_ref'];
            $param['created_on'] = strtotime($rec['created_on']);
            $trans['trans_id'] = trim($rec['trans_id']);
            $trans['lot_id'] = trim($rec['lot_id']);
            if ($rec['trans_qty']<0){
                // Void tx
                $param['mode'] = 'void';
                $param['lot_id2'] = $rec['lot_id2'];
            }
            $query = $this->_doParams($param);
            $qcurl_uri = '' ;
            $qcurl = $this->_doCurl($qcurl_uri, $query,$trans);
            $reply = $this->_doReadcurl($qcurl_uri, $qcurl);

            $this->debugger[] = 'First set params: ' . print_r($query,true);
            $this->debugger[] = 'First call to Blackhawk: ' . print_r($trans,true) . print_r($param,true);
            $this->debugger[] = 'First reply from Blackhawk: ' . $qcurl;

            if ($reply['p2_status']!="-2") {
                // Acceptable response. Save this response and quit.
                if ($reply['p2_status']>="0") {
                    // GOOD
                    $setlot["trans_id"] = $rec['trans_id'];
                    $setlot["line_num"] = $rec['line_num'];
                    $setlot["lot_line_num"] = $rec['lot_line_num'];
                    $setlot["lot_id1"] = $reply['p2_acct'];
                    $setlot["lot_id2"] = $reply['p2_txn'];
                    $setlot["string_udf1"] = $param['udf1'];
                    $setlot["string_udf2"] = $reply['p2_code'];
                    $setlot["string_udf3"] = ($rec['trans_qty']<0) ? 'Card voided' : 'Card activated';
                    $setlot["datetime_udf1"] = date('Y-m-d');
                    $setlot["status_level"] = 2;
                    $this->setLotstatus($rec['sys_id'], $setlot);

                    $this->debugger[] = 'First processing: ' . $setlot["string_udf3"];
                }
                else {
                    // ERROR
                    $setlot["trans_id"] = $rec['trans_id'];
                    $setlot["line_num"] = $rec['line_num'];
                    $setlot["lot_line_num"] = $rec['lot_line_num'];
                    $setlot["string_udf1"] = $param['udf1'];
                    $setlot["string_udf2"] = $reply['p2_code'];
                    $setlot["string_udf3"] = ($rec['trans_qty']<0) ? 'Void Error' : 'Activation Error';
                    $setlot["datetime_udf1"] = date('Y-m-d');
                    $setlot["status_level"] = 1;
                    $this->setLotstatus($rec['sys_id'], $setlot);

                    $this->debugger[] = 'First processing fail: ' . $setlot["string_udf3"];

                    // Send email - fail to the card
                    //$subj = 'iTunes Error - ' . $rec['trans_id'];
                    //$msg = "Trans ID: ".trim($rec['trans_id'])."<br>Card No: ".trim($rec['lot_id'])."<br><br>".$setlot["string_udf3"]."<br>Response: ".$reply['p2_code']." - ".$reply['p2_msg']."<br>(First Try)";
                    //$this->cherps_email($subj,$msg,$this->notification);
                    //$this->_doErrormail($rec,$setlot,$reply,'(First Try)',$addemail);
                }
            }
            else {
                // Unacceptable response (p2_status = -2)
                // Save failed results to pos_transaction_lot before allow retry.
                $setlot["trans_id"] = $rec['trans_id'];
                $setlot["line_num"] = $rec['line_num'];
                $setlot["lot_line_num"] = $rec['lot_line_num'];
                $setlot["datetime_udf1"] = date('Y-m-d'); //'GETDATE()';
                $setlot["string_udf1"] = $param['udf1'];
                $setlot["string_udf2"] = $reply['p2_code'];
                $setlot["string_udf3"] = ($reply['p2_status']>=0) ? (($rec['trans_qty']<0) ? 'Void Reversed' : 'Activate within 24h') : '' ;
                $this->setLotstatus($rec['sys_id'], $setlot);

                // Retry with SAF (p2_code: -4/74/15)
                $param['mode'] = '';
                $param['sys_id'] = $rec['sys_id'];
                $param['lot_id'] = trim($rec['lot_id']);
                $param['item_id'] = trim($rec['item_id']);
                $param['unit_price'] = $rec['regular_price'];
                $param['udf1'] = (strlen($param['udf1'])>1) ? $param['udf1'] : $this->getRefId('iTunesRef'); //$retrievalReferenceNumber
                $param['loc_id'] = $rec['loc_id']; // Using loc_pos_ref
                $param['fin_dim'] = $rec['fin_dim1']; // Using loc_pos_ref
                $param['pos_id'] = $rec['pos_id']; // Using loc_pos_ref
                $param['loc_pos_ref'] = $rec['loc_pos_ref'];
                $param['created_on'] = strtotime($rec['created_on']);
                $trans['trans_id'] = trim($rec['trans_id']);
                $trans['lot_id'] = trim($rec['lot_id']);
                if ($rec['trans_qty']<0){
                    // Void tx
                    $param['mode'] = 'void';
                    $param['lot_id2'] = $rec['lot_id2'];
                }
                $query = $this->_doParams($param);
                $qcurl_uri = ($param['sys_id']=='SMS' || $param['mode']=='void') ? 'reverse' : 'saf' ;
                $qcurl = $this->_doCurl($qcurl_uri, $query,$trans);
                $reply = $this->_doReadcurl($qcurl_uri, $qcurl);

                $this->debugger[] = '2nd set params: ' . print_r($query,true);
                $this->debugger[] = '2nd call to Blackhawk: ' . print_r($trans,true) . print_r($param,true);
                $this->debugger[] = '2nd reply from Blackhawk: ' . $qcurl;

                // Save retry record
                if ($reply['p2_status']>=0){
                    $setlot["trans_id"] = $rec['trans_id'];
                    $setlot["line_num"] = $rec['line_num'];
                    $setlot["lot_line_num"] = $rec['lot_line_num'];
                    $setlot["lot_id1"] = $reply['p2_acct'];
                    $setlot["lot_id2"] = $reply['p2_txn'];
                    $setlot["string_udf1"] = $param['udf1'];
                    $setlot["string_udf2"] = $reply['p2_code'];
                    $setlot["string_udf3"] = ($rec['trans_qty']<0) ? 'Void Reversed' : 'Activate within 24h';
                    $setlot["datetime_udf1"] = date('Y-m-d');
                    $setlot["status_level"] = 2;
                    $this->setLotstatus($rec['sys_id'], $setlot);

                    $this->debugger[] = '2nd processing: ' . $setlot["string_udf3"];
                }
                else {
                    $setlot["trans_id"] = $rec['trans_id'];
                    $setlot["line_num"] = $rec['line_num'];
                    $setlot["lot_line_num"] = $rec['lot_line_num'];
                    $setlot["string_udf1"] = $param['udf1'];
                    $setlot["string_udf2"] = $reply['p2_code'];
                    $setlot["string_udf3"] = ($rec['trans_qty']<0) ? 'Void Error' : 'Activation Error';
                    $setlot["datetime_udf1"] = date('Y-m-d');
                    $setlot["status_level"] = 1;
                    $this->setLotstatus($rec['sys_id'], $setlot);

                    $this->debugger[] = '2nd processing fail: ' . $setlot["string_udf3"];

                    // Send email - fail to the card
                    //$subj = 'iTunes Error - ' . $rec['trans_id'];
                    //$msg = "Trans ID: ".trim($rec['trans_id'])."<br>Card No: ".trim($rec['lot_id'])."<br><br>".$setlot["string_udf3"]."<br>Response: ".$reply['p2_code']." - ".$reply['p2_msg']."<br>(2nd Try)";
                    //$this->cherps_email($subj,$msg,$this->notification);
                    $this->_doErrormail($rec,$setlot,$reply,'(2nd Try)',$addemail);
                }
            }

            return array(
                "p1_status" => $reply['p2_status'],
                "trans_ref" => trim($rec['trans_id']).'-'.$rec['line_num'].'-'.trim($rec['lot_line_num']),
                "card_no" => $reply['p2_acct'],
                "code" => $reply['p2_code'],
                "txn" => $reply['p2_txn'],
                "message" => $reply['p2_msg']
            );
        }
    }

    private function _doReadcurl($qcurl_uri, $qcurl){
        $responsecodes = array(
            "-4" => 'Fail to get proper response from Blackhawk API',
            "-3" => 'Reversal Approved - Nothing has been done to transaction',
            "00" => 'Approved',
            "01" => 'Approved – balance unavailable',
            "03" => 'Approved – balance unavailable on external account number',
            "74" => 'Unable to route / System Error',
            "15" => 'Time Out occurred- Auth Server not available /responding',
            "02"=>"Refer to card issuer",
            "04"=>"Already Redeemed",
            "05"=>"Error account problem",
            "06"=>"Invalid expiration date",
            "07"=>"Unable to process",
            "08"=>"Card not found",
            "12"=>"Invalid transaction",
            "13"=>"Invalid amount",
            "14"=>"Invalid Product",
            "16"=>"Invalid status change",
            "17"=>"Invalid merchant",
            "18"=>"Invalid Phone Number",
            "20"=>"Invalid Pin",
            "21"=>"Card already active",
            "22"=>"Card Already Associated",
            "30"=>"Bad track2 – format error",
            "33"=>"Expired card",
            "34"=>"Already reversed",
            "35"=>"Already voided",
            "36"=>"Restricted card",
            "37"=>"Restricted External Account",
            "38"=>"Restricted Merchant",
            "41"=>"Lost card",
            "42"=>"Lost External Account",
            "43"=>"Stolen card",
            "44"=>"Stolen External Account",
            "51"=>"Insufficient funds",
            "54"=>"Expired External Account",
            "55"=>"Max recharge reached",
            "56"=>"Advance less amount / enter lesser amount",
            "58"=>"Request not permitted by merchant location",
            "59"=>"Request not permitted by processor",
            "61"=>"Exceeds withdrawal amt / over limit",
            "62"=>"Exceeds financial limit",
            "65"=>"Exceeds withdrawal frequency limit",
            "66"=>"Exceeds transaction count limit",
            "69"=>"Format error –bad data",
            "71"=>"Invalid External Account number",
            "94"=>"Duplicate transaction",
            "95"=>"Cannot Reverse the Original Transaction",
            "99"=>"General decline"
        );

        $resp = json_decode($qcurl);
        if ((array_key_exists($resp->response->transaction->responseCode,$responsecodes))){
            $code = $resp->response->transaction->responseCode;
            if ($code=="00"){
                $status = "1";
                if ($qcurl_uri=="reverse") {
                    $code = "-3";
                    $status = "-1";
                }
            }
            else if ($code=="01"||$code=="03"){
                $status = "0";
            }
            else if ($code=="74"||$code=="15"){
                $status = "-2"; // Go for retry with SAF
            }
            else {
                $status = "-1";
            }
        }
        else {
            $code = "-4";
            $status = "-2";
        }

        // Return code as status:
        //  0: No Process, 1: Processed OK, -1: Processed Fail, -2: Timeout/Error

        $return["p2_status"] = $status;
        $return["p2_code"] = $code;
        $return["p2_msg"] = $responsecodes[$code];
        $return["p2_acct"] = (!isset($resp->response->transaction->additionalTxnFields->redemptionAccountNumber)) ? $resp->response->transaction->primaryAccountNumber : $resp->response->transaction->additionalTxnFields->redemptionAccountNumber;
        $return["p2_txn"] = $resp->response->transaction->additionalTxnFields->transactionUniqueId;
        return $return;
    }

    private function _doCurl($uri, $query, $trans) {
        $uri = ($this->input->get('reverse')) ? 'reverse' : $uri;
        $uri = ($this->input->get('saf')) ? 'saf' : $uri;
        $url = ($uri=='') ? $this->apiurl : $this->apiurl .'/'. $uri;
        $query_json = json_encode($query);

        $headers[] = "Content-Type:application/json";
        $headers[] = "Accept:application/json";
        $ch = curl_init();
        //echo $url .' - '. $query_json; exit;

        if ($this->activaterely==1) {
            //Doing this to route via LIVE server
            $url = 'http://192.168.10.15:88/blackhawk.php?content=json&url=' . $url;
        }
        $this->debugger[] = '- Outgoing call: '.$url;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        curl_close($ch);


        // Save to DB
        $resp = json_decode($output);
        $code = $resp->response->transaction->responseCode;
        $lot_id = (!isset($resp->response->transaction->additionalTxnFields->redemptionAccountNumber)) ? $resp->response->transaction->primaryAccountNumber : $resp->response->transaction->additionalTxnFields->redemptionAccountNumber;
        $this->batch_count++;
        $sql = "INSERT INTO b2b_chitunes_message 
                    (coy_id,batch_id,line_num,trans_id,lot_id,url_link,msg_request,
                      resp_code,msg_response,created_by,created_on,modified_by,modified_on)
                  VALUES ('CTL','".$this->batch_id."','".$this->batch_count."','".$trans['trans_id']."','".$lot_id."','$url','$query_json',
                          '$code','$output','".getRealIpAddr(15)."', current_timestamp ,'".getRealIpAddr(15)."', current_timestamp)";
        $query = $this->db->query($sql);

        return $output;
    }

    private function _doParams($param){
        $arr['request']['header']['signature'] = 'BHNUMS';
        $arr['request']['header']['details']['productCategoryCode'] = '01';
        $arr['request']['header']['details']['specVersion'] = '43';

        if ($param['sys_id'] == 'SMS') {
            // Online tx
            $processingCode = ($param['mode'] == 'void' || $param['mode'] == 'saf-void') ? '775400' : '745400';
            $pointOfServiceEntryMode = '041';
            $productId = $this->default_onlineitem;// substr(trim($param['item_id']),0,7)=="9009111" ? $this->default_onlineitem : substr(trim($param['item_id']),0,12); // substr(trim($param['item_id']),0,12);
        }
        else {
            $processingCode = ($param['mode'] == 'void' || $param['mode'] == 'saf-void') ? '755400' : '725400';
            $pointOfServiceEntryMode = '031';
            $productId = substr(trim($param['item_id']),0,12);
        }

        $primaryAccountNumber = (strlen($param['lot_id'])>1) ? $param['lot_id'] : $this->default_acct;
        $transactionAmount = floor($param['unit_price'] * 100);
        $merchantTerminalId = $param['loc_pos_ref'];

        $retrievalReferenceNumber = (strlen($param['udf1'])>1) ? $param['udf1'] : $this->getRefId('iTunesRef');
        $systemTraceAuditNumber = $this->getRefId('iTuneTrace');

        $DateTime = $param['created_on'];
        $transmissionDateTime = date('ymdHis');
        $localTransactionTime = date('His',$DateTime);
        $localTransactionDate = date('ymd',$DateTime);

        $arr['request']['transaction']['primaryAccountNumber'] = $primaryAccountNumber;
        $arr['request']['transaction']['processingCode'] = $processingCode;
        $arr['request']['transaction']['transactionAmount'] = sprintf("%'012d",$transactionAmount); //-
        $arr['request']['transaction']['transmissionDateTime'] = $transmissionDateTime; //-C
        $arr['request']['transaction']['systemTraceAuditNumber'] = "$systemTraceAuditNumber"; //-X
        $arr['request']['transaction']['localTransactionTime'] = $localTransactionTime; //-C
        $arr['request']['transaction']['localTransactionDate'] = $localTransactionDate; //-C
        $arr['request']['transaction']['merchantCategoryCode'] = '4444';
        $arr['request']['transaction']['pointOfServiceEntryMode'] = $pointOfServiceEntryMode;
        $arr['request']['transaction']['acquiringInstitutionIdentifier'] = '60300003515';
        $arr['request']['transaction']['retrievalReferenceNumber'] = sprintf("%'012d",$retrievalReferenceNumber); //-X
        $arr['request']['transaction']['merchantTerminalId'] = $merchantTerminalId; //-
        $arr['request']['transaction']['merchantIdentifier'] = '60300003515    ';
        $arr['request']['transaction']['merchantLocation'] = 'CHALLENGER';
        $arr['request']['transaction']['transactionCurrencyCode'] = '702';
        $arr['request']['transaction']['additionalTxnFields']['productId'] = $productId; //-

        if ($param['mode'] == 'void' || $param['mode'] == 'saf-void') {
            if (strlen(trim($param['lot_id2']))>2)
                $arr['request']['transaction']['additionalTxnFields']['referenceTransactionId'] = trim($param['lot_id2']); //Void activation, this field can be empty. // $param['lot_id2'];
        }

        // Force replace of variables for checking
        if ($this->input->get('processingCode')) {
            $arr['request']['transaction']['processingCode'] = $this->input->get('processingCode');
        }
        if ($this->input->get('primaryAccountNumber')) {
            $arr['request']['transaction']['primaryAccountNumber'] = $this->input->get('primaryAccountNumber');
        }
        if ($this->input->get('productId')) {
            $arr['request']['transaction']['additionalTxnFields']['productId'] = $this->input->get('productId');
        }
        if ($this->input->get('referenceTransactionId')) {
            $arr['request']['transaction']['additionalTxnFields']['referenceTransactionId'] = $this->input->get('referenceTransactionId');
        }
        if ($this->input->get('retrievalReferenceNumber')) {
            $arr['request']['transaction']['retrievalReferenceNumber'] = $this->input->get('retrievalReferenceNumber');
        }
        if ($this->input->get('unit_price')) {
            $arr['request']['transaction']['transactionAmount'] = sprintf("%'012d",$this->input->get('unit_price'));
        }

        return $arr;
    }

    private function getRefId($trans_prefix){
        $sql = "select next_num from sys_trans_list where sys_id='ADS' and rtrim(trans_prefix)='$trans_prefix'";
        $res = $this->db->query($sql)->row_array();

        $sql = "update sys_trans_list set next_num = next_num + 1 where sys_id='ADS' and rtrim(trans_prefix)='$trans_prefix'";
        $this->db->query($sql);

        return $res['next_num'];
//        $sql = "EXEC [sp_SysGetNextDocId] '*','ADS','$trans_prefix','' ";
//        $res = $this->db->query($sql)->row_array();
//        return $res['doc_id'];
    }

    private function getPending($id='') {
        $sql_where_pos = ($id=='') ? " AND pos_transaction_item.created_on < '" . date('Y-m-d H:i:s', strtotime('5 min ago') ) . "' " : " AND pos_transaction_item.trans_id='$id' " ;
        $sql_where_sms = ($id=='') ? " AND sms_invoice_item.created_on < '" . date('Y-m-d H:i:s', strtotime('5 min ago') ) . "' " : " AND sms_invoice_item.invoice_id='$id' " ;

        $sql = "SELECT 
                    'POS' as sys_id,
                    concat('000', LEFT(ims_location_list.fin_dim1,2) , '     00' , RIGHT(RTRIM(pos_transaction_list.pos_id),1) , '   ') loc_pos_ref,
                    pos_transaction_list.loc_id, ims_location_list.fin_dim1, pos_transaction_list.pos_id, 
                    pos_transaction_item.item_id, pos_transaction_item.item_desc, pos_transaction_item.trans_qty, 
                    pos_transaction_item.regular_price as regular_price,  
                    pos_transaction_lot.coy_id, pos_transaction_lot.trans_id, pos_transaction_lot.line_num, pos_transaction_lot.lot_line_num, 
                    pos_transaction_lot.lot_id, 
						CASE WHEN pos_transaction_item.trans_qty<0 THEN (select lot_id2 from pos_transaction_lot where trans_id=pos_transaction_list.ref_id and lot_id=pos_transaction_lot.lot_id LIMIT 1 )
						ELSE pos_transaction_lot.lot_id2 END lot_id2 ,
						pos_transaction_lot.status_level, pos_transaction_lot.string_udf1, pos_transaction_lot.string_udf2, pos_transaction_lot.string_udf3, pos_transaction_lot.datetime_udf1, pos_transaction_lot.datetime_udf2, pos_transaction_lot.datetime_udf3,   
                    pos_transaction_lot.created_by, pos_transaction_lot.created_on, pos_transaction_lot.modified_by, pos_transaction_lot.modified_on,
					pos_transaction_list.email_addr,
					(case when ims_item_list.inv_dim4 in ('OFFICE CARD') then 'N' else 'Y' end) fraud_alert
                FROM pos_transaction_item
                    JOIN pos_transaction_list ON pos_transaction_list.coy_id=pos_transaction_item.coy_id AND pos_transaction_list.trans_id=pos_transaction_item.trans_id
                    LEFT JOIN pos_transaction_lot ON pos_transaction_lot.coy_id=pos_transaction_item.coy_id AND pos_transaction_lot.trans_id=pos_transaction_item.trans_id AND pos_transaction_lot.line_num=pos_transaction_item.line_num
                    JOIN ims_item_list ON ims_item_list.coy_id=pos_transaction_item.coy_id AND ims_item_list.item_id=pos_transaction_item.item_id
                    JOIN ims_location_list ON ims_location_list.coy_id=pos_transaction_list.coy_id AND ims_location_list.loc_id=pos_transaction_list.loc_id   
                WHERE
                    pos_transaction_list.coy_id = 'CTL' AND ims_item_list.item_type in ('G','H') AND
                    pos_transaction_item.status_level >= 0 AND ( pos_transaction_lot.status_level in (0,1) )
                    $sql_where_pos
                
                UNION ALL
                
                SELECT 
                    'SMS' as sys_id,
                    concat('000', LEFT(ims_location_list.fin_dim1,2) , '     000   ') loc_pos_ref,
                    sms_invoice_list.loc_id, ims_location_list.fin_dim1, '' as pos_id, 
                    sms_invoice_item.item_id, sms_invoice_item.item_desc, sms_invoice_item.qty_invoiced, 
                    sms_invoice_item.unit_price as regular_price, 
                    sms_invoice_item.coy_id, sms_invoice_item.invoice_id, sms_invoice_item.line_num, coalesce(sms_invoice_lot.lot_line_num,0) lot_line_num, 
                    sms_invoice_lot.lot_id, 
						CASE WHEN sms_invoice_item.qty_invoiced<0 THEN (select lot_id2 from sms_invoice_lot where invoice_id=sms_invoice_list.cust_po_code and string_udf2 in ('00','01','03') LIMIT 1)
						ELSE sms_invoice_lot.lot_id2 END lot_id2, 
						NULL as status_level , sms_invoice_lot.string_udf1, sms_invoice_lot.string_udf2, sms_invoice_lot.string_udf3, sms_invoice_lot.datetime_udf1, sms_invoice_lot.datetime_udf2, sms_invoice_lot.datetime_udf3,   
                    sms_invoice_item.created_by, sms_invoice_item.created_on, sms_invoice_item.modified_by, sms_invoice_item.modified_on,
					sms_invoice_list.email_addr,
					(case when ims_item_list.inv_dim4 in ('OFFICE CARD') then 'N' else 'Y' end) fraud_alert
                FROM sms_invoice_item
                    JOIN sms_invoice_list ON sms_invoice_list.coy_id=sms_invoice_item.coy_id AND sms_invoice_list.invoice_id=sms_invoice_item.invoice_id
                    LEFT JOIN sms_invoice_lot ON sms_invoice_lot.coy_id=sms_invoice_item.coy_id AND sms_invoice_lot.invoice_id=sms_invoice_item.invoice_id AND sms_invoice_lot.line_num=sms_invoice_item.line_num
                    JOIN ims_item_list ON ims_item_list.coy_id=sms_invoice_item.coy_id AND ims_item_list.item_id=sms_invoice_item.item_id
                    JOIN ims_location_list ON ims_location_list.coy_id=sms_invoice_list.coy_id AND ims_location_list.loc_id=sms_invoice_list.loc_id   
                WHERE
                    sms_invoice_list.coy_id = 'CTL' AND sms_invoice_list.inv_type IN ('HI','HR') AND 
                        ims_item_list.item_type in ('G','H') AND 
                    sms_invoice_item.status_level = 1 AND ( coalesce(sms_invoice_lot.string_udf2,'') NOT IN ('00','01') )
                    $sql_where_sms
                    
                ORDER BY modified_on ";

/*        // MS Version (if lot did not go into void tx)
        $sql = "SELECT 
                    'POS' as sys_id,
                    '000'+LEFT(ims_location_list.fin_dim1,2)+'     00'+RIGHT(RTRIM(pos_transaction_list.pos_id),1)+'   ' loc_pos_ref,
                    pos_transaction_list.loc_id, ims_location_list.fin_dim1, pos_transaction_list.pos_id, 
                    pos_transaction_item.item_id, pos_transaction_item.item_desc, pos_transaction_item.trans_qty, 
                    pos_transaction_item.regular_price as regular_price,
                    pos_transaction_lot.coy_id, pos_transaction_lot.trans_id, pos_transaction_lot.line_num, pos_transaction_lot.lot_line_num,
                    pos_transaction_lot.lot_id,
						CASE WHEN pos_transaction_item.trans_qty<0 THEN (select top 1 lot_id2 from pos_transaction_lot where trans_id=pos_transaction_list.ref_id and lot_id=pos_transaction_lot.lot_id )
						ELSE pos_transaction_lot.lot_id2 END lot_id2 ,
						pos_transaction_lot.status_level, pos_transaction_lot.string_udf1, pos_transaction_lot.string_udf2, pos_transaction_lot.string_udf3, pos_transaction_lot.datetime_udf1, pos_transaction_lot.datetime_udf2, pos_transaction_lot.datetime_udf3,
                    pos_transaction_lot.created_by, pos_transaction_lot.created_on, pos_transaction_lot.modified_by, pos_transaction_lot.modified_on,
					pos_transaction_list.email_addr,
					(case when ims_item_list.inv_dim4 in ('OFFICE CARD') then 'N' else 'Y' end) fraud_alert
                FROM pos_transaction_item
                    JOIN pos_transaction_list ON pos_transaction_list.coy_id=pos_transaction_item.coy_id AND pos_transaction_list.trans_id=pos_transaction_item.trans_id
                    LEFT JOIN pos_transaction_lot ON pos_transaction_lot.coy_id=pos_transaction_item.coy_id AND pos_transaction_lot.trans_id=pos_transaction_item.trans_id AND pos_transaction_lot.line_num=pos_transaction_item.line_num
                    JOIN ims_item_list ON ims_item_list.coy_id=pos_transaction_item.coy_id AND ims_item_list.item_id=pos_transaction_item.item_id
                    JOIN ims_location_list ON ims_location_list.coy_id=pos_transaction_list.coy_id AND ims_location_list.loc_id=pos_transaction_list.loc_id
                WHERE
                    pos_transaction_list.coy_id = 'CTL' AND ims_item_list.item_type in ('G','H') AND
                    pos_transaction_item.status_level >= 0 AND ( pos_transaction_lot.status_level in (0,1) )
                    $sql_where_pos

                UNION ALL

                SELECT
                    'SMS' as sys_id,
                    '000'+LEFT(ims_location_list.fin_dim1,2)+'     000   ' loc_pos_ref,
                    sms_invoice_list.loc_id, ims_location_list.fin_dim1, '' as pos_id,
                    sms_invoice_item.item_id, sms_invoice_item.item_desc, sms_invoice_item.qty_invoiced,
                    sms_invoice_item.unit_price as regular_price,
                    sms_invoice_item.coy_id, sms_invoice_item.invoice_id, sms_invoice_item.line_num, isnull(sms_invoice_lot.lot_line_num,0) lot_line_num,
                    sms_invoice_lot.lot_id,
						CASE WHEN sms_invoice_item.qty_invoiced<0 THEN (select top 1 lot_id2 from sms_invoice_lot where invoice_id=sms_invoice_list.cust_po_code and string_udf2 in ('00','01','03'))
						ELSE sms_invoice_lot.lot_id2 END lot_id2,
						NULL as status_level , sms_invoice_lot.string_udf1, sms_invoice_lot.string_udf2, sms_invoice_lot.string_udf3, sms_invoice_lot.datetime_udf1, sms_invoice_lot.datetime_udf2, sms_invoice_lot.datetime_udf3,
                    sms_invoice_item.created_by, sms_invoice_item.created_on, sms_invoice_item.modified_by, sms_invoice_item.modified_on,
					sms_invoice_list.email_addr,
					(case when ims_item_list.inv_dim4 in ('OFFICE CARD') then 'N' else 'Y' end) fraud_alert
                FROM sms_invoice_item
                    JOIN sms_invoice_list ON sms_invoice_list.coy_id=sms_invoice_item.coy_id AND sms_invoice_list.invoice_id=sms_invoice_item.invoice_id
                    LEFT JOIN sms_invoice_lot ON sms_invoice_lot.coy_id=sms_invoice_item.coy_id AND sms_invoice_lot.invoice_id=sms_invoice_item.invoice_id AND sms_invoice_lot.line_num=sms_invoice_item.line_num
                    JOIN ims_item_list ON ims_item_list.coy_id=sms_invoice_item.coy_id AND ims_item_list.item_id=sms_invoice_item.item_id
                    JOIN ims_location_list ON ims_location_list.coy_id=sms_invoice_list.coy_id AND ims_location_list.loc_id=sms_invoice_list.loc_id   
                WHERE
                    sms_invoice_list.coy_id = 'CTL' AND sms_invoice_list.inv_type IN ('HI','HR') AND 
                        ims_item_list.item_type in ('G','H') AND 
                    sms_invoice_item.status_level > 0 AND ( isnull(sms_invoice_lot.string_udf2,'') NOT IN ('00','01') )
                    $sql_where_sms
                    
                ORDER BY modified_on ";
*/
        //echo "<pre>" . $sql;exit;
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    private function setLotstatus($sys_id, $params){
        if ($sys_id=='SMS') {
            if ($params['status_level'] == 2) {
                // FOR ONLINE, only save lot if process successful
                $params['string_udf1'] = (isset($params['string_udf1']) && $params['string_udf1']!="") ? $params['string_udf1'] : '';
                $params['string_udf2'] = (isset($params['string_udf2']) && $params['string_udf2']!="") ? $params['string_udf2'] : '';
                $params['string_udf3'] = (isset($params['string_udf3']) && $params['string_udf3']!="") ? $params['string_udf3'] : '';
                if ($params['lot_line_num']==0) {
                    $sql = "INSERT INTO sms_invoice_lot (coy_id,invoice_id,line_num,lot_line_num,lot_id,lot_id2,trans_qty,string_udf1,string_udf2,string_udf3,datetime_udf1,datetime_udf2,datetime_udf3,created_by,created_on,modified_by,modified_on)
                  VALUES ('CTL','" . $params['trans_id'] . "','" . $params['line_num'] . "','" . $params['lot_line_num'] . "',
                        '" . $params['lot_id1'] . "','" . $params['lot_id2'] . "', 1, 
                        '" . $params['string_udf1'] . "','" . $params['string_udf2'] . "','" . $params['string_udf3'] . "', current_timestamp,current_timestamp,current_timestamp, 
                        'BLACKHAWK',current_timestamp,'BLACKHAWK',current_timestamp); ";
                }
                else {
                    $query_set  = ''; //$query_set = (isset($params['status_level'])) ? " , status_level='" . $params['status_level'] . "' " : "";
                    $query_set .= (isset($params['string_udf1'])) ? " , string_udf1='" . $params['string_udf1'] . "' " : "";
                    $query_set .= (isset($params['string_udf2'])) ? " , string_udf2='" . $params['string_udf2'] . "' " : "";
                    $query_set .= (isset($params['string_udf3'])) ? " , string_udf3='" . $params['string_udf3'] . "' " : "";
                    $query_set .= (isset($params['datetime_udf1'])) ? " , datetime_udf1=current_timestamp " : "";
                    $query_set .= (isset($params['datetime_udf2'])) ? " , datetime_udf2=current_timestamp " : "";
                    $query_set .= (isset($params['datetime_udf3'])) ? " , datetime_udf3=current_timestamp " : "";
                    $query_set .= (isset($params['lot_id2'])) ? " , lot_id2='" . $params['lot_id2'] . "' " : "";
                    $sql = "UPDATE sms_invoice_lot SET modified_by='BLACKHAWK', modified_on= current_timestamp $query_set
                      WHERE invoice_id='" . $params['trans_id'] . "' and line_num='" . $params['line_num'] . "' and lot_line_num='" . $params['lot_line_num'] . "' ";
                    $query = $this->db->query($sql);
                }
                if ($this->input->get('donotsave')) {
                    // Test
                }
                else {
                    $query = $this->db->query($sql);
                }
                return $query;
            }
            return NULL;
        }
        else {
            $query_set = (isset($params['status_level'])) ? " , status_level='" . $params['status_level'] . "' " : "";
            $query_set .= (isset($params['string_udf1'])) ? " , string_udf1='" . $params['string_udf1'] . "' " : "";
            $query_set .= (isset($params['string_udf2'])) ? " , string_udf2='" . $params['string_udf2'] . "' " : "";
            $query_set .= (isset($params['string_udf3'])) ? " , string_udf3='" . $params['string_udf3'] . "' " : "";
            $query_set .= (isset($params['datetime_udf1'])) ? " , datetime_udf1=current_timestamp " : "";
            $query_set .= (isset($params['datetime_udf2'])) ? " , datetime_udf2=current_timestamp " : "";
            $query_set .= (isset($params['datetime_udf3'])) ? " , datetime_udf3=current_timestamp " : "";
            $query_set .= (isset($params['lot_id2'])) ? " , lot_id2='" . $params['lot_id2'] . "' " : "";
            $sql = "UPDATE pos_transaction_lot SET modified_by='BLACKHAWK', modified_on= current_timestamp $query_set
                  WHERE trans_id='" . $params['trans_id'] . "' and line_num='" . $params['line_num'] . "' and lot_line_num='" . $params['lot_line_num'] . "' ";
            $query = $this->db->query($sql);
            return $query;
        }
    }

    private function _doErrormail($rec,$setlot,$reply,$addstr,$addemail='') {

        $email = 'yongsheng@challenger.sg';

        $subj = 'Blackhawk API Error - ' . $rec['trans_id'];
        $msg = "Trans ID: ".trim($rec['sys_id'])." ".trim($rec['trans_id'])."<br>Card No: ".trim($rec['lot_id'])."<br>Item Desc: ".trim($rec['item_desc']);
        $msg.= "<br><br>".$setlot["string_udf3"]."<br>Response: ".$reply['p2_code']." - ".$reply['p2_msg']."<br>$addstr";
        $email = $this->notification . ' ' . $addemail;
        $this->cherps_email($subj,$msg,$email);
    }

    public function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {

        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = '';
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }


    private function HachiTriggerEmail($trans_id, $email_addr, $unit_price, $acct_code) {
        $query = '{"email":"'.trim($email_addr).'","card_value": "'. sprintf('%01.2f',$unit_price) .'","claim_code": "'. trim($acct_code) .'","serial_no":""}';

        $headers[] = "X_AUTHORIZATION:NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->hachitriggerurl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);

        // Save to DB
        $response = json_decode($output);
        $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('CTL','BHK-HACHI','".md5(time())."','".trim($trans_id)."','','' ,
                        '".$this->hachitriggerurl."','".$query."','". $response->code ."','". ucfirst($response->message) ."','".$output."','".getRealIpAddr(15)."',current_timestamp)";
        $this->db->query($sql);

        return true;
    }

    public function logwritegw($params)
    {
        $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('CTL','" . $params['gateway_id'] . "','CHV-" . time() . "','" . trim($params['trans_id']) . "','','' ,
                        '" . trim($params['request_link']) . "','" . trim($params['request_msg']) . "','1','','','" . getRealIpAddr(15) . "', current_timestamp )";
        $this->db->query($sql);
    }

}
?>