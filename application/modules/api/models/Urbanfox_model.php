<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Urbanfox_model extends Base_Common_Model {

    private $apiurl;
    private $user;
    private $pass;
    private $billing_id;
    private $dlvr_type;

    public function __construct() {
        parent::__construct();

        $this->billing_id = '10167';
        $this->dlvr_type = 'wh_delivery';

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->apiurl = 'https://dp.storeviva.com/api/u/hachi/g5MRudaVsLA96XhSONNl9jAktvI/gql';
            $this->user = 'challenger';
            $this->pass = 'password123';
        }
        else {
            $this->apiurl = 'https://dp-staging-web.storeviva.com/api/u/hachi/g5MRudaVsLA96XhSONNl9jAktvI/gql';
            $this->user = 'challenger'; // 'challenger-test';
            $this->pass = 'password123';
        }
    }

    public function getParcel($invoice_id='') {
        $sql_invoice = (strlen($invoice_id)>3) ? "and p.invoice_id='$invoice_id'" : "";
        $sql = "select 
                    p.coy_id,p.invoice_id,p.parcel_id, 
                    CASE WHEN (p.parcel_id>1) THEN rtrim(i.invoice_id)+'-'+CAST(p.parcel_id as nvarchar(3)) ELSE rtrim(i.invoice_id) END as [external_track_no], 
                    CASE WHEN (rtrim(i.first_name)+' '+rtrim(i.last_name)!='') THEN rtrim(i.first_name)+' '+rtrim(i.last_name)
                        ELSE (cust_name) END as [dst_name],
                    i.tel_code as [dst_contact],
                    CASE WHEN (i.email_addr!='') THEN i.email_addr
                        ELSE (select email_addr from crm_member_list where coy_id='CTL' and mbr_id=i.cust_id) END as [dst_email],
                    REPLACE(d.addr_text,'''','') as [dst_addr],
                    d.postal_code as [dst_postcode], 
                    CASE WHEN d.country_id='SG' THEN 'Singapore' ELSE d.country_id END as [dst_country],
                    p.volumeL as [vol_l],
                    p.volumeW as [vol_w],
                    p.volumeH as [vol_h],
                    p.weight as [weight],
                    CAST(p.delivery_date as date) as [delivr_date],
                    CASE p.delivery_window WHEN -3 THEN '1800'
										   WHEN -2 THEN '0900'
										   WHEN -1 THEN '2201'
										   WHEN 0 THEN '0900'
										   WHEN 1 THEN '1200'
										   WHEN 2 THEN '1500'
										   WHEN 3 THEN '1800' END as [delivr_time],
                    CASE WHEN EXISTS(SELECT sms_invoice_item.loc_id FROM b2b_parcel_item,sms_invoice_item 
							WHERE sms_invoice_item.invoice_id=b2b_parcel_item.invoice_id
								AND sms_invoice_item.item_id=b2b_parcel_item.item_id
								AND b2b_parcel_item.invoice_id= p.invoice_id
								AND b2b_parcel_item.parcel_id= p.parcel_id 
						) 
						THEN (SELECT TOP 1 sms_invoice_item.loc_id FROM b2b_parcel_item,sms_invoice_item 
							WHERE sms_invoice_item.invoice_id=b2b_parcel_item.invoice_id
								AND sms_invoice_item.item_id=b2b_parcel_item.item_id
								AND b2b_parcel_item.invoice_id= p.invoice_id
								AND b2b_parcel_item.parcel_id= p.parcel_id ) 
						ELSE i.loc_id END as loc_id
                from b2b_parcel_list p
                left join sms_invoice_list i on i.invoice_id=p.invoice_id and i.coy_id=p.coy_id
                left join coy_address_book d on d.ref_id=i.invoice_id and d.addr_type=i.delv_addr
                where p.status_level =0 and p.delv_mode_id='NQX' $sql_invoice 
                order by p.created_on desc";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    public function getLocation($coy_id,$loc_id) {
        $sql = "select 
                    l.loc_name as [src_company],
                    l.tel_code as [src_contact],
                    l.email_ims as [src_email],
                    d.addr_text as [src_addr],
                    d.postal_code as [src_postcode], 
                CASE WHEN d.country_id='SG' THEN 'Singapore' ELSE d.country_id END as [src_country]
                from ims_location_list l
                left join coy_address_book d on d.ref_id=l.loc_id and d.coy_id=l.coy_id and d.ref_type='LOCATION'
                where l.coy_id='$coy_id' and l.loc_id='$loc_id'";
        $res = $this->db->query($sql)->row_array();
        return $res;
    }

    public function saveGatewayLog($params){
        $coy_id='CTL';
        $gateway_id='URBANFOX';
        $gateway_code = md5($params['trans_id'].$params['loc_id'].$params['pos_id'].$params['buyer_code'].$params['tx_time']);
        $gateway_request = $params['trans_id'].'/'.$params['loc_id'].'/'.$params['buyer_code'].'?t='.$params['tx_time'] ;
        if ($params['status']=="NEW") {
            $sql = "INSERT INTO b2b_gateway_message 
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id,request_link,request_msg,status_level,created_by,created_on)
                        VALUES ('$coy_id','$gateway_id','".$gateway_code."','".trim($params['trans_id'])."','".trim($params['loc_id'])."','".trim($params['pos_id'])."' ,
                        '".trim($params['url'])."','".$params['request']."','0','".getRealIpAddr(15)."',GETDATE())";
            $this->db->query($sql);
        }
        else {
            $sql = "UPDATE b2b_gateway_message
                        SET status_level=1, return_status='".$params['status']."', return_msg='".str_replace("'",'',$params['response'])."'
                        WHERE coy_id='$coy_id' AND gateway_id='$gateway_id' AND gateway_code='$gateway_code' ";
            $this->db->query($sql);
        }
    }

    public function updateParcel($params){
        $sql = "UPDATE b2b_parcel_list
                    SET transaction_id='".$params['transaction_id']."', tracking_id='".$params['tracking_id']."', status_level='".$params['status_level']."',
                        remarks='".$params['remarks']."',quick_ref='".$params['quick_ref']."',
                        modified_by='Urbanfox',modified_on=GETDATE()
                    WHERE coy_id='".$params['coy_id']."' AND invoice_id='".$params['invoice_id']."' AND parcel_id='".$params['parcel_id']."' and delv_mode_id='NQX' ";
        $this->db->query($sql);
    }

    public function query($id=''){

        date_default_timezone_set('Asia/Singapore');

        $tx_time = date("YmdHis");
        $invoice_id = ($id=='') ? $this->input->get('invoice_id') : $id;
        $invoice = $this->getParcel($invoice_id);

        if (!$invoice){
            $return["status"] = 'FAILED';
            $return["message"] = 'No invoice found.';
            return $return;
        }

        foreach ($invoice as $i=>$inv){
            // Fix contact_no
            $inv_contact = str_replace(' ','',$inv['dst_contact']);
            $invoice[$i]['dst_contact'] = (substr($inv_contact,0,2)=='65' && strlen($inv_contact)<=8) ? '65'.$inv_contact : $inv_contact;
        }

        if (empty($invoice[0]['dst_contact']) || empty($invoice[0]['dst_postcode']) || empty($invoice[0]['dst_country'])){
            $return["status"] = 'FAILED';
            $return["message"] = 'Invoice found does not have full details (contact/postcode/country).';
            return $return;
        }
        else if (strlen($invoice[0]['dst_contact'])<8) {
            // Invalid phone
            $return["status"] = 'FAILED';
            $return["message"] = 'Please check the phone number, do not have 8 digits ('.$invoice[0]['dst_contact'].').';
            return $return;
        }
        else if ( !($this->input->get('delivr_date')) && strtotime($invoice[0]['delivr_date'])<=time()-86400 ){
            $return["status"] = 'FAILED';
            $return["message"] = 'Please check the delivery window ('.$invoice[0]['delivr_date'].').';
            return $return;
        }

        $loc_id = $invoice[0]['loc_id']; //($this->input->get('loc_id')) ? $this->input->get('loc_id') : 'BF';
        $loc = $this->getLocation($invoice[0]['coy_id'],$loc_id);

        $dates['pickup_date'] = ($this->input->get('pickup_date')) ? strtotime($this->input->get('pickup_date')) : strtotime('today 16:00');
        $dates['delivr_date'] = ($this->input->get('delivr_date')) ? strtotime($this->input->get('delivr_date')) : strtotime($invoice[0]['delivr_date'].' '.$invoice[0]['delivr_time']);

        $query = $this->_doQuery($invoice,$loc,$dates);

        $this->saveGatewayLog( array(
            "status" => 'NEW',
            "trans_id" => trim($invoice[0]['invoice_id']),
            "loc_id" => $loc_id,
            "pos_id" => '',
            "buyer_code" => $dates['pickup_date'].'>'.$dates['delivr_date'],
            "url" => $this->apiurl,
            "tx_time" => $tx_time,
            "request" => $query,
            "response" => ''
        ) );

        $output = $this->_doCurl($query);

        $response = json_decode($output);
        $response_trackingtest = $response->data->q0->list[0]->ref_no;

        if ($response_trackingtest) {

            $return["status"] = 'SUCCESS';

            foreach ($invoice as $k=>$inv) {
                $res = 'q'.$k;
                $response_tracking = $response->data->$res->list[0]->ref_no;
                if ($response_tracking) {
                    $this->updateParcel(array(
                        "coy_id" => $inv['coy_id'],
                        "invoice_id" => $inv['invoice_id'],
                        "parcel_id" => $inv['parcel_id'],
                        "transaction_id" => $response_tracking,
                        "tracking_id" => $response_tracking,
                        "status_level" => 3,
                        "quick_ref" => substr($response_tracking, -4),
                        "remarks" => 'SUCCESSFUL' //FAILED
                    ));
                }

                $return["results"][] = array(
                    "invoice_id" => trim($inv['invoice_id']),
                    "parcel_id" => trim($inv['parcel_id']),
                    "tracking_id" => $response_tracking
                );
            }
        }
        else {

            if (strpos($response->errors[0]->message,'Invalid Payload')!==FALSE) {
                $response_trackingtest = str_replace('\'','',substr($response->errors[0]->message, strpos($response->errors[0]->message,'Invalid Payload'), strpos($response->errors[0]->message,' is ')));
            }

            $return["status"] = 'FAILED';
            $return["message"] = (isset($response_trackingtest) && $response_trackingtest!='') ? 'Response is ' . $response_trackingtest : 'Fail to receive expected response from Urbanfox API.';
        }

        $this->saveGatewayLog( array(
            "status" => ($response_trackingtest) ? json_encode($return) : 'ERROR',
            "trans_id" => trim($invoice[0]['invoice_id']),
            "loc_id" => $loc_id,
            "pos_id" => '',
            "buyer_code" => $dates['pickup_date'].'>'.$dates['delivr_date'],
            "url" => $this->apiurl,
            "tx_time" => $tx_time,
            "response" => $output
        ) );

        if ( $this->input->get('debug')==1 ) {
            print_r($output);
            exit;
        }

        return $return;
    }

    private function _doCurl($query) {
        $headers[] = "Content-Type: application/json;charset=utf-8";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiurl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['query' => $query]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    private function _doQuery($invoices,$loc,$dates){

        $track_no_suffix = (ENVIRONMENT=="production") ? '' : substr('#'.time(),-4);

        $query = 'mutation _ {';
        foreach ($invoices as $k=>$invoice) {

            $dst_contact = (strlen(str_replace('-','',trim($invoice['dst_contact'])))==8) ? '65'.trim($invoice['dst_contact']) : trim($invoice['dst_contact']);
            $query .= '
q'.$k.': transaction_create(
    src_name: "HACHI.TECH", 
    src_company: "' . trim($loc['src_company']) . '", 
    src_contact: "' . trim($loc['src_contact']) . '", 
    src_addr: "' . trim(preg_replace('/\s+/', ' ', $loc['src_addr'])) . '", 
    src_unit: "#00-00", 
    src_postcode: "' . trim($loc['src_postcode']) . '", 
    src_country: "' . trim($loc['src_country']) . '", 
    src_email: "' . trim($loc['src_email']) . '", 
    
    dst_name: "' . trim($invoice['dst_name']) . '", 
    dst_company: "' . trim($invoice['dst_name']) . '", 
    dst_contact: "' . $dst_contact . '", 
    dst_addr: "' . trim(preg_replace('/\s+/', ' ', $invoice['dst_addr'])) . '", 
    dst_unit: "#00-00", 
    dst_postcode: "' . trim($invoice['dst_postcode']) . '", 
    dst_country: "' . trim($invoice['dst_country']) . '", 
    dst_email: "' . trim($invoice['dst_email']) . '", 
    
    content_desc: "Hachi Order ' . trim($invoice['external_track_no']) . '", 
    content_value: 0.99, 
    vol_l: ' . trim($invoice['vol_l']) . ', vol_w: ' . trim($invoice['vol_w']) . ', vol_h: ' . trim($invoice['vol_h']) . ', weight: ' . trim($invoice['weight']) . ', 
    
    instruction: "", 
    delivr_date: "' . date(DATE_ATOM, $dates['delivr_date']) . '", 
    pickup_date: "' . date(DATE_ATOM, $dates['pickup_date']) . '", 
    
    external_track_no: "' . trim($invoice['external_track_no'].$track_no_suffix) . '", 
    cod_amount: 0.00, 
    
    billing_uid: "' . $this->billing_id . '", 
    delivery_type: "' . $this->dlvr_type . '",
    driver_pay: 0, 
    pending_date_start: "", 
    pending_date_end: ""
  ) 
  {
    num_deliveries,
    list {
        ref_no,
        external_track_no
    }
  }';
        }
        $query.= '}';

        return $query;
    }

}
?>