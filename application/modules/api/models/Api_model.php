<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Api_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
        
        $this->load->library('s3');
        $this->config->load('s3', true, true);
        $s3_config = $this->config->item('s3');
        $this->token = 'Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjE';
        if (ENVIRONMENT=="production") {
            // Product env
            $this->vc_url = 'http://vc.api.valueclub.asia/api/transaction';
        } else {
            // Test env
            $this->vc_url = 'http://chvc-api.sghachi.com/api/transaction';
        }
        $this->vc_auth = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';
    }

    public function esd_check($id){
        $sql = "select t.trans_id,t.trans_date, t.email_addr, p.po_id,p.status_text,p.msg_request,p.msg_response, k.item_info1 b2bponotes_iteminfo1,k.status_level b2bponotes_statuslevel
                from (
                    --1. Find the Invoice/Receipt
                    select trans_id,trans_date,email_addr from pos_transaction_list where coy_id='CTL' and trans_id='$id'
                    union all select invoice_id,invoice_date,email_addr from sms_invoice_list where coy_id = 'CTL' and invoice_id='$id'
                ) t
                left join (
                    --2. Get the PO
                    select po.po_id, right(po.remarks, (length(po.remarks)-13) ) ref_id, pm.status_text, pm.msg_request, pm.msg_response
                    from pms_po_list po
                    left join b2b_po_message pm on pm.trans_id=po.po_id
                    where po.coy_id='CTL' and (po.remarks like '%$id%')
                ) p on t.trans_id = p.ref_id
                left join b2b_po_notes k on t.trans_id = k.invoice_id;";
        $res = $this->db->query($sql)->result_array();
        return ($res) ? $res[0] : null;
    }

    public function cb_hclqoh(){
        $sql = "select i.item_id,i.item_desc,i.inv_dim2,i.inv_dim3, i.buffer_qty,
			(select sum(qty_on_hand-qty_reserved) from ims_inv_physical where item_id=i.item_id and loc_id='HCL') qoh_hcl
		from o2o_live_product p
		join ims_item_list i on p.item_id=i.item_id
	    order by qoh_hcl desc ";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    public function cherps_email($message,$subject='CHVOICES-Alert',$email='yongsheng@challenger.sg') {
        $params['to'] = $email;
        $params['subject'] = $subject;
        $params['message'] = $message;
        $rtn = $this->_SendEmail($params);
        return $rtn;
    }
    
    public function cherps_db($sp,$rtn=1) {
        $cherps_hachi = $this->load->database('cherps_hachi', TRUE);

        $sql = "EXEC $sp;";
        if ($rtn==1)
            $res = $cherps_hachi->query($sql)->result_array();
        else
            $res = $cherps_hachi->query($sql);
        return $res;
    }

    public function logwrite($filename,$content){
        $handle_path = FCPATH . 'assets/logs/';
        $uploadedcsv = trim($filename) . '.txt';
        file_put_contents($handle_path.$uploadedcsv, date('Y-m-d H:i:s') . ' ' . $content.PHP_EOL , FILE_APPEND | LOCK_EX);
    }

    public function logwritedb($params){
        $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,url_link,msg_request,status_code,status_text,msg_response,created_by,created_on,modified_by,modified_on)
                        VALUES ( 'CTL','".$params['trans_type']."','".$params['trans_id']."','".$params['url_link']."','".$params['msg_request']."',
                            ".$params['status_code'].",'".$params['status_text']."','".$params['msg_response']."',
                            '".$params['created_by']."',NOW(),'".$params['created_by']."',NOW() ); ";
        $query = $this->db->query($sql);
    }

    public function logwritegw($params){
        $return_msg = (isset($params['return_msg']) && $params['return_msg']!='') ? trim($params['return_msg']) : '' ;
        $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('CTL','".$params['gateway_id']."','CHV-".time()."','".trim($params['trans_id'])."','','' ,
                        '".trim($params['request_link'])."','".trim($params['request_msg'])."','1','','".$return_msg."','".getRealIpAddr(15)."',GETDATE())";
        $this->db->query($sql);
    }

    public function getb2bsetting($sys_id,$set_code,$set_default){
        $sql = "select sys_id,set_code,set_desc,set_default,remarks,modified_by,modified_on from b2b_setting_list
                  where sys_id='$sys_id' and set_code='$set_code' and set_default='$set_default' ";
        return $this->db->query($sql)->row_array();
    }
    
    public function uploadS3($file){
        
        $supp_id = $this->session->userdata('cm_supp_id');
        $name = $supp_id . '.' . time() . '.csv';
        $upload = $this->_upload($file, $name, "assets/uploads/", "csv", $supp_id.'.');
        
        if (!isset($upload["error"])) {
            $filename = $upload["upload_data"]["file_name"];
            $filedirrectory = FCPATH . 'assets/uploads/' . $filename;

            // Send it to S3 
            $S3_BUCKET = 'ctl-lambda';
            $destination = (ENVIRONMENT=="production") ? 'uploads/' : 'STAGING/uploads/';
            $destination = (ENVIRONMENT=="production") ? 'hachi-inventory-update/' : 'STAGING/hachi-inventory-update/';
            $header = array("Cache-Control"=>"no-cache"); 
            $s3upload = $this->s3->putObject($this->s3->inputFile($filedirrectory), $S3_BUCKET, $destination.$filename, S3::ACL_PUBLIC_READ, array(), $header);
            
            if (!$s3upload){
                $error = array('error' => "File could not be uploaded to server.");
                return $error;
            }
            else {
                $sql = "INSERT INTO o2o_extinv_list (extinv_vendor,extinv_uploaded,status_level,extinv_remarks,created_by,created_on) "
                        . "VALUES('".$supp_id."', '0', 0, '<!--FILE:".$filename."--> Processing...', '".$supp_id."', GETDATE()); "; 
                $result = CI::db()->query($sql);
                
                unlink($filedirrectory);
            }
        }
        else {
            return $upload;
        }
    }

    // Controller: challenger_notify
    public function cherps_usr($usr_id) {
        $sql = "select u.usr_id,c.staff_id,u.usr_name,u.email_addr,u.mobile_phone,
	              (CASE WHEN u.usr_id in ('ltloo','wktan') THEN 0 ELSE 1 END) notify_pn
                from sys_usr_list u
                join coy_usr_list c on c.usr_id=u.usr_id
                where u.usr_id ='$usr_id' limit 1; ";
        $res = $this->db->query($sql)->row_array();
        return $res;
    }
    public function logcheckdb($param) {
        $date_text = ($param['last_created']>0) ? "+ " . $param['last_created'] . " minutes" : ($param['last_created']*-1) . " minutes ago";
        $sql_date = date('Y-m-d H:i:s', strtotime($date_text) );
        $sql_where = ($param['last_created']) ? " and created_on > '$sql_date' " : "";
        $sql = "select count(*) cnt, max(created_on) created_on from b2b_po_message 
                where coy_id='CTL' and trans_type='".$param['trans_type']."' and trans_id='".$param['trans_id']."' 
                    and url_link='".$param['url_link']."' and status_code=1 $sql_where ";
        $res = $this->db->query($sql)->row_array();
        return $res;
    }

    // Controller: delivery
    public function challenger_parcel($invoice_id){
        $sql = "select invoice_id,delv_mode_id,delivery_date,tracking_id from b2b_parcel_list p where p.status_level IN (0,1) and p.invoice_id='$invoice_id' order by p.status_level DESC, p.created_on DESC limit 1;";
        $res = $this->db->query($sql)->result();
        return $res;
    }

    // Controller: ssew
    public function ssew_servicereq($id){
        $sql = "SELECT rtrim(service_id) service_id,rtrim(product_type) product_type,serial_num,svs_service_list.status_level,sys_status_list.status_desc,pending_type,created_on
                    FROM svs_service_list
                    JOIN sys_status_list ON sys_status_list.ref_id='service_list' AND sys_status_list.status_level = svs_service_list.status_level
                    WHERE service_id='$id'; ";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    /**
     * @api {post} /api/ssew_migrate/:from/:to?code=Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjE SSEW Migrate
     * @apiVersion 1.0.0
     * @apiGroup SSEW
     *
     * @apiParam {string} [from] optional from datetime. default is yesterday. eg: 2019-10-10
     * @apiParam {string} [to] optional to datetime. default is today. eg: 2019-10-11
     *
     * @apiSuccessExample SSEW to pgsql success
     * HTTP/1.1 200 OK
     *  {
     *      "status": "Success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "return": "0",
     *      "message": "Request code is error, please check your request code."
     *  }
     */
    public function ssew_migrate($from, $to)
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return array('return'=>'0','message'=>'Request code is error, please check your request code.');
        } else {
            $now = date("Y-m-d H:i:s");
            $yesterday = $from != '' ? $from : date('Y-m-d', strtotime("-1 days"));
            $today = $to != '' ? $to : date("Y-m-d");
//            $yesterday = '2020-03-29 14:50:00';
//            $today = '2020-03-29 15:00:00';
            // Get all ssew transaction from pos_transaction
            $get_pos_sew = "
                            WITH valid_month AS (SELECT set_data FROM coy_setting_list WHERE sys_id = 'SEW' AND set_code = 'EW_MTH' AND coy_id = 'CTL')
                            SELECT
                            1 as status_level,
                            ( SELECT inv_dim3 FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2 ) prod_type,
                            A3.lot_id serial_num,
                            ( SELECT brand_id FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2 ) brand_id,
                            ( SELECT model_id FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2 ) model_id,
                            COALESCE((
                            SELECT unit_price - ( disc_amount / trans_qty )
                            FROM
                            pos_transaction_item itm
                            WHERE
                            itm.coy_id = A3.coy_id
                            AND itm.trans_id = A3.string_udf1
                            AND itm.item_id = A3.string_udf2
                            AND itm.status_level >= 0
                            LIMIT 1
                            ),0) unit_price,
                            to_char(A1.trans_date, 'YYYY-MM-DD') offer_purchased_date,
                            valid_month.set_data as offer_valid_month,
                            to_char(A1.trans_date, 'YYYY-MM-DD') offer_reg_date,
                            to_char(A1.trans_date + interval '1 month' * (SELECT (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2), 'YYYY-MM-DD') offer_start_date,
                            to_char(A1.trans_date + interval '1 month' * (cast(valid_month.set_data as FLOAT) + (SELECT (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2)) + interval '-1 day', 'YYYY-MM-DD') offer_expiry_date,
                            A2.item_id offer_item_id,
                            A2.trans_cost AS offer_cost,
                            A2.trans_id offer_invoice_id,
                            concat_ws('/', A1.coy_id, A1.loc_id) string_udf5,
                            (SELECT (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2) string_udf1,
                            rtrim(mbr_id) mbr_id,
                            cust_name,
                            tel_code,
                            email_addr
                            FROM
                            pos_transaction_list A1, pos_transaction_item A2, pos_transaction_lot A3, ims_item_list c1, valid_month
                            WHERE
                            ( A2.coy_id = A1.coy_id ) and
                            ( A2.trans_id = A1.trans_id ) and
                            ( A2.coy_id = A3.coy_id ) and
                            ( A2.trans_id = A3.trans_id ) and
                            ( A2.line_num = A3.line_num ) and
                            ( c1.coy_id = A2.coy_id and c1.item_id = A2.item_id and c1.item_type = 'W' ) and
                            ( A1.status_level > 0 ) AND
                            ( A2.status_level > 0 ) AND
                            ( A1.trans_date >= '$yesterday' ) AND
                            ( A1.trans_date < '$today') AND
                            ( A3.status_level > 0 ) AND
                            ( A2.csd_qty < A2.trans_qty ) AND
                            ( A3.string_udf2 <> '') AND
                            ( string_udf1 > ''  and left(string_udf1,2)<>'OP' and string_udf1<>'3rd Party' and A3.coy_id='CTL')
                        ";
            $res_pos_sew = $this->db->query($get_pos_sew)->result_array();
            $mig_pos_sew = 'INSERT INTO svs_offer_list (coy_id,offer_id,status_level,product_type,product_purchased_date,serial_num,brand_id,model_id,unit_price,cust_id,offer_purchased_date,offer_valid_month,offer_reg_date,offer_start_date,offer_expiry_date,offer_item_id,offer_provider,offer_cost,offer_invoice_id,offer_sub_type,offer_refunded_date,claimable_amount,claimed_amount,date_udf1,date_udf2,date_udf3,date_udf4,date_udf5,string_udf1,string_udf2,string_udf3,string_udf4,string_udf5,created_by,created_on,modified_by,modified_on) VALUES ';
            $mig_pos_parts = array();
            $pos_trans_id = array();
            $cur_year = date("Y");
            $trans_prefix = 'EW' . $cur_year;
            $is_exists = $this->db->query("select 1 from sys_trans_list where coy_id = 'INC' and sys_id = 'SEW' and trans_prefix = '" . $trans_prefix . "'")->num_rows();
            if ($is_exists === 0) {
                $this->db->query("insert into sys_trans_list (coy_id, sys_id, trans_prefix, next_num, sys_date) VALUES ('INC', 'SEW', '". $trans_prefix ."', 1, '". $now ."')");
            }
            foreach ($res_pos_sew as $v){
                $trans_id = $this->_next_line($trans_prefix);
                $first_name = explode(' ', $v['cust_name'])[0];
                $last_name = isset(explode(' ', $v['cust_name'])[1]) ? explode(' ', $v['cust_name'])[1] : '';
                $this->db->query("insert into svs_customer_list (coy_id, cust_id, cust_title, first_name, last_name, contact_num, email_addr, cust_ref_id, created_by, created_on, modified_by, modified_on) values 
                                  ('INC', '". $trans_id ."', '-', '".$first_name."', '".$last_name."', '".$v['tel_code']."', '".$v['email_addr']."', '".$v['mbr_id']."', 'cherps', '".$now."', 'cherps', '".$now."')");
                $mig_pos_parts[] = "('INC', '" . $trans_id . "', " . $v['status_level'] . ", '" . $v['prod_type'] . "', '" . $v['offer_purchased_date'] . "', '" .
                    $v['serial_num'] . "', '" . $v['brand_id'] . "', '" . $v['model_id'] . "', " . $v['unit_price'] . ", '" .
                    $trans_id . "', '" . $v['offer_purchased_date'] . "', " . $v['offer_valid_month'] . ", '" .
                    $v['offer_reg_date'] . "', '" . $v['offer_start_date'] . "', '" . $v['offer_expiry_date'] . "', '" .
                    $v['offer_item_id'] . "', '', " . $v['offer_cost'] . ", '" . $v['offer_invoice_id'] . "', '', '1900-01-01', " .
                    $v['unit_price'] . ", 0, '" . $v['offer_reg_date'] . "', '" . $v['offer_start_date'] . "', '" .
                    $v['offer_expiry_date'] . "', '1900-01-01', '1990-01-01', '"  . $v['string_udf1'] . "', '', '', '', '"  . $v['string_udf5'] . "', 'cherps', '" .
                    $now . "', 'cherps', '" . $now . "')";
                $pos_trans_id[] = $v['offer_invoice_id'];
            }
            if (!empty($mig_pos_parts)){
                $mig_pos_sew .= implode(',', $mig_pos_parts);
                $result_pos = $this->db->query($mig_pos_sew);
                if ($result_pos) {
                    $rp = array(
                        "status" => 'Success',
                        "status_code" => 200,
                    );
                } else {
                    $rp = array(
                        "status" => 'Fail',
                        "status_code" => 400,
                    );
                }
            } else {
                $rp = array(
                    "status" => 'No pos transaction',
                    "status_code" => 200,
                );
            }

            // Get all ssew from sms_invoice
            $get_sms_sew = "
                            WITH valid_month AS (SELECT set_data FROM coy_setting_list WHERE sys_id = 'SEW' AND set_code = 'EW_MTH' AND coy_id = 'CTL')
                            SELECT
                            1 status_level,
                            to_char(B1.invoice_date, 'YYYY-MM-DD') offer_purchased_date,
                            valid_month.set_data offer_valid_month,
                            to_char(B1.invoice_date, 'YYYY-MM-DD') offer_reg_date,
                            to_char(B1.invoice_date, 'YYYY-MM-DD') offer_start_date,
                            to_char(B1.invoice_date + interval '1 month' * cast(valid_month.set_data AS FLOAT), 'YYYY-MM-DD') offer_expiry_date,
                            B2.item_id offer_item_id,
                            B2.unit_price*0.7/1.07 AS offer_cost,
                            B2.invoice_id offer_invoice_id,
                            concat_ws('/',B1.coy_id, B1.loc_id) string_udf5,
                            rtrim(cust_id) cust_id,
                            cust_name,
                            tel_code,
                            email_addr,
                            B2.qty_invoiced
                            FROM
                            sms_invoice_list B1, sms_invoice_item B2, ims_item_list d1, valid_month
                            WHERE
                            ( B1.status_level > 0 )
                            AND ( B2.status_level >= 0 )
                            AND ( B1.invoice_date >= '$yesterday' )
                            AND ( B1.invoice_date < '$today' )
                            AND ( B2.qty_invoiced > 0 )
                            AND (B1.invoice_id = B2.invoice_id AND B1.coy_id = B2.coy_id)
                            AND (B2.item_id = d1.item_id AND B2.coy_id = d1.coy_id AND d1.item_type = 'W')
                            ";
            $res_sms_sew = $this->db->query($get_sms_sew)->result_array();
            $mig_sms_sew = 'INSERT INTO svs_offer_list (coy_id,offer_id,status_level,product_type,product_purchased_date,serial_num,brand_id,model_id,unit_price,cust_id,offer_purchased_date,offer_valid_month,offer_reg_date,offer_start_date,offer_expiry_date,offer_item_id,offer_provider,offer_cost,offer_invoice_id,offer_sub_type,offer_refunded_date,claimable_amount,claimed_amount,date_udf1,date_udf2,date_udf3,date_udf4,date_udf5,string_udf1,string_udf2,string_udf3,string_udf4,string_udf5,created_by,created_on,modified_by,modified_on) VALUES ';
            $mig_sms_parts = array();
            $sms_invoice_id = array();
            foreach ($res_sms_sew as $v){
                $exists_lot = $this->db->query("SELECT 1 FROM sms_invoice_lot WHERE invoice_id = '".$v['offer_invoice_id']."'")->num_rows();
                $first_name = explode(' ', $v['cust_name'])[0];
                $last_name = isset(explode(' ', $v['cust_name'])[1]) ? explode(' ', $v['cust_name'])[1] : '';
                $start_dt = new DateTime($v['offer_start_date']);
                $expiry_dt = new DateTime($v['offer_expiry_date']);

                if ($exists_lot === 0) {
                    $item = $this->db->query("SELECT mst.inv_dim3 prod_type, mst.brand_id brand_id, mst.model_id model_id, i.unit_price - (i.disc_amount / i.qty_invoiced) unit_price, (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty
                                  FROM ims_item_list mst JOIN sms_invoice_item i ON i.item_id = mst.item_id AND i.coy_id = mst.coy_id
                                  WHERE i.invoice_id = '" . $v['offer_invoice_id'] . "' AND i.line_num = 1")->result_array();
                    $offer_start_date = $start_dt->modify('+' . (string)$item[0]['ext_warranty'] . ' month')->format('Y-m-d');
                    $offer_expiry_date = $expiry_dt->modify('+' . (string)$item[0]['ext_warranty'] . ' month -1 day')->format('Y-m-d');
                    foreach (range(1, $v['qty_invoiced']) as $i) {
                        $trans_id = $this->_next_line($trans_prefix);
                        $insert_sql = "insert into svs_customer_list (coy_id, cust_id, cust_title, first_name, last_name, contact_num, email_addr, cust_ref_id, created_by, created_on, modified_by, modified_on) values
                                  ('INC', '". $trans_id ."', '-', '".$first_name."', '".$last_name."', '".$v['tel_code']."', '".$v['email_addr']."', '".$v['cust_id']."', 'cherps', '".$now."', 'cherps', '".$now."')";
                        $this->db->query($insert_sql);
                        $mig_sms_parts[] = "('INC', '" . $trans_id . "', " . $v['status_level'] . ", '" . $item[0]['prod_type'] . "', '" . $v['offer_purchased_date'] . "', '', '"
                            . $item[0]['brand_id'] . "', '" . $item[0]['model_id'] . "', " . $item[0]['unit_price'] . ", '" .
                            $trans_id . "', '" . $v['offer_purchased_date'] . "', " . $v['offer_valid_month'] . ", '" .
                            $v['offer_reg_date'] . "', '" . $offer_start_date . "', '" . $offer_expiry_date . "', '" .
                            $v['offer_item_id'] . "', '', " . $v['offer_cost'] . ", '" . $v['offer_invoice_id'] . "', '', '1900-01-01', " .
                            $item[0]['unit_price'] . ", 0, '" . $v['offer_reg_date'] . "', '" . $v['offer_start_date'] . "', '" .
                            $v['offer_expiry_date'] . "', '1900-01-01', '1990-01-01', '"  . $item[0]['ext_warranty'] . "', '', '', '', '"  . $v['string_udf5'] . "', 'cherps', '" .
                            $now . "', 'cherps', '" . $now . "')";
                        $sms_invoice_id[] = $v['offer_invoice_id'];
                    }
                }
                else {

                    $items = $this->db->query("SELECT
                                                DISTINCT
                                                b.lot_id,i.inv_dim3 prod_type, i.brand_id brand_id, i.model_id model_id,
                                                e.unit_price - (e.disc_amount / e.qty_invoiced) unit_price,
                                                (CASE WHEN (rtrim(i.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(i.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty
                                                FROM sms_invoice_item a
                                                join sms_invoice_lot b on a.invoice_id=b.invoice_id and a.line_num=b.line_num
                                                left join sms_invoice_lot c on b.invoice_id=c.invoice_id and b.lot_id=c.lot_id and b.line_num<>c.line_num
                                                left join sms_invoice_item d on c.invoice_id=d.invoice_id and c.line_num=d.line_num
                                                left join sms_invoice_item e on e.invoice_id=a.invoice_id and e.line_num=1
                                                left join ims_item_list i ON i.item_id = e.item_id
                                                WHERE a.invoice_id = '" . $v['offer_invoice_id'] . "' and a.item_id='" . $v['offer_item_id'] . "'")->result_array();
//                    if (empty($items)) {
//                        $items = $this->db->query("SELECT mst.inv_dim3 prod_type, mst.brand_id brand_id, mst.model_id model_id, i.unit_price - (i.disc_amount / i.qty_invoiced) unit_price, (CASE WHEN (ISNUMERIC(mst.ext_warranty) = 1) THEN CAST(mst.ext_warranty AS FLOAT) ELSE 0 END) * 12 ext_warranty
//                                      FROM ims_item_list mst JOIN sms_invoice_item i ON i.item_id = mst.item_id AND i.coy_id = mst.coy_id
//                                      LEFT JOIN sms_invoice_lot l ON l.invoice_id = i.invoice_id
//                                      WHERE i.invoice_id = '" . $v['offer_invoice_id'] . "' AND i.line_num = 1")->result_array();
//                    }
                    foreach ($items as $item) {
                        $trans_id = $this->_next_line($trans_prefix);
                        $offer_start_date = $start_dt->modify('+' . (string)$item['ext_warranty'] . ' month')->format('Y-m-d');
                        $offer_expiry_date = $expiry_dt->modify('+' . (string)$item['ext_warranty'] . ' month -1 day')->format('Y-m-d');
                        $insert_sql = "insert into svs_customer_list (coy_id, cust_id, cust_title, first_name, last_name, contact_num, email_addr, cust_ref_id, created_by, created_on, modified_by, modified_on) values
                                  ('INC', '". $trans_id ."', '-', '".$first_name."', '".$last_name."', '".$v['tel_code']."', '".$v['email_addr']."', '".$v['cust_id']."', 'cherps', '".$now."', 'cherps', '".$now."')";
                        $this->db->query($insert_sql);
                        $mig_sms_parts[] = "('INC', '" . $trans_id . "', " . $v['status_level'] . ", '" . $item['prod_type'] . "', '" . $v['offer_purchased_date'] . "', '" .
                            $item['lot_id'] . "', '" . $item['brand_id'] . "', '" . $item['model_id'] . "', " . $item['unit_price'] . ", '" .
                            $trans_id . "', '" . $v['offer_purchased_date'] . "', " . $v['offer_valid_month'] . ", '" .
                            $v['offer_reg_date'] . "', '" . $offer_start_date . "', '" . $offer_expiry_date . "', '" .
                            $v['offer_item_id'] . "', '', " . $v['offer_cost'] . ", '" . $v['offer_invoice_id'] . "', '', '1900-01-01', " .
                            $item['unit_price'] . ", 0, '" . $v['offer_reg_date'] . "', '" . $v['offer_start_date'] . "', '" .
                            $v['offer_expiry_date'] . "', '1900-01-01', '1990-01-01', '"  . $item['ext_warranty'] . "', '', '', '', '"  . $v['string_udf5'] . "', 'cherps', '" .
                            $now . "', 'cherps', '" . $now . "')";
                        $sms_invoice_id[] = $v['offer_invoice_id'];
                    }
                }
            }
            if (!empty($mig_sms_parts)) {
                $mig_sms_sew .= implode(',', $mig_sms_parts);
                $result_sms = $this->db->query($mig_sms_sew);
                if ($result_sms) {
                    $rs = array(
                        "status" => 'Success',
                        "status_code" => 200,
                    );
                } else {
                    $rs = array(
                        "status" => 'Fail',
                        "status_code" => 400,
                    );
                }
            } else {
                $rs = array(
                    "status" => 'No sms invoice.',
                    "status_code" => 200,
                );
            }

            // update sms status
            $update_status = "
            SELECT 
            b1.status_level status_level,
            (SELECT lot_id FROM sms_invoice_lot B3 WHERE B2.invoice_id = B3.invoice_id AND B2.coy_id = B3.coy_id limit 1) serial_num,
            B2.item_id offer_item_id,
            B2.invoice_id offer_invoice_id,
            B2.qty_invoiced
            FROM
				sms_invoice_list B1, sms_invoice_item B2, ims_item_list d1 
            WHERE
				( B2.status_level >= 2 ) 
				AND ( B2.status_level >= 0 ) 
				AND ( B2.modified_on >= '$yesterday' ) 
				AND ( B2.modified_on < '$today' ) 
				AND ( B2.created_on < '$yesterday')
				AND ( B2.qty_invoiced > 0 )
				AND (B1.invoice_id = B2.invoice_id AND B1.coy_id = B2.coy_id)
				AND (B2.item_id = d1.item_id AND B2.coy_id = d1.coy_id AND d1.item_type = 'W')
            ";
            $res_update = $this->db->query($update_status)->result_array();
            foreach ($res_update as $u) {
                if ($u['qty_invoiced'] > 1) {
                    $res_items = $this->db->query("SELECT 
                                                    b.lot_id
                                                    FROM sms_invoice_item a
                                                    join sms_invoice_lot b on a.invoice_id=b.invoice_id and a.line_num=b.line_num
                                                    left join sms_invoice_lot c on b.invoice_id=c.invoice_id and b.lot_id=c.lot_id and b.line_num<>c.line_num
                                                    left join sms_invoice_item d on c.invoice_id=d.invoice_id and c.line_num=d.line_num
                                                    left join sms_invoice_item e on e.invoice_id=a.invoice_id and e.line_num=1
                                                    left join ims_item_list i ON i.item_id = e.item_id
                                                    WHERE a.invoice_id = '" . $u['offer_invoice_id'] . "' and a.item_id='".$u['offer_item_id']."'")->result_array();
                    if (!empty($res_items) && count($res_items) === (int)$u['qty_invoiced']) {
                        $res_offer_lists = $this->db->query("select offer_id from svs_offer_list where offer_invoice_id = '" . $u['offer_invoice_id'] . "'")->result_array();
                        if (!empty($res_offer_lists) && count($res_offer_lists) === (int)$u['qty_invoiced']) {
                            foreach (range(0, (int)$u['qty_invoiced'] - 1) as $l) {
                                $this->db->query("update svs_offer_list set serial_num = '" . $res_items[$l]['lot_id'] . "' where coy_id = 'INC' and offer_id = '" . $res_offer_lists[$l]['offer_id'] . "'");
                            }
                        }
                    }
                } else {
                    $this->db->query("update svs_offer_list set serial_num = '" . $u['serial_num'] . "' where coy_id = 'INC' and offer_item_id = '".$u['offer_item_id']."' and offer_invoice_id = '" . $u['offer_invoice_id'] . "'");
                }
            }
            $delete_status = "
            SELECT 
            b1.status_level status_level,
            B2.item_id offer_item_id,
            B2.invoice_id offer_invoice_id
            FROM
				sms_invoice_list B1, sms_invoice_item B2, ims_item_list d1 
            WHERE
				( B2.status_level < 0 ) 
				AND ( B2.modified_on >= '$yesterday' ) 
				AND ( B2.modified_on < '$today' ) 
				AND ( B2.created_on < '$yesterday')
				AND ( B2.qty_invoiced > 0 )
				AND (B1.invoice_id = B2.invoice_id AND B1.coy_id = B2.coy_id)
				AND (B2.item_id = d1.item_id AND B2.coy_id = d1.coy_id AND d1.item_type = 'W')
            ";
            $res_delete = $this->db->query($delete_status)->result_array();
            foreach ($res_delete as $d) {
                $this->db->query("update svs_offer_list set status_level = -1 where coy_id = 'INC' and offer_item_id = '".$d['offer_item_id']."' and offer_invoice_id = '" . $d['offer_invoice_id'] . "'");
            }

            // update pos status
            $update_pos_status = "
                            SELECT DISTINCT
                            A2.item_id offer_item_id,
                            A2.trans_id offer_invoice_id
                            FROM
                            pos_transaction_list A1, pos_transaction_item A2, pos_transaction_lot A3, ims_item_list c1
                            WHERE
                            ( A2.coy_id = A1.coy_id ) and
                            ( A2.trans_id = A1.trans_id ) and
                            ( A2.coy_id = A3.coy_id ) and
                            ( A2.trans_id = A3.trans_id ) and
                            ( A2.line_num = A3.line_num ) and
                            ( c1.coy_id = A2.coy_id and c1.item_id = A2.item_id and c1.item_type = 'W' ) and
                            ( A1.status_level > 0 ) AND
                            ( A2.status_level > 0 ) AND
                            ( A2.csd_qty > 0 ) AND
                            ( A2.modified_on >= '$yesterday' ) AND
                            ( A2.modified_on < '$today') AND
                            ( A2.created_on < '$yesterday') AND
                            ( A3.status_level > 0 ) AND
                            ( A3.string_udf2 <> '') AND
                            ( string_udf1 > ''  and left(string_udf1,2)<>'OP' and string_udf1<>'3rd Party' and A3.coy_id='CTL')
                        ";
            $res_pos_status = $this->db->query($update_pos_status)->result_array();
            foreach ($res_pos_status as $d) {
                $this->db->query("update svs_offer_list set status_level = -1 where coy_id = 'INC' and offer_item_id = '".$d['offer_item_id']."' and offer_invoice_id = '" . $d['offer_invoice_id'] . "'");
            }

            $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('INC','SSEW-MIGRATE','".md5('SSEW-MIGRATE'.time())."','','','','','"
                .json_encode($pos_trans_id) . json_encode($sms_invoice_id) . "', '". $rp['status_code'] . $rs['status_code'] ."','". $rp['status'] . $rs['status'] . "','','".getRealIpAddr(15)."',now())";
            $this->db->query($sql);
            return array($rp, $rs);
        }
    }

    public function ssew_single($id=null)
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return array('return'=>'0','message'=>'Request code is error, please check your request code.');
        } else {
            $now = date("Y-m-d H:i:s");
            // Get all ssew transaction from pos_transaction
            $cur_year = date("Y");
            $trans_prefix = 'EW' . $cur_year;
            if (substr($id, 0, 2) !== 'HI' && substr($id, 0, 2) !== 'IC') {
                $get_pos_sew = "WITH valid_month AS (SELECT set_data FROM coy_setting_list WHERE sys_id = 'SEW' AND set_code = 'EW_MTH' AND coy_id = 'CTL')
                                SELECT
                                1 as status_level,
                                ( SELECT inv_dim3 FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2 ) prod_type,
                                A3.lot_id serial_num,
                                ( SELECT brand_id FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2 ) brand_id,
                                ( SELECT model_id FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2 ) model_id,
                                COALESCE((
                                SELECT unit_price - ( disc_amount / trans_qty )
                                FROM
                                pos_transaction_item itm
                                WHERE
                                itm.coy_id = A3.coy_id
                                AND itm.trans_id = A3.string_udf1
                                AND itm.item_id = A3.string_udf2
                                AND itm.status_level >= 0
                                LIMIT 1
                                ),0) unit_price,
                                to_char(A1.trans_date, 'YYYY-MM-DD') offer_purchased_date,
                                valid_month.set_data as offer_valid_month,
                                to_char(A1.trans_date, 'YYYY-MM-DD') offer_reg_date,
                                to_char(A1.trans_date + interval '1 month' * (SELECT (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2), 'YYYY-MM-DD') offer_start_date,
                                to_char(A1.trans_date + interval '1 month' * (cast(valid_month.set_data as FLOAT) + (SELECT (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2)) + interval '-1 day', 'YYYY-MM-DD') offer_expiry_date,
                                A2.item_id offer_item_id,
                                A2.trans_cost AS offer_cost,
                                A2.trans_id offer_invoice_id,
                                concat_ws('/', A1.coy_id, A1.loc_id) string_udf5,
                                (SELECT (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty FROM ims_item_list mst WHERE mst.coy_id = A3.coy_id AND mst.item_id = A3.string_udf2) string_udf1,
                                rtrim(mbr_id) mbr_id,
                                cust_name,
                                tel_code,
                                email_addr
                                FROM
                                pos_transaction_list A1, pos_transaction_item A2, pos_transaction_lot A3, ims_item_list c1, valid_month
                                WHERE
                                ( A2.coy_id = A1.coy_id ) and
                                ( A2.trans_id = A1.trans_id ) and
                                ( A2.coy_id = A3.coy_id ) and
                                ( A2.trans_id = A3.trans_id ) and
                                ( A2.line_num = A3.line_num ) and
                                ( c1.coy_id = A2.coy_id and c1.item_id = A2.item_id and c1.item_type = 'W' ) and
                                ( A1.status_level > 0 ) AND
                                ( A2.status_level > 0 ) AND
                                ( A1.trans_id = '$id' ) AND
                                ( A3.status_level > 0 ) AND
                                ( A2.csd_qty < A2.trans_qty ) AND
                                ( A3.string_udf2 <> '') AND
                                ( string_udf1 > ''  and left(string_udf1,2)<>'OP' and string_udf1<>'3rd Party' and A3.coy_id='CTL')
                                ";
                $res_pos_sew = $this->db->query($get_pos_sew)->result_array();
                $mig_pos_sew = 'INSERT INTO svs_offer_list (coy_id,offer_id,status_level,product_type,product_purchased_date,serial_num,brand_id,model_id,unit_price,cust_id,offer_purchased_date,offer_valid_month,offer_reg_date,offer_start_date,offer_expiry_date,offer_item_id,offer_provider,offer_cost,offer_invoice_id,offer_sub_type,offer_refunded_date,claimable_amount,claimed_amount,date_udf1,date_udf2,date_udf3,date_udf4,date_udf5,string_udf1,string_udf2,string_udf3,string_udf4,string_udf5,created_by,created_on,modified_by,modified_on) VALUES ';
                $mig_pos_parts = array();
                $pos_trans_id = array();
                $is_exists = $this->db->query("select 1 from sys_trans_list where coy_id = 'INC' and sys_id = 'SEW' and trans_prefix = '" . $trans_prefix . "'")->num_rows();
                if ($is_exists === 0) {
                    $this->db->query("insert into sys_trans_list (coy_id, sys_id, trans_prefix, next_num, sys_date) VALUES ('INC', 'SEW', '". $trans_prefix ."', 1, '". $now ."')");
                }
                foreach ($res_pos_sew as $v){
                    $trans_id = $this->_next_line($trans_prefix);
                    $first_name = explode(' ', $v['cust_name'])[0];
                    $last_name = isset(explode(' ', $v['cust_name'])[1]) ? explode(' ', $v['cust_name'])[1] : '';
                    $this->db->query("insert into svs_customer_list (coy_id, cust_id, cust_title, first_name, last_name, contact_num, email_addr, cust_ref_id, created_by, created_on, modified_by, modified_on) values 
                                      ('INC', '". $trans_id ."', '-', '".$first_name."', '".$last_name."', '".$v['tel_code']."', '".$v['email_addr']."', '".$v['mbr_id']."', 'cherps', '".$now."', 'cherps', '".$now."')");
                    $mig_pos_parts[] = "('INC', '" . $trans_id . "', " . $v['status_level'] . ", '" . $v['prod_type'] . "', '" . $v['offer_purchased_date'] . "', '" .
                        $v['serial_num'] . "', '" . $v['brand_id'] . "', '" . $v['model_id'] . "', " . $v['unit_price'] . ", '" .
                        $trans_id . "', '" . $v['offer_purchased_date'] . "', " . $v['offer_valid_month'] . ", '" .
                        $v['offer_reg_date'] . "', '" . $v['offer_start_date'] . "', '" . $v['offer_expiry_date'] . "', '" .
                        $v['offer_item_id'] . "', '', " . $v['offer_cost'] . ", '" . $v['offer_invoice_id'] . "', '', '1900-01-01', " .
                        $v['unit_price'] . ", 0, '" . $v['offer_reg_date'] . "', '" . $v['offer_start_date'] . "', '" .
                        $v['offer_expiry_date'] . "', '1900-01-01', '1990-01-01', '"  . $v['string_udf1'] . "', '', '', '', '"  . $v['string_udf5'] . "', 'cherps', '" .
                        $now . "', 'cherps', '" . $now . "')";
                    $pos_trans_id[] = $v['offer_invoice_id'];
                }
                if (!empty($mig_pos_parts)){
                    $mig_pos_sew .= implode(',', $mig_pos_parts);
                    $result_pos = $this->db->query($mig_pos_sew);
                    if ($result_pos) {
                        $rp = array(
                            "status" => 'Success',
                            "status_code" => 200,
                        );
                    } else {
                        $rp = array(
                            "status" => 'Fail',
                            "status_code" => 400,
                        );
                    }
                } else {
                    $rp = array(
                        "status" => 'No pos transaction',
                        "status_code" => 200,
                    );
                }
            }
            else {
                // Get all ssew from sms_invoice
                $get_sms_sew = "
                                WITH valid_month AS (SELECT set_data FROM coy_setting_list WHERE sys_id = 'SEW' AND set_code = 'EW_MTH' AND coy_id = 'CTL')
                                SELECT
                                1 status_level,
                                to_char(B1.invoice_date, 'YYYY-MM-DD') offer_purchased_date,
                                valid_month.set_data offer_valid_month,
                                to_char(B1.invoice_date, 'YYYY-MM-DD') offer_reg_date,
                                to_char(B1.invoice_date, 'YYYY-MM-DD') offer_start_date,
                                to_char(B1.invoice_date + interval '1 month' * cast(valid_month.set_data AS FLOAT), 'YYYY-MM-DD') offer_expiry_date,
                                B2.item_id offer_item_id,
                                B2.unit_price*0.7/1.07 AS offer_cost,
                                B2.invoice_id offer_invoice_id,
                                concat_ws('/',B1.coy_id, B1.loc_id) string_udf5,
                                rtrim(cust_id) cust_id,
                                cust_name,
                                tel_code,
                                email_addr,
                                B2.qty_invoiced
                                FROM
                                sms_invoice_list B1, sms_invoice_item B2, ims_item_list d1, valid_month
                                WHERE
                                ( B1.status_level > 0 )
                                AND ( B2.status_level >= 0 )
                                AND ( B1.invoice_id = '$id' )
                                AND ( B2.qty_invoiced > 0 )
                                AND (B1.invoice_id = B2.invoice_id AND B1.coy_id = B2.coy_id)
                                AND (B2.item_id = d1.item_id AND B2.coy_id = d1.coy_id AND d1.item_type = 'W')
                                ";
                $res_sms_sew = $this->db->query($get_sms_sew)->result_array();
                $mig_sms_sew = 'INSERT INTO svs_offer_list (coy_id,offer_id,status_level,product_type,product_purchased_date,serial_num,brand_id,model_id,unit_price,cust_id,offer_purchased_date,offer_valid_month,offer_reg_date,offer_start_date,offer_expiry_date,offer_item_id,offer_provider,offer_cost,offer_invoice_id,offer_sub_type,offer_refunded_date,claimable_amount,claimed_amount,date_udf1,date_udf2,date_udf3,date_udf4,date_udf5,string_udf1,string_udf2,string_udf3,string_udf4,string_udf5,created_by,created_on,modified_by,modified_on) VALUES ';
                $mig_sms_parts = array();
                $sms_invoice_id = array();
                foreach ($res_sms_sew as $v){
                    $exists_lot = $this->db->query("SELECT 1 FROM sms_invoice_lot WHERE invoice_id = '".$v['offer_invoice_id']."'")->num_rows();
                    $first_name = explode(' ', $v['cust_name'])[0];
                    $last_name = isset(explode(' ', $v['cust_name'])[1]) ? explode(' ', $v['cust_name'])[1] : '';
                    $start_dt = new DateTime($v['offer_start_date']);
                    $expiry_dt = new DateTime($v['offer_expiry_date']);

                    if ($exists_lot === 0) {
                        $item = $this->db->query("SELECT mst.inv_dim3 prod_type, mst.brand_id brand_id, mst.model_id model_id, i.unit_price - (i.disc_amount / i.qty_invoiced) unit_price, (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty
                                                    FROM ims_item_list mst JOIN sms_invoice_item i ON i.item_id = mst.item_id AND i.coy_id = mst.coy_id
                                                    WHERE i.invoice_id = '" . $v['offer_invoice_id'] . "' AND i.line_num = 1")->result_array();
                        $offer_start_date = $start_dt->modify('+' . (string)$item[0]['ext_warranty'] . ' month')->format('Y-m-d');
                        $offer_expiry_date = $expiry_dt->modify('+' . (string)$item[0]['ext_warranty'] . ' month -1 day')->format('Y-m-d');
                        foreach (range(1, $v['qty_invoiced']) as $i) {
                            $trans_id = $this->_next_line($trans_prefix);
                            $insert_sql = "insert into svs_customer_list (coy_id, cust_id, cust_title, first_name, last_name, contact_num, email_addr, cust_ref_id, created_by, created_on, modified_by, modified_on) values
                                  ('INC', '". $trans_id ."', '-', '".$first_name."', '".$last_name."', '".$v['tel_code']."', '".$v['email_addr']."', '".$v['cust_id']."', 'cherps', '".$now."', 'cherps', '".$now."')";
                            $this->db->query($insert_sql);
                            $mig_sms_parts[] = "('INC', '" . $trans_id . "', " . $v['status_level'] . ", '" . $item[0]['prod_type'] . "', '" . $v['offer_purchased_date'] . "', '', '"
                                . $item[0]['brand_id'] . "', '" . $item[0]['model_id'] . "', " . $item[0]['unit_price'] . ", '" .
                                $trans_id . "', '" . $v['offer_purchased_date'] . "', " . $v['offer_valid_month'] . ", '" .
                                $v['offer_reg_date'] . "', '" . $offer_start_date . "', '" . $offer_expiry_date . "', '" .
                                $v['offer_item_id'] . "', '', " . $v['offer_cost'] . ", '" . $v['offer_invoice_id'] . "', '', '1900-01-01', " .
                                $item[0]['unit_price'] . ", 0, '" . $v['offer_reg_date'] . "', '" . $v['offer_start_date'] . "', '" .
                                $v['offer_expiry_date'] . "', '1900-01-01', '1990-01-01', '"  . $item[0]['ext_warranty'] . "', '', '', '', '"  . $v['string_udf5'] . "', 'cherps', '" .
                                $now . "', 'cherps', '" . $now . "')";
                            $sms_invoice_id[] = $v['offer_invoice_id'];
                        }
                    }
                    else {

                        $items = $this->db->query("SELECT
                                                    DISTINCT
                                                    b.lot_id,i.inv_dim3 prod_type, i.brand_id brand_id, i.model_id model_id,
                                                    e.unit_price - (e.disc_amount / e.qty_invoiced) unit_price,
                                                    (CASE WHEN (rtrim(i.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(i.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty
                                                    FROM sms_invoice_item a
                                                    join sms_invoice_lot b on a.invoice_id=b.invoice_id and a.line_num=b.line_num
                                                    left join sms_invoice_lot c on b.invoice_id=c.invoice_id and b.lot_id=c.lot_id and b.line_num<>c.line_num
                                                    left join sms_invoice_item d on c.invoice_id=d.invoice_id and c.line_num=d.line_num
                                                    left join sms_invoice_item e on e.invoice_id=a.invoice_id and e.line_num=1
                                                    left join ims_item_list i ON i.item_id = e.item_id
                                                    WHERE a.invoice_id = '" . $v['offer_invoice_id'] . "' and a.item_id='" . $v['offer_item_id'] . "'")->result_array();
                        if (empty($items)) {
                            $items = $this->db->query("SELECT mst.inv_dim3 prod_type, mst.brand_id brand_id, mst.model_id model_id, i.unit_price - (i.disc_amount / i.qty_invoiced) unit_price, (CASE WHEN (rtrim(mst.ext_warranty) ~ '^[0-9\.]+$') THEN CAST(rtrim(mst.ext_warranty) AS FLOAT) ELSE 0 END) * 12 ext_warranty
                                                        FROM ims_item_list mst JOIN sms_invoice_item i ON i.item_id = mst.item_id AND i.coy_id = mst.coy_id
                                                        LEFT JOIN sms_invoice_lot l ON l.invoice_id = i.invoice_id
                                                        WHERE i.invoice_id = '" . $v['offer_invoice_id'] . "' AND i.line_num = 1")->result_array();
                        }
                        foreach ($items as $item) {
                            $trans_id = $this->_next_line($trans_prefix);
                            $offer_start_date = $start_dt->modify('+' . (string)$item['ext_warranty'] . ' month')->format('Y-m-d');
                            $offer_expiry_date = $expiry_dt->modify('+' . (string)$item['ext_warranty'] . ' month -1 day')->format('Y-m-d');
                            $insert_sql = "insert into svs_customer_list (coy_id, cust_id, cust_title, first_name, last_name, contact_num, email_addr, cust_ref_id, created_by, created_on, modified_by, modified_on) values
                                  ('INC', '". $trans_id ."', '-', '".$first_name."', '".$last_name."', '".$v['tel_code']."', '".$v['email_addr']."', '".$v['cust_id']."', 'cherps', '".$now."', 'cherps', '".$now."')";
                            $this->db->query($insert_sql);
                            $mig_sms_parts[] = "('INC', '" . $trans_id . "', " . $v['status_level'] . ", '" . $item['prod_type'] . "', '" . $v['offer_purchased_date'] . "', '" .
                                $item['lot_id'] . "', '" . $item['brand_id'] . "', '" . $item['model_id'] . "', " . $item['unit_price'] . ", '" .
                                $trans_id . "', '" . $v['offer_purchased_date'] . "', " . $v['offer_valid_month'] . ", '" .
                                $v['offer_reg_date'] . "', '" . $offer_start_date . "', '" . $offer_expiry_date . "', '" .
                                $v['offer_item_id'] . "', '', " . $v['offer_cost'] . ", '" . $v['offer_invoice_id'] . "', '', '1900-01-01', " .
                                $item['unit_price'] . ", 0, '" . $v['offer_reg_date'] . "', '" . $v['offer_start_date'] . "', '" .
                                $v['offer_expiry_date'] . "', '1900-01-01', '1990-01-01', '"  . $item['ext_warranty'] . "', '', '', '', '"  . $v['string_udf5'] . "', 'cherps', '" .
                                $now . "', 'cherps', '" . $now . "')";
                            $sms_invoice_id[] = $v['offer_invoice_id'];
                        }
                    }
                }
                if (!empty($mig_sms_parts)) {
                    $mig_sms_sew .= implode(',', $mig_sms_parts);
                    $result_sms = $this->db->query($mig_sms_sew);
                    if ($result_sms) {
                        $rs = array(
                            "status" => 'Success',
                            "status_code" => 200,
                        );
                    } else {
                        $rs = array(
                            "status" => 'Fail',
                            "status_code" => 400,
                        );
                    }
                } else {
                    $rs = array(
                        "status" => 'No sms invoice.',
                        "status_code" => 200,
                    );
                }
            }

//            $sql = "INSERT INTO b2b_gateway_message
//                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
//                        VALUES ('INC','SSEW-SI','".md5('SSEW-MIGRATE'.time())."','','','','','"
//                .json_encode($pos_trans_id) . json_encode($sms_invoice_id) . "', '". $rp['status_code'] . $rs['status_code'] ."','". $rp['status'] . $rs['status'] . "','','".getRealIpAddr(15)."',GETDATE())";
//            $this->db->query($sql);
            return array($rp, $rs);
        }
    }

    /**
     * @api {post} /api/ssew_trans Save SSEW
     * @apiVersion 1.0.0
     * @apiGroup SSEW
     *
     * @apiParam {string} invoice_id Invoice ID
     * @apiParam {string} offer_type Offer Type. 'EX','RF','HR' and 'HX' for refund, others for normal.
     * @apiParam {string} [rf_invoice_id] Refund invoice id. Optional, but if offer_type is refund, required.
     * @apiParam {string} offer_date Offer date
     * @apiParam {string} coy_id Coy ID
     * @apiParam {string} loc_id Loc ID
     * @apiParam {Object[]} offers Offers
     * @apiParam {Object} offers.item Item
     * @apiParam {Object} offers.item.prod_type Product_type inv_dim3 in ims_item_list
     * @apiParam {Object} offers.item.serial_num Serial_number lot_id in pos_transaction_lot
     * @apiParam {Object} offers.item.brand_id Brand ID brand_id in ims_item_list
     * @apiParam {Object} offers.item.model_id Model ID model_id in ims_item_list
     * @apiParam {Object} offers.item.unit_price Unit Price trans_cost in pos_transaction_item
     * @apiParam {Object} offers.item.ext_warranty Ext warranty (ext_warranty in ims_item_list)*12
     * @apiParam {Object} offers.ssew SSEW
     * @apiParam {Object} offers.ssew.offer_id SSEW item_id
     * @apiParam {Object} offers.ssew.offer_cost cost. (unit_price - ( disc_amount / trans_qty )) in pos_transaction_item
     * @apiParam {Object} offers.ssew.ew_month SSEW month
     * @apiParam {Object} cust_info Customer info
     * @apiParam {Object} cust_info.mbr_id Mbr Id
     * @apiParam {Object} cust_info.first_name First Name
     * @apiParam {Object} cust_info.last_name Last Name
     * @apiParam {Object} cust_info.email_addr Email
     * @apiParam {Object} cust_info.contact_num Contact
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "invoice_id": "W2C888888",
     *      "offer_type": "HI",
     *      "rf_invoice_id": "W2C888888",
     *      "offer_date": "2020-07-08 13:00:12",
     *      "coy_id": "CTL",
     *      "loc_id": "WP",
     *      "offers": [{
     *          "item": {
     *              "prod_type": "NOTEBOOK",
     *              "serial_num": "L6N0CX00F25423H",
     *              "brand_id": "ASUS",
     *              "model_id": "X509MA-EJ029T",
     *              "unit_price": "699",
     *              "ext_warranty": "12"
     *          },
     *          "ssew": {
     *              "offer_id": "SSEW",
     *              "offer_cost": 100,
     *              "ew_month": 24
     *          }
     *      },],
     *      "cust_info": {
     *          "mbr_id": "V183535400",
     *          "first_name": "Test",
     *          "last_name": "Test",
     *          "email_addr": "test@test.sg",
     *          "contact_num": "32132132"
     *      }
     *  }
     *
     * @apiSuccessExample success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Success add ssew",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample server error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Whoops! There was a problem processing your ssew. Please try again."
     *  }
     *
     */
    public function ssew_trans($invoice_id, $offer_type, $rf_invoice_id, $offer_date, $coy_id, $loc_id, $offers, $cust_info)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);

        $now = date("Y-m-d H:i:s");
        $offer_date = new DateTime($offer_date);
        $reg_date = $offer_date->format('Y-m-d');
        $cur_year = date("Y");
        $trans_prefix = 'EW' . $cur_year;
        if ($offer_type === "HR" || $offer_type === 'HX' || $offer_type === 'EX' || $offer_type === 'RF') {
            $udf4 = 'CTL/' . $loc_id;
            $update_offer = "update svs_offer_list set status_level = -1, offer_refunded_date = '$reg_date', string_udf2 = '$invoice_id', string_udf3 = '$offer_type', string_udf4= '$udf4', modified_on = '$now' where offer_invoice_id = '$rf_invoice_id'";
            $this->db->query($update_offer);
        } else {
            $insert_sew = 'INSERT INTO svs_offer_list (coy_id,offer_id,status_level,product_type,product_purchased_date,serial_num,brand_id,model_id,unit_price,cust_id,offer_purchased_date,offer_valid_month,offer_reg_date,offer_start_date,offer_expiry_date,offer_item_id,offer_provider,offer_cost,offer_invoice_id,offer_sub_type,offer_refunded_date,claimable_amount,claimed_amount,date_udf1,date_udf2,date_udf3,date_udf4,date_udf5,string_udf1,string_udf2,string_udf3,string_udf4,string_udf5,created_by,created_on,modified_by,modified_on) VALUES ';
            $insert_sew_parts = array();
            foreach ($offers as $offer) {
                $next_num = $this->db->query("select next_num from sys_trans_list where coy_id = 'INC' and sys_id = 'SEW' and trans_prefix = '" . $trans_prefix . "'")->result_array();
                $offer_id = $trans_prefix . sprintf("%09s", $next_num[0]['next_num']);
                $this->db->query("update sys_trans_list set next_num = " . $next_num[0]['next_num'] . "+1 where coy_id = 'INC' and sys_id = 'SEW' and trans_prefix = '" . $trans_prefix . "'");
                $insert_sql = "insert into svs_customer_list (coy_id, cust_id, cust_title, first_name, last_name, contact_num, email_addr, cust_ref_id, created_by, created_on, modified_by, modified_on) values ('INC', '". $offer_id ."', '-', '".$cust_info['first_name']."', '".$cust_info['last_name']."', '".$cust_info['contact_num']."', '".$cust_info['email_addr']."', '".$cust_info['mbr_id']."', 'cherps', '".$now."', 'cherps', '".$now."')";
                $this->db->query($insert_sql);
                $ext_warranty = is_numeric($offer['item']['ext_warranty']) ? (int)($offer['item']['ext_warranty']) : 0;
                $ew_month = is_numeric($offer['ssew']['ew_month']) ? (int)$offer['ssew']['ew_month'] : 0;
                $offer_date_cp = $offer_date;
                $offer_date_cp1 = $offer_date;
                $start_date = $offer_date_cp->modify('+' . (string)$ext_warranty . ' month')->format('Y-m-d');
                $expiry_date = $offer_date_cp1->modify('+' . (string)$ext_warranty . '+' . (string)$ew_month . ' month -1 day')->format('Y-m-d');
                $udf5 = $coy_id . '/' . $loc_id;
                $insert_sew_parts[] = "('INC', '" . $offer_id . "',  1, '" . $offer['item']['prod_type'] . "', '" . $reg_date . "', '" .
                    $offer['item']['serial_num'] . "', '" . $offer['item']['brand_id'] . "', '" . $offer['item']['model_id'] . "', " . $offer['item']['unit_price'] . ", '" .
                    $offer_id . "', '" . $reg_date . "', " . $offer['ssew']['ew_month'] . ", '" .
                    $reg_date . "', '" . $start_date . "', '" . $expiry_date . "', '" .
                    $offer['ssew']['offer_id'] . "', '', " . $offer['ssew']['offer_cost'] . ", '" . $invoice_id . "', '', '1900-01-01', " .
                    $offer['item']['unit_price'] . ", 0, '" . $reg_date . "', '" . $start_date . "', '" .
                    $expiry_date . "', '1900-01-01', '1990-01-01', '"  . $offer['item']['ext_warranty'] . "', '', '', '', '"  . $udf5 . "', 'cherps', '" .
                    $now . "', 'cherps', '" . $now . "')";
            }
            if (!empty($insert_sew_parts)) {
                $insert_sew .= implode(',', $insert_sew_parts);
                $this->db->query($insert_sew);
            }
        }


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function credit_migrate($id, $refundMode)
    {
        $this->msdb = $this->load->database('cherps_hachi', TRUE);
        $now = date("Y-m-d H:i:s");
        $date_expiry = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")."+90 days"));

        if (substr($id, 0, 2) !== 'HX' && substr($id, 0, 2) !== 'HR') {
            return json_encode(array(
                "status_code" => 400,
                "message" => "Invoice id should begin with 'HX' or 'HR'"
            ));
        }

        $trans_list = $this->db->query("select rtrim(loc_id) loc_id,inv_type, rtrim(cust_id) cust_id, cust_po_code, rtrim(created_by) created_by, to_char(created_on,'YYYY-MM-DD HH24:MI:SS') created_on, delv_mode_id from sms_invoice_list where invoice_id = '".$id."' and status_level < 2 and status_level >= 0")->first_row();

        if (!$trans_list) {
            return json_encode(array(
                "status_code" => 400,
                "message" => "Status level should in (0, 1) or invoice not exists"
            ));
        }

        $mbr_id = $trans_list->cust_id;
        $trans_type = $trans_list->inv_type;
        $loc_id = $trans_list->loc_id;
        $po_code = $trans_list->cust_po_code;
        $trans_date = $trans_list->created_on;
        $cashier_id = $trans_list->created_by;
        $delv_mode = $trans_list->delv_mode_id;

        $has_coupon = $this->db->query("SELECT 1 FROM sms_payment_list WHERE (pay_mode = '!VCH-CREDIT' or pay_mode = '!VCH-EGIFT' or pay_mode = 'DPD') AND invoice_id = '".$po_code."'")->num_rows();
        if ($has_coupon > 0 && $refundMode === 'original') {
            return json_encode(array(
                "status_code" => 400,
                "message" => "Coupon in the payment list, so refund mode should be 'ECREDIT'"
            ));
        }

        $trans_item = $this->db->query("select line_num, rtrim(item_id) item_id, item_desc, qty_invoiced, unit_price, disc_amount, fin_dim2 from sms_invoice_item where invoice_id = '".$id."'")->result_array();
        $items = [];
        $total_price = 0;
//        $n = 0;
        foreach ($trans_item as $i) {
            $item_id = $i['item_id'];
            $qty_invoiced = (int)$i['qty_invoiced'];
            // For sms_payment_list
            $item_price = (abs($qty_invoiced) * (float)$i['unit_price']) - abs($i['disc_amount']);
            $total_price = $total_price + $item_price;
            if (strpos($item_id, '#') !== false) {
                continue;
            }
            // For Hx, ims_inv_physical
            if (substr($id, 0, 2) === 'HX') {
                $original_qty = abs($qty_invoiced);
                $reserve = $this->db->query("SELECT * FROM ims_inv_physical WHERE item_id = '".$item_id."' AND loc_id = '".$loc_id."' ORDER BY initial_date desc")->result_array();
                if (!empty($reserve)) {
                    foreach ($reserve as $k => $v) {
                        if ($original_qty > 0) {
                            $remain_reserve = (int)$v['qty_reserved'];
                            if ($remain_reserve >= $original_qty) {
                                $this->db->query("update ims_inv_physical set qty_reserved = qty_reserved - ".$original_qty." where item_id = '".$item_id."' AND loc_id = '".$loc_id."' and line_num = ".$v['line_num']);
                                $original_qty = 0;
                            } else {
                                $this->db->query("update ims_inv_physical set qty_reserved = qty_reserved - ".$remain_reserve." where item_id = '".$item_id."' AND loc_id = '".$loc_id."' and line_num = ".$v['line_num']);
                                $original_qty = (int)$original_qty - $remain_reserve;
                            }
                        }
                    }
                }
                $this->db->query("update sms_invoice_item set status_level = -1 where invoice_id = '".$po_code."' and item_id = " . $item_id);
            }
            // For Hr, ims_inv_movement_item
            if (substr($id, 0, 2) === 'HR') {
                $unit_cost = $this->db->query("select unit_price from ims_item_price where item_id='".$item_id."' 
                                                and price_type='SUPPLIER' and eff_from<now() and eff_to>=now()
                                                 order by price_ind limit 1")->first_row()->unit_price;
                $this->db->query("INSERT INTO ims_inv_movement_item (coy_id, trans_id, line_num, movement_ind, item_id, 
                                  item_desc, uom_id, trans_qty, unit_cost, supp_csg, created_on, modified_on, approved_qty, 
                                  verified_qty) VALUES ('CTL', '".$id."', ".$i['line_num'].", '+', '".$item_id."', '".$i['item_desc']."',
                                  'pcs', ".abs($qty_invoiced).", ".$unit_cost.", 'N', '".$now."', '".$now."', 0,0) ON conflict 
                                  (coy_id, trans_id, line_num) DO NOTHING");

                $po_loc_id = $this->db->query("select rtrim(loc_id) loc_id from sms_invoice_list where invoice_id = '".$po_code."'")->first_row()->loc_id;
                $ref_id_trans = $this->_generate_trans_id($po_loc_id.$loc_id);
                $original_transaction = $this->db->query("SELECT * FROM ims_inv_transaction WHERE ref_id2 = '".$po_code."' and item_id = '".$item_id."' limit 1")->first_row();
                $ini_date = isset($original_transaction->initial_date) ? $original_transaction->initial_date : $now;
                $ref_rev = isset($original_transaction->ref_rev) ? $original_transaction->ref_rev : 0;
                $ref_num = isset($original_transaction->ref_num) ? $original_transaction->ref_num : 1;
                $trans_ref_id = isset($original_transaction->ref_id) ? $original_transaction->ref_id : $id;
                $insert_trans_sql = "INSERT INTO ims_inv_transaction (coy_id, trans_id, trans_type, status_level, trans_date,
                                             movement_ind, item_id, loc_id, supp_csg, inv_qty, inv_value, initial_date,
                                            ref_id, ref_rev, ref_num, fin_dim1, fin_dim2, created_by, created_on, modified_by,
                                             modified_on, ref_id2) VALUES ('CTL', '$ref_id_trans', 'SN', 0, '$now', '+',
                                             '$item_id', '$loc_id', 'N', ".abs($qty_invoiced).", ".$unit_cost.",
                                              '".$ini_date."', '".$trans_ref_id."', ". $ref_rev .", ".$ref_num.", '61-ONLINE SALES', '".$i['fin_dim2']."',
                                              '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."',
                                              '".$id."') ON conflict (coy_id, trans_id) DO NOTHING";
                $this->db->query($insert_trans_sql);
                $hi_ref_id = $this->db->query("select rtrim(ref_id) ref_id from ims_inv_transaction where item_id = '".$item_id."' and ref_id2 = '".$po_code."'")->first_row()->ref_id;

                $is_phy_exists = $this->db->query("select 1 from ims_inv_physical where item_id = '".$item_id."' and loc_id = '".$loc_id."' and ref_id = '".$hi_ref_id."'")->num_rows();
                if ($is_phy_exists > 0) {
                    $this->db->query("update ims_inv_physical set qty_on_hand = qty_on_hand + ".abs($qty_invoiced)." where item_id = '".$item_id."' and loc_id = '".$loc_id."' and ref_id = '".$hi_ref_id."'");
                } else {
                    $phyical_line_num = $this->db->query("select max(line_num) from ims_inv_physical where item_id = '".$item_id."' and loc_id = '".$loc_id."'")->first_row()->line_num + 1;
                    $insert_physical_sql = "INSERT INTO ims_inv_physical (coy_id, item_id, loc_id, line_num, trans_date,
                                        qty_on_hand, qty_reserved, unit_cost, supp_csg, initial_date,
                                        ref_id, ref_rev, ref_num, created_by, created_on, modified_by, modified_on, updated_on)
                                         VALUES ('CTL', '$item_id', '$loc_id', '$phyical_line_num', '$trans_date', ".abs($qty_invoiced).",
                                         0, '$unit_cost', 'N', '$trans_date', '$hi_ref_id', 0, 1, '$cashier_id', '$trans_date',
                                         '$cashier_id', '$trans_date', '$trans_date')";
                    $this->db->query($insert_physical_sql);
                }
            }

            // For crm_member
            $ms_trans = $this->msdb->query("select * from crm_member_transaction where trans_id = '".$po_code."' and item_id = '".$item_id."'")->first_row();
            $items[] = array(
                "line_num" => $i['line_num'],
                "item_id" => $item_id,
                "item_desc" => $ms_trans->item_desc,
                "qty_ordered" => $qty_invoiced,
                "unit_price" => $ms_trans->unit_price,
                "unit_discount" => $ms_trans->disc_amount,
                "regular_price" => $ms_trans->regular_price,
                "trans_point" => -(int)$ms_trans->trans_points,
                "mbr_savings" => -(float)$ms_trans->mbr_savings
            );
        }
        // Check sms_payment_list if exists !VCH-STAR001
        $star = $this->db->query("SELECT trans_amount FROM sms_payment_list WHERE invoice_id = '".$po_code."' AND pay_mode = '!VCH-STAR001'")->first_row()->trans_amount;
        if (!empty($star)) {
            $max_line = $this->db->query("SELECT max(line_num) line_num FROM sms_invoice_item WHERE invoice_id = '".$po_code."'")->first_row()->line_num;
            $points = (int) ($star * 100);
            $items[] = array(
                "line_num" => $max_line + 1,
                "item_id" => '!VCH-STAR001',
                "item_desc" => '$0.01 Star Rewards Voucher',
                "qty_ordered" => $points,
                "unit_price" => 1,
                "unit_discount" => 0,
                "regular_price" => 1,
                "trans_point" => $points,
                "mbr_savings" => 0
            );
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 2, '!VCH-HC001',
                              ".-$star.", '', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
            $total_price = $total_price - $star;
        }
        // For Hr, ims_inv_movement
        if (substr($id, 0, 2) === 'HR') {
            $this->db->query("INSERT INTO ims_inv_movement (coy_id, trans_id, move_type, status_level, loc_id, loc_to, 
                            requester_id, request_date, verifier_id, verify_date, approve_date, reason_ind, remarks, 
                            reject_notes, ack_date, created_by, created_on, modified_by, modified_on) VALUES ('CTL',
                            '".$id."', 'SN', 4, '".$loc_id."', '".$loc_id."', '".$cashier_id."', '".$now."', '".$cashier_id."',
                             '".$now."', '1900-01-01 00:00:00',
                            'Refund', '".$po_code."', '', '1900-01-01 00:00:00', '".$cashier_id."', '".$now."', '".$cashier_id."',
                             '".$now."') ON conflict (coy_id, trans_id) DO NOTHING");
        }
        // For update status of invoice_list and invoice_item
        $this->db->query("update sms_invoice_list set status_level = 2 where invoice_id = '".$id."'");
        $this->db->query("update sms_invoice_item set status_level = 2 where invoice_id = '".$id."'");

        // For sms_payment_list and crm_voucher_list
        if (strtolower($refundMode) === 'original') {
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 1, 'CP',
                              ".-$total_price.", '', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
        } else {
            $voucher_id = $this->_voucher_id();
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 1, '!VCH-CREDIT',
                              ".-$total_price.", '".$voucher_id."', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
            $this->msdb->query("INSERT INTO crm_voucher_list (coy_id, coupon_id, coupon_serialno, mbr_id, trans_date, 
                                issue_date, sale_date, utilize_date, expiry_date, print_date, receipt_id1, voucher_amount,
                                redeemed_amount, expired_amount, issue_type, issue_reason, status_level, created_by, created_on,
                                 modified_by, modified_on) VALUES ('CTL','!VCH-CREDIT','".$voucher_id."','".$mbr_id."',
                                 '".$now."', '".$now."', '".$now."', '1900-01-01 00:00:00', '".$date_expiry."', '".$now."',
                                 '".$id."', '".$total_price."', 0,0,'CREDIT', 'REFUND', 0, '".$cashier_id."', '".$now."',
                                  '".$cashier_id."', '".$now."')");
        }

        $transaction = array(
            "loc_id" => $loc_id,
            "pos_id" => "01",
            "invoice_num" => $id,
            "trans_type" => $trans_type,
            "trans_date" => $trans_date,
            "cashier_id" => $cashier_id,
            "customer_info" => array(
                "id" => $mbr_id
            ),
            "items" => $items,
            "payments" => array(
                array("pay_mode" => "CARD")
            )
        );
        $res = $this->_send_api($transaction);
        $this->_refund_email($po_code, $id, $loc_id, $items, $delv_mode, $trans_date);

        return $res;
    }

    /**
     * @api {post} /api/hachi_credit_cancel Hachi Cancel
     * @apiVersion 1.0.0
     * @apiGroup REFUND
     *
     * @apiParam {string} inv_type Invoice Type. 'HR' or 'HX'
     * @apiParam {string} cancel_mode Refund Mode. 'original or ecredit.
     * @apiParam {string} original_invoice_id Original invoice id.
     * @apiParam {string} loc_id Loc ID
     * @apiParam {string} cancel_reason Refund Reason
     * @apiParam {string} cashier_id Cashier ID
     * @apiParam {Object[]} items Items
     * @apiParam {Object} items.line_num Line number
     * @apiParam {Object} items.qty qty_invoice
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "inv_type": "HR",
     *      "cancel_mode": "original",
     *      "original_invoice_id": "HIC13840",
     *      "loc_id": "VC",
     *      "cancel_reason": "CHANGE",
     *      "cashier_id": "3810",
     *      "items": [
     *          {
     *              "line_num": 1,
     *              "qty": 1
     *          },],
     *  }
     *
     * @apiSuccessExample success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "invoice_id": "HRC01527"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     */
    public function hachi_credit_cancel($trans_type, $refund_mode, $po_code, $items, $loc_id, $refund_reason, $cashier_id, $remarks_email)
    {
        $this->msdb = $this->load->database('cherps_hachi', TRUE);
        $now = date("Y-m-d H:i:s");
        $date_expiry = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")."+90 days"));
        $has_coupon = $this->db->query("SELECT 1 FROM sms_payment_list WHERE (pay_mode = '!VCH-CREDIT' or pay_mode = '!VCH-EGIFT' or pay_mode = 'DPD') AND invoice_id = '".$po_code."'")->num_rows();
        if ($has_coupon > 0 && $refund_mode === 'original') {
            return json_encode(array(
                "status_code" => 400,
                "message" => "Coupon in the payment list, so refund mode should be 'ECREDIT'"
            ));
        }
        $id = $this->_generate_invoice_id($trans_type);
        $this->msdb->query("insert into o2o_url_key values ('".$trans_type."', '".$id."', NEWID())");
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        // Begin
        $list = $this->db->query("select * from sms_invoice_list where invoice_id = '".$po_code."'")->first_row();
//        $loc_id_from = $list->loc_id;
        $remarks = 'To cancel via '.$refund_mode.' payment mode.';
        // new refund record in sms_invoice_list
        $ref_id = $list->ref_id;
        $cust_name = $list->cust_name;
        $or_invoice_date = $list->invoice_date;
        $delv_mode = $list->delv_mode_id;
        $insert_list_sql = "INSERT INTO sms_invoice_list (coy_id, invoice_id, inv_type, invoice_date, status_level, cust_id,
                         cust_name, cust_addr, tel_code, contact_person, cust_po_code, mbr_ind, delv_date, loc_id, delv_name,
                          delv_addr, curr_id, exchg_rate, exchg_unit, payment_id, delv_term_id, delv_mode_id, tax_id,
                          tax_percent, approve_by, approve_date, remarks, ref_id, ref_rev, fault_loc, created_by,
                          created_on, modified_by, modified_on)
                        VALUES ('$list->coy_id', '$id', '$trans_type', '$now', 2, '$list->cust_id', '$list->cust_name', '$list->cust_addr',
                        '$list->tel_code', '$list->contact_person', '$po_code', '$list->mbr_ind', '1900-01-01 08:00:00',
                        '$loc_id', '$list->delv_name', 
                        '$list->delv_addr', '$list->curr_id', '$list->exchg_rate', '$list->exchg_unit', '$list->payment_id', 
                        '$list->delv_term_id', '$list->delv_mode_id', '$list->tax_id', '$list->tax_percent', '$list->approve_by', 
                        '$now','$remarks','$ref_id', '$list->ref_rev', '$list->fault_loc', '$cashier_id', '$now', '$cashier_id', '$now') ON conflict (coy_id, invoice_id) DO NOTHING";
        $this->db->query($insert_list_sql);

        $mbr_id = $list->cust_id;
        $trans_items = [];
        $total_price = 0;
        $item_ids = [];
        foreach ($items as $n=>$i) {
            $line_num = $i['line_num'];
            $qty_invoiced = -(int)$i['qty'];
            $is_exists = $this->db->query("select 1 from sms_invoice_item where coy_id = 'CTL' and ref_id = '".$po_code."' and ref_num = ".$line_num)->num_rows();
            if ($is_exists > 0) {
                return json_encode(array(
                    "status_code" => 400,
                    "message" => "Line number of ".(string)$line_num." has been cancel, please check."
                ));
            }
            $inv_item = $this->db->query("select * from sms_invoice_item where invoice_id = '".$po_code."' and line_num = ".$line_num)->first_row();
            $item_id = rtrim($inv_item->item_id);
            $item_desc = $inv_item->item_desc;
            $inv_loc = rtrim($inv_item->inv_loc);
            $item_status_level = $inv_item->status_level;
            $insert_item_sql = "INSERT INTO sms_invoice_item (coy_id, invoice_id, line_num, item_id, item_desc, qty_invoiced,
                                uom_id, unit_price, disc_amount, disc_percent, status_level, long_desc, fin_dim1,
                                fin_dim2, ref_id, ref_rev, ref_num, receipt_line, qty_picked, disc_rebate, code_send, 
                                code_attempt, delv_date, posted_on, created_by, created_on, modified_by, modified_on, loc_id)
                                VALUES ('$inv_item->coy_id', '$id', '$line_num', '$item_id', '$item_desc',
                                $qty_invoiced, '$inv_item->uom_id', '$inv_item->unit_price', '$inv_item->disc_amount', '$inv_item->disc_percent', 
                                2,'$inv_item->long_desc', '$inv_item->fin_dim1', '$inv_item->fin_dim2', '$po_code', 
                                '$inv_item->ref_rev', '$line_num', '$inv_item->receipt_line', 0, 
                                '$inv_item->disc_rebate', '$inv_item->code_send', '$inv_item->code_attempt', '$now', 
                                '$now', '$cashier_id', '$now', '$cashier_id', '$now', '$loc_id')";
            $this->db->query($insert_item_sql);

            // For sms_payment_list
            $item_price = (abs($qty_invoiced) * (float)$inv_item->unit_price) - abs($inv_item->disc_amount);
            $total_price = $total_price + $item_price;
            if (strpos($item_id, '#') !== false) {
                $this->db->query("update sms_invoice_item set status_level = -1, modified_by = '".$cashier_id."', modified_on = now() where invoice_id = '".$po_code."' and line_num = " . $line_num);
                continue;
            }
            // For Hx, ims_inv_physical
            if ($trans_type === 'HX') {
                if ($item_status_level === -1) {
                    return json_encode(array(
                        "status_code" => 400,
                        "message" => "No item exists."
                    ));
                }
                $this->db->query("update sms_invoice_item set status_level = -1, modified_by = '".$cashier_id."', modified_on = now() where invoice_id = '".$po_code."' and line_num = " . $line_num);
            }
            // For crm_member
            $item_ids[] = ['item_id'=>$item_id, 'qty_invoiced'=>$qty_invoiced, 'line_num'=>$line_num];
        }
        // Update sms_invoice_list
        $len_po_items = $this->db->query("select count(*) len from sms_invoice_item where invoice_id = '".$po_code."'")->first_row()->len;
        if (substr($id, 0, 2) === 'HX' && (int)$len_po_items === (int)sizeof($items)) {
            $this->db->query("update sms_invoice_list set status_level = -1, modified_by = '".$cashier_id."', modified_on = now() where invoice_id = '".$po_code."'");
        }
        // Check sms_payment_list if exists !VCH-STAR001
        $payment_star = $this->db->query("SELECT trans_amount, line_num FROM sms_payment_list WHERE invoice_id = '".$po_code."' AND pay_mode = '!VCH-STAR001' and rtrim(trans_ref6) = ''")->first_row();
        if (!empty($payment_star)) {
            $star = $payment_star->trans_amount;
            $pay_line_num = $payment_star->line_num;
            $max_line = $this->db->query("SELECT max(line_num) line_num FROM sms_invoice_item WHERE invoice_id = '".$po_code."'")->first_row()->line_num;
            $points = (int) ($star * 100);
            $trans_items[] = array(
                "line_num" => $max_line + 1,
                "item_id" => '!VCH-STAR001',
                "item_desc" => '$0.01 Star Rewards Voucher',
                "qty_ordered" => $points,
                "unit_price" => 1,
                "unit_discount" => 0,
                "regular_price" => 1,
                "trans_point" => $points,
                "mbr_savings" => 0
            );
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 2, '!VCH-HC001',
                              ".-$star.", '', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
            $this->db->query("Update sms_payment_list set trans_ref6 = '".$id."', modified_by = '".$cashier_id."', modified_on = now()
                                where coy_id = 'CTL' and invoice_id = '".$po_code."' and line_num = ".$pay_line_num);
            $total_price = $total_price - $star;
        }

        // For sms_payment_list and crm_voucher_list
        if ($refund_mode === 'original') {
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 1, 'CP',
                              ".-$total_price.", '', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
        } else {
            $voucher_id = $this->_voucher_id();
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 1, '!VCH-CREDIT',
                              ".-$total_price.", '".$voucher_id."', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
        }
        $this->db->query("INSERT INTO sms_invoice_reason (coy_id, invoice_id, reason_code, trans_type, created_by, created_on, modified_by, modified_on)
                          VALUES ('CTL', '".$id."', '".$refund_reason."', 'REFUND', '".$cashier_id."', '".$now."',
                                  '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id) DO NOTHING");

        // End
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return json_encode(array(
                "status_code" => 400,
                "message" => "Whoops! There was a problem processing your award. Please try again."
            ));
        }
        else {
            $this->db->trans_commit();
        }

        $has_invoice = $this->db->query("SELECT 1 FROM sms_invoice_list WHERE coy_id = 'CTL' and invoice_id = '".$id."' limit 1")->num_rows();
        if ($has_invoice > 0) {
            foreach ($item_ids as $a) {
                // For crm_member
                $item_id = $a['item_id'];
                $ms_trans = $this->msdb->query("select * from crm_member_transaction where trans_id = '".$po_code."' and item_id = '".$item_id."'")->first_row();
                $trans_items[] = array(
                    "line_num" => $a['line_num'],
                    "item_id" => $item_id,
                    "item_desc" => $ms_trans->item_desc,
                    "qty_ordered" => $a['qty_invoiced'],
                    "unit_price" => $ms_trans->unit_price,
                    "unit_discount" => $ms_trans->disc_amount,
                    "regular_price" => $ms_trans->regular_price,
                    "trans_point" => -(int)$ms_trans->trans_points,
                    "mbr_savings" => -(float)$ms_trans->mbr_savings
                );
            }
            if ($refund_mode !== 'original') {
                $this->msdb->query("INSERT INTO crm_voucher_list (coy_id, coupon_id, coupon_serialno, mbr_id, trans_date, 
                                issue_date, sale_date, utilize_date, expiry_date, print_date, receipt_id1, voucher_amount,
                                redeemed_amount, expired_amount, issue_type, issue_reason, status_level, created_by, created_on,
                                 modified_by, modified_on) VALUES ('CTL','!VCH-CREDIT','".$voucher_id."','".$mbr_id."',
                                 '".$now."', '".$now."', '".$now."', '1900-01-01 00:00:00', '".$date_expiry."', '".$now."',
                                 '".$id."', '".$total_price."', 0,0,'CREDIT', 'REFUND', 0, '".$cashier_id."', '".$now."',
                                  '".$cashier_id."', '".$now."')");
            }
            $transaction = array(
                "loc_id" => $loc_id,
                "pos_id" => "01",
                "invoice_num" => $id,
                "trans_type" => $trans_type,
                "trans_date" => $now,
                "cashier_id" => $cashier_id,
                "customer_info" => array(
                    "id" => $mbr_id
                ),
                "items" => $trans_items,
                "payments" => array(
                    array("pay_mode" => "CARD")
                )
            );
            $email_refund_mode = $refund_mode === 'ecredit' ? 'ECREDITS' : 'ORIGINAL';
            $this->msdb->query("INSERT INTO o2o_email_resent_job (xtype, invoice_id, delv_mode_id, created_on, mbr_id, 
                                  resent_status, refund_amt) VALUES ('email refund', '".$id."', '".$email_refund_mode."', '".$now."', '".$mbr_id."', 0, ".$total_price.")");
            if ($refund_mode === 'original') {
                $refund = $total_price+$star;
                $credit_card = $this->msdb->query("SELECT top 1 * FROM o2o_bank_payment_success WHERE transaction_id = '".$ref_id."'")->first_row()->credit_card;
                $this->msdb->query("INSERT INTO o2o_refund_transaction_list (transaction_id, trans20_id, invoice_date, 
                                or_invoice_id, or_invoice_date, mbr_name, invoice_id, status, reason, refund, total_amount, 
                                gateway_name, created_on, created_by, modified_by, modified_on, credit_card) VALUES ('".$ref_id."', '".$ref_id."',
                                '".$now."', '".$po_code."', '".$or_invoice_date."', '".$cust_name."', '".$id."', 0, '".$refund_reason."', 
                                ".$total_price.", ".$refund.", 'CP', '".$now."', '".$cashier_id."', '".$cashier_id."', '".$now."','".$credit_card."')");
            }
            $this->_send_api($transaction);
            $this->_refund_email($po_code, $id, $loc_id, $trans_items, $delv_mode, $or_invoice_date, $remarks_email);
            return json_encode(array(
                "status_code" => 200,
                "data" => ['invoice_id'=>$id]
            ));
        } else {
            return json_encode(array(
                "status_code" => 400,
                "message" => "Whoops! There was a problem processing your award. Please try again."
            ));
        }
    }

    /**
     * @api {post} /api/hachi_credit_migrate Hachi Refund
     * @apiVersion 1.0.0
     * @apiGroup REFUND
     *
     * @apiParam {string} inv_type Invoice Type. 'HR' or 'HX'
     * @apiParam {string} refund_mode Refund Mode. 'original or ecredit.
     * @apiParam {string} original_invoice_id Original invoice id.
     * @apiParam {string} loc_id Loc ID
     * @apiParam {string} refund_reason Refund Reason
     * @apiParam {string} cashier_id Cashier ID
     * @apiParam {Object[]} items Items
     * @apiParam {Object} items.line_num Line number
     * @apiParam {Object} items.qty qty_invoice
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "inv_type": "HR",
     *      "refund_mode": "original",
     *      "original_invoice_id": "HIC13840",
     *      "loc_id": "VC",
     *      "refund_reason": "CHANGE",
     *      "cashier_id": "3810",
     *      "items": [
     *          {
     *              "line_num": 1,
     *              "qty": 1
     *          },],
     *  }
     *
     * @apiSuccessExample success
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "invoice_id": "HRC01527"
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     */
    public function hachi_credit_migrate($trans_type, $refund_mode, $po_code, $items, $loc_id, $refund_reason, $cashier_id, $remarks_email)
    {
        $this->msdb = $this->load->database('cherps_hachi', TRUE);
        $now = date("Y-m-d H:i:s");
        $date_expiry = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")."+90 days"));
        $has_coupon = $this->db->query("SELECT 1 FROM sms_payment_list WHERE (pay_mode = '!VCH-CREDIT' or pay_mode = '!VCH-EGIFT' or pay_mode = 'DPD') AND invoice_id = '".$po_code."'")->num_rows();
        if ($has_coupon > 0 && $refund_mode === 'original') {
            return json_encode(array(
                "status_code" => 400,
                "message" => "Coupon in the payment list, so refund mode should be 'ECREDIT'"
            ));
        }
        $id = $this->_generate_invoice_id($trans_type);
        $this->msdb->query("insert into o2o_url_key values ('".$trans_type."', '".$id."', NEWID())");
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        // Begin
        $list = $this->db->query("select * from sms_invoice_list where invoice_id = '".$po_code."'")->first_row();
//        $loc_id_from = $list->loc_id;
        $remarks = 'To refund via '.$refund_mode.' payment mode.';
        // new refund record in sms_invoice_list
        $ref_id = $list->ref_id;
        $cust_name = $list->cust_name;
        $or_invoice_date = $list->invoice_date;
        $delv_mode = $list->delv_mode_id;
        $insert_list_sql = "INSERT INTO sms_invoice_list (coy_id, invoice_id, inv_type, invoice_date, status_level, cust_id,
                         cust_name, cust_addr, tel_code, contact_person, cust_po_code, mbr_ind, delv_date, loc_id, delv_name,
                          delv_addr, curr_id, exchg_rate, exchg_unit, payment_id, delv_term_id, delv_mode_id, tax_id,
                          tax_percent, approve_by, approve_date, remarks, ref_id, ref_rev, fault_loc, created_by,
                          created_on, modified_by, modified_on)
                        VALUES ('$list->coy_id', '$id', '$trans_type', '$now', 2, '$list->cust_id', '$list->cust_name', '$list->cust_addr',
                        '$list->tel_code', '$list->contact_person', '$po_code', '$list->mbr_ind', '1900-01-01 08:00:00',
                        '$loc_id', '$list->delv_name', 
                        '$list->delv_addr', '$list->curr_id', '$list->exchg_rate', '$list->exchg_unit', '$list->payment_id', 
                        '$list->delv_term_id', '$list->delv_mode_id', '$list->tax_id', '$list->tax_percent', '$list->approve_by', 
                        '$now','$remarks','$ref_id', '$list->ref_rev', '$list->fault_loc', '$cashier_id', '$now', '$cashier_id', '$now') ON conflict (coy_id, invoice_id) DO NOTHING";
        $this->db->query($insert_list_sql);

        $mbr_id = $list->cust_id;
        $trans_items = [];
        $total_price = 0;
        $item_ids = [];
        foreach ($items as $n=>$i) {
            $line_num = $i['line_num'];
            $qty_invoiced = -(int)$i['qty'];
            $is_exists = $this->db->query("select 1 from sms_invoice_item where coy_id = 'CTL' and ref_id = '".$po_code."' and ref_num = ".$line_num)->num_rows();
            if ($is_exists > 0) {
                return json_encode(array(
                    "status_code" => 400,
                    "message" => "Line number of ".(string)$line_num." has been refund, please check."
                ));
            }
            $inv_item = $this->db->query("select * from sms_invoice_item where invoice_id = '".$po_code."' and line_num = ".$line_num)->first_row();
            $item_id = rtrim($inv_item->item_id);
            $item_desc = $inv_item->item_desc;
            $inv_loc = rtrim($inv_item->inv_loc);
            $item_status_level = $inv_item->status_level;
            $insert_item_sql = "INSERT INTO sms_invoice_item (coy_id, invoice_id, line_num, item_id, item_desc, qty_invoiced,
                                uom_id, unit_price, disc_amount, disc_percent, status_level, long_desc, fin_dim1,
                                fin_dim2, ref_id, ref_rev, ref_num, receipt_line, qty_picked, disc_rebate, code_send, 
                                code_attempt, delv_date, posted_on, created_by, created_on, modified_by, modified_on, loc_id)
                                VALUES ('$inv_item->coy_id', '$id', '$line_num', '$item_id', '$item_desc',
                                $qty_invoiced, '$inv_item->uom_id', '$inv_item->unit_price', '$inv_item->disc_amount', '$inv_item->disc_percent', 
                                2,'$inv_item->long_desc', '$inv_item->fin_dim1', '$inv_item->fin_dim2', '$po_code', 
                                '$inv_item->ref_rev', '$line_num', '$inv_item->receipt_line', 0, 
                                '$inv_item->disc_rebate', '$inv_item->code_send', '$inv_item->code_attempt', '$now', 
                                '$now', '$cashier_id', '$now', '$cashier_id', '$now', '$loc_id')";
            $this->db->query($insert_item_sql);

            // For sms_payment_list
            $item_price = (abs($qty_invoiced) * (float)$inv_item->unit_price) - abs($inv_item->disc_amount);
            $total_price = $total_price + $item_price;
//            if (strpos($item_id, '#') !== false) {
//                $this->db->query("update sms_invoice_item set status_level = -1, modified_by = '".$cashier_id."', modified_on = now() where invoice_id = '".$po_code."' and line_num = " . $line_num);
//                continue;
//            }
            $check_type = $this->db->query("select 1 from ims_item_list where item_id = '".$item_id."' and item_type in ('I', 'B') and inv_dim4 not like '%-ESD%' limit 1")->num_rows();
            if ($check_type == 0) {
                $this->db->query("update sms_invoice_item set status_level = -1, modified_by = '".$cashier_id."', modified_on = now() where invoice_id = '".$po_code."' and line_num = " . $line_num);
                continue;
            }
            // For Hx, ims_inv_physical
            if ($trans_type === 'HX') {
                $original_qty = abs($qty_invoiced);
                $reserve = $this->db->query("SELECT * FROM ims_inv_physical WHERE item_id = '".$item_id."' AND loc_id = '".$inv_loc."' ORDER BY initial_date asc")->result_array();
                if (!empty($reserve)) {
                    foreach ($reserve as $k => $v) {
                        if ($original_qty > 0) {
                            $remain_reserve = (int)$v['qty_reserved'];
                            if ($remain_reserve >= $original_qty) {
                                $this->db->query("update ims_inv_physical set qty_reserved = qty_reserved - ".$original_qty.", modified_by = '".$cashier_id."', modified_on = now() where item_id = '".$item_id."' AND loc_id = '".$loc_id."' and line_num = ".$v['line_num']);
                                $original_qty = 0;
                            } else {
                                $this->db->query("update ims_inv_physical set qty_reserved = qty_reserved - ".$remain_reserve.", modified_by = '".$cashier_id."', modified_on = now() where item_id = '".$item_id."' AND loc_id = '".$loc_id."' and line_num = ".$v['line_num']);
                                $original_qty = (int)$original_qty - $remain_reserve;
                            }
                        }
                    }
                }
                if ($item_status_level === -1) {
                    return json_encode(array(
                        "status_code" => 400,
                        "message" => "No item exists."
                    ));
                }
                $this->db->query("update sms_invoice_item set status_level = -1, modified_by = '".$cashier_id."', modified_on = now() where invoice_id = '".$po_code."' and line_num = " . $line_num);
            }
            // For Hr, ims_inv_movement_item
            if ($trans_type === 'HR') {
                $unit_cost = $this->db->query("select unit_price from ims_item_price where item_id='".$item_id."' 
                                                and price_type='SUPPLIER' and eff_from<now() and eff_to>=now()
                                                 order by price_ind limit 1")->first_row()->unit_price;
                $unit_cost = $unit_cost ? $unit_cost : 0;
                $insert_move_item_sql = "INSERT INTO ims_inv_movement_item (coy_id, trans_id, line_num, movement_ind, item_id, 
                                  item_desc, uom_id, trans_qty, unit_cost, supp_csg, created_on, modified_on, approved_qty, 
                                  verified_qty) VALUES ('CTL', '".$id."', ".$line_num.", '+', '".$item_id."', '".$item_desc."',
                                  'pcs', ".abs($qty_invoiced).", ".$unit_cost.", 'N', '".$now."', '".$now."', 0,0) ON conflict 
                                  (coy_id, trans_id, line_num) DO NOTHING";
                $this->db->query($insert_move_item_sql);


//                $po_loc_id = $this->db->query("select rtrim(loc_id) loc_id from sms_invoice_list where invoice_id = '".$po_code."'")->first_row()->loc_id;
                if ($n === 0) {
                    $ref_id_trans = $id;
                } else {
                    $ref_id_trans = $id . '-' . (string)$n;
                }
//                $ref_id_trans = $this->_generate_trans_id($po_loc_id.$loc_id);
                $original_transaction = $this->db->query("SELECT * FROM ims_inv_transaction WHERE ref_id2 = '".$po_code."' and item_id = '".$item_id."' and coy_id = 'CTL' limit 1")->first_row();
                $ini_date = isset($original_transaction->initial_date) ? $original_transaction->initial_date : $now;
                $ref_rev = isset($original_transaction->ref_rev) ? $original_transaction->ref_rev : 0;
                $ref_num = isset($original_transaction->ref_num) ? $original_transaction->ref_num : 1;
                $trans_ref_id = isset($original_transaction->ref_id) ? $original_transaction->ref_id : $id;
                $insert_trans_sql = "INSERT INTO ims_inv_transaction (coy_id, trans_id, trans_type, status_level, trans_date,
                                             movement_ind, item_id, loc_id, supp_csg, inv_qty, inv_value, initial_date,
                                            ref_id, ref_rev, ref_num, fin_dim1, fin_dim2, created_by, created_on, modified_by,
                                             modified_on, ref_id2) VALUES ('CTL', '$ref_id_trans', 'SN', 0, '$now', '+',
                                             '$item_id', '$loc_id', 'N', ".abs($qty_invoiced).", ".$unit_cost.",
                                              '".$ini_date."', '".$trans_ref_id."', ". $ref_rev .", ".$ref_num.", '61-ONLINE SALES', '".$inv_item->fin_dim2."',
                                              '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."',
                                              '".$id."') ON conflict (coy_id, trans_id) DO NOTHING";
                $this->db->query($insert_trans_sql);
                $hi_ref_id = $this->db->query("select rtrim(ref_id) ref_id from ims_inv_transaction where item_id = '".$item_id."' and ref_id2 = '".$po_code."' and coy_id = 'CTL'")->first_row()->ref_id;

                // Check need update ims_inv_physical
                $check_update = $this->db->query("select 1 from ims_item_list where item_id = '".$item_id."' and item_type = 'I' and inv_dim4 not like '%-ESD%' limit 1")->num_rows();
                if ($check_update > 0) {
                    $is_phy_exists = $this->db->query("select 1 from ims_inv_physical where item_id = '".$item_id."' and loc_id = '".$loc_id."' and ref_id = '".$hi_ref_id."'")->num_rows();
                    if ($is_phy_exists > 0) {
                        $this->db->query("update ims_inv_physical set qty_on_hand = qty_on_hand + ".abs($qty_invoiced).", modified_by = '".$cashier_id."', modified_on = now() where item_id = '".$item_id."' and loc_id = '".$loc_id."' and ref_id = '".$hi_ref_id."'");
                    } else {
                        $phyical_line_num = $this->db->query("select max(line_num) line_num from ims_inv_physical where item_id = '".$item_id."' and loc_id = '".$loc_id."'")->first_row()->line_num + 1;
                        $insert_physical_sql = "INSERT INTO ims_inv_physical (coy_id, item_id, loc_id, line_num, trans_date,
                                        qty_on_hand, qty_reserved, unit_cost, supp_csg, initial_date,
                                        ref_id, ref_rev, ref_num, created_by, created_on, modified_by, modified_on, updated_on)
                                         VALUES ('CTL', '$item_id', '$loc_id', '$phyical_line_num', '$now', ".abs($qty_invoiced).",
                                         0, '$unit_cost', 'N', '$now', '$hi_ref_id', 0, 1, '$cashier_id', '$now',
                                         '$cashier_id', '$now', '$now')";
                        $this->db->query($insert_physical_sql);
                    }
                }
            }

            // For crm_member
            $item_ids[] = ['item_id'=>$item_id, 'qty_invoiced'=>$qty_invoiced, 'line_num'=>$line_num];
        }
        // Update sms_invoice_list
        $len_po_items = $this->db->query("select count(*) len from sms_invoice_item where invoice_id = '".$po_code."'")->first_row()->len;
        if (substr($id, 0, 2) === 'HX' && (int)$len_po_items === (int)sizeof($items)) {
            $this->db->query("update sms_invoice_list set status_level = -1, modified_by = '".$cashier_id."', modified_on = now() where invoice_id = '".$po_code."'");
        }
        // Check sms_payment_list if exists !VCH-STAR001
        $payment_star = $this->db->query("SELECT trans_amount, line_num FROM sms_payment_list WHERE invoice_id = '".$po_code."' AND pay_mode = '!VCH-STAR001' and rtrim(trans_ref6) = ''")->first_row();
        if (!empty($payment_star)) {
            $star = $payment_star->trans_amount;
            $pay_line_num = $payment_star->line_num;
            $max_line = $this->db->query("SELECT max(line_num) line_num FROM sms_invoice_item WHERE invoice_id = '".$po_code."'")->first_row()->line_num;
            $points = (int) ($star * 100);
            $trans_items[] = array(
                "line_num" => $max_line + 1,
                "item_id" => '!VCH-STAR001',
                "item_desc" => '$0.01 Star Rewards Voucher',
                "qty_ordered" => $points,
                "unit_price" => 1,
                "unit_discount" => 0,
                "regular_price" => 1,
                "trans_point" => $points,
                "mbr_savings" => 0
            );
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 2, '!VCH-HC001',
                              ".-$star.", '', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
            $this->db->query("Update sms_payment_list set trans_ref6 = '".$id."', modified_by = '".$cashier_id."', modified_on = now()
                                where coy_id = 'CTL' and invoice_id = '".$po_code."' and line_num = ".$pay_line_num);
            $total_price = $total_price - $star;
        }
        // For Hr, ims_inv_movement
        if (substr($id, 0, 2) === 'HR') {
            $this->db->query("INSERT INTO ims_inv_movement (coy_id, trans_id, move_type, status_level, loc_id, loc_to, 
                            requester_id, request_date, verifier_id, verify_date, approve_date, reason_ind, remarks, 
                            reject_notes, ack_date, created_by, created_on, modified_by, modified_on) VALUES ('CTL',
                            '".$id."', 'SN', 4, '".$loc_id."', '".$loc_id."', '".$cashier_id."', '".$now."', '".$cashier_id."',
                             '".$now."', '1900-01-01 00:00:00',
                            'Refund', '".$po_code."', '', '1900-01-01 00:00:00', '".$cashier_id."', '".$now."', '".$cashier_id."',
                             '".$now."') ON conflict (coy_id, trans_id) DO NOTHING");
        }

        // For sms_payment_list and crm_voucher_list
        if ($refund_mode === 'original') {
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 1, 'CP',
                              ".-$total_price.", '', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
        } else {
            $voucher_id = $this->_voucher_id();
            $this->db->query("INSERT INTO sms_payment_list (coy_id, invoice_id, line_num, pay_mode, trans_amount, trans_ref1,
                              status_level, created_by, created_on, modified_by, modified_on) VALUES ('CTL', '".$id."', 1, '!VCH-CREDIT',
                              ".-$total_price.", '".$voucher_id."', 2, '".$cashier_id."', '".$now."', '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id, line_num) DO NOTHING");
        }
        $this->db->query("INSERT INTO sms_invoice_reason (coy_id, invoice_id, reason_code, trans_type, created_by, created_on, modified_by, modified_on)
                          VALUES ('CTL', '".$id."', '".$refund_reason."', 'REFUND', '".$cashier_id."', '".$now."',
                                  '".$cashier_id."', '".$now."') ON conflict (coy_id, invoice_id) DO NOTHING");

        // End
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return json_encode(array(
                "status_code" => 400,
                "message" => "Whoops! There was a problem processing your award. Please try again."
            ));
        }
        else {
            $this->db->trans_commit();
        }

        $has_invoice = $this->db->query("SELECT 1 FROM sms_invoice_list WHERE coy_id = 'CTL' and invoice_id = '".$id."' limit 1")->num_rows();
        if ($has_invoice > 0) {
            foreach ($item_ids as $a) {
                // For crm_member
                $item_id = $a['item_id'];
                $ms_trans = $this->msdb->query("select * from crm_member_transaction where trans_id = '".$po_code."' and item_id = '".$item_id."'")->first_row();
                $trans_items[] = array(
                    "line_num" => $a['line_num'],
                    "item_id" => $item_id,
                    "item_desc" => $ms_trans->item_desc,
                    "qty_ordered" => $a['qty_invoiced'],
                    "unit_price" => $ms_trans->unit_price,
                    "unit_discount" => $ms_trans->disc_amount,
                    "regular_price" => $ms_trans->regular_price,
                    "trans_point" => -(int)$ms_trans->trans_points,
                    "mbr_savings" => -(float)$ms_trans->mbr_savings
                );
            }
            if ($refund_mode !== 'original') {
                $this->msdb->query("INSERT INTO crm_voucher_list (coy_id, coupon_id, coupon_serialno, mbr_id, trans_date, 
                                issue_date, sale_date, utilize_date, expiry_date, print_date, receipt_id1, voucher_amount,
                                redeemed_amount, expired_amount, issue_type, issue_reason, status_level, created_by, created_on,
                                 modified_by, modified_on) VALUES ('CTL','!VCH-CREDIT','".$voucher_id."','".$mbr_id."',
                                 '".$now."', '".$now."', '".$now."', '1900-01-01 00:00:00', '".$date_expiry."', '".$now."',
                                 '".$id."', '".$total_price."', 0,0,'CREDIT', 'REFUND', 0, '".$cashier_id."', '".$now."',
                                  '".$cashier_id."', '".$now."')");
            }
            $transaction = array(
                "loc_id" => $loc_id,
                "pos_id" => "01",
                "invoice_num" => $id,
                "trans_type" => $trans_type,
                "trans_date" => $now,
                "cashier_id" => $cashier_id,
                "customer_info" => array(
                    "id" => $mbr_id
                ),
                "items" => $trans_items,
                "payments" => array(
                    array("pay_mode" => "CARD")
                )
            );
            $email_refund_mode = $refund_mode === 'ecredit' ? 'ECREDITS' : 'ORIGINAL';
            $this->msdb->query("INSERT INTO o2o_email_resent_job (xtype, invoice_id, delv_mode_id, created_on, mbr_id, 
                                  resent_status, refund_amt) VALUES ('email refund', '".$id."', '".$email_refund_mode."', '".$now."', '".$mbr_id."', 0, ".$total_price.")");
            if ($refund_mode === 'original') {
                $refund = $total_price+$star;
                $credit_card = $this->msdb->query("SELECT top 1 * FROM o2o_bank_payment_success WHERE transaction_id = '".$ref_id."'")->first_row()->credit_card;
                $this->msdb->query("INSERT INTO o2o_refund_transaction_list (transaction_id, trans20_id, invoice_date, 
                                or_invoice_id, or_invoice_date, mbr_name, invoice_id, status, reason, refund, total_amount, 
                                gateway_name, created_on, created_by, modified_by, modified_on, credit_card) VALUES ('".$ref_id."', '".$ref_id."',
                                '".$now."', '".$po_code."', '".$or_invoice_date."', '".$cust_name."', '".$id."', 0, '".$refund_reason."', 
                                ".$total_price.", ".$refund.", 'CP', '".$now."', '".$cashier_id."', '".$cashier_id."', '".$now."','".$credit_card."')");
            }
            $this->_send_api($transaction);
            $this->_refund_email($po_code, $id, $email_refund_mode, $trans_items, $delv_mode, $or_invoice_date, $remarks_email);
            return json_encode(array(
                "status_code" => 200,
                "data" => ['invoice_id'=>$id]
            ));
        } else {
            return json_encode(array(
                "status_code" => 400,
                "message" => "Whoops! There was a problem processing your award. Please try again."
            ));
        }
    }

    public function _refund_email($po_code, $id, $email_refund_mode, $items, $delv_mode, $original_date, $remarks)
    {
        $now = date("d/m/Y", strtotime($original_date));
        if (($key = array_search('!VCH-STAR001', array_column($items, "item_id"))) !== false) {
            unset($items[$key]);
        }
        $title = substr($id, 0, 2) . " for Invoice No. ".$po_code;
        $reason = $this->db->query("SELECT reason_code FROM sms_invoice_reason WHERE invoice_id = '".$id."'")->first_row()->reason_code;
        $or_items = $this->db->query("select * from sms_invoice_item where invoice_id = '".$po_code."'")->result_array();
        $loc_id_from = $or_items[0]['loc_id'];
        $message = "The following item (s) has been cancelled: \n";
        $table = "
The updated invoice status is as follows: 
Status          Price              Loc ID      Description
-----------------------------------------------------------\n";
        foreach ($or_items as $o) {
            $delv_mode_item = rtrim($o['delv_mode_id']);
            $loc_id_item = rtrim($o['loc_id']);
            if (in_array((int)$o['line_num'], array_column($items, 'line_num'))) {
                $t = "Deleted          ".number_format($o['unit_price'], 2, '.', ',')."             ".$loc_id_item."       ".$o['item_desc']." \n";
                $item_detail = "\nInvoice Date : $now \nItem id          : ".$o['item_id']." \n Product        : ".$o['item_desc']." \nQty               : ".abs($o['qty_invoiced'])." \nDoc Ref         : ".$id." \nDelv Mode   : $delv_mode_item\nReason         : $reason\nPay Model   : $email_refund_mode\n";
                $message = $message.$item_detail;
            } else {
                $o_status = '';
                if ($o['status_level'] === "1") {
                    $o_status = 'Picked';
                } elseif ($o['status_level'] === "2") {
                    $o_status = 'Delivered';
                } elseif ($o['status_level'] === "3") {
                    $o_status = 'Posted';
                } elseif ($o['status_level'] === "-1") {
                    $o_status = 'Deleted';
                }
                $o_loc_id = $o['loc_id'];
                $t = $o_status."          ".number_format($o['unit_price'], 2, '.', ',')."             ".$o_loc_id."       ".$o['item_desc']." \n";
            }
            $table = $table.$t;
        }
        $message = $message . "\nRemarks      : $remarks\n";
        $message = $message . $table;
        $message = $message . "\n Please proceed with the necessary, if applicable.";
        if (ENVIRONMENT=="production") {
            $email = $this->db->query("SELECT email_ims FROM ims_location_list WHERE loc_id = '".$loc_id_from."' AND coy_id = 'CTL'")->first_row()->email_ims;
            $cc = "HQ.CustServiceGroup@challenger.sg; yongsheng@challenger.sg; riiko.wang@challenger.sg; li.liangze@challenger.sg; ken@challenger.sg";
        }
        else {
//            HQ.CustServiceGroup@challenger.sg, chen@challenger.sg
//            $email = 'riiko.wang@challenger.sg';
            $email = "li.liangze@challenger.sg";
        }
        $params['to'] = $email;
        $params['cc'] = $cc;
        $params['subject'] = $title;
        $params['message'] = $message;
        $this->_SendEmail($params);
    }

    private function _voucher_id() {
        $next_num = $this->db->query("select next_num from sys_trans_list where coy_id = 'CTL' and sys_id = 'SMS' and trans_prefix = 'VCH-CREDITVCH-CR'")->result_array();
        $voucher_id = 'VCH-CR' . sprintf("%09s", $next_num[0]['next_num']);
        $this->db->query("update sys_trans_list set next_num = " . $next_num[0]['next_num'] . "+1 where coy_id = 'CTL' and sys_id = 'SMS' and trans_prefix = 'VCH-CREDITVCH-CR'");
        return $voucher_id;
    }

    private function _send_api($query) {
        $headers[] = "Content-Type: application/json";
        $auth = '?key_code=' . $this->vc_auth;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->vc_url.$auth);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    public function _offer_exist($rf_invoice_id)
    {
        $offer_exists = $this->db->query("select 1 from svs_offer_list where offer_invoice_id = '".$rf_invoice_id."' limit 1")->num_rows();
        if ($offer_exists > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function _generate_invoice_id($type)
    {
        $year = date('y');
        $chars = 'CDEFGHIJKLMNOPQRSTUVWXYZ';
        $char = substr($chars, (int)$year - 20, 1);
        $trans_prefix = $type === 'HR' ? 'INVHR'.$char : 'INVHX'.$char;
        $is_exists = $this->db->query("select 1 from sys_trans_list where coy_id = 'CTL' and sys_id = 'SMS' and trans_prefix = '" . $trans_prefix . "'")->num_rows();
        if ($is_exists === 0) {
            $this->db->query("insert into sys_trans_list (coy_id, sys_id, trans_prefix, next_num, sys_date) VALUES ('CTL', 'SMS', '". $trans_prefix ."', 1, now())");
        }
        $next_num = $this->db->query("select next_num from sys_trans_list where coy_id = 'CTL' and sys_id = 'SMS' and trans_prefix = '" . $trans_prefix . "'")->result_array();
        $invoice_id = $type. $char . sprintf("%05s", $next_num[0]['next_num']);
        $this->db->query("update sys_trans_list set next_num = " . $next_num[0]['next_num'] . "+1 where coy_id = 'CTL' and sys_id = 'SMS' and trans_prefix = '" . $trans_prefix . "'");
        return $invoice_id;
    }

    public function _generate_trans_id($type)
    {
//        $year = date('Ym');
        $date = date('y');
        // MOVE_TYPEAMKAMK202009
        $trans_prefix = 'STK_MOVE'. $type . $date;
        $is_exists = $this->db->query("select 1 from sys_trans_list where coy_id = 'CTL' and sys_id = 'IMS' and trans_prefix = '" . $trans_prefix . "'")->num_rows();
        if ($is_exists === 0) {
            $this->db->query("insert into sys_trans_list VALUES ('CTL', 'IMS', '". $trans_prefix ."', 1, now())");
        }
        $next_num = $this->db->query("select next_num from sys_trans_list where coy_id = 'CTL' and sys_id = 'IMS' and trans_prefix = '" . $trans_prefix . "'")->result_array();
        $invoice_id = $type. $date . sprintf("%0" . (string)(13-strlen($type)) . "s", $next_num[0]['next_num']);
        $this->db->query("update sys_trans_list set next_num = " . $next_num[0]['next_num'] . "+1 where coy_id = 'CTL' and sys_id = 'IMS' and trans_prefix = '" . $trans_prefix . "'");
        return $invoice_id;
    }

    public function _next_line($trans_prefix)
    {
        $next_num = $this->db->query("select next_num from sys_trans_list where coy_id = 'INC' and sys_id = 'SEW' and trans_prefix = '" . $trans_prefix . "'")->result_array();
        $trans_id = $trans_prefix . sprintf("%09s", $next_num[0]['next_num']);
        $this->db->query("update sys_trans_list set next_num = " . $next_num[0]['next_num'] . "+1 where coy_id = 'INC' and sys_id = 'SEW' and trans_prefix = '" . $trans_prefix . "'");
        return $trans_id;
    }

    // Controller: challenger_po
    public function challenger_po($mode,$params){
        if ($mode=="SUPPLIER") {
            $supp_id_str = "";
            foreach ($params as $id)
                $supp_id_str .= "'$id',";
            $sql = "SELECT RTRIM(po_id) po_id,RTRIM(supp_id) supp_id
                    FROM pms_po_list p
                    LEFT JOIN b2b_po_message m ON m.coy_id=p.coy_id AND m.trans_id=p.po_id AND m.status_code=1
                    WHERE p.coy_id='CTL' AND p.status_level IN (1,2) AND p.po_date> now() + interval '-7 day' AND m.status_code IS NULL
                    AND p.supp_id in ($supp_id_str'')
                    ORDER BY p.po_date; ";
            $res = $this->db->query($sql)->result();
            return $res;
        }
        else if ($mode=="PO") {
            $sql = "SELECT RTRIM(po_id) po_id,RTRIM(supp_id) supp_id, m.status_code
                    FROM pms_po_list p
                    LEFT JOIN b2b_po_message m ON m.coy_id=p.coy_id AND m.trans_id=p.po_id AND m.status_code=1
                    WHERE p.coy_id='CTL' 
                    AND p.po_id = '$params' 
                    ORDER BY m.created_on DESC limit 1";
            $res = $this->db->query($sql)->row_array();
            return $res;
        }
        else if ($mode=="VRC-CLAIM") {
            $supp_id_str = "";
            foreach ($params as $id) {
                if (!in_array($id,['SA0006']))
                    $supp_id_str .= "'$id',";
            }
            $sql = "select c.trans_id,c.claim_type,c.supp_id,c.trans_date,c.status_level, c.created_by,
                      f.trans_id jour_trans_id, f.trans_type jour_trans_type, f.trans_date jour_trans_date, f.status_level jour_status_level,
                      m.status_code, c.remarks
                    from pms_claim_list c
                    left join fin_journal f on f.coy_id='CTL' and f.cust_supp_id=c.supp_id and f.doc_ref=c.trans_id
                    left join b2b_po_message m on m.coy_id='CTL' and m.trans_type='CL' and m.trans_id=f.trans_id
                    where c.coy_id='CTL' and c.status_level in (1,2) and c.trans_date >= '2019-10-01'
                      and m.status_code is null
                      and (
                        (c.supp_id='SA0006' AND c.remarks like '%TRADESHIFT%')
                        OR c.supp_id in ($supp_id_str'') 
                      )
                    order by c.created_on DESC limit 1";
            $res = $this->db->query($sql)->row_array();
            return $res;
        }
        else if ($mode=="VRC") {
            $sql = "SELECT RTRIM(trans_id) trans_id,RTRIM(cust_supp_id) supp_id, status_level
                    FROM fin_journal
                    WHERE coy_id='CTL' AND trans_id='$params'
                    ORDER BY trans_date DESC limit 1";
            $res = $this->db->query($sql)->row_array();
            return $res;
        }
        else if ($mode=="PRINTPDF") {
            $sql = "SELECT RTRIM(po_id) po_id,RTRIM(supp_id) supp_id 
                    FROM pms_po_list p 
                    WHERE p.coy_id='CTL' AND p.po_id = '$params' limit 1 ";
            $res = $this->db->query($sql)->row_array();
            return $res;
        }
    }

    // Controller: delivery_track
    public function o2o_delivery_log_refresh($supp_id){
        $sql_time = date('Y-m-d', strtotime('-7 days', time()));
        $sql = "INSERT INTO o2o_delivery_log (po_id,invoice_id,tracking_id)
                    select l.po_id,i.ref_id,i.ref_id from pms_po_list l
                        join pms_po_item i on i.po_id=l.po_id
                        where l.supp_id='$supp_id' and left(l.po_id,5) = 'HPDSH'
                                and l.po_date>'$sql_time' and i.ref_id NOT IN (SELECT invoice_id FROM o2o_delivery_log WHERE tracking_on>'$sql_time') "; 
        $query = $this->db->query($sql); 
    }

    // Controller: qrscan
    public function get_mbrid($params) {
        $sql = "SELECT first_name + ' ' + last_name as name FROM crm_member_list WHERE coy_id = ? AND mbr_id= ? limit 1";
        $res = $this->db->query($sql,$params)->row();
        return $res;
    }

    public function qrscan($params) {
        $sqlparams["del_coy_id"] = (isset($params["coy_id"])) ? $params["coy_id"] : "";
        $sqlparams["del_ref_id"] = (isset($params["ref_id"])) ? $params["ref_id"] : "";
        $sqlparams["del_loc_id"] = (isset($params["loc_id"])) ? $params["loc_id"] : "";
        $sqlparams["del_pos_id"] = (isset($params["pos_id"])) ? $params["pos_id"] : "";
        $sqlparams["coy_id"] = (isset($params["coy_id"])) ? $params["coy_id"] : "";
        $sqlparams["ref_id"] = (isset($params["ref_id"])) ? $params["ref_id"] : "";
        $sqlparams["loc_id"] = (isset($params["loc_id"])) ? $params["loc_id"] : "";
        $sqlparams["pos_id"] = (isset($params["pos_id"])) ? $params["pos_id"] : "";
        $sqlparams["mbr_id"] = (isset($params["mbr_id"])) ? $params["mbr_id"] : "";
        $sqlparams["created_by"] = (isset($params["created_by"])) ? $params["created_by"] : "";
        $sql = "DELETE FROM o2o_qrscan WHERE coy_id=? AND ref_id=? AND loc_id=? AND pos_id=?; "
            . "INSERT INTO o2o_qrscan (coy_id,ref_id,loc_id,pos_id,mbr_id,created_by,created_on) VALUES (?,?,?,?,?,?,GETDATE()); ";
        $res = $this->db->query($sql,$sqlparams);
        return $res;
    }

}

?>
