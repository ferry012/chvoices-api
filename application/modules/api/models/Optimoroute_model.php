<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Optimoroute_model extends Base_Common_Model {

    private $apiurl;
    private $token;
    private $locDuration;

    public function __construct() {
        parent::__construct();

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->apiurl = 'https://api.optimoroute.com/v1/';
            $this->token = 'b43898e28ae512da90b40a93adef2350dlc8CzWnrGY';
        }
        else {
            $this->apiurl = 'https://api.optimoroute.com/v1/';
            $this->token = 'ea60ffb6e8b89c71c9ce40676a18854600ib7F41FA';
        }

        $this->locDuration = 4;
    }

    private function mapDrivers($driverId) {
        return 'staffId';
    }

    public function routes($driver) {
        $date = date('Y-m-d');

        $headers[] = "Content-Type: application/json";
        $uri = '?key=' . $this->token;
        $uri.= '&date=' . $date;
        if ($driver!='')
            $uri.= '&driverSerial=' . $driver;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiurl.'get_routes'.$uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $return = curl_exec($ch);
        curl_close($ch);

        $return_arr = json_decode($return,true);

        $msgStats = '';
        if ($return_arr['success'])
            $msgStats = (count($return_arr['routes']) == 1) ? count($return_arr['routes'][0]['stops']) . ' Stops retrieved for this route' : count($return_arr['routes']) . ' Routes retrieved';

        return [
            'code' => ($return_arr['success']) ? '1' : '0',
            'msg'  => ($return_arr['success']) ?  $msgStats : 'Error: ' . $return_arr['code'],
            'routes' => ($return_arr['success']) ? $return_arr['routes'] : []
        ];
    }

    public function plan($date) {
        if ($date=='')
            $date = date('Y-m-d');

        $query = [
            'date' => $date
        ];
        $return = $this->sendApi('start_planning', $query);
        $return_arr = json_decode($return,true);

        return [
            'code' => ($return_arr['success']) ? '1' : '0',
            'msg'  => ($return_arr['success']) ? 'Planning ID ' . $return_arr['planningId'] : 'Error: ' . $return_arr['code']
        ];
    }

    public function auto($delv_date=''){
        set_time_limit(300);

        $sqlDelv = ($delv_date!='') ? " and cast(b.delivery_date as date) = '$delv_date' " : "";

        $sql = "select b.created_on,b.parcel_id, m.status_level,
                           rtrim(b.invoice_id) invoice_id, b.delivery_date,
                           rtrim(l.cust_id)+'_'+rtrim(a.postal_code) mbrPostal, a.street_line1, a.postal_code,
                           street_line1 + ' ' + street_line2 + ' ' + street_line3 + ' ' + street_line4 + ' SINGAPORE ' + postal_code address,
                           left(b.tracking_id,15) tracking_id, RTRIM(UPPER(LEFT(tracking_id,3))) driver_id, 
                           weight, --Weight=cast(volumeW as decimal(10,2))/10 ,
                           volume = cast(volume/1000 as decimal(10,2))/10
                    from b2b_parcel_list b WITH(NOLOCK)
                    join sms_invoice_list l WITH(NOLOCK) on b.coy_id=l.coy_id and b.invoice_id=l.invoice_id
                    join coy_address_book a WITH(NOLOCK) ON a.coy_id=l.coy_id and a.ref_type='CASH_INV  ' and a.ref_id=l.invoice_id and a.addr_type='2-DELIVERY'
                    left join b2b_gateway_message m WITH(NOLOCK) ON m.coy_id='CTL' and m.gateway_id='OPTIMO' and m.trans_id=left(b.tracking_id,15) and m.status_level>0
                    WHERE b.coy_id='CTL' and b.delv_mode_id='NID' and b.tracking_id<>'' and b.delivery_date >= '2020-04-11'
                        and left(b.tracking_id,3) in ('YUN','LOW','LIM','ROY','CAR','SUD')
                        and m.status_level IS NULL $sqlDelv
                    order by b.created_on desc; ";
        $resTotal = $this->db->query($sql)->result_array();

        $sql = "select TOP 100 b.created_on,b.parcel_id, m.status_level,
                           rtrim(b.invoice_id) invoice_id, b.delivery_date,
                           rtrim(l.cust_id)+'_'+rtrim(a.postal_code) mbrPostal, a.street_line1, a.postal_code,
                           street_line1 + ' ' + street_line2 + ' ' + street_line3 + ' ' + street_line4 + ' SINGAPORE ' + postal_code address,
                           left(b.tracking_id,15) tracking_id, RTRIM(UPPER(LEFT(tracking_id,3))) driver_id, 
                           weight, --Weight=cast(volumeW as decimal(10,2))/10 ,
                           volume = cast(volume/1000 as decimal(10,2))/10
                    from b2b_parcel_list b WITH(NOLOCK)
                    join sms_invoice_list l WITH(NOLOCK) on b.coy_id=l.coy_id and b.invoice_id=l.invoice_id
                    join coy_address_book a WITH(NOLOCK) ON a.coy_id=l.coy_id and a.ref_type='CASH_INV  ' and a.ref_id=l.invoice_id and a.addr_type='2-DELIVERY'
                    left join b2b_gateway_message m WITH(NOLOCK) ON m.coy_id='CTL' and m.gateway_id='OPTIMO' and m.trans_id=left(b.tracking_id,15) and m.status_level>0
                    WHERE b.coy_id='CTL' and b.delv_mode_id='NID' and b.tracking_id<>'' and b.delivery_date >= '2020-04-11'
                        and left(b.tracking_id,3) in ('YUN','LOW','LIM','ROY','CAR','SUD')
                        and m.status_level IS NULL $sqlDelv
                    order by b.created_on desc; ";
        $res = $this->db->query($sql)->result_array();

        $responses = ["total" => count($resTotal), "success" => 0, "orders" => [] ];
        foreach($res as $ord) {
            $r = $this->send($ord['invoice_id'], $ord);
            $responses['orders'][] = $r;
            if ($r['code'] > 0)
                $responses['success'] ++;
        }
        return $responses;
    }

    public function send($id='', $data=[]){
        $order = (count($data)==0) ? $this->getParcel($id) : $data;
        if ($order) {
            $query = [
                'operation' => 'MERGE',
                'orderNo' => $order['invoice_id'].'-'.$order['parcel_id'],
                'type' => 'D',
                'date' => date('Y-m-d', strtotime($order['delivery_date']) ),
                'duration' => $this->locDuration,
                'location' => [
                    "address" => $order['address'],
                    "locationNo" => $order['mbrPostal'],
                    "locationName" => $order['street_line1'],
                    "acceptPartialMatch" => true
                ],
                "load1"     => floatval($order['weight']),
                "load2"     => floatval($order['volume']),
//                'notes'     => '',
//                "twFrom"    => "10:00",
//                "twTo"      => "10:59",
//                "vehicleFeatures" => ["FR"],
//                "skills" => ["SK001", "SK002"],
            ];
            if ($order['driver_id']!='' && !isset($_GET['nodriver']) ) {
                $query['assignedTo'] = [
                    'serial' => $order['driver_id']
                ];
            }
            $return = $this->sendApi('create_order', $query);
            $return_arr = json_decode($return,true);

            $this->saveGatewayLog([
                "trans_id"      => $order['tracking_id'],
                "url"           => $this->apiurl.'create_order',
                "request"       => json_encode($query),
                "return_status" => $return_arr['mode'] .' '. (isset($return_arr['code']) ? $return_arr['code'] : '') ,
                "response"      => $return,
                "status_level"  => ($return_arr['success']) ? '1' : '0',
            ]);

            if ($return_arr['success'] == false && $return_arr['code'] == 'ERR_LOC_GEOCODING_MULTIPLE') {
                // Let's try once more with only Postal Code
                $query['location']['address'] = 'SINGAPORE ' . $order['postal_code'];
                $return = $this->sendApi('create_order', $query);
                $return_arr = json_decode($return,true);

                $this->saveGatewayLog([
                    "trans_id"      => $order['tracking_id'],
                    "url"           => $this->apiurl.'create_order',
                    "request"       => json_encode($query),
                    "return_status" => $return_arr['mode'] .' '. (isset($return_arr['code']) ? $return_arr['code'] : '') ,
                    "response"      => $return,
                    "status_level"  => ($return_arr['success']) ? '1' : '0',
                ]);

                if ($return_arr['success'] == false) {
                    // Save to parcel remarks
                    $sql = "UPDATE b2b_parcel_list SET remarks = 'OPTIMO: " . $return_arr['code'] . " ' + remarks
                                WHERE coy_id='CTL' and invoice_id='".$order['invoice_id']."' and parcel_id='".$order['parcel_id']."' ";
                    $this->db->query($sql);
                }
            }

            return [
                'code' => ($return_arr['success']) ? '1' : '0',
                'invoice' => $order['invoice_id'],
                'delivery' => date('Y-m-d', strtotime($order['delivery_date']) ),
                'tracking' => $order['tracking_id'],
                'msg'  => $return
            ];
        }
        else {
            return [
                'code' => '0',
                'invoice' => $id,
                'delivery' => '',
                'tracking' => '',
                'msg'  => 'No data found'
            ];
        }
    }

    private function sendApi($action,$query) {

        if (isset($_GET['debug']) && $_GET['debug']==2) {
            return json_encode([
                'success' => true,
                'mode' => 'TEST',
                'data' => $query
            ]);
        }

        $headers[] = "Content-Type: application/json";
        $auth = '?key=' . $this->token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiurl.$action.$auth);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    private function getParcel($id){
        $sql = "select rtrim(b.invoice_id) invoice_id, b.parcel_id, b.delivery_date,
                       rtrim(l.cust_id)+'_'+rtrim(a.postal_code) mbrPostal, a.street_line1, a.postal_code,
                       street_line1 + ' ' + street_line2 + ' ' + street_line3 + ' ' + street_line4 + ' SINGAPORE ' + postal_code address,
                       left(tracking_id,15) tracking_id, RTRIM(UPPER(LEFT(tracking_id,3))) driver_id,
                       weight, -- Weight=cast(volumeW as decimal(10,2))/10 ,
                       volume = cast(volume/1000 as decimal(10,2))/10
                from b2b_parcel_list b
                join sms_invoice_list l on b.coy_id=l.coy_id and b.invoice_id=l.invoice_id
                join coy_address_book a ON a.coy_id=l.coy_id and a.ref_type='CASH_INV  ' and a.ref_id=l.invoice_id and a.addr_type='0-PRIMARY'
                WHERE b.coy_id='CTL' and b.delv_mode_id='NID'
                    and l.invoice_id = '$id'; ";
        $res = $this->db->query($sql)->result_array();
        if ($res)
            return $res[0];

        return NULL;
    }
    public function saveGatewayLog($params){

        if (isset($_GET['debug']) && $_GET['debug']==2) {
            return null;
        }

        $params['request'] = str_replace("'", "", $params['request']);
        $params['response'] = str_replace("'", "", $params['response']);

        $coy_id='CTL';
        $gateway_id='OPTIMO';
        $gateway_code = md5(trim($params['trans_id']) . time());
        $sql = "INSERT INTO b2b_gateway_message 
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id,request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('$coy_id','$gateway_id','".$gateway_code."','".trim($params['trans_id'])."','','' ,
                        '".trim($params['url'])."','".$params['request']."','".$params['status_level']."','".$params['return_status']."','".$params['response']."',
                        '".getRealIpAddr(15)."',GETDATE())";
        $this->db->query($sql);
    }
}
?>