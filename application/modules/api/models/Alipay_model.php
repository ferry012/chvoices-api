<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Alipay_model extends Base_Common_Model {
    
    private $partner;
    private $partnerkey;
    private $currency;
    private $apiurl;

    public function __construct() {
        parent::__construct();

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            /*$this->partner = '2088621736144172';
            $this->partnerkey = 'fpk9f2tg3cecocbyfk76pnkxmetjujan';
            $this->currency = 'SGD';
            $this->apiurl = 'https://intlmapi.alipay.com/gateway.do?';*/
            
            $this->partner = 'Guest';
            $this->partnerkey = md5('C6318'); //md5('Guest');
            $this->currency = 'SGD';
            $this->pay_apiurl = 'https://ccpay.sg/dci/api_v2/cashier_app';
            $this->check_apiurl = 'https://ccpay.sg/dci/api_v2/transaction_detail_app';
            $this->refund_apiurl = 'https://ccpay.sg/dci/api_v2/refund_app';
            $this->pay_notify_url = base_url('/alipay/alipay_svr_return_pay');
            $this->refund_notify_url = base_url('/alipay/alipay_svr_return_refund');
        }
        else {
            $this->partner = 'Guest';
            $this->partnerkey = md5('C6318'); //md5('Guest');
            $this->currency = 'SGD';
            $this->pay_apiurl = 'https://ccpay.sg/dci/api_v2/cashier_app';
            $this->check_apiurl = 'https://ccpay.sg/dci/api_v2/transaction_detail_app';
            $this->refund_apiurl = 'https://ccpay.sg/dci/api_v2/refund_app';
            $this->pay_notify_url = base_url('/alipay/alipay_svr_return_pay');
            $this->refund_notify_url = base_url('/alipay/alipay_svr_return_refund');
        }
    }
    
    public function saveGatewayLog($params){
        $coy_id='CTL';
        $gateway_id='ALIPAY';
        $gateway_code = md5($params['trans_id'].$params['loc_id'].$params['pos_id'].$params['buyer_code'].$params['tx_time']);
        $gateway_request = $params['trans_id'].'/'.$params['loc_id'].'/'.$params['pos_id'].'/'.$params['buyer_code'].'?t='.$params['tx_time'] ;
        if ($params['status']=="NEW") {
            $sql = "INSERT INTO b2b_gateway_message 
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id,request_link,request_msg,status_level,created_by,created_on)
                        VALUES ('$coy_id','$gateway_id','".$gateway_code."','".trim($params['trans_id'])."','".trim($params['loc_id'])."','".trim($params['pos_id'])."' ,
                        '".trim($params['url'])."','".$params['request']."','0','".getRealIpAddr(15)."',GETDATE())";
            $this->db->query($sql);
        }
        else {
            $sql = "UPDATE b2b_gateway_message
                        SET status_level=1, return_status='".$params['status']."', return_msg='".$params['response']."'
                        WHERE coy_id='$coy_id' AND gateway_id='$gateway_id' AND gateway_code='$gateway_code' ";
            $this->db->query($sql);
        }
    }
    
    public function pay($trans_id,$loc_id,$pos_id,$trans_amt,$buyer_code){
        if ($pos_id=='YS') {
            $rtn = array(
                "trans_id" => $trans_id,
                "payment_id" => "123123123",
                "payment_amount" => 30
            );
            return $rtn;
            exit;
        }

        $tx_time = date("YmdHis");
        $tx_id = trim($trans_id);
        
        //New API mode
        $user_id = 'CHALLENGER-'.trim($loc_id); //$this->partner;
        $user_password = $this->partnerkey;
        $amount = trim(str_replace([',','$'],'',$trans_amt));
        $payer = trim($buyer_code);
        $order = 'Challenger '.trim($trans_id);
        $notify_url = $this->pay_notify_url;
        $alipay_url = $this->pay_apiurl;
        
        $post_array_data = array();
        $post_array_data["user_id"] = $user_id;
        $post_array_data["user_password"] = $user_password;
        $post_array_data["amount"] = $amount;
        $post_array_data["payer"] = $payer;
        $post_array_data["order"] = $order;
        $post_array_data["notify_url"] = $notify_url;
        $post_array_data["sign_string"] = md5($user_id.$user_password.$amount.$payer.$order.$notify_url);

        //Old api mode log
        $this->saveGatewayLog( array(
            "status" => 'NEW',
            "trans_id" => $tx_id,
            "loc_id" => $loc_id,
            "pos_id" => $pos_id,
            "buyer_code" => $buyer_code,
            "url" => $alipay_url,
            "tx_time" => $tx_time,
            "response" => json_encode($post_array_data)
        ) );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->pay_apiurl);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array_data);

        $responses = curl_exec($ch);
        $responses = json_decode($responses);
        curl_close($ch);
        
        $return = [];
        
        if($responses->is_success === 'T')
        {
            //Check for trade_status = 90 - unknown result
            if($responses->trade_status === 90)
            {
                $return['error'] = trim($responses->trade_msg);
                $return['result'] = 'UNKNOW';
            }
            else
            {
                //Successful transaction
                $return['payment_id'] = $responses->trade_no;
                $return['trans_id'] = $responses->partner_trans_id;
                $return['result'] = 'SUCCESS';
                $return['payment_amount'] = $responses->trans_amount;
                
                //{"payment_id":"2019070522001488551056656630","trans_id":"T1B26417","result":"SUCCESS","payment_amount":"32.00"}
            }
        }
        elseif($responses->is_success === 'F')
        {
            //Failed transaction
            $return['error'] = trim($responses->trade_msg);
            $return['result'] = 'FAILED';
        }
        else
        {
            $return['error'] = trim($responses->trade_msg);
            $return['result'] = 'FAILED';
        }
        
        $remove[] = "'s";
        $remove[] = "'";
        $remove[] = '"';
        $remove[] = "-";
        $remove[] = "(";
        $remove[] = ")";

        foreach($return as $key=>$data)
        {
            $return[$key] = str_replace($remove, '', $data);
        }
        
        foreach($responses as $key=>$data)
        {
            $responses->$key = str_replace($remove, '', $data);
        }
        
        $this->saveGatewayLog( array(
            "status" => json_encode($return),
            "trans_id" => $tx_id,
            "loc_id" => $loc_id,
            "pos_id" => $pos_id,
            "buyer_code" => $buyer_code,
            "url" => $alipay_url,
            "tx_time" => $tx_time,
            "response" => json_encode($responses)
        ) );

        if ($this->input->get('debug')==1){
            print_r($return);
            exit;
        }
        
        return $return;
    }

    public function refund($trans_id,$loc_id,$pos_id,$trans_amt,$trans_old){

        $buyer_code = time();
        $alipay_url = $this->refund_apiurl;
        $tx_time = date("YmdHis");
        $tx_id = trim($trans_id);

        //New API mode
        $user_id = 'CHALLENGER-'.trim($loc_id); //$this->partner;
        $user_password = $this->partnerkey;
        $amount = trim(str_replace([',','$'],'',$trans_amt));
        $order = 'Challenger '.trim($trans_id);
        $notify_url = $this->refund_notify_url;
        
        $this->saveGatewayLog( array(
            "status" => 'NEW',
            "trans_id" => $tx_id,
            "loc_id" => $loc_id,
            "pos_id" => $pos_id,
            "buyer_code" => $buyer_code,
            "url" => $alipay_url,
            "tx_time" => $tx_time,
            "response" => ''
        ) );
        
        $post_array_data = array();
        $post_array_data["user_id"] = $user_id;
        $post_array_data["user_password"] = $user_password;
        $post_array_data["amount"] = $amount;
        $post_array_data["order"] = $order;
        $post_array_data["notify_url"] = $notify_url;
        $post_array_data["sign_string"] = md5($user_id.$user_password.$amount.$order.$notify_url);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->refund_apiurl);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array_data);

        $responses = curl_exec($ch);
        $responses = json_decode($responses);
        curl_close($ch);
        
        $return = [];
        if($responses->is_success === 'T')
        {
            //Check for trade_status = 90 - unknown result
            if($responses->trade_status === 90)
            {
                $return['error'] = trim($responses->error_msg);
                $return['result'] = 'UNKNOW';
            }
            else
            {
                //Successful transaction
                $return['payment_id'] = $responses->trade_no;
                $return['trans_id'] = $responses->partner_trans_id;
                $return['result'] = 'SUCCESS';
                $return['payment_amount'] = $responses->trans_amount;
                
                //{"payment_id":"2019070522001488551056656630","trans_id":"T1B26417","result":"SUCCESS","payment_amount":"32.00"}
            }
        }
        elseif($responses->is_success === 'F')
        {
            //Failed transaction
            $return['error'] = trim($responses->error_msg);
            $return['result'] = 'FAILED';
        }
        else
        {
            $return['error'] = trim($responses->error_msg);
            $return['result'] = 'FAILED';
        }
        
        $remove[] = "'s";
        $remove[] = "'";
        $remove[] = '"';
        $remove[] = "-";
        $remove[] = "(";
        $remove[] = ")";

        foreach($return as $key=>$data)
        {
            $return[$key] = str_replace($remove, '', $data);
        }

        foreach($responses as $key=>$data)
        {
            $responses->$key = str_replace($remove, '', $data);
        }
        
        $this->saveGatewayLog( array(
            "status" => json_encode($return),
            "trans_id" => $tx_id,
            "loc_id" => $loc_id,
            "pos_id" => $pos_id,
            "buyer_code" => $buyer_code,
            "url" => $alipay_url,
            "tx_time" => $tx_time,
            "response" => json_encode($responses)
        ) );

        if ($this->input->get('debug')==1){
            print_r($post_array_data);
            exit;
        }

        return $return;
    }

    public function check($trans_id){

        $partner = 'S170818'; //$this->partner;
        $partnerkey = md5('C9828'); //$this->partnerkey;
        $order = trim($trans_id);
        
        $parameter = array(
            "user_id" => $partner,
            "user_password" => $partnerkey,
            "order" => $order
        );
        
        $post_array_data = array();
        $post_array_data["user_id"] = $partner;
        $post_array_data["user_password"] = $partnerkey;
        $post_array_data["order"] = $order;
        $post_array_data["sign_string"] = md5($partner.$partnerkey.$order);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->check_apiurl);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array_data);

        $responses = curl_exec($ch);
        $responses = json_decode($responses);
        curl_close($ch);
        
        $return = [];
        
        if($responses->is_success === 'T')
        {
            //Get results details
            foreach ($responses->results as $key=>$result_datao)
            {
                $result_data = (array) $result_datao;
                $return['payment_id'] = $result_data['trade_no'];
                $return['result2'] = '';
                $return['trans_id'] = $result_data['partner_trans_id'];
                $return['result1'] = 'SUCCESS';
                $return['payment_amount'] = $result_data['trans_amount'];
            }
        }
        elseif($responses->is_success === 'F')
        {
            //Failed transaction
            $return['error'] = trim($responses->error_msg);
            $return['result'] = 'FAILED';
        }
        else
        {
            $return['error'] = trim($responses->error_msg);
            $return['result'] = 'FAILED';
        }

        if ($this->input->get('debug')==1){
            print_r($parameter);
            print_r($responses);
            exit;
        }

        return $return;
    }
    
    public function svr_return_pay(){
        if ($_POST) {
            $return = [];
            
            if($_POST['is_success'] === 'T')
            {
                //Check for trade_status = 90 - unknown result
                if($_POST['trade_status'] === 90)
                {
                    $return['error'] = trim($_POST['trade_msg']);
                    $return['result'] = 'UNKNOW';
                }
                else
                {
                    //Successful transaction
                    $return['payment_id'] = $_POST['trade_no'];
                    $return['trans_id'] = $_POST['partner_trans_id'];
                    $return['result'] = 'SUCCESS';
                    $return['payment_amount'] = $_POST['trans_amount'];

                    //{"payment_id":"2019070522001488551056656630","trans_id":"T1B26417","result":"SUCCESS","payment_amount":"32.00"}
                }
            }
            elseif($_POST['is_success'] === 'F')
            {
                //Failed transaction
                $return['error'] = trim($_POST['trade_msg']);
                $return['result'] = 'FAILED';
            }
            else
            {
                $return['error'] = trim($_POST['trade_msg']);
                $return['result'] = 'FAILED';
            }
            
            $remove[] = "'s";
            $remove[] = "'";
            $remove[] = '"';
            $remove[] = "-";
            $remove[] = "(";
            $remove[] = ")";

            foreach($return as $key=>$data)
            {
                $return[$key] = str_replace($remove, '', $data);
            }
            
            foreach($_POST as $key=>$data)
            {
                $_POST[$key] = str_replace($remove, '', $data);
            }
            
            $this->saveGatewayLog( array(
                "status" => json_encode($return),
                "trans_id" => $return['trans_id'],
                "loc_id" => '',
                "pos_id" => '',
                "buyer_code" => $return['buyer_email'],
                "url" => $this->pay_apiurl,
                "tx_time" => $return['pay_time'],
                "response" => json_encode($_POST)
            ) );
        }
    }
    
    public function svr_return_refund(){
        if ($_POST) {
            $return = [];
            
            if($_POST['is_success'] === 'T')
            {
                //Check for trade_status = 90 - unknown result
                if($_POST['trade_status'] === 90)
                {
                    $return['error'] = trim($_POST['error_msg']);
                    $return['result'] = 'UNKNOW';
                }
                else
                {
                    //Successful transaction
                    $return['payment_id'] = $_POST['trade_no'];
                    $return['trans_id'] = $_POST['partner_trans_id'];
                    $return['result'] = 'SUCCESS';
                    $return['payment_amount'] = $_POST['trans_amount'];

                    //{"payment_id":"2019070522001488551056656630","trans_id":"T1B26417","result":"SUCCESS","payment_amount":"32.00"}
                }
            }
            elseif($_POST['is_success'] === 'F')
            {
                //Failed transaction
                $return['error'] = trim($_POST['error_msg']);
                $return['result'] = 'FAILED';
            }
            else
            {
                $return['error'] = trim($_POST['error_msg']);
                $return['result'] = 'FAILED';
            }

            $remove[] = "'s";
            $remove[] = "'";
            $remove[] = '"';
            $remove[] = "-";
            $remove[] = "(";
            $remove[] = ")";

            foreach($return as $key=>$data)
            {
                $return[$key] = str_replace($remove, '', $data);
            }
            
            foreach($_POST as $key=>$data)
            {
                $_POST[$key] = str_replace($remove, '', $data);
            }

            $this->saveGatewayLog( array(
                "status" => json_encode($return),
                "trans_id" => $return['trans_id'],
                "loc_id" => '',
                "pos_id" => '',
                "buyer_code" => $return['buyer_email'],
                "url" => $this->refund_apiurl,
                "tx_time" => $return['pay_time'],
                "response" => json_encode($_POST)
            ) );
        }

        /*if ($this->input->get('debug')==1){
            print_r($return);
            exit;
        }
        
        return $return;*/
    }

    private function _doCurl($apiUrl){
        // MAKING THE CALL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$apiUrl);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $data = curl_exec($ch);

        if ($this->input->get('debug')==1){
            $errmsg = curl_error($ch);
            $cInfo = curl_getinfo($ch);

            echo print_r($data,true) . "<hr>" . print_r($errmsg,true) . "<hr>" . print_r($cInfo,true) . "<hr>";
        }
        curl_close($ch);
        return $data;
    }

}

function paraFilter($para) {
    $para_filter = array();
    while (list ($key, $val) = each ($para)) {
        if($key == "sign" || $key == "sign_type" || $val == "")continue;
        else	$para_filter[$key] = $para[$key];
    }
    return $para_filter;
}
function argSort($para) {
    ksort($para);
    reset($para);
    return $para;
}
function createLinkstring($para,$urlencoding=true) {
    $arg  = "";
    while (list ($key, $val) = each ($para)) {
        $value = ($urlencoding==true) ? urlencode($val) : $val ;
        $arg.=$key."=".$value."&";
    }
    $arg = substr($arg,0,count($arg)-2);
    if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}

    return $arg;
}
function md5Sign($prestr, $key) {
    $prestr = $prestr . $key;
    return md5($prestr);
}

?>
