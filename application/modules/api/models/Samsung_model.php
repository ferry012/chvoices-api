<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Samsung_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();

        if (ENVIRONMENT=="production") {
            // Product env
            $this->host = '203.126.64.186';
            $this->username = 'mcs2user';
            $this->password = 'sp24yr%21%40';
            $this->base = '/';
        } else {
            // Test env
//            $this->host = '203.126.64.186';
//            $this->username = 'mcs2user';
//            $this->password = 'sp24yr%21%40';
//            $this->base = '/';

            $this->host = '10.0.35.201';
            $this->username = 'ec2-user';
            $this->password = 'hr%40ctl2016%21';
            $this->base = '/home/ec2-user/samsung/';
        }

    }

    public function generate_data($type, $date='')
    {
        if ($date == '') {
            $today = date('Ymd');
            $from = date("Y-m-d", strtotime("-1 day"));
            $to = date('Y-m-d');
        } else {
            $today = date('Ymd', strtotime($date));
            $from = date('Y-m-d', strtotime($date));
            $to = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        if ($type == 'sales') {
            $data = $this->_get_sales_date($from, $to);
            $now = date('YmdHi');
            $filename = 'assets/logs/MCS_SALES_CHALLENGERSINGAPORE_'.$now.'.txt';
            $new_data = [];
            foreach ($data as $k => $v) {
                $new_data[] = [
                    "C540", "CHALLENGERSINGAPORE", "C000168165", $today, (int)$v['trans_qty'], $v['inv_dim4'],
                    $v['lot_id']
                ];
            }
            $res = $this->_write_array_to_file_1($new_data, $filename);
            return $res;
        }
    }

    public function _write_array_to_file($array, $filename)
    {
        $text = "";
        foreach ($array as $k => $v) {
            $line = "";
            $num_items = count($v);
            $i = 0;
            foreach ($v as $n) {
                if (++$i === $num_items) {
                    $line .= '"' . $n . '"';
                } else {
                    $line .= '"' . $n . '",';
                }
            }
            $text .= $line . "\n";
        }
        file_put_contents($filename, print_r($text, TRUE));
        $res = $this->_upload_file($filename);
        return $res;
    }

    public function _write_array_to_file_1($array, $filename)
    {
        $text = "";
        foreach ($array as $k => $v) {
            $line = "";
            foreach ($v as $n) {
                $line .= $n . "\t";
            }
            $text .= $line . "\n";
        }
        file_put_contents($filename, print_r($text, TRUE));
        $res = $this->_upload_file($filename);
        return $res;
    }

    public function _upload_file($filename)
    {
        $ch = curl_init();
        $localfile = end(explode('/', $filename));
        $fp = fopen($filename, 'r');
        $url = 'sftp://' . $this->username . ':' . $this->password . '@' . $this->host . $this->base . $localfile;

        curl_setopt($ch, CURLOPT_STDERR, fopen(getcwd().'/assets/errorlog.txt', 'w'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_UPLOAD, 1);
        curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
        curl_setopt($ch, CURLOPT_INFILE, $fp);
        curl_setopt($ch, CURLOPT_INFILESIZE, filesize($filename));
        $res = curl_exec ($ch);

        $error_no = curl_error($ch);
        curl_close ($ch);

        if ($error_no == 0) {
            return 1;
        } else {
            return 0;
        }
    }


    private function _get_sales_date($from, $to)
    {
        $sql_pos = "select r.trans_date, r.trans_qty, rtrim(i.inv_dim4) inv_dim4, rtrim(p.lot_id) lot_id, rtrim(r.trans_id) trans_id
                    from rep_sales_data r 
                    left join pos_transaction_lot p on p.trans_id = r.trans_id 
                    left join ims_item_list i on r.item_id = i.item_id 
                    where r.coy_id = 'CTL' and i.inv_dim4 in ('SAMSUNG-TAB-ANDROID', 'SAMSUNG-SP-ANDRO') and r.trans_type not in ('HI', 'HR', 'HX') 
                    and r.trans_date >= '$from' and r.trans_date < '$to'
                    order by r.trans_date asc";
        $sql_sms = "select r.trans_date, r.trans_qty, rtrim(i.inv_dim4) inv_dim4, rtrim(s.lot_id) lot_id, rtrim(r.trans_id) trans_id
                    from rep_sales_data r
                        left join sms_invoice_lot s on s.invoice_id = r.trans_id
                        left join ims_item_list i on r.item_id = i.item_id
                    where r.coy_id = 'CTL'
                      and i.inv_dim4 in ('SAMSUNG-TAB-ANDROID', 'SAMSUNG-SP-ANDRO')
                      and r.trans_type in ('HI', 'HR', 'HX')
                      and r.trans_date >= '2021-02-10' and r.trans_date < '2021-02-11'
                    order by r.trans_date asc";
        $result_pos = $this->db->query($sql_pos)->result_array();
        $result_sms = $this->db->query($sql_sms)->result_array();
        $result = array_merge($result_pos, $result_sms);
        return ($result) ? $result : NULL;
    }

}

?>
