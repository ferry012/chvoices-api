<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Crosspoint_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
        $http_origin = $_SERVER['HTTP_ORIGIN'];
        header("Access-Control-Allow-Origin: *");

        $this->loc_list = array(
            'BF' => 'Challenger Bugis Junction Basement 1',
            'WP' => 'Challenger Waterway Point',
            'COM' => 'Challenger - Compass One',
            'FM' => 'Challenger Tampines Hub',
            'MB' => 'Challenger Musica @ Ion',
            'GWC' => 'Challenger Great World City',
            'JL' => 'Challenger Jewel  Changi Airport',
            'WCP' => 'West Coast Plaza Challenger',
            'PLQ' => 'Challenger Paya Lebar Quarter',
            'JP' => 'Jurong Point Challenger',
            'VC' => 'VivoCity Challenger'
        );
    }

    public function crosspoint($view,$loc_id,$date){
        if (strtoupper($view)=='MONTH') {
            $this->month($loc_id, $date);
        }
        else {
            $this->daily($loc_id, $date);
        }
    }

    public function month($loc_id,$m){

        $loc_list = $this->loc_list;
        $loc_id = ($loc_id=='') ? 'BF' : trim($loc_id);

        $m = ($m=='') ? time() : strtotime($m);
        $dd_first = date('Y-m-01', $m);
        $dd_last = date('Y-m-t', $m);
        $first_time = strtotime( $dd_first . "10am");

        $counter_times = array();
        $dd_date = strtotime($dd_first . "10am");
        for ($d=1;$d <= date('t', strtotime($dd_first));$d++ ) {
            for ($i = 0; $i <= 12; $i++) {
                $counter_times[] = $dd_date;
                $dd_date += 3600;
            }
            $dd_date += (11 * 3600);
        }

        $last_in = $last_out = 0; $last_date='';
        $this->db = $this->load->database('hachi', TRUE);
        $sql = "SELECT * FROM crosspoint WHERE counter_time in ('". implode("','",$counter_times) ."') and loc_name='".$loc_list[$loc_id]."' order by counter_time";
        $cp_data = $this->db->query($sql)->result_array();
        foreach ($cp_data as $r) {
            if ($last_date!=date('Y-m-d',$r['counter_time'])) {
                $last_date = date('Y-m-d',$r['counter_time']);
                $last_in=0;
                $last_out=0;
            }
            $results[date('Y-m-d H:i:s',$r['counter_time'])] = array(
                "visitor_in" => $r['visitor_in'],
                "visitor_out" => $r['visitor_out'],
                "alarm_count" => $r['alarm_count'],
                "deactivation" => $r['deactivation'],
                "period_in" => intval($r['visitor_in']) - $last_in,
                "period_out" => intval($r['visitor_out']) - $last_out,
                "trans_count" => 0,
                "trans_qty" => 0,
                "trans_value" => 0
            );
            $last_in = intval($r['visitor_in']);
            $last_out = intval($r['visitor_out']);
        }

        $this->db = $this->load->database('cherps_hachi', TRUE);
        $sql = "SELECT CONVERT(nvarchar(19), cast(l.trans_date as date) , 120) [tdate],
                       DATEPART(HOUR, l.trans_date) as [th], 
                       (DATEPART(MINUTE, l.trans_date) / 60) as [tm], 
                       COUNT(DISTINCT l.trans_id) as [c], 
                       SUM(i.trans_qty) as [q], 
                       SUM(i.trans_qty*i.unit_price) as [v] 
                    FROM pos_transaction_list l
                    JOIN pos_transaction_item i ON i.trans_id=l.trans_id
                    WHERE l.loc_id='$loc_id' AND CAST(l.trans_date AS date) >= '".$dd_first."' AND CAST(l.trans_date AS date) <= '".$dd_last."'
                        AND l.status_level>0 AND i.status_level>0 
                        -- (Add DM)
                    GROUP BY 
                    CONVERT(nvarchar(19), cast(l.trans_date as date) , 120),
                    DATEPART(YEAR, l.trans_date),
                    DATEPART(MONTH, l.trans_date),
                    DATEPART(DAY, l.trans_date),
                    DATEPART(HOUR, l.trans_date),
                    (DATEPART(MINUTE, l.trans_date) / 60)
                    order by tdate,th,tm";
        $pos_data = $this->db->query($sql)->result_array();
        foreach ($pos_data as $p) {
            $k_date = date('Y-m-d',strtotime($p["tdate"])) .' '.$p["th"].':'.sprintf("%02d", ($p["tm"]*15) ).':'.'00';

            $results[$k_date]["trans_count"] = $p["c"];
            $results[$k_date]["trans_qty"] = intval($p["q"]);
            $results[$k_date]["trans_value"] = intval($p["v"]);
        }

        $result_bydate = array();
        for ($c=1;$c<28;$c++) {
            $c1 = date('Y-m-', $m) . sprintf("%02d", $c);
            $result_bydate[$c1] = array();
        }
        foreach ($results as $datetime=>$row) {
            $t = strtotime($datetime);
            $d = date("Y-m-d",$t);
            $h = date("H",$t);
            $result_bydate[$d][$h] = $row;

        }
        //echo "<pre>"; print_r($result_bydate);exit;

        // FORM THE REPORT VISUAL
        if (strtolower($_GET["format"])=="html") {
            echo '<table border=1 cellpadding=3>';
            echo '<tr><td rowspan=2>DATE</td>';
            for ($y = 10; $y <= 22; $y++) {
                echo '<td colspan=2>' . $y . ':00</td>';
            }
            echo '</tr>';
            echo '<tr>';
            for ($y = 10; $y <= 22; $y++) {
                echo '<td>Visitor</td>';
                echo '<td>Txn</td>';
            }
            echo '</tr>';
            foreach ($result_bydate as $date => $dayrecords) {
                echo '<tr><td>' . $date . '</td>';
                for ($y = 10; $y <= 22; $y++) {
                    echo '<td>' . $dayrecords[$y]['period_in'] . '</td>';
                    echo '<td>' . $dayrecords[$y]['trans_count'] . '</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
        }
        else {
            $csvexp = array();

            $csvtmp = array();
            $csvtmp[] = 'DATE';
            for ($y = 10; $y <= 22; $y++) {
                $csvtmp[] = $y.':00 Visitor IN';
                $csvtmp[] = $y.':00 Visitor OUT';
                $csvtmp[] = $y.':00 Txn Count';
                $csvtmp[] = $y.':00 Txn Value';
            }
            $csvexp[] = $csvtmp;

            foreach ($result_bydate as $date => $dayrecords) {
                $csvtmp = array();
                $csvtmp[] = $date;
                for ($y = 10; $y <= 22; $y++) {
                    $csvtmp[] = $dayrecords[$y]['period_in'];
                    $csvtmp[] = $dayrecords[$y]['period_out'];
                    $csvtmp[] = $dayrecords[$y]['trans_count'];
                    $csvtmp[] = $dayrecords[$y]['trans_value'];
                }
                $csvexp[] = $csvtmp;
            }


            $handle_path = FCPATH . 'assets/uploads/';
            $handle_filename = 'crosspoint-'.strtolower($loc_id).'.csv';
            $fp = fopen($handle_path . $handle_filename, 'w');
            foreach ($csvexp as $fields) {
                fputcsv($fp, $fields);
            }
            fclose($fp);

            redirect(base_url('assets/uploads/'.$handle_filename));
        }

    }
    
    public function daily($loc_id,$d){

        $loc_list = $this->loc_list;
        $loc_id = ($loc_id=='') ? 'BF' : trim($loc_id);
        $key_date = ($d=='') ? date('Y-m-d') : date('Y-m-d', strtotime($d));

        $last_in = $last_out = 0;
        $this->db = $this->load->database('hachi', TRUE);
        $sql = "SELECT * FROM crosspoint WHERE counter_date='".$key_date."' and loc_name='".$loc_list[$loc_id]."' order by counter_time";
        $cp_data = $this->db->query($sql)->result_array();
        foreach ($cp_data as $r) {
            $results[date('Y-m-d H:i:s',$r['counter_time'])] = array(
                "visitor_in" => $r['visitor_in'],
                "visitor_out" => $r['visitor_out'],
                "alarm_count" => $r['alarm_count'],
                "deactivation" => $r['deactivation'],
                "period_in" => intval($r['visitor_in']) - $last_in,
                "period_out" => intval($r['visitor_out']) - $last_out,
                "trans_count" => 0,
                "trans_qty" => 0,
                "trans_value" => 0
            );
            $last_in = intval($r['visitor_in']);
            $last_out = intval($r['visitor_out']);
        }

        $this->db = $this->load->database('cherps_hachi', TRUE);
        $sql = "SELECT DATEPART(HOUR, l.trans_date) as [th], (DATEPART(MINUTE, l.trans_date) / 15) as [tm], COUNT(DISTINCT l.trans_id) as [c], SUM(i.trans_qty) as [q], SUM(i.trans_qty*i.unit_price) as [v] 
                    FROM pos_transaction_list l
                    JOIN pos_transaction_item i ON i.trans_id=l.trans_id
                    WHERE l.loc_id='$loc_id' AND CAST(l.trans_date AS date) = '".$key_date."'
                        AND l.status_level>0 AND i.status_level>0
                        AND i.unit_price>0
                    GROUP BY 
                    DATEPART(YEAR, l.trans_date),
                    DATEPART(MONTH, l.trans_date),
                    DATEPART(DAY, l.trans_date),
                    DATEPART(HOUR, l.trans_date),
                    (DATEPART(MINUTE, l.trans_date) / 15)";
        $pos_data = $this->db->query($sql)->result_array();
        foreach ($pos_data as $p) {
            $k_date = $key_date.' '.$p["th"].':'.sprintf("%02d", ($p["tm"]*15) ).':'.'00';

            $results[$k_date]["trans_count"] = $p["c"];
            $results[$k_date]["trans_qty"] = intval($p["q"]);
            $results[$k_date]["trans_value"] = intval($p["v"]);
        }

        if (strtolower($_GET["format"])=="html") {
            echo '<table width=800>';
            echo '<thead><th></th><th>Accu. In</th><th>Accu. Out</th><th>Alarm</th><th>Deactivate</th> <th>In</th><th>Out</th> <th>Trans Count</th><th>Qty</th><th>Value</th></thead>';
            foreach ($results as $k=>$r) {
                echo '<tr>';
                echo '<td>'.$k.'</td>';
                echo '<td>'.$r['visitor_in'].'</td>';
                echo '<td>'.$r['visitor_out'].'</td>';
                echo '<td>'.$r['alarm_count'].'</td>';
                echo '<td>'.$r['deactivation'].'</td>';
                echo '<td>'.$r['period_in'].'</td>';
                echo '<td>'.$r['period_out'].'</td>';

                echo '<td>'.$r['trans_count'].'</td>';
                echo '<td>'.$r['trans_qty'].'</td>';
                echo '<td>$'.$r['trans_value'].'</td>';
                echo '</tr>';
            }
            echo '</table>';
        }
        else {
            header("Access-Control-Allow-Origin: *");
            header("Content-type:application/json");
            print_r($data = json_encode($results));
        }

    }

    public function load($step) {

        $this->db = $this->load->database('hachi', TRUE);
        // Connect S3 and read into folder s3/buckets/ctl-lambda/SES-crosspoint-xml

        $this->load->library('s3');
        $this->config->load('s3', true, true);
        $s3_config = $this->config->item('s3');

        $processList = [];

        if ($step==0 || $step==1) {
            // Record all filenames
            $sql = "SELECT * FROM xml_processed ORDER BY last_modified DESC LIMIT 0,55";
            $keys = $this->db->query($sql)->result_array();
            $keyCheck = '';
            $keyList = [];
            foreach($keys as $i => $k) {
                $keyList[] = $k['key_id'];
                if ($i == 50)
                    $keyCheck = $k['key_id'];
            }

            $lists = $this->s3->getBucket('ctl-lambda','CTL-crosspoint-xml', $keyCheck);
            foreach ($lists as $l) {
                $key = $l['name'];
                $time = date('Y-m-d H:i:s', $l['time']);

                if (!in_array($key, $keyList)) {
                    $processList[] = $key . ' (' . $time . ')';

                    $sql = "INSERT INTO xml_processed VALUES ('$key','$time',0)";
                    $this->db->query($sql);
                }
            }
        }

        $xmlListSuccess = [];
        $xmlListError = [];

        if ($step==0 || $step==2) {
            // Process xml
            $sql = "SELECT * FROM xml_processed WHERE status_level=0 ORDER BY last_modified LIMIT 0,600";
            $keys = $this->db->query($sql)->result_array();
            foreach ($keys as $key) {
                // Read xml file
                $ob = $this->s3->getObject('ctl-lambda', $key['key_id']);
                $data = simplexml_load_string($ob->body);

                $ins = [];
                $ins['loc_name'] = (string)$data->Location->attributes()->name;
                $ins['counter_date'] = (string)$data->Visitors->Day->attributes()->date;
                $ins['counter_time'] = strtotime($data->FileInfo->GenerationDate); // time();
                //$ins['counter_time_text'] = (string) $data->FileInfo->GenerationDate; // time();
                $ins['visitor_in'] = (int)$data->Visitors->Day->attributes()->in;
                $ins['visitor_out'] = (int)$data->Visitors->Day->attributes()->out;
                $ins['alarm_count'] = isset($data->Alarms) ? (int)$data->Alarms->Day->attributes()->total : 0;
                $ins['deactivation'] = isset($data->Deactivations) ? (int)$data->Deactivations->Day->attributes()->successfull : 0;
                $ins["created_on"] = date('Y-m-d H:i:s');
                $q = $this->db->insert('crosspoint', $ins);
                if ($q) {
                    // Update status of this key
                    $sql = "update xml_processed SET status_level=1 WHERE key_id = '" . $key['key_id'] . "'; ";
                    $q = $this->db->query($sql);
                    if ($q) {
                        $xmlListSuccess[] = [
                            'key_id' => $key['key_id'],
                            'xml_status' => 1,
                            'msg' => 'Completed: ' . $ins['loc_name'] .' '. date('Y-m-d H:i:s', $ins['counter_time']),
                            'data' => $ins
                        ];
                    }
                    else {
                        $xmlListError[] = [
                            'key_id' => $key['key_id'],
                            'xml_status' => 1,
                            'msg' => 'WARNING. Duplicate crosspoint data. Fail to update key status',
                            'data' => $ins
                        ];
                    }
                }
                else {
                    $xmlListError[] = [
                        'key_id' => $key['key_id'],
                        'xml_status' => 0,
                        'msg' => 'Fail to save crosspoint data',
                        'data' => $ins
                    ];
                }
            }
        }

        echo json_encode([
            's3' => [
                'count' => count($processList),
                'msg' => count($processList) . ' new keys loaded from S3',
                'data' => $processList
            ],
            'xml' => [
                'error' => [
                    'count' => count($xmlListError),
                    'data' => $xmlListError
                ],
                'success' => [
                    'count' => count($xmlListSuccess),
                    'data' => $xmlListSuccess
                ]
            ]
        ]);
    }
}

?>
