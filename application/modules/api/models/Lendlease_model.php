<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Lendlease_model extends Base_Common_Model {

    private $subkey;
    private $apiurl;
    private $notification;
    private $gatewayid;
    private $gatewaycode;
    private $gatewayuser;
    private $token;

    private $libcode;
    private $vchfindim;

    public function __construct() {
        parent::__construct();

        $this->gatewayid = 'LENDLEASE';
        $this->gatewaycode = md5(time());
        $this->token = '';
        $this->gatewayuser = '';

        $this->libcode = array(
            '313' => array('mall_code'=>'65100003','machine_id'=>'8000198'),
            'JEM' => array('mall_code'=>'65100003','machine_id'=>'8000198')
        );

        $this->vchfindim = array(
            'PHYSICAL' => 'LENDLEASE',
            'EVOUCHER' => 'E-LENDLEASE'
        );

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
//            $this->subkey = 'bb7117edf2ba44a59d2aa6b6d367b43a';
//            $this->apiurl = 'https://api.capitastar.com/';
//            $this->notification = 'MIS.team@challenger.sg; ';
        }
        else {
            $this->subkey = '44501d7b43f44bf287bcf9992a745edf';
            $this->apiurl = 'http://202.136.18.58/';
            $this->notification = 'yongsheng@challenger.sg; ';
        }
    }

    public function auth_token($loc_id,$pos_id){

        if ($this->token!='') {
            return $this->token;
        }

        // Full model - Either reply the valid token or refresh+save+reply new token
        $set_code_acct='LENDLEASE_ACCT'; $set_code_token='LENDLEASE_TOKEN'; $token_validity='-2 days'; $valid_token=[];
        $loc_id = str_replace('-R','',$loc_id);

        $sql = "SELECT * FROM b2b_setting_list
                    WHERE sys_id='API' AND set_code IN ('$set_code_acct','$set_code_token') AND set_default='$loc_id' ORDER BY set_code DESC ";
        $b2b_settings = $this->db->query($sql)->result_array();
        foreach ($b2b_settings as $b2bset) {
            if (trim($b2bset['set_code'])==$set_code_token) {
                $b2btoken = $b2bset;
            }
            else if (trim($b2bset['set_code'])==$set_code_acct) {
                $b2bacct = $b2bset;
            }
        }

        $token_auth_on = date('Y-m-d H:i:s');
        if (strtotime($b2btoken['modified_on']) > strtotime($token_validity)) {
            // Token Valid
            $valid_token['token'] = $b2btoken['remarks'];
            $valid_token['mall_code'] = substr($b2btoken['set_desc'], 0, strpos($b2btoken['set_desc'],','));
            $valid_token['machine_id'] = substr($b2btoken['set_desc'], (strpos($b2btoken['set_desc'],',')+1) );
            $token_auth_on = date('Y-m-d H:i:s',strtotime($b2btoken['modified_on']));
        }
        else {
            // Refresh & Save
            $logging = array("trans_id"=>$loc_id.$pos_id.'-'.rand(10,99), "loc_id"=>$loc_id, "pos_id"=>$pos_id);
            $acct = (array) json_decode(base64_decode($b2bacct['remarks']));
            $qacct['UserID'] = $acct['email'];
            $qacct['Password'] = $acct['password'];
            $refresh = $this->_doCurl('POSManager.asmx/GetToken','',$qacct,$logging);

            $valid_token['token'] = $refresh['Token'];
            $valid_token['mall_code'] = $this->libcode[$loc_id]['mall_code'];
            $valid_token['machine_id'] = $this->libcode[$loc_id]['machine_id'];

            if (strlen($valid_token['token'])>10) {
                if (isset($b2btoken)) {
                    $sql = "UPDATE b2b_setting_list
                        SET set_desc='" . $valid_token['mall_code'].','.$valid_token['machine_id'] . "', remarks='" . $valid_token['token'] . "', modified_by='" . getRealIpAddr(15) . "', modified_on=GETDATE()
                        WHERE sys_id='API' AND set_code='$set_code_token' AND set_default='$loc_id' ";
                    $rtnx = $this->db->query($sql);
                } else {
                    $sql = "INSERT INTO b2b_setting_list (sys_id,set_code,set_default,set_desc,remarks,created_by,created_on,modified_by,modified_on)
                      VALUES ('API','$set_code_token','$loc_id','" . $valid_token['mall_code'].','.$valid_token['machine_id'] . "','" . $valid_token['token'] . "', '" . getRealIpAddr(15) . "',GETDATE(),'" . getRealIpAddr(15) . "',GETDATE() );";
                    $rtnx = $this->db->query($sql);
                }
                $b2btoken['modified_on'] = time();
            }
            else {
                // Cannot auth. Send email to MIS
                $msg = 'LANDLEASE API - AUTH LOGIN';
                $msg.= '<br>Fail to login to LANDLEASE API with '.$acct['email'].'.<br>Please go to https://chvoices.challenger.sg/cherps/page?view=capitastar to save your password to the system.';
                $msg.= '<br><br>'.json_encode($refresh);
                $msgemail = $this->notification;
                if (ENVIRONMENT=="production") $msgemail.= $loc_id.'.ic@challenger.sg; ';
                $this->cherps_email('LANDLEASE API Auth - Fail refresh token '.$loc_id, $msg, $msgemail);
            }

        }

        $this->gatewayuser = array("loc_id"=>$loc_id,"pos_id"=>$pos_id,"auth_on"=>$token_auth_on);
        $this->token = $valid_token;
        return (isset($valid_token)) ? $valid_token : '';
    }

    public function validate($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str){

        $postbody = trim(file_get_contents('php://input'));
        $postarr = json_decode($postbody);
        $wallet_usr = $postarr->member;

        if ($wallet_usr!='') {
            if ($trans_amt>0) {
                return $this->validate_wallet($trans_id, $loc_id, $pos_id, $trans_amt, $wallet_usr);
            }
            else {
                $rtn = array(
                    "code" => 0,
                    "result" => FAIL,
                    "mode" => "VALIDATE",
                    "trans_id" => $trans_id,
                    "error" => 'Invalid amount to validate'
                );
                return $rtn;
            }
        }
        else {
            return $this->validate_physical($trans_id, $loc_id, $pos_id, $trans_amt, $vouchers_str);
        }
    }

    public function validate_physical($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str) { // Validate physical vouchers

        $token = $this->auth_token($loc_id, $pos_id);
        if (!isset($token) || $token == '') {
            return array(
                "code" => -1,
                "result" => "API FAILED"
            );
        }

        $created_on = time();
        $vouchers = $this->_getVouchers($vouchers_str);
        $trans_amt = floatval(trim(str_replace([',', '$'], '', $trans_amt)));

        $validated = array("OK"=>0,"OKAMT"=>0,"FAIL"=>0,"VCHLOG"=>array());
        $logging = array("trans_id"=>$trans_id, "loc_id"=>$loc_id, "pos_id"=>$pos_id);
        $trans_pastvch = $this->get_gwlot($trans_id);

        foreach($vouchers as $vch) {
            $qacct['Token'] = $token['token'];
            $qacct['machine_id'] = $token['machine_id'];
            $qacct['mall_code'] = $token['mall_code'];
            $qacct['voucher_qr'] = $vch;
            $refresh = $this->_doCurl('POSManager.asmx/CheckAnonymousVoucher', '', $qacct, $logging);

            if ($refresh->data->Voucher) {
                $gwvch = (string) $refresh->data->Voucher->voucher_no;
                $gwval = floatval($refresh->data->Voucher->voucher_value);
                $gwremark = array('voucher_qr'=>$vch,'voucher_expiry'=>(string) $refresh->data->Voucher->voucher_expiry_date);
                $gwpar = array();
                $gwpar['duplicates'] = array_key_exists($gwvch,$trans_pastvch);
                $gwpar['loc_id'] = $loc_id;
                //$gwpar['lot_id2'] = $vch;
                $gwpar['trans_id'] = $trans_id;
                $gwpar['lot_id'] = $gwvch;
                $gwpar['unit_price'] = $gwval;
                $gwpar['string_udf1'] = $this->vchfindim['PHYSICAL'];
                $gwpar['string_udf3'] = '-';
                $gwpar['vch_meta'] = $gwremark;
                $this->save_gwlot($gwpar);

                $validated["OK"]++;
                $validated["OKAMT"] += $gwval;
                $validated["VCHLOG"][] = array(
                    "voucher" => $gwvch,
                    "amount" => $gwval,
                    "remark" => $gwremark
                );
            }
            else {
                $validated["FAIL"]++;
                $validated["VCHLOG"][] = array(
                    "voucher" => $qacct['voucher_qr'],
                    "remark" => (string) $refresh->error->ErrorList->error_details->ErrorDetail->error_message
                );
            }
        }

        $rtn = array(
            "code" => ($validated["OK"]>0) ? 1 : 0,
            "result" => ($validated["FAIL"]>0) ? "FAIL" : "SUCCESS",
            "mode" => "VALIDATE",
            "trans_id" => $trans_id,
            "valid_amount" => $validated["OKAMT"],
            "voucher_log" => $validated["VCHLOG"]
        );
        return $rtn;
    }

    public function validate_wallet($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str) { // Get equal value vouchers from wallet

        $token = $this->auth_token($loc_id, $pos_id);
        if (!isset($token) || $token == '') {
            return array(
                "code" => -1,
                "result" => "API FAILED"
            );
        }

        $created_on = time();
        $vouchers = $this->_getVouchers($vouchers_str);
        $trans_amt = floatval(trim(str_replace([',', '$'], '', $trans_amt)));

        $validated = array("OK"=>0,"OKAMT"=>0,"FAIL"=>0,"VCHLOG"=>array());
        $logging = array("trans_id"=>$trans_id, "loc_id"=>$loc_id, "pos_id"=>$pos_id);
        $trans_pastvch = $this->get_gwlot($trans_id);

        // Get all vouchers from wallet
        $qacct['Token'] = $token['token'];
        $qacct['nric'] = '';
        $qacct['qrcode'] = $vouchers[0];
        $qacct['machineid'] = $token['machine_id'];
        $qacct['mallcode'] = $token['mall_code'];
        $refresh = $this->_doCurl('POSManager.asmx/FindShopper','',$qacct,$logging);
        if ($refresh->data->Shopper) {

            $shopper_id = (string) $refresh->data->Shopper->shopper_id;
            $shopper_vch = (array) $refresh->data->Shopper->vouchers;

            $shopper_allvch = array();
            foreach ($shopper_vch['Voucher'] as $v1){
                $v2 = (array) $v1;
                if (strtoupper($v2['voucher_status'])=='UNUSED')
                    $shopper_allvch[substr($v2['voucher_expiry_date'],0,10).'-'.$v2['voucher_record_id']] = $v2;
            }
            ksort($shopper_allvch);

            $trans_amt_calc=$trans_amt;
            foreach ($shopper_allvch as $v3){
                if ( $trans_amt_calc>0 && ($trans_amt_calc-$v3['voucher_value'])>=0 && strtotime($v3['voucher_expiry_date'])>time() ) {
                    $trans_amt_calc -= $v3['voucher_value'];

//                    $qacct['Token'] = $token['token'];
//                    $qacct['machine_id'] = $token['machine_id'];
//                    $qacct['mall_code'] = $token['mall_code'];
//                    $qacct['shopper_id'] = $shopper_id; //'16595';
//                    $qacct['voucher_record_id'] = $v3['voucher_record_id']; //1504;
//                    $refresh = $this->_doCurl('POSManager.asmx/CheckWalletVoucher','',$qacct,$logging);

                    $gwpar = array();
                    $gwpar['duplicates'] = array_key_exists($v3['voucher_no'],$trans_pastvch);
                    $gwremark = array('shopper_qr'=>$vouchers[0],'shopper_id'=>$shopper_id,'voucher_record_id'=>$v3['voucher_record_id'],'voucher_expiry'=>$v3['voucher_expiry_date']);
                    $gwpar['loc_id'] = $loc_id;
                    $gwpar['trans_id'] = $trans_id;
                    $gwpar['lot_id'] = $v3['voucher_no'];
                    //$gwpar['lot_id2'] = $v3['voucher_record_id'];
                    $gwpar['unit_price'] = $v3['voucher_value'];
                    $gwpar['string_udf1'] = $this->vchfindim['EVOUCHER'];
                    $gwpar['string_udf3'] = '-';
                    $gwpar['vch_meta'] = $gwremark;
                    $this->save_gwlot($gwpar);

                    $validated["OK"]++;
                    $validated["OKAMT"] += floatval($v3['voucher_value']);
                    $validated["VCHLOG"][] = array(
                        "voucher" => $v3['voucher_no'],
                        "amount" => $v3['voucher_value'],
                        "remark" => $gwremark
                    );
                }
            }

        }
        else {
            $validated["FAIL"]++;
            $validated["VCHLOG"][] = array(
                "voucher" => $qacct['qrcode'],
                "remark" => (string) $refresh->error->ErrorList->error_message
            );
        }

        $rtn = array(
            "code" => ($validated["OK"]>0) ? 1 : 0,
            "result" => ($validated["FAIL"]>0) ? "FAIL" : "SUCCESS",
            "mode" => "VALIDATE",
            "trans_id" => $trans_id,
            "valid_amount" => $validated["OKAMT"],
            "voucher_log" => $validated["VCHLOG"]
        );
        return $rtn;
    }

    public function pay($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str) {
        $vouchers = $this->_getVouchers($vouchers_str);
        $trans_amt = floatval(trim(str_replace([',', '$'], '', $trans_amt)));
        if (count($vouchers)<1) {
            // Get vouchers from validated
            $sql = "select trans_id,lot_id,lot_id2,unit_price from b2b_gateway_lot where coy_id='CTL' and gateway_id='".$this->gatewayid."' and trans_id='$trans_id' and unit_price>0 and string_udf3=''; ";
            $voucher_db = $this->db->query($sql)->result_array();
            if ($voucher_db) {
                foreach ($voucher_db as $vch_db) {
                    if (!in_array(trim($vch_db['lot_id']), $vouchers))
                        $vouchers[] = trim($vch_db['lot_id']);
                }
                $validates['code'] = 1;
            }
            else {
                $validates['code'] = 0;
            }
        }
        else {
            // Validates the submitted vouchers
            $validates = $this->validate($trans_id, $loc_id, $pos_id, $trans_amt, implode(';', $vouchers));
            $vouchers = array();
            foreach ($validates['voucher_log'] as $vlog) {
                $vouchers[] = $vlog['voucher'];
            }
        }
        if ($validates['code'] == 1) {
            // Burn them
            $burns = $this->pay_burn($trans_id, $loc_id, $pos_id, $trans_amt, implode(';', $vouchers));
            return $burns;
        } else {
            $validates['code'] = 0;
            return $validates;
        }
    }

    public function pay_burn($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str) {

        $token = $this->auth_token($loc_id,$pos_id);
        if (!isset($token) || $token == '') {
            return array(
                "code" => -1,
                "result" => "API FAILED"
            );
        }
        $batch_id = 0; //$this->get_gwlot($trans_id);
        $vouchers = $this->_getVouchers($vouchers_str);
        $trans_amt = floatval(trim(str_replace([',', '$'], '', $trans_amt)));

        $validated = array("OKAMT"=>0,"VCHLOG"=>'');
        $logging = array("trans_id"=>$trans_id, "loc_id"=>$loc_id, "pos_id"=>$pos_id);
        $trans_pastvch = $this->get_gwlot($trans_id);

        // Get the validated vouchers meta
        $qshopper='';$qwalvch=array();$qphyvch=array();
        $sql = "select rtrim(trans_id) trans_id,rtrim(lot_id) lot_id,rtrim(lot_id2) lot_id2,unit_price,string_udf1,gateway_meta
                from b2b_gateway_lot where coy_id='CTL' and gateway_id='".$this->gatewayid."' and trans_id='$trans_id' and unit_price>0 and string_udf3=''
                and lot_id in ('". implode("','",$vouchers) ."') ; ";
        $voucher_db = $this->db->query($sql)->result_array();
        foreach($voucher_db as $vchdb) {
            $vchdbmeta = json_decode($vchdb['gateway_meta'],true);
            if ($vchdb['string_udf1']==$this->vchfindim['PHYSICAL']) {
                $qphyvch[] = array('voucher_qr' => $vchdbmeta['voucher']['voucher_qr']);
            }
            if ($vchdb['string_udf1']==$this->vchfindim['EVOUCHER']) {
                $qshopper = $vchdbmeta['voucher']['shopper_id'];
                $qwalvch[] = array('voucher_record_id' => $vchdbmeta['voucher']['voucher_record_id']);
            }
        }

        // Burn
        $qacct['Token'] = $token['token'];
        $qacct['shopper_id'] = $qshopper;
        $qacct['machine_id'] = $token['machine_id'];
        $qacct['mall_code'] = $token['mall_code'];
        $qacct['wallet_vouchers'] = ($qwalvch) ? $qwalvch : '';
        $qacct['vouchers'] = ($qphyvch) ? json_encode($qphyvch) : '';
        $qacct['payment_value'] = 0;
        $refresh = $this->_doCurl('POSManager.asmx/BurnMultiVouchers','',$qacct,$logging);
        print_r($qacct);print_r($refresh);exit;


//        if ($refresh->data->Voucher) {
//
//            $validatedamt = 0;
//            foreach ($burn->vouchers as $voucher) {
//                $gwpar = array();
//                $gwpar['duplicates'] = in_array($voucher->voucherNumber, $trans_pastvch);
//                $gwpar['loc_id'] = $loc_id;
//                $gwpar['trans_id'] = $trans_id;
//                $gwpar['lot_id'] = $voucher->voucherNumber;
//                $gwpar['string_udf3'] = 'UTILIZED';
//                $gwpar['datetime_udf'] = date('Y-m-d H:i:s');
//                if (!$gwpar['duplicates']){
//                    $gwpar['unit_price'] = $voucher->voucherValue;
//                    $gwpar['string_udf1'] = $this->_readVchDim($voucher->voucherType); // ($voucher->voucherType=='eVoucher' || $voucher->voucherType=='eCapitaVoucher') ? 'E-CAPITALAND' : 'CAPITALAND';
//                }
//                $this->save_gwlot($gwpar);
//
//                $gwmark['lot_id'] = $voucher->voucherNumber;
//                $gwmark['lot_id2'] = $trans_id;
//                $gwmark['string_udf3'] = 'USED-OTHERRTXN';
//                $gwmark['trans_id'] = $trans_id;
//                $this->mark_gwlot($gwmark);
//
//                $validatedamt += floatval($voucher->voucherValue);
//                $validatedvch[] = array(
//                    "voucher" => $voucher->voucherNumber,
//                    "amount" => $voucher->voucherValue,
//                    "remark" => $voucher->voucherName
//                );
//
//            }
//
//            $validated = array(
//                "OKAMT"=> $validatedamt,
//                "VCHLOG"=> $validatedvch
//            );
//
//        }
//        else {
//            foreach ($vouchers as $voucher) {
//                $gwpar = array();
//                $gwpar['duplicates'] = in_array($voucher, $trans_pastvch);
//                $gwpar['loc_id'] = $loc_id;
//                $gwpar['trans_id'] = $trans_id;
//                $gwpar['lot_id'] = $voucher;
//                $gwpar['string_udf2'] = $burn->message . ' ' . $burn->errorCode;
//                $gwpar['string_udf3'] = 'ERROR-UTILIZE';
//                $this->save_gwlot($gwpar);
//            }
//
//            $validated = array(
//                "OKAMT"=> 0,
//                "VCHLOG"=> $burn->message
//            );
//        }

        if (count($vouchers)>0 && $validated["OKAMT"]>0){
            // Voucher burnt
            $rtn = array(
                "code" => 1,
                "result" => "SUCCESS",
                "mode" => "PAY",
                "trans_id" => $trans_id,
                "payment_id" => $this->gatewaycode,
                "payment_amount" => $validated["OKAMT"]
            );
        }
        else {
            // Invalid
            $rtn = array(
                "code" => -1,
                "result" => "FAILED",
                "mode" => "PAY",
                "trans_id" => $trans_id,
                "error" => $validated["VCHLOG"]
            );
        }

        return $rtn;
    }

    public function get_gwlot($trans_id) {
        $sql = "SELECT rtrim(lot_id) lot_id,unit_price,string_udf1,string_udf2,string_udf3 from b2b_gateway_lot WHERE trans_id='$trans_id';";
        $rtnx = $this->db->query($sql)->result_array();
        $vouchers = array();
        foreach ($rtnx as $r) {
            $vouchers[$r['lot_id']] = $r;
        }
        return $vouchers;
    }

    public function save_gwlot($gwpar){

        $gateway_meta = $this->gatewayuser;
        $gateway_meta['voucher'] = $gwpar['vch_meta'];
        $gateway_meta['last_gateway_code'] = $this->gatewaycode;

        $gwpar['lot_id2'] = (isset($gwpar['lot_id2'])) ? $gwpar['lot_id2'] : '';
        $gwpar['unit_price'] = (isset($gwpar['unit_price'])) ? $gwpar['unit_price'] : 0;
        $gwpar['string_udf1'] = (isset($gwpar['string_udf1'])) ? $gwpar['string_udf1'] : '';
        $gwpar['string_udf2'] = (isset($gwpar['string_udf2'])) ? $gwpar['string_udf2'] : '';
        $gwpar['string_udf3'] = (isset($gwpar['string_udf3'])) ? $gwpar['string_udf3'] : '';
        $gwpar['datetime_udf'] = (isset($gwpar['datetime_udf'])) ? $gwpar['datetime_udf'] : '';
        if ($gwpar['duplicates']){
            // Update
            $sql = "UPDATE b2b_gateway_lot SET ";
            $sql.= ($gwpar['lot_id2']!='') ? " lot_id2='".$gwpar['lot_id2']."', " : "" ;
            $sql.= ($gwpar['unit_price']>0) ? " unit_price='".$gwpar['unit_price']."', " : "" ;
            $sql.= ($gwpar['string_udf1']!='') ? " string_udf1='".$gwpar['string_udf1']."', " : "" ;
            $sql.= ($gwpar['string_udf2']!='') ? " string_udf2='".$gwpar['string_udf2']."', " : "" ;
            $sql.= ($gwpar['string_udf3']=='-') ? " string_udf3='', " : ( ($gwpar['string_udf3']!='') ? " string_udf3='".$gwpar['string_udf3']."', " : "" ) ;
            $sql.= ($gwpar['datetime_udf']!='') ? " datetime_udf='".$gwpar['datetime_udf']."', " : "" ;
            $sql.= " gateway_meta='". json_encode($gateway_meta) ."', gateway_num=gateway_num+1, modified_by='".getRealIpAddr(15)."',modified_on=GETDATE() ";
            $sql.= " WHERE coy_id='CTL' and gateway_id='".$this->gatewayid."' and trans_id='".$gwpar['trans_id']."' and lot_id='".$gwpar['lot_id']."' ";
            $rtnx = $this->db->query($sql);
        }
        else {
            // Insert
            $gwpar['string_udf3'] = ($gwpar['string_udf3']=='-') ? '' : $gwpar['string_udf3'] ;
            $sql = "INSERT INTO b2b_gateway_lot (coy_id,gateway_id,trans_id,loc_id,lot_id,lot_id2,unit_price,string_udf1,string_udf2,string_udf3,datetime_udf,gateway_meta,gateway_num,created_by,created_on,modified_by,modified_on)
                            VALUES ('CTL','".$this->gatewayid."','".$gwpar['trans_id']."', '".$gwpar['loc_id']."', '".$gwpar['lot_id']."','".$gwpar['lot_id2']."' , '".$gwpar['unit_price']."' , 
                            '".$gwpar['string_udf1']."','". $gwpar['string_udf2'] ."',LEFT('".$gwpar['string_udf3']."',255),'".$gwpar['datetime_udf']."' ,
                            '". json_encode($gateway_meta) ."' ,1,'".getRealIpAddr(15)."',GETDATE(),'".getRealIpAddr(15)."',GETDATE())";
            $rtnx = $this->db->query($sql);
        }
    }

    private function _getVouchers($str=''){

        if ($str!='' && strlen($str)>3) {
            $vouchers_str = urldecode($str);
            $vouchers = explode(';',$vouchers_str);
        }
        else {
            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody);
            $vouchers = $postarr->voucher;
        }

        $uppVch = array();
        if (count($vouchers)>0) {
            $vouchers = array_filter($vouchers);
            foreach ($vouchers as $vch) {
                $uppVch[] = ($vch);
            }
        }
        return $uppVch;
    }

    public function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {

        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = '';
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }

    private function _doCurl($apiuri,$token='', $query,$logging) {

        libxml_use_internal_errors(true);
        $this->gatewaycode = md5($logging['trans_id'].time().rand(1000,9999).rand(1000,9999).rand(1000,9999));

        $created_on = time();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        $headers[] = "Accept: application/json";
//        if ($token!='') {
//            $headers[] = "Token: " . $token;
//        }
        $queryjson = urldecode(http_build_query($query, null, '&', PHP_QUERY_RFC1738));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiurl.$apiuri);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $queryjson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);

        $xml    = str_replace(array("diffgr:","msdata:"),'', $output);
        $data   = simplexml_load_string($xml);
        if (!$data){

            $gwpar['created_on'] = date('Y-m-d H:i:s',$created_on);
            $gwpar['modified_on'] = date('Y-m-d H:i:s');
            $gwpar['trans_id'] = $logging['trans_id'];
            $gwpar['loc_id'] = $logging['loc_id'];
            $gwpar['pos_id'] = $logging['pos_id'];
            $gwpar['request_link'] = $this->apiurl.$apiuri;
            $gwpar['request_msg'] = $queryjson;
            $gwpar['status_level'] = -1;
            $gwpar['return_status'] = '';
            $gwpar['return_msg'] = $output;
            $this->save_gwmsg($gwpar);

            libxml_clear_errors();
            if (ENVIRONMENT=="production") {
                $msg = 'LANDLEASE API - Fail for '.$this->gatewaycode.'<br><br>This API call return something that is not recognizable. Please check.';
                $this->cherps_email('LANDLEASE API XML Fail - ' . $loc_id, $msg, $this->notification);
                return NULL;
            }
        }

        if ($apiuri=='POSManager.asmx/GetToken') {
            $outputarr = (array) $data->diffgram->DocumentElement->ReturnTable;
            unset($outputarr['@attributes']);
        }
        else {
            $outputarr = $data;
        }

        $gwpar['created_on'] = date('Y-m-d H:i:s',$created_on);
        $gwpar['modified_on'] = date('Y-m-d H:i:s');
        $gwpar['trans_id'] = $logging['trans_id'];
        $gwpar['loc_id'] = $logging['loc_id'];
        $gwpar['pos_id'] = $logging['pos_id'];
        $gwpar['request_link'] = $this->apiurl.$apiuri;
        $gwpar['request_msg'] = $queryjson;
        $gwpar['status_level'] = 0;
        $gwpar['return_status'] = $outputarr->Status;
        $gwpar['return_msg'] = $output;
        $this->save_gwmsg($gwpar);

        return $outputarr;
    }

    public function save_gwmsg($gwpar){
        $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on,modified_by,modified_on)
                        VALUES ('CTL','LANDLEASE','".$this->gatewaycode."','".$gwpar['trans_id']."','".$gwpar['loc_id']."','".$gwpar['pos_id']."' ,
                        '".$gwpar['request_link']."','".$gwpar['request_msg']."','". $gwpar['status_level'] ."','".$gwpar['return_status']."','".$gwpar['return_msg']."' ,
                        '".getRealIpAddr(15)."','".$gwpar['created_on']."','".getRealIpAddr(15)."','".$gwpar['modified_on']."')";
        $rtnx = $this->db->query($sql);
    }

}

?>
