<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

!defined('BASEPATH') OR ('No direct script access allowed');

class Razer_model extends Base_Common_Model {

    private $apiUrl;
    private $appCode;
    private $secretPin;

    // http://chvoices-test.challenger.sg/api/appledep/XXXX

    /***
     * (1) Customer DEP number must be input in "Tax-Reg Code" of SMS customer master (Format: DEP-XXXXXX)
     * (2) Invoice must be from IC/IB (CR/BR for refund).
     * (3) Item must be from acceptable categories, lot_id must be scanned in.
     * (4) Daily job will take valid, unprocessed, past 7 days invoices to submit.
     */

    public function __construct()
    {
        parent::__construct();

        if (ENVIRONMENT=="production") {
            $this->apiUrl = 'https://api.mol.com';
            $this->appCode = '202011120470';
            $this->secretPin = 'sems7363';
        }
        else {
            $this->apiUrl = 'https://sandbox-api.mol.com';
            $this->appCode = '202011030428';
            $this->secretPin = 'idql3630';
        }
    }

    public function pin_store($id = '')
    {
        ini_set('max_execution_time', '300');

        //1- Get orders with DEP to process
        if ($id != '') {
            $pro_res = $this->process_perinvoice($id);
            if ($pro_res['status_code'] !== 200) {
                return json_encode($pro_res);
            }
            $r = array("status_code"=>200,"info"=>"Success process " . $id);
        }
        else {
            $invoices = $this->querySQL();

            $r_inv = array();
            foreach ($invoices as $id){
                $pro_res = $this->process_perinvoice($id['invoice_id']);
                if ($pro_res['status_code'] !== 200) {
                    // TODO: send email
//                    $this->_doErrormail($invoice, $rtn);
                    return json_encode($pro_res);
                }
                array_push($r_inv, $id['invoice_id']);
            }

            $r = array("status_code"=>200,"info"=>"Success process " . implode(", ",$r_inv));
        }
        return json_encode($r);
    }

    public function process_perinvoice($id){

        $invoice = $this->querySQL($id);

        if (!empty($invoice)) {
            foreach ($invoice as $i) {
                // Initiation
                $init_url = 'pinstore/purchaseinitiation';
                $invoice_id = $i['invoice_id'];
                $prod_code = $i['long_desc'];
                $item_id = $i['item_id'];
                $line_num = $i['line_num'];
                $quantity = $i['qty_invoiced'];
                $ref_id = $this->_generate_ref_id();
                $version = "v1";
                $signature = md5($this->appCode . $prod_code . $quantity . $ref_id . $version . $this->secretPin);
                $init_data = [
                    "applicationCode" => $this->appCode,
                    "version" => $version,
                    "referenceId" => $ref_id,
                    "productCode" => $prod_code,
                    "quantity" => $quantity,
                    "merchantProductCode" => $item_id,
                    "signature" => $signature
                ];
                $init_url_param = http_build_query($init_data);
                $init_res = $this->_acc_credentials('RAZERPININIT', $init_url, $init_url_param, $invoice_id);
                if ($init_res['status_code'] !== 200) {
                    return $init_res;
                }

                // Confirm
                $validatedToken = $init_res['data']->validatedToken;
                $confirm_signature = md5($this->appCode . $ref_id . $version . $validatedToken . $this->secretPin);
                $confirm_url = 'pinstore/purchaseconfirmation';
                $confirm_data = [
                    "applicationCode" => $this->appCode,
                    "version" => "v1",
                    "referenceId" => $ref_id,
                    "validatedToken" => $validatedToken,
                    "signature" => $confirm_signature
                ];
                $confirm_url_param = http_build_query($confirm_data);
                $confirm_res = $this->_acc_credentials('RAZERPINCONFIRM',$confirm_url, $confirm_url_param, $invoice_id);
                if ($confirm_res['status_code'] !== 200) {
                    return array(
                        "status_code" => 400,
                        "message" => "Error to process ".$invoice_id .", line num " . (string) $line_num
                    );
                }
                if (!empty($confirm_res['data'])) {
                    // insert sms_invoice_lot
                    $this->db->query("delete from sms_invoice_lot where invoice_id = '".$invoice_id."' and coy_id = 'CTL' and line_num = " . $line_num);
                    foreach ($confirm_res['data']->coupons as $k=>$v) {
                        $lot_sql = "INSERT INTO sms_invoice_lot VALUES ('CTL', '".$invoice_id."',". $line_num.",". ($k+1).", '".$v->serials[0]."', '".$v->pins[0]."',
                                    1, '$invoice_id', '', 'SUCCESS', '$v->expiryDate', now(), now(), 'RAZER', now(), 'RAZER', now())";
                        $this->db->query($lot_sql);
                        $email_url = "http://vc8.api.valueclub.asia/api/emarsys/RAZER_GOLD";
                        $email_data = array(
                            "source" => "hachi",
                            "trans_id" => $id,
                            "email" => $i['email_addr'],
                            "data" => [
                                "r_card_value" => number_format($i['unit_price'],0,'.',','),
                                "r_claim_code" => $v->pins[0]
                            ]
                        );
                        $this->doCurl($email_url, $email_data);
                    }
                } else {
                    return array(
                        "status_code" => 400,
                        "message" => "Error to process ".$invoice_id
                    );
                }
            }
            return array(
                "status_code" => 200,
                "message" => "Success"
            );
        }
        else {
            return array(
                "status_code" => 400,
                "message" => 'No invoice.'
            );
        }

    }

    public function querySQL($id=''){
        $query = "FROM
                sms_invoice_item,
                sms_invoice_list,
                ims_item_list
                WHERE
                ( sms_invoice_item.coy_id = sms_invoice_list.coy_id ) 
                AND ( sms_invoice_item.invoice_id = sms_invoice_list.invoice_id ) 
                AND ( sms_invoice_item.item_id = ims_item_list.item_id )
                AND ( sms_invoice_item.status_level >= 1 ) 
                AND ( ims_item_list.inv_dim4 = 'RAZER GOLD' )
                AND NOT EXISTS (select 1 FROM sms_invoice_lot WHERE sms_invoice_lot.invoice_id = sms_invoice_list.invoice_id AND sms_invoice_lot.line_num = sms_invoice_item.line_num AND rtrim(sms_invoice_lot.string_udf3) = 'SUCCESS')
                ";

        if ($id=='') {
            $sql = "select DISTINCT rtrim(sms_invoice_list.invoice_id) invoice_id 
                  $query and (sms_invoice_list.invoice_date >= now() + interval '-7 day') 
                  order by invoice_id ";
        }
        else {
            $sql = "SELECT
                sms_invoice_item.coy_id,
                rtrim(sms_invoice_item.invoice_id) AS invoice_id,
                sms_invoice_item.line_num,
                rtrim(sms_invoice_item.item_id) item_id,
                cast(sms_invoice_item.qty_invoiced as int) qty_invoiced,
                rtrim(ims_item_list.long_desc) long_desc,
                sms_invoice_item.status_level,
                sms_invoice_list.invoice_date AS trans_date,
                rtrim(sms_invoice_list.cust_id) cust_id,
                rtrim(sms_invoice_list.cust_name) cust_name,
                sms_invoice_list.tel_code,
                sms_invoice_list.email_addr,
                rtrim(sms_invoice_list.first_name) first_name,
                rtrim(sms_invoice_list.last_name) last_name,
                sms_invoice_item.unit_price 
                $query and sms_invoice_list.invoice_id='$id' 
                order by sms_invoice_list.invoice_date,sms_invoice_list.invoice_id,sms_invoice_item.line_num; ";
        }

        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    private function updTxnSuccess($sys_id,$trans_id,$line_num,$tx,$dep_status) {
        if ($sys_id=='SMS') {
            $sql = "UPDATE sms_invoice_lot SET string_udf2='$tx',datetime_udf2=now(),string_udf3='$dep_status' WHERE coy_id='CTL' and invoice_id='$trans_id' and line_num=$line_num ";
            $query = $this->db->query($sql);
        }
        if ($sys_id=='POS') {
            $sql = "UPDATE pos_transaction_lot SET string_udf2='$tx',datetime_udf2=now(),string_udf3='$dep_status' WHERE coy_id='CTL' and trans_id='$trans_id' and line_num=$line_num ";
            $query = $this->db->query($sql);
        }
    }

    public function _acc_credentials($type, $url, $param, $invoice_id){

        $post_url = $this->apiUrl . '/' . $url;

        $headers[] = "Content-Type:application/x-www-form-urlencoded";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpcode === 200) {
            $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('CTL','".$type."','".md5($type.time())."','".$invoice_id."','','' ,
                        '".$post_url."','".$param."',".$httpcode.",'','". $data ."','".getRealIpAddr(15)."',now())";
            $this->db->query($sql);
            return array(
                "status_code" => 200,
                "data" => json_decode($data)
            );
        } else {
            $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('CTL','".$type."','".md5($type.time())."','".$invoice_id."','','' ,
                        '".$post_url."','".$param."',".$httpcode.",'','".$data."','".getRealIpAddr(15)."',now())";
            $this->db->query($sql);
            return array(
                "status_code" => 400,
                "message" => $invoice_id . ' can not process ' . $type . '. Error Message: ' .json_decode($data)->message
            );
        }
    }

    private function _doSuccessmail($invoice,$rtn) {

        if (ENVIRONMENT != 'production') {
            // If not production, do not trigger email
            //return ;
        }

        $msg_item = '';
        foreach ($invoice as $i)
            $msg_item .= "<br>".trim($i['item_id'])." / ".trim($i['lot_id']);

        $subj = 'Apple Device Enrollment - ' . $invoice[0]['invoice_id'];
        $msg = "Trans ID: ".trim($invoice[0]['sys_id'])." ".trim($invoice[0]['invoice_id'])."<br>Item/SN: ".$msg_item;
        $msg.= "<br><br>".$rtn["message"];
        $email = 'yongsheng@challenger.sg;'; //'MIS.Team@challenger.sg;';
        $this->cherps_email($subj,$msg,$email);
    }

    private function _doErrormail($invoice,$rtn) {

        if (ENVIRONMENT != 'production') {
            // If not production, do not trigger email
            return ;
        }

        $msg_item = '';
        foreach ($invoice as $i)
            $msg_item .= "<br>".trim($i['item_id'])." / ".trim($i['lot_id']);

        $subj = 'Apple Device Enrollment ERROR - ' . $invoice[0]['invoice_id'];
        $msg = "Trans ID: ".trim($invoice[0]['sys_id'])." ".trim($invoice[0]['invoice_id'])."<br>Item/SN: ".$msg_item;
        $msg.= "<br><br>".$rtn["message"];
        $email = $this->receipients;
        $this->cherps_email($subj,$msg,$email);
    }

    private function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {

        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = 'li.liangze@challenger.sg; yongsheng@challenger.sg; ';
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }

    public function _generate_ref_id()
    {
        $date = date('y');
        $trans_prefix = 'RAZER' . $date;
        $is_exists = $this->db->query("select 1 from sys_trans_list where coy_id = '*' and sys_id = 'ADS' and trans_prefix = '" . $trans_prefix . "'")->num_rows();
        if ($is_exists === 0) {
            $this->db->query("insert into sys_trans_list VALUES ('*', 'ADS', '". $trans_prefix ."', 1, now())");
        }
        $next_num = $this->db->query("select next_num from sys_trans_list where coy_id = '*' and sys_id = 'ADS' and trans_prefix = '" . $trans_prefix . "'")->result_array();
        $invoice_id = 'RA' . $date . sprintf("%06s", $next_num[0]['next_num']);
        $this->db->query("update sys_trans_list set next_num = " . $next_num[0]['next_num'] . "+1 where coy_id = '*' and sys_id = 'ADS' and trans_prefix = '" . $trans_prefix . "'");
        return $invoice_id;
    }

    private function doCurl($url, $data){
        $headers[] = "Content-Type: application/json";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
    }

}

?>