<?php
!defined('BASEPATH') OR ('No direct script access allowed');

class Applecare_model extends Base_Common_Model
{

    // http://chvoices-test.challenger.sg/api/applecare/

    /**
     * Applecare_model constructor.
     * Documentation Website: https://applecareconnect.apple.com/api-docs/accuat/html/WSReference.html?user=reseller
     *
     * Dev/Test environment:
     * https://acc-ipt.apple.com/order-service/1.0/verify-order/
     * Joint UAT environment:
     * https://api-applecareconnect-ept.apple.com/order-service/1.0/verify-order/ (even soldToId)
     * https://api-applecareconnect-ept2.apple.com/order-service/1.0/verify-order/ (odd soldToId)
     */
    private $shiptoAcct;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('cherps/Pogrn_model');
        $this->token = 'Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjE';

        $this->shiptoAcct = '0000793749';
    }

    public function correction(){
        $sql = "select * from zys_applecare";
        $query = $this->db->query($sql)->result_array();

        foreach ($query as $qq){

            if ($qq['po_id']==null){
                print_r($qq);
            }
        }
        exit;
        return $query;
    }


    /**
     * @api {post} /api/applecare/:transID?code=Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjE Apple Care Verify
     * @apiVersion 1.0.0
     * @apiGroup Applecare
     *
     * @apiSuccessExample PO Confirmed Success
     * HTTP/1.1 200 OK
     *  {
     *      "status": "PROCESSED",
     *      "status_code": 200,
     *      "message": {},
     *      "documents": {}
     *  }
     *
     * @apiErrorExample No orders found
     *  {
     *      "PO_RETAIL": {
     *          "status": "IDLE",
     *          "status_code": 400,
     *          "message": "No orders found"},
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "return": "0",
     *      "message": "Request code is error, please check your request code.",
     *      "po_id": "IPVC190066393"
     *  }
     */
    public function process($id = ''){
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return array('return'=>'0','message'=>'Request code is error, please check your request code.');
        } else {
            ini_set('max_execution_time', '300');

            $create_order1 = $this->create_order($id,'PO_RETAIL');
            $create_order2 = $this->create_order($id,'PO_HACHI');
            $create_order3 = $this->create_order($id,'PO_CORPS');
            $create_order4 = $this->cancel_order($id,'RN_RETAIL');
            $create_order5 = $this->cancel_order($id,'RN_HACHI');
            $create_order6 = $this->cancel_order($id,'RN_CORPS');
            return array(
                "PO_RETAIL" => $create_order1,
                "PO_HACHI" => $create_order2,
                "PO_CORPS" => $create_order3,
                "RN_RETAIL" => $create_order4,
                "RN_HACHI" => $create_order5,
                "RN_CORPS" => $create_order6
            );
        }
    }

    /**
     * Usage Defines the AppleCare Settings for the API calls
     * @param $type Options: create_order, cancel_order or verify_order
     * @param $vars Request date
     * @param int $second
     * @return Reposible Data
     */
    public function _acc_credentials($type, $vars, $logging, $second=60){

        if (ENVIRONMENT == 'production') {
            $base_url = 'https://api-applecareconnect2.apple.com/order-service/1.0';
        } else {
            // $base_url = 'https://api-applecareconnect-ept2.apple.com/order-service/1.0';
            $base_url = 'https://acc-ipt.apple.com/order-service/1.0';
        }

        $post_url = $base_url . '/' . $type;
        $headers[] = "Content-Type:application/json";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_STDERR, fopen(getcwd().'/assets/errorlog.txt', 'w'));
        //curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_SSLVERSION,6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'/www/arae19feb/GRX-0000793749.pem');
//        curl_setopt($ch, CURLOPT_SSLKEY, getcwd().'/www/arae19feb/acprivatenewkey.pem');

        curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'/www/arae21/GRX-0000793749.ACC1914.Prod.AppleCare.chain.pem');
        curl_setopt($ch, CURLOPT_SSLKEY, getcwd().'/www/arae21/acprivatenewkey.pem');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($vars));

        $data = curl_exec($ch);
        curl_close($ch);

        if ($this->input->get('debug')==1) {
            echo file_get_contents(getcwd().'/assets/errorlog.txt');
            echo '

';
            echo $data;
            echo '

';
            print_r($this->_response_handler($data));
        }

        if ($data) {

            $res = $this->_response_handler($data);
            if (!is_array($res['error']) && $res['error']=='') {
                $rr= array(
                    "status" => $type . ' successful',
                    "status_code" => 200,
                    "url" => $post_url,
                    "message" => $res['json_response']
                );
            } else {
                $rr= array(
                    "status" => $type . ' failed',
                    "status_code" => 400,
                    "url" => $post_url,
                    "message" => $res['error']
                );
            }

            $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on)
                        VALUES ('CTL','APPLECARE','".md5('APPLECARE'.time())."','".trim($logging['trans_id'])."','','' ,
                        '".$post_url."','".json_encode($vars)."','". $rr['status_code'] ."','". $rr['status'] ."','". $data ."','".getRealIpAddr(15)."',now())";
            $this->db->query($sql);
            return $rr;

        } else {
            return false;
        }
    }


    /**
     * @param $json_array String variable to be validated if it is JSON
     * @return bool True if json_array is valid, False if not
     */
    public function _is_json($json_array)
    {
        json_decode($json_array);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @param $full_response JSON response from the DEP API
     * @return mixed Full API response and any API errors and their messages
     */
    public function _response_handler($full_response)
    {
        $valid_return = $this->_is_json($full_response);

        if ($valid_return) {
            $json_response = json_decode($full_response, true);

            // Verify/Create/Cancel Order Error
            if (isset($json_response['orderErrorResponse'])) {
                $api_errors = $json_response['orderErrorResponse'];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Device Enrollment Error
            elseif (isset($json_response["orderDetailsResponses"]["deviceEligibility"]["deviceErrorResponse"])) {
                $api_errors = $json_response["orderDetailsResponses"]["deviceEligibility"]["deviceErrorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // POC Content Error
            elseif (isset($json_response["pocErrorResponse"])) {
                $api_errors = $json_response["pocErrorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Device Configuration Error
            elseif (isset($json_response["deviceConfigErrorResponse"])) {
                $api_errors = $json_response["deviceConfigErrorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Failed Auth Error
            elseif (isset($json_response["failedAuthErrorResponse"])) {
                $api_errors = $json_response["failedAuthErrorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Consolidated POC Error
            elseif (isset($json_response["errorResponse"])) {
                $api_errors = $json_response["errorResponse"];
                foreach ($api_errors as $key => $value) {
                    if ($key == 'errorCode') {
                        $error['error_code'] = $value;
                    } else {
                        $error['error_message'] = $value;
                    }
                }
            } // Ship-to Error
            elseif (isset($json_response['error_code'])) {
                $api_errors = $json_response["error_code"];
                $error['error_code'] = $api_errors['errorCode'];
                $error['error_message'] = $api_errors['errorMessage'];
            } // Other Errors
            elseif (isset($json_response['errorCode'])) {
                $error['error_code'] = $json_response['errorCode'];
                $error['error_message'] = $json_response['errorMessage'];
            } // No Error
            else {
                return array("json_response"=>$json_response, "error"=> '');
            }
        } else {
            $error['error_code'] = 'ACC_ERR_0001';
            $error['error_message'] = 'JSON is invalid - Inspect full response for errors';
        }
        return array("json_response"=>$json_response, "error"=> $error);
    }

    /**
     * @param $id Transaction Id
     * @return mixed Query Sql return data
     */
    public function querySQL($id, $type='PO')
    {
        //$result = $this->db->query("SET ANSI_NULLS ON;");
        //$result = $this->db->query("SET ANSI_WARNINGS ON;");

        if ($type=='PO_RETAIL' || $type=='RN_RETAIL') {
            $sql_where = ($id=='') ? " AND pos_transaction_list.trans_date < now() + interval '-5 minute' " : " AND pos_transaction_list.trans_id='$id' ";
            $sql_qty_level = ($type=='RN_RETAIL') ? " AND ( pos_transaction_item.trans_qty < 0 ) " : " AND ( pos_transaction_item.trans_qty > 0 ) ";
            $sql = "SELECT 
                    'POS' sys_id,
                    pos_transaction_item.coy_id,
                    rtrim(pos_transaction_item.trans_id) trans_id,
                    pos_transaction_item.line_num,
                    pos_transaction_item.item_id,
                    pos_transaction_item.trans_qty,
                    pos_transaction_item.status_level,
                    pos_transaction_list.trans_date,
                    rtrim(pos_transaction_lot.lot_id) lot_id,
                    pos_transaction_list.loc_id,
                    pos_transaction_item.promo_id,
                    pos_transaction_list.cust_id,
                    pos_transaction_list.cust_name,
                    pos_transaction_list.tel_code,
                    pos_transaction_list.email_addr,
                    pos_transaction_list.first_name,
                    pos_transaction_list.last_name,
                    rtrim(pos_transaction_lot.lot_id2) lot_id2,
                    (select unit_price from ims_item_price where item_id=pos_transaction_item.item_id and coy_id=pos_transaction_item.coy_id and price_type='SUPPLIER' and eff_from<=now() and eff_to>=now() order by line_num desc limit 1) unit_price 
                FROM
                    pos_transaction_item,
                    pos_transaction_list,
                    ims_item_list,
                    pos_transaction_lot 
                WHERE
                    ( pos_transaction_item.coy_id = pos_transaction_list.coy_id ) 
                    AND ( pos_transaction_item.trans_id = pos_transaction_list.trans_id ) 
                    AND ( pos_transaction_item.item_id = ims_item_list.item_id ) 
                    AND ( pos_transaction_item.coy_id = pos_transaction_lot.coy_id ) 
                    AND ( pos_transaction_item.trans_id = pos_transaction_lot.trans_id ) 
                    AND ( pos_transaction_item.line_num = pos_transaction_lot.line_num ) 
                    AND (
                        ( pos_transaction_list.status_level > 0 ) 
                        AND ( pos_transaction_item.status_level > 0 ) 
                        AND ( pos_transaction_item.status_level < 3 ) 
                        AND ( ims_item_list.item_type = 'A' ) 
                        AND ( pos_transaction_lot.status_level > 0 )  
                        $sql_where $sql_qty_level
                    ) 
                ORDER BY trans_date ";
        }
        if ($type=='PO_HACHI' || $type=='PO_CORPS' || $type=='RN_HACHI' || $type=='RN_CORPS') {
            $sql_type = ($type=='PO_HACHI' || $type=='RN_HACHI') ? " sms_invoice_list.inv_type = 'IO' OR sms_invoice_list.inv_type = 'HI' " : " sms_invoice_list.loc_id = 'CS' OR sms_invoice_list.loc_id = 'UL' OR sms_invoice_list.inv_type = 'IB' ";
            $sql_where = ($id == '') ? " " : " AND sms_invoice_list.invoice_id='$id' ";
            $sql_qty_level = ($type=='RN_HACHI' || $type=='RN_CORPS') ? " AND ( sms_invoice_lot.trans_qty < 0 ) " : " AND ( sms_invoice_lot.trans_qty > 0 ) ";
            $sql = " 
                SELECT
                    'SMS' sys_id,
                    sms_invoice_item.coy_id,
                    rtrim(sms_invoice_item.invoice_id) AS trans_id,
                    sms_invoice_item.line_num,
                    sms_invoice_item.item_id,
                    sms_invoice_lot.trans_qty,
                    sms_invoice_item.status_level,
                    sms_invoice_list.invoice_date AS trans_date,
                    rtrim(sms_invoice_lot.lot_id) lot_id,
                    sms_invoice_list.loc_id,
                    sms_invoice_item.promo_id,
                    sms_invoice_list.cust_id,
                    sms_invoice_list.cust_name,
                    sms_invoice_list.tel_code,
                    sms_invoice_list.email_addr,
                    sms_invoice_list.first_name,
                    sms_invoice_list.last_name,
                    rtrim(sms_invoice_lot.lot_id2) lot_id2,
                    sms_invoice_item.unit_price 
                FROM
                    sms_invoice_item,
                    sms_invoice_list,
                    ims_item_list,
                    sms_invoice_lot 
                WHERE
                    ( sms_invoice_item.coy_id = sms_invoice_list.coy_id ) 
                    AND ( sms_invoice_item.invoice_id = sms_invoice_list.invoice_id ) 
                    AND ( sms_invoice_item.item_id = ims_item_list.item_id ) 
                    AND ( sms_invoice_item.coy_id = sms_invoice_lot.coy_id ) 
                    AND ( sms_invoice_item.invoice_id = sms_invoice_lot.invoice_id ) 
                    AND ( sms_invoice_item.line_num = sms_invoice_lot.line_num ) 
                    AND ( sms_invoice_item.status_level = 3 ) 
                    AND ( RTRIM( sms_invoice_lot.string_udf1 ) = '' ) 
                    AND ( ims_item_list.item_type = 'A' ) 
                    AND ( $sql_type )
                    $sql_where $sql_qty_level
                ORDER BY trans_date";
        }
        $query = $this->db->query($sql)->result_array();
        return $query;
    }

    private function save_log($params){
        $ins["coy_id"] = 'CTL';
        $ins["ref_type"] = $params["ref_type"];
        $ins["ref_id"] = $params["ref_id"];
        $ins["ref_rev"] = ($params["ref_rev"]) ? $params["ref_rev"] : 0;
        $ins["ref_num"] = $params["ref_num"];
        $ins["trans_id"] = $params["trans_id"];
        $ins["line_num"] = $params["line_num"];
        $ins["loc_id"] = $params["loc_id"];
        $ins["item_id"] = $params["item_id"];
        $ins["status_level"] = $params["status_level"];
        $ins["serialnumber"] = ($params["serialnumber"]) ? $params["serialnumber"] : ' ';
        $ins["secondaryserialnumber"] = ($params["secondaryserialnumber"]) ? $params["secondaryserialnumber"] : ' ';
        $ins["url"] = $params["url"];
        $ins["ponumber"] = $params["ponumber"];

        $ins["usersessionid"] = ($params["usersessionid"]) ? $params["usersessionid"] : ' ';
        $ins["operationid"] = ($params["operationid"]) ? $params["operationid"] : ' ';
        $ins["agreementnumber"] = ($params["agreementnumber"]) ? $params["agreementnumber"] : ' ';
        $ins["tax"] = ($params["tax"]) ? $params["tax"] : ' ';
        $ins["totalfromorder"] = ($params["totalfromorder"]) ? $params["totalfromorder"] : ' ';
        $ins["partnumber"] = ($params["partnumber"]) ? $params["partnumber"] : ' ';
        $ins["parttype"] = ($params["parttype"]) ? $params["parttype"] : ' ';
        $ins["netprice"] = ($params["netprice"]) ? $params["netprice"] : ' ';
        $ins["currency"] = ($params["currency"]) ? $params["currency"] : ' ';
        $ins["availability"] = ($params["availability"]) ? $params["availability"] : ' ';
        $ins["quantity"] = ($params["quantity"]) ? $params["quantity"] : ' ';
        $ins["creditreseller"] = ($params["creditreseller"]) ? $params["creditreseller"] : ' ';
        $ins["totalcreditfororder"] = ($params["totalcreditfororder"]) ? $params["totalcreditfororder"] : ' ';
        $ins["coveragedurationstatement"] = ($params["coveragedurationstatement"]) ? $params["coveragedurationstatement"] : ' ';
        $ins["warningmessage"] = ($params["warningmessage"]) ? $params["warningmessage"] : ' ';
        $ins["created_by"] = $params["created_by"];
        $ins["created_on"] = date('Y-m-d H:i:s');
        $ins["modified_by"] = $params["created_by"];
        $ins["modified_on"] = date('Y-m-d H:i:s');
        $result1 = $this->db->insert('b2b_po_applecare', $ins);
    }

    private function updTxnSuccess($sys_id,$trans_id,$line_num,$po_id) {
        if ($sys_id=='SMS') {
            $sql = "UPDATE sms_invoice_lot SET string_udf1='$po_id',datetime_udf1=now() WHERE coy_id='CTL' and invoice_id='$trans_id' and line_num=$line_num ";
            $query = $this->db->query($sql);
        }
        if ($sys_id=='POS') {
            $sql = "UPDATE pos_transaction_item SET status_level=3,modified_by='chvoices',modified_on=now() WHERE coy_id='CTL' and trans_id='$trans_id' and line_num=$line_num ";
            $query = $this->db->query($sql);
            $sql = "UPDATE pos_transaction_lot SET string_udf1='$po_id',datetime_udf1=now() WHERE coy_id='CTL' and trans_id='$trans_id' and line_num=$line_num ";
            $query = $this->db->query($sql);
        }
    }

    private function _doErrormail($params) {

        if (ENVIRONMENT != 'production') {
            // If not production, do not trigger email
            return ;
        }

        $msgRemark1 = '';
        $msgRemark2 = '';

        if ( trim($params["warningmessage"]) == "The device ID you entered is not valid. Please enter a valid serial number, IMEI or MEID.") {
            $email = 'MIS.Team@challenger.sg;';
            if ($params['sys_id']=="SMS") {
                $email .= "HQ.Inventory@challenger.sg";
                $msgRemark1 = "Hi Logistics Team, Please provide SN for following order.<br><br>---<br><br>";
            }
            else {
                $email .= trim($params['loc_id']).'.ic@challenger.sg';
                $msgRemark1 = "Hi IC, Please provide SN for following order.<br><br>---<br><br>";
            }
        }
        else {
            $email = 'MIS.Team@challenger.sg;';
            $msgRemark1 = "<br><br>Please check and email to ac.connect.escalations.apac@apple.com";
        }

        $subj = 'AppleCare Registration Error - ' . $params['trans_id'];
        $msg = $msgRemark1;
        $msg.= "Trans ID: ".trim($params['sys_id'])." ".trim($params['trans_id'])."<br>Item/SN: ".trim($params['item_id'])." / ".trim($params['lot_id']);
        $msg.= "<br><br>Customer: " . $params['cust_name'] . " (".$params['cust_email'].")<br>Applecare PO: ".$params['po_number']."<br>Sale date: ".$params['purchase_date'];
        $msg.= "<br><br>".$params["warningmessage"];
        $this->cherps_email($subj,$msg,$email);

    }

    private function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {

        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = '';
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }

    /**
     * @param string $id
     * @return array
     */
    public function create_order($id='',$create_type='PO'){

        $request_array = $this->querySQL($id,$create_type);
        if (count($request_array)>0) {

            $po_type = 'BB';
            $po_id = $this->Pogrn_model->getRefId( $po_type, $po_type, 'PMS', 'CTL', "y", 13);

            $po_processed = array();
            $this_po_line = 0;
            foreach ($request_array as $k => $v) {
                $this_po_line++;
                $this_po = trim($po_id).'/'.sprintf('%04d', $this_po_line);
                $sale_date = date('d/m/y', strtotime($v['trans_date']));
                $data = array(
                    "requestContext" => array(
                        "shipTo" => $this->shiptoAcct,
                        "langCode" => 'en',
                        "timeZone" => '-480'
                    ),
                    "appleCareSalesDate" => $sale_date,
                    "pocLanguage" => 'ENG',
                    "pocDeliveryPreference" => 'E',
                    "mobileNumber" => '63886811',
                    "purchaseOrderNumber" => $this_po,
                    "marketID" => '',
                    "overridePocFlag" => '',
                    "smsFlag" => 'Y',
                    "emailFlag" => 'Y',
                    "customerRequest" => array(
                        "customerFirstName" => substr(trim($v['first_name']),0,30).' ',
                        "customerLastName" => substr(trim($v['last_name']),0,30).' ',
                        "companyName" => '',
                        "customerEmailId" => trim($v['email_addr']),
                        "addressLine1" => '1 UBI LINK',
                        "addressLine2" => '1 UBI LINK',
                        "city" => 'SINGAPORE',
                        "stateCode" => 'SG',
                        "countryCode" => 'SG',
                        "primaryPhoneNumber" => '63886811',
                        "zipCode" => '408553',
                    ),
                    "deviceRequest" => array(
                        "deviceId" => rtrim($v['lot_id'], ' '),
                        "secondarySerialNumber" => rtrim($v['lot_id2'], ' '),
                        "hardwareDateOfPurchase" => $sale_date
                    )
                );
                $create = $this->_acc_credentials('create-order', $data, array("trans_id"=>$this_po) );
                if ($create['status_code'] == 200 && $create['message']['orderDetailsResponses']['orderConfirmation']["agreementNumber"]!='') {
                    // Success
                    $po_success++;
                    $logged = $create['message']['orderDetailsResponses']['orderConfirmation'];
                    $this->updTxnSuccess($v["sys_id"],$v["trans_id"],$v["line_num"],$po_id);

                    $ins = array();
                    $ins["ref_type"] = $po_type;
                    $ins["ref_id"] = $po_id;
                    $ins["ref_num"] = $this_po_line;
                    $ins["trans_id"] = $v["trans_id"];
                    $ins["line_num"] = $v["line_num"];
                    $ins["loc_id"] = $v["loc_id"];
                    $ins["item_id"] = $v["item_id"];
                    $ins["status_level"] = $v["status_level"];
                    $ins["serialnumber"] = $v["lot_id"];
                    $ins["secondaryserialnumber"] = $v["lot_id2"];
                    $ins["ponumber"] = $logged["purchaseOrderNumber"];
                    $ins["usersessionid"] = '';
                    $ins["operationid"] = '';
                    $ins["tax"] = $logged["tax"];
                    $ins["agreementnumber"] = $logged["agreementNumber"];
                    $ins["partnumber"] = $logged["partNumber"];
                    $ins["parttype"] = $logged["partType"];
                    $ins["coveragedurationstatement"] = $logged["coverageDurationStatement"];
                    $ins["url"] = $logged["termConditionURL"];
                    $ins["warningmessage"] = $logged['warningMessage'];
                    $ins["created_by"] = 'chvoices';
                    $this->save_log($ins);

                    $po_processed[] = array(
                        "process" => 'SUCCESS',
                        "po_id" => $po_id,
                        "po_line_num" => $this_po_line,
                        "trans_id" => $v["trans_id"],
                        "trans_line_num" => $v['line_num'],
                        "loc_id" => $v['loc_id'],
                        "item_id" => $v['item_id'],
                        "trans_qty" => $v['trans_qty'],
                        "serialnumber" => $v["lot_id"],
                        "secondaryserialnumber" => $v["lot_id2"],
                        "poNumber" => $this_po,
                        "agreementNumber" => $logged["agreementNumber"]
                    );

                } else {
                    // Fail
                    $ins = array();
                    $ins["ref_type"] = $po_type;
                    $ins["ref_id"] = $po_id;
                    $ins["ref_num"] = $this_po_line;
                    $ins["trans_id"] = $v["trans_id"];
                    $ins["line_num"] = $v["line_num"];
                    $ins["loc_id"] = $v["loc_id"];
                    $ins["item_id"] = $v["item_id"];
                    $ins["status_level"] = $v["status_level"];
                    $ins["serialnumber"] = $v["lot_id"];
                    $ins["secondaryserialnumber"] = $v["lot_id2"];
                    $ins["ponumber"] = $this_po;
                    $ins["usersessionid"] = '';
                    $ins["operationid"] = '';
                    $ins["url"] = '';
                    $ins["warningmessage"] = (is_array($create['message']['error_message'])) ? json_encode($create['message']['error_message']) : $create['message']['error_message'];
                    $ins["created_by"] = 'chvoices';
                    $this->save_log($ins);

                    $po_processed[] = array(
                        "process" => 'FAIL',
                        "po_id" => $po_id,
                        "po_line_num" => $this_po_line,
                        "trans_id" => $v["trans_id"],
                        "trans_line_num" => $v['line_num'],
                        "loc_id" => $v['loc_id'],
                        "item_id" => $v['item_id'],
                        "trans_qty" => $v['trans_qty'],
                        "serialnumber" => $v["lot_id"],
                        "secondaryserialnumber" => $v["lot_id2"],
                        "poNumber" => $this_po,
                        "errorMessage" => $create['message']['error_message']
                    );

                    $this->_doErrormail(
                        array(
                            "po_id" => $this_po,
                            "sys_id" => $v["sys_id"],
                            "trans_id" => $v["trans_id"],
                            "item_id" => $v["item_id"],
                            "lot_id" => $v["lot_id"],
                            "warningmessage" => $ins["warningmessage"],
                            "purchase_date" => $sale_date,
                            "po_number" => $po_id,
                            "cust_name" => trim($v['first_name']).' '.trim($v['last_name']),
                            "cust_email" => trim($v['email_addr']),
                            "loc_id" => $v["loc_id"]
                        )
                    );
                }

            }

            // Generate PO
            $params = array();
            $params["po_id"] = $po_id; // Send in the PO number
            $params["emailPo"] = false; // Email notify supplier once po generated
            $params["sendapiPo"] = false; // Trigger api to supplier once po generated
            $params["coy_id"] = 'CTL';
            $params["trans_type"] = 'BB';
            $params["loc_id"] = 'HCL';
            $params["buyer_id"] = 'cherps';
            $params["supp_id"] = 'AC0015';
            $params["status_level"] = 4; // 4 means "po received"
            $params["tax_id"] = 'SG7';
            $params["remarks"] = '';
            $params["requester_id"] = 'cherps';
            $params["approve_by"] = 'cherps';
            $params["approve_date"] = date('Y-m-d H:i:s');
            $params["created_by"] = 'chvoices';
            $params["items"] = array();
            foreach ($po_processed as $v) {
                if ($v['process']=='SUCCESS') {
                    $params["items"][] = array(
                        "ref_id" => $v['trans_id'],
                        "ref_num" => $v['trans_line_num'],
                        "item_id" => $v['item_id'],
                        "qty_order" => $v['trans_qty'],
                        "loc_id" => $v['loc_id'],
                        "bom_ind" => ''
                    );
                }
            }
            if ( count($params["items"])>0 ){
                $created_po = $this->Pogrn_model->genPo($params, false);

                // Generate GRN
                $grnparam = array();
                $grnparam['coy_id'] = 'CTL';
                $grnparam['po_id'] = $po_id;
                $grnparam['loc_id'] = 'HCL';
                $grnparam['supp_id'] = 'AC0015';
                $grnparam['supp_do'] = 'B2B-HCL';
                $grnparam['order_type'] = 'I';
                $grnparam['created_by'] = 'chvoices';
                $grnparam['status_level'] = 0;
                $grnparam["items"] = array();
                foreach ($po_processed as $v) {
                    if ($v['process']=='SUCCESS') {
                        $grnparam["items"][] = array(
                            "item_id" => trim($v['item_id']),
                            "grn_qty" => $v['trans_qty'],
                            "loc_id" => $v['loc_id']
                        );
                    }
                }
                $created_grn = $this->Pogrn_model->genGrn($grnparam, $v['trans_id']);
            }

            return array(
                "status" => 'PROCESSED',
                "status_code" => 200,
                "message" => $po_processed,
                "documents" => array('PO'=>$created_po, 'GRN'=>$created_grn)
            );

        } else {
            return array(
                "status" => 'IDLE',
                "status_code" => 400,
                "message" => 'No orders found'
            );
        }

    }

    public function cancel_order($id='',$create_type='RN'){

        $request_array = $this->querySQL($id,$create_type);
        if (count($request_array)>0) {

            $po_processed = array();
            foreach ($request_array as $k => $v) {

                $po_type = 'RBB'. trim($v['loc_id']);
                $po_id = $this->Pogrn_model->getRefId( $po_type, "RN" . $po_type, 'PMS', 'CTL', "Ym", 15);
                $this_po_line = 1;
                $this_po = trim($po_id).'/'.sprintf('%04d', $this_po_line);
                $sale_date = date('d/m/y', strtotime($v['trans_date']));
                $data = array(
                    "requestContext" => array(
                        "shipTo" => $this->shiptoAcct,
                        "langCode" => 'en',
                        "timeZone" => '-480'
                    ),
                    "purchaseOrderNumber" => '',
                    "cancellationDate" => $sale_date,
                    "deviceId" => rtrim($v['lot_id'], ' ')
                );
                $create = $this->_acc_credentials('cancel-order', $data, array("trans_id"=>$po_id) );
                if ($create['status_code'] == 200 && $create['message']['cancelOrderResponse']["agreementNumber"]!='') {
                    // Success
                    $po_success++;
                    $logged = $create['message']['cancelOrderResponse'];
                    $this->updTxnSuccess($v["sys_id"],$v["trans_id"],$v["line_num"],$po_id);

                    // Generate RN
                    $params = array();
                    $params["rn_id"] = $po_id;
                    $params["coy_id"] = 'CTL';
                    $params["trans_type"] = 'BB';
                    $params["loc_id"] = $v['loc_id'];
                    $params["status_level"] = 1;
                    $params["buyer_id"] = 'karfatt';
                    $params["submit_by"] = 'cherps';
                    $params["approve_by"] = 'cherps';
                    $params["supp_id"] = 'AC0015';
                    $params["receipt_id"] = 'B2B-'.date('ymd'); // Generate a "receipt num"
                    $params["remarks"] = rtrim($v['lot_id'], ' ');
                    $params["created_by"] = 'chvoices';
                    $params["items"] = array();
                    $params["items"][] = array(
                        "ref_id" => $v['trans_id'],
                        "ref_rev" => '0',
                        "ref_num" => $v['line_num'],
                        "item_id" => $v['item_id'],
                        "trans_qty" => $v['trans_qty'] * -1,
                        "unit_price" => $v['unit_price']
                    );
                    $rn = $this->Pogrn_model->genRn($params);

                    $ins = array();
                    $ins["ref_type"] = 'CR';
                    $ins["ref_id"] = $po_id;
                    $ins["ref_num"] = $this_po_line;
                    $ins["trans_id"] = $v["trans_id"];
                    $ins["line_num"] = $v["line_num"];
                    $ins["loc_id"] = $v["loc_id"];
                    $ins["item_id"] = $v["item_id"];
                    $ins["status_level"] = $v["status_level"];
                    $ins["serialnumber"] = $v["lot_id"];
                    $ins["secondaryserialnumber"] = $v["lot_id2"];
                    $ins["ponumber"] = $logged["purchaseOrderNumber"];
                    $ins["usersessionid"] = '';
                    $ins["operationid"] = '';
                    $ins["tax"] ='';
                    $ins["agreementnumber"] = $logged["agreementNumber"];
                    $ins["partnumber"] = $logged["partNumber"];
                    $ins["parttype"] = '';
                    $ins["coveragedurationstatement"] = 'Cancellation Date:' . $logged["cancellationDate"] ;
                    $ins["url"] = '';
                    $ins["warningmessage"] = $logged['cancellationDesc'];
                    $ins["created_by"] = 'chvoices';
                    $this->save_log($ins);

                    $po_processed[] = array(
                        "process" => 'SUCCESS',
                        "po_id" => $po_id,
                        "po_line_num" => $this_po_line,
                        "trans_id" => $v["trans_id"],
                        "trans_line_num" => $v['line_num'],
                        "loc_id" => $v['loc_id'],
                        "item_id" => $v['item_id'],
                        "trans_qty" => $v['trans_qty'],
                        "serialnumber" => $v["lot_id"],
                        "secondaryserialnumber" => $v["lot_id2"],
                        "poNumber" => $this_po,
                        "agreementNumber" => $logged["agreementNumber"]
                    );

                } else {
                    // Fail
                    $ins["ref_type"] = 'CR';
                    $ins["ref_id"] = $po_id;
                    $ins["ref_num"] = $this_po_line;
                    $ins["trans_id"] = $v["trans_id"];
                    $ins["line_num"] = $v["line_num"];
                    $ins["loc_id"] = $v["loc_id"];
                    $ins["item_id"] = $v["item_id"];
                    $ins["status_level"] = $v["status_level"];
                    $ins["serialnumber"] = $v["lot_id"];
                    $ins["secondaryserialnumber"] = $v["lot_id2"];
                    $ins["ponumber"] = $this_po;
                    $ins["usersessionid"] = '';
                    $ins["operationid"] = '';
                    $ins["warningmessage"] = (is_array($create['message']['error_message'])) ? json_encode($create['message']['error_message']) : $create['message']['error_message'];
                    $ins["created_by"] = 'chvoices';
                    $this->save_log($ins);

                    $po_processed[] = array(
                        "process" => 'FAIL',
                        "po_id" => $po_id,
                        "po_line_num" => $this_po_line,
                        "trans_id" => $v["trans_id"],
                        "trans_line_num" => $v['line_num'],
                        "loc_id" => $v['loc_id'],
                        "item_id" => $v['item_id'],
                        "trans_qty" => $v['trans_qty'],
                        "serialnumber" => $v["lot_id"],
                        "secondaryserialnumber" => $v["lot_id2"],
                        "poNumber" => $this_po,
                        "errorMessage" => $create['message']['error_message']
                    );

                    $this->_doErrormail(
                        array(
                            "po_id" => $this_po,
                            "sys_id" => $v["sys_id"],
                            "trans_id" => $v["trans_id"],
                            "item_id" => $v["item_id"],
                            "lot_id" => $v["lot_id"],
                            "warningmessage" => $ins["warningmessage"] /*,
                            "purchase_date" => $sale_date,
                            "po_number" => $po_id,
                            "cust_name" => trim($v['first_name']).' '.trim($v['last_name']),
                            "cust_email" => trim($v['email_addr']),
                            "loc_id" => $v["loc_id"] */
                        )
                    );

                }

            }

            return array(
                "status" => 'PROCESSED',
                "status_code" => 200,
                "message" => $po_processed,
                "documents" => array('RN'=>$created_po)
            );

        } else {
            return array(
                "status" => 'IDLE',
                "status_code" => 200,
                "message" => 'No orders found'
            );
        }

    }
}

?>