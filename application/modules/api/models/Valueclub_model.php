<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Valueclub_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
        ini_set('display_errors', 0);
        $this->coy_id = 'CTL';
        $this->blacklistemail = ['hq@challenger.sg'];
    }

    public function apireturncodes($code,$additionalrtn=NULL){
        switch($code){
            case 90000:
                $return = array("code" => 0, "msg" => 'No existing record', "msgcode"=>90000);
                break;
            case 10041:
                $return = array("code" => -1, "msg" => 'Invalid member ID', "msgcode"=>10041);
                break;
            case 10042:
                $return = array("code" => -1, "msg" => 'Enter a valid email and contact number', "msgcode"=>10042, "msgparts" => array("email_addr"=>'Enter a valid email address',"contact_num"=>'Enter a valid contact number') );
                break;
            case 10043:
                $return = array("code" => -1, "msg" => 'Enter a valid email address', "msgcode"=>10043, "msgparts" => array("email_addr"=>'Enter a valid email address',"contact_num"=>'') );
                break;
            case 10044:
                $return = array("code" => -1, "msg" => 'Enter a valid contact number', "msgcode"=>10044, "msgparts" => array("email_addr"=>'',"contact_num"=>'Enter a valid contact number') );
                break;
            case 10045:
                $return = array("code" => -1, "msg" => 'Enter a valid email address', "msgcode"=>10045, "msgparts" => array("email_addr"=>'Enter a valid email address',"contact_num"=>'') );
                break;
            case 10031:
                $return = array("code" => -1, "msg" => 'This email and contact no. exists in the system', "msgcode"=>10031, "msgparts" => array("email_addr"=>'This email exists in the system',"contact_num"=>'This contact no. exists in the system'));
                break;
            case 10032:
                $return = array("code" => -1, "msg" => 'This email exists in the system.', "msgcode"=>10032, "msgparts" => array("email_addr"=>'This email exists in the system.',"contact_num"=>''));
                break;
            case 10033:
                $return = array("code" => -1, "msg" => 'This contact no. exists in the system.', "msgcode"=>10033, "msgparts" => array("email_addr"=>'',"contact_num"=>'This contact no. exists in the system.'));
                break;
            case 80011:
                $return = array("code" => 1, "msg" => 'Existing record pending verification', "member"=>$rtnmbr, "msgcode"=>80011);
                break;
            case 80012:
                $return = array("code" => 1, "msg" => 'Existing record pending processing', "member"=>$rtnmbr, "msgcode"=>80012);
                break;
            case 80020:
                $return = array("code" => 1, "msg" => 'Existing record has been processed', "member"=>$rtnmbr, "msgcode"=>80020);
                break;
            case 80050:
                $return = array("code" => 1, "msg" => 'This membership has been verified.', "msgcode"=>80050);
                break;
            case 70011:
                $return = array("code" => 2, "msg" => 'Record created. Pending verification', "msgcode"=>70011);
                break;
            case 70012:
                $return = array("code" => 2, "msg" => 'Record created. Pending processing', "msgcode"=>70012);
                break;
            case 70021:
                $return = array("code" => 2, "msg" => 'Successfully verified record. Pending processing.', "msgcode"=>70021);
                break;
            default:
                $return = array("code" => -1, "msg" => 'General error', "msgcode"=>99999);
                break;
        }
        if (!is_null($additionalrtn)) {
            foreach ($additionalrtn as $rkey => $rtn) {
                $return[$rkey] = $rtn;
            }
        }
        return $return;
    }

    public function verify($mbr_id){

        $postbody = trim(file_get_contents('php://input'));

        if ($mbr_id=='') {
            return $this->apireturncodes(10041);
        }
        if (strlen(trim($mbr_id))==10 && substr($mbr_id,0,1)=='V' && !in_array($mbr_id,['VE5414533P','VIVG145718']) ) {
            return $this->apireturncodes(80050);
        }

        // 1. Check if member is verified, if verified reply with the authed contact
        $sql = "SELECT mbr.coy_id,rtrim(mbr.mbr_id) mbr_id,rtrim(mbr.mbr_pwd) mbr_pwd,rtrim(mbr.first_name) first_name,rtrim(mbr.email_addr) email_addr,rtrim(mbr.contact_num) contact_num,
                      v.new_email_addr,v.new_contact_num,isnull(v.status_level,-3) verify_status
                    FROM crm_member_list mbr
                    LEFT JOIN crm_member_verification v on mbr.coy_id=v.coy_id and mbr.mbr_id=v.mbr_id
                    WHERE mbr.coy_id='".$this->coy_id."' AND mbr.mbr_id='$mbr_id'
                    ORDER BY v.status_level DESC ";
        $mbrs = $this->db->query($sql)->result_array();
        $rtnmbr = NULL;
        if (!$mbrs) {
            $rtncode = 10041;
        }
        else if ($mbrs && $mbrs[0]['verify_status']>=0) {
            $rtnmbr['member'] = array("mbr_id" => trim($mbrs[0]['mbr_id']), "email_addr" => $mbrs[0]['new_email_addr'], "contact_num" => $mbrs[0]['new_contact_num']);
            $rtncode = ($mbrs[0]['verify_status']==2) ? 80020 : ( ($mbrs[0]['verify_status']==1) ? 80012 : 80011 ) ;
        }
        else {
            // Do not exist, insert record
            $post = json_decode($postbody);
            $post->new_email_addr = trim($post->new_email_addr);
            $post->new_contact_num = trim($post->new_contact_num);
            if ($postbody=='') {
                // For querying call
                $rtncode = 90000;
            }
            else if (!in_array($post->new_email_addr,$this->blacklistemail) && filter_var($post->new_email_addr, FILTER_VALIDATE_EMAIL) &&
                        strlen($post->new_contact_num)>=8 && strlen($post->new_contact_num)<=10 &&
                        (in_array(substr($post->new_contact_num,0,1),[6,8,9])) //preg_match('/(6|8|9)[0-9]{7}/m', $post->new_contact_num)>0
                        ) {

                //Check for duplicates in db
                $chg_status=0;
                $sql = "SELECT sum(email_repeat) email_repeat, sum(contact_repeat) contact_repeat FROM (
                            select (case when email_addr='" . $post->new_email_addr . "' then 1 else 0 end) email_repeat, (case when contact_num='" . $post->new_contact_num . "' then 1 else 0 end) contact_repeat
                            from crm_member_list where coy_id='".$this->coy_id."' and mbr_id <> '$mbr_id' and (email_addr='" . $post->new_email_addr . "' or contact_num='" . $post->new_contact_num . "') and status_level>=0
                            ) X ";
                $mbrsexist = $this->db->query($sql)->row_array();
                if ($mbrsexist['email_repeat']>0 && $mbrsexist['contact_repeat']>0) {
                    $chg_status=-1;
                    $rtncode = 10031;
                }
                else if ($mbrsexist['email_repeat']>0) {
                    $chg_status=-1;
                    $rtncode = 10032;
                }
                else if ($mbrsexist['contact_repeat']>0) {
                    $chg_status=-1;
                    $rtncode = 10033;
                }

                if ($chg_status >= 0) {
                    $created_by = (isset($post->source) && $post->source!='') ? $post->source : getRealIpAddr(15);
                    $chg_status = (trim(strtolower($post->new_email_addr))!=trim(strtolower($mbrs[0]['email_addr']))) ? 0 : 1 ; // If change of email, trigger verification email
                    $sql = "INSERT INTO crm_member_verification (coy_id,mbr_id,email_addr,contact_num,new_email_addr,new_contact_num,status_level,created_by) 
                            VALUES ('" . $this->coy_id . "','" . trim($mbrs[0]['mbr_id']) . "','" . $mbrs[0]['email_addr'] . "','" . $mbrs[0]['contact_num'] . "','" . $post->new_email_addr . "','" . $post->new_contact_num . "',".$chg_status.",'$created_by')";
                    if (trim($mbrs[0]['mbr_id'])!='STU-180000020235')
                        $ins = $this->db->query($sql);

                    if ($chg_status == 0) {
                        $rtncode = 70011;

                        // Trigger email for verification
                        $this->sendUpdateDetailEmail($mbrs[0]['mbr_id'],$post->new_email_addr,$mbrs[0]['first_name'],$created_by);
                    }
                    else {
                        $this->updverified($mbr_id); // Update to crm_member_list
                        $rtncode = 70012;
                    }
                }

            }
            else {
                $rtncode = 10042;
                if (in_array($post->new_email_addr,$this->blacklistemail)) {
                    $rtncode = 10045;
                }
                else if (!filter_var($post->new_email_addr, FILTER_VALIDATE_EMAIL) && (!(strlen($post->new_contact_num)>=8 && strlen($post->new_contact_num)<=10) || !(in_array(substr($post->new_contact_num,0,1),[6,8,9])) ) ) {
                    $rtncode = 10042;
                }
                else if (!filter_var($post->new_email_addr, FILTER_VALIDATE_EMAIL)) {
                    $rtncode = 10043;
                }
                else if (!(strlen($post->new_contact_num)>=8 && strlen($post->new_contact_num)<=10)  || !(in_array(substr($post->new_contact_num,0,1),[6,8,9])) ){
                    $rtncode = 10044;
                }
            }
        }

        $returnreply = $this->apireturncodes($rtncode,$rtnmbr);

        // Save log
        $sql = "insert b2b_gateway_message (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id,request_link,request_msg,status_level,return_status,return_msg,created_by)
                    values ('CTL','VCMBRVERIFY',left(newid(),20),'','','','verify/".$mbr_id."','".$postbody."',0,'".$rtncode."','".$returnreply['msg']."','CHV')";
        $insw = $this->db->query($sql);

        // Return cases
        return $returnreply;

    }

    public function verified($mbr_id) {

        // 1. Check if member is verified, if verified reply with the authed contact
        $sql = "SELECT mbr.coy_id,mbr.mbr_id,mbr.email_addr,mbr.contact_num,v.new_email_addr,v.new_contact_num,isnull(v.status_level,-3) verify_status
                    FROM crm_member_list mbr
                    LEFT JOIN crm_member_verification v on mbr.coy_id=v.coy_id and mbr.mbr_id=v.mbr_id
                    WHERE mbr.coy_id='".$this->coy_id."' AND mbr.mbr_id='$mbr_id'
                    ORDER BY v.status_level DESC ";
        $mbrs = $this->db->query($sql)->result_array();

        $rtnmbr = NULL;
        if (!$mbrs) {
            $rtncode = 10041;
        }
        else if ($mbrs && $mbrs[0]['verify_status']>=2) {
            $rtncode = 80020;
        }
        else if ($mbrs && $mbrs[0]['verify_status']>=0) {
            $sql = "UPDATE crm_member_verification SET status_level=1 WHERE coy_id='".$this->coy_id."' AND mbr_id='$mbr_id'";
            $this->db->query($sql);
            $this->updverified($mbr_id); // Update to crm_member_list
            $rtncode = 70021;

            $callback = $this->input->get('callback');
            if ($callback){
                redirect($callback.'?&code=1&msg=Successfully verified.');
            }
        }
        else {
            $rtncode = 90000; // No exist
        }


        return $this->apireturncodes($rtncode);

    }

    public function updverified($mbr_id) {
        $sql = "update crm_member_list
	            set email_addr = v.new_email_addr , contact_num = v.new_contact_num , modified_on=GETDATE()
                from crm_member_list, crm_member_verification v
                where crm_member_list.coy_id=v.coy_id and crm_member_list.mbr_id=v.mbr_id and v.status_level=1
                and crm_member_list.mbr_id='".trim($mbr_id)."'";
        $mbrs = $this->db->query($sql);
        return $mbrs;
    }

    public function updnewmbrid($mbr_id='') {
        if (strlen($mbr_id)>3) $mbr_id = $mbr_id;
        $sql = "EXEC [sp_crm_member_updverified] '$mbr_id'";
        $mbrs = $this->db->query($sql)->result_array();
        return $mbrs;
    }
    public function delverified($mbr_id=''){
        if ($mbr_id!='') {
            $sql = "DELETE FROM crm_member_verification WHERE coy_id='".$this->coy_id."' AND mbr_id='$mbr_id'";
            $mbrs = $this->db->query($sql);
            return $mbrs;
        }
    }

    public function sendUpdateDetailEmail($mbr_id,$email_addr,$first_name,$source) {
        $url = (ENVIRONMENT=='production') ? 'https://www.hachi.tech/api/vc2/sendUpdateDetailEmail' : 'https://web3.sghachi.com/api/vc2/sendUpdateDetailEmail';
        $uri = '/'.$mbr_id;

        $json_data = json_encode( array("data"=>array('u' => $mbr_id, 'e' => $email_addr),"first_name"=>$first_name,"source"=>$source) );
        return $this->_doCurlHachi($url.$uri , $json_data) ;
    }

    private function _doCurlHachi($url,$json_data)
    {
        $headers[] = "X_AUTHORIZATION:  NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }



    public function insertOsTxn($date=''){
        $cherps_mssql = $this->load->database('cherps_hachi', TRUE);
        $cherps_pgsql = $this->load->database('pg92', TRUE);

        if ($date=='')
            $date = date('Y-m-d');

        $invoice_id = 'OS' . date('Ym', strtotime($date) ) . 'M0000' . date('d', strtotime($date) );

        $this->db->delete('sms_invoice_list', "invoice_id = '$invoice_id' ");
        $this->db->delete('sms_invoice_item', "invoice_id = '$invoice_id' ");

        $sql = "select 'CTL' coy_id, '$invoice_id' invoice_id, ROW_NUMBER() OVER(ORDER BY mbr_type) line_num,
                       (case mbr_type when 'MST' then '!MEMBER-EDU' when 'MSTP' then '!MEMBER-EDU' when 'MAS' then '!MEMBER-ASC' when 'MNS' then '!MEMBER-NS' end) item_id,
                       'Valueclub Membership - ' + mbr_type item_desc,
                       count(*) qty_invoiced
                from crm_member_list
                where coy_id='CTL' and mbr_type in ('MST','MSTP','MNS','MAS') and cast(join_date as date) = cast('$date' as date)
                group by mbr_type";
        $result = $cherps_mssql->query($sql)->result_array();

        $response = [];
        foreach($result as $ins) {
            $response[] = array(
                "item_id"   => $ins['item_id'],
                "qty"       => $ins['qty_invoiced']
            );

            $ins["deposit_id"] = '';
            $ins["created_by"] = 'VC';
            $ins["created_on"] = date('Y-m-d H:i');
            $ins["modified_by"] = 'VC';
            $ins["modified_on"] = date('Y-m-d H:i');

            $cherps_pgsql->insert('sms_invoice_item', $ins);
        }

        $insHeader = array(
            "coy_id"        => 'CTL',
            "invoice_id"    => $invoice_id,
            "inv_type"      => 'OS',
            "invoice_date"  => date('Y-m-d H:i', strtotime($date) ),
            "cust_id"       => 'VALUECLUB',
            "status_level"  => 3,
            "created_by"    => 'VC',
            "created_on"    => date('Y-m-d H:i'),
            "modified_by"   => 'VC',
            "modified_on"   => date('Y-m-d H:i')
        );
        $this->db->insert('sms_invoice_list', $insHeader);

        return array("invoice_id" => $invoice_id, "items" => $response );
    }
}

?>
