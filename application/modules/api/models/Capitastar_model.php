<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Capitastar_model extends Base_Common_Model {

    private $subkey;
    private $apiurl;
    private $notification;
    private $gatewaycode;
    private $gatewayuser;
    private $token;

    public function __construct() {
        parent::__construct();
        $this->db = $this->load->database('cherps_hachi', TRUE);

        $this->gatewaycode = md5(time());
        $this->token = '';
        $this->gatewayuser = '';

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->subkey = 'bb7117edf2ba44a59d2aa6b6d367b43a';
            $this->apiurl = 'https://api.capitastar.com/';
            $this->notification = 'MIS.team@challenger.sg; ';
        }
        else {
            $this->subkey = '44501d7b43f44bf287bcf9992a745edf';
            $this->apiurl = 'https://api-uat.capitastar.com/';
            $this->notification = 'yongsheng@challenger.sg; ';
        }
    }

    public function nomoremistake($loc_id,$pos_id) {
        $token = $this->auth_token($loc_id,$pos_id);
        $logging = array("trans_id"=>'YS20181226', "loc_id"=>$loc_id, "pos_id"=>$pos_id);

        if ($pos_id=='V') {
            $sql = "select top 100 trans_id,lot_id,unit_price,gateway_meta from b2b_gateway_lot2 
                        where loc_id='$loc_id'
                        order by modified_on ; ";
            $voucher_db = $this->db->query($sql)->result_array();
            if ($voucher_db) {
                foreach ($voucher_db as $vch_db) {

                    $oldloc = json_decode($vch_db['gateway_meta']);

                    $qacct['merchantUserId'] = $token['id'];
                    $qacct['voucherNumber'] = trim($vch_db['lot_id']);
                    $qacct['requestTimestamp'] = (int)time() . '000';
                    $qacct['signature'] = hash('sha256', $qacct['merchantUserId'] . $qacct['voucherNumber'] . $token['token'] . $qacct['requestTimestamp']);
                    $refresh = $this->_doCurl('merchant/voucher/v1/validate', $token['token'], $qacct, $logging);

                    if ($refresh->status=="Success") {
                        $vchValue = (isset($refresh->voucherValue) && $refresh->voucherValue>0) ? $refresh->voucherValue : $this->_readVchDeal($refresh->voucherName);
                        $sql = "update b2b_gateway_lot2 set modified_on=GETDATE(), string_udf2='".$refresh->voucherValue."' 
                                    where lot_id='".trim($vch_db['lot_id'])."' ; ";
                        $voucher_db = $this->db->query($sql);

                        $vouchers[] = 'OK [OLD:'.$oldloc->loc_id.'] '.trim($vch_db['trans_id']).' '.trim($vch_db['lot_id']) .' - '.$refresh->voucherValue;
                    }
                    else {

                        $sql = "update b2b_gateway_lot2 set modified_on=GETDATE(), string_udf2='".$refresh->message."' 
                                    where lot_id='".trim($vch_db['lot_id'])."' ; ";
                        $voucher_db = $this->db->query($sql);

                        $vouchers[] = 'FAIL [OLD:'.$oldloc->loc_id.'] '.trim($vch_db['trans_id']).' '.trim($vch_db['lot_id']) .' - '.$refresh->message;
                    }

                }
            }
        }
        echo json_encode($vouchers);
        exit;
    }

    public function tally_daily($trans_date,$loc_id,$pos_id='') {
        $token = $this->auth_token($loc_id,$pos_id);
        if (!isset($token['id']) || $token['id']=='') {
            return array(
                "code" => -1,
                "result" => "API FAILED"
            );
        }

        $logging = array("trans_id"=>'', "loc_id"=>$loc_id, "pos_id"=>$pos_id);
        $qacct['merchantUserId'] = $token['id'];
        $qacct['date'] = strtoupper(date('d-M-Y', strtotime($trans_date)));
        $qacct['requestTimestamp'] = (int)time() . '000';
        $qacct['signature'] = hash('sha256', $qacct['merchantUserId'] . $qacct['date'] . $token['token'] . $qacct['requestTimestamp']);
        $refresh = $this->_doCurl('merchant/voucher/v1/summary', $token['token'], $qacct, $logging);

        $return['loc_id'] = $loc_id;
        $return['trans_date'] = $qacct['date'];
        foreach ($refresh as $type){
            $return['capitastar_summary'][] = array(
                "voucher_type" => $type->voucherType,
                "voucher_subtotal" => $type->amount,
                "voucher_count" => $type->count
            );
        }

        $sql = "SELECT rtrim(trans_id) trans_id,rtrim(lot_id) lot_id,unit_price,string_udf1,string_udf2,string_udf3,datetime_udf FROM b2b_gateway_lot 
                  WHERE coy_id='CTL' and loc_id='$loc_id' and cast(created_on as date)='".date('Y-m-d', strtotime($trans_date))."'
                    AND string_udf3 NOT IN ('USED-OTHERRTXN')
                  ORDER BY created_on ";
        $rec_pos = $this->db->query($sql)->result_array();
        foreach ($rec_pos as $r){
            if ($r['string_udf3']=='UTILIZED') {
                // Add to summary
                $v = $r['string_udf1'];
                $pos_summary[$v]['subtotal'] += $r['unit_price'] ;
                $pos_summary[$v]['count'] ++ ;
            }

            // Add transaction_records
            $trans_records[$r['trans_id']]['vouchers'][] = array('vch_id'=>$r['lot_id'], 'vch_amt'=>$r['unit_price'], 'vch_remark'=>$r['string_udf2']);
            $trans_records[$r['trans_id']]['collection'] += $r['unit_price'];
        }
        foreach ($pos_summary as $key=>$type){
            $pos_summary_rtn[] = array(
                "collection_type" => $key,
                "collection_subtotal" => $type['subtotal'],
                "collection_count" => $type['count']
            );
        }
        $return['pos_summary'] = $pos_summary_rtn;

        $sql = "select rtrim(p.trans_id) trans_id,rtrim(p.fin_dim2) trans_dim,p.trans_amount from pos_transaction_list l join pos_payment_list p on l.coy_id=p.coy_id and l.trans_id=p.trans_id
                    where l.coy_id='CTL'and p.pay_mode='2-Voucher(O)' and l.loc_id='$loc_id'  and cast(l.trans_date as date)='".date('Y-m-d', strtotime($trans_date))."'
                    order by p.created_on";
        $rec_settle = $this->db->query($sql)->result_array();
        foreach ($rec_settle as $r) {
            $v = $r['trans_dim'];
            $settle_summary[$v]['subtotal'] += $r['trans_amount'] ;

            // Add transaction_records
            $trans_records[$r['trans_id']]['settlement'] += $r['trans_amount'];
        }
        foreach ($settle_summary as $key=>$type){
            $settle_summary_rtn[] = array(
                "settlement_type" => $key,
                "settlement_subtotal" => $type['subtotal']
            );
        }
        $return['settlement_summary'] = $settle_summary_rtn;

        $return['transactions'] = $trans_records;

        return $return;
        exit;
    }

    public function validate($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str) {

        if ($trans_id=='NOMOREMISTAKE') {
            return $this->nomoremistake($loc_id,$pos_id);
        }

        $vouchers = $this->_getVouchers($vouchers_str);

        if (count($vouchers)==1 && substr($vouchers[0],0,3)=='ECV') {
            /* Sample response by pay_burn:
                $rtn = array(
                    "code" => 1,
                    "result" => "SUCCESS",
                    "mode" => "PAY",
                    "trans_id" => $trans_id,
                    "payment_id" => $this->gatewaycode,
                    "payment_amount" => $validated["OKAMT"]
                );
            */

            if ($pos_id=='YS') {
                return array(
                    "code" => 1,
                    "result" => "SUCCESS",
                    "mode" => "PAY",
                    "trans_id" => $trans_id,
                    "payment_id" => "123123123",
                    "payment_amount" => 30
                );
            }

            return $this->pay_burn($trans_id,$loc_id,$pos_id,0,$vouchers[0]);
        }
        else {
            if ($pos_id=='YS') {
                return array(
                    "code" => -1,
                    "result" => "SUCCESS",
                    "mode" => "VALIDATE",
                    "trans_id" => $trans_id,
                    "valid_amount" => 20,
                    "voucher_log" => [
                        [
                            "voucher" => '***010-0000000###',
                            "amount" => 10,
                            "remark" => '$10 Voucher'
                        ],
                        [
                            "voucher" => '***010-0000001###',
                            "amount" => 10,
                            "remark" => '$10 Voucher'
                        ]
                    ]
                );
            }
            return $this->vch_validate($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str);
        }

    }

    public function vch_validate($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str) {

        $created_on = time();

        $token = $this->auth_token($loc_id,$pos_id);
        if (!isset($token['id']) || $token['id']=='') {
            return array(
                "code" => -1,
                "result" => "API FAILED"
            );
        }
        $batch_id = 0; //$this->get_gwlot($trans_id);
        $vouchers = $this->_getVouchers($vouchers_str);
        $trans_amt = floatval(trim(str_replace([',','$'],'',$trans_amt))) ;

        $validated = array("OK"=>0,"OKAMT"=>0,"FAIL"=>0,"VCHLOG"=>array());
        $logging = array("trans_id"=>$trans_id, "loc_id"=>$loc_id, "pos_id"=>$pos_id);

        $vouchers_dupstat = $this->get_gwlot($trans_id,$vouchers,$rtn='lot_id');
        foreach ($vouchers as $voucher) {
            $qacct['merchantUserId'] = $token['id'];
            $qacct['voucherNumber'] = $voucher;
            $qacct['requestTimestamp'] = (int)time() . '000';
            $qacct['signature'] = hash('sha256', $qacct['merchantUserId'] . $qacct['voucherNumber'] . $token['token'] . $qacct['requestTimestamp']);
            $refresh = $this->_doCurl('merchant/voucher/v1/validate', $token['token'], $qacct, $logging);

            if ($refresh->status=="Success") {
                $vchValue = (isset($refresh->voucherValue) && $refresh->voucherValue>0) ? $refresh->voucherValue : $this->_readVchDeal($refresh->voucherName);
                $gwpar = array();
                $gwpar['duplicates'] = in_array($qacct['voucherNumber'],$vouchers_dupstat);
                $gwpar['loc_id'] = $loc_id;
                $gwpar['trans_id'] = $trans_id;
                $gwpar['lot_id'] = $qacct['voucherNumber'];
                $gwpar['unit_price'] = $vchValue;
                $gwpar['string_udf1'] = $this->_readVchDim($refresh->voucherType); // ($refresh->voucherType=='eVoucher' || $refresh->voucherType=='eCapitaVoucher') ? 'E-CAPITALAND' : 'CAPITALAND';
                $gwpar['string_udf3'] = '-';
                $this->save_gwlot($gwpar);

                $validated["OK"]++;
                $validated["OKAMT"] += floatval($vchValue);
                $validated["VCHLOG"][] = array(
                    "voucher" => $qacct['voucherNumber'],
                    "amount" => $vchValue,
                    "remark" => $refresh->voucherName
                );
            }
            else {
                $gwpar = array();
                $gwpar['duplicates'] = in_array($qacct['voucherNumber'],$vouchers_dupstat);
                $gwpar['loc_id'] = $loc_id;
                $gwpar['trans_id'] = $trans_id;
                $gwpar['lot_id'] = $qacct['voucherNumber'];
                $gwpar['string_udf2'] = $refresh->message .' '. $refresh->errorCode;
                $gwpar['string_udf3'] = 'ERROR';
                $this->save_gwlot($gwpar);

                $validated["FAIL"]++;
                $validated["VCHLOG"][] = array(
                    "voucher" => $qacct['voucherNumber'],
                    "amount" => 0,
                    "remark" => $refresh->message
                );
            }

            if (substr(trim($loc_id),-2)=='-R')
                $this->save_roadshow($qacct['voucherNumber']);
        }

        if (count($vouchers)>0 && $validated["FAIL"]==0 && ($trans_amt==$validated["OKAMT"] || $trans_amt==0) ) {
            // All good
            $rtn = array(
                "code" => 1,
                "result" => "SUCCESS",
                "mode" => "VALIDATE",
                "trans_id" => $trans_id,
                "valid_amount" => $validated["OKAMT"],
                "voucher_log" => $validated["VCHLOG"]
            );
        }
        else if (count($vouchers)>0 && $validated["FAIL"]==0){
            // Voucher valid but amount don't match
            $rtn = array(
                "code" => 0,
                "result" => "AMOUNT MISMATCH",
                "mode" => "VALIDATE",
                "trans_id" => $trans_id,
                "valid_amount" => $validated["OKAMT"],
                "voucher_log" => $validated["VCHLOG"]
            );
        }
        else {
            // Invalid
            $rtn = array(
                "code" => -1,
                "result" => "FAILED",
                "mode" => "VALIDATE",
                "trans_id" => $trans_id,
                "valid_amount" => $validated["OKAMT"],
                "voucher_log" => $validated["VCHLOG"]
            );
        }

//        $this->gatewaycode = md5('TRACK-'.$logging['trans_id'].time().rand(1000,9999).rand(1000,9999).rand(1000,9999));
//        $gwpar['created_on'] = date('Y-m-d H:i:s',$created_on);
//        $gwpar['modified_on'] = date('Y-m-d H:i:s');
//        $gwpar['trans_id'] = $logging['trans_id'];
//        $gwpar['loc_id'] = $logging['loc_id'];
//        $gwpar['pos_id'] = $logging['pos_id'];
//        $gwpar['request_link'] = 'https://internal.challenger.sg/api/capitastar/validate';
//        $gwpar['request_msg'] = '';
//        $gwpar['status_level'] = '0';
//        $gwpar['return_status'] = '';
//        $gwpar['return_msg'] = 'Tracer';
//        $this->save_gwmsg($gwpar);

        return $rtn;
    }

    public function pay($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str) {

        if ($pos_id=='YS') {
            $rtn = array(
                "code" => 1,
                "result" => "SUCCESS",
                "mode" => "PAY",
                "trans_id" => $trans_id,
                "payment_id" => "123123123",
                "payment_amount" => 30
            );
            return $rtn;
            exit;
        }

        if (strtoupper($trans_id)=='MISSING') {
            return $this->pay_missing($trans_id,$loc_id,$pos_id);
            exit;
        }

        $vouchers = $this->_getVouchers($vouchers_str);
        $trans_amt = floatval(trim(str_replace([',', '$'], '', $trans_amt)));
        if (count($vouchers)<1) {
            // Get vouchers from validated
            $sql = "select trans_id,lot_id,unit_price from b2b_gateway_lot where coy_id='CTL' and gateway_id='CAPITASTAR' and trans_id='$trans_id' and unit_price>0 and string_udf3=''; ";
            $voucher_db = $this->db->query($sql)->result_array();
            if ($voucher_db) {
                foreach ($voucher_db as $vch_db) {
                    if (!in_array(trim($vch_db['lot_id']), $vouchers))
                        $vouchers[] = trim($vch_db['lot_id']);
                }
                $validates['code'] = 1;
            }
            else {
                $validates['code'] = 0;
            }
        }
        else {
            // Validates the submitted vouchers
            $validates = $this->validate($trans_id, $loc_id, $pos_id, $trans_amt, implode(';', $vouchers));
        }
        if ( !isset($validates['mode']) || (isset($validates['mode']) && $validates['mode'] != 'PAY')) {
            // Burn them
            $burns = $this->pay_burn($trans_id, $loc_id, $pos_id, $trans_amt, implode(';', $vouchers));
            return $burns;
        } else {
            //$validates['code'] = 0;
            return $validates;
        }

    }

    public function pay_missing($trans_id,$loc_id,$pos_id){
        // Burn vouchers that are missed by API
        $sql_date = ($this->input->get('date')) ? "'".$this->input->get('date')."'" : "cast(GETDATE() as date)" ;
        $sql = "select top 10 loc_id,pos_id,rtrim(trans_id) trans_id,api_amt,utilized_amt,pos_amt, api_amt-pos_amt variance
                    from (
                        select loc_id,left(trans_id,2) pos_id, trans_id, sum(unit_price) api_amt,
                            isnull((select sum(unit_price) from b2b_gateway_lot b where b.trans_id=b2b_gateway_lot.trans_id and unit_price>0 and string_udf3='UTILIZED'),0) utilized_amt,
                            isnull((select sum(trans_amount) from pos_payment_list where trans_id=b2b_gateway_lot.trans_id and pay_mode='2-VOUCHER(O)'),0) pos_amt
                        from b2b_gateway_lot
                        where cast(created_on as date)=$sql_date and unit_price>0 and loc_id='$loc_id'
                        group by loc_id,trans_id
                    ) x
                    order by utilized_amt,variance";
        $missing = $this->db->query($sql)->result_array();
        $validates=array();
        foreach ($missing as $m) {
            if ($m['variance']==0 && $m['utilized_amt']==0) {
                $validates[] = array(
                    'trans_id' => $m['trans_id'],
                    'response' => ($pos_id=='YS') ? $this->pay($m['trans_id'], $m['loc_id'], $m['pos_id'], 0, '') : 'Pls submit with valid pos_id to utlize this transaction.'
                );
            }
            else {
                if ($m['utilized_amt']!=$m['api_amt']) {
                    $validates[] = array(
                        'trans_id' => $m['trans_id'],
                        'response' => 'API amt $' . $m['api_amt'] . ' | Utilized amt $' . $m['utilized_amt'] . ' | POS amt $' . $m['pos_amt']
                    );
                }
            }
        }
        return $validates;
        exit;
    }

    private function pay_burn($trans_id,$loc_id,$pos_id,$trans_amt,$vouchers_str) {

        $token = $this->auth_token($loc_id,$pos_id);
        if (!isset($token['id']) || $token['id']=='') {
            return array(
                "code" => -1,
                "result" => "API FAILED"
            );
        }
        $batch_id = 0; //$this->get_gwlot($trans_id);
        $vouchers = $this->_getVouchers($vouchers_str);
        $trans_amt = floatval(trim(str_replace([',', '$'], '', $trans_amt)));

        $validated = array("OKAMT"=>0,"VCHLOG"=>'');
        $logging = array("trans_id"=>$trans_id, "loc_id"=>$loc_id, "pos_id"=>$pos_id);

        $vouchers_stat = $this->get_gwlot($trans_id,$vouchers,$rtn='SUMMARY');
        $vouchers_dupstat = $vouchers_stat['lot_id'];
        if (count($vouchers_stat['lot_id'])>1 && $vouchers_stat['string_udf1']=='CAPITALAND') {
            // Burn Multiple
            $qacct['merchantUserId'] = $token['id'];
            $qacct['voucherNumbers'] = $vouchers;
            $qacct['requestTimestamp'] = (int)time() . '000';
            $qacct['signature'] = hash('sha256', $qacct['merchantUserId'] . implode('',$vouchers) . $token['token'] . $qacct['requestTimestamp']);
            $burn = $this->_doCurl('merchant/voucher/v1/burn/capitavoucher', $token['token'], $qacct, $logging);

            if ($burn->status=="Success") {
                $validatedamt = 0;
                foreach ($burn->vouchers as $voucher) {
                    $gwpar = array();
                    $gwpar['duplicates'] = in_array($voucher->voucherNumber, $vouchers_dupstat);
                    $gwpar['loc_id'] = $loc_id;
                    $gwpar['trans_id'] = $trans_id;
                    $gwpar['lot_id'] = $voucher->voucherNumber;
                    $gwpar['string_udf3'] = 'UTILIZED';
                    $gwpar['datetime_udf'] = date('Y-m-d H:i:s');
                    if (!$gwpar['duplicates']){
                        $gwpar['unit_price'] = $voucher->voucherValue;
                        $gwpar['string_udf1'] = $this->_readVchDim($voucher->voucherType); // ($voucher->voucherType=='eVoucher' || $voucher->voucherType=='eCapitaVoucher') ? 'E-CAPITALAND' : 'CAPITALAND';
                    }
                    $this->save_gwlot($gwpar);

                    $gwmark['lot_id'] = $voucher->voucherNumber;
                    $gwmark['lot_id2'] = $trans_id;
                    $gwmark['string_udf3'] = 'USED-OTHERRTXN';
                    $gwmark['trans_id'] = $trans_id;
                    $this->mark_gwlot($gwmark);

                    $validatedamt += floatval($voucher->voucherValue);
                    $validatedvch[] = array(
                        "voucher" => $voucher->voucherNumber,
                        "amount" => $voucher->voucherValue,
                        "remark" => $voucher->voucherName
                    );

                }

                $validated = array(
                    "OKAMT"=> $validatedamt,
                    "VCHLOG"=> $validatedvch
                );


            }
            else {
                foreach ($vouchers as $voucher) {
                    $gwpar = array();
                    $gwpar['duplicates'] = in_array($voucher, $vouchers_dupstat);
                    $gwpar['loc_id'] = $loc_id;
                    $gwpar['trans_id'] = $trans_id;
                    $gwpar['lot_id'] = $voucher;
                    $gwpar['string_udf2'] = $burn->message . ' ' . $burn->errorCode;
                    $gwpar['string_udf3'] = 'ERROR-UTILIZE';
                    $this->save_gwlot($gwpar);
                }

                $validated = array(
                    "OKAMT"=> 0,
                    "VCHLOG"=> $burn->message
                );
            }
        }
        else {
            foreach ($vouchers as $voucher) {
                // Burn one
                $qacct['merchantUserId'] = $token['id'];
                $qacct['voucherNumber'] = $voucher;
                $qacct['requestTimestamp'] = (int)time() . '000';
                $qacct['signature'] = hash('sha256', $qacct['merchantUserId'] . $qacct['voucherNumber'] . $token['token'] . $qacct['requestTimestamp']);
                $burn = $this->_doCurl('merchant/voucher/v1/burn', $token['token'], $qacct, $logging);

                if ($burn->status == "Success") {
                    $gwpar = array();
                    $gwpar['duplicates'] = in_array($qacct['voucherNumber'], $vouchers_dupstat);
                    $gwpar['loc_id'] = $loc_id;
                    $gwpar['trans_id'] = $trans_id;
                    $gwpar['lot_id'] = $qacct['voucherNumber'];
                    $gwpar['string_udf3'] = 'UTILIZED';
                    $gwpar['datetime_udf'] = date('Y-m-d H:i:s');
                    if (!$gwpar['duplicates']){
                        $gwpar['unit_price'] = $burn->voucherValue;
                        $gwpar['string_udf1'] = $this->_readVchDim($burn->voucherType); // ($burn->voucherType=='eVoucher' || $burn->voucherType=='eCapitaVoucher') ? 'E-CAPITALAND' : 'CAPITALAND';
                    }
                    $this->save_gwlot($gwpar);

                    $gwmark['lot_id'] = $qacct['voucherNumber'];
                    $gwmark['lot_id2'] = $trans_id;
                    $gwmark['string_udf3'] = 'USED-OTHERRTXN';
                    $gwmark['trans_id'] = $trans_id;
                    $this->mark_gwlot($gwmark);

                    $validated["OKAMT"] += floatval($burn->voucherValue);
                    $validated["VCHLOG"][] = array(
                        "voucher" => $qacct['voucherNumber'],
                        "amount" => $burn->voucherValue,
                        "remark" => $burn->voucherName
                    );

                } else {
                    $gwpar = array();
                    $gwpar['duplicates'] = in_array($qacct['voucherNumber'], $vouchers_dupstat);
                    $gwpar['loc_id'] = $loc_id;
                    $gwpar['trans_id'] = $trans_id;
                    $gwpar['lot_id'] = $qacct['voucherNumber'];
                    $gwpar['string_udf2'] = $burn->message . ' ' . $burn->errorCode;
                    $gwpar['string_udf3'] = 'ERROR-UTILIZE';
                    $this->save_gwlot($gwpar);

                    $validated = array(
                        "OKAMT" => 0,
                        "VCHLOG" => $burn->message
                    );
                }
            }
        }

        if (count($vouchers)>0 && $validated["OKAMT"]>0){
            // Voucher burnt
            $rtn = array(
                "code" => 1,
                "result" => "SUCCESS",
                "mode" => "PAY",
                "trans_id" => $trans_id,
                "payment_id" => $this->gatewaycode,
                "payment_amount" => $validated["OKAMT"]
            );
        }
        else {

            if (count($vouchers)==1) {
                // CHPOS2.0 Play Cheat - If only 1 voucher, go into b2b_gateway_lot to retrieve amount if its there
                $sql = "select trans_id,lot_id,unit_price,gateway_meta from b2b_gateway_lot where trans_id='$trans_id' and lot_id='".$vouchers[0]."' and cast(datetime_udf as date)=cast(getdate() as date); ";
                $retrieve = $this->db->query($sql)->result_array();
                if (count($retrieve)>0) {
                    $rtn = array(
                        "code" => 1,
                        "result" => "SUCCESS",
                        "mode" => "PAY",
                        "trans_id" => $trans_id,
                        "payment_id" => 'RETREIVED',
                        "payment_amount" => $retrieve[0]['unit_price']
                    );
                }
                else {
                    // Invalid
                    $rtn = array(
                        "code" => -1,
                        "result" => "RETREIVE FAILED",
                        "mode" => "PAY",
                        "trans_id" => $trans_id,
                        "error" => $validated["VCHLOG"]
                    );
                }
            }
            else {
                // Invalid
                $rtn = array(
                    "code" => -1,
                    "result" => "VOUCHER PAYMENT FAILED",
                    "mode" => "PAY",
                    "trans_id" => $trans_id,
                    "error" => $validated["VCHLOG"]
                );
            }


        }

        return $rtn;
    }

    public function auth_token($loc_id,$pos_id){

        if ($this->token!='') {
            return $this->token;
        }

        // Full model - Either reply the valid token or refresh+save+reply new token
        $set_code_acct='CAPITASTAR_ACCT'; $set_code_token='CAPITASTAR_TOKEN'; $token_validity='-30 days'; $valid_token=[];
        $loc_id = str_replace('-R','',$loc_id);

        $sql = "SELECT * FROM b2b_setting_list
                    WHERE sys_id='API' AND set_code IN ('$set_code_acct','$set_code_token') AND set_default='$loc_id' ORDER BY set_code DESC ";
        $b2b_settings = $this->db->query($sql)->result_array();
        foreach ($b2b_settings as $b2bset) {
            if (trim($b2bset['set_code'])==$set_code_token) {
                $b2btoken = $b2bset;
            }
            else if (trim($b2bset['set_code'])==$set_code_acct) {
                $b2bacct = $b2bset;
            }
        }

        $token_auth_on = date('Y-m-d H:i:s');
        if (strtotime($b2btoken['modified_on']) > strtotime($token_validity)) {
            // Token Valid
            $valid_token['id'] = $b2btoken['set_desc'];
            $valid_token['token'] = $b2btoken['remarks'];
            $token_auth_on = date('Y-m-d H:i:s',strtotime($b2btoken['modified_on']));
        }
        else {
            // Refresh & Save
            $logging = array("trans_id"=>$loc_id.$pos_id.'-'.rand(10,99), "loc_id"=>$loc_id, "pos_id"=>$pos_id);
            $acct = (array) json_decode(base64_decode($b2bacct['remarks']));
            $qacct['email'] = $acct['email'];
            $qacct['password'] = $acct['password'];
            $qacct['requestTimestamp'] = (int) time() .'000';
            $qacct['signature'] = hash('sha256', $qacct['email'].$qacct['password'].''.$qacct['requestTimestamp'] );
            $refresh = $this->_doCurl('partner/auth/v1/login','',$qacct,$logging);


            $valid_token['id'] = $refresh->loginUser->id;
            $valid_token['token'] = $refresh->token;

            if (strlen($valid_token['token'])>32) {
                if (isset($b2btoken)) {
                    $sql = "UPDATE b2b_setting_list
                        SET set_desc='" . $valid_token['id'] . "', remarks='" . $valid_token['token'] . "', modified_by='" . getRealIpAddr(15) . "', modified_on=GETDATE()
                        WHERE sys_id='API' AND set_code='$set_code_token' AND set_default='$loc_id' ";
                    $rtnx = $this->db->query($sql);
                } else {
                    $sql = "INSERT INTO b2b_setting_list (sys_id,set_code,set_default,set_desc,remarks,created_by,created_on,modified_by,modified_on)
                      VALUES ('API','$set_code_token','$loc_id','" . $valid_token['id'] . "','" . $valid_token['token'] . "', '" . getRealIpAddr(15) . "',GETDATE(),'" . getRealIpAddr(15) . "',GETDATE() );";
                    $rtnx = $this->db->query($sql);
                }
                $b2btoken['modified_on'] = time();
            }
            else {
                // Cannot auth. Send email to MIS
                $msg = 'CAPITASTAR API - AUTH LOGIN';
                $msg.= '<br>Fail to login to CAPITASTAR MERCHANT ACCOUNT with '.$acct['email'].'.<br>Please go to https://chvoices.challenger.sg/cherps/page?view=capitastar to save your password to the system.';
                $msg.= '<br><br>'.json_encode($refresh);
                $msgemail = $this->notification;
                if (ENVIRONMENT=="production") $msgemail.= $loc_id.'.ic@challenger.sg; ';
                $this->cherps_email('Capitastar API Auth - Fail refresh token '.$loc_id, $msg, $msgemail);
            }

        }

        $this->gatewayuser = array("loc_id"=>$loc_id,"pos_id"=>$pos_id,"cstar_user_id"=>$valid_token['id'],"cstar_auth_on"=>$token_auth_on);
        $this->token = $valid_token;
        return $valid_token;
    }

    public function save_gwlot($gwpar){

        $gateway_meta = $this->gatewayuser;
        $gateway_meta['last_gateway_code'] = $this->gatewaycode;

        $gwpar['lot_id2'] = (isset($gwpar['lot_id2'])) ? $gwpar['lot_id2'] : '';
        $gwpar['unit_price'] = (isset($gwpar['unit_price'])) ? $gwpar['unit_price'] : 0;
        $gwpar['string_udf1'] = (isset($gwpar['string_udf1'])) ? $gwpar['string_udf1'] : '';
        $gwpar['string_udf2'] = (isset($gwpar['string_udf2'])) ? $gwpar['string_udf2'] : '';
        $gwpar['string_udf3'] = (isset($gwpar['string_udf3'])) ? $gwpar['string_udf3'] : '';
        $gwpar['datetime_udf'] = (isset($gwpar['datetime_udf'])) ? $gwpar['datetime_udf'] : '';
        if ($gwpar['duplicates']){
            // Update
            $sql = "UPDATE b2b_gateway_lot SET ";
            $sql.= ($gwpar['lot_id2']!='') ? " lot_id2='".$gwpar['lot_id2']."', " : "" ;
            $sql.= ($gwpar['unit_price']>0) ? " unit_price='".$gwpar['unit_price']."', " : "" ;
            $sql.= ($gwpar['string_udf1']!='') ? " string_udf1='".$gwpar['string_udf1']."', " : "" ;
            $sql.= ($gwpar['string_udf2']!='') ? " string_udf2='".$gwpar['string_udf2']."', " : "" ;
            $sql.= ($gwpar['string_udf3']=='-') ? " string_udf3='', " : ( ($gwpar['string_udf3']!='') ? " string_udf3='".$gwpar['string_udf3']."', " : "" ) ;
            $sql.= ($gwpar['datetime_udf']!='') ? " datetime_udf='".$gwpar['datetime_udf']."', " : "" ;
            $sql.= " gateway_meta='". json_encode($gateway_meta) ."', gateway_num=gateway_num+1, modified_by='".getRealIpAddr(15)."',modified_on=GETDATE() ";
            $sql.= " WHERE coy_id='CTL' and gateway_id='CAPITASTAR' and trans_id='".$gwpar['trans_id']."' and lot_id='".$gwpar['lot_id']."' ";
            $rtnx = $this->db->query($sql);
        }
        else {
            // Insert
            $gwpar['string_udf3'] = ($gwpar['string_udf3']=='-') ? '' : $gwpar['string_udf3'] ;
            $sql = "INSERT INTO b2b_gateway_lot (coy_id,gateway_id,trans_id,loc_id,lot_id,lot_id2,unit_price,string_udf1,string_udf2,string_udf3,datetime_udf,gateway_meta,gateway_num,created_by,created_on,modified_by,modified_on)
                            VALUES ('CTL','CAPITASTAR','".$gwpar['trans_id']."', '".$gwpar['loc_id']."', '".$gwpar['lot_id']."','".$gwpar['lot_id2']."' , '".$gwpar['unit_price']."' , 
                            '".$gwpar['string_udf1']."','". $gwpar['string_udf2'] ."',LEFT('".$gwpar['string_udf3']."',255),'".$gwpar['datetime_udf']."' ,
                            '". json_encode($gateway_meta) ."' ,1,'".getRealIpAddr(15)."',GETDATE(),'".getRealIpAddr(15)."',GETDATE())";
            $rtnx = $this->db->query($sql);
        }
    }

    public function mark_gwlot($gwpar) {
        // Mark same voucher in other Tx
        $gwpar['lot_id2'] = (isset($gwpar['lot_id2'])) ? $gwpar['lot_id2'] : '';
        $gwpar['string_udf1'] = (isset($gwpar['string_udf1'])) ? $gwpar['string_udf1'] : '';
        $gwpar['string_udf2'] = (isset($gwpar['string_udf2'])) ? $gwpar['string_udf2'] : '';
        $gwpar['string_udf3'] = (isset($gwpar['string_udf3'])) ? $gwpar['string_udf3'] : '';

        $sql = "UPDATE b2b_gateway_lot SET ";
        $sql.= ($gwpar['lot_id2']!='') ? " lot_id2='".$gwpar['lot_id2']."', " : "" ;
        $sql.= ($gwpar['string_udf1']!='') ? " string_udf1='".$gwpar['string_udf1']."', " : "" ;
        $sql.= ($gwpar['string_udf2']!='') ? " string_udf2='".$gwpar['string_udf2']."', " : "" ;
        $sql.= ($gwpar['string_udf3']!='') ? " string_udf3='".$gwpar['string_udf3']."', " : "" ;
        $sql.= " modified_by='".getRealIpAddr(15)."',modified_on=GETDATE() ";
        $sql.= " WHERE coy_id='CTL' and gateway_id='CAPITASTAR' and trans_id<>'".$gwpar['trans_id']."' and lot_id='".$gwpar['lot_id']."' ";
        $rtnx = $this->db->query($sql);
    }

    public function get_gwlot($trans_id,$vouchers, $return_column=''){
        $sql_vch = "'" . implode("','",$vouchers) . "'";
        $sql = "SELECT rtrim(lot_id) lot_id,unit_price,string_udf1,string_udf2,string_udf3 from b2b_gateway_lot WHERE trans_id='$trans_id' AND lot_id in ($sql_vch) ";
        $rtnx = $this->db->query($sql)->result_array();
        if (in_array($return_column,['lot_id','unit_price','string_udf1','string_udf2','string_udf3'])) {
            $rtnsingle = array();
            foreach ($rtnx as $rtn) {
                $rtnsingle[] = $rtn[$return_column];
            }
            return $rtnsingle;
        }
        else if ($return_column=='SUMMARY') {
            $rtnsingle = array(); $unit_price=0; $string_udf1=$rtnx[0]['string_udf1'];
            foreach ($rtnx as $rtn) {
                $rtnsingle[] = $rtn['lot_id'];
                $unit_price += $rtn['unit_price'];
                if ($rtn['string_udf1']!=$string_udf1)
                    $string_udf1 = 'MIXED';
            }
            return array("string_udf1"=>$string_udf1,"unit_price"=>$unit_price,"lot_id"=>$rtnsingle);
        }
        else {
            return $rtnx;
        }
    }

    public function save_roadshow($lot_id){
        $sql = "SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; EXEC sp_b2b_gateway_lot_rs '$lot_id'; ";
        $rtnx = $this->db->query($sql);
    }

    public function save_gwmsg($gwpar){
        $sql = "INSERT INTO b2b_gateway_message
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level,return_status,return_msg,created_by,created_on,modified_by,modified_on)
                        VALUES ('CTL','CAPITASTAR','".$this->gatewaycode."','".$gwpar['trans_id']."','".$gwpar['loc_id']."','".$gwpar['pos_id']."' ,
                        '".$gwpar['request_link']."','".$gwpar['request_msg']."','". $gwpar['status_level'] ."','".$gwpar['return_status']."','".$gwpar['return_msg']."' ,
                        '".getRealIpAddr(15)."','".$gwpar['created_on']."','".getRealIpAddr(15)."','".$gwpar['modified_on']."')";
        $rtnx = $this->db->query($sql);
    }

    private function _getVouchers($str=''){

        if ($str!='' && strlen($str)>3) {
            $vouchers_str = urldecode($str);
            $vouchers = explode(';',$vouchers_str);
        }
        else {
            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody);
            $vouchers = $postarr->voucher;
        }

        $uppVch = array();
        if (count($vouchers)>0) {
            $vouchers = array_filter($vouchers);
            foreach ($vouchers as $vch) {
                $uppVch[] = (substr($vch,0,3)=='ECV') ? $vch : substr(strtoupper($vch),0,45);
            }
        }
        return $uppVch;
    }

    private function _readVchDim($str){
        if ($str=='eVoucher')
            return 'C-CAPITALAND'; // E-deals
        else if ($str=='eCapitaVoucher')
            return 'E-CAPITALAND'; // Capitavoucher
        else
            return 'CAPITALAND'; // Physical Capitavoucher
    }

    private function _readVchDeal($str){
        /*
        if ($str=='$10 Challenger e-Voucher')
            return 10;
        else if ($str=='$5 Challenger e-Voucher')
            return 5;
        else
            return 1;
        */
        preg_match_all('!\d+!', $str, $no);
        return $no[0][0];
    }

    private function _doCurl($apiuri,$token='', $query,$logging) {
        $created_on = time();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Ocp-Apim-Subscription-Key: " . $this->subkey;
        if ($token!='') {
            $headers[] = "Token: " . $token;
        }
        $queryjson =  json_encode($query);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiurl.$apiuri);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $queryjson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);

        if ($this->input->get('debug')==1) {
//            print_r($this->apiurl.$apiuri); echo "<hr>";
//            print_r($headers); echo "<hr>";
//            print_r($queryjson); echo "<hr>";
            print_r($output); exit;
        }

        $outputarr = json_decode($output);
        $this->gatewaycode = md5($logging['trans_id'].time().rand(1000,9999).rand(1000,9999).rand(1000,9999));
        $gwpar['created_on'] = date('Y-m-d H:i:s',$created_on);
        $gwpar['modified_on'] = date('Y-m-d H:i:s');
        $gwpar['trans_id'] = $logging['trans_id'];
        $gwpar['loc_id'] = $logging['loc_id'];
        $gwpar['pos_id'] = $logging['pos_id'];
        $gwpar['request_link'] = $this->apiurl.$apiuri;
        $gwpar['request_msg'] = $queryjson;
        $gwpar['status_level'] = isset($outputarr->errorCode) ? $outputarr->errorCode : '200';
        $gwpar['return_status'] = isset($outputarr->errorCode) ? $outputarr->message : 'Success';
        $gwpar['return_msg'] = $output;
        $this->save_gwmsg($gwpar);

        return $outputarr;
    }

//    public function get_gwlot($trans_id,$opt=''){
//        $sql = "SELECT TOP 1 batch_num FROM b2b_gateway_lot WHERE trans_id='$trans_id' ORDER BY batch_num DESC";
//        $rtnx = $this->db->query($sql)->row_array();
//        $new_batch = intval($rtnx['batch_num']) +1;
//        return ($rtnx) ? $new_batch : 1;
//    }

    public function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {

        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = '';
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }

}

?>
