<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Dfs_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
        ini_set('display_errors', 1);
        $this->coy_id = 'CTL';
        $this->blacklistemail = ['hq@challenger.sg'];
    }

    private function upload_image($file, $name) {
        $prefix = $this->session->userdata('cm_supp_id');
        $upload = $this->_upload($file, $name, "assets/uploads/", "*", $prefix . '_');
        return $upload;
    }

    private function copyfile($old_path, $new_path) {
        $upload = $this->_CopyFileServer($old_path,$new_path);
        return $upload;
    }

    private function createfile($upload, $content, $overwrite=false) {
        $handle_path = FCPATH . 'assets/uploads/';
        if ($overwrite)
            file_put_contents($handle_path.$upload, $content, LOCK_EX);
        else
            file_put_contents($handle_path.$upload, $content, FILE_APPEND | LOCK_EX);
        return $handle_path.$upload;
    }

    public function index($mode){

        if ($mode=='create') {

            $postbody = trim(file_get_contents('php://input'));
            $postarr = json_decode($postbody,1);

            $filename = $postarr['filename'];
            $content  = $postarr['content'];
            $target_path  = $postarr['path'];

            return $this->createAndUpload($target_path,$filename,$content);

        }
        else {
            // Upload file
            return [
                'code'=>0,
                'msg'=>'Fail upload',
                'error'=>'Function error'
            ];
        }

    }

    public function createAndUpload($target_path,$filename,$content) {
        $uploadpath = $this->createfile($filename, $content);
        $new_directory =  FILE_SERVER . $target_path;
        if ( $this->copyfile($uploadpath, $new_directory.$filename) ){
            unlink($uploadpath);
            return [
                'code'=>1,
                'msg'=>'File uploaded - '.$filename,
                'path'=>base_url("login/displayfile/pdf/".base64_encode($new_directory.$filename))
            ];
        }
        else {
            exec( "sudo cp ".$uploadpath." ".$new_directory.$filename . ' 2>&1', $output);

            return [
                'code'=>0,
                'msg'=>'Fail upload',
                'error'=>$output
            ];
        }
    }
}

?>
