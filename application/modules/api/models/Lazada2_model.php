<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Lazada2_model extends Base_Common_Model {
    // LAZADA2
    private $apiresturl;
    private $apikey;
    private $apisecret;
    private $lazacct;

    public function __construct() {

        $this->db = $this->load->database('cherps_hachi', TRUE);
        ini_set('display_errors', 1);
        parent::__construct();

        if (strtoupper($this->input->get('acct'))=='FITBIT') {
            // Fitbit Official
            $this->lazacct = 'FITBIT';
        }
        else {
            // Challenger store
            $this->lazacct = 'CTL';
        }

        // LAZADA2
        include "application/libraries/LazopClient.php";
        include "application/libraries/LazopRequest.php";
        
        $this->apiresturl = 'https://api.lazada.sg/rest';
        $this->apikey = '102435';
        $this->apisecret = 'VijtQlbx2RYRy1lMZsxcUNxmxRCsFt4A';
    }

    public function openplatform_token($old_token=''){
        // Auth URL: https://auth.lazada.com/oauth/authorize?response_type=code&redirect_uri=https://chvoices-test.challenger.sg/api/lazada/login&client_id=102125
        if ($old_token=='' && !($this->input->get("code"))) {
            // Redirect for login
            redirect('https://auth.lazada.com/oauth/authorize?response_type=code&redirect_uri='.base_url('api/lazada/login%3Facct%3D'.$this->lazacct).'&client_id='.$this->apikey);
            exit;
        }

        // Call to generate token
        $apiresturl = 'https://auth.lazada.com/rest';

        $c = new LazopClient($apiresturl, $this->apikey, $this->apisecret);
        if ($this->input->get("code")) {
            // Get new token
            $action = 'Generate new token';
            $uri = '/auth/token/create';
            $code = $this->input->get("code");
            $request = new LazopRequest($uri);
            $request->addApiParam('code', $code);
        }
        else {
            // Refresh token
            $action = 'Refresh token';
            $code = $old_token;
            $uri = '/auth/token/refresh';
            $request = new LazopRequest($uri);
            $request->addApiParam('refresh_token',$code);
        }
        $response = json_decode($c->execute($request));
        $token = $response->access_token;
        $expires = $response->expires_in + time();

        if ( strlen($token)<5 ){
            $this->cherps_email('Lazada '.$this->lazacct.' - Fail to Auth','API has failed to Authenticate.<br>Please login your account at https://chvoices.challenger.sg/api/lazada/orders?acct='.$this->lazacct,"dongwei@challenger.sg; yongsheng@challenger.sg;");
            redirect('https://auth.lazada.com/oauth/authorize?response_type=code&redirect_uri='.base_url('api/lazada/login%3Facct%3D'.$this->lazacct).'&client_id='.$this->apikey);
            exit;
        }

        $sql = "INSERT INTO b2b_gateway_message 
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id, request_link,request_msg,status_level, return_status,return_msg,created_by,created_on,modified_on)
                        VALUES ('CTL','LAZADA2','".md5($code.time())."','".$this->lazacct."','','' ,
                            '".$apiresturl.$laz2apiUri."','".print_r($request,true)."','1',
                            '".$token."','".json_encode($response)."','".getRealIpAddr(15)."',GETDATE(),'".date('Y-m-d H:i:s',$expires)."')";
        $this->db->query($sql);

        return array("action"=>$action,"code"=>$code,"access_token"=>$response->access_token,"expiry_date"=>date('Y-m-d H:i:s',$expires),"refresh_token"=>$response->refresh_token);
    }

    public function openplatform_gettoken(){
        $sql = "SELECT TOP 1 return_status access_token,modified_on expiry_date,return_msg 
                  FROM b2b_gateway_message WHERE coy_id='CTL' and gateway_id='LAZADA2' and trans_id='".$this->lazacct."' and return_status<>'' ORDER BY created_on DESC";
        $result = $this->db->query($sql)->row_array();

        $expires = strtotime($result['expiry_date']);
        if ( strtotime('+2 days') > $expires ) {
            $response = json_decode($result['return_msg']);
            $newresult = $this->openplatform_token($response->refresh_token);
            return $newresult['access_token'];
        }
        else {
            return $result['access_token'];
        }
    }

    public function cherps_email($subject,$message,$email='yongsheng@challenger.sg',$format='HTML') {
        $sql = "exec sp_SysSendDBmailFormat 
                        @from_address = 'chvoices@challenger.sg', 
                        @recipients = '".$email."', 
                        @copy_recipients = '', 
                        @subject = '$subject', 
                        @body_format = '$format' , 
                        @body = '".$message."', 
                        @file_attachments = ''";
        $sendmail = CI::db()->query($sql);
    }

    private function trimItemSku($itemsku){
        $itemsku = trim($itemsku);
        if (strlen($itemsku)==16 && strpos('-'.$itemsku , 'CTL')==1) {
            // Remove first 3 letters (CTL)
            $itemsku = substr($itemsku,3);
        } else if (strlen($itemsku)<13) {
            // Add leading zero
            $itemsku = str_pad($itemsku, 13, '0', STR_PAD_LEFT);
        } else if (strpos('Â',$itemsku)>0) {
            // Replace unknown char
            $itemsku = str_replace('Â','',$itemsku);
        }
        return trim($itemsku);
    }

    /****
     * Orders Sync
     */

    public function import_orders(){

        $access_token = $this->openplatform_gettoken();
        if ($this->input->get('date')){
            $datei = $this->input->get('date');
            $datestamp = date('Y-m-d\TH:i:00+0800',strtotime($datei)) ;//. 'T00:00:00+0800';
        }
        else {
            $date = new DateTime();
            $date->add(DateInterval::createFromDateString('-24 hour'));
            $datestamp = $date->format(DateTime::ISO8601);

            // Get last date from b2b_lazada_order_list
            $sql = "select top 1 order_date from b2b_lazada_order_list order by order_date desc,order_id desc";
            $dbt = $this->db->query($sql)->row_array();
            $datestamp = date('Y-m-d\TH:i:00+0800', (strtotime($dbt['order_date'])-600) ) ;//. 'T00:00:00+0800';
        }

        $c = new LazopClient($this->apiresturl, $this->apikey, $this->apisecret);
        $request = new LazopRequest('/orders/get','GET');
        $request->addApiParam('limit','100');
        $request->addApiParam('created_after',$datestamp);
        $data = json_decode($c->execute($request, $access_token));

        if ($this->input->get('debug')) {
            print_r( array("datestamp"=>$datestamp, "response"=>$data) );
            exit;
        }

        foreach ($data->data->orders as $order) {
            $captured = array();
            $captured['laz_account'] = $this->lazacct;
            $captured['order_id'] = trim($order->order_id);
            $captured['order_no'] = $order->order_number;
            $captured['order_date'] = date('Y-m-d H:i',strtotime($order->created_at));
            $neworder = $this->b2b_import_order_list($captured, "b2b_lazada_order_list");

            if ($neworder==TRUE) {
                $c = new LazopClient($this->apiresturl, $this->apikey, $this->apisecret);
                $request = new LazopRequest('/order/items/get', 'GET');
                $request->addApiParam('order_id', $captured['order_id']);
                $dataitems = json_decode($c->execute($request, $access_token));

                $params['items'] = $dataitems->data;
                foreach ($dataitems->data as $orderitem) {
                    $captureditem = array();
                    $captureditem["order_id"] = $orderitem->order_id;
                    $captureditem["item_sku"] = $this->trimItemSku($orderitem->sku);
                    $captureditem["item_price"] = floatval(str_replace(',', '', $orderitem->item_price));
                    $captureditem["created_at"] = date('Y-m-d', strtotime($orderitem->created_at));
                    $captureditem["updated_at"] = date('Y-m-d H:i:s');

                    $captureditem["item_id"] = $orderitem->order_item_id;
                    $captureditem["shop_id"] = $orderitem->shop_id;
                    $captureditem["item_name"] = $orderitem->name;
                    $captureditem["shop_sku"] = $orderitem->shop_sku;
                    $captureditem["shipping_type"] = $orderitem->shipping_type;
                    $captureditem["paid_price"] = $orderitem->paid_price;
                    $captureditem["curr_id"] = $orderitem->currency;
                    $captureditem["item_ref_url"] = $orderitem->product_detail_url;
                    $this->b2b_import_order_item($captureditem, "b2b_lazada_order_item");
                }
            }
        }

        // Exec o2o_sp_import_exorder
        if (ENVIRONMENT=="production") {
            $this->db->query("SET ANSI_NULLS ON;");
            $this->db->query("SET ANSI_WARNINGS ON;");

            $sql = "EXEC sp_b2b_lazada_import;";
            $result = $this->db->query($sql)->result_array();
            $result[0]['acct'] = $this->lazacct;

            $this->pgdb = $this->load->database('pg92', TRUE);
            foreach($result as $res) {
                $this_id = substr($res['msg'], -15);
                $sql = "SELECT * FROM sms_invoice_list where invoice_id='$this_id' ";
                $result_array = $this->db->query($sql)->result_array();
                $this->pgdb->insert('sms_invoice_list', $result_array);

                $sql = "SELECT * FROM sms_invoice_item where invoice_id='$this_id' ";
                $result_array = $this->db->query($sql)->result_array();
                $this->pgdb->insert('sms_invoice_item', $result_array);
            }
        }
        else {
            $result[0]['acct'] = $this->lazacct;
        }

        return $result;
    }

    private function b2b_import_order_list($params, $table){
        $sql = "SELECT * FROM $table WHERE order_id= '".$params['order_id']."' ";
        $query = $this->db->query($sql);
        if($query !== FALSE && $query->num_rows() > 0){
            return FALSE;
        }
        else {
            $params = (object) $params;
            $result = $this->db->insert($table, $params);
            return TRUE;
        }
    }

    private function b2b_import_order_item($params, $table){
        $params = (object) $params;
        $result = $this->db->insert($table, $params);
        return $result;
    }

    /****
     *  PRODUCT SYNC
     *  https://internal.challenger.sg/api/lazada/products_qty?acct=ctl&trial=1
     */

    public function upd_products_qty(){

        $products = array();
        $offset=0;$limit=100;

        do {
            $access_token = $this->openplatform_gettoken();

            $c = new LazopClient($this->apiresturl, $this->apikey, $this->apisecret);
            $request = new LazopRequest('/products/get','GET');
            $request->addApiParam('filter','live');
            $request->addApiParam('limit',$limit);
            $request->addApiParam('offset',$offset);
            $data = json_decode($c->execute($request, $access_token));
            $datacount = $data->data->total_products;

            foreach ( $data->data->products as $datalist) {
                $dataitemname = $datalist->attributes->name;
                foreach ($datalist->skus as $dataitem) {
                    $dataitemid = $this->trimItemSku($dataitem->SellerSku);
                    $product = array(
                        "item_id" => $dataitemid,
                        "seller_sku" => $dataitem->SellerSku,
                        "shop_sku" => $dataitem->ShopSku,
                        "name" => $dataitemname,
                        "quantity" => $dataitem->quantity,
                        "price" => $dataitem->price,
                        "special_price" => $dataitem->special_price, //special_to_time
                        "available" => $dataitem->Available,
                        "status" => $dataitem->Status,
                        "url" => $dataitem->Url,
                        "dbqty" => -1,
                        "dbsync" => -1
                    );
                    $this->b2b_import_products($product);
                    $products[$dataitemid] = $product;
                }
            }
            $offset = $offset + $limit;
        } while ( $datacount > count($products) );

        // Get all the product info from DB and match into $products
        $dbproducts = $this->b2b_get_products();
        foreach ($dbproducts as $dbprod){
            $dbproditemid = $dbprod['seller_sku'];
            if (array_key_exists($dbproditemid,$products)) {
                $products[$dbproditemid]['dbqty'] = $dbprod['sync_qty'];
                $products[$dbproditemid]['dbsync'] = ($dbprod['available'] != $dbprod['sync_qty']) ? 1 : 0;
            }
        }

        // Scan through $products to find items to update
        $error_products = array();
        $sync_products = array();
        $sync_xml = '';
        foreach ($products as $prod){
            if ($prod['dbsync']==-1) {
                // Item not synced in DB
                $error_products[] = array("seller_sku" => $prod['seller_sku'], "name" => $prod['name']);
            }
            else if ($prod['dbsync']==1) {
                // Prepare to sync items
                $sync_products[] = array("seller_sku" => $prod['seller_sku'], "name" => $prod['name'], 'old_qty' => $prod['available'], 'new_qty' => $prod['dbqty'] );
            }
        }

        $trial = ($this->input->get('trial')) ? true : false;

        // Update to Lazada
        $success_products = [];
        if (!$trial && count($sync_products)>0) {
            $payload = '<?xml version="1.0" encoding="UTF-8"?><Request><Product><Skus>';
            foreach ($sync_products as $sync_prod) {
                if (count($success_products)<50) {
                    $success_products[] = $sync_prod['seller_sku'];
                    $payload .= '<Sku><SellerSku>' . $sync_prod['seller_sku'] . '</SellerSku><Quantity>' . $sync_prod['new_qty'] . '</Quantity><Price/><SalePrice/><SaleStartDate/><SaleEndDate/></Sku>';
                }
            }
            $payload .= '</Skus></Product></Request>';

            if (ENVIRONMENT == "production") {
                $c = new LazopClient($this->apiresturl, $this->apikey, $this->apisecret);
                $request = new LazopRequest('/product/price_quantity/update', 'POST');
                $request->addApiParam('payload', $payload);
                $upddata = $c->execute($request, $access_token) ;
            }
        }

        // Email results
        if (count($sync_products)>0 || count($error_products)>0) {
            $emhtml = '<h3>Lazada Product Sync - '.$this->lazacct.' on '.date('Y-m-d').'</h3>';
            $emhtml.= 'Following items were updated:<br><br><table><thead><td>SKU</td><td>NAME</td><td>OLD QTY</td><td>NEW QTY</td></thead>';
            foreach ($sync_products as $prod) {
                if (in_array($prod['seller_sku'],$success_products))
                    $emhtml .= '<tr><td>' . $prod['seller_sku'] . '</td><td>' . $prod['name'] . '</td><td>' . $prod['old_qty'] . '</td><td>' . $prod['new_qty'] . '</td><td>Synced</td></td></tr>';
                else
                    $emhtml .= '<tr><td>' . $prod['seller_sku'] . '</td><td>' . $prod['name'] . '</td><td>' . $prod['old_qty'] . '</td><td>' . $prod['new_qty'] . '</td><td>Overlimit</td></tr>';
            }
            foreach ($error_products as $prod) {
                $emhtml .= '<tr><td>' . $prod['seller_sku'] . '</td><td>' . $prod['name'] . '</td><td colspan="2"></td><td></td><td>Failed</td></tr>';
            }
            $emhtml.= '</table>';
            $emhtml.= '<hr>' . $upddata ;

            if (!$trial) {
                //$emrecv = 'yongsheng@challenger.sg;';
                $emrecv = 'th.wong@challenger.sg; dongwei@challenger.sg; yongsheng@challenger.sg;';
                $this->cherps_email('Lazada ' . $this->lazacct . ' Products Sync', $emhtml,$emrecv, 'HTML');

                return json_decode($upddata);
            }
            else {
                echo "SYNC<pre>" . print_r($sync_products,true) . "</pre>";
                echo "SUCCESS<pre>" . print_r($success_products,true) . "</pre>";
                echo "ERROR<pre>" . print_r($error_products,true) . "</pre>";
                echo $emhtml;
                exit;
            }
        }
    }

    private function b2b_import_products($params){
        $sql_inv_loc = "'HCL'";
        $sql = "SELECT * FROM b2b_lazada_products WHERE seller_sku= '".$params['seller_sku']."' ";
        $query = $this->db->query($sql);
        if($query !== FALSE && $query->num_rows() > 0){
            // Update quantity & available
            $sql = "UPDATE b2b_lazada_products
                        SET available='".$params['available']."', quantity='".$params['quantity']."', modified_on=GETDATE(),
                            buffer_qty = ims_item_list.buffer_qty, updated_on=GETDATE(),
                            qty_on_hand = ISNULL((SELECT SUM(qty_on_hand)-SUM(qty_reserved) from ims_inv_physical where loc_id in ($sql_inv_loc) and coy_id=ims_item_list.coy_id and item_id=ims_item_list.item_id group by item_id),0)
                    FROM b2b_lazada_products
                    LEFT JOIN ims_item_list ON ims_item_list.item_id = b2b_lazada_products.item_id
                    WHERE b2b_lazada_products.shop_sku='".$params['shop_sku']."'";
            $query = $this->db->query($sql);
        }
        else {
            // Insert new entry
            $sql = "INSERT INTO b2b_lazada_products (laz_account,shop_sku,seller_sku,name,price,special_price,status,available,quantity,item_id,buffer_qty,qty_on_hand,created_on,modified_on,updated_on)
                    SELECT '".$this->lazacct."','".$params['shop_sku']."','".$params['seller_sku']."','".$params['name']."',
                            '".$params['price']."','".$params['special_price']."','".$params['status']."','".$params['available']."','".$params['quantity']."',
                        item_id,buffer_qty, 
                        ISNULL((SELECT SUM(qty_on_hand)-SUM(qty_reserved) from ims_inv_physical where loc_id in ($sql_inv_loc) and coy_id=ims_item_list.coy_id and item_id=ims_item_list.item_id group by item_id),0) qty_on_hand,
                        GETDATE(),GETDATE(),GETDATE()
                        FROM ims_item_list where item_id='".$params['item_id']."'";
            $query = $this->db->query($sql);
        }
    }

    private function b2b_get_products() {
        $sql = "SELECT shop_sku,seller_sku,name,available,quantity,qty_on_hand,buffer_qty, (CASE WHEN qty_on_hand<0 THEN 0 ELSE qty_on_hand END) sync_qty  
                  FROM b2b_lazada_products WHERE laz_account='".$this->lazacct."'; ";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

}

?>
