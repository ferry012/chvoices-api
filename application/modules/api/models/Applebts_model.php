<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Applebts_model extends Base_Common_Model
{

    private $email_notification;

    public function __construct()
    {
        parent::__construct();

        if (ENVIRONMENT=="production")
            $this->email_notification = 'LOKE.KARFATT@CHALLENGER.SG; yongsheng@challenger.sg;';
        else
            $this->email_notification = 'yongsheng@challenger.sg;';
    }

    private function save_log($id,$email,$content){
        $ins["coy_id"] = 'CTL';
        $ins["gateway_id"] = 'APPLEBTSFILE';
        $ins["loc_id"] = '';
        $ins['trans_id'] = $id;
        $ins['lot_id'] = 'S-'.date('ymdHis');
        $ins['lot_id2'] = $email;
        $ins['unit_price'] = 0.0;
        $ins['string_udf1'] = '';
        $ins['string_udf2'] = '';
        $ins['string_udf3'] = $content;
        $ins['datetime_udf'] = '1900-01-01';
        $ins['gateway_meta'] = '';
        $ins['gateway_num'] = 0;
        $ins["created_by"] = 'CHVOICES';
        $ins["created_on"] = date('Y-m-d H:i:s');
        $ins["modified_by"] = 'CHVOICES';
        $ins["modified_on"] = date('Y-m-d H:i:s');
        $result1 = $this->db->insert('b2b_gateway_lot', $ins);
        return $result1;
    }

    public function update_lot($trans_id,$vch){
        $sql = "UPDATE b2b_gateway_lot SET string_udf1='$vch', datetime_udf='".date('Y-m-d H:i:s')."', modified_on='".date('Y-m-d H:i:s')."'
                    WHERE trans_id='$trans_id' 
                    AND lot_id IN (select TOP 1 lot_id from b2b_gateway_lot where trans_id='$trans_id' order by created_on desc)";
        $res = $this->db->query($sql);
        return $res;
    }

    public function get_lot_details($trans_id,$rtn='lot_id2'){
        if ($rtn=='') {
            $sql = "SELECT TOP 1 trans_id,lot_id2, string_udf1 from b2b_gateway_lot where trans_id='$trans_id' -- and created_on >= cast(GETDATE() as date)
                        ORDER BY created_on desc";
            $res = $this->db->query($sql)->result_array();
            return ($res) ? $res[0] : null;
        }
        else {
            $sql = "SELECT TOP 1 trans_id,lot_id2, string_udf1 from b2b_gateway_lot where trans_id='$trans_id' -- and created_on >= cast(GETDATE() as date) 
                        ORDER BY created_on desc";
            $res = $this->db->query($sql)->result_array();
            return ($res) ? $res[0][$rtn] : null;
        }
    }

    public function create_voucher($id,$itm){
        //1. Get mbr_id,email from id
        $sql = "  select TOP 1 m.mbr_id,l.lot_id2 email_addr, trans_id, string_udf1
                  from b2b_gateway_lot l
                  join crm_member_list m on l.lot_id2=m.email_addr
                  where l.trans_id='$id' and l.string_udf1=''
                  order by l.created_on desc ";
        $res = $this->db->query($sql)->result_array();
        if ($res) {
            $mbr = $res[0]['mbr_id'];
            $vch = $id.'-'.strtoupper($itm);
            $vch_start = date('Y-m-d 00:00:00');
            $vch_end = date('Y-m-d 23:59:00', strtotime('+1 day'));
            $sql = "insert into crm_voucher_list (coy_id,coupon_id,coupon_serialno,promocart_id,mbr_id,trans_id,trans_date,ho_ref,ho_date,issue_date,sale_date,expiry_date,
                        loc_id,receipt_id1,receipt_id2,voucher_amount,redeemed_amount,expired_amount,issue_type,issue_reason,status_level,
                        created_by,created_on,modified_by,modified_on)
                    VALUES ('CTL','!DCS-APPLEBTS','$vch','!DCS-APPLEBTS','$mbr','','$vch_start','','$vch_start','$vch_start','$vch_start', '$vch_end',
                            '','','',0,0,0,'','',0, 'CHVOICES',getdate(),'CHVOICES',getdate()); ";
            $res = $this->db->query($sql);

            return $vch;
        }
        else {
            return null;
        }
    }

    public function claim_voucher($voucher_sn,$receipt_id,$receipt_date) {

        if ($voucher_sn=='') {
            // Check crm_voucher_list for outstanding
        }

        // Get details from b2b_gateway_lot
        $sql = "SELECT TOP 1 trans_id,string_udf3 from b2b_gateway_lot where string_udf1='$voucher_sn' and string_udf2='' ORDER BY created_on desc; ";
        $res = $this->db->query($sql)->result_array();
        if ($res) {
            $id = trim($res[0]['trans_id']);
            $content_body = substr($res[0]['string_udf3'],10);
            $content = '"redeem"' . $content_body . ',"'.$receipt_id.'","'.$receipt_date.'"' ;
            $this->Api_model->logwritegw( array("gateway_id"=>'APPLEBTSFILE',"trans_id"=>$id,"request_link"=>'applebts/claim',"request_msg"=>$content, "return_msg"=>'') );

            $this->load->model('api/Dfs_model');
            $filename = trim($id).'.csv';
            $result = $this->Dfs_model->createAndUpload('pdf/_RPA91/MKT_APPLEBTS/',$filename,$content);

            $call = $this->Api_model->cherps_email($content, 'APPLEBTS '.$id.' - REDEEM-START', $this->email_notification);
            return $result;
        }

        return [
            'code'=>0,
            'msg'=>'No valid voucher found',
        ];
    }

    public function process($mode,$id='',$id2='',$id3='') {

        if ($mode=='claim-voucher') {
            return $this->claim_voucher($id,$id2,$id3);
        }
        else if ($mode=='dropfile') {

            $content = file_get_contents('php://input');
            $contentjson = json_decode($content);
            $content = substr( substr($content,1) ,0,-1); // Remove the array brackets for CSV file

            if (strlen($id) != 10 || substr($id,0,3) != 'AAI' || !is_numeric(substr($id,3)) ) {
                return $result = [
                    'code'=>0,
                    'msg'=>'Not a valid code',
                    'error'=>''
                ];
            }

            $this->Api_model->logwritegw( array("gateway_id"=>'APPLEBTSFILE',"trans_id"=>$id,"request_link"=>'applebts/'.$mode,"request_msg"=>$content, "return_msg"=>$contentjson[4]) );

            // Get last record
            $last_voucher = $this->get_lot_details($id,'');
            //print_r($last_voucher);
            if (!$last_voucher || ($last_voucher && $last_voucher['string_udf1']!='') ) {
                $this->save_log($id,$contentjson[4],$content);

                $this->load->model('api/Dfs_model');
                $filename = trim($id) . '.csv';
                $result = $this->Dfs_model->createAndUpload('pdf/_RPA91/MKT_APPLEBTS/', $filename, $content);

                $call = $this->Api_model->cherps_email($content, 'APPLEBTS ' . $id . ' - Submit', $this->email_notification);
            }
            else {
                $result = [
                    'code'=>0,
                    'msg'=>'You have an outstanding application',
                    'error'=>''
                ];
            }

        }
        else if ($mode=='validate-ok'){
            // Call API to issue voucher

            $content = $id.'/'.$id2.'/'.$id3;
            $this->Api_model->logwritegw( array("gateway_id"=>'APPLEBTSFILE',"trans_id"=>$id,"request_link"=>'applebts/'.$mode,"request_msg"=>$content, "return_msg"=>'') );

            // Save voucher sn into crm_voucher_list
            $vchSn = $this->create_voucher($id,strtoupper($id3));
            $this->update_lot($id,$vchSn);

            // Call API to send edm
            $email_addr = $this->get_lot_details($id,'lot_id2');
            $url = 'https://www.hachi.tech/email/apple_bts_success/'.trim($email_addr).'/'.$vchSn;
            $this->doCurl($url);

            $call = $this->Api_model->cherps_email($content, 'APPLEBTS '.$id.' - Validate-OK', $this->email_notification);
            echo '<html><head><title>APPLEBTS API</title></head><body>Validate OK recorded</body></html>';
            exit;
        }
        else if ($mode=='validate-fail'){
            // Call API to send edm
            $content = $id.'/'.$id2.'/'.$id3;
            $this->Api_model->logwritegw( array("gateway_id"=>'APPLEBTSFILE',"trans_id"=>$id,"request_link"=>'applebts/'.$mode,"request_msg"=>$content, "return_msg"=>'') );

            $this->update_lot($id,'FAIL VALIDATE');

            // Call API to send edm (failed)
            $email_addr = $this->get_lot_details($id,'lot_id2');
            $url = 'https://www.hachi.tech/email/apple_bts_fail/'.trim($email_addr);
            $this->doCurl($url);

            $call = $this->Api_model->cherps_email($content, 'APPLEBTS '.$id.' - Validate-FAIL', $this->email_notification);
            echo '<html><head><title>APPLEBTS API</title></head><body>Validate fail recorded</body></html>';
            exit;
        }

        else if ($mode=='redeem-ok'){

            $sql = "UPDATE b2b_gateway_lot SET string_udf3='REDEEM ".date('Y-m-d H:i:s')."', modified_on='".date('Y-m-d H:i:s')."'
                    WHERE trans_id='$id' 
                    AND lot_id IN (select TOP 1 lot_id from b2b_gateway_lot where trans_id='$id' order by created_on desc)";
            $res = $this->db->query($sql);

            // Email KF
            $msg = 'APPLE BTS - OK - ' . $id2 .'('.$id.')';
            $call = $this->Api_model->cherps_email($msg,'APPLEBTS '.$id.' - REDEEM-OK', $this->email_notification);
            if ($call)
                $result = ['code'=>1, 'msg'=>'Email delivered'];
            else
                $result = ['code'=>0, 'msg'=>'Email failed'];
        }
        else if ($mode=='redeem-fail'){
            // Email KF
            $msg = 'APPLE BTS - FAIL - ' . $id2 .'('.$id.')';
            $call = $this->Api_model->cherps_email($msg,'APPLEBTS '.$id.' - REDEEM-FAIL', $this->email_notification);
            if ($call)
                $result = ['code'=>1, 'msg'=>'Email delivered'];
            else
                $result = ['code'=>0, 'msg'=>'Email failed'];
        }

        return $result;
    }

    private function doCurl($url){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $queryjson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);

    }
}
?>
