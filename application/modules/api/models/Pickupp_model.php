<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Pickupp_model extends Base_Common_Model {

    private $apiurl;
    private $token;

    public function __construct() {
        parent::__construct();

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->apiurl = 'https://gateway.sg.pickupp.io/v2/merchant/orders';
            $this->token['HCL'] = 'aGMudWJpQGhhY2hpLnNnOjMzYmMwNjRjY2Y4YTIxZDAyMzhkZjQ2ZTc1YmMyYmY2';
            $this->token['BF'] = 'YmZAY2hhbGxlbmdlci5zZzowOWRlZGFiODNjMzFjZWZlOTExYWZmNzRjODVmNWNkNQ==';
        }
        else {
            $this->apiurl = 'https://gateway-uat.hkpickup.com/v2/merchant/orders';
            $this->token['HCL'] = 'eW9uZ3NoZW5nQGNoYWxsZW5nZXIuc2c6ZGMxOTc3ZWJmNmIyMzBkMDM2MzdjNWU0ODYzN2FkYzA=';
        }
    }

    public function getParcel($invoice_id='') {
        $sql_invoice = (strlen($invoice_id)>3) ? "and p.invoice_id='$invoice_id'" : "";
        $sql = "select 
                    p.coy_id,p.invoice_id,p.parcel_id, 
                    CASE WHEN (p.parcel_id>1) THEN concat(rtrim(i.invoice_id),'-',p.parcel_id::varchar) ELSE rtrim(i.invoice_id) END as external_track_no, 
                    CASE WHEN concat(rtrim(i.first_name),' ',rtrim(i.last_name))!='' THEN concat(rtrim(i.first_name),' ',rtrim(i.last_name))
                        ELSE (cust_name) END as dst_name,
                    i.tel_code as dst_contact,
                    CASE WHEN (i.email_addr!='') THEN i.email_addr
                        ELSE (select email_addr from crm_member_list where coy_id='CTL' and mbr_id=i.cust_id) END as dst_email,
                    REPLACE(d.addr_text,'''','') as dst_addr,
                    d.postal_code as dst_postcode, 
                    CASE WHEN d.country_id='SG' THEN 'Singapore' ELSE d.country_id END as dst_country,
                    p.volumeL as vol_l,
                    p.volumeW as vol_w,
                    p.volumeH as vol_h,
                    p.weight as weight,
					current_timestamp as pickup_datetime,
                    p.delivery_date::date as delivr_date,
                    '1800' as delivr_time,
                    (CASE WHEN EXISTS(SELECT sms_invoice_item.loc_id FROM b2b_parcel_item,sms_invoice_item 
							WHERE sms_invoice_item.invoice_id=b2b_parcel_item.invoice_id
								AND sms_invoice_item.item_id=b2b_parcel_item.item_id
								AND b2b_parcel_item.invoice_id= p.invoice_id
								AND b2b_parcel_item.parcel_id= p.parcel_id 
						) 
						THEN (SELECT sms_invoice_item.loc_id FROM b2b_parcel_item,sms_invoice_item,b2b_parcel_list
							WHERE sms_invoice_item.invoice_id=b2b_parcel_item.invoice_id AND sms_invoice_item.item_id=b2b_parcel_item.item_id
								AND b2b_parcel_list.invoice_id=b2b_parcel_item.invoice_id AND b2b_parcel_list.parcel_id=b2b_parcel_item.parcel_id
									 AND b2b_parcel_list.delv_mode_id=b2b_parcel_item.delv_mode_id
								AND b2b_parcel_item.invoice_id= p.invoice_id AND b2b_parcel_item.parcel_id= p.parcel_id 
                             ORDER BY b2b_parcel_list.created_on DESC LIMIT 1) 
						ELSE i.loc_id END) as loc_id
                from b2b_parcel_list p
                left join sms_invoice_list i on i.invoice_id=p.invoice_id and i.coy_id=p.coy_id
                left join coy_address_book d on d.ref_id=i.invoice_id and d.addr_type=i.delv_addr
                where p.status_level =0 and p.delv_mode_id='SPP' $sql_invoice 
                order by p.created_on desc";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    public function getLocation($coy_id,$loc_id) {
        $sql = "select 
                    l.loc_name as src_company,
                    l.tel_code as src_contact,
                    l.email_ims as src_email,
                    d.addr_text as src_addr,
                    d.postal_code as src_postcode, 
                CASE WHEN d.country_id='SG' THEN 'Singapore' ELSE d.country_id END as src_country
                from ims_location_list l
                left join coy_address_book d on d.ref_id=l.loc_id and d.coy_id=l.coy_id and d.ref_type='LOCATION'
                where l.coy_id='$coy_id' and l.loc_id='$loc_id'";
        $res = $this->db->query($sql)->row_array();
        return $res;
    }

    public function saveGatewayLog($params){
        $coy_id='CTL';
        $gateway_id='PICKUPP';
        $gateway_code = md5($params['trans_id'].$params['loc_id'].$params['pos_id'].$params['buyer_code'].$params['tx_time']);
        $gateway_request = $params['trans_id'].'/'.$params['loc_id'].'/'.$params['buyer_code'].'?t='.$params['tx_time'] ;
        if ($params['status']=="NEW") {
            $sql = "INSERT INTO b2b_gateway_message 
                        (coy_id,gateway_id,gateway_code,trans_id,loc_id,pos_id,request_link,request_msg,status_level,created_by,created_on)
                        VALUES ('$coy_id','$gateway_id','".$gateway_code."','".trim($params['trans_id'])."','".trim($params['loc_id'])."','".trim($params['pos_id'])."' ,
                        '".trim($params['url'])."','".$params['request']."','0','".getRealIpAddr(15)."',current_timestamp)";
            $this->db->query($sql);
        }
        else {
            $sql = "UPDATE b2b_gateway_message
                        SET status_level=1, return_status='".$params['status']."', return_msg='".str_replace("'",'',$params['response'])."'
                        WHERE coy_id='$coy_id' AND gateway_id='$gateway_id' AND gateway_code='$gateway_code' ";
            $this->db->query($sql);
        }
    }

    public function updateParcel($params){
        $sql = "UPDATE b2b_parcel_list
                    SET transaction_id='".$params['transaction_id']."', tracking_id='".$params['tracking_id']."', status_level='".$params['status_level']."',
                        remarks='".$params['remarks']."',quick_ref='".$params['quick_ref']."',
                        modified_by='Pickupp',modified_on=current_timestamp
                    WHERE coy_id='".$params['coy_id']."' AND invoice_id='".$params['invoice_id']."' AND parcel_id='".$params['parcel_id']."' and delv_mode_id='SPP' ";
        $this->db->query($sql);
    }

    public function query($id=''){

        date_default_timezone_set('Asia/Singapore');

        $tx_time = date("YmdHis");
        $invoice_id = ($id=='') ? $this->input->get('invoice_id') : $id;
        $invoice = $this->getParcel($invoice_id);

        if (!$invoice){
            $return["status"] = 'FAILED';
            $return["message"] = 'No invoice found.';
            return $return;
        }

        foreach ($invoice as $i=>$inv){
            // Fix contact_no
            $inv_contact = str_replace(' ','',$inv['dst_contact']);
            $invoice[$i]['dst_contact'] = (substr($inv_contact,0,2)=='65' && strlen($inv_contact)<=8) ? '65'.$inv_contact : $inv_contact;
        }

        if (empty($invoice[0]['dst_contact']) || empty($invoice[0]['dst_postcode']) || empty($invoice[0]['dst_country'])){
            $return["status"] = 'FAILED';
            $return["message"] = 'Invoice found does not have full details (contact/postcode/country).';
            return $return;
        }
        else if (strlen($invoice[0]['dst_postcode'])!=6) {
            // Invalid postal code
            $return["status"] = 'FAILED';
            $return["message"] = 'Please check postcode ('.$invoice[0]['dst_postcode'].').';
            return $return;
        }
        else if (strlen($invoice[0]['dst_contact'])<8) {
            // Invalid phone
            $return["status"] = 'FAILED';
            $return["message"] = 'Please check the phone number, do not have 8 digits ('.$invoice[0]['dst_contact'].').';
            return $return;
        }
        else if ( !($this->input->get('delivr_date')) && strtotime($invoice[0]['delivr_date'])<=time()-86400 ){
            $return["status"] = 'FAILED';
            $return["message"] = 'Please check the delivery window ('.$invoice[0]['delivr_date'].').';
            return $return;
        }

        $loc_id = $invoice[0]['loc_id']; //($this->input->get('loc_id')) ? $this->input->get('loc_id') : 'BF';
        $loc = $this->getLocation($invoice[0]['coy_id'],$loc_id);

        $dates['pickup_date'] = ($this->input->get('pickup_date')) ? strtotime($this->input->get('pickup_date')) : strtotime($invoice[0]['pickup_datetime']);
        $dates['delivr_date'] = ($this->input->get('delivr_date')) ? strtotime($this->input->get('delivr_date')) : strtotime($invoice[0]['delivr_date'].' '.$invoice[0]['delivr_time']);

        $query = $this->_doQuery($invoice,$loc,$dates);

        $this->saveGatewayLog( array(
            "status" => 'NEW',
            "trans_id" => trim($invoice[0]['invoice_id']),
            "loc_id" => $loc_id,
            "pos_id" => '',
            "buyer_code" => $dates['pickup_date'].'>'.$dates['delivr_date'],
            "url" => $this->apiurl,
            "tx_time" => $tx_time,
            "request" => urldecode($query),
            "response" => ''
        ) );

        $output = $this->_doCurl($query, $loc_id);

        $response = json_decode($output);
        $response_trackingtest = $response->data[0]->id;

        if ($response_trackingtest) {

            $return["status"] = 'SUCCESS';
            if (isset($response->meta->error_message)) {
                $return["message"] = $response->meta->error_message;
            }

            foreach ($invoice as $k=>$inv) {
                $res_invoice = $response->data[$k]->client_reference_number;
                $response_tracking = $response->data[$k]->order_number;
                $token_tracking = $response->data[$k]->token;
                if ($response_tracking) {
                    $this->updateParcel(array(
                        "coy_id" => $inv['coy_id'],
                        "invoice_id" => $inv['invoice_id'],
                        "parcel_id" => $inv['parcel_id'],
                        "transaction_id" => $response_tracking,
                        "tracking_id" => $response_tracking,
                        "status_level" => 3,
                        "quick_ref" => $token_tracking,
                        "remarks" => 'SUCCESSFUL' //FAILED
                    ));
                }

                $return["results"][] = array(
                    "invoice_id" => trim($inv['invoice_id']),
                    "parcel_id" => trim($inv['parcel_id']),
                    "tracking_id" => $response_tracking
                );
            }
        }
        else {
            $return["status"] = 'FAILED';
            $return["message"] = $response->meta->error_message;
        }

        $this->saveGatewayLog( array(
            "status" => ($response_trackingtest) ? json_encode($return) : 'ERROR',
            "trans_id" => trim($invoice[0]['invoice_id']),
            "loc_id" => $loc_id,
            "pos_id" => '',
            "buyer_code" => $dates['pickup_date'].'>'.$dates['delivr_date'],
            "url" => $this->apiurl,
            "tx_time" => $tx_time,
            "response" => $output
        ) );

        if ( $this->input->get('debug')==1 ) {
            print_r($output);
            exit;
        }

        return $return;
    }

    private function _doCurl($query, $acct) {

        $token = $this->token['HCL'];
        if (isset($this->token[$acct]))
            $token = $this->token[$acct];

        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        $headers[] = "Authorization: Basic " . $token;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    private function _doQuery($invoices,$loc,$dates){
        $query = array(); $i=0;
        foreach ($invoices as $k=>$invoice) {
            $q['pickup_contact_person'] = $loc['src_company'];
            $q['pickup_address_line_1'] = $loc['src_country'] .' '. $loc['src_postcode'];
            $q['pickup_address_line_2'] = $loc['src_addr'];
            $q['pickup_contact_phone'] = $loc['src_contact'];
            $q['pickup_time'] = date(DATE_ATOM, $dates['pickup_date']);

            $q['dropoff_contact_person'] = $invoice['dst_name'];
            $q['dropoff_address_line_1'] = $invoice['dst_country'] .' '. $invoice['dst_postcode'];
            $q['dropoff_address_line_2'] = $invoice['dst_addr'];
            $q['dropoff_contact_phone'] = $invoice['dst_contact'];
            $q['dropoff_time'] = date(DATE_ATOM, $dates['delivr_date']);

            $q['width'] = $invoice['vol_w'];
            $q['length'] = $invoice['vol_l'];
            $q['height'] = $invoice['vol_h'];
            $q['weight'] = $invoice['weight'];
            $q['client_reference_number'] = trim($invoice['invoice_id']);
            $q['origin'] = 'CHALLENGER';

            $query[$i] = $q;
            $i++;
        }
        //print_r($query);exit;
        return http_build_query( array("orders"=>$query));
    }

}
?>