<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Api extends Front_Controller {

    /*
     *  Developed APIs:
     *      api/vcmember/[verify] - Valueclub member verification
     *
     * http://chvoices-test.challenger.sg/
     *      api/alipay/[pay|refund|check] - AliPay
     *      api/capitastar/[validate|pay] - Capitastar voucher
     *      api/lendlease/[validate|pay] - Lendlease voucher
     *      api/ctlvch/ - Challenger vouchers
     *
     *      api/applecare/
     *      api/appledep/
     *
     *      api/blackhawk/[process|trans_id|check] - Activation/Void for Itunes/GoogleCard via Blackhawk
     *      api/lazada/orders - Sync orders from Lazada into Cherps SMS Invoice
     *      api/lazada/products - [Dev] Sync product soh
     *
     *      api/urbanfox/create?invoice_id=HIA00001 - Create delivery order with Urbanfox for prepared invoice
     *      api/pickupp/create?invoice_id=HIA00001 - Create delivery order with Pickupp for prepared invoice
     *      api/honestbee/create?invoice_id=HIA00001 - Create delivery order with Pickupp for prepared invoice
     *      api/delivery/[invoice_id] - Create delivery order with Pickupp for prepared invoice
     *
     *      api/challenger_po/generate/[invoice_id] - Generate PO from SMS Invoice
     *      api/challenger_po/transmit/[po_id] - Push PO to Vendor system
     *      api/challenger_po - Scan for new POs to push to vendor's system
     *      api/challenger_notify - Call email/push notification
     *      api/crosspoint - Retrieve data from Crosspoint and Cherps for display of analytic chart
     *
     *      api/ssew/ - For SSEW website to retrieve SR info from ssew tables
     *
     *      api/challenger_ctlreport/check_fwd - For report email redirect checking (Use by Lambda)
     *
     *      api/cherps_db
     *      api/challenger_asn
     *      api/pos_email
     *      api/posgettoken
     */

    public function postest($mode='connect'){
        if ($mode=='connect') {
            echo 'connected!!!';
        }
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Api_model');
        
        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->apipath = 'https://api.hachi.tech/v1/';
            $this->hachipath = 'https://www.hachi.tech/';
        }
        else {
            $this->apipath = 'https://api.hachi.tech/test/';
            $this->hachipath = 'https://dev-lamb.sghachi.com/';
        }
    }

    public function esd_check($id) {
        $result = $this->Api_model->esd_check($id);

        $api_msg = [
            'response'  => $result['msg_response'],
            'request'   => $result['msg_request']
        ];

        unset($result['msg_request']);
        unset($result['msg_response']);

        echo "<h2>ESD Check</h2><pre>" . json_encode($result, JSON_PRETTY_PRINT) . "</pre>";
        echo "<hr>Request: <textarea style='border:none;width:100%;height:200px' readonly>" . ($api_msg['request']) . "</textarea>";
        echo "<br><br>Response: <textarea style='border:none;width:100%;height:200px' readonly>" . ($api_msg['response']) . "</textarea>";

    }

    public function cb_hclqoh($m=0){
        $results = $this->Api_model->cb_hclqoh($m);

        $date = date('d M Y H:i:s');
        $header[] = ['HCL QOH AS OF '.$date];
        $header[] = ['item_id','item_desc','inv_dim2','inv_dim3','buffet_qty','qoh_hcl'];


        $filecontent = array_merge($header,$results);
        $handle_path = FCPATH . 'assets/uploads/';
        $uploaded = 'HCL_QOH.csv';

        $f = fopen($handle_path . $uploaded, "w");
        foreach ($filecontent as $line) {
            if ($line[5]>=$m)
                fputcsv($f, $line);
        }

        echo base_url('assets/uploads/HCL_QOH.csv');

    }

    public function ctlvch($vchtype,$mode,$id=''){
        $this->load->model('api/Ctlvch_model');
        if ($vchtype=="transaction") {
            $result = $this->Ctlvch_model->sms_activate_trans($mode);
            print_r($data = json_encode($result));
        }
        else if ($vchtype=="sms_voucher") {
            $result = $this->Ctlvch_model->sms_voucher($mode,$id);
            print_r($data = json_encode($result));
        }
        else if ($vchtype=="crm_voucher") {
            $result = $this->Ctlvch_model->crm_voucher($mode,$id);
            print_r($data = json_encode($result));
        }
        else {
            print_r($data = json_encode(['code' => -1, 'msg' => 'Missing voucher type.']));
        }
    }

    public function vcreporting($date=''){
        $this->load->model('api/Valueclub_model');
        $result = $this->Valueclub_model->insertOsTxn($date);
        print_r($data = json_encode($result));
    }

    public function vcmember($mode,$mbr_id='') {

        $result = array("code" => 1, "msg" => 'This membership has been verified.', "msgcode"=>80050);
        print_r(json_encode($result));
        exit;
        
        if (!challengerIpAddr() && $mode!='verified') {
            $apians = $this->Api_model->getb2bsetting('API','CTLVALUECLUB_VERIFY','LIVE');
            if ($this->input->get_request_header($apians['set_desc']) != $apians['remarks']) {
                $msg = 'Unauthorized access to ' . base_url('vcmember/' . $sp) . ' from ' . getRealIpAddr(15);
                //$call = $this->Api_model->cherps_email($msg, 'Unauthorized access to fx vcmember', 'yongsheng@challenger.sg');

                $rtn = array(
                    "code" => '-1',
                    "messages" => 'Unauthorized access location. Please make sure you are at a whitelisted IP location.'
                );
                echo json_encode($rtn);
                return;
            }
        }

        $this->load->model('api/Valueclub_model');
        if ($mode=='verify') {
            $result = $this->Valueclub_model->verify($mbr_id);
            print_r($data = json_encode($result));
        }
        else if ($mode=='verified') {
            $result = $this->Valueclub_model->verified(base64_decode($mbr_id));
            print_r($data = json_encode($result));
        }
        else if ($mode=='updverified') {
            $result = $this->Valueclub_model->updnewmbrid($mbr_id);
            print_r($data = json_encode($result));
        }
        else if ($mode=='delverified') {
            $result = $this->Valueclub_model->delverified($mbr_id);
            print_r($data = json_encode($result));
        }
        else if ($mode=='testverifyemail') {
            $result = $this->Valueclub_model->sendUpdateDetailEmail('S8713750C','yongsheng@challenger.sg','YONG');
            print_r($data =  ($result));

        }

    }

    public function alipay($mode,$trans_id='',$loc_id='',$pos_id='',$trans_amt='',$buyer_code=''){
        $this->load->model('api/Alipay_model');
        if ($mode=='pay') {
            $result = $this->Alipay_model->pay($trans_id, $loc_id, $pos_id, $trans_amt, $buyer_code);
            print_r($data = json_encode($result));
        }
        else if ($mode=='check') {
            $result = $this->Alipay_model->check($trans_id);
            print_r($data = json_encode($result));
        }
        else if ($mode=='refund') {
            $result = $this->Alipay_model->refund($trans_id, $loc_id, $pos_id, $trans_amt, $buyer_code);
            print_r($data = json_encode($result));
        }
        else if ($mode=='alipay_svr_return_pay') {
            $result = $this->Alipay_model->svr_return_pay();
            print_r($data = json_encode($result));
        }
        else if ($mode=='alipay_svr_return_refund') {
            $result = $this->Alipay_model->svr_return_refund();
            print_r($data = json_encode($result));
        }
    }

    public function capitastar($mode,$trans_id='',$loc_id='',$pos_id='',$trans_amt=0,$vouchers=''){
        $this->load->model('api/Capitastar_model');
        if ($mode=='pay') {
            $result = $this->Capitastar_model->pay($trans_id, $loc_id, $pos_id, $trans_amt, $vouchers);
            print_r($data = json_encode($result));
        }
        else if ($mode=='validate') {
            $result = $this->Capitastar_model->validate($trans_id, $loc_id, $pos_id, $trans_amt, $vouchers);
            print_r($data = json_encode($result));
        }
        else if ($mode=='daily') {
            $result = $this->Capitastar_model->tally_daily($trans_id,$loc_id);
            print_r($data = json_encode($result));
        }
    }

    public function lendlease($mode,$trans_id='',$loc_id='',$pos_id='',$trans_amt=0,$vouchers=''){
        $this->load->model('api/Lendlease_model');
        if ($mode=='validate') {
            $result = $this->Lendlease_model->validate($trans_id, $loc_id, $pos_id, $trans_amt, $vouchers);
            print_r($data = json_encode($result));
        }
        else if ($mode=='pay') {
            $result = $this->Lendlease_model->pay($trans_id, $loc_id, $pos_id, $trans_amt, $vouchers);
            print_r($data = json_encode($result));
        }
//        else if ($mode=='daily') {
//            $result = $this->Lendlease_model->tally_daily($trans_id);
//            print_r($data = json_encode($result));
//        }
    }

    public function lazada($mode) {
        if ($mode=="orders") {
            $this->load->model('api/Lazada2_model');
            $result = $this->Lazada2_model->import_orders();
            print_r($data = json_encode($result));
        }
        else if ($mode=="products_qty") {
            $this->load->model('api/Lazada2_model');
            $result = $this->Lazada2_model->upd_products_qty();
            print_r($data = json_encode($result));
        }
        else if ($mode=="login") {
            $this->load->model('api/Lazada2_model');
            $result = $this->Lazada2_model->openplatform_token();
            print_r($data = json_encode($result));
        }
    }

    public function applecare($id=''){
        $this->load->model('api/Applecare_model');
        $result = $this->Applecare_model->process($id);
        print_r($data = json_encode($result));
    }

    public function applecorrect(){
        $this->load->model('api/Applecare_model');
        $result = $this->Applecare_model->correction();
        print_r($data = json_encode($result));
    }

    public function appledep($id='', $mode=''){
        $this->load->model('api/Appledep_model');
        $result = $this->Appledep_model->process($id, $mode);
        print_r($data = json_encode($result));
    }

    public function applebts($mode,$id='',$id2='',$id3='') {
        $this->load->model('api/Applebts_model');
        $result = $this->Applebts_model->process($mode,$id,$id2,$id3);
        print_r($data = json_encode($result));
    }

    public function blackhawk($id=''){
        $this->load->model('api/Blackhawk_model');
        $result = $this->Blackhawk_model->process($id);
        print_r($data = json_encode($result));
    }

    public function optimoroute($mode='send',$id=''){
        $this->load->model('api/Optimoroute_model');
        if ($mode=='auto')
            $result = $this->Optimoroute_model->auto($id);
        else if ($mode=='plan')
            $result = $this->Optimoroute_model->plan($id);
        else if ($mode=='routes')
            $result = $this->Optimoroute_model->routes($id);
        else
            $result = $this->Optimoroute_model->send($id);
        print_r($data = json_encode($result));
    }

    public function delivery($id=''){
        $mode_id = $this->Api_model->challenger_parcel($id);
        switch ($mode_id[0]->delv_mode_id)
        {
            case 'NQX':
                $this->load->model('api/Urbanfox_model');
                $result = $this->Urbanfox_model->query($id);
                break;
            case 'SPP':
                $this->load->model('api/Pickupp_model');
                $result = $this->Pickupp_model->query($id);
                break;
            case 'NHB':
            case 'SHB':
                $this->load->model('api/Honestbee_model');
                $result = $this->Honestbee_model->query($id);
                break;
            case 'SRW':
            case 'NRW':
                $this->load->model('api/Detrack_model');
                $result = $this->Detrack_model->query($id);
                break;
            case 'NJA':
                $this->load->model('api/Janio_model');
                $result = $this->Janio_model->query($id);
                break;
            default:
                $result = array("status"=>"FAIL", "message"=>"No invoice found or Invalid delv_mode_id");
                break;
        }
        print_r($data = json_encode($result));
    }
    public function urbanfox($mode=''){
        $this->load->model('api/Urbanfox_model');
        $result = $this->Urbanfox_model->query();
        print_r($data = json_encode($result));
    }
    public function pickupp($mode=''){
        $this->load->model('api/Pickupp_model');
        $result = $this->Pickupp_model->query();
        print_r($data = json_encode($result));
    }
    public function honestbee($mode=''){
        $this->load->model('api/Honestbee_model');
        $result = $this->Honestbee_model->query();
        print_r($data = json_encode($result));
    }
    public function riverwood($mode=''){
        $this->load->model('api/Detrack_model');
        $result = $this->Detrack_model->query();
        print_r($data = json_encode($result));
    }
    public function janio($mode=''){
        $this->load->model('api/Janio_model');
        $result = $this->Janio_model->query();
        print_r($data = json_encode($result));
    }

    public function ssew($mode,$id){
        //$apians = $this->Api_model->getb2bsetting('API','CHVOICES_SSEW','LIVE');
        if ($this->input->get_request_header('X_AUTHORIZATION') != 'AC909E9626F90152738EAB20916B6F16' && !challengerIpAddr()) {
            print_r($data = json_encode( array("code"=>-1, "msg"=>'Unauthorized access location. Please make sure you are at a whitelisted IP location.') ));
            return;
        }

        if ($mode=='service_req'){
            $list = $this->Api_model->ssew_servicereq($id);
            if ($list){
                print_r($data = json_encode( array("code"=>1, "msg"=> count($list).' SR found', "data"=>$list) ));
            }
            else {
                print_r($data = json_encode( array("code"=>-0, "msg"=>'No valid SR found') ));
            }
        }
        else {
            print_r($data = json_encode( array("code"=>-1, "msg"=>'Invalid mode') ));
        }
    }

    public function samsung($type, $date='')
    {
        $this->load->model('api/Samsung_model');
        $result = $this->Samsung_model->generate_data($type, $date);
        if ($result == 1) {
            return $this->_successWithInfo("Success upload");
        } else {
            return $this->_errorWithInfo("Whoops! There was a problem processing your ssew. Please try again.");
        }
    }

    public function ssew_migrate($from='', $to='')
    {
        $this->load->model('api/Api_model');
        $result = $this->Api_model->ssew_migrate($from, $to);
        print_r($data=json_encode($result));
    }

    public function ssew_single($id='')
    {
        $this->load->model('api/Api_model');
        $result = $this->Api_model->ssew_single($id);
        print_r($data=json_encode($result));
    }

    public function ssew_trans()
    {
        $check_auth = $this->_check_auth();
        if ($check_auth['status'] === 'error'){
            return $this->_errorWithCodeAndInfo('500', $check_auth['message']);
        }

        $this->load->model('api/Api_model');
        $input = json_decode(file_get_contents('php://input'), true);

        $invoice_id = $input['invoice_id'];
        if (is_null($invoice_id) || empty($invoice_id)) {
            return $this->_validateError("invoice_id required");
        }
        $offer_type = $input['offer_type'];
        if (is_null($offer_type) || empty($offer_type)) {
            return $this->_validateError("offer_type required");
        }
        $rf_invoice_id = $input['rf_invoice_id'];
        if ($offer_type === "HR" || $offer_type === 'HX' || $offer_type === 'EX' || $offer_type === 'RF') {
            if (is_null($rf_invoice_id) || empty($rf_invoice_id)) {
                return $this->_validateError("rf_invoice_id required");
            }
            if (!$this->Api_model->_offer_exist($rf_invoice_id)) {
                return $this->_errorWithInfo("Sorry, can not find refund invoice in our records.");
            }
        }
        $offer_date = $input['offer_date'];
        if (is_null($offer_date) || empty($offer_date)) {
            return $this->_validateError("offer_date required");
        }
        $coy_id = $input['coy_id'];
        if (is_null($coy_id) || empty($coy_id)) {
            return $this->_validateError("coy_id required");
        }
        $loc_id = $input['loc_id'];
        if (is_null($loc_id) || empty($loc_id)) {
            return $this->_validateError("loc_id required");
        }
        $offers = $input['offers'];
        if (is_null($offers) || empty($offers)) {
            return $this->_validateError("offers required");
        }
        $cust_info = $input['cust_info'];
        if (is_null($cust_info) || empty($cust_info)) {
            return $this->_validateError("cust_info required");
        }

        $res = $this->Api_model->ssew_trans($invoice_id, $offer_type, $rf_invoice_id, $offer_date, $coy_id, $loc_id, $offers, $cust_info);
        if ($res) {
            return $this->_successWithInfo("Success add ssew");
        } else {
            return $this->_errorWithInfo("Whoops! There was a problem processing your ssew. Please try again.");
        }
    }

    public function credit_migrate($id, $refundMode)
    {
        $check_auth = $this->_check_auth();
        if ($check_auth['status'] === 'error'){
            return $this->_errorWithCodeAndInfo('500', $check_auth['message']);
        }
        $this->load->model('api/Api_model');
        $refundMode = strtolower($refundMode);
        if (!in_array($refundMode, ['original', 'ecredit'])) {
            return $this->_errorWithInfo("RefundMode must be 'original' or 'ecredit'.");
        }
        $result = $this->Api_model->credit_migrate($id, $refundMode);
        $result = json_decode($result);
        if ($result->status_code == 200) {
            return $this->_successWithInfo($result->info);
        } else {
            return $this->_errorWithInfo($result->message);
        }
    }

    public function hachi_credit_migrate()
    {
        $check_auth = $this->_check_auth();
        if ($check_auth['status'] === 'error'){
            return $this->_errorWithCodeAndInfo('500', $check_auth['message']);
        }
        $input = json_decode(file_get_contents('php://input'), true);

        $inv_type = $input['inv_type'];
        if (is_null($inv_type) || empty($inv_type)) {
            return $this->_validateError("inv_type required");
        }
        if ($inv_type !== 'HR' && $inv_type !== 'HX') {
            return $this->_validateError("inv_type should be 'HX' or 'HR'");
        }

        $refund_mode = strtolower($input['refund_mode']);
        if (!in_array($refund_mode, ['original', 'ecredit'])) {
            return $this->_errorWithInfo("RefundMode must be 'original' or 'ecredit'.");
        }

        $refund_reason = $input['refund_reason'];
        if (is_null($refund_reason) || empty($refund_reason)) {
            return $this->_errorWithInfo("Refund reason required.");
        }

        $cashier_id = substr($input['cashier_id'], 0, 10);
        if (is_null($cashier_id) || empty($cashier_id)) {
            return $this->_errorWithInfo("Cashier ID required.");
        }

        $original_invoice_id = $input['original_invoice_id'];
        if (is_null($original_invoice_id) || empty($original_invoice_id)) {
            return $this->_validateError("original_invoice_id required");
        }

        $loc_id = $input['loc_id'];
        if (is_null($loc_id) || empty($loc_id)) {
            return $this->_validateError("loc_id required");
        }

        $items = $input['items'];
        if (is_null($items) || empty($items)) {
            return $this->_validateError("items required");
        }

        $remarks = isset($input['remarks']) ? $input['remarks'] : '';

        $this->load->model('api/Api_model');

        $res = $this->Api_model->hachi_credit_migrate($inv_type, $refund_mode, $original_invoice_id, $items, $loc_id, $refund_reason, $cashier_id, $remarks);
        $result = json_decode($res);
        if ($result->status_code == 200) {
            return $this->_successWithData($result->data);
        } else {
            return $this->_errorWithInfo($result->message);
        }
    }

    public function hachi_credit_cancel()
    {
        $check_auth = $this->_check_auth();
        if ($check_auth['status'] === 'error'){
            return $this->_errorWithCodeAndInfo('500', $check_auth['message']);
        }
        $input = json_decode(file_get_contents('php://input'), true);

        $inv_type = $input['inv_type'];
        if (is_null($inv_type) || empty($inv_type)) {
            return $this->_validateError("inv_type required");
        }
        if ($inv_type !== 'HR' && $inv_type !== 'HX') {
            return $this->_validateError("inv_type should be 'HX' or 'HR'");
        }

        $refund_mode = strtolower($input['cancel_mode']);
        if (!in_array($refund_mode, ['original', 'ecredit'])) {
            return $this->_errorWithInfo("CancelMode must be 'original' or 'ecredit'.");
        }

        $refund_reason = $input['cancel_reason'];
        if (is_null($refund_reason) || empty($refund_reason)) {
            return $this->_errorWithInfo("Cancel reason required.");
        }

        $cashier_id = substr($input['cashier_id'], 0, 10);
        if (is_null($cashier_id) || empty($cashier_id)) {
            return $this->_errorWithInfo("Cashier ID required.");
        }

        $original_invoice_id = $input['original_invoice_id'];
        if (is_null($original_invoice_id) || empty($original_invoice_id)) {
            return $this->_validateError("original_invoice_id required");
        }

        $loc_id = $input['loc_id'];
        if (is_null($loc_id) || empty($loc_id)) {
            return $this->_validateError("loc_id required");
        }

        $items = $input['items'];
        if (is_null($items) || empty($items)) {
            return $this->_validateError("items required");
        }

        $remarks = isset($input['remarks']) ? $input['remarks'] : '';

        $this->load->model('api/Api_model');

        $res = $this->Api_model->hachi_credit_cancel($inv_type, $refund_mode, $original_invoice_id, $items, $loc_id, $refund_reason, $cashier_id, $remarks);
        $result = json_decode($res);
        if ($result->status_code == 200) {
            return $this->_successWithData($result->data);
        } else {
            return $this->_errorWithInfo($result->message);
        }
    }

    public function _check_auth()
    {
        $key_code = 'Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjEAA';
        if (empty($this->input->get()['code']) && empty($this->input->request_headers()['Authorization'])) {
            return array('status'=>'error','message'=>'Auth code is empty');
        } else {
            if (!empty($this->input->request_headers()['Authorization'])) {
                $jwt = explode(" ", $this->input->request_headers()['Authorization']);
                if ($jwt[0] !== 'Bearer' || $jwt[1] !== $key_code){
                    return array('status'=>'error','message'=>'Auth header invalid');
                }
            }
            else if (!empty($this->input->get()['code']) && $this->input->get()['code'] !== $key_code) {
                return array('status'=>'error','message'=>'Key code invalid');
            }
        }
        return array('status'=>'success');
    }

    public function _errorWithInfo($info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(array(
                'status' => 'error',
                'status_code' => 404,
                'message' => $info
            )));
    }

    public function _errorWithCodeAndInfo($code, $info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output(json_encode(array(
                'status' => 'error',
                'status_code' => $code,
                'message' => $info
            )));
    }

    public function _successWithInfo($info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                'info' => $info,
                'status' => 'success',
                'status_code' => 200
            )));
    }

    public function _successWithData($data)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                'data' => $data,
                'status' => 'success',
                'status_code' => 200
            )));
    }

    public function _validateError($errors)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(422)
            ->set_output(json_encode(array(
                'status' => 'validate error',
                'status_code' => 422,
                'errors' => $errors
            )));
    }

    public function razer_pin_store($id='')
    {
        $this->load->model('api/Razer_model');
        $result = $this->Razer_model->pin_store($id);
        $result = json_decode($result);
        if ($result->status_code == 200) {
            return $this->_successWithInfo($result->info);
        } else {
            return $this->_errorWithInfo($result->message);
        }
    }

    public function crosspoint($loc_id='',$date='',$view='JSON'){
        $this->load->model('api/Crosspoint_model');
        $result = $this->Crosspoint_model->crosspoint($view,$loc_id,$date);
    }
    public function crosspoint_load($step=0){
        $this->load->model('api/Crosspoint_model');
        $result = $this->Crosspoint_model->load($step);
    }

    public function challenger_ctlreport($mode){
        $jsoncontent = json_decode(trim(file_get_contents('php://input')));
        if ($mode=='history') {
            $sql = "INSERT INTO rep_email_history VALUES 
                      ('$jsoncontent->messageid',GETDATE(),
                        '$jsoncontent->subject','$jsoncontent->status','$jsoncontent->remarks');";
            echo $sql;
            $cp_data = $this->db->query($sql);
        }
        else if ($mode=='check_fwd') {
            $sql = "SELECT TOP 1 email_addr FROM rep_email_list where subject_name = LTRIM(RTRIM('$jsoncontent->subject')) ORDER BY created_on DESC ";
            $cp_data = $this->db->query($sql)->row_array();
            echo json_encode($cp_data['email_addr']);
        }
        return;
    }

    // chvoices.challenger.sg/api/challenger_transfer
    public function challenger_transfer() {
        $this->load->model('cherps/Consign_model');
        $result = $this->Consign_model->createTransfer();
        print_r($data = json_encode($result));
    }

    // chvoices.challenger.sg/api/challenger_po/generate/HIC000001
    // chvoices.challenger.sg/api/challenger_po/transmit/IPHCL00000111
    public function challenger_po($mode='',$id1=''){

        if ($mode=='generate') {
            // Convert Invoice into PO
            $this->load->model('cherps/Consign_model');
            $result = $this->Consign_model->generateSmsTransId($id1);
            print_r($data = json_encode($result));

        }
        else if ($mode=='transmit') {
            // Trigger API - send PO to supplier
            $list = $this->Api_model->challenger_po('PO',$id1);
            if ($list) {
                $this->load->model('cherps/Pogrn_model');
                $sendapi = $this->Pogrn_model->transmitPoVendor($list['po_id'], array("supp_id" => $list['supp_id']));
                print_r($data = json_encode($sendapi));
            }
            else {
                print_r($data = json_encode( array("code"=>-1, "msg"=>'No valid PO found') ));
            }
        }
        else if ($mode=="printpdf") {
            $list = $this->Api_model->challenger_po('PRINTPDF',$id1);
            if ($list) {
                $url = base_url('po2/export/pdf/'.trim($list['po_id']).'/'.urlencode(trim($list['supp_id'])).'?cherps='.time());
                redirect($url);
            }
        }
        else if ($mode=='confirmation'){
            // http://chvoices-test.challenger.sg/api/challenger_po/confirmation/{supp_id}
            if ($id1=="ER0001") {
                $this->load->model('cherps_supplier/Er0001_model');
                $result = $this->Er0001_model->read_xml($id1);
                print_r($data = json_encode($result));
            }
            else {
                print_r($data = json_encode( array("code"=>-1, "msg"=>'Supplier not permitted') ));
            }
        }
        else if ($mode=='vrc') {

            if ($id1=='batch') {
                // If {VRC} submitted is 'batch', read for new VRC that is not sent.
                $batch = $this->Api_model->challenger_po('VRC-CLAIM', ['SA0006'] );
                if (count($batch)==0) {
                    print_r($data = json_encode(array("code" => -1, "msg" => 'No pending VRC')));
                    exit;
                }
                else {
                    $id1 = $batch['jour_trans_id'];
                }
            }

            // Trigger API - Send VRC to supplier
            $list = $this->Api_model->challenger_po('VRC',$id1);
            if ($list) {
                if ($list['supp_id']=="SA0006") {
                    $this->load->model('cherps_supplier/Sa0006_model');
                    $sendapi = $this->Sa0006_model->send_vrc($list['trans_id']);
                    $stat = ($sendapi['code']==1) ? 'SUCCESS' : 'FAILED' ;
                    $call = $this->Api_model->cherps_email( $sendapi['message'] , 'Tradeshift upload - '.$id1.' '.$stat, 'meihui@challenger.sg; li.liangze@challenger.sg; yongsheng@challenger.sg; ');
                }
                print_r($data = json_encode($sendapi));
            }
            else {
                print_r($data = json_encode( array("code"=>-1, "msg"=>'No VRC found') ));
            }
        }
        else  {

            /*
             *  Send generated PO to vendors (scheduled batch job)
             *  Will match back with b2b_po_message to ensure no duplicate sending
             */

            $this->load->model('cherps/Pogrn_model');
            $supp_id_list = ['SH0004','LS0003','BL0001']; // List of suppliers set up for service

            $log = array();
            $handle_path = FCPATH . 'assets/uploads/';
            $uploaded = 'ZZ9999_CTLPOSEND.json';

            if (file_exists($handle_path . $uploaded)) {
                $filecontent = file_get_contents($handle_path . $uploaded);
                $filecontentarray = (array)json_decode($filecontent);
                $date = $filecontentarray["date"];
                $list = $filecontentarray["list"];
                $log[] = 'History ' . $uploaded . ' exist, with ' . count($list) . ' entries.';
            } else {
                $date = 0;
                $log[] = 'History ' . $uploaded . ' do not exist.';
            }

            if ($date < (time() - 3600)) {
                // If no file or file more than 1 hour - Generate new file
                $list = $this->Api_model->challenger_po('SUPPLIER',$supp_id_list);
                $date = time();
                $log[] = 'Found ' . count($list) . ' entries for Supp: ' . print_r($supp_id_list, true) . '.';
            }

            if (isset($list[0])) {
                $sendapi = $this->Pogrn_model->transmitPoVendor($list[0]->po_id, array("supp_id" => $list[0]->supp_id));
                $log[] = 'Processing 1 entry: ' . json_encode($list[0]);
                unset($list[0]);
                sort($list);
            } else {
                $result['message'] = 'No order found';
            }

            $filecontent = json_encode(array("list" => $list, "date" => $date));
            file_put_contents($handle_path . $uploaded, $filecontent);
            $result['last-check'] = date('Y-m-d H:i:s', $date);
            $log[] = json_encode($result);
            print_r($data = json_encode($result));
            if ($this->input->get('debug')) {
                foreach ($log as $l)
                    echo "<br> - " . $l;
            }

        }

    }

    public function challenger_notify($mode,$usr_id,$id) {

        $log = array();

        $this->load->model('cherps/Cherps_model');
        $req = '';
        $rtn = array("code"=>0,"msg"=>'No notify mode');

        if ($mode=="po") {
            $po_id = $id;
            $po =  $this->Cherps_model->get_po_list($po_id);
            $log[] = ($po) ? 'PO entry found for ID '.$po_id : 'PO entry not found for ID '.$po_id;

            if ($po){
                $logcheck = $this->Api_model->logcheckdb( array('trans_type'=>'PO','trans_id'=>$po_id,'url_link'=>base_url("api/challenger_notify/$mode/$usr_id/$id"),'last_created'=>-60 ) );
                $log[] = 'PO triggered in past 60 mins: ' . $logcheck['cnt'];
                if ($logcheck['cnt']>0){
                    $rtn = array("code"=>0,"msg"=>'You cannot trigger same notification in 60 minutes (Last trigger: '.$logcheck['created_on'].').',"mode"=>$mode);
                }
                else {
                    $url = $this->Cherps_model->build_url($mode, $usr_id, trim($po['po_id']), $po['status_level']);
                    $usr = $this->Api_model->cherps_usr($usr_id);

                    $po_id_rev = ($po['current_rev'] > 0) ? $po_id . 'R' . $po['current_rev'] : $po_id;
                    $subj = "PO $po_id_rev approval required";
                    $msg = "Purchase Order: " . $po_id_rev . " to: " . $po['supp_name'] . "\n\nYour approval is required.";
                    $msgemail = $msg . "\n\nPlease use link below see PO details:\n" . $url . "\n(Only available on Mobile view)";

                    $req = json_encode([
                        "po_id" => $po_id_rev,
                        "supp_id" => $po['supp_id'],
                        "url" => $url
                    ]);

                    $log[] = ($usr) ? 'User entry found for User ID ' . $usr_id : 'User entry not found for ID ' . $usr_id;
                    $log[] = 'CHERPS Mobile URL: ' . $url;

                    if ($usr) {
                        $method = [];

                        $call = $this->Api_model->cherps_email($msgemail, $subj, $usr['email_addr']);
                        $log[] = 'Email Call: ' . json_encode($call);
                        if ($call)
                            $method[] = 'Email';

                        if ($usr['notify_pn'] >= 1) {
                            $call = $this->Cherps_model->pn_notify($subj, $url, $usr['staff_id'], $msg);
                            $log[] = 'PN Call: ' . json_encode($call);
                            if ($call)
                                $method[] = 'Push notification';
                        }

                        $rtn = array("code" => 1, "msg" => 'Notification triggered.', "method" => $method, "mode" => $mode, "link" => $url);
                    } else {
                        $rtn = array("code" => 0, "msg" => 'User not found, no notification sent.', "mode" => $mode);
                    }
                }
            }
            else {
                $rtn = array("code"=>0,"msg"=>'PO not found, no notification sent',"mode"=>$mode);
            }

        }
        else if ($mode=="pr") {
            $pr_id = $id;
            $pr =  $this->Cherps_model->get_pr_list($pr_id);
            $log[] = ($pr) ? 'PR entry found for ID '.$pr_id : 'PR entry not found for ID '.$pr_id;

            if ($pr){
                $logcheck = $this->Api_model->logcheckdb( array('trans_type'=>'PR','trans_id'=>$pr_id,'url_link'=>base_url("api/challenger_notify/$mode/$usr_id/$id"),'last_created'=>-60 ) );
                $log[] = 'PR triggered in past 60 mins: ' . $logcheck['cnt'];
                if ($logcheck['cnt']>0){
                    $rtn = array("code"=>0,"msg"=>'You cannot trigger same notification in 60 minutes (Last trigger: '.$logcheck['created_on'].').',"mode"=>$mode);
                }
                else {
                    $url = $this->Cherps_model->build_url($mode, $usr_id, trim($pr['pr_id']), $pr['status_level']);
                    $usr = $this->Api_model->cherps_usr($usr_id);

                    $pr_id_rev = ($pr['current_rev'] > 0) ? $pr_id . 'R' . $pr['current_rev'] : $pr_id;
                    $subj = "PR $pr_id_rev approval required";
                    $msg = "PR: " . $pr_id_rev . " to: " . $pr['supp_name'] . "\n\nYour approval is required.";
                    $msgemail = $msg . "\n\nPlease use link below see PR details:\n" . $url . "\n(Only available on Mobile view)";

                    $req = json_encode([
                        "po_id" => $pr_id_rev,
                        "supp_id" => $pr['supp_id'],
                        "url" => $url
                    ]);

                    $log[] = ($usr) ? 'User entry found for User ID ' . $usr_id : 'User entry not found for ID ' . $usr_id;
                    $log[] = 'CHERPS Mobile URL: ' . $url;

                    if ($usr) {
                        $method = [];

                        $call = $this->Api_model->cherps_email($msgemail, $subj, $usr['email_addr']);
                        $log[] = 'Email Call: ' . json_encode($call);
                        if ($call)
                            $method[] = 'Email';

                        if ($usr['notify_pn'] >= 1) {
                            $call = $this->Cherps_model->pn_notify($subj, $url, $usr['staff_id'], $msg);
                            $log[] = 'PN Call: ' . json_encode($call);
                            if ($call)
                                $method[] = 'Push notification';
                        }

                        $rtn = array("code" => 1, "msg" => 'Notification triggered.', "method" => $method, "mode" => $mode, "link" => $url);
                    } else {
                        $rtn = array("code" => 0, "msg" => 'User not found, no notification sent.', "mode" => $mode);
                    }
                }
            }
            else {
                $rtn = array("code"=>0,"msg"=>'PO not found, no notification sent',"mode"=>$mode);
            }

        }
        $log[] = 'Return: '. json_encode($rtn);

        $params['trans_type'] = strtoupper($mode); // 'PO';
        $params['trans_id'] = $id; //$po_id;
        $params['url_link'] = base_url("api/challenger_notify/$mode/$usr_id/$id");
        $params['msg_request'] = $req;
        $params['status_code'] = $rtn['code'];
        $params['status_text'] = ($rtn['code']==1) ? 'OK' : 'XOK';
        $params['msg_response'] = json_encode($rtn);
        $params['created_by'] = substr(getRealIpAddr(),0,15); //'CHVOICES-API';
        $this->Api_model->logwritedb($params);
        $log[] = 'DB entry written by: '. $params['created_by'];

        echo json_encode($rtn);

        if ($this->input->get('debug')) {
            echo "<hr>";
            foreach($log as $l)
                echo "<br> - " . $l;
        }
    }

    public function challenger_inventory($supp_id='') {
        if ($supp_id=='ER0001') {
            $this->load->model('cherps_supplier/Er0001_model');
            $result['message'] = $this->Er0001_model->inventory_sync();
        }
        echo json_encode($result);
    }

    public function challenger_mis($usr_id,$coy_id='CTL'){
        $this->load->model('cherps/Cherps_model');

        if ($this->input->post('by') && $this->input->post('pin')) {
            $email = $this->input->post('by');
            $pin = $this->input->post('pin');
            $admin_id = $this->Cherps_model->usr_verifyadmin($email,$pin);
            if (!$admin_id){
                echo json_encode( array("code"=>0,"msg"=>"Fail MIS ADMIN Login.") );
                exit;
            }

            $admin_action = $this->Cherps_model->usr_adminstration($usr_id, $admin_id['usr_id']);
            echo json_encode( array("code"=>1,"admin"=>$admin_id['usr_id'],"user"=>$usr_id,"actions"=>$admin_action));
            exit;
        }

        $data = $this->Cherps_model->usr_get_detail($usr_id,$coy_id);
        echo json_encode($data);
    }

    public function challenger_txn($mode,$id){
        $this->load->model('cherps/Cherps_model');
        $data = $this->Cherps_model->get_pos_trans($mode,$id);
        echo json_encode($data);
    }


    /*
     *  Forwarding-call APIs. Intermediate API for internal-systems to call actual external-URL, usually triggered by POS.
     */


    // Quick way to call stored-procedure. Triggered by rundeck scheduler.
    public function cherps_db($sp){
        if ($sp=="loginlog" && $_SERVER['SERVER_NAME']=="chvoices-test.challenger.sg") {
            $shostname = $this->input->get('hostname');
            redirect('http://10.0.30.168/api/cherps_db/loginlog?hostname='.$shostname);
        }
        //$this->Api_model->logwritegw( array("gateway_id"=>'cherps_db',"trans_id"=>'',"request_link"=>'api/cherps_db',"request_msg"=>$sp) );
        if (!challengerIpAddr() && $sp!="loginlog" && $sp!="o2o_sp_live_product_generate") {
            $msg = 'Unauthorized access to ' . base_url('cherps_db/'.$sp) . ' from ' . getRealIpAddr(15);
            $call = $this->Api_model->cherps_email($msg, 'Unauthorized access to fx cherps_db', 'yongsheng@challenger.sg');

            $result = array(
                "code" => '-1',
                "messages" => 'Unauthorized access location. Please make sure you are at a whitelisted IP location.'
            );
            print_r($data = json_encode($result));
            exit;
        }
        
        ini_set('display_errors', 1);
        ini_set('max_execution_time', 300);

        if ($sp=="o2o_sp_live_product_generate") {
            $sp = " msdb.dbo.sp_start_job @job_name='Hachi- EOD o2o_sp_live_product_generate'";
            $result = $this->Api_model->cherps_db($sp,0);
            $result = array('msg'=>'Job running. Please wait for results in email');
        }
        else if ($sp=="loginlog") {
            $shostname = $this->input->get('hostname');
            $sipaddr = getRealIpAddr();
            $sp = "sp_sys_login_list '" . $shostname . "','". $sipaddr ."' ";
            $result = $this->Api_model->cherps_db($sp,0);
            echo '<center><h1>Opening CHERPS...</h1>Your entry ('.$shostname.') has been logged successfully '.$sipaddr.'</center>';
            exit;
        }
        else {
            $result = $this->Api_model->cherps_db($sp,1);
        }
        print_r($data = json_encode($result));
    }

    // Forward Ingram ASN to Lambda for processing (https://api.hachi.tech/delivery-asn)
    public function challenger_asn() {

        $apikey = $_SERVER['HTTP_X_API_KEY'];
        $segments='delivery-asn';

        if ($apikey!=''){
            log_message('debug', 'User '.$apikey.' accessing '.$segments);

            $callUrl = $this->apipath.$segments;

            $xmlcontent = trim(file_get_contents('php://input'));
            if (strlen($xmlcontent)>10) {

                $headers = array(
                    "Content-type: application/xml;",
                    "X-API-KEY:" . $apikey
                );

                //setting the curl parameters.
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $callUrl);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlcontent);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
                $data = curl_exec($ch);
                curl_close($ch);

                //convert the XML result into array
                echo $data;
                exit;

            }
        }

    }

    // Forward to API to trigger email (https://www.hachi.tech/challenger_email/ctl_welcome?xxx)
    public function pos_email() {
        $url = $this->hachipath . 'challenger_email/ctl_welcome?';
        foreach ($_GET as $k=>$val){
            if ($k!="f") {
                $url .= $k.'='.$val.'&';
            }
        }

        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlcontent);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);

        //convert the XML result into array
        $data = trim_all(strip_tags($data));
        echo $data;
        exit;

    }

    // Forward to API to retrieve Token (https://www.hachi.tech/api/vc/POSGetToken/xxmbr/xxpts/xxopt)
    public function posgettoken($mbr_id,$points,$otp) {
        $url = $this->hachipath . 'api/vc2/POSGetToken/'.$mbr_id.'/'.$points.'/'.$otp;
        $headers = array("X_AUTHORIZATION: NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG");

        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlcontent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);

        $this->Api_model->logwritegw( array("gateway_id"=>'posgettoken',"trans_id"=>'',"request_link"=>'api/posgettoken',"request_msg"=>$url, "return_msg"=>$data) );

        //convert the XML result into array
        $data = trim_all(strip_tags($data));
        echo $data;
        exit;

    }

    public function sendEmail(){
        $sql = "SELECT distinct supp_id
        FROM public.v_james_grn_unmatched_list
        where created_on >= date_trunc('month', current_date - interval '3' month)
	    and created_on < date_trunc('month', current_date - interval '0' month)";//only retrieve last month, based on current month

        $suppList = $this->db->query($sql)->result();

        foreach($suppList as $row){
            $sql = "SELECT *
            FROM public.v_james_grn_unmatched_list
            where supp_id = '$row->supp_id' and po_type != 'CR'
            and created_on >= date_trunc('month', current_date - interval '3' month)
	        and created_on < date_trunc('month', current_date - interval '0' month)";//only retrieve last month, based on current month

            $grnItem = $this->db->query($sql)->result();

            $grnItemTable = "<table border = 1><tr>
            <th>date_received</th><th>supp_do</th><th>grn_id</th><th>ref_id</th>
            <th>item_id</th><th>item_desc</th><th>grn_qty</th><th>unit_price</th> 
            <th>supp_name</th><th>line_amount</th><th>buyer_id</th>
            </tr>";

            $recipients = "";
            $copy_recipients = "";
            $buyer_email = "";
            $rowCount = 1;
            $totalAmount = 0;
            foreach($grnItem as $item){
                $grnItemTable .= "<tr>";
                $grnItemTable .= "<td>".$item->date_received  ."</td>";
                $grnItemTable .= "<td>".$item->supp_do        ."</td>";
                $grnItemTable .= "<td>".$item->grn_id         ."</td>";
                $grnItemTable .= "<td>".$item->ref_id         ."</td>";
                $grnItemTable .= "<td>".$item->item_id        ."</td>";
                $grnItemTable .= "<td>".$item->item_desc      ."</td>";
                $grnItemTable .= "<td>".$item->grn_qty        ."</td>";
                $grnItemTable .= "<td>".$item->unit_price     ."</td>"; 
                $grnItemTable .= "<td>".$item->supp_name      ."</td>";
                $grnItemTable .= "<td>".$item->line_amount    ."</td>";
                $grnItemTable .= "<td>".$item->buyer_id       ."</td>";
                $grnItemTable .= "</tr>";
                $totalAmount += ($item->grn_qty * $item->unit_price);
                if($buyer_email != $row->buyer_email){
                    $copy_recipients = $row->buyer_email .";";
                    $buyer_email = $row->buyer_email;
                }
                if($rowCount == count($grnItem)){
                    $recipients .= $item->email_addr3;
                }
                $rowCount++;
            }
            $grnItemTable .= "</table>";
            echo $grnItemTable;
            echo "recipients: ".$recipients ."<br>";
            echo "copy_recipients: ".$copy_recipients;
            
            $body = "Dear Vendor<br> Please help to complete the below PO matching.<br>
            Total outstanding amount: <b>$$totalAmount</b><br><br>$grnItemTable<br>
            <br>This is an automate email please do not reply.<br>
            Please email to zul for any clarification!<br><br>Regards<br>James<br>";

            $period = date("M Y", strtotime ( '-1 month', strtotime(date('M Y'))));
            $postData = ["recipients"=> "$recipients",
                        // "recipients"=> "james.chen@challenger.sg",
                        "copy_recipients"=> "$copy_recipients,zulaiha@challenger.sg,james.chen@challenger.sg",
                        // "copy_recipients"=> "james.chen@challenger.sg",
                        "subject"=> "Unmatched PO in $period",
                        "body"=> $body,
                    ];

            //cURL to send email
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://ctlmailer.api.valueclub.asia/send',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: 8f413c39b4d1b4e7c584943df22e7a8c'
                ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            echo $response;
        }
    }

    /*
     *  Depreciated functions
     */

    public function delivery_track($dlvr_mode='RFH',$step=0) {

        if ($dlvr_mode=='RFH') {
            // Refresh o2o_delivery_log table with orders
            $supp_id = 'BI0007';
            $result = $this->Api_model->o2o_delivery_log_refresh($supp_id);
        }
        else if ($dlvr_mode=='NQX') {
            $this->load->model('api/Courex_model');
            $result = $this->Courex_model->check_delivery();
        }
        print_r($data = json_encode($result));

    }

    public function qrscan($action,$key) {
        $keyarr = explode("---",$key);

        $params["coy_id"] = strtoupper($keyarr[0]);
        $params["ref_id"] = strtoupper($keyarr[1]);
        $params["loc_id"] = strtoupper($keyarr[2]);
        $params["pos_id"] = strtoupper($keyarr[3]);
        $params["mbr_id"] = strtoupper($keyarr[4]);

        $mbr_params["coy_id"] = $params["coy_id"];
        $mbr_params["mbr_id"] = $params["mbr_id"];
        $mbr = $this->Api_model->get_mbrid($mbr_params);
        if ($mbr) {
            $result = $this->Api_model->qrscan($params);
            print_r($data = json_encode($mbr));
        }
    }

    public function pos_qr(){
        $this->load->view('api/pos_qr');
    }
}
