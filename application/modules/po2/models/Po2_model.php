<?php

!defined('BASEPATH') OR ( 'No direct script access allowed');

class Po2_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }

    public function paging($page = NULL, $limit = NULL) {
        $page = $this->input->get('page');
        $limit = $this->input->get('limit');
        if (!empty($page) && !is_numeric($page))
            $this->invalid_params('page');
        if (!empty($limit) && !is_numeric($limit))
            $this->invalid_params('limit');
        if (!isset($page) || empty($page) || $page <= 0)
            $page = DEFAULT_PAGE;
        if (!isset($limit) || empty($limit) || $limit <= 0)
            $limit = DEFAULT_LIMIT;
        return array(
            "page" => $page,
            "limit" => $limit
        );
    }

    public function get_all_po_list(array $params) {
        $status_level = (isset($params["status_level"]) && $params["status_level"] != NULL) ? $params["status_level"] : "1";
        if (isset($params["show_confirmed"]) && $params["show_confirmed"]==1) {
            $sql_status = " AND (pms_po_list.status_level=1 OR (pms_po_list.status_level=2 AND pms_po_list.current_rev <= 1) ) ";
        }
        else {
            $sql_status = " AND pms_po_list.status_level IN ($status_level)";
        }
        $sql_search = '';
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (pms_po_list.po_id LIKE '%" . $params["search"] . "%' OR pms_po_list.buyer_id LIKE '%" . $params["search"] . "%')" : "";
        $sql_search.= (isset($params["search_to"]) && $params["search_to"] != NULL) ? " AND pms_po_list.po_date <= '" . date('Y-m-d H:i:s',strtotime($params["search_to"] . "+1 days")) . "'" : "";
        $sql_search.= (isset($params["search_from"]) && $params["search_from"] != NULL) ? " AND pms_po_list.po_date >= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_from"]))) . "'" : "";
        $sql = "SELECT DISTINCT pms_po_list.po_id, 
                    pms_po_list.coy_id, 
                    (SELECT SUM(pms_po_item.qty_order*pms_po_item.unit_price) FROM pms_po_item WHERE pms_po_item.po_id=pms_po_list.po_id and pms_po_item.rev_num=pms_po_list.rev_num GROUP BY pms_po_item.po_id) as amount,
                    (SELECT COUNT(pms_po_item.po_id) FROM pms_po_item WHERE pms_po_item.po_id=pms_po_list.po_id and pms_po_list.coy_id = pms_po_item.coy_id and pms_po_item.rev_num=pms_po_list.rev_num) as line,
                    pms_po_list.rev_num,
                    TO_CHAR(pms_po_list.po_date,'DD-MM-YYYY') po_date, --CONVERT(VARCHAR(10), CAST(pms_po_list.po_date AS DATE), 105) AS po_date,
                    TO_CHAR(pms_po_list.delv_date,'DD-MM-YYYY') delv_date, --CONVERT(VARCHAR(10), CAST(pms_po_list.delv_date AS DATE), 105) AS delv_date,
                    pms_po_list.buyer_id,
                    pms_po_list.approve_by,
                    TO_CHAR(pms_po_list.approve_date,'DD-MM-YYYY') approve_date, --CONVERT(VARCHAR(10), CAST(pms_po_list.approve_date AS DATE), 105) AS approve_date,
                    sys_status_list.status_desc,
                    pms_po_list.loc_id,
                    pms_po_list.remarks,
                    pms_supplier_list.curr_id,
                    (CASE WHEN pms_po_list.loc_id='DSH' THEN
                        coalesce((SELECT invoice_id FROM sms_invoice_item WHERE coy_id='CTL' and trans_id=pms_po_list.po_id ORDER BY modified_on DESC LIMIT 1),'')
                        ELSE '' END) invoice
                FROM  pms_po_list 
                    JOIN pms_po_item ON pms_po_list.po_id = pms_po_item.po_id 
                    JOIN pms_supplier_list ON pms_supplier_list.supp_id = pms_po_list.supp_id and pms_supplier_list.coy_id = pms_po_list.coy_id
                    JOIN sys_status_list ON sys_status_list.status_level = pms_po_list.status_level AND sys_status_list.ref_id = 'po_list'
                WHERE pms_po_list.supp_id = ltrim(rtrim('" . $params["supp_id"] . "')) $sql_status
                AND pms_po_list.rev_num = pms_po_list.current_rev 
                AND pms_po_list.coy_id ='" . Base_Common_Model::COMPANY_ID . "'" . $sql_search . " ORDER BY pms_po_list.po_id ASC";
        //echo"<pre>";print_r($sql);exit;
        $result = CI::db()->query($sql)->result_array();
        //echo"<pre>";print_r($this->db->last_query());exit;
        if ($result) {
            return $result;
        }
        return FALSE;
    }

    public function get_all_po_items($params){
        $sql = "SELECT pms_po_item.line_num,
                    pms_po_item.item_id,
                    pms_po_item.item_desc,
                    pms_po_item.qty_order,
                    pms_po_item.uom_id,
                    pms_po_item.qty_received,
                    pms_po_item.unit_price,
                    pms_po_item.supp_item_id,
                    (qty_order*unit_price) AS amount,
                    pms_po_list.status_level,
                    pms_po_item.coy_id,
                    pms_po_item.po_id,
                    pms_po_item.rev_num
		        FROM pms_po_item
                    LEFT JOIN pms_po_list ON pms_po_list.po_id = pms_po_item.po_id and pms_po_list.coy_id = pms_po_item.coy_id
                WHERE pms_po_item.po_id = rtrim('" . $params["po_id"] . "')
                AND pms_po_item.rev_num = pms_po_list.current_rev
                AND pms_po_list.rev_num = pms_po_list.current_rev 
                AND pms_po_item.coy_id ='" . Base_Common_Model::COMPANY_ID . "'
                ORDER BY line_num ASC";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }
        return FALSE;
    }

    public function do_po_confirmation($params) {

        $check_qtyinvalid = false; $check_revised = false; $check_newtotal = 0;

        $new_items = [];
        foreach($params['items'] as $itm) {
            if ($itm['new_qty'] > $itm['qty_ordered'] || $itm['qty_ordered'] < 0)
                $check_qtyinvalid = true;
            else if ($itm['new_qty'] != $itm['qty_ordered'])
                $check_revised = true;

            if ($itm['new_qty'] > 0) {
                $check_newtotal += $itm['new_qty'];
                $new_items[$itm['line_num']] = $itm['new_qty'];
            }
        }

        if (strtotime($params['delv_date']) < time() - 86400) {
            // Check delv_date
            return array('return' => '0', 'message' => 'Invalid date ' . $params['delv_date'], 'po_id' => $params["po_id"]);
        }
        else if ($check_qtyinvalid) {
            // Check new_qty submitted
            return array('return' => '0', 'message' => 'New Qty cannot be higher than Qty Order', 'po_id' => $params["po_id"]);
        }
        else {

            $supp_id = ($this->session->userdata('cm_supp_id')) ? $this->session->userdata('cm_supp_id') : 'chvoices2pg';
            $new_date = date('Y-m-d H:i:s');

            if ($check_newtotal <= 0) {
                // No qty, revise as deleted PO

                $sql = "UPDATE pms_po_list SET status_level=-2, modified_by='$supp_id', modified_on='$new_date' 
                            WHERE coy_id='" . $params["coy_id"] . "' AND po_id='" . $params["po_id"] . "'";
                CI::db()->query($sql);

                $sql = "UPDATE pms_po_item SET status_level=-2, modified_by='$supp_id', modified_on='$new_date' 
                            WHERE coy_id='" . $params["coy_id"] . "' AND po_id='" . $params["po_id"] . "'";
                CI::db()->query($sql);

                $return = array('return' => '1', 'message' => 'Revise PO Confirmed', 'po_id' => $params["po_id"]);
            }
            else if ($check_revised) {
                // Revised PO

                // 1a. Get header
                $sql = "SELECT * FROM pms_po_list WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num=current_rev ORDER BY rev_num DESC";
                $po_header = CI::db()->query($sql)->result_array();
                if ($po_header) {
                    // 1b. Delete all header
                    $old_rev = $po_header[0]['current_rev'];
                    $new_rev = $po_header[0]['current_rev'] + 1;
                    $sql = "UPDATE pms_po_list SET status_level=-2, current_rev=" . $new_rev . " WHERE coy_id='" . $params["coy_id"] . "' AND po_id='" . $params["po_id"] . "'";
                    CI::db()->query($sql);
                    // 1c. Prepare new header info
                    $po_header[0]['rev_num'] = $new_rev;
                    $po_header[0]['current_rev'] = $new_rev;
                    $po_header[0]['status_level'] = 2;
                    $po_header[0]['created_by'] = $supp_id;
                    $po_header[0]['created_on'] = $new_date;
                    $po_header[0]['modified_by'] = $supp_id;
                    $po_header[0]['modified_on'] = $new_date;
                    // 1d. Insert new header
                    CI::db()->insert('pms_po_list', $po_header[0]);

                    // 2a. Get all items in latest PO only
                    $sql = "SELECT * FROM pms_po_item WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num=$old_rev ORDER BY line_num";
                    $po_items = CI::db()->query($sql)->result_array();
                    // 2b. Delete all items
                    $sql = "UPDATE pms_po_item SET status_level=-2 WHERE coy_id='" . $params["coy_id"] . "' AND po_id='" . $params["po_id"] . "'";
                    CI::db()->query($sql);
                    // 2c. Foreach row, insert into db
                    foreach($po_items as $itm){
                        $new_qty = (isset($new_items[$itm['line_num']])) ? $new_items[$itm['line_num']] : 0;
                        if ($new_qty > 0) {
                            $itm['rev_num'] = $new_rev;
                            $itm['qty_order'] = $new_qty;
                            $itm['status_level'] = 1;
                            $itm['created_by'] = $supp_id;
                            $itm['created_on'] = $new_date;
                            $itm['modified_by'] = $supp_id;
                            $itm['modified_on'] = $new_date;
                            CI::db()->insert('pms_po_item', $itm);
                        }
                    }

                    $return = array('return' => '1', 'message' => 'Revise PO Confirmed', 'po_id' => $params["po_id"]);
                }
                else {
                    // No PO Found
                    return array('return'=>'0','message'=>'No valid PO', 'po_id'=>$params["po_id"]);
                }
            }
            else {
                // Confirm PO
                $params_upd = array(
                    'status_level'=> '2',
                    'coy_id'=> $params["coy_id"],
                    'po_id'=> $params["po_id"],
                    'rev_num'=> $params["rev_num"],
                    'modified_on'=> $new_date,
                    'modified_by'=> $supp_id
                );
                $updated = $this->update_po_list($params_upd);
                if ($updated) {
                    $params_upd = array(
                        'status_level' => '1',
                        'coy_id' => $params["coy_id"],
                        'po_id' => $params["po_id"],
                        'rev_num' => $params["rev_num"],
                        'modified_on' => $new_date,
                        'modified_by' => $supp_id
                    );
                    $this->update_po_item($params_upd);

                    $return = array('return' => '1', 'message' => 'PO Confirmed', 'po_id' => $params["po_id"]);
                }
                else {
                    // No found
                    return array('return'=>'0','message'=>'No valid PO', 'po_id'=>$params["po_id"]);
                }
            }

            // :: After confirmed actions ::
            if ($return['return']=='1') {
                if (substr($params["po_id"],0,2)=="HP" && $return['message']=='Revise PO Confirmed') {
                    // If revised & po_type=HP, trigger email
                    $by_supp_name = ($this->session->userdata('cm_supp_name')) ? ' By ' . $this->session->userdata('cm_supp_name') : '';
                    $email_param["from"] = "chvoices@challenger.sg";
                    $email_param["to"] = "HQ.CustServiceGroup; mrd.hc@challenger.sg; joshua-woon@challenger.sg; kunalan@challenger.sg; ";
                    $email_param["subject"] = "PO revised by supplier - " . $params["po_id"];
                    $email_param["message"] = "PO ".$params["po_id"]." revised" . $by_supp_name;
                    $this->Po2_model->CallSP_email($email_param);
                }
            }

            return $return;
        }
    }

    public function update_po_list($params) {
        $supp_id = $this->session->userdata('cm_supp_id');
        $sql_set = (isset($params["delv_date"])) ? "delv_date='".$params["delv_date"]."', " : '' ;
        $sql_set.= (isset($params["status_level"])) ? "status_level='".$params["status_level"]."', " : '' ;
        $sql_whr = (isset($params["coy_id"])) ? " AND coy_id='".$params["coy_id"]."' " : '' ;
        $sql_whr.= (isset($params["rev_num"])) ? " AND rev_num='".$params["rev_num"]."' " : '' ;
        $sql = "UPDATE pms_po_list SET " . $sql_set
            . "modified_by='" . $supp_id . "',"
            . "modified_on='" . date("Y-m-d H:i:s") . "'"
            . " WHERE po_id IN ('" . $params["po_id"] . "') " . $sql_whr;
        $result = CI::db()->query($sql);
        return $result;
    }

    public function update_po_item($params) {
        $supp_id = $this->session->userdata('cm_supp_id');
        $sql_set = (isset($params["qty_order"])) ? "delv_date='".$params["qty_order"]."', " : '' ;
        $sql_set.= (isset($params["status_level"])) ? "status_level='".$params["status_level"]."', " : '' ;
        $sql_whr = (isset($params["coy_id"])) ? " AND coy_id='".$params["coy_id"]."' " : '' ;
        $sql_whr.= (isset($params["rev_num"])) ? " AND rev_num='".$params["rev_num"]."' " : '' ;
        $sql_whr.= (isset($params["line_num"])) ? " AND line_num='".$params["line_num"]."' " : '' ;
        $sql = "UPDATE pms_po_item SET " . $sql_set
            . "modified_by='" . $supp_id . "',"
            . "modified_on='" . date("Y-m-d H:i:s") . "'"
            . " WHERE po_id IN ('" . $params["po_id"] . "') " . $sql_whr;
        $result = CI::db()->query($sql);
        return $result;
    }

    // Commented out - replace by do_po_confirmation
    public function revise_po($params, $po_qty) {

        //  Prepares SQL query here and execute.
        //    If no items in po_qty, will insert header & set as cancelled (-1)
        //    Else will copy header & valid items, set as confirmed (2)

        $sql_linenum = "";
        foreach ($po_qty as $line=>$qty)
            $sql_linenum[] = $line;

        $supp_id = $this->session->userdata('cm_supp_id');
        $temp_table = 'temp_po_' . rand(1000,9999);
        $new_status = (count($po_qty)==0) ? "-1" : "2" ;

        $sql = "
UPDATE pms_po_list SET current_rev='".($params["rev_num"]+1)."',status_level='-2',modified_by='".$supp_id."',modified_on=CURRENT_TIMESTAMP 
    WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."';
SELECT * INTO TEMP ".$temp_table." FROM pms_po_list 
    WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num='".$params["rev_num"]."';
UPDATE ".$temp_table." SET rev_num='".($params["rev_num"]+1)."',status_level='$new_status',modified_by='".$supp_id."',modified_on=CURRENT_TIMESTAMP ;
INSERT INTO pms_po_list
    SELECT * FROM ".$temp_table."; ";
        if (count($po_qty)>0) {
            $sql.= "
UPDATE pms_po_item SET status_level='-2',modified_by='".$supp_id."',modified_on=CURRENT_TIMESTAMP 
    WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num='".$params["rev_num"]."';
SELECT * INTO TEMP ".$temp_table."I FROM pms_po_item 
    WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num='".$params["rev_num"]."'
        AND line_num IN (". implode(',',$sql_linenum) .") ;
UPDATE ".$temp_table."I SET rev_num='".($params["rev_num"]+1)."',status_level='1',modified_by='".$supp_id."',modified_on=CURRENT_TIMESTAMP ;
INSERT INTO pms_po_item
    SELECT * FROM ".$temp_table."I ; ";
            foreach ($po_qty as $line=>$qty) {
                $sql.= "
UPDATE pms_po_item SET qty_order=$qty
    WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num='".($params["rev_num"]+1)."' AND line_num=$line ; ";
            }
        }
        $result = CI::db()->query($sql);
        return $result;
    }

    public function pdf_list($params){
        $poinfo = $this->pdf_info($params);

        $polists = array(); $last_poid='';
        foreach ($poinfo as $info) {
            if ($last_poid!=$info["po_id"]) {
                $last_poid = trim($info["po_id"]);
            }
            $polists[$last_poid][] = $info;
        }
        return $polists;
    }

    public function pdf_info($params) {
        $sql_po = implode("','",$params["ids"]);
        $sql_supp = ($params["supp_id"]=='CHERPS') ? "" : " AND l.supp_id='".$params["supp_id"]."'";
        $sql = "SELECT l.po_id,l.current_rev,
                            TO_CHAR(l.po_date,'DD-MM-YYYY') po_date, --CONVERT(VARCHAR(10), CAST(l.po_date AS DATE), 105) AS po_date,
                            TO_CHAR(l.delv_date,'DD-MM-YYYY') delv_date, --CONVERT(VARCHAR(10), CAST(l.delv_date AS DATE), 105) AS delv_date, 
                            l.curr_id,l.payment_id, l.current_rev,l.status_level,sys_status_list.status_desc,
                        l.supp_id,l.supp_name,l.contact_person,l.tel_code,l.fax_code,l.email_addr, l.loc_id,l.loc_name,
                        (SELECT addr_text FROM coy_address_book WHERE coy_address_book.ref_id=l.supp_id AND coy_address_book.coy_id=l.coy_id AND coy_address_book.ref_type='SUPPLIER' AND coy_address_book.addr_type='0-PRIMARY' LIMIT 1) as supp_addr_text,
                        (SELECT addr_text FROM coy_address_book WHERE coy_address_book.ref_id=l.loc_id AND coy_address_book.coy_id=l.coy_id AND coy_address_book.addr_type='0-PRIMARY' LIMIT 1) as loc_addr_text,
                        i.line_num,i.item_id,i.item_desc,i.long_desc,i.qty_order,i.uom_id,i.unit_price,i.disc_percent,
                        l.remarks,
                        (SELECT qty_order FROM pms_po_item oi WHERE oi.coy_id=i.coy_id AND oi.po_id=i.po_id AND oi.line_num=i.line_num AND oi.rev_num='0' AND oi.item_id=i.item_id LIMIT 1) as original_qty,
                        (SELECT usr_name FROM sys_usr_list WHERE usr_id=lower(l.buyer_id) LIMIT 1) as buyer_id_usr,
                        (SELECT usr_name FROM sys_usr_list WHERE usr_id=lower(l.requester_id) LIMIT 1) as requester_id_usr,
                        (SELECT usr_name FROM sys_usr_list WHERE usr_id=lower(l.created_by) LIMIT 1) as created_by_usr,
                        (SELECT usr_name FROM sys_usr_list WHERE usr_id=lower(l.approve_by) LIMIT 1) as approve_by_usr
                    FROM pms_po_item i
                        JOIN pms_po_list l ON l.po_id=i.po_id and l.coy_id=i.coy_id
                        JOIN sys_status_list ON sys_status_list.status_level = l.status_level AND sys_status_list.ref_id = 'po_list'
                    WHERE i.po_id IN ('$sql_po') AND l.current_rev=l.rev_num AND i.rev_num=l.current_rev ".$sql_supp."
                    ORDER BY l.po_id,i.line_num";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }
        return FALSE;
    }

    public function CallSP_email(array $params) {
        $result = $this->_SendEmail($params);
        return $result;
    }

    public function get_all_pr_list(array $params) {
        $status_level = (isset($params["status_level"]) && $params["status_level"] != NULL) ? $params["status_level"] : "1";
        $sql_status = " AND pms_pr_list.status_level IN ($status_level)";

        $sql_search = '';
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (pms_pr_list.pr_id LIKE '%" . $params["search"] . "%' OR pms_pr_list.buyer_id LIKE '%" . $params["search"] . "%')" : "";
        $sql_search.= (isset($params["search_to"]) && $params["search_to"] != NULL) ? " AND pms_pr_list.pr_date <= '" . date('Y-m-d H:i:s',strtotime($params["search_to"] . "+1 days")) . "'" : "";
        $sql_search.= (isset($params["search_from"]) && $params["search_from"] != NULL) ? " AND pms_pr_list.pr_date >= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_from"]))) . "'" : "";
        $sql = "SELECT DISTINCT pms_pr_list.pr_id, 
                    pms_pr_list.coy_id, 
                    (SELECT SUM(pms_pr_item.qty_order*pms_pr_item.unit_price) FROM pms_pr_item WHERE pms_pr_item.pr_id=pms_pr_list.pr_id and pms_pr_item.rev_num=pms_pr_list.rev_num GROUP BY pms_pr_item.pr_id) as amount,
                    (SELECT COUNT(pms_pr_item.pr_id) FROM pms_pr_item WHERE pms_pr_item.pr_id=pms_pr_list.pr_id and pms_pr_list.coy_id = pms_pr_item.coy_id and pms_pr_item.rev_num=pms_pr_list.rev_num) as line,
                    pms_pr_list.rev_num,
                    TO_CHAR(pms_pr_list.pr_date,'DD-MM-YYYY') pr_date, --CONVERT(VARCHAR(10), CAST(pms_pr_list.pr_date AS DATE), 105) AS pr_date,
                    TO_CHAR(pms_pr_list.delv_date,'DD-MM-YYYY') delv_date, --CONVERT(VARCHAR(10), CAST(pms_pr_list.delv_date AS DATE), 105) AS delv_date,
                    pms_pr_list.buyer_id,
                    pms_pr_list.approve_by,
                    TO_CHAR(pms_pr_list.approve_date,'DD-MM-YYYY') approve_date, --CONVERT(VARCHAR(10), CAST(pms_pr_list.approve_date AS DATE), 105) AS approve_date,
                    sys_status_list.status_desc,
                    pms_pr_list.loc_id,
                    pms_pr_list.remarks,
                    pms_supplier_list.curr_id
                FROM  pms_pr_list 
                    JOIN pms_pr_item ON pms_pr_list.pr_id = pms_pr_item.pr_id 
                    JOIN pms_supplier_list ON pms_supplier_list.supp_id = pms_pr_list.supp_id and pms_supplier_list.coy_id = pms_pr_list.coy_id
                    JOIN sys_status_list ON sys_status_list.status_level = pms_pr_list.status_level AND sys_status_list.ref_id = 'po_list'
                WHERE pms_pr_list.supp_id = ltrim(rtrim('" . $params["supp_id"] . "')) $sql_status
                AND pms_pr_list.rev_num = pms_pr_list.current_rev 
                AND pms_pr_list.coy_id ='" . Base_Common_Model::COMPANY_ID . "'" . $sql_search . " ORDER BY pms_pr_list.pr_id ASC";
        //echo"<pre>";print_r($sql);exit;
        $result = CI::db()->query($sql)->result_array();
        //echo"<pre>";print_r($this->db->last_query());exit;
        if ($result) {
            return $result;
        }
        return FALSE;
    }

    public function pr_pdf_list($params){
        $poinfo = $this->pr_pdf_info($params);

        $polists = array(); $last_poid='';
        foreach ($poinfo as $info) {
            if ($last_poid!=$info["pr_id"]) {
                $last_poid = trim($info["pr_id"]);
            }
            $polists[$last_poid][] = $info;
        }
        return $polists;
    }

    public function pr_pdf_info($params) {
        $sql_po = implode("','",$params["ids"]);
        $sql_supp = ($params["supp_id"]=='CHERPS') ? "" : " AND l.supp_id='".$params["supp_id"]."'";
        $sql = "SELECT l.pr_id,l.current_rev, l.pr_type,
                            TO_CHAR(l.pr_date,'DD-MM-YYYY') pr_date, --CONVERT(VARCHAR(10), CAST(l.po_date AS DATE), 105) AS po_date,
                            TO_CHAR(l.delv_date,'DD-MM-YYYY') delv_date, --CONVERT(VARCHAR(10), CAST(l.delv_date AS DATE), 105) AS delv_date,
                            l.curr_id,l.payment_id, l.current_rev,l.status_level,sys_status_list.status_desc,
                        l.supp_id,l.supp_name,l.contact_person,l.tel_code,l.fax_code,l.email_addr, l.loc_id,l.loc_name,
                        (SELECT addr_text FROM coy_address_book WHERE coy_address_book.ref_id=l.supp_id AND coy_address_book.coy_id=l.coy_id AND coy_address_book.ref_type='SUPPLIER' AND coy_address_book.addr_type='0-PRIMARY' LIMIT 1) as supp_addr_text,
                        (SELECT addr_text FROM coy_address_book WHERE coy_address_book.ref_id=l.loc_id AND coy_address_book.coy_id=l.coy_id AND coy_address_book.addr_type='0-PRIMARY' LIMIT 1) as loc_addr_text,
                        i.line_num,i.item_id,i.item_desc,i.long_desc,i.qty_order,i.uom_id,i.unit_price,i.disc_percent,
                        l.remarks,
                        (SELECT qty_order FROM pms_pr_item oi WHERE oi.coy_id=i.coy_id AND oi.pr_id=i.pr_id AND oi.line_num=i.line_num AND oi.rev_num='0' AND oi.item_id=i.item_id LIMIT 1) as original_qty,
                        (SELECT usr_name FROM sys_usr_list WHERE usr_id=lower(l.buyer_id) LIMIT 1) as buyer_id_usr,
                        (SELECT usr_name FROM sys_usr_list WHERE usr_id=lower(l.requester_id) LIMIT 1) as requester_id_usr,
                        (SELECT usr_name FROM sys_usr_list WHERE usr_id=lower(l.created_by) LIMIT 1) as created_by_usr,
                        (SELECT usr_name FROM sys_usr_list WHERE usr_id=lower(l.approve_by) LIMIT 1) as approve_by_usr
                    FROM pms_pr_item i
                        JOIN pms_pr_list l ON l.pr_id=i.pr_id and l.coy_id=i.coy_id
                        JOIN sys_status_list ON sys_status_list.status_level = l.status_level AND sys_status_list.ref_id = 'pr_list'
                    WHERE i.pr_id IN ('$sql_po') AND l.current_rev=l.rev_num AND i.rev_num=l.current_rev ".$sql_supp."
                    ORDER BY l.pr_id,i.line_num";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }
        return FALSE;
    }

}

?>
