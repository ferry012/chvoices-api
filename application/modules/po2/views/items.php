<?php $po_amt = 0; ?>
<span id="InvbtnSpanDiv" >
    <a style="float:right" href="<?php echo base_url("po2/export/csv/" . $po_id); ?>" class="popuplink btn btn-warning m-l-10"  ><i class="fa fa-arrow-up"></i> Export CSV</a>
    <a style="float:right" href="<?php echo base_url("po2/export/pdf/" . $po_id); ?>" class="popuplink btn btn-warning m-l-10"  ><i class="fa fa-print"></i> Print PO</a>
</span>

<table id="datatable" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th class="h-mobile">No</th> 
            <th>Item ID</th>
            <th>Description</th>
            <th>Qty Order</th>
            <th class="h-mobile">UOM</th>
            <th class="h-mobile">Qty Received</th>
            <th>Unit Price</th>
            <th class="h-mobile">Supplier Item ID</th> 
            <th>Amount</th> 
        </tr>
    </thead>
    <tbody>
        <?php if ($results): ?>
            <?php foreach ($results as $result): ?>
                <?php
                $line_num = isset($result["line_num"]) ? $result["line_num"] : "";
                $item_id = isset($result["item_id"]) ? $result["item_id"] : "";
                $item_desc = isset($result["item_desc"]) ? $result["item_desc"] : "";
                $qty_order = isset($result["qty_order"]) ? $result["qty_order"] : "";
                $uom_id = isset($result["uom_id"]) ? $result["uom_id"] : "";
                $qty_received = isset($result["qty_received"]) ? $result["qty_received"] : "";
                $unit_price = isset($result["unit_price"]) ? $result["unit_price"] : "";
                $supp_item_id = isset($result["supp_item_id"]) ? $result["supp_item_id"] : "";
                $amount = isset($result["amount"]) ? $result["amount"] : "";
                $po_amt += $result["amount"];
                ?>
                <tr>
                    <td class="h-mobile"><?php echo $line_num; ?></td>
                    <td><?php echo $item_id; ?></td>
                    <td><?php echo $item_desc; ?></td>
                    <td>
                        <?php echo display_number($qty_order); ?>
                        <input type="hidden" class="po_item_newqty" value="<?php echo display_number($qty_order); ?>" data-unitprice="<?php echo number_format($unit_price, 4, '.', ''); ?>">
                    </td>
                    <td class="h-mobile"><?php echo $uom_id; ?></td>
                    <td class="h-mobile"><?php echo display_number($qty_received); ?></td>
                    <td><?php echo number_format($unit_price, 4, '.', ','); ?></td>
                    <td class="h-mobile"><?php echo $supp_item_id; ?></td>
                    <td><?php echo price_no_symbol($amount); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
    <?php if ($results): ?>
    <tfoot>
        <tr>
            <td class="h-mobile"></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="h-mobile"></td>
            <td class="h-mobile"></td>
            <td></td>
            <td class="h-mobile"></td>
            <td><b>$<?php echo price_no_symbol($po_amt); ?></b></td> 
        </tr>
    </tfoot>
    <?php endif; ?>
</table>

<script>
function KeepRunFxI(){
    $("#InvbtnSpanDiv").appendTo("#listing_porpt_items #datatable_length label"); 
}
var KeepRun = setTimeout( KeepRunFxI , 1200); 
</script>