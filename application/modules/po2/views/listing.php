<section class="content clearfix" id="Po2_Reprint">
    <?php echo theme_view('general/search_bar_filter'); ?>
    
    <div class="row hidden">
        <div class="col-xs-12">
            <div class="box box-danger " >
                <div class="box-header hidden">
                    <h3 class="box-title">PO Actions</h3>
                </div>
                <div class="box-body"></div> 
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row" id="listing_porpt">
        <div class="col-xs-12">
            <div class="box"> 
                <div class="hidden box-header">
                    <h3 class="box-title">PO Listings</h3>
                </div>
                <div class="box-body">       
                    <span id="ListInvbtnSpanDiv" >
                        <a style="float:right" href="#" data-type="csv" class="InvbtnSpanDiv_Export btn btn-warning m-l-10"  ><i class="fa fa-arrow-up"></i> Export CSV</a>
                        <a style="float:right" href="#" data-type="pdf" class="InvbtnSpanDiv_Export btn btn-warning m-l-10"  ><i class="fa fa-print"></i> Print PO</a>
                    </span>       
                    
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="no-sort"><span class="fa fa-check-square-o" id="sel_all_btn" title="Select all on this page"></span></th>
                                <th>PO ID / Rev</th> 
                                <th class="">PO Date</th>
                                <th class="">Delv Date</th>
                                <th class="no-sort">Curr</th>
                                <th class="">Buyer ID</th>
                                <th class="">Approve</th>
                                <th class="">Status</th>
                                <th class="">Loc ID</th>
                                <th class="">PO Amount</th>
                                <th class="no-sort">Remarks</th>
                                <th class="no-sort">Line</th>
                                <th class="no-sort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($results): ?>
                                <?php foreach ($results as $k => $result): ?>
                                    <?php
                                    $coy_id = isset($result["coy_id"]) ? trim($result["coy_id"]) : "";
                                    $po_id = isset($result["po_id"]) ? trim($result["po_id"]) : "";
                                    $rev_num = isset($result["rev_num"]) ? $result["rev_num"] : "";
                                    $po_date = isset($result["po_date"]) ? $result["po_date"] : "";
                                    $delv_date = isset($result["delv_date"]) ? $result["delv_date"] : "";
                                    $curr_id = isset($result["curr_id"]) ? $result["curr_id"] : "";
                                    $buyer_id = isset($result["buyer_id"]) ? $result["buyer_id"] : "";
                                    $approve_by = isset($result["approve_by"]) ? $result["approve_by"] : "";
                                    $approve_date = isset($result["approve_date"]) ? $result["approve_date"] : "";
                                    $status_desc = isset($result["status_desc"]) ? $result["status_desc"] : "";
                                    $loc_id = isset($result["loc_id"]) ? $result["loc_id"] : "";
                                    $line = isset($result["line"]) ? $result["line"] : "";
                                    $amount = isset($result["amount"]) ? $result["amount"] : "";
                                    $remarks = isset($result["remarks"]) ? $result["remarks"] : "";
                                    $invoice = isset($result["invoice"]) ? trim($result["invoice"]) : "";
                                    ?>
                                    <tr>
                                        <td class=""><input type="checkbox" name="po_checkbox" class="cb" id="<?php echo 'cb_' . $k; ?>" id2="<?php echo trim($po_id); ?>" name="po_selected[]" value="<?php echo $coy_id . '---' . $po_id . '---' . $rev_num; ?>" ></td>
                                        <td>
                                            <?php echo $po_id; ?>
                                            <small><br>Rev: <?php echo $rev_num; ?></small>
                                        </td>
                                        <td data-sort="<?php echo strtotime($po_date); ?>"><?php echo $po_date; ?></td>
                                        <td data-sort="<?php echo strtotime($delv_date); ?>"><?php echo $delv_date; ?></td>
                                        <td class="h-mobile"><?php echo $curr_id; ?></td>
                                        <td class="h-mobile"><?php echo $buyer_id; ?></td>
                                        <td class="h-mobile" data-sort="<?php echo strtotime($approve_date); ?>">
                                            <?php echo $approve_date; ?>
                                            <small><br>By: <?php echo $approve_by; ?></small>
                                        </td>
                                        <td><?php echo $status_desc; ?></td>
                                        <td class="h-mobile"><?php echo $loc_id; ?></td>
                                        <td><?php echo price_no_symbol($amount); ?></td>
                                        <td class="h-mobile">
                                            <?php if ($invoice!=''): ?>
                                                <a href="<?php echo $invoice; ?>" target="_blank"><?php echo trim($remarks); ?></a>
                                            <?php else: ?>
                                                <?php echo trim($remarks); ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="h-mobile"><?php echo $line; ?></td>
                                        <td>
                                            <a class="btn btn-info btn-sm ViewPoItems" id="<?php echo trim($po_id); ?>" href="#">Items</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="listing_porpt_items">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header hidden">
                    <h3 class="box-title">PO Items <span id="rn_no"></span></h3>
                    <span id="ItembtnSpan"></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="datatable_div">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="h-mobile">No</th> 
                                <th>Item ID</th>
                                <th>Description</th>
                                <th>Qty Order</th>
                                <th class="h-mobile">UOM</th>
                                <th class="h-mobile">Qty Received</th>
                                <th>Unit Price</th>
                                <th class="h-mobile">Supplier Item ID</th> 
                                <th>Amount</th> 
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    
    <span class="pull-right" id="ViewPoItemsCloseSpan"><a href="#" id="ViewPoItemsClose" class="btn btn-default text-bold"><i class="fa fa-arrow-left"></i> Back to Reprint PO</a></span>
    
</section>

<script>
function KeepRunFxL(){
    $("#ListInvbtnSpanDiv").appendTo("#listing_porpt #datatable_length label"); 
}
var KeepRun = setTimeout( KeepRunFxL , 1200); 
</script>