<?php 

if ($results[0]["status_level"]>2 || ($results[0]["status_level"]==2 && $results[0]["rev_num"]>1) ) {
    echo '<br><br><center><p><b>PO '.$results[0]["po_id"].' was confirmed. Please find your PO from Reprint PO.</b></p></center><br><br>';
    exit;
}

$po_amt = 0; 
?>

<span id="InvbtnSpanDiv" ></span>

<table id="" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th class="h-mobile">No</th> 
            <th>Item ID</th>
            <th>Description</th>
            <th>Qty Order</th>
            <th class="chv-green-gradient">New Qty</th>
            <th class="h-mobile">UOM</th>
            <th class="hidden h-mobile">Qty Received</th>
            <th class="chv-green-gradient">Unit Price</th>
            <th class="h-mobile">Supplier Item ID</th> 
            <th>Amount</th> 
        </tr>
    </thead>
    <tbody>
        <?php if ($results): ?>
            <?php foreach ($results as $result): ?>
                <?php
                $line_num = isset($result["line_num"]) ? $result["line_num"] : "";
                $item_id = isset($result["item_id"]) ? $result["item_id"] : "";
                $item_desc = isset($result["item_desc"]) ? $result["item_desc"] : "";
                $qty_order = isset($result["qty_order"]) ? $result["qty_order"] : "";
                $uom_id = isset($result["uom_id"]) ? $result["uom_id"] : "";
                $qty_received = isset($result["qty_received"]) ? $result["qty_received"] : "";
                $unit_price = isset($result["unit_price"]) ? $result["unit_price"] : "";
                $supp_item_id = isset($result["supp_item_id"]) ? $result["supp_item_id"] : "";
                $amount = isset($result["amount"]) ? $result["amount"] : "";
                $po_amt += $result["amount"];
                $row = trim($result["coy_id"]) .'---'. trim($result["po_id"]) .'---'. trim($result["rev_num"]) .'---'. trim($result["line_num"]) ;
                ?>
                <tr>
                    <td class="h-mobile"><?php echo $line_num; ?></td>
                    <td><?php echo $item_id; ?></td>
                    <td><?php echo $item_desc; ?></td>
                    <td>
                        <?php echo display_number($qty_order); ?>
                        <input type="hidden" class="po_item_oldqty" name="qtyold[<?php echo $row; ?>]" value="<?php echo display_number($qty_order); ?>" data-unitprice="<?php echo number_format($unit_price, 4, '.', ''); ?>">
                    </td>
                    <td class="chv-green-gradient">
                        <input type="number" size="4" class="form-control po_item_newqty" name="qtynew[<?php echo $row; ?>]" value="<?php echo display_number($qty_order); ?>" min="0" max="<?php echo display_number($qty_order); ?>" data-unitprice="<?php echo number_format($unit_price, 4, '.', ''); ?>">
                    </td>
                    <td class="h-mobile"><?php echo $uom_id; ?></td>
                    <td class="hidden h-mobile"><?php echo display_number($qty_received); ?></td>
                    <td class="chv-green-gradient"><?php echo number_format($unit_price, 4, '.', ','); ?></td>
                    <td class="h-mobile"><?php echo $supp_item_id; ?></td>
                    <td><?php echo price_no_symbol($amount); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
    <?php if ($results): ?>
    <tfoot>
        <tr>
            <td class="h-mobile"></td>
            <td></td>
            <td align="right"><b>Order/New Amount:</b></td>
            <td><b>$<?php echo price_no_symbol($po_amt); ?></b></td>
            <td><b><span id="po_item_newqty_total">$<?php echo price_no_symbol($po_amt); ?></span></b></td>
            <td class="h-mobile"></td>
            <td class="hidden h-mobile"></td>
            <td></td>
            <td class="h-mobile"></td>
            <td></td> 
        </tr>
    </tfoot>
    <?php endif; ?>
</table>
    
<script>
function KeepRunFxI(){
    $("#InvbtnSpanDiv").appendTo("#listing_porpt_items #datatable_length label"); 
}
var KeepRun = setTimeout( KeepRunFxI , 1200); 
</script>