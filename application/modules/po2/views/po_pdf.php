<?php
ini_set('display_errors', 1);
ob_start();
tcpdf();

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        $this->SetFont('helvetica', '', 9);
        $headerData = $this->getHeaderData();
        $this->writeHTML($headerData['string']);
    }

    // Page footer
    public function Footer() {
        $this->SetY(-50);
        $this->SetFont('helvetica', '', 9);
        $footer = $this->getFooterString();
        $footer .= ''; //'<table style="padding: 5px;"><tr><td align=left></td><td align=center></td><td align="right">Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages() . '</td></tr></table>';
        
        $this->writeHTML($footer);
    }

    public $isLastPage = false;
    public function lastPage($resetmargins = false) {
        $this->setPage($this->getNumPages(), $resetmargins);
        $this->isLastPage = true;
    }

}

$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, 95, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(true, 65);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);

$obj_pdf->SetHeaderMargin(5);
$obj_pdf->setPrintHeader(true);
$obj_pdf->SetFooterMargin(20);
$obj_pdf->setPrintFooter(true);

foreach ($poinfos as $poinfo) {
    
    $CustomHeader = HEADERP($poinfo,$ctl_addr,$obj_pdf);
    $CustomFooter = FOOTERP($poinfo);
    $content = CONTENTP($poinfo);

    $obj_pdf->setHeaderData($ln = '', $lw = 0, $ht = '', $CustomHeader, $tc = array(0, 0, 0), $lc = array(0, 0, 0));
    $obj_pdf->setFooterData(array(0, 0, 0), array(0, 0, 0), $CustomFooter);
    $obj_pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->AddPage(); 
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    if (ceil($obj_pdf->GetY()) >= 90) {
        $obj_pdf->AddPage();
    }
    $lastPage = $obj_pdf->getPage();
    $obj_pdf->deletePage($lastPage);
}

if (isset($save_path)) {
    $obj_pdf->Output($save_path, 'F');
}
else {
    $obj_pdf->Output('output.pdf', 'I');
}


function HEADERP($info,$ctl_addr,$obj_pdf){
    $logo = 'http://chvoices.challenger.sg/assets/images/logo.png'; // base_url('assets/images/logo.png');
    $barcode_params = $obj_pdf->serializeTCPDFtagParameters(array($info[0]['po_id'], 'C128', '', '', 63, 5, 0.4, array('position'=>'S', 'border'=>false, 'padding'=>0, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4), 'N'));
    $barcode = '<tcpdf method="write1DBarcode" params="'.$barcode_params.'" />';

    $po_num = ($info[0]['current_rev']=="0") ? trim($info[0]['po_id']) : '<span style="color:#F00">'.trim($info[0]['po_id']).'R'.$info[0]['current_rev'].'</span>';
    $po_stat = ($info[0]['current_rev']=="0") ? '' : '***REVISED PO***';
    return '<table style="font-size:9px;">
    <tr>
        <td style="width: 25%; text-align: left;">
            <div>
                <img style="width: 130px; height: auto;" src="' . $logo . '"/><br/>
                <span><br>STATUS: <b>'.$info[0]['status_desc'].'</b></span> 
                <span><br>BUYER: <b>'.$info[0]['buyer_id_usr'].'</b></span> 
            </div>
        </td>
        <td style="width: 39%; text-align: left; margin-left:10px;">
            <table width="100%">
                <tr><td>&nbsp;<br><span style="font-size:10px"><b>'.$ctl_addr["coy_name"].'</b></span></td></tr>
                <tr><td>'.$ctl_addr["street_line1"].'</td></tr>
                <tr><td>'.$ctl_addr["street_line2"].'</td></tr>
                <tr><td>'.$ctl_addr["country_name"].' '.$ctl_addr["postal_code"].'</td></tr>
                <tr><td>Tel: '.$ctl_addr["tel_code"].' Fax: '.$ctl_addr["fax_code"].'</td></tr>
                <tr><td>Co. Reg. No.: '.$ctl_addr["coy_reg_code"].'</td></tr>
                <tr><td>GST. No.: '.$ctl_addr["tax_reg_code"].'</td></tr>
            </table>
        </td>
        <td style="width: 34%;" align="right">
            <table width="100%">
                <tr><td align="right" width="190">&nbsp;<br><span style="font-weight:bold;color:#F00">'.$po_stat.' &nbsp;&nbsp;</span></td></tr>
                <tr><td align="right" width="190"><span style="font-weight:bold;">PURCHASE ORDER &nbsp;&nbsp;</span></td></tr>
                <tr><td>
                    <table width="100%" cellspacing="1" cellpadding="1">
                        <tr><td align="right" width="52"><b>DATE:</b></td><td width="130" style="border:1px solid #888;"><span>'.$info[0]['po_date'].'</span></td></tr>
                        <tr><td align="right" width="52"><b>CURR.:</b></td><td width="43" style="border:1px solid #888"><span>'.$info[0]['curr_id'].'</span></td>
                            <td align="right" width="43"><b>TERM:</b></td><td width="42" style="border:1px solid #888"><span>'.$info[0]['payment_id'].'</span></td></tr>
                        <tr><td align="right" width="52"><b>DELIVERY:</b></td><td width="130" style="border:1px solid #888;"><span>'.$info[0]['delv_date'].'</span></td></tr>
                        <tr><td align="right" width="52"><b>PO. NO.:</b></td><td style="border:1px solid #888">'.$po_num.'</td></tr>
                    </table>
                </td></tr>
                <tr><td style="font-size:6px;">&nbsp;</td></tr>
                <tr><td style="font-size:6px;" align="right">'.$barcode.'</td></tr>
                <tr><td style="font-size:6px;">&nbsp;</td></tr>
            </table>
        </td>
    </tr>
</table>

<table style="padding: 5px;font-size: 9px;">
    <tr>
        <td style="border: 1px solid black; width: 270px; height: 90px;"><span><b><i>To:</i></b> '.$info[0]["supp_name"].'</span>
            <div>'.nl2br($info[0]["supp_addr_text"]).' <br>
                 Tel: '.nl2br($info[0]["tel_code"]).' &nbsp; Fax: '.nl2br($info[0]["fax_code"]).'<br>
                 Attn: '.nl2br($info[0]["contact_person"]).'<br>
                 Email: '.nl2br($info[0]["email_addr"]).'</div>
        </td>
        <td style="border: 1px solid black; width: 270px; height: 90px;"><span><b><i>Deliver To:</i></b> '.$info[0]["loc_name"].'</span>
            <div>'.nl2br($info[0]["loc_addr_text"]).'</div>
        </td>
    </tr>
</table>

<table style="paddding: 5px;margin-top:10px">
    <tr><td colspan="4">&nbsp;</td></tr>
    <tr style="background-color:#DDD;">
        <th style="width: 30px;  border: 1px solid black;text-align: center; font-size: 10px;">Item</th>
        <th style="width: 220px; border: 1px solid black; text-align: center; font-size: 10px;">Description</th>
        <th style="width: 90px; border: 1px solid black;text-align: center; font-size: 10px;">Quantity</th>
        <th style="width: 100px; border: 1px solid black;text-align: center; font-size: 10px;">Unit Price</th>
        <th style="width: 100px; border: 1px solid black;text-align: center; font-size: 10px;">Amount</th>
    </tr>
</table>';
}

function FOOTERP($info){
    if ($info[0]["created_by_usr"]==NULL)
        $info[0]["created_by_usr"] = $info[0]["requester_id_usr"];
    return '<table style="padding: 5px;">
                <tr>
                    <td align="center"><b><u>Lunch hour from 12.30 to 1.30pm. Pls avoid.</u></b></td>
                </tr>
            </table>
            <table cellpadding="5" cellspacing="2">
                <tr>
                    <td style="border: 1px solid black; width: 160px; height: auto; font-size: 9px;">
<b>Requested By:</b><br>'.$info[0]["requester_id_usr"].'<br>
<b>Order/Issue By:</b><br>'.$info[0]["created_by_usr"].'<br>
<b>Approved By:</b><br>'.$info[0]["approve_by_usr"].'
                    </td>
                    <td align="center" style="border: 1px solid black; width: 210px; height: auto; font-size: 9px;">
NOTE: GOODS RECEIVING TIME<br>Mon – Fri except Sat/ Sun Public Holiday.<br>Avoid lunch hour.
                    </td>
                    <td align="center" style="border: 0px solid white; width: 170px; height: auto; font-size: 9px;">
This is a computer generated document. No signature required.
                    </td>
                </tr>
            </table>';
}

function CONTENTP($info){
    $c = '<table style="paddding: 5px;margin-top:10px;font-size: 10px;">
    <tr><td colspan="5">&nbsp;</td></tr>';
    
    $cT = 0; 
    foreach ($info as $item):
        $c1 = ($item["qty_order"]!=$item["original_qty"]) ? '<span style="color:#F00">*</span>' : '';
        $lD = (strlen($item["long_desc"])>0) ? '<br>'.$item["long_desc"] : '';
        $fp = ($item["disc_percent"] > 0) ? ($item["qty_order"] * $item["unit_price"] * ((100-$item["disc_percent"])/100) ) : $item["qty_order"] * $item["unit_price"] ;
        $c.= '<tr style="margin-top:3px">
            <td align="right" style="width: 30px;text-align: left; font-size: 9px;">'.$c1.''.$item["line_num"].'</td>
            <td style="width: 220px;  text-align: left; font-size: 9px;">'.$item["item_desc"].'<br>Our ref: '.$item["item_id"] . $lD.'<br>&nbsp;</td>
            <td style="width: 90px;text-align: right; font-size: 9px;">'.display_number($item["qty_order"]).' '.$item["uom_id"].'</td>
            <td style="width: 100px;text-align: right; font-size: 9px;">
            '.($item["unit_price"]).'
            '. ( ($item["disc_percent"] > 0) ? '<br>Disc: '. $item["disc_percent"] .'%' : '') .'
            </td>
            <td style="width: 100px;text-align: right; font-size: 9px;">'.price_no_symbol($fp).'</td>
        </tr>';
        $cT += ($item["qty_order"] * $item["unit_price"]);
    endforeach; 
            
    $c.= '<tr><td colspan="5">&nbsp;</td></tr>
</table>
<table>
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr>
        <td colspan="3" style="width:320"></td>
        <td style="width:110;text-align: center;"><span style="font-size:14px"><b>TOTAL</b></span></td>
        <td height="18" style="width:110;background-color: #F4F4F4; text-align: center; border: 1px solid black;">
            <span style="font-size:14px">'.price_no_symbol($cT).'</span>
        </td>
    </tr>  
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr>
        <td colspan="5" style="height: 30px; "><table><tr><td width="60"><b>Remarks:</b></td><td>'.nl2br($info[0]['remarks']).'</td></tr></table></td>
    </tr>
</table>';
    return $c;
}

?>