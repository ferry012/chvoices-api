<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Po2 extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('po2/Po2_model');
        $this->load->model('api/Api_model');
        $this->load->model('rn/Rn_model');
        $this->load->model('login/Login_model');
        $this->load->model('cherps/Cherps_model');
        $this->load->library('session');

        if ($this->input->get('cherps')>(time()-30) && $this->input->get('cherps')<(time()+5)) {
            // Skip check for login
            // https://chvoices-test.challenger.sg/cherps/eyJtIjoicG9wZGYiLCJ1IjoiU1MwMDExIiwiaSI6IklQSkVNMTgwMDcyNzIiLCJzIjoxLCJ0IjoxNTE5OTczMjI5fQ%3D%3D
        }
        else {
            $this->_checkLogged();
        }
    }

    public function confirmation() {
        $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : date('d-m-Y',strtotime("-1 month"));
        $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : date('d-m-Y');
        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
            $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : '';
            $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : '';
        }
        if (isset($data["search_from"])) {
            $params["search_from"] = $data["search_from"];
        }
        if (isset($data["search_to"])) {
            $params["search_to"] = $data["search_to"];
        }
        if ($this->input->get("show_confirmed")) {
            $params["show_confirmed"] = 1;
        }
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        $params["status_level"] = "1";
        $output = $this->Po2_model->get_all_po_list($params);
        //echo "<pre>";print_r($output);exit;
        $data["results"] = $output;
        $data["page_title"] = 'PO Confirmation';
        $data["sidebar"] = 'Purchase Order';
        $data["search_bar_options"] =  ['show_confirmed'];
        Template::set($data);
        Template::render();
    }
    
    public function confirmpo() {
        $supp_id = $this->session->userdata('cm_supp_id');
        $new_codate = $this->input->post('new_codate');
        
        $qtynew = $this->input->post('qtynew');
        $qtyold = $this->input->post('qtyold');

        $confirmParam = [];
        foreach ($qtynew as $r=>$row) {
            $row_data = explode('---',$r);
            $confirmParam["coy_id"] = $row_data[0];
            $confirmParam["po_id"] = $row_data[1];
            $confirmParam["rev_num"] = $row_data[2];
            $confirmParam['delv_date'] = date("Y-m-d H:i:s", strtotime($new_codate));

            $confirmParam['items'][] = [
                'line_num'      => $row_data[3],
                'qty_ordered'   => $qtyold[$r],
                'new_qty'       => $qtynew[$r],
            ];
        }

        $return = $this->Po2_model->do_po_confirmation($confirmParam);

        /*
        $changed=0; $po_data=$po_qty=array(); 
        foreach ($qtynew as $r=>$row) {
            if ($qtynew[$r]!=$qtyold[$r])
                $changed++;
            if ($qtynew[$r]>$qtyold[$r])
                $changed= -10000;
            
            $row_data = explode('---',$r);
            $po_data["coy_id"] = $row_data[0];
            $po_data["po_id"] = $row_data[1];
            $po_data["rev_num"] = $row_data[2];
            if ($row>0) 
                $po_qty[ $row_data[3] ] = $row; 
        }
        
        if (strtotime($new_codate) > time() - 86400) {
            $params["delv_date"] = date("Y-m-d H:i:s", strtotime($new_codate));
            $params["po_id"] = $po_data["po_id"]; 
            $output = $this->Po2_model->update_po_list($params); 
        }
        
        $return = [];
        if (count($qtynew)==0 || $changed<0 || strtotime($new_codate) < time() - 86400) {
            // Changed
            if (count($qtynew)==0)
                $return = array('return'=>'0','message'=>'No valid PO', 'po_id'=>$po_data["po_id"]);
            elseif (strtotime($new_codate) < time() - 86400)
                $return = array('return'=>'0','message'=>'Invalid date ' . $new_codate, 'po_id'=>$po_data["po_id"]);
            else
                $return = array('return'=>'0','message'=>'New Qty cannot be higher than Qty Order', 'po_id'=>$po_data["po_id"]);
        }
        else if ($changed==0) {
            // No change in Qty. Confirm this PO 
            $params = array(
                'status_level'=> '2',
                'coy_id'=> $po_data["coy_id"],
                'po_id'=> $po_data["po_id"],
                'rev_num'=> $po_data["rev_num"]
            );
            $output = $this->Po2_model->update_po_list($params);
            $params = array(
                'status_level'=> '1',
                'coy_id'=> $po_data["coy_id"],
                'po_id'=> $po_data["po_id"],
                'rev_num'=> $po_data["rev_num"]
            );
            $output = $this->Po2_model->update_po_item($params);
            
            $return = array('return'=>'1','message'=>'PO Confirmed', 'po_id'=>$po_data["po_id"]);
        }
        else {
            // Changes in Qty. Revise this PO
            $params = array( 
                'coy_id'=> $po_data["coy_id"],
                'po_id'=> $po_data["po_id"],
                'rev_num'=> $po_data["rev_num"]
            );
            $output = $this->Po2_model->revise_po($params, $po_qty);
            
            $return = array('return'=>'1','message'=>'Revise PO Confirmed', 'po_id'=>$po_data["po_id"]);
        } 

        // :: After confirmed actions :: 
        if ($return['return']=='1') {
            if (substr($po_data["po_id"],0,2)=="HP" && $return['message']=='Revise PO Confirmed') {
                // If revised & po_type=HP, trigger email
                $supp_name = $this->session->userdata('cm_supp_name');
                $email_param["from"] = "chvoices@challenger.sg";
                $email_param["to"] = "HQ.CustServiceGroup; mrd.hc@challenger.sg; joshua-woon@challenger.sg; kunalan@challenger.sg; ";
                $email_param["subject"] = "PO revised by supplier - " . $po_data["po_id"];
                $email_param["message"] = "PO ".$po_data["po_id"]." revised by " . $supp_name; 
                $this->Po2_model->CallSP_email($email_param);
            }
        }
        */
        
        echo json_encode($return);
    }
    
    public function updatedate() {
        $supp_id = $this->session->userdata('cm_supp_id');
        $new_codate = $this->input->post('new_codate');
        $new_trans_id = $this->input->post('new_trans_id');
        $new_trans_id_str = implode("','", $new_trans_id);

        if (strtotime($new_codate) > time() - 86400) {
            $params["delv_date"] = date("Y-m-d H:i:s", strtotime($new_codate));
            $params["po_id"] = $new_trans_id_str; 
            $output = $this->Po2_model->update_po_list($params);

            echo $new_codate;
        } else {
            echo '<span class="text-danger">Invalid date '.$new_codate.'.</span>';
        }
    }

    public function listing() {
        $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : date('d-m-Y',strtotime("-1 month"));
        $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : date('d-m-Y');
        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
            $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : '';
            $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : '';
        }
        if (isset($data["search_from"])) {
            $params["search_from"] = $data["search_from"];
        }
        if (isset($data["search_to"])) {
            $params["search_to"] = $data["search_to"];
        }
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        $params["status_level"] = "1,2,3,4,5";
        $output = $this->Po2_model->get_all_po_list($params);
        if ($output) {
            foreach ($output as $k => $row) {
                if ($row['invoice'] != '') {
                    // Get the DO URL
                    $output[$k]['invoice'] = $this->Cherps_model->get_document('hachi-do', $row['invoice'], 'DSH', 'DSH');
                }
            }
        }
        //echo "<pre>";print_r($output);exit;
        $data["results"] = $output;
        $data["page_title"] = 'Reprint PO';
        $data["sidebar"] = 'Purchase Order';
        Template::set($data);
        Template::render();
    }

    public function items($po_id = NULL, $state='') { 
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        $params["po_id"] = isset($po_id) ? $po_id : NULL;
        $output = $this->Po2_model->get_all_po_items($params); 
        $data["po_id"] = $po_id;
        $data["results"] = $output;
        $data["page_title"] = 'Reprint PO';
        $data["sidebar"] = 'Purchase Order';

        if ($state=='confirmation') {
            $this->load->view('confirmation_items', $data);
        }
        else {
            $this->load->view('items', $data);
        }
    }

    public function export($export_type='pdf',$po_no,$supp_id=''){
        $this->po_export($export_type,$po_no,$supp_id);
    }

    public function po_export($export_type='pdf',$po_no,$supp_id='') {
        ini_set('max_execution_time',300);

        $params["ids"] = isset($po_no) ? explode(',',$po_no) : '';
        $params["supp_id"] = ($supp_id=='') ? $this->session->userdata('cm_supp_id') : urldecode($supp_id);
//        $poinfo = $this->Po2_model->pdf_info($params);
//
//        $polists = array(); $last_poid='';
//        foreach ($poinfo as $info) {
//            if ($last_poid!=$info["po_id"]) {
//                $last_poid = trim($info["po_id"]);
//            }
//            $polists[$last_poid][] = $info;
//        }
//        $data["poinfos"] = $polists;
        $polists = $this->Po2_model->pdf_list($params);
        $data["poinfos"] = $polists;

        $data["ctl_addr"] = $this->Login_model->get_coy_address("CTL","CTL","full");

        if ($this->input->get("debug")==1) {
            print_r($data);
            exit;
        }

        if ($export_type=="pdf") {
            $this->load->helper('pdf_helper');
            $this->load->view('po2/po_pdf', $data);
        }
        else {
            // Prepares CSV file for user to download on request 
            $handle_path = FCPATH . 'assets/uploads/';
            $uploadedcsv = $params["supp_id"] . '_poexport.csv';
            $fp = fopen($handle_path . $uploadedcsv, 'w');
            fputcsv($fp, array('PO ID','PO Date','Delv Date','Curr','Buyer ID','Approve By','Status','Loc ID','Item Line','Item ID','Description','Qty Order','UOM','Unit Price','Amount','Long Desc'));

            foreach ($polists as $polist) {
                foreach ($polist as $result) {
                    $a = array();
                    $a[] = $result["po_id"];
                    $a[] = $result["po_date"];
                    $a[] = $result["delv_date"];
                    $a[] = $result["curr_id"];
                    $a[] = $result["buyer_id_usr"];
                    $a[] = $result["approve_by_usr"];
                    $a[] = $result["status_desc"];
                    $a[] = $result["loc_id"];
                    $a[] = $result["line_num"];
                    $a[] = $result["item_id"];
                    $a[] = $result["item_desc"];
                    $a[] = display_number($result["qty_order"]);
                    $a[] = $result["uom_id"];
                    $a[] = $result["unit_price"];
                    $a[] = price_no_symbol($result["unit_price"]*$result["qty_order"]);
                    $a[] = $result["long_desc"];
                    fputcsv($fp, $a);
                }
            }
            fclose($fp);

            if (file_exists($handle_path . $uploadedcsv)) {
                redirect(base_url("assets/uploads/".$uploadedcsv));
            }
        }
    }

    public function pr_listing() {
        $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : date('d-m-Y',strtotime("-1 month"));
        $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : date('d-m-Y');
        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
            $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : '';
            $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : '';
        }
        if (isset($data["search_from"])) {
            $params["search_from"] = $data["search_from"];
        }
        if (isset($data["search_to"])) {
            $params["search_to"] = $data["search_to"];
        }
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        $params["status_level"] = "1";
        $output = $this->Po2_model->get_all_pr_list($params);
        //echo "<pre>";print_r($output);exit;
        $data["results"] = $output;
        $data["page_title"] = 'Consignment Request';
        $data["sidebar"] = 'Purchase Order';
        Template::set($data);
        Template::render();
    }

    public function pr_export($export_type='pdf',$po_no,$supp_id='') {
        ini_set('max_execution_time',300);

        $params["ids"] = isset($po_no) ? explode(',',$po_no) : '';
        $params["supp_id"] = ($supp_id=='') ? $this->session->userdata('cm_supp_id') : urldecode($supp_id);

        $polists = $this->Po2_model->pr_pdf_list($params);
        $data["poinfos"] = $polists;

        $data["ctl_addr"] = $this->Login_model->get_coy_address("CTL","CTL","full");

        if ($this->input->get("debug")==1) {
            print_r($data);
            exit;
        }

        if ($export_type=="pdf") {
            $this->load->helper('pdf_helper');
            $this->load->view('po2/pr_pdf', $data);
        }
        else {
            // Prepares CSV file for user to download on request
            $handle_path = FCPATH . 'assets/uploads/';
            $uploadedcsv = $params["supp_id"] . '_poexport.csv';
            $fp = fopen($handle_path . $uploadedcsv, 'w');
            fputcsv($fp, array('PR ID','PR Date','Delv Date','Curr','Buyer ID','Approve By','Status','Loc ID','Item Line','Item ID','Description','Qty Order','UOM','Unit Price','Amount','Long Desc'));

            foreach ($polists as $polist) {
                foreach ($polist as $result) {
                    $a = array();
                    $a[] = $result["pr_id"];
                    $a[] = $result["pr_date"];
                    $a[] = $result["delv_date"];
                    $a[] = $result["curr_id"];
                    $a[] = $result["buyer_id_usr"];
                    $a[] = $result["approve_by_usr"];
                    $a[] = $result["status_desc"];
                    $a[] = $result["loc_id"];
                    $a[] = $result["line_num"];
                    $a[] = $result["item_id"];
                    $a[] = $result["item_desc"];
                    $a[] = display_number($result["qty_order"]);
                    $a[] = $result["uom_id"];
                    $a[] = $result["unit_price"];
                    $a[] = price_no_symbol($result["unit_price"]*$result["qty_order"]);
                    $a[] = $result["long_desc"];
                    fputcsv($fp, $a);
                }
            }
            fclose($fp);

            if (file_exists($handle_path . $uploadedcsv)) {
                redirect(base_url("assets/uploads/".$uploadedcsv));
            }
        }
    }

}
