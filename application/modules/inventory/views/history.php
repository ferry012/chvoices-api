<section class="content clearfix" id="Inventory_DIV">
    <?php //echo theme_view('general/search_bar_filter'); ?>
    
    <div class="hidden row" id="inv_upload">
        <div class="col-xs-12">
            <div class="box box-danger " >
                <div class="box-header hidden">
                    <h3 class="box-title">Upload Inventory</h3>
                </div>
                <div class="box-body"> 

                    <form role="form" class="" action="<?php echo base_url("inventory/upload"); ?>" method="post" enctype="multipart/form-data">
                        <div class="col-md-6 "> 
                            <p>Upload inventory listings (.csv file).</p>
                            <div class="col-xs-8 form-group">
                                    <input type="file" name="upload_csv" id="upload_csv" placeholder="Choose CSV"  accept=".csv"> 
                            </div> 
                            <div class="col-xs-4 form-group">
                                <button class="btn btn-default">Upload File</button> 
                            </div>
                        </div> 
                    </form>
                    
                </div> 
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row" id="inv_list">
        <div class="col-xs-12">
            <div class="box">
                <div class="hidden box-header">
                    <h3 class="box-title">Listings</h3>
                </div>
                <div class="box-body">                    
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="hidden"></th>
                                <th>Sn #</th> 
                                <th>Date</th>
                                <th>Submitted Items</th>
                                <th class="h-mobile">Verified Items</th>
                                <th class="h-mobile">Invalid Items</th>
                                <th class="h-mobile">Remarks</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($results): $k = 0; ?>
                                <?php foreach ($results as $result): ?>
                                    <?php
                                    ?>
                                    <tr>
                                        <td class="hidden"></td>
                                        <td>EIL-<?php echo $result["extinv_sn_id"]; ?></td>
                                        <td><?php echo display_date_time_format($result["created_on"]); ?></td>
                                        <td><?php echo $result["extinv_uploaded"]; ?></td>
                                        <td class="h-mobile"><?php echo $result["extinv_success"]; ?></td>
                                        <td class="h-mobile"><?php echo $result["extinv_error"]; ?></td>
                                        <td class="h-mobile" width="30%">
                                            <?php if ( ($result["extinv_success"]+$result["extinv_error"])!=$result["extinv_uploaded"] ) echo '<i class="fa fa-exclamation-circle text-orange" style="font-size:22px;" title="Results do not tally. Please check the inventory on hand to verify your update."></i> '; ?>
                                            <?php echo $result["extinv_remarks"]; ?>
                                        </td>
                                        <td class="">
                                            <a class="btn btn-info btn-sm ViewItems" id="<?php echo $result["extinv_sn_id"]; ?>" href="#">Items</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $k++;
                                endforeach;
                                ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row" id="inv_items">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Uploaded Items <span id="sn_no"></span></h3>
                    <span id="ItembtnSpan"></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="datatable_div">
                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    
    <span class="pull-right" id="inv_items_closer"><a href="#" id="ViewItemsClose" class="btn btn-default text-bold"><i class="fa fa-arrow-left"></i> Back to Listings</a></span>
    
</section>
