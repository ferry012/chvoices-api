
<form action="<?php echo base_url("inventory/newitemlist_save"); ?>" method="post" id="newinventory_form">
    <p>Enter the list of Item ID and Qty (i.e. 8888123456789,1). Use new row for every item.</p>
    <textarea name="itemlist" class="form-control" placeholder="888123456789,10
888123456788,5"></textarea>
</form>

<!--
<?php if ($results): ?> 
    <form action="<?php echo base_url("inventory/newitem_save"); ?>" method="post" id="newinventory_form">
    <table id="datatable" class="table table-bordered table-striped">
        <thead>
            <tr> 
                <th></th>
                <th>Item ID</th>
                <th>Description</th>
                <th>Qty</th> 
            </tr>
        </thead> 
        <tbody>
        <?php foreach ($results as $result): ?> 
            <tr> 
                <td><input type="checkbox" name="newinvsel[]" value="<?php echo $result["item_id"]; ?>"></td>
                <td><?php echo $result["item_id"]; ?></td>
                <td><?php echo $result["item_desc"]; ?></td>
                <td>
                    <input type="number" name="newinv[<?php echo trim($result["item_id"]); ?>]" value="0" min="0" class="form-control text-right" >
                    <input type="hidden" name="newinvtype[<?php echo trim($result["item_id"]); ?>]" value="<?php echo $result["inv_type"]; ?>">
                    <input type="hidden" name="newinvprice[<?php echo trim($result["item_id"]); ?>]" value="<?php echo trim($result["unit_price"]); ?>">
                </td> 
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    </form>
<?php else: ?> 
    <div style="padding:20px;text-align:center">
        <i>No new items</i>
    </div>
<?php endif; ?>

<p>If you do not see your item here, please confirm with buyer that the item have the correct Inventory Type & uploaded to Hachi.</p>
-->