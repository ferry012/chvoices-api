<?php if ($results): ?>
    <table id="datatable" class="table table-bordered table-striped">
        <thead>
            <tr> 
                <th class="hidden"></th>
                <th>Item ID</th>
                <th>Description</th>
                <th>Qty</th>
                <th class="h-mobile">Remarks</th> 
            </tr>
        </thead> 
        <tbody>
        <?php foreach ($results as $result): ?> 
            <tr> 
                <td class="hidden"></td>
                <td><?php echo $result["extinv_item_id"]; ?></td>
                <td><?php echo $result["item_desc"]; ?></td>
                <td><?php echo display_number($result["extinv_quantity"]); ?></td>
                <td class="h-mobile"><?php echo $result["extinv_remarks"]; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?> 
    <div style="padding:20px;text-align:center">
        <a class="btn btn-info btn-sm ViewItems" id="<?php echo $sn; ?>" href="#">Refresh Items</a>
    </div>
<?php endif; ?>