<section class="content clearfix" id="Inventory_DIV">
    <?php //echo theme_view('general/search_bar_filter'); ?>
    
    <div class="row" id="inv_upload">
        <div class="col-xs-12">
            <div class="box box-danger " > 
                <div class="box-body"> 

                    <div class="col-md-6"> 
                        <div>
                            <p><b>Update inventory listings:</b></p> 
                            <form role="form" class="" action="<?php echo base_url("inventory/upload"); ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="file" name="upload_csv" id="upload_csv" placeholder="Choose CSV" accept=".csv"> 
                                <button class="btn btn-default nfi-outerbtn" id="upload_csv_btn" ><i class="fa fa-upload"></i> Upload File</button>
                            </div>
                            </form>
                        </div>
                    </div> 
                    <div class="col-md-3" >
                        <?php if ($results): $k = 0; ?>
                        <div class="pull-right form-group ">
                            <p>&nbsp;</p>
                            <button class="btn btn-success" id="update_inv_btn"><i class="fa fa-send"></i> Update Quantity</button>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-3 <?php echo (isset($_GET['addnewitem'])) ? '' : 'hidden' ?>" style="border-left:1px solid #EEE; ">
                        <?php if ($results): $k = 0; ?>
                        <div class="pull-right col-xs-12 form-group text-left">
                            <p><b>Add new item:</b></p>
                            <a class="btn btn-success" id="new_inv_btn"><i class="fa fa-plus"></i> Add New Item</a>
                        </div>
                        <?php endif; ?>
                    </div>
                    
                </div> 
                <!-- /.box-body -->
            </div>
        </div>
    </div>

<span id="InvbtnSpanDiv" >
    <?php if (isset($exportcsv) && $exportcsv != NULL): ?>
        <a style="float:right" href="<?php echo base_url("assets/uploads/" . $exportcsv); ?>" class="btn btn-primary m-l-10"><i class="fa fa-arrow-up"></i> Export CSV</a>
    <?php endif; ?>
</span>
    
    <div class="row" id="inv_list">
        <div class="col-xs-12">
            <div class="box">
                <div class="hidden box-header">
                    <h3 class="box-title">Listings</h3>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url("inventory/listings_qkupdate"); ?>" method="post" id="update_inv_form">
                    <input type="submit" class="hidden" value="Update">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="hidden"></th>
                                <th>Item ID</th> 
                                <th>Description</th>
                                <th class="chv-green-gradient">Qty</th>
                                <th>Reserved Qty</th>
                                <th>Last Updated</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($results): $k = 0; ?>
                                <?php foreach ($results as $result): ?>
                                    <?php
                                        $qty_class = ($result["qty_on_hand"]>0) ? "chv-green-gradient" : "chv-red-gradient";
                                    ?>
                                    <tr>
                                        <td class="hidden"></td>
                                        <td><?php echo $result["item_id"]; ?><small><br><?php echo $result["loc_id"]; ?></small></td>
                                        <td width="35%"><?php echo $result["item_desc"]; ?></td>
                                        <td width="15%" class="<?php echo $qty_class; ?>" align="center" valign="middle" data-sort="<?php echo display_number($result["qty_on_hand"]); ?>"> 
                                            <input type="text" class="form-control text-right inv_qty" name="inv[<?php echo trim($result["coy_id"]).'---'.trim($result["item_id"]).'---'.trim($result["loc_id"]); ?>]" value="<?php echo display_number($result["qty_on_hand"]); ?>">
                                            <input type="hidden" class="form-control text-right inv_qty" name="oldinv[<?php echo trim($result["coy_id"]).'---'.trim($result["item_id"]).'---'.trim($result["loc_id"]); ?>]" value="<?php echo display_number($result["qty_on_hand"]); ?>">
                                        </td>
                                        <td><?php echo display_number($result["qty_reserved"]); ?></td>
                                        <td><?php echo display_date_time_format($result["modified_on"]); ?></td>
                                    </tr>
                                    <?php
                                    $k++;
                                endforeach;
                                ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade" id="modal_newinventory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Items</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="modal_newinventory_add" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
            </div>
        </div>
    </div>
</div>


<?php if (isset($exportcsv) && $exportcsv != NULL): ?>
<script>
function KeepRunFx(){
    $("#InvbtnSpanDiv").appendTo("#datatable_length label"); 
}
var KeepRun = setTimeout( KeepRunFx , 1200);
var KeepRun = setTimeout( KeepRunFx , 8000);
</script>
<?php endif; ?>