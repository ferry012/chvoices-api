<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

function cleanUpc($string) {
    $string = str_replace(' ', '', $string);
    $clean = preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
    return str_pad($clean, 13,'0', STR_PAD_LEFT);
}
function EmailNewRow() {
    return '
';
}

class Inventory_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();

        $this->load->library('s3');
        $this->config->load('s3', true, true);
        $s3_config = $this->config->item('s3');
    }

    public function paging($page = NULL, $limit = NULL) {
        $page = $this->input->get('page');
        $limit = $this->input->get('limit');
        if (!empty($page) && !is_numeric($page))
            $this->invalid_params('page');
        if (!empty($limit) && !is_numeric($limit))
            $this->invalid_params('limit');
        if (!isset($page) || empty($page) || $page <= 0)
            $page = DEFAULT_PAGE;
        if (!isset($limit) || empty($limit) || $limit <= 0)
            $limit = DEFAULT_LIMIT;
        return array(
            "page" => $page,
            "limit" => $limit
        );
    }

    public function uploadIMS($file){

        $supp_id = $this->session->userdata('cm_supp_id');
        $supp_type = $this->session->userdata('cm_supp_onlinetype');
        $chvoice_ref = 'V'.trim($supp_id).date('ymd');
        $tmpName = $_FILES[$file]['tmp_name'];
        $csvAsArray = array_map('str_getcsv', file($tmpName));
        foreach($csvAsArray as $row) {
            $sql_row = "('CTL',".$row[0].",'$supp_type',1,CURRENT_TIMESTAMP,".$row[1].",0,0,'Y',CURRENT_TIMESTAMP,'$chvoice_ref',0,0,'$supp_id',CURRENT_TIMESTAMP,'$supp_id',CURRENT_TIMESTAMP) ";
            $sql = "INSERT INTO ims_inv_physical
                        VALUES $sql_row
                        ON CONFLICT (coy_id,item_id,loc_id,line_num) DO UPDATE 
                          SET qty_on_hand = excluded.qty_on_hand;";
            $result = CI::db()->query($sql);
        }
        return $result;

    }

    public function uploadS3($file){

        $supp_type = $this->session->userdata('cm_supp_onlinetype');
        $supp_id = $this->session->userdata('cm_supp_id');
        $name = $supp_id . '.' . time() . '.csv';
        $upload = $this->_upload($file, $name, "assets/uploads/", "csv", $supp_id.'.');

        if (!isset($upload["error"])) {
            $filename = $upload["upload_data"]["file_name"];
            $filedirrectory = FCPATH . 'assets/uploads/' . $filename;

            if (($handle = fopen($filedirrectory, "r")) !== FALSE) {
                $r=1;
                $sqlitems = [];
                $sqlquantity = [];
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $id = cleanUpc($data[0]); 
                    if ($r > 1 && trim($id) != '') {
                        $sqlitems[] = $id;
                        $sqlquantity[$id] = (int) $data[1];
                    }
                    $r++;
                }
                fclose($handle);

                $response = [];
                $sql = "select coy_id,trim(item_id) item_id,'$supp_type' loc_id, '1' line_num,'Y' supp_csg, '0' qty_reserved, '$supp_id' created_by,
                               (select count(*) from ims_inv_physical where coy_id=ims_item_list.coy_id and item_id=ims_item_list.item_id and loc_id='$supp_type') qty_on_hand, 
                               (select unit_price from ims_item_price where coy_id=ims_item_list.coy_id and item_id=ims_item_list.item_id and eff_from<= current_timestamp and eff_to>=current_timestamp and price_type='SUPPLIER' order by price_ind limit 1) unit_cost, 
                               (select trim(ref_id) from ims_item_price where coy_id=ims_item_list.coy_id and item_id=ims_item_list.item_id and eff_from<= current_timestamp and eff_to>=current_timestamp and price_type='SUPPLIER' order by price_ind limit 1) ref_id, 
                               '0' ref_rev,'0' ref_num
                        from ims_item_list WHERE coy_id='CTL' and item_id in ('" . implode("','", $sqlitems) . "')";
                $res = $this->db->query($sql)->result_array();
                foreach ($res as $row) {
                    $id = trim($row['item_id']);
                    if ($row['ref_id'] != $supp_id) {
                        $response[$id] = 'Item do not belong to supplier';
                    }
                    else if ($row['qty_on_hand'] > 0 && $sqlquantity[$id] > 0) {
                        // Update qty
                        $sql = "UPDATE ims_inv_physical SET qty_on_hand = " . $sqlquantity[$id] . "  WHERE coy_id='" . trim($row['coy_id']) . "' and trim(item_id)='".$id."' and trim(loc_id)='$supp_type' ";
                        $act = $this->db->query($sql);
                        $response[$id] = ($act) ? 'Inventory qty updated' : '';
                    }
                    else if ($row['qty_on_hand'] > 0) {
                        // Delete row
                        $sql = "DELETE FROM ims_inv_physical WHERE coy_id='".trim($row['coy_id'])."' and trim(item_id)='".$id."' and trim(loc_id)='$supp_type' ";
                        $act = $this->db->query($sql);
                        $response[$id] = ($act) ? 'Inventory deleted' : '';
                    }
                    else {
                        // Insert
                        $act = $this->db->insert('ims_inv_physical', $row);
                        $response[$id] = ($act) ? 'New inventory added' : '';
                    }
                }
                unlink($filedirrectory);
                // Send an email
                $msg = "CHVOICES - Inventory Update on " . date('d-m-Y H:i') . EmailNewRow();
                $msg.= "Supplier " . $supp_id . EmailNewRow() . EmailNewRow() . '-----';
                foreach($response as $key => $val) {
                    $msg .= EmailNewRow() . "Item " . $key . " - " . $val;
                }

                $params['from'] = 'chvoices@challenger.sg';
                $params['to'] = 'yongsheng@challenger.sg';
                $params['cc'] = '';
                $params['subject'] = 'Vender Ext Inventory Upload - ' . $supp_id .' (' . date('d-m-Y') .')';
                $params['message'] = $msg;
                $params['body_format'] = 'HTML';
                $rtn = $this->_SendEmail($params);
            }

/*
            // Send it to S3
            $S3_BUCKET = 'ctl-lambda';
            $destination = (ENVIRONMENT=="production") ? 'uploads/' : 'STAGING/uploads/';
            $destination = (ENVIRONMENT=="production") ? 'hachi-inventory-update/' : 'STAGING/hachi-inventory-update/';
            $header = array("Cache-Control"=>"no-cache");
            $s3upload = $this->s3->putObject($this->s3->inputFile($filedirrectory), $S3_BUCKET, $destination.$filename, S3::ACL_PUBLIC_READ, array(), $header);

            if (!$s3upload){
                $error = array('error' => "File could not be uploaded to server.");
                return $error;
            }
            else {
                $cherps_mssql = $this->load->database('cherps_hachi', TRUE);
                $sql = "INSERT INTO o2o_extinv_list (extinv_vendor,extinv_uploaded,status_level,extinv_remarks,created_by,created_on) "
                    . "VALUES('".$supp_id."', '0', 0, '<!--FILE:".$filename."--> Processing...', '".$supp_id."', CURRENT_TIMESTAMP); ";
                $result = $cherps_mssql->query($sql);

                unlink($filedirrectory);
            }
*/
        }
        else {
            return $upload;
        }
    }

    public function get_extinv_list($params = array()) {
        $supp_id = $this->session->userdata('cm_supp_id');
        $supp_type = $this->session->userdata('cm_supp_onlinetype');
        $prefix = $this->session->userdata('cm_loc_id');

        $sql_search = '';
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (extinv_sn_id LIKE '%" . $params["search"] . "%' ) " : "";
        $sql_search.= (isset($params["search_to"]) && $params["search_to"] != NULL) ? " AND created_on <= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_to"]))) . "'" : "";
        $sql_search.= (isset($params["search_from"]) && $params["search_from"] != NULL) ? " AND created_on >= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_from"]))) . "'" : "";
        $sql = "SELECT TOP 1000 (extinv_sn_id)
                        ,(SELECT count(*) FROM o2o_extinv_items WHERE o2o_extinv_items.extinv_sn_id = o2o_extinv_list.extinv_sn_id) as extinv_uploaded
                        ,(SELECT count(*) FROM o2o_extinv_items WHERE o2o_extinv_items.extinv_sn_id = o2o_extinv_list.extinv_sn_id AND o2o_extinv_items.extinv_status_level=1) as extinv_success
                        ,(SELECT count(*) FROM o2o_extinv_items WHERE o2o_extinv_items.extinv_sn_id = o2o_extinv_list.extinv_sn_id AND o2o_extinv_items.extinv_status_level=-1) as extinv_error
                        ,(extinv_remarks)
                        ,(status_level)
                        ,created_by
                        ,created_on 
                    FROM o2o_extinv_list
                    WHERE created_by='$supp_id' $sql_search
                    ORDER BY created_on DESC 
                    ";
        $cherps_mssql = $this->load->database('cherps_hachi', TRUE);
        $result = $cherps_mssql->query($sql)->result_array();
        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function get_extinv_items($params = array()) {
        $sql_search = '';
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (extinv_sn_id LIKE '%" . $params["search"] . "%' ) " : "";
        $sql_search.= (isset($params["search_to"]) && $params["search_to"] != NULL) ? " AND created_on <= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_to"]))) . "'" : "";
        $sql_search.= (isset($params["search_from"]) && $params["search_from"] != NULL) ? " AND created_on >= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_from"]))) . "'" : "";
        $sql = "SELECT extinv_item_id,ims_item_list.item_desc, extinv_quantity,extinv_remarks
                    FROM o2o_extinv_items
                    LEFT JOIN ims_item_list ON ims_item_list.item_id = o2o_extinv_items.extinv_item_id
                    WHERE extinv_sn_id='".$params["sn"]."'
                    order by extinv_status_level,extinv_quantity";
        $cherps_mssql = $this->load->database('cherps_hachi', TRUE);
        $result = $cherps_mssql->query($sql)->result_array();
        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function get_ims_inv_physical($params = array()) {
        $supp_id = $this->session->userdata('cm_supp_id');
        $supp_type = $this->session->userdata('cm_supp_onlinetype');
        $loc_id = $this->session->userdata('cm_loc_id');

        $sql_condition = (strtoupper(trim($supp_id))=="ER0001") ? "ims_inv_physical.loc_id IN ('IM-C')" : "ims_inv_physical.loc_id IN ('$supp_type','DSH-C','VEN-C') AND prc.ref_id='$supp_id'";
        $sql = "SELECT ims_inv_physical.coy_id,ims_inv_physical.loc_id, ims_inv_physical.item_id, ims_item_list.item_desc, ims_inv_physical.qty_on_hand, ims_inv_physical.qty_reserved, ims_inv_physical.modified_on, prc.ref_id as supp_id
                FROM ims_inv_physical 
                LEFT JOIN ims_item_list ON ims_item_list.item_id = ims_inv_physical.item_id
                LEFT JOIN ims_item_price prc ON 
                    prc.coy_id = 'CTL' and prc.price_type = 'SUPPLIER' and prc.eff_from <= CURRENT_TIMESTAMP and prc.eff_to >= CURRENT_TIMESTAMP 
                    and prc.price_ind='PRI'
                    and prc.ref_id in (select supp_id from pms_supplier_list where coy_id = 'CTL' and online_type <> '')
                    and prc.item_id=ims_inv_physical.item_id
                WHERE $sql_condition
                LIMIT 1000";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function update_ims_inv_physical(array $params) {
        $sql = "";
        foreach ($params as $param){
            $sql.= "UPDATE ims_inv_physical SET qty_on_hand='".$param["qty"]."', modified_on=CURRENT_TIMESTAMP , modified_by='CHVOICES'
                    WHERE coy_id='".$param["coy_id"]."' AND item_id='".$param["item_id"]."' AND loc_id='".$param["loc_id"]."'; ";
        }
        $result = CI::db()->query($sql);
        return $result;
    }

    public function update_online_type(array $params) {
        $sql = "UPDATE pms_supplier_list SET online_type='".$params["online_type"]."', modified_on=CURRENT_TIMESTAMP , modified_by='CHVOICES'
                    WHERE coy_id='".$params["coy_id"]."' AND supp_id='".$params["supp_id"]."'; ";
        $result = CI::db()->query($sql);
        return $result;
    }

    public function get_new_ims_item() {
        $supp_id = $this->session->userdata('cm_supp_id');
        $supp_type = $this->session->userdata('cm_supp_onlinetype');
        $loc_id = $this->session->userdata('cm_loc_id');

        $sql = "SELECT ims_item_list.item_id, ims_item_list.item_desc,  ims_inv_physical.coy_id,ims_inv_physical.loc_id, ims_inv_physical.qty_on_hand, ims_inv_physical.qty_reserved, ims_inv_physical.modified_on, prc.ref_id as supp_id,
                    prc.unit_price,
                    ims_item_list.is_active, ims_item_list.status_level, ims_item_list.inv_type, o2o_product.is_active,o2o_product.in_catalog
                FROM ims_item_list
                LEFT JOIN ims_item_price prc ON 
                    prc.coy_id = 'CTL' and prc.price_type = 'SUPPLIER' and prc.eff_from <= CURRENT_TIMESTAMP and prc.eff_to >= CURRENT_TIMESTAMP 
                    and prc.price_ind='PRI'
                    and prc.ref_id in (select supp_id from pms_supplier_list where coy_id = 'CTL' and online_type <> '')
                    and prc.item_id=ims_item_list.item_id
		LEFT JOIN o2o_product ON o2o_product.item_id=ims_item_list.item_id
                LEFT JOIN ims_inv_physical  ON ims_item_list.item_id = ims_inv_physical.item_id and ims_inv_physical.loc_id IN (NULL,'DSH-C','VEN-C','IM-C')
                WHERE prc.ref_id='$supp_id' 
                    AND ims_inv_physical.item_id IS NULL 
                    AND ims_item_list.is_active='Y' AND ims_item_list.inv_type IN ('DSH-C','VEN-C','IM-C')
                    AND o2o_product.is_active = 'Y' 
                order by ims_item_list.modified_on desc
                limit 50 ";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function newitem_ims_inv_physical($params){
        $supp_id = $this->session->userdata('cm_supp_id');
        $created_by = $this->session->userdata('cm_user_id');
        $sql = "";
        foreach ($params as $param){
            $sql.= "INSERT INTO ims_inv_physical (coy_id,item_id,loc_id,qty_on_hand,unit_cost,created_by, 
                        qty_reserved,line_num,supp_csg,ref_id,ref_rev,ref_num) VALUES 
                        ('".$param['coy_id']."','".$param['item_id']."','".$param['loc_id']."','".$param['qty_on_hand']."','".$param['unit_cost']."','".$created_by."', 
                        '0','1','Y','CHVOICES-".date('ymd')."','0','0') ; ";
        }
        $result = CI::db()->query($sql);
        return $result;
    }

}

?>
