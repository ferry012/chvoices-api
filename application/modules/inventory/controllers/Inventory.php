<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Inventory extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('inventory/Inventory_model'); 
        $this->load->library('session');
        $this->_checkLogged();
    }

    public function index() {
        redirect(base_url("inventory/listings"), 'refresh');
    }
    
    public function history() {
        $data["results"] = $this->Inventory_model->get_extinv_list();  
        $data["page_title"] = 'Update History';
        $data["sidebar"] = 'Inventory';
        Template::set($data);
        Template::render();
    }

    public function items($sn) {
        $data["sn"] = $sn;
        $data["results"] = $this->Inventory_model->get_extinv_items( array("sn"=>$sn) ); 
        $this->load->view('items', $data);
    }

    public function upload() {
        
        $supp_type = $this->session->userdata('cm_supp_onlinetype');
        if (empty($supp_type)){
            $data["error_message"] = strip_tags("Your account type is not enabled for uploading.");
            $this->session->set_flashdata('error_message', $data["error_message"]);
            redirect(base_url("inventory/listings"), 'refresh');
        }
        
        $upload = $this->Inventory_model->uploadS3("upload_csv");
        
        if (isset($upload["error"])) {
            $data["error_message"] = strip_tags("Fail to upload. " . $upload["error"]);
            $this->session->set_flashdata('error_message', $data["error_message"]);
        }
        else {
            $data["success_message"] = "Successfully uploaded. Your inventory will be updated on Hachi.";
            $this->session->set_flashdata('success_message', $data["success_message"]);
        }
            redirect(base_url("inventory/listings"), 'refresh');
        
    }
    
    public function listings(){
        
        $supp_id = $this->session->userdata('cm_supp_id');
        
        $grn_items = $this->Inventory_model->get_ims_inv_physical();
        $data["results"] = $grn_items;
        
        if (isset($grn_items) && count($grn_items)>=1 && isset($grn_items[0]["item_id"]) ) {
            // Prepares CSV file for user to download on request 
            $handle_path = FCPATH . 'assets/uploads/';
            $uploadedcsv = $supp_id . '_imsinventory.csv';
            $fp = fopen($handle_path . $uploadedcsv, 'w');
            fputcsv($fp, array('challenger_upc','qty_available','description')); 

            foreach ($grn_items as $result) {
                $a = array();
                $a[] = "#".$result["item_id"];
                $a[] = display_number($result["qty_on_hand"]);
                $a[] = $result["item_desc"];
                fputcsv($fp, $a);
            }
            fclose($fp);

            if (file_exists($handle_path . $uploadedcsv)) {
                $data["exportcsv"] = $uploadedcsv;
            }
        }
        
        $data["page_title"] = 'Inventory Listings';
        $data["sidebar"] = 'Inventory';
        Template::set($data);
        Template::render(); 
    }
    
    public function listings_qkupdate(){
        
        $updates = $this->input->post("inv");
        $oldinvs = $this->input->post("oldinv");
        foreach ($updates as $k=>$val){ 
            if ($val!=$oldinvs[$k]){
                $keys = explode('---',$k);
                $param["coy_id"] = $keys[0];
                $param["item_id"] = $keys[1];
                $param["loc_id"] = $keys[2];
                $param["qty"] = $val;

                $params[] = $param;
            }
        }
        $this->Inventory_model->update_ims_inv_physical($params);
         
        $data["success_message"] = "Inventory update: " . count($params) . " item(s) successfully updated.";
        $this->session->set_flashdata('success_message', $data["success_message"]); 
        
        redirect(base_url("inventory/listings"), 'refresh');
    }

    public function newitem() {
        $data["results"] = $this->Inventory_model->get_new_ims_item(); 
        $this->load->view('add_new_item', $data);
    }
    
    public function newitemlist_save(){
        $itemlist = $this->input->post("itemlist");
        $itemarray = explode("\n", $itemlist);
        $newitems = [];
        foreach ($itemarray as $itemarrayrow) {
            $newitems[] = explode(",",$itemarrayrow);
        }
        print_r($newitems);
    }

    public function newitem_save() {
        $params = array();
        $newinv = $this->input->post("newinv");
        $newinvsel = $this->input->post("newinvsel");
        $newinvtype = $this->input->post("newinvtype"); 
        $newinvprice = $this->input->post("newinvprice"); 
        foreach ($newinv as $id=>$val){ 
            if ($val>0 || (in_array($id,$newinvsel)) ) {
                $params[] = array(
                    "coy_id" => Base_Common_Model::COMPANY_ID,
                    "item_id" => $id,
                    "loc_id" => $newinvtype[$id],
                    "unit_cost" => $newinvprice[$id],
                    "qty_on_hand" => $val,
                );
            }
        }
        $this->Inventory_model->newitem_ims_inv_physical($params);
         
        $data["success_message"] = "Inventory added: " . count($params) . " item(s) successfully updated.";
        $this->session->set_flashdata('success_message', $data["success_message"]); 
        
        redirect(base_url("inventory/listings"), 'refresh');
        print_r($params);
    }
    
    public function enabler($inv_type){
        if (in_array($inv_type, array("VEN","DSH") )){
            $param["online_type"] = $inv_type;
            $param["coy_id"] = Base_Common_Model::COMPANY_ID;
            $param["supp_id"] = $this->session->userdata('cm_supp_id');
            $this->Inventory_model->update_online_type($param);
            redirect("home/account");
        }
        else {
            redirect("home/account");
        }
    }

}
