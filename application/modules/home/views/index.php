<?php $access_control =  $this->session->userdata('r_access_control'); ?>

<?php if ($access_control[0]=='Y'): ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
<style>
    .inner-box-label {
        background-color:rgba(0,0,0,0.3);border-radius: 5px; color:#FFF; padding-left:5px;
        max-width: 230px
    }
    .inner-box-mainlabel {
        color:#FFF
    }
    .inner-box-mainlabel>sup{
        font-size: 14px;font-weight:normal;
        margin-top:-20px;margin-left:-5px;margin-bottom:20px;
    }
</style>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-4 col-xs-12">
            <!-- small box -->
            <div class="small-box" style="background-color:#7bc342;border-radius: 6px;">
                <div class="inner">
                    <p class="inner-box-label"> Unconfirmed Purchase Orders</p>
                    <h3 class="inner-box-mainlabel"><?php echo $summary["PO"]["ref1"] .' <sup>PO</sup> / '. $summary["PO"]["ref2"] . ' <sup>Items</sup>'; ?></h3>
                </div>
                <div class="icon" style="margin-top:20px">
                    <i class="ion ion-archive"></i>
                </div>
                <a href="<?php echo base_url("po2/confirmation") ?>" class="small-box-footer" style="background-color:#222d32">PO Confirmation <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
            <!-- small box -->
            <div class="small-box" style="background-color:#4285c3;border-radius: 6px;">
                <div class="inner">
                    <p class="inner-box-label">Unmatched GRN Items</p>
                    <h3 class="inner-box-mainlabel"><?php echo $summary["GRN"]["ref1"] .' <sup>GRN</sup> / '. $summary["GRN"]["ref2"] . ' <sup>Items</sup>'; ?></h3>
                </div>
                <div class="icon" style="margin-top:20px">
                    <i class="ion ion-filing"></i>
                </div>
                <p class="small-box-footer" style="background-color:#222d32">
                    <a href="<?php echo base_url("invoice/submit_new_match") ?>" class="db-footer-a">Invoice Matching (Single) <i class="fa fa-arrow-circle-right"></i></a>  
                    <a href="<?php echo base_url("invoice/submit_batch") ?>" class="db-footer-a">Invoice Matching (Batch) <i class="fa fa-arrow-circle-right"></i></a>
                </p>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
            <!-- small box -->
            <div class="small-box" style="background-color:#424ec3;border-radius: 6px;">
                <div class="inner">
                    <p class="inner-box-label">Uncollected Return Notes</p>
                    <h3 class="inner-box-mainlabel"><?php echo $summary["RN"]["ref1"] .' <sup>RN</sup> / '. $summary["RN"]["ref2"] . ' <sup>Items</sup>'; ?></h3>
                </div>
                <div class="icon" style="margin-top:20px">
                    <i class="ion ion-clipboard"></i>
                </div>
                <a href="<?php echo base_url("rn/collection") ?>" class="small-box-footer" style="background-color:#222d32">RN Collection Date <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col --> 
    </div>
    <!-- /.row --> 
    <div class="row">
        <!-- Left col -->
        <section class="hidden col-lg-6 connectedSortable">
            <!-- Chat box -->
            <div class="box box-success">
                <div class="box-header">
                    <i class="ion ion-archive"></i> 
                    <h3 class="box-title">Purchase Order (Outstanding Deliveries)</h3> 
                </div>
                <div class="box-body chat" id="chat-box">
                    <center><div style="max-width:400px;max-height:285px">
                            <canvas id="myBar" width="270" height="180"></canvas>
                            <script>
                                var ctx = document.getElementById("myBar");
                                var data = {
                                    labels: ["0 - 6 Days", "7 - 30 Days", "More than 30 days"],
                                    datasets: [
                                        {
                                            label: ["# of PO"],
                                            backgroundColor: [
                                                "#00ca6d",
                                                "#FFCE56",
                                                "#ca0000"
                                            ],
                                            borderColor: [
                                                "#00ca6d",
                                                "#FFCE56",
                                                "#ca0000"
                                            ],
                                            borderWidth: 1,
                                            data: [<?php echo $summary["DELV"]["ref1"]; ?> , <?php echo $summary["DELV"]["ref2"]; ?>, <?php echo $summary["DELV"]["ref3"]; ?>],
                                        }
                                    ]
                                };
                                var myPieChart = new Chart(ctx, {
                                    type: 'bar',
                                    data: data,
                                    options: {
                                        legend: {
                                            display: false
                                        },
                                        scales: {
                                            xAxes: [{
                                                    gridLines: {
                                                        display:false
                                                    }
                                                }],
                                            yAxes: [{
                                                    gridLines: {
                                                        display:false
                                                    }   
                                                }]
                                            }

                                        }
                                });
                            </script>
                    </div></center>
                    <!-- chat item --> 
                    <!-- /.item -->  
                </div>
                <!-- /.chat --> 
                <br>
                <a href="<?php echo base_url("po/reprint_listing") ?>" class="db-footer" style="background-color:#222d32">Reprint PO <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.box (chat box) -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-6 connectedSortable">
            <!-- TO DO List -->
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-ios-box-outline"></i>

                    <h3 class="box-title">Hachi Inventory</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body text-center">
                    <center><div style="max-width:300px;max-height:260px">
                            <canvas id="myChart" width="209" height="180"></canvas>
                            <script>
                                var ctx = document.getElementById("myChart");
                                var data = {
                                    labels: [
                                        "Instock (<?php echo ($summary["INV"]["ref1"]-$summary["INV"]["ref2"]); ?> Items)",
                                        "Out of Stock (<?php echo $summary["INV"]["ref2"]; ?> Items)",
                                    ],
                                    datasets: [
                                        {
                                            data: [<?php echo $summary["INV"]["ref1"]; ?>,<?php echo $summary["INV"]["ref2"]; ?>],
                                            backgroundColor: [
                                                "#00ca6d",
                                                "#ca0000"
                                            ],
                                            hoverBackgroundColor: [
                                                "#00ca6d",
                                                "#ca0000"
                                            ]
                                        }]
                                };
                                var myPieChart = new Chart(ctx, {
                                    type: 'pie',
                                    data: data,
                                    options: {
                                        tooltips: {
                                            enabled: false
                                        },
                                        animation: {
                                            animateScale: true,
                                            circumference: 1
                                        }
                                    }
                                });
                            </script>
                    </div></center>
                </div>
                <!-- /.box-body -->
                <br>
                <a href="<?php echo base_url("inventory/listings") ?>" class="db-footer" style="background-color:#222d32">Inventory Listing <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.box -->
        </section>
        <!-- right col -->
    </div>

    <?php /*
      <!-- Main row -->
      <div class=" row">
      <!-- Left col -->
      <section class="col-lg-7 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="nav-tabs-custom">
      <!-- Tabs within a box -->
      <ul class="nav nav-tabs pull-right">
      <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
      <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
      <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
      </ul>
      <div class="tab-content no-padding">
      <!-- Morris chart - Sales -->
      <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
      <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
      </div>
      </div>
      <!-- /.nav-tabs-custom -->

      <!-- Chat box -->
      <div class="box box-success">
      <div class="box-header">
      <i class="fa fa-comments-o"></i>

      <h3 class="box-title">Chat</h3>

      <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
      <div class="btn-group" data-toggle="btn-toggle">
      <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i>
      </button>
      <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
      </div>
      </div>
      </div>
      <div class="box-body chat" id="chat-box">
      <!-- chat item -->
      <div class="item">
      <img src="https://almsaeedstudio.com/themes/AdminLTE/dist/img/user4-128x128.jpg" alt="user image" class="online">

      <p class="message">
      <a href="#" class="name">
      <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
      Mike Doe
      </a>
      I would like to meet you to discuss the latest news about
      the arrival of the new theme. They say it is going to be one the
      best themes on the market
      </p>
      <div class="attachment">
      <h4>Attachments:</h4>

      <p class="filename">
      Theme-thumbnail-image.jpg
      </p>

      <div class="pull-right">
      <button type="button" class="btn btn-primary btn-sm btn-flat">Open</button>
      </div>
      </div>
      <!-- /.attachment -->
      </div>
      <!-- /.item -->
      <!-- chat item -->
      <div class="item">
      <img src="https://almsaeedstudio.com/themes/AdminLTE/dist/img/user3-128x128.jpg" alt="user image" class="offline">

      <p class="message">
      <a href="#" class="name">
      <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
      Alexander Pierce
      </a>
      I would like to meet you to discuss the latest news about
      the arrival of the new theme. They say it is going to be one the
      best themes on the market
      </p>
      </div>
      <!-- /.item -->
      <!-- chat item -->
      <div class="item">
      <img src="https://almsaeedstudio.com/themes/AdminLTE/dist/img/user2-160x160.jpg" alt="user image" class="offline">

      <p class="message">
      <a href="#" class="name">
      <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
      Susan Doe
      </a>
      I would like to meet you to discuss the latest news about
      the arrival of the new theme. They say it is going to be one the
      best themes on the market
      </p>
      </div>
      <!-- /.item -->
      </div>
      <!-- /.chat -->
      <div class="box-footer">
      <div class="input-group">
      <input class="form-control" placeholder="Type message...">

      <div class="input-group-btn">
      <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
      </div>
      </div>
      </div>
      </div>
      <!-- /.box (chat box) -->

      <!-- TO DO List -->
      <div class="box box-primary">
      <div class="box-header">
      <i class="ion ion-clipboard"></i>

      <h3 class="box-title">To Do List</h3>

      <div class="box-tools pull-right">
      <ul class="pagination pagination-sm inline">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">&raquo;</a></li>
      </ul>
      </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
      <ul class="todo-list">
      <li>
      <!-- drag handle -->
      <span class="handle">
      <i class="fa fa-ellipsis-v"></i>
      <i class="fa fa-ellipsis-v"></i>
      </span>
      <!-- checkbox -->
      <input type="checkbox" value="">
      <!-- todo text -->
      <span class="text">Design a nice theme</span>
      <!-- Emphasis label -->
      <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
      <!-- General tools such as edit or delete-->
      <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
      </div>
      </li>
      <li>
      <span class="handle">
      <i class="fa fa-ellipsis-v"></i>
      <i class="fa fa-ellipsis-v"></i>
      </span>
      <input type="checkbox" value="">
      <span class="text">Make the theme responsive</span>
      <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
      <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
      </div>
      </li>
      <li>
      <span class="handle">
      <i class="fa fa-ellipsis-v"></i>
      <i class="fa fa-ellipsis-v"></i>
      </span>
      <input type="checkbox" value="">
      <span class="text">Let theme shine like a star</span>
      <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
      <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
      </div>
      </li>
      <li>
      <span class="handle">
      <i class="fa fa-ellipsis-v"></i>
      <i class="fa fa-ellipsis-v"></i>
      </span>
      <input type="checkbox" value="">
      <span class="text">Let theme shine like a star</span>
      <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
      <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
      </div>
      </li>
      <li>
      <span class="handle">
      <i class="fa fa-ellipsis-v"></i>
      <i class="fa fa-ellipsis-v"></i>
      </span>
      <input type="checkbox" value="">
      <span class="text">Check your messages and notifications</span>
      <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
      <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
      </div>
      </li>
      <li>
      <span class="handle">
      <i class="fa fa-ellipsis-v"></i>
      <i class="fa fa-ellipsis-v"></i>
      </span>
      <input type="checkbox" value="">
      <span class="text">Let theme shine like a star</span>
      <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
      <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
      </div>
      </li>
      </ul>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix no-border">
      <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
      </div>
      </div>
      <!-- /.box -->

      <!-- quick email widget -->
      <div class="box box-info">
      <div class="box-header">
      <i class="fa fa-envelope"></i>

      <h3 class="box-title">Quick Email</h3>
      <!-- tools box -->
      <div class="pull-right box-tools">
      <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
      <i class="fa fa-times"></i></button>
      </div>
      <!-- /. tools -->
      </div>
      <div class="box-body">
      <form action="#" method="post">
      <div class="form-group">
      <input type="email" class="form-control" name="emailto" placeholder="Email to:">
      </div>
      <div class="form-group">
      <input type="text" class="form-control" name="subject" placeholder="Subject">
      </div>
      <div>
      <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
      </div>
      </form>
      </div>
      <div class="box-footer clearfix">
      <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
      <i class="fa fa-arrow-circle-right"></i></button>
      </div>
      </div>

      </section>
      <!-- /.Left col -->
      <!-- right col (We are only adding the ID to make the widgets sortable)-->
      <section class="col-lg-5 connectedSortable">

      <!-- Map box -->
      <div class="box box-solid bg-light-blue-gradient">
      <div class="box-header">
      <!-- tools box -->
      <div class="pull-right box-tools">
      <button type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range">
      <i class="fa fa-calendar"></i></button>
      <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
      <i class="fa fa-minus"></i></button>
      </div>
      <!-- /. tools -->

      <i class="fa fa-map-marker"></i>

      <h3 class="box-title">
      Visitors
      </h3>
      </div>
      <div class="box-body">
      <div id="world-map" style="height: 250px; width: 100%;"></div>
      </div>
      <!-- /.box-body-->
      <div class="box-footer no-border">
      <div class="row">
      <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
      <div id="sparkline-1"></div>
      <div class="knob-label">Visitors</div>
      </div>
      <!-- ./col -->
      <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
      <div id="sparkline-2"></div>
      <div class="knob-label">Online</div>
      </div>
      <!-- ./col -->
      <div class="col-xs-4 text-center">
      <div id="sparkline-3"></div>
      <div class="knob-label">Exists</div>
      </div>
      <!-- ./col -->
      </div>
      <!-- /.row -->
      </div>
      </div>
      <!-- /.box -->

      <!-- solid sales graph -->
      <div class="box box-solid bg-teal-gradient">
      <div class="box-header">
      <i class="fa fa-th"></i>

      <h3 class="box-title">Sales Graph</h3>

      <div class="box-tools pull-right">
      <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
      </button>
      </div>
      </div>
      <div class="box-body border-radius-none">
      <div class="chart" id="line-chart" style="height: 250px;"></div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer no-border">
      <div class="row">
      <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
      <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60" data-fgColor="#39CCCC">

      <div class="knob-label">Mail-Orders</div>
      </div>
      <!-- ./col -->
      <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
      <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60" data-fgColor="#39CCCC">

      <div class="knob-label">Online</div>
      </div>
      <!-- ./col -->
      <div class="col-xs-4 text-center">
      <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60" data-fgColor="#39CCCC">

      <div class="knob-label">In-Store</div>
      </div>
      <!-- ./col -->
      </div>
      <!-- /.row -->
      </div>
      <!-- /.box-footer -->
      </div>
      <!-- /.box -->

      <!-- Calendar -->
      <div class="box box-solid bg-green-gradient">
      <div class="box-header">
      <i class="fa fa-calendar"></i>

      <h3 class="box-title">Calendar</h3>
      <!-- tools box -->
      <div class="pull-right box-tools">
      <!-- button with a dropdown -->
      <div class="btn-group">
      <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-bars"></i></button>
      <ul class="dropdown-menu pull-right" role="menu">
      <li><a href="#">Add new event</a></li>
      <li><a href="#">Clear events</a></li>
      <li class="divider"></li>
      <li><a href="#">View calendar</a></li>
      </ul>
      </div>
      <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
      </button>
      </div>
      <!-- /. tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
      <!--The calendar -->
      <div id="calendar" style="width: 100%"></div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer text-black">
      <div class="row">
      <div class="col-sm-6">
      <!-- Progress bars -->
      <div class="clearfix">
      <span class="pull-left">Task #1</span>
      <small class="pull-right">90%</small>
      </div>
      <div class="progress xs">
      <div class="progress-bar progress-bar-green" style="width: 90%;"></div>
      </div>

      <div class="clearfix">
      <span class="pull-left">Task #2</span>
      <small class="pull-right">70%</small>
      </div>
      <div class="progress xs">
      <div class="progress-bar progress-bar-green" style="width: 70%;"></div>
      </div>
      </div>
      <!-- /.col -->
      <div class="col-sm-6">
      <div class="clearfix">
      <span class="pull-left">Task #3</span>
      <small class="pull-right">60%</small>
      </div>
      <div class="progress xs">
      <div class="progress-bar progress-bar-green" style="width: 60%;"></div>
      </div>

      <div class="clearfix">
      <span class="pull-left">Task #4</span>
      <small class="pull-right">40%</small>
      </div>
      <div class="progress xs">
      <div class="progress-bar progress-bar-green" style="width: 40%;"></div>
      </div>
      </div>
      <!-- /.col -->
      </div>
      <!-- /.row -->
      </div>
      </div>
      <!-- /.box -->

      </section>
      <!-- right col -->
      </div>
      <!-- /.row (main row) -->
     */ ?>


</section>
<!-- /.content -->
<?php endif; ?>

<script>
    var replace = document.getElementsByClassName("content-header")[0].getElementsByTagName("h1")[0];
    replace.innerHTML = "Welcome to CHVOICES";
</script>