<!-- Main content -->
<section class="content">
    <div class="row" id="listing_list_div">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body clearfix" id="">

                    <div class="col-md-12" style="padding:5px 0px">
                        <div class="col-md-1">Supplier ID:</div>
                        <div class="col-md-8 text-bold"><?php echo $account["supp_id"]; ?></div>
                    </div>
                    <div class="col-md-12" style="padding:5px 0px">
                        <div class="col-md-1">Name:</div>
                        <div class="col-md-8 text-bold"><?php echo $account["supp_name"]; ?></div>
                    </div>
                    <div class="col-md-12" style="padding:5px 0px">
                        <div class="col-md-1">Address:</div>
                        <div class="col-md-8 text-bold">
                            <?php
                            $ToAdd = ($account['street_line1']!='') ? $account['street_line1'] : '' ;
                            $ToAdd.= ($account['street_line2']!='') ? "<br>".$account['street_line2'] : '' ;
                            $ToAdd.= ($account['street_line3']!='') ? "<br>".$account['street_line3'] : '' ;
                            $ToAdd.= ($account['street_line4']!='') ? "<br>".$account['street_line4'] : '' ;
                            $ToAdd.= ($account['country_id']=='SG') ? "<br>Singapore " : "<br>".$account['country_id'] ;
                            $ToAdd.= ($account['postal_code']!='') ? $account['postal_code'] : '' ;
                            $ToAdd.= ($account['tel_code']!='') ? '<br>Tel: '.$account['tel_code'] : '' ;
                            $ToAdd.= ($account['fax_code']!='') ? '<br>Fax: '.$account['fax_code'] : '' ;
                            echo nl2br($ToAdd);
                            ?></div>
                    </div>
                    <div class="col-md-12" style="padding:5px 0px">
                        <div class="col-md-1">Contact:</div>
                        <div class="col-md-8 text-bold"><?php echo $account["contact_person"] .' '. $account["email_addr"] ; ?></div>
                    </div>
                    <div class="col-md-12" style="padding:5px 0px">
                        <div class="col-md-1"></div>
                        <div class="col-md-8 text-bold"><?php echo $account["contact_person2"] .' '. $account["email_addr2"] ; ?></div>
                    </div>
                    <div class="col-md-12" style="padding:5px 0px">
                        <div class="col-md-1"></div>
                        <div class="col-md-8 text-bold"><?php echo $account["contact_person3"] .' '. $account["email_addr3"] ; ?></div>
                    </div>
                    <div class="col-md-12" style="padding:5px 0px">
                        <div class="col-md-1">Online Type</div>
                        <div class="col-md-8 text-bold">
                            <?php
                            if ( trim($account["online_type"])!="" ) {
                                echo $account["online_type"];
                            }
                            else {
                                echo '- ';
                                if (isset($_GET["online_type"]) && in_array($_GET["online_type"], array('VEN','DSH') ))
                                    echo '<a class="text-normal" href="'.base_url("inventory/enabler/".$_GET["online_type"]).'">Change online type - '.$_GET["online_type"].'</a>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="listing_list_div">
        <div class="col-xs-12">

            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title ">Change Password</h3>
                </div>

                <!-- /.box-header -->

                <div class="box-body" id="">

                    <?php if (isset($error_message) && !empty($error_message)): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <i class="fa fa-bell"></i>
                            <b><?php echo $error_message; ?></b>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($success_message) && !empty($success_message)): ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <i class="fa fa-check-circle"></i>
                            <b><?php echo $success_message; ?></b>
                        </div>
                    <?php endif; ?>

                    <form action="<?php echo base_url("home/account"); ?>" method="post">

                        <div class="col-md-12" style="padding-bottom:10px">
                            <?php if ($this->session->userdata('r_access_type')=="ADMIN"): ?>
                                <p>This will change the password of your Supplier account.</p>
                            <?php else: ?>
                                <p>Change the password of your user account.</p>
                            <?php endif; ?>
                        </div>

                        <?php if ($this->session->userdata('r_access_type')!="ADMIN"): ?>
                        <div class="col-md-12" style="padding-top:5px">
                            <div class="col-md-2">
                                User Email:
                            </div>
                            <div class="col-md-4">
                                <input class="form-control" value="<?php echo $this->session->userdata('r_acct_email'); ?>" readonly>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-md-12" style="padding-top:5px">
                            <div class="col-md-2">
                                Old Password:
                            </div>
                            <div class="col-md-4">
                                <input type="password" class="form-control" name="old_pwd" id="old_pwd" value="" required="">
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:5px">
                            <div class="col-md-2">
                                New Password:
                            </div>
                            <div class="col-md-4">
                                <input type="password" class="form-control" name="new_pwd" id="new_pwd" value="" required="">
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:5px">
                            <div class="col-md-2">
                                Confirm Password:
                            </div>
                            <div class="col-md-4">
                                <input type="password" class="form-control" name="new_pwd2" id="new_pwd2" value="" required="">
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:15px">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-4">
                                <input type="submit" class="btn btn-primary" name="submit" value="Update Password">
                            </div>
                        </div>

                    </form>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</section>