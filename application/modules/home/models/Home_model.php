<?php

/* YS
 * 20-07-2016
 */
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Home_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }

    public function pms_supplier_list($params) {

        $sql = "SELECT pms_supplier_list.coy_id,
                    pms_supplier_list.supp_id,
                    pms_supplier_list.supp_type,
                    pms_supplier_list.supp_name,
                    pms_supplier_list.contact_person,
                    pms_supplier_list.contact_person2,
                    pms_supplier_list.contact_person3,
                    pms_supplier_list.email_addr,
                    pms_supplier_list.email_addr2,
                    pms_supplier_list.email_addr3,
                    pms_supplier_list.tel_code,
                    pms_supplier_list.fax_code,
                    pms_supplier_list.pwd_changed,
                    pms_supplier_list.online_type,
                    coy_address_book.street_line1,coy_address_book.street_line2,coy_address_book.street_line3,coy_address_book.street_line4,coy_address_book.country_id,coy_address_book.postal_code
                FROM pms_supplier_list 
                LEFT JOIN coy_address_book ON coy_address_book.ref_type='SUPPLIER' AND coy_address_book.addr_type=pms_supplier_list.supp_addr
                    AND coy_address_book.ref_id=pms_supplier_list.supp_id AND coy_address_book.coy_id=pms_supplier_list.coy_id
                WHERE pms_supplier_list.coy_id = '" . Base_Common_Model::COMPANY_ID . "' AND pms_supplier_list.supp_id = '" . $params["supp_id"] . "';";
        $result = CI::db()->query($sql)->result_array();

        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function dashboard_summary() {

        $supp_id = $this->session->userdata('cm_supp_id'); //$params["supp_id"];
        $sql = "SELECT 'PO' as k, count(DISTINCT pms_po_list.po_id) AS ref1, count(*) AS ref2, '0' AS ref3 FROM pms_po_item
                    LEFT JOIN pms_po_list ON pms_po_list.po_id=pms_po_item.po_id 
                    WHERE pms_po_list.coy_id=pms_po_item.coy_id and pms_po_list.supp_id='$supp_id' AND pms_po_list.status_level=1 
                        AND pms_po_list.coy_id='".Base_Common_Model::COMPANY_ID."'
                UNION SELECT 'GRN' as k, count(DISTINCT pms_grn_list.grn_id) AS ref1, count(*) AS ref2, '0' AS ref3 FROM pms_grn_item 
                    JOIN pms_grn_list ON pms_grn_list.grn_id = pms_grn_item.grn_id
                    WHERE pms_grn_list.coy_id=pms_grn_item.coy_id and pms_grn_list.supp_id = '$supp_id' AND pms_grn_item.status_level=0 AND pms_grn_item.unit_price>0 AND pms_grn_list.order_type IN ('I','N')
                        AND pms_grn_list.coy_id='".Base_Common_Model::COMPANY_ID."'
                UNION SELECT 'RN' as k, count(DISTINCT pms_rn_list.trans_id) AS ref1, count(*) AS ref2, '0' AS ref3 FROM pms_rn_item
                    LEFT JOIN pms_rn_list  ON pms_rn_list.trans_id=pms_rn_item.trans_id 
                    WHERE pms_rn_list.coy_id=pms_rn_item.coy_id and pms_rn_list.supp_id = '$supp_id' AND pms_rn_list.status_level=1
                        AND pms_rn_list.coy_id='".Base_Common_Model::COMPANY_ID."'
                UNION SELECT 'INV' as k, count(*) AS ref1, SUM( CASE WHEN ims_inv_physical.qty_on_hand=0 THEN 1 ELSE 0 END ) as ref2, '0' AS ref3 
                    FROM ims_inv_physical 
                    JOIN ims_item_price prc ON prc.item_id=ims_inv_physical.item_id and 
                        prc.coy_id = 'CTL' and prc.price_type = 'SUPPLIER' and prc.eff_from <= CURRENT_TIMESTAMP and prc.eff_to >= CURRENT_TIMESTAMP 
                        and prc.price_ind='PRI' 
                    WHERE ims_inv_physical.loc_id IN ('IM-C','VEN-C','DSH-C') and prc.ref_id='$supp_id'
                UNION SELECT 'DELV' as k, 
                    /*
                        SUM( CASE WHEN delv_date > dateadd(day, -6, CURRENT_TIMESTAMP) THEN 1 ELSE 0 END ) as ref1, 
                        SUM( CASE WHEN (delv_date < dateadd(day, -6, CURRENT_TIMESTAMP) and delv_date > dateadd(day, -31, CURRENT_TIMESTAMP) ) THEN 1 ELSE 0 END ) as ref2, 
                        SUM( CASE WHEN delv_date < dateadd(day, -31, CURRENT_TIMESTAMP) THEN 1 ELSE 0 END ) as ref3
                    */
                        SUM( CASE WHEN delv_date > CURRENT_TIMESTAMP - INTERVAL '6 day' THEN 1 ELSE 0 END ) as ref1, 
                        SUM( CASE WHEN (delv_date < CURRENT_TIMESTAMP - INTERVAL '6 day' and delv_date > CURRENT_TIMESTAMP - INTERVAL '31 day' ) THEN 1 ELSE 0 END ) as ref2, 
                        SUM( CASE WHEN delv_date < CURRENT_TIMESTAMP - INTERVAL '31 day' THEN 1 ELSE 0 END )::TEXT as ref3
                      
                    FROM pms_po_list 
                    WHERE supp_id='$supp_id' and status_level=2 and rev_num=current_rev 
                        AND coy_id='".Base_Common_Model::COMPANY_ID."';";
        $results = CI::db()->query($sql)->result_array();

        if ($results) {
            $return = array();
            foreach ($results as $result) {
                $return[$result["k"]] = array(
                    "ref1" => $result["ref1"],
                    "ref2" => $result["ref2"],
                    "ref3" => $result["ref3"]
                );
            }
            return $return;
        }

        return FALSE;
    }

}

?>
