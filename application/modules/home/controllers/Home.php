<?php

/* YS
 * 20-07-2016
 */

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Home extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('home/home_model');
        $this->load->model('login/Login_model');
//        $this->load->model('home/utility_model');
//        $this->load->model('home/home_constants');

        if (!$this->session->userdata('cm_logged_in'))
            redirect("login");
    }

    public function index() {
        
        // Delete supp files more than 24hours
        $supp_id = $this->session->userdata('cm_supp_id');
        $directory = FCPATH . '/' . 'assets/uploads/';
        foreach (glob("$directory*$supp_id*.*") as $key => $file) {
            if (filemtime($file) < time() - 86400) {
                $f = basename($file);
                unlink($directory . $f);
            }
        } 
        
        $data["summary"] = $this->home_model->dashboard_summary();
        
        $data["page_title"] = 'Dashboard';
        Template::set($data);
        Template::render();
    }
    
    public function account(){
        
        $param_acct['supp_id'] = $this->session->userdata('cm_supp_id');
        $data["account"] = $this->home_model->pms_supplier_list($param_acct)[0];
        
        $old_pwd = $this->input->post("old_pwd");
        $new_pwd = $this->input->post("new_pwd");
        $new_pwd2 = $this->input->post("new_pwd2");
        
        if ($new_pwd != $new_pwd2) {
            $data["error_message"] = 'Fail to update password. The new passwords do not match.';
        }
        else if ($old_pwd!="" && strlen($new_pwd)>=6){
            if ($this->session->userdata('r_access_type')=="ADMIN") {
                $params["supp_id"] = $this->session->userdata('cm_supp_id');
                $params["old_pwd"] = $old_pwd;
                $params["new_pwd"] = $new_pwd;
                $result = $this->Login_model->change_password($params);
            }
            else {
                $params["supp_id"] = $this->session->userdata('cm_supp_id');
                $params["acct_id"] = $this->session->userdata('cm_user_id');
                $params["old_pwd"] = $old_pwd;
                $params["new_pwd"] = $new_pwd;
                $result = $this->Login_model->change_password_b2busr($params);
            }
            if ($result) {
                $data["success_message"] = 'Password updated successfully.';
            }
            else {
                $data["error_message"] = 'Your current password is incorrect. Please try again.';
            }
        }
        else if ($old_pwd!=""){
            $data["error_message"] = 'Fail to update password. Please try again.';
        }
        
        $data["page_title"] = 'Account Details';
        Template::set($data);
        Template::render();
    }
    
    public function UserActivityLog(){

        $supp_id = $this->session->userdata('cm_supp_id');

        $msg = "CHVOICES Activity Log\n";
        $msg.= "\nSupplier: " .  $supp_id;
        $msg.= "\nDate: " .  date("Y-m-d H:i:s");
        $msg.= "\n\nLog:";
        $msg.= file_get_contents('php://input');
        
        if (ENVIRONMENT=="production") {
            $subj = "CHVOICES Activity Log ".$supp_id."";
            $sql = "exec sp_SysSendDBmail @from_address = 'chvoices@challenger.sg', @recipients = 'yongsheng@challenger.sg', @copy_recipients = '', @subject = '".$subj."', @body = '".$msg."', @file_attachments = '' ";
            $sendmail = CI::db()->query($sql);
        }
        else {
            $handle_path = FCPATH . 'assets/uploads/';
            $uploadedcsv = $supp_id . '_actlog_'.time().'.csv';
            
            file_put_contents($handle_path.$uploadedcsv, $msg, FILE_APPEND | LOCK_EX);
        }
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
