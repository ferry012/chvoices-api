<?php

/* YS
 * 20-07-2016
 */
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Invoice_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }

    public function upload_image($file, $name) {
        $prefix = $this->session->userdata('cm_supp_id');
        $upload = $this->_upload($file, $name, "assets/uploads/", "*", $prefix . '_');
        return $upload;
    }

    public function copyfile($old_path, $new_path) {
        $upload = $this->_CopyFileServer($old_path,$new_path);
        return $upload;
    }

    public function get_fin_journal($params) {
        $sql_search = '';
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (fin_journal.trans_id LIKE '%" . $params["search"] . "%'"
            . " OR fin_journal.doc_ref LIKE '%" . $params["search"] . "%') " : "";
        $sql_search.= (isset($params["search_to"]) && $params["search_to"] != NULL) ? " AND fin_journal.doc_date <= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_to"]))) . "'" : "";
        $sql_search.= (isset($params["search_from"]) && $params["search_from"] != NULL) ? " AND fin_journal.doc_date >= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_from"]))) . "'" : "";

        $sql = "SELECT
                        fin_journal.trans_id ,
                        MAX( fin_journal.doc_ref ) doc_ref, 
                        MAX( fin_journal.doc_date ) doc_date,
                        MAX( fin_journal.trans_date ) trans_date,
                        MAX( fin_journal.trans_amount ) trans_amount,
                        MAX( fin_journal.status_level ) status_level,
                        MAX( pms_grn_item.coy_id ) coy_id,
                        MAX( pms_grn_item.grn_id ) grn_id,
                        MAX( pms_grn_item.ref_id ) ref_id,
                        SUM( pms_grn_item.grn_qty ) grn_qty,
                        MAX( pms_grn_list.po_id ) po_id,
                        MAX( pms_grn_list.supp_do ) supp_do,
                        MAX( pms_grn_list.created_by ) created_by,
                        MAX( fin_journal.trans_amount ) - (case when (select amount_outst from fin_open_item where fin_open_item.coy_id = MAX(fin_journal.coy_id) and fin_open_item.trans_id = fin_journal.trans_id ) IS NULL then MAX( fin_journal.trans_amount ) else (select amount_outst from fin_open_item where fin_open_item.coy_id = MAX(fin_journal.coy_id) and fin_open_item.trans_id = fin_journal.trans_id) end) paid_amount
                    FROM fin_journal 
                    JOIN pms_grn_item ON pms_grn_item.trans_id = fin_journal.trans_id
                    JOIN pms_grn_list ON pms_grn_list.grn_id = pms_grn_item.grn_id
                    WHERE trans_type='PI' AND cust_supp_id='" . $params["supp_id"] . "' 
                        AND fin_journal.coy_id='".Base_Common_Model::COMPANY_ID."'
                        $sql_search
                    GROUP BY fin_journal.trans_id ORDER BY doc_ref ASC LIMIT 1000";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function get_pms_grn_item($params) {
        $sql_search = '';
        $sql_search.= (isset($params["search_po"]) && $params["search_po"] != NULL) ? " AND (pms_grn_list.po_id IN ('" . $params["search_po"] . "') ) " : "";
        $sql_search.= (isset($params["search_grn"]) && $params["search_grn"] != NULL) ? " AND (pms_grn_item.grn_id IN ('" . $params["search_grn"] . "') ) " : "";
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (pms_grn_item.trans_id LIKE '%" . $params["search"] . "%'"
            . " OR pms_grn_list.po_id LIKE '%" . $params["search"] . "%' "
            . " OR pms_grn_item.grn_id LIKE '%" . $params["search"] . "%') " : "";
        $sql_search.= (isset($params["search_to"]) && $params["search_to"] != NULL) ? " AND pms_grn_list.date_received <= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_to"]))+86400) . "'" : "";
        $sql_search.= (isset($params["search_from"]) && $params["search_from"] != NULL) ? " AND pms_grn_list.date_received >= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_from"]))+86400) . "'" : "";
        $sql_status = (isset($params["status_level"]) && $params["status_level"] != NULL) ? "AND pms_grn_item.status_level IN (" . $params["status_level"] . ")" : "";
        //$sql_status.= (isset($params["status_level"]) && $params["status_level"] == "0") ? " AND pms_grn_item.unit_price>0" : "";
        $sql = "SELECT
                        pms_grn_item.coy_id,
                        pms_grn_item.grn_id,
                        pms_grn_item.line_num,
                        pms_grn_item.trans_id,
                        pms_grn_list.po_id,
                        pms_grn_item.item_id,
                        pms_grn_item.item_desc,
                        pms_grn_item.status_level,
                        sys_status_list.status_desc,
                        pms_grn_item.grn_qty,
                        pms_grn_item.unit_price,
                        pms_grn_item.disc_percent,
                        (pms_grn_item.unit_price * pms_grn_item.grn_qty) as amount,
                        (pms_grn_item.unit_price * pms_grn_item.grn_qty * (coy_tax_list.tax_percent/100)) as tax_amt,
                        pms_grn_list.date_received,
                        pms_grn_list.supp_do,
                        coy_tax_list.tax_id,
                        coy_tax_list.tax_desc,
                        coy_tax_list.tax_percent,
                        pms_po_list.curr_id
                  FROM pms_grn_item
                  JOIN pms_grn_list ON pms_grn_list.grn_id = pms_grn_item.grn_id AND pms_grn_list.coy_id = pms_grn_item.coy_id
                  JOIN pms_po_list ON pms_po_list.po_id = pms_grn_list.po_id AND pms_po_list.coy_id = pms_grn_list.coy_id AND pms_po_list.rev_num=pms_po_list.current_rev
                  JOIN coy_tax_list ON coy_tax_list.tax_id = pms_po_list.tax_id AND coy_tax_list.coy_id = pms_po_list.coy_id
                  LEFT JOIN sys_status_list ON sys_status_list.status_level = pms_grn_item.status_level AND sys_status_list.ref_id = 'grn_item'
                  WHERE pms_grn_list.supp_id = '" . $params["supp_id"] . "'
                      AND pms_grn_list.order_type IN ('I','N','C')
                      AND pms_grn_list.coy_id='".Base_Common_Model::COMPANY_ID."'
                     $sql_status $sql_search LIMIT 5000;";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function update_pms_grn_item($params) {
        $sql = "UPDATE pms_grn_item
                  SET trans_id='" . $params["trans_id"] . "', status_level='" . $params["status_level"] . "', 
                      modified_by='" . $params["modified_by"] . "', modified_on='" . $params["modified_on"] . "'
                  WHERE coy_id='" . $params["coy_id"] . "' AND grn_id='" . $params["grn_id"] . "' AND line_num='" . $params["line_num"] . "'; ";

        $result = CI::db()->query($sql);
        return $result;
    }

    public function get_pending_pms_grn_item($grn_id) {
        $sql = "SELECT line_num
                  FROM pms_grn_item
                  WHERE grn_id='" . $grn_id . "' AND status_level=0";

        $result = CI::db()->query($sql)->result_array();

        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function update_pending_pms_grn_item($params) {
        $sql = "UPDATE pms_grn_list
                  SET status_level='" . $params["status_level"] . "', 
                      modified_by='" . $params["modified_by"] . "', modified_on='" . $params["modified_on"] . "'
                  WHERE grn_id='" . $params["grn_id"] . "' ";

        $result = CI::db()->query($sql);
        return $result;
    }

    public function gen_pi(array $params){
//        $sql = "EXEC sp_InsJournal_PI ?,?,?,?,?,?,?,?,''";
//        $inv_date = CI::db()->query($sql, [ $params["coy_id"] , $params["inv_num"] , $params["inv_date"] , $params["supp_id"] , $params["curr_id"], $params["inv_amt"], $params["tax_id"] , $params["tax_percent"] ])->row();
//        return !empty($inv_date) ? $inv_date->TransId : NULL;

        $doc_prefix = 'PI' . date('Ym');
        $coy_id = $params["coy_id"];
        $trans_id = $this->_SysNewDocId('CTL','APS','TRANS_TYPE',$doc_prefix,15); // Get new PI num

        $curr_id = $params["curr_id"];
        $exchg_rate = 1; // ($curr_id!='SGD') ? 1 : 1;
        $exchg_unit = 1; // ($curr_id!='SGD') ? 1 : 1;
        if ($curr_id != 'SGD'){
            $sql_curr = "SELECT exchg_rate, exchg_unit from coy_foreign_exchg
			                where coy_id = 'CTL' and curr_id = '$curr_id'  and eff_from <= CURRENT_TIMESTAMP and eff_to >= CURRENT_TIMESTAMP; ";
            $result_curr = CI::db()->query($sql_curr)->result_array();
            $exchg_rate = $result_curr[0]['exchg_rate'];
            $exchg_unit = $result_curr[0]['exchg_unit'];
        }

        $supp_id = trim($params["supp_id"]);
        $supp_name = $params["supp_name"];
        $inv_num = $params["inv_num"];
        $inv_date = $params["inv_date"];
        $inv_amt = $params["inv_amt"];
        $inv_amt_local = $params["inv_amt"] * $exchg_rate / $exchg_unit;
        $tax_id = $params["tax_id"];
        $tax_percent = $params["tax_percent"];
        $due_date = $params["inv_date"];
        $sql = "insert into fin_journal
                 values ('$coy_id', 'APS', '$trans_id', 'PI', 0, CURRENT_TIMESTAMP, '', '$inv_num', '$inv_date', '', '$supp_id', '$supp_name', '$curr_id', '$inv_amt', '$inv_amt_local',
                            $exchg_rate, $exchg_unit, '$tax_id', '$tax_percent', '', '', 0, 0, '', '', 'N', '1900-01-01', '1900-01-01', '', '$due_date', 'chvoices', CURRENT_TIMESTAMP, '$supp_id', CURRENT_TIMESTAMP) ";
        $result = CI::db()->query($sql);
        return $trans_id;
    }

    public function get_pi($pi_id) {
        $sql = "SELECT trans_id FROM fin_journal WHERE trans_id = '".$pi_id."'";
        $result = CI::db()->query($sql)->result_array();
        return ($result) ? $result : FALSE;
    }

    public function get_pi_docref($doc_ref,$supp_id) {
        $sql = "SELECT doc_ref FROM fin_journal 
                    WHERE coy_id='CTL' and trans_type='PI' and doc_ref = '".$doc_ref."' and cust_supp_id='".$supp_id."'  and status_level >= 0";
        $result = CI::db()->query($sql)->result_array();
        return ($result) ? $result : FALSE;
    }

    public function get_pi_docdate($doc_ref,$supp_id) {
        $sql = "SELECT trans_id,doc_date FROM fin_journal WHERE doc_ref = '".$doc_ref."' and cust_supp_id='".$supp_id."'  and status_level >= 0";
        $result = CI::db()->query($sql)->result_array();
        return ($result) ? $result : FALSE;
    }

    public function upd_pi_docdate($trans_id,$doc_date) {
        $sql = "UPDATE fin_journal SET doc_date='$doc_date' WHERE trans_id='$trans_id' ";
        $result = CI::db()->query($sql);
        return $result;
    }

    public function CallSP_email(array $params) {
        $result = $this->_SendEmail($params);
        return $result;
    }

    public function get_tax_list() {
        $sql = "SELECT * from coy_tax_list where coy_id='CTL'";
        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }
    }

    public function update_tax_item($params) {
        $sql = "UPDATE pms_po_list
                  SET po_id='" . $params["po_id"] . "', 
                      tax_id='" . $params["tax_id"] . "', tax_percent='" . $params["tax_percent"] . "'
                  WHERE po_id='" . $params["po_id"] . "' ";

        $result = CI::db()->query($sql);
        return $result;
    }

    public function get_pms_po_list($params) {
        $sql_search = '';
        $sql_search.= (isset($params["search_po"]) && $params["search_po"] != NULL) ? " AND (pms_po_list.po_id IN ('" . $params["search_po"] . "') ) " : "";
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (pms_po_list.po_id LIKE '%" . $params["search"] . "%'"
            . " OR pms_po_list.supp_name LIKE '%" . $params["search"] . "%' "
            . " OR pms_po_list.supp_id LIKE '%" . $params["search"] . "%') " 
            : "";
        
        $sql = "SELECT distinct on (pms_po_list.coy_id, pms_po_list.po_id)
                pms_po_list.coy_id,
                pms_po_list.po_id,
                sys_status_list.status_desc,
                pms_po_list.po_date,
                pms_po_list.tax_id,
                pms_po_list.tax_percent,
                pms_po_list.curr_id,
                pms_po_list.supp_id,
                pms_po_list.supp_name,
                pms_po_list.loc_id
            FROM pms_grn_item
            JOIN pms_grn_list ON pms_grn_list.grn_id = pms_grn_item.grn_id AND pms_grn_list.coy_id = pms_grn_item.coy_id
            JOIN pms_po_list ON pms_grn_list.coy_id = pms_po_list.coy_id AND pms_grn_list.po_id = pms_po_list.po_id
            LEFT JOIN sys_status_list ON sys_status_list.status_level = pms_po_list.status_level AND sys_status_list.ref_id = 'po_list'
            WHERE pms_grn_list.coy_id='CTL' AND pms_grn_list.supp_id = 'ER0001'
            AND pms_grn_list.order_type IN ('I','N','C') -- Only these GRN order type requires matching by vendor
            AND pms_grn_item.status_level=0 AND pms_grn_item.trans_id='' -- Those matched will have a fin_journal.trans_id saved into pms_grn_item.trans_id 
            $sql_search LIMIT 5000";

        $result = CI::db()->query($sql)->result_array();
        if ($result) {
            return $result;
        }

        return FALSE;
    }


}

?>
