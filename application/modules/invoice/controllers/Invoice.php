<?php

/* YS
 * 20-07-2016
 */

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Invoice extends Front_Controller {

    private $loadCopydo;
    private function NetworkDriveDown(){
        //$this->session->set_flashdata('error_message', 'We are experiencing problem uploading supporting documents. Please try again later.');
        $this->loadCopydo = ['SS0011','LS0003','AC0015','SA0006','SE0008'];

        $t = date('Hi');
        if (date('N')==5 && $t > 830 && $t < 930) {
            $this->session->set_flashdata('error_message', '<b>Weekly System Maintenance (Every Friday 8.30am to 9.30am)</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; During the maintenance period, you may experience problem at Invoice Matching. Please try again later.');
        }
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('invoice/Invoice_model');
        $this->load->model('login/Login_model');
        $this->load->library('session');
        $this->_checkLogged(); 
    }

    public function quickcheck($mode,$id) {
        $return = array("code"=>1);
        if ($mode=='invoice_id') {
            $supp_id = $this->session->userdata('cm_supp_id');
            $checkdocref = $this->Invoice_model->get_pi_docref(trim($id),trim($supp_id));
            if ($checkdocref) {
                $return = array("code"=>0, "msg"=>'Invoice number already matched to another PO.');
            }
        }
        echo json_encode($return);
    }

    public function update_tax() { 
        $this->NetworkDriveDown();

        $supp_id = $this->session->userdata('cm_supp_id');
        
        $data["page_title"] = 'Invoice Tax Update';
        $data["sidebar"] = 'Invoice';

        $tax_list = $this->Invoice_model->get_tax_list();
        $data["tax_list"] = $tax_list;

        $params["supp_id"] = $supp_id;
        // allow invoice tax update only for invoices that are (3) partially / (4) fully received
        $params['status_level'] = "3,4";
        
        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
        } 
        // else {
        //     $params["status_level"] = "0";
        // }
        $po_items = $this->Invoice_model->get_pms_po_list($params);
        $data["results"] = $po_items;


        $selected_post = $this->input->post('invoice_selected');
        $inv_tax = $this->input->post('tax');
        $inv_tax_id = explode('---', $inv_tax)[0];
        $inv_tax_percent = explode('---', $inv_tax)[1];

        $data['selected_post'] = $selected_post;
        $data['inv_tax_id'] = $inv_tax_id;
        $data['inv_tax_percent'] = $inv_tax_percent;

        $error_counter = 0;

        // on submit
        if (isset($_POST['submit'])) {
            // if fields are filled
            if (isset($selected_post) && $inv_tax !== "") {
                $params_item['tax_id'] = $inv_tax_id;
                $params_item['tax_percent'] = $inv_tax_percent;

                foreach($selected_post as $selected_invoice) {
                    $params_item['po_id'] = $selected_invoice;
                    $output = $this->Invoice_model->update_tax_item($params_item);
                    if (!$output) {
                        $error_counter++;
                    }
                }
                // get list again to show tax updates
                $po_items = $this->Invoice_model->get_pms_po_list($params);
                $data["results"] = $po_items;
            }
        }

        $data['error_counter'] = $error_counter;
        

        $this->load->view('update_tax', $data);

        Template::set($data);
        Template::render();
    }

    // Single Invoice Matching. Use grn_items to list status=0 items
    public function submit_new_match() { 
        $this->NetworkDriveDown();

        $supp_id = $this->session->userdata('cm_supp_id');
        $data['loadCopydo'] = (in_array($supp_id, $this->loadCopydo )) ? 1 : 0 ;
        
        $data["page_title"] = 'Invoice Matching (Single)';
        $data["sidebar"] = 'Invoice';
        Template::set($data);
        Template::render();
    }

    // Batch Invoice Matching. 
    public function submit_batch() {
        $this->NetworkDriveDown();
        
        // Page for batch matching - redeployment of fx in submit_new_save_batch()
        // 1. Upload CSV
        $step1_csv = $this->submit_new_save_batch(1); 
        $data["step1_csv"] = $step1_csv;
        
        // 2. Upload Files
        $data["page_title"] = 'Invoice Matching (Batch)';
        $data["sidebar"] = 'Invoice'; 
        Template::set($data);
        Template::render();
    }

    // Invoices listing page. Use grn_items & grn_items_files to show details
    public function listing() {
        $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : date('d-m-Y', strtotime("-1 months"));
        $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : date('d-m-Y');

        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
            $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : '';
            $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : '';
        }
        if (isset($data["search_from"]))
            $params["search_from"] = $data["search_from"];
        if (isset($data["search_to"]))
            $params["search_to"] = $data["search_to"];

        $supp_id = $this->session->userdata('cm_supp_id');
        $params["supp_id"] = $supp_id ;
        $grn_items = $this->Invoice_model->get_fin_journal($params);
        $data["results"] = $grn_items;
        //echo "<pre>";print_r($data["results"]);exit;
        //if ( true || isset($export) && $export != NULL) {
        if (isset($grn_items) && count($grn_items)>=1 && isset($grn_items[0]["doc_ref"]) ) {
            // Prepares CSV file for user to download on request 
            $handle_path = FCPATH . 'assets/uploads/';
            $uploadedcsv = $supp_id . '_matchedinvoices.csv';
            $fp = fopen($handle_path . $uploadedcsv, 'w');
            fputcsv($fp, array('Your Invoice','Inv Date','Inv Amount','Our PO','Your DO','Our Ref','Trans Date','Amount Paid','Status','Items')); 

            foreach ($grn_items as $result) {
                $a = array();
                $a[] = $result["doc_ref"];
                $a[] = display_date_format($result["doc_date"]);
                $a[] = price_no_symbol($result["trans_amount"]);
                $a[] = $result["po_id"];
                $a[] = $result["supp_do"];
                $a[] = $result["trans_id"];
                $a[] = display_date_format($result["trans_date"]);
                $a[] = price_no_symbol($result["paid_amount"]);
                $a[] = $result["status_level"]==2 ? 'Paid' : 'Pending' ;
                $a[] = display_number($result["grn_qty"]);
                fputcsv($fp, $a);
            }
            fclose($fp);

            if (file_exists($handle_path . $uploadedcsv)) {
                $data["exportcsv"] = $uploadedcsv;
            }
        }
        //}

        $data["page_title"] = 'Invoice Info';
        $data["sidebar"] = 'Invoice';
        Template::set($data);
        Template::render();
    }

    public function get_grn_items($readonly = 0) {
        $export = $this->input->get("export");
        $supp_id = $this->session->userdata('cm_supp_id');
        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
        } else {
            $params["status_level"] = "0";
        }
        $params["supp_id"] = $supp_id;
        $grn_items = $this->Invoice_model->get_pms_grn_item($params);

        $upload_type = $this->input->post('upload_type');
        if (isset($upload_type)) {
            // Read from CSV if matched items is uploaded
            $u = $this->Invoice_model->upload_image('upload_file', '');
            $uploadedcsv = $u["upload_data"]["file_name"];
            $selected_post = array();
            $handle_path = FCPATH . 'assets/uploads/';
            if (file_exists($handle_path . $uploadedcsv)) {
                $handle = fopen($handle_path . $uploadedcsv, "r");

                $line = 0;
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($line > 0 && $data[0] == "1") {
                        $selected_post[] = $data[1];
                    }
                    $line++;
                }
            }

            foreach ($grn_items as $key => $item) {
                if (in_array($item["coy_id"] . '---' . $item["grn_id"] . '---' . $item["line_num"], $selected_post)) {
                    $grn_items[$key]["checked"] = 1;
                } else {
                    unset($grn_items[$key]);
                }
            }
            sort($grn_items);
        }


        if (isset($export) && $export != NULL) {
            // Prepares CSV file for user to download on request
            $handle_path = FCPATH . 'assets/uploads/';
            $uploadedcsv = $supp_id . '_grnitems.csv';
            $fp = fopen($handle_path . $uploadedcsv, 'w');
            fputcsv($fp, array('PO_ID', 'Item_ID', 'Item_Desc', 'GRN_Qty', 'Unit_Price', 'Disc', 'Amount', 'Tax_Amt', 'Supp_DO', 'Recv_Date', 'PO_Status', 'Tax', 'Curr'));
            foreach ($grn_items as $result) {
                $a = array();
                //$a[] = '';
                //$a[] = $result["coy_id"] . '---' . $result["grn_id"] . '---' . $result["line_num"];
                $a[] = $result["po_id"];
                $a[] = '"'.$result["item_id"].'"';
                $a[] = $result["item_desc"];
                $a[] = display_number($result["grn_qty"]);
                $a[] = price_no_symbol($result["unit_price"]);
                $a[] = display_number($result["disc_percent"]);
                $a[] = price_no_symbol($result["amount"]);
                $a[] = price_no_symbol($result["tax_amt"]);
                $a[] = $result["supp_do"];
                $a[] = display_date_format($result["date_received"]);
                $a[] = $result["status_desc"];
                $a[] = display_number($result["tax_percent"]);
                $a[] = $result["curr_id"];
                fputcsv($fp, $a);
            }
            fclose($fp);

            if (file_exists($handle_path . $uploadedcsv)) {
                $data["exportcsv"] = $uploadedcsv;
            }
        }

        $data["results"] = $grn_items;
        $data["readonly"] = $readonly;

        // $tax_list = $this->Invoice_model->get_tax_list();
        // $data["tax_list"] = $tax_list;
        // echo "<pre>";print_r($data["tax_list"]);exit;
        $this->load->view('get_grn_items', $data);
    }
    
    public function get_grn_items_files($inv){

        $directory =  FILE_SERVER . '/pdf/SUPP_INV/';
        $supp_id = $this->session->userdata('cm_supp_id');

        $doc_date = $this->Invoice_model->get_pi_docdate($inv,$supp_id);
        $file_prefixdate = (strtotime($doc_date[0]['doc_date'])>=START_PREFIXDATE) ? date(SUPP_INV_PREFIXDATE, strtotime($doc_date[0]['doc_date'])) : '';
        $file_prefix = $file_prefixdate.$supp_id.'_'.$inv;
//        foreach (glob("$directory*$file_prefix*.*") as $key => $file) {
//            $uploadedfiles[] = basename($file);
//        }
        if (file_exists($directory.$file_prefix.'.pdf')) $uploadedfiles[] = $file_prefix.'.pdf' ;
        if (file_exists($directory.$file_prefix.'-INV.pdf')) $uploadedfiles[] = $file_prefix.'-INV.pdf' ;
        if (file_exists($directory.$file_prefix.'-DO.pdf')) $uploadedfiles[] = $file_prefix.'-DO.pdf' ;
        
        //echo "<pre>"; print_r($uploadedfiles); echo "</pre>";
        
        if (count($uploadedfiles)>0){
            
            foreach ($uploadedfiles as $filename) {
                if (strpos('-' . $filename, $file_prefix)) {
                    $filetype = $this->upload_scan_filetype($filename);
                    echo '<div style="float:left;text-align:center;padding:0px 15px"><a title="' . $filename . '" href="' . base_url("login/displayfile/pdf/".base64_encode($directory.$filename)) . '" class="popuplink"><span class="fa-2x fa fa-file-pdf-o"></a><br>' . $filetype . '</div>';
                    echo '<input type="hidden" name="uploads[]" value="' . $filename . '">';
                }
            }
        }
        else {
            echo '<div style="float:left;text-align:center;padding:0px 15px"><a title="#" class="popuplink"><span class="fa-2x fa fa-file-pdf-o" style="color:#BBB"></a><br>NO FILE</div>';
        }
        
    }

    //
    // Supporting documents function - Single Upload, Batch Upload, Doc Delete
    //

    public function submit_new_match_upload() { 
        // Only uploading of documents - Single

        $supp_id = $this->session->userdata('cm_supp_id');
        $inv_no = $this->input->post('inv_no');
        $file_prefix = cleanString($supp_id . '_' . $inv_no);

        $uploadedfiles = array();
        $directory = FCPATH . '/' . 'assets/uploads/';
        foreach (glob("$directory*$file_prefix*.*") as $key => $file) {
            $uploadedfiles[] = basename($file);
        }
        foreach ($uploadedfiles as $k => $filename) {
            if (strpos('-' . $filename, $file_prefix)) {
                unlink($directory.$filename);
                unset($uploadedfiles[$k]);
            }
        }

        $del_do = '';
        if (isset($_POST['upload_copy_do']) && $_POST['upload_copy_do']!='') {
            $file_prefix_tail = '-DO';
            $uploadfile = $file_prefix.$file_prefix_tail.'.pdf';
            $u = $this->Invoice_model->copyfile( $_POST['upload_copy_do'] , $directory.$uploadfile );
            if ($u) {
                $del_do = $_POST['upload_copy_do'];
                $uploadedfiles[] = $uploadfile;
            }
        }
        if (isset($_FILES["upload_file_inv"])) {
            $file_prefix_tail = '-INV';
            $u = $this->Invoice_model->upload_image("upload_file_inv", $file_prefix . $file_prefix_tail);
            if (!in_array($u["upload_data"]["file_name"], $uploadedfiles)) {
                $uploadedfiles[] = $u["upload_data"]["file_name"];
            }
        }
        if (isset($_FILES["upload_file_do"])) {
            $file_prefix_tail = '-DO';
            $u = $this->Invoice_model->upload_image("upload_file_do", $file_prefix . $file_prefix_tail);
            if (!in_array($u["upload_data"]["file_name"], $uploadedfiles)) {
                $uploadedfiles[] = $u["upload_data"]["file_name"];
            }
        }
        if (isset($_FILES["upload_file_invdo"])) {
            $file_prefix_tail = '';
            $u = $this->Invoice_model->upload_image("upload_file_invdo", $file_prefix . $file_prefix_tail);
            if (!in_array($u["upload_data"]["file_name"], $uploadedfiles)) {
                $uploadedfiles[] = $u["upload_data"]["file_name"];
            }
        }

        $k=0;
        foreach ($uploadedfiles as $filename) {
            if (strpos('-' . $filename, $file_prefix)) {
                $filetype = $this->upload_scan_filetype($filename);
                echo '<div id="myUploadFiles_file'.$k.'" style="float:left;text-align:center;padding:0px 15px"><a title="' . $filename . '" href="' . base_url("assets/uploads/" . $filename) . '" class="popuplink"><span class="fa-2x fa fa-file-pdf-o"></a><br>';
                    echo '<div class="label label-default" style="margin-top:2px;padding:2px;display:block;width:100%;min-width:35px;text-align:left;"><div style="padding:0px">';
                    echo $filetype;
                    echo '<a href="#" data-target="myUploadFiles_file'.$k.'" title="Remove File" class="myUploadModalSender_Delete" style="float:right;color:#F00">X</a>';
                    echo '</div></div>';
                echo '<input type="hidden" name="uploads[]" id="uploads_'.$k.'" value="' . $filename . '">';
                echo '</div>';
                $k++;
            }
        }
        if ($del_do!=''){
            echo '<input type="hidden" name="copy_do_del" value="'.$del_do.'">';
        }
        echo '<script>$(\'.popuplink\').click(function (e){var url = $(this).attr("href");window.open(url, \'PopUp\', \'window settings\');return false;});</script>';
    }

    public function submit_new_save_batch_upload() {
        // Only uploading of documents - Shift+Select
        
        $supp_id = $this->session->userdata('cm_supp_id');
        $file_prefix = cleanString($supp_id . '_');

        $uploadedfiles = array();
        $directory = FCPATH . '/' . 'assets/uploads/';
        foreach (glob("$directory*$file_prefix*.pdf") as $key => $file) {
            $uploadedfiles[] = basename($file);
        }

        foreach ($_FILES as $fieldname => $fileObject) {  //fieldname is the form field name
            $u = $this->Invoice_model->upload_image($fieldname, '');
            $f = $u["upload_data"]["file_name"];
            if (!in_array($f,$uploadedfiles)) {
                $uploadedfiles[] = $f;
            }
        }
        
        if ($this->input->get("uploadedcsv_name") && $this->input->get("uploadedcsv_name")!="") {
            echo json_encode($uploadedfiles);
            exit;
        }
        
        $k=0;
        foreach ($uploadedfiles as $filename) {
            if (strpos('-' . $filename, $file_prefix)) {
                $filetype = $this->upload_scan_filetype($filename);
                $fileidentity = str_replace($file_prefix,'',$filename);
                echo '<div class="col-xs-4"><div class="label label-default" style="margin-bottom:5px;display:block;width:100%;text-align:left"><div style="padding:5px 3px 3px 3px">';
                echo '<a title="' . $filename . '" href="' . base_url("assets/uploads/" . $filename) . '" class="popuplink" style="font-weight:normal;color:#000"><i class="fa fa-file-pdf-o"></i>&nbsp; ' . $fileidentity . '</a>';
                echo '<a href="#" id="'.$filename.'" title="Remove File" class="myBatchModalFilesDelete" style="float:right;color:#F00">X</a>';
                echo '</div></div></div>';
                echo '<input type="hidden" name="uploads[]" id="uploads_'.$k.'" value="' . $filename . '">';
                $k++;
            }
        }
            
        echo '<script>$(\'.popuplink\').click(function (e){var url = $(this).attr("href");window.open(url, \'PopUp\', \'window settings\');return false;});</script>';
    }

    public function submit_new_save_batch_upload_delete() {
        // Only delete one uploaded document - Single

        $supp_id = $this->session->userdata('cm_supp_id');
        $file_prefix = cleanString($supp_id . '_');
        
        $del_filename = $this->input->get("filename");

        $uploadedfiles = array();
        $directory = FCPATH . '/' . 'assets/uploads/';
        foreach (glob("$directory*$file_prefix*.*") as $key => $file) {
            $f = basename($file);
            
            if ($f==$del_filename){
                unlink($directory.$f);
            }
            else {
                $uploadedfiles[] = basename($f);
            }
        }
        
        if ($this->input->get("uploadedcsv_name") && $this->input->get("uploadedcsv_name")!="") {
            echo json_encode($uploadedfiles);
            exit;
        }
        
        $k=0;
        foreach ($uploadedfiles as $filename) {
            if (strpos('-' . $filename, $file_prefix)) {
                $filetype = $this->upload_scan_filetype($filename);
                $fileidentity = str_replace($file_prefix,'',$filename);
                echo '<div class="col-xs-4"><div class="label label-default" style="margin-bottom:5px;display:block;width:100%;text-align:left"><div style="padding:5px 3px 3px 3px">';
                echo '<a title="' . $filename . '" href="' . base_url("assets/uploads/" . $filename) . '" class="popuplink" style="font-weight:normal;color:#000"><i class="fa fa-file-pdf-o"></i>&nbsp; ' . $fileidentity . '</a>';
                echo '<a href="#" id="'.$filename.'" class="myBatchModalFilesDelete" style="float:right;color:#F00">X</a>';
                echo '</div></div></div>';
                echo '<input type="hidden" name="uploads[]" id="uploads_'.$k.'" value="' . $filename . '">';
                $k++;
            }
        }
            
        echo '<script>$(\'.popuplink\').click(function (e){var url = $(this).attr("href");window.open(url, \'PopUp\', \'window settings\');return false;});</script>';
    }

    public function get_do_files($po){
        $directory =  FILE_SERVER . '/pdf/SUPP_DO/';
        $filename = strtoupper(trim(urldecode($po))) . '.pdf';
        if (file_exists($directory.$filename)) {
            // Copy the file to this folder, return the address
            //$up = $this->Invoice_model->copyfile($directory . $filename, $uploaddir . $uploadfile);
            echo json_encode(array(
                "code"=>1,
                "file"=>$filename,
                "filepath"=>$directory.$filename,
                "url"=>base_url("login/displayfile/pdf/".base64_encode($directory.$filename))
            ));
        }
        else {
            echo json_encode(array("code"=>-1,"result"=>"FILE NOT FOUND"));
        }
    }

    private function upload_scan_filetype($filename) {
        if (strpos(substr($filename, -8), 'INV.'))
            $filetype = 'INV';
        else if (strpos(substr($filename, -8), 'DO.'))
            $filetype = 'DO';
        elseif (strpos(substr($filename, -4), 'PDF'))
            $filetype = 'PDF';
        else
            $filetype = 'ALL';
        return $filetype;
    }

    public function submit_new_save() {

        $inv_num = $this->input->post('inv_no');
        $inv_date = $this->input->post('inv_date');
        $inv_amt = $this->input->post('inv_amt');

        // Uploaded files
        $uploads_post = $this->input->post('uploads');
        foreach ($uploads_post as $upload) {
            $uploads[] = $upload;
        }

        // Verify selected items 
        $grn_ids = array();
        $tally_amt = 0;
        $selected_post = $this->input->post('inv_selected');
        foreach ($selected_post as $select) {
            $sel = explode("---", $select);
            $selected[] = $sel;
            if (!in_array($selected[1], $grn_ids)) {
                $grn_ids[] = $sel[1];
            }
        }
        
        $tax_settings = array();
        
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        $params["search_grn"] = implode("','", $grn_ids);
        $grn_items = $this->Invoice_model->get_pms_grn_item($params);
        if ($grn_items) {
            foreach ($grn_items as $item) {
                if (in_array($item["coy_id"] . '---' . $item["grn_id"] . '---' . $item["line_num"], $selected_post)) {
                    //$selected[] = array($item["coy_id"],$item["grn_id"],$item["line_num"]);
                    $tally_amt += $item["amount"] + $item["tax_amt"];
                    $tax_settings["coy_id"] = $item["coy_id"];
                    $tax_settings["tax_id"] = $item["tax_id"];
                    $tax_settings["tax_percent"] = $item["tax_percent"];
                    $tax_settings["curr_id"] = $item["curr_id"];
                }
            }
        }

        $delfile = ($this->input->post('copy_do_del')) ? $this->input->post('copy_do_del') : '';

        if (!( $inv_amt > 0 && ($inv_amt + 0.1) > $tally_amt && ($inv_amt - 0.1) < $tally_amt )) {
            $message = 'Selected items in PO $' . price_no_symbol($tally_amt) . ' do not match the invoice amount ($' . price_no_symbol($inv_amt) . ').';
            $this->session->set_flashdata('error_message', $message);
            redirect(base_url("invoice/submit_new_match"));
        } else if (count($selected) == 0) {
            $message = 'No valid items in PO';
            $this->session->set_flashdata('error_message', $message);
            redirect(base_url("invoice/submit_new_match"));
        } else if ( $this->save_matching('match', $uploads, $selected, $inv_num, $inv_date, $inv_amt, $tax_settings, $delfile) > 0 ) {
            $this->session->set_flashdata('success_message', $message);
            redirect(base_url("invoice/listing?search=" . $inv_num));
        } else {
//            $message = 'Fail to match this invoice.';
//            $this->session->set_flashdata('error_message', $message);
            redirect(base_url("invoice/submit_new_match"));
        }
    }

    public function submit_new_save_batch($runtype=0) {
        // $runtype == 0 : Normal ajax upload on single page app.
        // $runtype == 1 : Uploading CSV only, return CSV content. Use by submit_batch() page.
        
        $supp_id = $this->session->userdata('cm_supp_id');
        $upload_resultpdf_file = '';
        $upload_success = array();
        $upload_response = array();

        $uploadedcsv = ''; // Get the CSV file
        $uploaded = array();
        foreach ($_FILES as $fieldname => $fileObject) {  //fieldname is the form field name
            $u = $this->Invoice_model->upload_image($fieldname, '');
            $uploadedcsv = $u["upload_data"]["file_name"];
        }
        
        if (!(isset($uploadedcsv) && $uploadedcsv!='') && ($this->input->post("uploadedcsv_name")) ) {
            $uploadedcsv = $this->input->post("uploadedcsv_name");
        } 
        
        if (isset($uploadedcsv) && $uploadedcsv!='') { 
            $uploaded = array();
            $directory = FCPATH . '/' . 'assets/uploads/';
            foreach (glob("$directory*$file_prefix*.*") as $key => $file) {
                $uploaded[] = basename($file);
            }

            // Read CSV File for details
            $upload_results = array();
            $handle_path = FCPATH . 'assets/uploads/';
            if (file_exists($handle_path . $uploadedcsv)) {
                $handle = fopen($handle_path . $uploadedcsv, "r");

                $line = 0;
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($line > 0) {
                        $inv = (strpos('-' . $data[0], '#') == 1) ? trim(strtoupper(substr($data[0], 1))) : trim(strtoupper($data[0]));
                        $upload_results[$inv]["inv_amt"] = str_replace(',','',cleanString($data[1]));
                        $upload_results[$inv]["po_id"] = substr(cleanString($data[3]),0,13);
                        
                        $date = cleanString($data[2]);
                        $dateX = explode('/',$date);
                        if ( count($dateX)==3 && strlen($dateX[2])==2 ) {
                            $dateX[2] += 2000;
                            $data[2] = implode('/',$dateX);
                        }
                        $inv_date = display_date_format(cleanString($data[2]));
                        $upload_results[$inv]["inv_date"] = $inv_date;
                        
                        foreach ($uploaded as $file) {
                            if (strpos('-' . $file, $inv)) {
                                $upload_results[$inv]["upload"][] = $file;
                            }
                        }
                        
                        if ( (strtotime($inv_date)<(time()-160000000)) || (strtotime($inv_date)>(time()+160000000)) ){
                            $message = 'Invalid date format in file.';
                            $this->session->set_flashdata('error_message', $message);
                            return false;
                        }
                    }
                    $line++;
                }
            } 
            
            if ($runtype == 1){
                // Return $upload_results to fx submit_batch() 
                return array("uploadedcsv_name"=>$uploadedcsv,"upload_results"=>$upload_results);
            }
            
            $upload_resultpdf = array();
            $upload_resultpdf[] = array('Inv_Num','Inv_Amt','Inv_Date','PO_Num','Remarks','Status','');
            foreach ($upload_results as $inv => $uploaded) {

                $inv_num = $inv;
                $inv_date = date("d-m-Y", strtotime($uploaded["inv_date"]));
                $inv_amt = $uploaded["inv_amt"];
                $uploads = $uploaded["upload"];
                $po_id = $uploaded["po_id"];

                $upload_resultpdf_status = 'Error';
                if (!isset($uploaded["upload"])) {
                    $upload_response[$inv] = 'No INV/DO uploaded.';
                } else if (!($uploaded["inv_amt"] > 0)) {
                    $upload_response[$inv] = 'Invalid invoice amount.';
                } else if (!(validateDate($uploaded["inv_date"]))) {
                    $upload_response[$inv] = 'Invalid invoice date.';
                } else {
                    $upload_response[$inv] = 'Processing...';
                    $tax_settings = array();

                    // Get selected from DB and compare amount
                    $selected = array();
                    $tally_amt = 0;
                    $params["supp_id"] = $this->session->userdata('cm_supp_id');
                    $params["search_po"] = $uploaded["po_id"];
                    $params["status_level"] = "0";
                    $grn_items = $this->Invoice_model->get_pms_grn_item($params);
                    if ($grn_items) {
                        foreach ($grn_items as $item) {
                            $selected[] = array($item["coy_id"], $item["grn_id"], $item["line_num"]);
                            $tally_amt += $item["amount"] + $item["tax_amt"];
                            
                            $tax_settings["coy_id"] = $item["coy_id"];
                            $tax_settings["tax_id"] = $item["tax_id"];
                            $tax_settings["tax_percent"] = $item["tax_percent"];
                            $tax_settings["curr_id"] = $item["curr_id"];
                        }
                    }

                    if (!( $inv_amt > 0 && ($inv_amt + 0.1) > $tally_amt && ($inv_amt - 0.1) < $tally_amt )) {
                        if ($tally_amt>0) {
                            $upload_response[$inv] = 'Selected items in PO '.$po_id.' ($' . price_no_symbol($tally_amt) . ') do not match the invoice amount ($' . price_no_symbol($inv_amt) . ').';
                            $upload_resultpdf_status = 'Error';
                        }
                        else {
                            $upload_response[$inv] = 'No valid items found in PO '.$po_id.'';
                            $upload_resultpdf_status = 'Error';
                        }
                    } else if (count($selected) == 0) {
                        $upload_response[$inv] = 'Invalid PO number '.$po_id.' submitted.';
                        $upload_resultpdf_status = 'Error';
                    } else {
                        $save_matching_result = $this->save_matching('batch', $uploads, $selected, $inv_num, $inv_date, $inv_amt, $tax_settings);
                        if ($save_matching_result>0) {
                            $upload_response[$inv] = 'Invoice submitted successfully (' . count($selected) . ' items in PO '.$po_id.'). ';
                            $upload_resultpdf_status = 'OK';
                            $upload_success[] = $inv;
                        } else {
                            $upload_resultpdf_status = 'Error';
                            if ($save_matching_result == -2)
                                $upload_response[$inv] = 'Fail to match this invoice (PO ' . $po_id . '). No supporting documents uploaded.';
                            elseif ($save_matching_result == -1)
                                $upload_response[$inv] = 'Fail to match this invoice (PO ' . $po_id . '). Invoice number already matched to another PO.';
                            elseif ($save_matching_result == -3)
                                $upload_response[$inv] = 'Fail to match this invoice (PO ' . $po_id . '). Missing supporting document, no DO uploaded.';
                            elseif ($save_matching_result == -4)
                                $upload_response[$inv] = 'Fail to match this invoice (PO ' . $po_id . '). Missing supporting document, no INV uploaded.';
                            else
                                $upload_response[$inv] = 'Fail to match this invoice (PO ' . $po_id . ').';
                        }
                    }
                }
                
                $upload_resultpdf[] = array($inv_num,$inv_amt,$inv_date,$po_id,$upload_response[$inv],$upload_resultpdf_status);
                
            }
            
            if (count($upload_resultpdf)>1){
                // Prepares CSV file for user to download on request
                $handle_path = FCPATH . 'assets/uploads/';
                $uploadedcsv = $supp_id . '_matchedbatch.csv';
                $fp = fopen($handle_path . $uploadedcsv, 'w');
                foreach ($upload_resultpdf as $a) {
                    fputcsv($fp, $a);
                }
                fclose($fp);

                $upload_resultpdf_file = $uploadedcsv;
            }
        }

        if ($runtype==0) {
            $this->session->set_flashdata('error_message', NULL);
            // Print results
            /* <i class="fa fa-check-circle text-green" style="font-size:22px;float:right"></i> <i class="fa fa-times-circle text-red" style="font-size:22px;float:right"></i> <i class="fa fa-exclamation-circle text-orange" style="font-size:22px;float:right"></i> */
            if ($upload_resultpdf_file!='') {
                echo '<p class="pull-right"><h4 style="float:left" >Batch Results:</h4><a style="float:right" href="'.base_url("assets/uploads/" . $upload_resultpdf_file).'" class="btn btn-warning m-l-10">Export</a></p>';
            }

            echo '<table class="table table-bordered table-striped"> <thead><tr><th>Invoice</th><th>Remarks</th></tr></thead>'; 
            foreach ($upload_response as $printrow_inv => $printrow) { 
                $faicon = (!in_array($printrow_inv, $upload_success)) ? "fa fa-exclamation-circle text-orange" : "fa fa-check-circle text-green";
                echo '<tr><td>' . $printrow_inv . '</td><td>' . $printrow . ' <i class="'.$faicon.' " style="font-size:22px;float:right"></i> </td></tr>';
            }
            echo '</table>';
            
            /*
            echo '<table class="table table-bordered table-striped"> <thead><tr><th>Invoice</th><th>Remarks</th></tr></thead>';
            foreach ($upload_success as $printrow_inv) {
                echo '<tr><td>' . $printrow_inv . '</td><td>' . $upload_response[$printrow_inv] . ' <i class="fa fa-check-circle text-green" style="font-size:22px;float:right"></i> </td></tr>';
            }
            foreach ($upload_response as $printrow_inv => $printrow) {
                if (!in_array($printrow_inv, $upload_success))
                    echo '<tr><td>' . $printrow_inv . '</td><td>' . $printrow . ' <i class="fa fa-exclamation-circle text-orange" style="font-size:22px;float:right"></i> </td></tr>';
            }
            echo '</table>';
            */
        }
    }

    private function save_matching($matchtype, $uploads, $selected, $inv_num, $inv_date, $inv_amt, $tax_settings, $delfile) {

        $supp_id = $this->session->userdata('cm_supp_id');
        
        $processmessages=array();
        $grn_lists = array();
        $message='';
        
        // For logging:
        $processmessages[] = "Method - ".$matchtype;
        $processmessages[] = "Invoice - ".$inv_num." ($".$inv_amt.") / ".$inv_date;
        $processmessages[] = "Files to upload - ".count($uploads);
        $processmessages[] = "Items to match - ".count($selected);

        $checkdocref = $this->Invoice_model->get_pi_docref(trim($inv_num),trim($supp_id));
        $processmessages[] = (!$checkdocref) ? "Begin new invoice matching.." : "Duplicate invoice number ".$inv_num." in fin_journal";
        if ($checkdocref) {
            $message = 'Invoice number already matched to another PO.';
            $processmessages[] = $message;
            $this->save_matching_notify($inv_num, 'FAIL', $processmessages);
            $this->session->set_flashdata('error_message', $message);
            return -1;
        }

        // Upload the file to server
        $uploaded = 0;
        if (count($uploads)<=1) {
            if (count($uploads)==0) {
                $message = 'Missing supporting documents.';
                $processmessages[] = $message;
                $this->save_matching_notify($inv_num, 'FAIL', $processmessages);
                $this->session->set_flashdata('error_message', $message);
                return -2;
            }
            else if ( strtoupper(substr($uploads[0],-7)) =='INV.PDF' ) {
                $message = 'Missing supporting documents. Please upload with DO.';
                $processmessages[] = $message;
                $this->save_matching_notify($inv_num, 'FAIL', $processmessages);
                $this->session->set_flashdata('error_message', $message);
                return -3;
            }
            else if ( strtoupper(substr($uploads[0],-6)) == 'DO.PDF' ) {
                $message = 'Missing supporting documents. Please upload with INV.';
                $processmessages[] = $message;
                $this->save_matching_notify($inv_num, 'FAIL', $processmessages);
                $this->session->set_flashdata('error_message', $message);
                return -4;
            }
        }
        if (isset($uploads)) {
            foreach ($uploads as $upload) {
                $old_directory = FCPATH . '/' . 'assets/uploads/';
                $new_directory = PDF_SERVER . 'SUPP_INV/';
                $new_directory = '/mnt/cherpsfilewrite/pdf/SUPP_INV/';
                $new_directory_dateprefix = (strtotime(convert_to_mysql_time($inv_date))>=START_PREFIXDATE) ? date(SUPP_INV_PREFIXDATE,strtotime(convert_to_mysql_time($inv_date))) : '' ; // New filename convention after April

                if ($this->Invoice_model->copyfile($old_directory . $upload, $new_directory . $new_directory_dateprefix . $upload)) {
                    $processmessages[] = "File uploaded - " . $new_directory.$new_directory_dateprefix.$upload;
                    $uploaded++;
                } else {
                    $processmessages[] = "FAIL File uploaded - " . $upload;
                    $message = 'Fail to upload supporting documents. ';
                }
            }
        }

        // Delete DO file if copied over
        if ($delfile!=''){
            exec( "sudo rm ".$delfile. ' 2>&1', $output);
            $processmessages[] = "Delete file $delfile - " . json_encode($output);
        }

        // Call storeprocedures at CHERPS for processing, receive PI
        if (isset($uploaded) && $uploaded > 0) {
            $params_pi["coy_id"] = (isset($tax_settings["coy_id"]) && $tax_settings["coy_id"] != NULL) ? $tax_settings["coy_id"] : CTL_COY_ID;
            $params_pi["curr_id"] = (isset($tax_settings["curr_id"]) && $tax_settings["curr_id"] != NULL) ? $tax_settings["curr_id"] : "SGD";
            $params_pi["tax_id"] = (isset($tax_settings["tax_id"]) && $tax_settings["tax_id"] != NULL) ? $tax_settings["tax_id"] : "SG7";
            $params_pi["tax_percent"] = (isset($tax_settings["tax_percent"]) && $tax_settings["tax_percent"] != NULL) ? $tax_settings["tax_percent"] : "7";
            $params_pi["supp_id"] = $this->session->userdata('cm_supp_id');
            $params_pi["supp_name"] = $this->session->userdata('cm_supp_name');
            $params_pi["inv_num"] = $inv_num;
            $params_pi["inv_date"] = date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($inv_date)));
            $params_pi["inv_amt"] = $inv_amt;
            $pi_id = $this->Invoice_model->gen_pi($params_pi);
            $processmessages[] = "PI Stored Procedure returns - " . $pi_id;
            if ($pi_id == "") {
                $message = 'Fail to generate PI transaction number for invoice. ';
            }
        } else {
            $message = 'No supporting documents uploaded. ';
        }

        if (isset($uploaded) && $uploaded > 0 && isset($pi_id)) {
            // Update DB grn_item with PI and status=1
            $params_item["trans_id"] = $pi_id;
            $params_item["status_level"] = "1";
            $params_item["modified_by"] = $this->session->userdata('cm_user_id');
            $params_item["modified_on"] = date("Y-m-d H:i:s");
            foreach ($selected as $select) {
                if (!in_array($select[1], $grn_lists)) {
                    $grn_lists[] = $select[1];
                }
                $params_item["coy_id"] = $select[0];
                $params_item["grn_id"] = $select[1];
                $params_item["line_num"] = $select[2];
                $output = $this->Invoice_model->update_pms_grn_item($params_item);
                if ($output) {
                    $processmessages[] = "Item updated - " . $select[0] . '---' . $select[1] . '---' . $select[2];
                } else {
                    $processmessages[] = "FAIL Item update - " . $select[0] . '---' . $select[1] . '---' . $select[2];
                }
            }

            // Check whole GRN status, update grn_list
            foreach ($grn_lists as $grn_id) {
                // Query check for status 0
                $update_grnlist = $this->Invoice_model->get_pending_pms_grn_item($grn_id);
                if (!$update_grnlist) {
                    $params_grnli["grn_id"] = $grn_id;
                    $params_grnli["status_level"] = "1";
                    $params_grnli["modified_by"] = $this->session->userdata('cm_user_id');
                    $params_grnli["modified_on"] = date("Y-m-d H:i:s");
                    $this->Invoice_model->update_pending_pms_grn_item($params_grnli);
                    $processmessages[] = "Invoice header status update - " . $grn_id;
                }
            }

            // Additional check for PI in fin_journal. Issue warning only.
            $recp = '';
            $check = $this->Invoice_model->get_pi($pi_id);
            if (!$check) {
                $processmessages[] = "FAIL TO FIND PI " . $pi_id . " IN FINANCE JOURNAL!";
                $recp = 'joshua-woon@challenger.sg';
                $this->save_matching_notify($inv_num, 'WARNING', $processmessages, $recp);
            } else {
                $this->save_matching_notify($inv_num, 'OK', $processmessages, $recp);
            }

            return 1;
        } else {
            $this->save_matching_notify($inv_num, 'FAIL', $processmessages);
            $this->session->set_flashdata('error_message', $message);
            return -2;
        }
    }
    
    public function save_matching_notify($inv_num,$status,$processmessages,$recp=''){

        $supp_id = $this->session->userdata('cm_supp_id');

        $msg = "CHVOICES Invoice Matching\n";
        $msg.= "\nSupplier: " .  $supp_id;
        $msg.= "\nInvoice: " .  $inv_num;
        $msg.= "\nDate: " .  date("Y-m-d H:i:s");
        $msg.= "\n\nLog:";
        foreach ($processmessages as $m){
            $msg.= "\n".$m;
        }
        
        if (ENVIRONMENT=="production") {
            $subj = "CHVOICES Invoice Matching [".$status."] ".$supp_id."/".$inv_num."";
//            $sql = "exec sp_SysSendDBmail @from_address = 'chvoices@challenger.sg', @recipients = 'yongsheng@challenger.sg; ".$recp."', @copy_recipients = '', @subject = '".$subj."', @body = '".$msg."', @file_attachments = '' ";
//            $sendmail = CI::db()->query($sql);
            $params["to"] = 'yongsheng@challenger.sg; ' . $recp;
            $params["cc"] = '';
            $params["from"] = 'chvoices@challenger.sg';
            $params["subject"] = $subj;
            $params["message"] = $msg;
            $this->Invoice_model->CallSP_email($params);
        }
        else {
            $handle_path = FCPATH . 'assets/uploads/';
            $uploadedcsv = $supp_id . '_invlog_'.time().'.csv';
            
            file_put_contents($handle_path.$uploadedcsv, $msg, FILE_APPEND | LOCK_EX);
        }
    }
    
    public function testupload(){
            $upload = 'chvoicestestupload.txt';
            $handle_path = FCPATH . 'assets/uploads/';
            file_put_contents($handle_path.$upload, 'TEST UPLOAD FILE', FILE_APPEND | LOCK_EX);
            
            // Upload
            
            $old_directory = FCPATH . '/' . 'assets/uploads/'; 
            $new_directory =  PDF_SERVER . '';
            $new_directory = '/mnt/cherpsfilewrite/pdf/SUPP_INV/';
            
            if ( $this->Invoice_model->copyfile($old_directory.$upload, $new_directory.$upload) ){
                echo "File uploaded - ".$upload;
            }
            else {
                echo "FAIL File uploaded - ".$upload; 
                
                // If fail, try the script directly.
                echo '<hr>';
                exec( "sudo cp ".$old_directory.$upload." ".$new_directory.$upload . ' 2>&1', $output);
                print_r($output,true);

            }
            
    }

}
