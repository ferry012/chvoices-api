<section class="content clearfix" id="Invoice_submit_new_match_DIV">
    <?php //echo theme_view('general/search_bar_filter'); ?>
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger " >
                <div class="hidden box-header">
                    <h3 class="box-title">Invoice Details</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-md-5">
                        <div><p><span style="padding-top:3px"><b>Step 1: Upload invoice listings (.csv file).</b></span></p></div>
                        <div>
                            <?php if (is_array($step1_csv) && count($step1_csv)>0): ?>
                            <div class="form-group">
                                <input type="hidden" name="uploadedcsv_name" id="uploadedcsv_name" value="<?php echo $step1_csv["uploadedcsv_name"]; ?>" >
                                <div class="NFI-wrapper" style="border-color: #A5BCD2;overflow: auto; display: inline-block;">
                                    <div class="NFI-button" disabled="disabled" style="background-color: #E0E0E0;cursor:default; overflow: hidden; position: relative; display: block; float: left; white-space: nowrap; text-align: center; box-shadow:none;opacity:.65;cursor: not-allowed;">Uploaded file</div>
                                    <input type="text" readonly="readonly" class="NFI-filename" value="<?php echo $step1_csv["uploadedcsv_name"]; ?>" style="display: block; float: left; margin: 0px; padding: 0px 5px;">
                                </div>
                            </div>
                            <?php else: ?>
                            <form id="runsupporting" role="form" class="" action="<?php echo base_url("invoice/submit_batch"); ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="file" name="upload_file" id="upload_file" placeholder="Choose CSV" accept=".csv"> 
                                <button class="btn btn-default nfi-outerbtn" ><i class="fa fa-upload"></i> Upload File</button>
                            </div>
                            </form>
                            <div id="">
                                Download the template for Batch Invoice Matching <a href="<?php echo base_url("assets/chvoices-batchmatching-tpl.csv"); ?>">here</a>.
                                <br>(Make sure date is in DD/MM/YYYY and save as CSV format.)
                            </div>
                            <?php endif; ?> 
                        </div> 
                    </div>
                    <div class="col-xs-12 col-md-5" style="border-left:1px solid #EEE;"> 
                        <hr class="hidden visible-xs visible-sm">
                        <div><p><span style="padding-top:3px"><b>Step 2: Uploading supporting documents</b></span> <span id="upload_dir_header"></span></p></div>
                        <div>
                            <?php if (is_array($step1_csv) && count($step1_csv)>0): ?>
                            <div class="form-group">
                                <input type="file" name="upload_dir[]" id="upload_dir" placeholder="Choose files" webkitdirectory="" mozdirectory="" directory="" accept="application/pdf" multiple="true">
                                <input class="hidden" type="file" name="upload_batch" id="upload_batch" placeholder="Choose files" accept="application/pdf" multiple="true"> 
                                <a href="#" id="myBatchModalSp1_UploadBtn" class="btn btn-default nfi-outerbtn" ><i class="fa fa-upload"></i> Upload Files</a>
                            </div>
                            <div id="submit_new_save_batch_info">
                                <span id="upload_dir_txt">To upload multiple files, press Shift between files to select a range of files or click and drag your mouse over several files.</span><br><br>
                                Please make sure you are following the file naming guidelines:<br>
                                <table class="w-100" style="max-width:600px"><tr><td>INV File:</td><td>Follow the Invoice number followed by '-INV'</td><td>(e.g. A12345-INV.pdf)</td></tr>
                                    <tr><td>DO File:</td><td>Follow the Invoice number followed by '-DO'</td><td>(e.g. A12345-DO.pdf)</td></tr>
                                    <tr><td>Single File:</td><td>If Inv & DO are in 1 file, please use the Invoice number</td><td>(e.g. A12345.pdf)</td></tr></table><br><br>
                            </div>
                            <?php else: ?>
                            <div class="form-group">
                                <div class="NFI-wrapper" style="border-color: #A5BCD2;overflow: auto; display: inline-block;">
                                    <div class="NFI-button" disabled="disabled" style="background-color: #E0E0E0; cursor:default; overflow: hidden; position: relative; display: block; float: left; white-space: nowrap; text-align: center; box-shadow:none;opacity:.65;cursor: not-allowed;"><i class="fa fa-paperclip"></i> Choose files</div>
                                    <input type="text" readonly="readonly" class="NFI-filename" style="display: block; float: left; margin: 0px; padding: 0px 5px;">
                                </div> 
                            </div>
                            <?php endif; ?>
                        </div> 
                    </div>
                    <div class="col-xs-12 col-md-2" style="border-left:1px solid #EEE;">
                        <hr class="hidden visible-xs visible-sm">
                        <div><p><span style="padding-top:3px"><b>Step 3: Submit matching</b></span></p></div>
                        <div class="m-t-10">
                            <input type="button" class="hidden" id="sel_all_btn" >
                            <button id="myBatchModalSender" type="submit" class="btn btn-danger" disabled ><i class="fa fa-send"></i> Submit</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            
            
            <div class="box"> 
                <div class="box-body" id="myBatchModal">
                    <div id="submit_new_save_batch_div">
                        <div id="myBatchModalFiles" class="col-xs-8" style="margin:0px 0px 20px 0px"></div>
                        <table id="datatable8" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>PO ID</th> 
                                    <th>Invoice ID</th>
                                    <th>Invoice Date</th>
                                    <th>Invoice Amount</th>
                                    <th>Supporting Documents</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr></tr>
                            </tbody>
                        </table> 
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<?php if (is_array($step1_csv) && count($step1_csv)>0): ?>
<script>
    var myBatchCSV_prefix = '<?php echo $this->session->userdata('cm_supp_id').'_' ?>'; 
    var myBatchCSV = <?php echo json_encode($step1_csv["upload_results"]); ?>; 
</script>
<?php endif; ?>