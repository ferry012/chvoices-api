<?php $maxlen = 0; ?>

<table id="datatable" class="table table-bordered table-striped">
    <thead>
        <tr>
            <?php if ($readonly == 0): ?>
                <th class="no-sort"><span class="fa fa-check-square-o" id="sel_all" title="Select all on this page"></span></th>
            <?php endif; ?>
            <th>PO ID</th> 
            <th>Item ID</th>
            <th class="h-mobile">Item Description</th>
            <th>GRN Qty</th>
            <th>Unit Price</th>
            <th class="h-mobile no-sort">Disc %</th>
            <th class="h-mobile">Amount</th>
            <th class="h-mobile no-sort">Tax Amt</th>
            <th class="h-mobile no-sort">Supp DO<br>Recv. Date</th>
            <th class="h-mobile no-sort">Tax<br>Curr</th>  
        </tr>
    </thead>
    <tbody>
        <?php
        $k = 0;
        if ($results):
            foreach ($results as $result):
                $maxlen = (strlen($result["po_id"]) > strlen($maxlen)) ? strlen($result["po_id"]) : $maxlen;
                ?>
                <tr>
                    <?php if ($readonly == 0): ?>
                        <td><input type="checkbox" class="cb" id="<?php echo 'cb_' . $k; ?>"
                                   name="inv_selected[]" value="<?php echo $result["coy_id"] . '---' . $result["grn_id"] . '---' . $result["line_num"]; ?>" <?php if (isset($result["checked"])) echo 'checked'; ?>
                                   data-po="<?php echo urlencode(trim($result["po_id"])); ?>" data-do="<?php echo urlencode(trim($result["supp_do"])); ?>"></td>
                    <?php endif; ?>
                    <td>
                        <?php echo $result["po_id"]; ?>
                        <small><br><?php echo $result["status_desc"]; ?></small>
                    </td>
                    <td>
                        <?php echo $result["item_id"]; ?>
                        <span class="hidden visible-xs visible-sm"><small><?php echo $result["item_desc"]; ?></small></span>
                    </td> 
                    <td class="h-mobile">
                        <?php echo $result["item_desc"]; ?>
                    </td> 
                    <td>
                        <?php echo display_number($result["grn_qty"]); ?>
                        <input type="hidden" id="<?php echo 'grnqty_' . $k; ?>" value="<?php echo price_no_symbol($result["grn_qty"]); ?>">
                    </td>
                    <td><?php echo price_no_symbol($result["unit_price"]); ?></td>
                    <td class="h-mobile"><?php echo display_number($result["disc_percent"]); ?></td>
                    <td class="h-mobile">
                        <?php echo price_no_symbol($result["amount"]); ?>
                        <input type="hidden" id="<?php echo 'amt_' . $k; ?>" value="<?php echo ($result["amount"]); ?>">
                    </td>
                    <td class="h-mobile">
                        <?php echo price_no_symbol($result["tax_amt"]); ?>
                        <input type="hidden" id="<?php echo 'tax_' . $k; ?>" value="<?php echo ($result["tax_amt"]); ?>">
                    </td>
                    <td class="h-mobile">
                        <?php echo $result["supp_do"]; ?>
                        <small><br><?php echo display_date_format($result["date_received"]); ?></small>
                    </td> 
                    <td class="h-mobile">
                        <?php echo display_number($result["tax_percent"]); ?>%<br>
                        <?php echo $result["curr_id"]; ?>
                    </td> 
                </tr>
                <?php
                $k++;
            endforeach;
        endif;
        ?>
    </tbody>
    <tfoot>
        <tr>
            <?php if ($readonly == 0): ?>
                <td class="datatransparent"></td>
            <?php endif; ?>
            <td class="datatransparent h-mobile" style="color:#FFF"><?php
                $maxlen+=3;
                for ($i = 0; $i < $maxlen; $i++)
                    echo '_';
                ?></td>
            <td class="datatransparent"></td>
            <td class="datatransparent h-mobile"></td>
            <td class="datatransparent text-center" colspan="5">
                <span id="inv_qty_tally_cp"></span> &nbsp; 
                <span id="inv_amt_tally_cp"></span>
            </td>
            <td class="datatransparent h-mobile"></td>
            <td class="datatransparent h-mobile"></td>
        </tr>
    </tfoot>
</table>
<div id="ItembtnSpanDiv" class="m-t-20">
    <?php if (isset($exportcsv) && $exportcsv != NULL): ?>
        <a style="float:right" href="<?php echo base_url("assets/uploads/" . $exportcsv); ?>" class="btn btn-primary m-l-10"><i class="fa fa-arrow-up"></i> Export</a> &nbsp;
        <a style="display:none;float:right" class="btn btn-primary m-l-10" data-toggle="modal" data-target="#myImpMatchModal">Import Matched GRN</a>
    <?php endif; ?>
</div>
<script>
$('.cb').click(function () {$("#inv_amt_tally").trigger("click");});
$('#sel_all').click(function () {$("#sel_all_btn").trigger("click");});
$('#ItembtnSpan').html($('#ItembtnSpanDiv').html());
</script>