<section class="clearfix content" id="Invoice_tax_update_DIV">
<br>
<form id="myUpdateTaxSender_SepFm" role="form" action="<?php echo base_url("invoice/update_tax"); ?>" method="post" enctype="multipart/form-data">
    <p style="color: black;">
        <select name="tax" class="form-control input-sm" style="display: inline-block; width:30%; margin-left: 15px;">
            <option value="">Select Tax</option>
            <?php
            foreach ($tax_list as $value) {
                echo '<option value="' . $value['tax_id'] . '---' . $value['tax_percent'] . '">' . $value['tax_desc'] . ' (' . $value['tax_id'] . ', ' . $value['tax_percent'] . ' %)' . '</option>';
            }
            ?>
        </select>
        <button id="update_tax_btn" name="submit" type="submit" class="btn">Submit</button>
        <!-- <span style="margin-left:15px;">
        <a href="#" type="submit" id="myUpdateTaxBtn" class="btn">Update PO Tax</a>
    </span> -->
        <hr>

    </p>
    <?php

    if (isset($_POST['submit'])) {
        $url = base_url("invoice/update_tax");

        if (empty($_POST['invoice_selected']) && $_POST['tax'] == "") {
            echo '<div role="alert" class="alert alert-warning">Select tax option and check invoices before proceeding.</div>';
            header("Refresh: 3; URL=" . $url);
        } else if ($_POST['tax'] == "") {
            echo '<div role="alert" class="alert alert-warning">No tax option selected. Select one before proceeding.</div>';
            header("Refresh: 3; URL=" . $url);
        } else if (empty($_POST['invoice_selected'])) {
            echo '<div role="alert" class="alert alert-warning">No invoice(s) selected for tax update.</div>';
            header("Refresh: 3; URL=" . $url);
        } else {
            if ($error_counter == 0) {
                echo '<div role="alert" class="alert alert-info">Tax update ('
                    . $inv_tax_id . ', ' . $inv_tax_percent . ' %) successful for '
                    . count($_POST['invoice_selected']) . ' invoice(s) with PO id: <br>';

                foreach ($_POST['invoice_selected'] as $selected_invoice) {
                    echo $selected_invoice . '<br>';
                }

                echo '</div>';
            } else {
                echo '<div role="alert" class="alert alert-danger">Update unsuccessful.</div>';
            }
        }
    }

    ?>

    <div class="box-body" id="datatable_div">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    
                    <th class="no-sort"><span class="fa fa-check-square-o" id="sel_all" title="Select all on this page"></span></th>
                    
                    <th>PO ID</th>
                    <th>PO Date</th>
                    <th class="h-mobile">Loc ID</th>
                    <th class="h-mobile no-sort">Supp ID</th>
                    <th class="h-mobile no-sort">Supp Name</th>
                    <th class="no-sort">Tax ID</th>
                    <th class="no-sort">Tax Percent</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $k = 0;
                if ($results) :
                    foreach ($results as $result) :
                        $maxlen = (strlen($result["po_id"]) > strlen($maxlen)) ? strlen($result["po_id"]) : $maxlen;
                ?>
                        <tr>
                            <?php if ($readonly == 0) : ?>
                                <td><input type="checkbox" class="cb" id="<?php echo 'cb_' . $k; ?>" name="invoice_selected[]" value="<?php echo $result["po_id"]; ?>" <?php if (isset($result["checked"])) echo 'checked'; ?> data-po="<?php echo urlencode(trim($result["po_id"])); ?>"></td>
                            <?php endif; ?>
                            <td>
                                <?php echo $result["po_id"]; ?>
                                <small><br><?php echo $result["status_desc"]; ?></small>
                            </td>
                            <td>
                                <?php echo $result["po_date"]; ?>
                                
                            </td>
                            <td class="h-mobile">
                                <?php echo $result["loc_id"]; ?>
                            </td>
                            <td class="h-mobile">
                                <?php echo $result["supp_id"]; ?>
                                <span class="hidden visible-xs visible-sm"><small><?php echo $result["supp_name"]; ?></small></span>
                            </td>
                            <td class="h-mobile">
                                <?php echo $result["supp_name"]; ?>
                            </td>
                            <td>
                                <?php echo $result["tax_id"]; ?>
                            </td>
                            <td>
                                <?php echo display_number($result["tax_percent"]); ?>%<br>
                                <?php echo $result["curr_id"]; ?>
                            </td>
                        </tr>
                <?php
                        $k++;
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
    </div>
</form>
</section>

