<!-- Main content -->
<section class="clearfix content" id="Invoice_submit_new_match_DIV">
    <div class="row">
        <div class="col-xs-12">
            <!-- form start -->
            <form role="form" id="Invoice_submit_new_match" 
                  action="<?php echo base_url("invoice/submit_new_save"); ?>" method="post" enctype="multipart/form-data">
                <div class="box box-danger " >
                    <div class="hidden box-header">
                        <h3 class="box-title">Invoice Details</h3> 
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12 col-md-6">
                            <div><p><span style="padding-top:3px"><b>Step 1: Enter invoice details</b></span></p></div>
                            <div>
                                <div class="col-xs-4">
                                    <div class="inner">
                                        <p class="submit_new_match-step1-label no-padding no-margin">Invoice No.</p>
                                        <input type="text" class="form-control" name="inv_no" id="inv_no" value="<?php echo $inv_no; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="inner">
                                        <p class="submit_new_match-step1-label no-padding no-margin">Inv. Date</p>
                                        <div class="input-group date datepicker" style="margin:0px;padding:0px"> 
                                            <input type="text" class="form-control" name="inv_date" id="inv_date" value="<?php echo date('d-m-Y'); ?>" required="" > 
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="inner">
                                        <p class="submit_new_match-step1-label no-padding no-margin">Inv. Amount</p> 
                                        <input type="text" class="form-control text-right" name="inv_amt" id="inv_amt" value="">
                                        <input type="text" class="form-control hidden" id="inv_amt_tally" value="<?php echo price_no_symbol(0); ?>" readonly="">
                                        <input type="text" class="form-control hidden" id="inv_amt_diff" value="<?php echo price_no_symbol(0); ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4" style="border-left:1px solid #EEE;"> 
                            <hr class="hidden visible-xs visible-sm">
                            <div><p><span style="padding-top:3px"><b>Step 3: Uploading supporting documents</b></span></p></div>
                            <div class="m-t-30">
                                <div style="float:left">
                                    <div id="myUploadFiles"></div> 
                                </div>
                                <div style="float:left;margin-left:15px">
                                    <a href="#" id="myUploadModalBtn" <?php /*data-toggle="modal" data-target="#myUploadModal"*/ ?> class="btn btn-default"><i class="fa fa-upload"></i> Upload File</a>
                                </div>
                            </div> 
                        </div>
                        <div class="col-xs-12 col-md-2" style="border-left:1px solid #EEE;">
                            <hr class="hidden visible-xs visible-sm">
                            <div><p><span style="padding-top:3px"><b>Step 4: Submit matching</b></span></p></div>
                            <div class="m-t-30">
                                <input type="button" class="hidden" id="sel_all_btn" >
                                <button id="submit_inv_btn" type="submit" class="btn btn-danger"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="hidden box-header">
                        <h3 class="box-title ">GRN Items</h3> 
                    </div>
                    <div style="padding:10px 10px 0px 15px"><p><span><b>Step 2: Select PO/GRN items</b></span></p></div>
                    <!-- /.box-header --> 
                    <span id="ItembtnSpan"></span>
                    
                    <div class="box-body" id="datatable_div">
                        <table id="datatable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="no-sort"><span class="fa fa-check-square-o" id="sel_all" title="Select all on this page"></span></th>
                                    <th>PO ID</th> 
                                    <th>Item ID</th>
                                    <th>Item Description</th>
                                    <th>GRN Qty</th>
                                    <th>Unit Price</th>
                                    <th class="h-mobile no-sort">Disc %</th>
                                    <th class="h-mobile">Amount</th>
                                    <th class="h-mobile no-sort">Tax Amt</th>
                                    <th class="h-mobile no-sort">Supp DO<br>Recv. Date</th>
                                    <th class="h-mobile no-sort">Tax<br>Curr</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $k = 0;
                                if ($results):
                                    foreach ($results as $result):
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="cb" id="<?php echo 'cb_' . $k; ?>" name="inv_selected[]" value="<?php echo $result["coy_id"] . '---' . $result["grn_id"] . '---' . $result["line_num"]; ?>" data-po="<?php echo trim($result["po_id"]); ?>"></td>
                                            <td>
                                                <?php echo $result["po_id"]; ?>
                                                <small><br><?php echo $result["status_desc"]; ?></small>
                                            </td>
                                            <td>
                                                <?php echo $result["item_id"]; ?>
                                            </td> 
                                            <td>
                                                <?php echo $result["item_desc"]; ?>
                                            </td> 
                                            <td><?php echo display_number($result["grn_qty"]); ?></td>
                                            <td><?php echo price_no_symbol($result["unit_price"]); ?></td>
                                            <td class="h-mobile"><?php echo display_number($result["disc_percent"]); ?></td>
                                            <td>
                                                <?php echo price_no_symbol($result["amount"]); ?>
                                                <input type="hidden" id="<?php echo 'amt_' . $k; ?>" value="<?php echo price_no_symbol($result["amount"]); ?>">
                                            </td>
                                            <td class="h-mobile">
                                                <?php echo price_no_symbol($result["tax_amt"]); ?>
                                                <input type="hidden" id="<?php echo 'tax_' . $k; ?>" value="<?php echo price_no_symbol($result["tax_amt"]); ?>">
                                            </td>
                                            <td class="h-mobile">
                                                <?php echo $result["supp_do"]; ?>
                                                <small><br><?php echo display_date_format($result["date_received"]); ?></small>
                                            </td> 
                                            <td class="h-mobile">
                                                <?php echo display_number($result["tax_percent"]); ?>%<br>
                                                <?php echo $result["curr_id"]; ?>
                                            </td> 
                                        </tr>
                                        <?php
                                        $k++;
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </form>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<!-- Hidden section start -->
<div id="hidden_div" style="display:none">

    <span id="GRNitemstable_search">
        <input id="GRNitemstable_search_po" placeholder="Search PO" class="form-control input-sm">
        <input id="GRNitemstable_search_do" placeholder="Search DO" class="form-control input-sm">
    </span>

</div>

<div class="modal fade" id="myUploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="box-shadow:none">
                    <ul class="nav nav-tabs">
                        <li style="background-color: #EEE; padding:5px 15px" class="active"><h4><a href="#tab_1" data-toggle="tab">Separate INV & DO</a></h4></li>
                        <li style="background-color: #EEE; padding:5px 15px"><h4><a href="#tab_2" data-toggle="tab">Combined INV & DO</a></h4></li>
                        <li class="pull-right">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </li>
                    </ul>
                    <div class="tab-content" style="padding:10px 0px 0px 0px;">
                        <div class="tab-pane clearfix active" id="tab_1">
                            <p>Browse for your Invoice and DO files in the respective fields below to continue.</p>
                            <form id="myUploadModalSender_SepFm" role="form" action="<?php echo base_url("invoice/submit_new_match"); ?>" method="post" enctype="multipart/form-data">
                                <div class="box-body"> 
                                    <div class="form-group">
                                        <input type="file" name="upload_inv" id="upload_inv" placeholder="Choose INV" accept="application/pdf"> 
                                    </div>
                                    <div id="upload_do_div" class="form-group">
                                        <input type="file" name="upload_do" id="upload_do" placeholder="Choose DO" accept="application/pdf">
                                    </div>
                                    <div id="copy_do" style="border:dotted #369 1px;max-width:360px;min-height:38px;padding:4px;clear:both"></div>
                                    <input type="hidden" id="copy_do_input">
                                </div> 
                            </form>
                            <p>Note: Previous uploaded files will be replaced.</p>
                            
                            <div class="pull-right clearfix">
                                <button type="button" id="myUploadModalCloser" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" id="myUploadModalSender_Sep" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                            </div>
                            
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane clearfix" id="tab_2">
                            <p>Browse for your PDF files in the field below to continue.</p>
                            <form id="myUploadModalSender_ComFm" role="form" action="<?php echo base_url("invoice/submit_new_match"); ?>" method="post" enctype="multipart/form-data">
                                <div class="box-body"> 
                                    <div class="form-group">
                                        <input type="file" name="upload_inv_do" id="upload_inv_do" placeholder="Choose PDF" accept="application/pdf"> 
                                    </div>
                                </div> 
                            </form>
                            <p>Note: Previous uploaded files will be replaced.</p>
                            
                            <div class="pull-right clearfix">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" id="myUploadModalSender_Com" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                            </div>
                            
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->

            </div> 
        </div>
    </div>
</div>

<div class="modal fade" id="myBatchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content"> 
            <div class="modal-body">

                <!-- Custom Tabs -->
                <div class="nav-tabs-custom" style="box-shadow:none">
                    <ul class="nav nav-tabs">
                        <li style="background-color: #EEE; padding:5px 15px" class="active"><h4><a href="#tab2_1" data-toggle="tab" id="myBatchModalSp1">Supporting Documents</a></h4></li>
                        <li style="background-color: #EEE; padding:5px 15px"><h4><a href="#tab2_2" data-toggle="tab" id="myBatchModalSp2">Batch File (CSV)</a></h4></li>
                        <li class="pull-right">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </li>
                    </ul>
                    <div class="tab-content" style="padding:10px 0px 0px 0px;">
                        <div class="tab-pane clearfix active" id="tab2_1">
                            <form role="form" id="Invoice_submit_batch1" 
                                  action="<?php echo base_url("invoice/submit_new_save_batch_upload"); ?>" method="post" enctype="multipart/form-data">
                                <div class="box-body"> 
                                    <p>Upload supporting documents (INV & DO). Press shift to select a range of files.</p>
                                    <input type="hidden" name="uploadedcsv_name" id="uploadedcsv_name" value="" >
                                    <div class="form-group">
                                        <input type="file" name="upload_batch" id="upload_batch" placeholder="Choose files" accept="application/pdf" multiple="true"> 
                                        <a href="#" id="myBatchModalSp1_UploadBtn" class="btn btn-default" style="margin-top:-32px;margin-left:5px">Upload Files</a>
                                    </div>
                                    <div id="submit_new_save_batch_info">
                                        To upload multiple files, press Shift between files to select a range of files or click and drag your mouse over several files.<br><br>
                                        Please make sure you are following the file naming guidelines:<br>
                                        <table class="w-100"><tr><td>INV File:</td><td>Follow the Invoice number followed by '-INV'</td><td>(e.g. A12345-INV.pdf)</td></tr>
                                            <tr><td>DO File:</td><td>Follow the Invoice number followed by '-DO'</td><td>(e.g. A12345-DO.pdf)</td></tr>
                                            <tr><td>Single File:</td><td>If Inv & DO are in 1 file, please use the Invoice number</td><td>(e.g. A12345.pdf)</td></tr></table><br><br>
                                    </div>
                                    <div id="myBatchModalFiles"></div>
                                </div>
                            </form>

                            <div class="pull-right clearfix">
                                <button type="button" id="" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" id="myBatchModalSp2Btn" class="btn btn-primary">Next Step</button>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane clearfix" id="tab2_2"> 
                            <form role="form" id="Invoice_submit_batch2" 
                                  action="<?php echo base_url("invoice/submit_new_save_batch"); ?>" method="post" enctype="multipart/form-data">
                                <div class="box-body"> 
                                    <p>Upload invoice listings (.csv file).</p>
                                    <div class="form-group">
                                        <input type="file" name="upload_batch_csv" id="upload_batch_csv" placeholder="Choose CSV"  accept=".csv"> 
                                    </div>
                                    <div id="submit_new_save_batch_div"></div>
                                </div> 
                            </form>
                            
                            <div id="Invoice_submit_batch2_CloseDiv" class="pull-right clearfix">
                                <button type="button" id="myBatchModalSp1Btn" class="btn btn-default" >Back</button>
                                <button type="button" id="myBatchModalSender" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                            </div>
                            
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
                
            </div> 
        </div>
    </div>
</div>

<div class="modal fade" id="myImpMatchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Import Matched GRN Items</h4>
            </div>
            <div class="modal-body">
                <p>Upload the CSV file for batch invoice matching.</p>
                <form role="form" id="Invoice_submit_match" 
                      action="<?php echo base_url("invoice/get_grn_items"); ?>" method="post" enctype="multipart/form-data">
                    <div class="box-body"> 
                        <div class="form-group">
                            <input type="file" name="upload_match" id="upload_match" placeholder="Choose CSV" accept=".csv"> 
                        </div>
                        <div id="submit_new_save_batch_div">
                            Please make sure you are using the latest file exported from this page.
                        </div>
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="myMatchModalCloser" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="myMatchModalSender" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
            </div>
        </div>
    </div>
</div>

<script>
    var LOADCOPYDO = <?php echo ( isset($loadCopydo) && $loadCopydo==1 ) ? 1 : 0 ; ?>;
</script>