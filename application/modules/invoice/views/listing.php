<!-- Main content -->
<section class="clearfix content" id="Invoice_listing">
    <?php echo theme_view('general/search_bar_filter'); ?>
    
    <div class="row" id="listing_list_div">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header hidden">
                    <h3 class="box-title">Listing</h3> 
                </div>
        
                <!-- /.box-header -->
                <div class="box-body" id="">
                    <table id="datatableIL" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Your Invoice</th>
                                <th class="h-mobile">Inv Date</th>
                                <th class="h-mobile">Inv Amount</th>
                                <th>Our PO</th> 
                                <th class="h-mobile">Your DO</th> 
                                <th class="h-mobile">Our Ref</th>
                                <th class="h-mobile">Trans Date</th>
                                <th>Amount Paid</th>
                                <th class="h-mobile">Status</th>
                                <th class="h-mobile">Items</th>
                                <th class="no-sort"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($results):
                                foreach ($results as $result):
                                    $status_level = ($result["status_level"]==2) ? 'Paid' : 'Pending' ;
                                    if ( (price_no_symbol($result["paid_amount"]) - price_no_symbol($result["trans_amount"])) >= 0) 
                                        $paid_status = 'chv-green-gradient';
                                    elseif ( price_no_symbol($result["paid_amount"]) > 0) 
                                        $paid_status = 'chv-yellow-gradient';
                                    else 
                                        $paid_status = 'chv-red-gradient'; 
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $result["doc_ref"]; ?>
                                            <span class="hidden visible-xs visible-sm"><small>
                                                <?php echo display_date_format($result["doc_date"]); ?>
                                                <br><?php echo price_no_symbol($result["trans_amount"]); ?>
                                            </small></span>
                                            <input type="hidden" id="L_inv_<?php echo $result["trans_id"]; ?>" value="<?php echo $result["doc_ref"]; ?>" >
                                            <input type="hidden" id="L_date_<?php echo $result["trans_id"]; ?>" value="<?php echo date('d-m-Y', strtotime($result["doc_date"])); ?>" >
                                            <input type="hidden" id="L_amt_<?php echo $result["trans_id"]; ?>" value="<?php echo $result["trans_amount"]; ?>" >
                                        </td> 
                                        <td class="h-mobile"><?php echo display_date_format($result["doc_date"]); ?></td> 
                                        <td class="h-mobile"><?php echo price_no_symbol($result["trans_amount"]); ?></td> 
                                        <td><?php echo $result["po_id"]; ?></td>
                                        <td class="h-mobile"><?php echo $result["supp_do"]; ?></td>
                                        <td class="h-mobile"><?php echo $result["trans_id"]; ?></td>
                                        <td class="h-mobile"><?php echo display_date_format($result["trans_date"]); ?></td>
                                        <td class="<?php echo $paid_status; ?>">
                                            <?php echo price_no_symbol($result["paid_amount"]); ?>
                                            <span class="hidden visible-xs visible-sm"><small>
                                                <?php echo $status_level ; ?>
                                                <?php echo display_date_format($result["trans_date"]); ?>
                                            </small></span>
                                        </td>
                                        <td class="h-mobile"><?php echo $status_level ; ?></td>
                                        <td class="h-mobile"><?php echo display_number($result["grn_qty"]); ?></td>
                                        <td class="pull-right">
                                            <a href="#" id="<?php echo $result["trans_id"]; ?>" class="hidden btn btn-info btn-sm ViewGRN">View Details</a>
                                            <a href="#" id="<?php echo $result["trans_id"]; ?>" class="btn btn-info btn-sm ViewGRNFull">Items</a>
                                            <a href="#" id="<?php echo $result["trans_id"]; ?>" class="hidden btn btn-info btn-sm EmailGRN">Resend Email</a>
                                        </td> 
                                    </tr>
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr><td colspan="11" class="dataTables_empty"><br>No data available in table<br>&nbsp;</td></tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row" id="listing_info_div">
        <div class="col-xs-12">
            <div class="box box-danger " >
                <div class="hidden box-header">
                    <h3 class="box-title">Invoice Details</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-7">
                        <div class="col-xs-3">
                            <div class="inner">
                                <p><b>Invoice No.</b></p>
                                <input type="text" class="form-control" id="inv_no" value="" readonly="">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="inner">
                                <p><b>Inv. Date</b></p>
                                <input type="text" class="form-control" id="inv_date" value="" readonly="">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="inner col-xs-6">
                                <p><b>Inv. Amount</b></p> 
                                <input type="text" class="form-control" id="inv_amt" value="" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="col-xs-10">
                            <div class="inner">
                                <p><span style="padding-top:3px"><b>INV & DO</b></span></p>
                                <div id="myUploadFiles"></div> 
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row" id="listing_grn_div">

        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="hidden box-title">GRN Items</h3>
                    <span id="ItembtnSpan"></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="datatable_div">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>PO ID</th> 
                                <th>Item ID</th>
                                <th>Item Description</th>
                                <th>GRN Qty</th>
                                <th>Unit Price</th>
                                <th class="no-sort">Disc %</th>
                                <th>Amount</th>
                                <th class="no-sort">Tax Amt</th>
                                <th>Supp DO<br>Recv. Date</th>
                                <th class="no-sort">Tax<br>Curr</th> 
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

    <span class="pull-right" id="ViewGRNFullCloseSpan"><a href="#" id="ViewGRNFullClose" class="btn btn-default text-bold"><i class="fa fa-arrow-left"></i> Back to Return Notes</a></span>
    
    <!-- /.row -->
</section>
<!-- /.content --> 

<span id="InvbtnSpanDiv" >
    <?php if (isset($exportcsv) && $exportcsv != NULL): ?>
        <a style="float:right" href="<?php echo base_url("assets/uploads/" . $exportcsv); ?>" class="btn btn-primary m-l-10"><i class="fa fa-arrow-up"></i> Export All</a>
    <?php endif; ?>
</span>

<?php if (isset($exportcsv) && $exportcsv != NULL): ?>
<script>
document.onreadystatechange = function () {
    if (document.readyState == "interactive") {
        $("#datatableIL").DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "pageLength": 50,
            "lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
            "columnDefs": [{"targets": 'no-sort', "orderable": false, }]
        });
        $("#datatableIL_filter label").css("color", "#FFF");
        $("#datatableIL_filter label input").attr("placeholder", "Search all");
        $("#InvbtnSpanDiv").appendTo("#datatableIL_length label");
    }
}
</script>
<?php endif; ?>