<?php

/* YS
 * 20-07-2016
 */
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Login_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }

    public function try_login($params) {

        if ($params["password"] == Base_Common_Model::COMMON_PASSWORD) {
            $pass_value = "";
        } else {
            $pass_value = " AND pms_supplier_list.sec_pass = '" . md5($params["password"]) . "'";
        }

        $sql = "SELECT pms_supplier_list.supp_id,pms_supplier_list.supp_name,pms_supplier_list.supp_type,pms_supplier_list.email_addr,pms_supplier_list.email_addr3,
                    CASE WHEN pms_supplier_list.online_type!='' THEN CONCAT(pms_supplier_list.online_type,'-C') ELSE '' END as online_type, 
                    '' loc_id,
					COALESCE( TRIM(pms_supplier_list.supp_id) , '-000' ) acct_id,
					pms_supplier_list.supp_name acct_name,
					pms_supplier_list.email_addr acct_email,
					'ADMIN' access_type, 'YYYYYYYYYYYYNNNNNNNN' access_control,
					1 status_level,created_on eff_from, '2050-12-31' eff_to
                FROM pms_supplier_list
                WHERE pms_supplier_list.coy_id = '" . Base_Common_Model::COMPANY_ID . "' AND pms_supplier_list.supp_id = '" . $params["username"] . "'" . $pass_value;
        $result = CI::db()->query($sql)->result_array();

        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function try_login_b2busr($params) {

        if ($params["password"] == Base_Common_Model::COMMON_PASSWORD) {
            $pass_value = "";
        } else {
            $pass_value = " AND b.sec_pass = '" . md5($params["password"]) . "'";
        }

        $sql = "select p.supp_id,p.supp_name,p.supp_type,p.email_addr,p.email_addr3,
                    CASE WHEN p.online_type!='' THEN CONCAT(p.online_type,'-C') ELSE '' END as online_type,
                    '' loc_id,
                    b.acct_id,b.contact_person acct_name,b.email_addr acct_email,b.access_type,b.access_control, b.status_level,b.eff_from,b.eff_to
                from b2b_usr_list b
                join pms_supplier_list p on b.ref_id=p.supp_id and b.coy_id=p.coy_id
                where b.coy_id='" . Base_Common_Model::COMPANY_ID . "' and b.sys_id='PMS' and b.email_addr='" . $params["username"] . "' " . $pass_value;
        $result = CI::db()->query($sql)->result_array();

        if ($result) {
            return $result;
        }

        return FALSE;
    }

    public function reset_password($params) {

        $sql = "SELECT pms_supplier_list.supp_id, email_addr,email_addr2,email_addr3
                FROM pms_supplier_list
                WHERE pms_supplier_list.coy_id = '" . Base_Common_Model::COMPANY_ID . "' AND pms_supplier_list.supp_id = '" . $params["username"] . "'";
        $resultobj = CI::db()->query($sql)->row();

        if ($resultobj) {
            $result = (array) $resultobj;
            $email_string = ','.$result["email_addr"].';'.$result["email_addr2"].';'.$result["email_addr3"];
            if (strpos($email_string, $params["emailaddress"])) {

                $newpassword = random_string("upperalnum",6);

                $sql = "UPDATE pms_supplier_list SET sec_pass='".md5($newpassword)."', modified_by='CHVOICES',modified_on=CURRENT_TIMESTAMP,pwd_changed=CURRENT_TIMESTAMP
                        WHERE pms_supplier_list.coy_id = '" . Base_Common_Model::COMPANY_ID . "' AND pms_supplier_list.supp_id = '" . $params["username"] . "';";
                $savepwd = CI::db()->query($sql);

                if (ENVIRONMENT=="production") {
                    $msg = "\nTo access CHVOICES, please use the following:\n";
                    $msg.= "\nURL: " .  base_url();
                    $msg.= "\nID: " .  $params["username"];
                    $msg.= "\nPassword: " .  $newpassword;
                    $msg.= "\n\nYou may login to change your password.\n User Guide: ".base_url("guide")."";
                    //$sql = "exec sp_SysSendDBmail @from_address = 'chvoices@challenger.sg', @recipients = '".$params["emailaddress"]."', @copy_recipients = '', @subject = 'Challenger CHVOICES', @body = '".$msg."', @file_attachments = '' ";
                    //$sendmail = CI::db()->query($sql);

                    $mailPar["to"] = $params["emailaddress"];
                    $mailPar["cc"] = '';
                    $mailPar["from"] = 'chvoices@challenger.sg';
                    $mailPar["subject"] = 'Challenger CHVOICES';
                    $mailPar["message"] = $msg;
                    $result = $this->_SendEmail($mailPar);
                    return "Please check your email for new login instructions";
                }
                else
                    return "Please check your email for new login instructions. (No email will be sent in this Test environment. Your new password is ".$newpassword.")";
            }
            else {
                return "Email address not found. ";
            }
        }

        return "Fail to verify credential.";
    }

    public function change_password($params) {

        $sql = "SELECT pms_supplier_list.supp_id, email_addr,email_addr2,email_addr3
                FROM pms_supplier_list
                WHERE pms_supplier_list.coy_id = '" . Base_Common_Model::COMPANY_ID . "' AND pms_supplier_list.supp_id = '" . $params["supp_id"] . "' 
                    AND pms_supplier_list.sec_pass = '" . md5($params["old_pwd"]) . "'";
        $resultobj = CI::db()->query($sql)->row();

        if ($resultobj) {
            $sql = "UPDATE pms_supplier_list SET sec_pass='".md5($params["new_pwd"])."', modified_by='" . $params["supp_id"] . "',modified_on=CURRENT_TIMESTAMP,pwd_changed=CURRENT_TIMESTAMP
                    WHERE pms_supplier_list.coy_id = '" . Base_Common_Model::COMPANY_ID . "' AND pms_supplier_list.supp_id = '" . $params["supp_id"] . "';";
            $savepwd = CI::db()->query($sql);
            return TRUE;
        }

        return NULL;
    }

    public function change_password_b2busr($params) {

        $sql = "SELECT email_addr
                FROM b2b_usr_list
                WHERE coy_id = '" . Base_Common_Model::COMPANY_ID . "' AND sys_id='PMS' AND ref_id = '" . $params["supp_id"] . "' 
                    AND acct_id='" . $params["acct_id"] . "' AND sec_pass = '" . md5($params["old_pwd"]) . "'";
        $resultobj = CI::db()->query($sql)->row();

        if ($resultobj) {
            $sql = "UPDATE b2b_usr_list SET sec_pass='".md5($params["new_pwd"])."', modified_by='" . $params["acct_id"] . "',modified_on=CURRENT_TIMESTAMP,pwd_changed=CURRENT_TIMESTAMP
                    WHERE coy_id = '" . Base_Common_Model::COMPANY_ID . "' AND sys_id='PMS' AND ref_id = '" . $params["supp_id"] . "' 
                        AND acct_id='" . $params["acct_id"] . "';";
            $savepwd = CI::db()->query($sql);
            return TRUE;
        }

        return NULL;
    }

    public function get_sysusr_email($id,$table='sys_usr_list') {
        if ($table=='ims_location_list')
            $sql = "SELECT email_ims as email FROM ims_location_list WHERE loc_id='$id' AND coy_id='CTL' LIMIT 1;";
        else
            $sql = "SELECT email_addr as email FROM sys_usr_list where usr_id='$id' LIMIT 1;";

        $result = CI::db()->query($sql)->row();

        if ($result) {
            return $result->email;
        }

        return "";
    }

    public function get_coy_address($ref_id, $coy_id = Base_Common_Model::COMPANY_ID, $return_format="" , $ref_type='') {

        // $return_format
        //  ""      => return all
        //  "address" => return addr_text only

        $sql_ref = ($ref_type!='') ? "  AND coy_address_book.ref_type='".$ref_type."' " : "" ;
        $sql = "SELECT 
                        coy_address_book.coy_id,ref_type,ref_id,addr_type,addr_format,
                        street_line1,street_line2,street_line3,street_line4,city_name,state_name,coy_address_book.country_id,com_country_list.country_name,postal_code,
                        addr_text,
                        sys_coy_list.coy_name, sys_coy_list.tel_code, sys_coy_list.fax_code, sys_coy_list.coy_reg_code, sys_coy_list.tax_reg_code
                    FROM coy_address_book 
                    LEFT JOIN com_country_list ON com_country_list.country_id=coy_address_book.country_id
                    LEFT JOIN sys_coy_list ON sys_coy_list.coy_id=coy_address_book.ref_id
                    WHERE  coy_address_book.ref_id='$ref_id' AND coy_address_book.coy_id='$coy_id' AND 
                        coy_address_book.addr_type='0-PRIMARY' $sql_ref ";

        $result = CI::db()->query($sql)->row();

        if ($result) {
            if ($return_format=="address") {
                return $result->addr_text;
            }
            else {
                return (array) $result;
            }
        }

        return "";
    }

}

?>
