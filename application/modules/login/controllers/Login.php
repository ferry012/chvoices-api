<?php

/* YS
 * 20-07-2016
 */

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Login extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login/Login_model');
        $this->load->model('api/Api_model');
    }
    
    public function userguide(){
        redirect( base_url("assets/chvoices-userguide-4.pdf") );
    }

    public function index() {
        $this->login();
    }

    public function login() {
        $this->isLogged();
    }

    public function login_validation() {
        $this->isLogged(); 

        $params["username"] = strtoupper($this->input->post('username'));
        $params["password"] = $this->input->post('password');
        $this->Api_model->logwrite( preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $params["username"])), 'Login attempt ' );

        if (filter_var($params["username"], FILTER_VALIDATE_EMAIL)) {
            $result = $this->Login_model->try_login_b2busr($params);
            if ( !empty($params["password"]) && $result != FALSE) {
                $this->Api_model->logwrite( $result[0]["supp_id"], 'Login succeess' );

                if ( strtotime($result[0]["eff_from"]) < time() && strtotime($result[0]["eff_to"]) > time() && $result[0]["status_level"]>0 ) {
                    $data = array(
                        'cm_user_id' => cleanString($result[0]["acct_id"]),
                        'cm_supp_id' => cleanString($result[0]["supp_id"]),
                        'cm_supp_onlinetype' => cleanString($result[0]["online_type"]),
                        'cm_supp_name' => $result[0]["supp_name"],
                        'email_addr' => $result[0]["email_addr"],
                        'email_addr3' => $result[0]["email_addr3"],
                        'cm_loc_id' => cleanString($result[0]["loc_id"]),
                        'cm_logged_in' => 1,
                        'r_acct_name' => $result[0]["acct_name"],
                        'r_acct_email' => $result[0]["acct_email"],
                        'r_access_type' => $result[0]["access_type"],
                        'r_access_control' => $result[0]["access_control"]
                    );
                    $this->session->set_userdata($data);
                    redirect('home/index');
                }
                else {
                    // Account is no longer active
                    $data["error_message"] = "Your account is no longer active. Please contact your Account in-charge.";
                    $this->session->set_flashdata('error_message', $data["error_message"]);
                    redirect('login');
                }

            }
        } else {
            // Default account login
            $result = $this->Login_model->try_login($params);
            if ( !empty($params["password"]) && $result != FALSE) {
                $this->Api_model->logwrite( $result[0]["supp_id"], 'Login succeess' );

                $data = array(
                    'cm_user_id' => cleanString($result[0]["supp_id"]),
                    'cm_supp_id' => cleanString($result[0]["supp_id"]),
                    'cm_supp_onlinetype' => cleanString($result[0]["online_type"]),
                    'cm_supp_name' => $result[0]["supp_name"],
                    'email_addr' => $result[0]["email_addr"],
                    'email_addr3' => $result[0]["email_addr3"],
                    'cm_loc_id' => cleanString($result[0]["loc_id"]),
                    'cm_logged_in' => 1,
                    'r_acct_name' => $result[0]["acct_name"],
                    'r_acct_email' => $result[0]["acct_email"],
                    'r_access_type' => $result[0]["access_type"],
                    'r_access_control' => $result[0]["access_control"]
                );
                $this->session->set_userdata($data);
                redirect('home/index');
            }
        }

        $data["error_message"] = "You have entered an invalid username / password combination.";
        $this->session->set_flashdata('error_message', $data["error_message"]);
        redirect('login');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

    public function isLogged() {
        if ($this->session->userdata('cm_logged_in') == 1) {
            redirect('home/index');
        } else {
            //$this->session->sess_destroy();
            $this->load->view('index');
        }
    }
    
    public function displayfile($filetype,$path_encode){
        // Provide full path to file in encode format
        $path_decode = base64_decode($path_encode); 
        $filename = basename($path_decode); 
        redirect("login/displayfilen/".$filetype.'/'.$path_encode.'/'.$filename);
    }
    
    public function displayfilen($filetype,$path_encode){
        // Provide full path to file in encode format
        $path_decode = base64_decode($path_encode);
        $data = file_get_contents($path_decode);
        if (in_array(strtolower($filetype), array('jpg','jpeg','gif','png')))
            header('Content-Type: image/png');
        else
            header("Content-type:application/pdf");
        echo $data;
    }
    
    public function reset_password(){
        $params["username"] = strtoupper($this->input->post("reset-username"));
        $params["emailaddress"] = $this->input->post("reset-emailaddress");
        $result = $this->Login_model->reset_password($params); 
        $this->Api_model->logwrite( $params["username"], 'Password reset: ' . $result );
        echo $result;
    }

}
