<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Challenger CHVOICES</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/iCheck/square/blue.css"); ?>">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $bgimg = (ENVIRONMENT == "production") ? base_url("assets/images/bg-live.jpg") : base_url("assets/images/bg-stage.jpg");
        ?>
        <style>
            body { 
                background: url(<?php echo $bgimg; ?>) no-repeat top center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            #myModal .modal-dialog { 
                width: 380px;
                height: 300px;
                position: absolute;
                top: 50%;
                left: 50%;
                margin-top: -150px;
                margin-left: -190px; 
            }
        </style>
    </head>
    <body class="hold-transition login-page">
        <div style="position: absolute; left:0px;top:0px; width:100%;height:100px">
            <div style="clear:both;margin:auto auto;width:1000px;padding:20px 0px 5px 0px">
                <h1 style="float:left;color:#000;margin:10px 0px;color:#093D56;font-family:arial;font-size:40px;">CHVOICES</h1>
                <span style="float:right"><img src="<?php echo base_url("assets/images/challenger-logo.png"); ?>" width="150px"></span>
            </div>
            <div style="clear:both;background-color:rgba(0, 0, 0, 0.7);height:40px;color:#FFF">
                <div style="clear:both;margin:auto auto;padding-top:8px;width:1000px;font-family:arial;font-size:18px;">
                    Challenger Vendor Order &amp; Invoice Confirmation System
                    <span style="padding-top:3px;float:right;font-size:14px">
                        <a data-toggle="modal" href="#myModal" style="color:#FFF;">Reset Password</a> |
                        <a href="<?php echo base_url("guide"); ?>" target="_blank" style="color:#FFF;">User Guide</a>
                    </span>
                </div>
            </div>
        </div>
        <div class="login-box" >
            <!-- /.login-logo -->
            <div class="login-box-body">
                <div class="login-logo">Sign in to start</div>
                <p class="login-box-msg">
                    <?php
                    $error_message = $this->session->flashdata('error_message');
                    if ($error_message)
                        echo '<font color="red">' . $error_message . '</font>';
                    ?>
                </p>
                <form action="<?php echo base_url("login/login_validation") ?>" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="username" class="form-control" placeholder="Supplier ID">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat w-100" style="background-color:#013C5A;border-color:#013C5A">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form> 
                <!-- /.social-auth-links -->
                <a href="#" class="hidden">I forgot my password</a><br> 
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
        <script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/iCheck/icheck.min.js"); ?>"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>


        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reset Password</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your Supplier ID and Email Address to receive a new password.</p>
                        
                        <form id="reset-form" method="post" onsubmit="return false;">
                            <div class="form-group has-feedback">
                                <input type="text" name="username" id="reset-username" class="form-control" placeholder="Supplier ID">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="text" name="email" id="reset-emailaddress" class="form-control" placeholder="Email Address">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <p id="reset-message"></p>
                            <div class="row">
                                <!-- /.col -->
                                <div class="col-xs-12">
                                    <button type="submit" id="reset-submit" class="btn btn-primary btn-block btn-flat w-100" style="background-color:#013C5A;border-color:#013C5A">Reset Password</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form> 
                        
                    </div>
                    <div class="hidden modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

<script>
$(document).ready(function () {
    
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $('#reset-submit').click(function (e) { 
        if ( $("#reset-username").val().length<3 || !(isEmail($("#reset-emailaddress").val())) ) {
            $('#reset-message').text('Please check your Supplier ID and Email Address.').css("color","red")
            return false;
        }
        
        $('#reset-message').text("Verifying your credential...").css("color","orange")

        var data = new FormData();
        data.append('reset-username',$("#reset-username").val())
        data.append('reset-emailaddress',$("#reset-emailaddress").val())

        $.ajax({
            type: "POST",
            //contentType: "application/json; charset=utf-8",
            url: "<?php echo base_url("login/reset_password") ;?>",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (msg) {
                $('#reset-message').text(msg).css("color","red")
            }
        });
    });
    
});
</script>
    </body>
</html>
