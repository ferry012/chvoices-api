<?php

/* YS
 * 19-12-2017
 */
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Elogistics_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkInvoiceExists($invoiceId)
    {
        $sql = "select 1 from sms_invoice_list where invoice_id='$invoiceId'";
        $res = $this->db->query($sql)->num_rows();
        if ($res > 0){
            return true;
        }else {
            return false;
        }
    }

    public function checkParcelExists($invoiceId)
    {
        $sql = "select 1 from b2b_parcel_list where invoice_id='$invoiceId'";
        $res = $this->db->query($sql)->num_rows();
        if ($res > 0){
            return true;
        }else {
            return false;
        }
    }

    public function getParcelInfo($invoiceId)
    {
        $sql = "select 
                    rtrim(l.invoice_id) invoice_id, l.parcel_id, l.delv_mode_id,
                    CAST(l.delivery_date as date) as [delivr_date],
                    l.status_level, 
                    case when EXISTS (select 1 FROM b2b_parcel_item i WHERE i.invoice_id = l.invoice_id) THEN 
                    (SELECT sum(qty_packed) FROM b2b_parcel_item i WHERE i.invoice_id = l.invoice_id)
                    ELSE 0 end as qty_packed, to_char(l.created_on, 'YYYY-MM-DD HH24:MI:SS') created_on
                from b2b_parcel_list l
                where l.invoice_id = '$invoiceId'";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    public function getInvoiceInfo($invoiceId)
    {
        $sql = "SELECT rtrim(a.invoice_id) invoice_id, to_char(a.invoice_date, 'YYYY-MM-DD HH24:MI:SS') invoice_date, 
                cast((SELECT sum(b.qty_invoiced) FROM sms_invoice_item b WHERE b.invoice_id = a.invoice_id) as int) qty_invoiced,
                cast((SELECT sum(b.qty_picked) FROM sms_invoice_item b WHERE b.invoice_id = a.invoice_id) as int) qty_picked,
                to_char((SELECT pick_date FROM sms_invoice_item b WHERE b.invoice_id = a.invoice_id limit 1), 'YYYY-MM-DD HH24:MI:SS') pick_date,
                case when EXISTS (select 1 FROM b2b_parcel_item i WHERE i.invoice_id = a.invoice_id) THEN 
                        (SELECT sum(qty_packed) FROM b2b_parcel_item i WHERE i.invoice_id = a.invoice_id)
                        ELSE 0 end as qty_packed
                FROM sms_invoice_list a
                WHERE a.invoice_id = '$invoiceId'";
        $res = $this->db->query($sql)->row();
        return $res;
    }

    public function getSize()
    {
        $sql = "SELECT delv_mode_id, parcel_size_id, rtrim(size_id) size_id, rtrim(size_desc) size_desc, rtrim(weight_max) weight_max, rtrim(dim_max) dim_max, dim_width, dim_height, dim_length,def_weight 
                FROM b2b_parcel_size 
                where delv_mode_id in ('NID','NJA','NRW','SPP','NSP')";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    public function getInvoiceItem($invoiceId)
    {
        $sql = "SELECT rtrim(s.invoice_id) invoice_id, rtrim(s.item_id) item_id, s.item_desc, cast(s.qty_picked as int) qty_picked, i.delv_mode_id, i.ref_num, i.qty_packed FROM sms_invoice_item s
                LEFT JOIN b2b_parcel_item i ON i.invoice_id = s.invoice_id AND i.coy_id = s.coy_id AND i.item_id = s.item_id
                WHERE s.invoice_id = '$invoiceId'";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    public function getNextParcelId($invoiceId)
    {
        $sql = "SELECT max(parcel_id) FROM b2b_parcel_list WHERE invoice_id = '$invoiceId'";
        $res = $this->db->query($sql)->first_row()->computed;
        if (empty($res)) {
            $res = 1;
        } else {
            $res = $res + 1;
        }
        return $res;
    }

    public function checkParcelListExists($invoiceId, $parcelId)
    {
        $sql = "select 1 from b2b_parcel_list where invoice_id='$invoiceId' and parcel_id = '$parcelId'";
        $res = $this->db->query($sql)->num_rows();
        if ($res > 0){
            return true;
        }else {
            return false;
        }
    }

    public function checkParcelItemExists($invoiceId, $parcelId)
    {
        $sql = "select 1 from b2b_parcel_list where invoice_id='$invoiceId' and parcel_id = '$parcelId'";
        $res = $this->db->query($sql)->num_rows();
        if ($res > 0){
            return true;
        }else {
            return false;
        }
    }

    public function getParcelItemMaxLine($invoiceId, $parcelId)
    {
        $sql = "SELECT max(parcel_line_num) FROM b2b_parcel_item WHERE invoice_id = '$invoiceId' and parcel_id = '$parcelId'";
        $res = $this->db->query($sql)->first_row()->computed;
        if (empty($res)) {
            $res = 1;
        } else {
            $res = $res + 1;
        }
        return $res;
    }

    public function updateOrAddParcelInfo($invoice_id, $username, $parcel_id, $volume, $volumeW, $volumeH, $volumeL, $weight, $parcel)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);

        $is_parcel_exists = $this->checkParcelListExists($invoice_id, $parcel_id);
        if ($is_parcel_exists) {
            $update_sql_list = "update b2b_parcel_list set volume = '$volume', \"volumeW\" = '$volumeW', \"volumeH\" = $volumeH, \"volumeL\" = '$volumeL', weight = '$weight'
                                where invoice_id = '$invoice_id' and parcel_id = '$parcel_id'";
            $this->db->query($update_sql_list);
            $delte_sql_item = "delete from b2b_parcel_item where invoice_id = '$invoice_id' and parcel_id = $parcel_id";
            $this->db->query($delte_sql_item);
        } else {
            $add_sql_list = "INSERT INTO b2b_parcel_list (coy_id, invoice_id, parcel_id, status_level, volume, \"volumeW\", \"volumeH\", \"volumeL\", weight, delivery_window, created_by, modified_by) VALUES 
                        ('CTL', '$invoice_id', '$parcel_id', 0, '$volume', '$volumeW', '$volumeH', '$volumeL', '$weight', -1, '$username', '$username')";
            $this->db->query($add_sql_list);
        }
        $parcel_item = array();
        $sql_parcel_item = "INSERT INTO b2b_parcel_item (coy_id, invoice_id, parcel_id, parcel_line_num, item_id, item_desc, qty_packed, ref_num) VALUES ";
        $line = 1;
        foreach ($parcel as $p) {
            $parcel_item[] = "('CTL', '" . $invoice_id ."', " . $parcel_id . ", ".$line . ", '" . $p['item_id'] . "', '" . $p['item_desc'] . "',
                                " . $p['qty_packed'] . ", " . $p['ref_num'] . ")";
            $line++;
        }
        if (!empty($parcel_item)) {
            $sql_parcel_item .= implode(',', $parcel_item);
            $this->db->query($sql_parcel_item);
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getDevlMode($invoiceId, $parcelId)
    {
        $sql_delv = "SELECT s.delv_mode_id, s.parcel_size_id, rtrim(s.size_id) size_id, rtrim(s.size_desc) size_desc, 
                        rtrim(s.weight_max) weight_max, rtrim(s.dim_max) dim_max, s.dim_width, s.dim_height, s.dim_length,
                        s.def_weight, l.tracking_id
                        FROM b2b_parcel_size s, b2b_parcel_list l
                        WHERE l.invoice_id = '$invoiceId' AND l.parcel_id = $parcelId
                        AND l.volumeW <= s.dim_width
                        AND l.volumeH <= s.dim_height
                        AND l.volumeL <= s.dim_length
                        AND s.delv_mode_id in ('NID','NJA','NRW','SPP','NSP')";
        $res_delv = $this->db->query($sql_delv)->result_array();
        $delv_window = array(
            "-2" => "0900 - 1800",
            "-1" => "0900 - 2200",
            "0" => "0900 - 1200",
            "1" => "1200 - 1500",
            "2" => "1500 - 1800",
            "3" => "1800 - 2200",
            "4" => "0900 - 1400",
            "5" => "1400 - 1800",
            "6" => "1800 - 2200",
            "7" => "1200 - 1800",
            "8" => "1100 - 1300"
        );
        return array("delv_mode" => $res_delv, "delv_window"=>$delv_window);
    }

    public function updateDispatchInfo($invoice_id, $parcel_id, $delv_mode, $parcel_size_id, $delivery_window, $username)
    {
        $tracking_id = $invoice_id . "-" . $delv_mode . "-" . (string)$parcel_id;
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);

        $parcel_list_sql = "UPDATE b2b_parcel_list SET delv_mode_id = '$delv_mode', status_level = 3, parcel_size_id = '$parcel_size_id', 
                            delivery_window = '$delivery_window', transaction_id = '$tracking_id', tracking_id = '$tracking_id', modified_by = '$username', modified_on=now()
                            WHERE invoice_id = '$invoice_id' AND parcel_id = '$parcel_id'";
        $this->db->query($parcel_list_sql);

        $parcel_item_sql = "UPDATE b2b_parcel_item SET delv_mode_id = '$delv_mode'
                            WHERE invoice_id = '$invoice_id' AND parcel_id = '$parcel_id'";
        $this->db->query($parcel_item_sql);

        $invoice_item_sql = "UPDATE sms_invoice_item SET delv_mode_id = '$delv_mode'
                              WHERE invoice_id = '$invoice_id'";
        $this->db->query($invoice_item_sql);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function printLabel($invoiceId, $parcelId)
    {
        $sql = "update b2b_parcel_list set label_print = now() where invoice_id = '$invoiceId' and parcel_id = '$parcelId'";
        $res = $this->db->query($sql);
        return $res;
    }

    public function IsNID($invoiceId)
    {
        $sql = "SELECT delv_mode_id FROM b2b_parcel_list WHERE invoice_id = '$invoiceId'";
        $res = $this->db->query($sql)->first_row()->delv_mode_id;
        if ($res === 'NID') {
            return true;
        } else {
            return false;
        }
    }

    public function getDriverName($invoiceId)
    {
        $sql = "SELECT rtrim(driver_name) driver_name FROM b2b_postcode_driver GROUP BY driver_name
                UNION
                SELECT rtrim(driver_name1) driver_name FROM b2b_postcode_driver GROUP BY driver_name1
                UNION
                SELECT rtrim(driver_name2) driver_name FROM b2b_postcode_driver GROUP BY driver_name2
                UNION
                SELECT rtrim(driver_name3) driver_name FROM b2b_postcode_driver GROUP BY driver_name3
                ";
        $res = $this->db->query($sql)->result_array();
        $res = array_map (function($res){
            return $res['driver_name'];
        } , $res);
        $sql_default = "SELECT rtrim(driver_name) driver_name FROM b2b_postcode_driver
                        WHERE post_code = (
                        SELECT LEFT(postal_code, 2) FROM 
                        sms_invoice_list l
                        LEFT JOIN coy_address_book b ON l.invoice_id = b.ref_id AND b.addr_type=l.delv_addr 
                        WHERE l.invoice_id = '$invoiceId'
                        )";
        $res_default = $this->db->query($sql_default)->first_row()->driver_name;
        $data = array(
            "default" => $res_default,
            "all_driver" => $res
        );
        return $data;
    }
}

?>
