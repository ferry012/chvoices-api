<?php

/* YS
 * 2020-06-03
 */
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Elogistics extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('elogistics/elogistics_model');
        $this->token = 'Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjE';
    }

    public function index(){
        echo json_encode(["code"=>0]);
    }

    /**
     * @api {get} /elogistics/getParcel/{invoiceId} Invoice and Parcel Info
     * @apiVersion 1.0.0
     * @apiGroup Elogistics
     *
     * @apiParam {string} invoiceId Invoice Id
     *
     * @apiSuccessExample Success get parcel info
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "invoice": {
     *              "invoice_id": "HIB20751",
     *              "invoice_date": "2019-11-25 12:56:26",
     *              "qty_invoiced": 9,
     *              "qty_picked": 9,
     *              "pick_date": "2019-12-13 10:56:07",
     *              "qty_packed": 9
     *          },
     *          "parcel": [{
     *              "invoice_id": "HIB20751",
     *              "parcel_id": 1,
     *              "delv_mode_id": "NID",
     *              "delivr_date": "2020-04-06",
     *              "status_level": 0,
     *              "qty_packed": 9,
     *              "created_on": "2020-04-04 20:55:12"
     *          },]
     *      }
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample invoice invalid
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, invoice invalid"
     *  }
     */
    public function getParcel($invoiceId = null)
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return $this->_errorWithInfo("Request code is error, please check your request code.");
        }
        if (is_null($invoiceId)) {
            return $this->_validateError("invoice_id required");
        }
        $invoice_exists = $this->elogistics_model->checkInvoiceExists($invoiceId);
        if ($invoice_exists) {
            $invoice_info = $this->elogistics_model->getInvoiceInfo($invoiceId);
            $parcel_exists = $this->elogistics_model->checkParcelExists($invoiceId);
            $parcel_info = [];
            if ($parcel_exists) {
                $parcel_info = $this->elogistics_model->getParcelInfo($invoiceId);
            }
            return $this->_successWithData(array('invoice'=>$invoice_info, 'parcel'=>$parcel_info));
        } else {
            return $this->_errorWithInfo("Sorry, invoice invalid");
        }
    }

    /**
     * @api {get} /elogistics/getSize All available parcel size
     * @apiVersion 1.0.0
     * @apiGroup Elogistics
     *
     * @apiSuccessExample Success get parcel size
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "delv_mode": [
     *              {
     *                  "delv_mode_id": "NID",
     *                  "parcel_size_id": 1,
     *                  "size_id": "EC2",
     *                  "size_desc": "Medium",
     *                  "weight_max": "10Kg",
     *                  "dim_max": "120cm",
     *                  "dim_width": 40,
     *                  "dim_height": 30,
     *                  "dim_length": 40,
     *                  "def_weight": 7500,
     *              },]
     *      }
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample server error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, can not get data"
     *  }
     */
    public function getSize()
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return $this->_errorWithInfo("Request code is error, please check your request code.");
        }

        $size = $this->elogistics_model->getSize();
        if (empty($size)) {
            return $this->_errorWithInfo("Sorry, can not get data");
        } else {
            return $this->_successWithData($size);
        }
    }

    /**
     * @api {get} /elogistics/getInvoiceItems/{invoiceId} Invoice Item
     * @apiVersion 1.0.0
     * @apiGroup Elogistics
     *
     * @apiParam {string} invoiceId Invoice Id
     *
     * @apiSuccessExample Success get item
     * HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {
     *              "invoice_id": "HIB20753",
     *              "item_id": "0889842501032",
     *              "item_desc": "Microsoft Surface Pro 7 (Intel i7,16GB RAM, 512GB SSD) (Blac",
     *              "qty_picked": 0,
     *              "delv_mode_id": null,
     *              "ref_num": null,
     *              "qty_packed": null
     *          },
     *          {
     *              "invoice_id": "HIB20753",
     *              "item_id": "8000000089397",
     *              "item_desc": "Valore PD01 20000 45W Power Bank bw Surface GO",
     *              "qty_picked": 1,
     *              "delv_mode_id": "NID",
     *              "ref_num": 3,
     *              "qty_packed": 1
     *          },]
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample invoice invalid
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, invoice invalid"
     *  }
     *
     * @apiErrorExample server error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, can not get data"
     *  }
     */
    public function getInvoiceItems($invoiceId=null)
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return $this->_errorWithInfo("Request code is error, please check your request code.");
        }
        if (is_null($invoiceId)) {
            return $this->_validateError("invoice_id required");
        }
        $invoice_exists = $this->elogistics_model->checkInvoiceExists($invoiceId);
        if ($invoice_exists) {
            $res = $this->elogistics_model->getInvoiceItem($invoiceId);
            if (!empty($res)) {
                return $this->_successWithData($res);
            } else {
                return $this->_errorWithInfo("Sorry, can not get data");
            }
        } else {
            return $this->_errorWithInfo("Sorry, invoice invalid");
        }
    }

    /**
     * @api {post} /elogistics/updateParcelInfo Save Parcel
     * @apiVersion 1.0.0
     * @apiGroup Elogistics
     *
     * @apiParam {string} invoice_id Invoice ID
     * @apiParam {number} [parcel_id] Parcel ID. Option, if null parcel_id is max+1
     * @apiParam {number} volume Volume
     * @apiParam {number} volumeW Volume Width
     * @apiParam {number} volumeH Volume Height
     * @apiParam {number} volumeL Volume Lenght
     * @apiParam {number} weight Weight
     * @apiParam {string} [username] Cashier Name. Option, default is 'MIS'
     * @apiParam {Object[]} parcel Parcel Item
     * @apiParam {Object} parcel.item_id Items Id
     * @apiParam {Object} parcel.item_desc Items Description
     * @apiParam {Object} parcel.qty_packed Qty Packed
     * @apiParam {Object} parcel.ref_num Items Ref Number
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "invoice_id": "HIB20752",
     *      "parcel_id": 3,
     *      "volume": 48000,
     *      "volumeW": 40,
     *      "volumeH": 30,
     *      "volumeL": 40,
     *      "weight": 1.66,
     *      "username": "3810",
     *      "parcel": [{
     *          "item_id": "0889842200829",
     *          "item_desc": "Surface Pro Type Cover (Black) ",
     *          "qty_packed": 1,
     *          "ref_num": 1
     *      },]
     *  }
     *
     * @apiSuccessExample success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Success add or update parcel info",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample server error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Whoops! There was a problem processing your parcel. Please try again."
     *  }
     *
     */
    public function updateParcelInfo()
    {
        ini_set("display_errors", 0);
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return $this->_errorWithInfo("Request code is error, please check your request code.");
        }
        $input = json_decode(file_get_contents('php://input'), true);
        $invoice_id = $input['invoice_id'];
        $invoice_exists = $this->elogistics_model->checkInvoiceExists($invoice_id);
        if (is_null($invoice_id)) {
            return $this->_validateError("invoice_id required");
        }
        if (!$invoice_exists) {
            return $this->_errorWithInfo("Sorry, invoice invalid");
        }
        $username = isset($input['username']) ? $input['username'] : 'MIS';
        $parcel_id = isset($input['parcel_id']) ? $input['parcel_id'] : $this->elogistics_model->getNextParcelId($invoice_id);
        $volume = $input['volume'];
        if (!isset($volume) || !is_numeric($volume)) {
            return $this->_validateError("volume required and must be number");
        }
        $volume = (int) $volume;
        $volumeW = $input['volumeW'];
        if (!isset($volumeW) || !is_numeric($volumeW)) {
            return $this->_validateError("volume required and must be number");
        }
        $volumeW = (int) $volumeW;
        $volumeH = $input['volumeH'];
        if (!isset($volumeH) || !is_numeric($volumeH)) {
            return $this->_validateError("volume required and must be number");
        }
        $volumeH = (int) $volumeH;
        $volumeL = $input['volumeL'];
        if (!isset($volumeL) || !is_numeric($volumeL)) {
            return $this->_validateError("volume required and must be number");
        }
        $volumeL = (int) $volumeL;
        $weight = $input['weight'];
        if (!isset($weight) || !is_numeric($weight)) {
            return $this->_validateError("volume required and must be number");
        }
        $weight = number_format($weight, 2, '.', '');
        $parcel = $input['parcel'];

        // Update or Add
        $res = $this->elogistics_model->updateOrAddParcelInfo($invoice_id, $username, $parcel_id, $volume, $volumeW, $volumeH, $volumeL, $weight, $parcel);
        if ($res) {
            return $this->_successWithInfo("Success add or update parcel info");
        } else {
            return $this->_errorWithInfo("Whoops! There was a problem processing your parcel. Please try again.");
        }
    }

    /**
     * @api {get} /elogistics/getDelvInfo/{invoiceId}/{parcelId} Delivery Mode & window
     * @apiVersion 1.0.0
     * @apiGroup Elogistics
     *
     * @apiParam {string} invoiceId Invoice Id
     * @apiParam {string} parcelId Parcel Id
     *
     * @apiSuccessExample Success get delivery
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "delv_mode": [
     *              {
     *                  "delv_mode_id": "NID",
     *                  "parcel_size_id": 1,
     *                  "size_id": "EC2",
     *                  "size_desc": "Medium",
     *                  "weight_max": "10Kg",
     *                  "dim_max": "120cm",
     *                  "dim_width": 40,
     *                  "dim_height": 30,
     *                  "dim_length": 40,
     *                  "def_weight": 7500,
     *                  "tracking_id": ""
     *              },]
     *          "delv_window": {
     *              "-2": "0900 - 1800",
     *              "-1": "0900 - 2200",
     *              "0": "0900 - 1200",
     *              "1": "1200 - 1500",
     *              "2": "1500 - 1800",
     *              "3": "1800 - 2200",
     *              "4": "0900 - 1400",
     *              "5": "1400 - 1800",
     *              "6": "1800 - 2200",
     *              "7": "1200 - 1800",
     *              "8": "1100 - 1300"
     *          }
     *      }
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample invoice_id or parcel_id invalid
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, invoice_id or parcel_id invalid"
     *  }
     */
    public function getDelvInfo($invoiceId = null, $parcelId = null)
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return $this->_errorWithInfo("Request code is error, please check your request code.");
        }
        if (is_null($invoiceId) || is_null($parcelId) || !is_numeric($parcelId)) {
            return $this->_validateError("invoice_id and parcel_id required");
        }
        $parcel_exists = $this->elogistics_model->checkParcelListExists($invoiceId, $parcelId);
        if ($parcel_exists) {
            $res = $this->elogistics_model->getDevlMode($invoiceId, $parcelId);
            return $this->_successWithData($res);
        } else {
            return $this->_errorWithInfo("Sorry, invoice_id or parcel_id invalid");
        }
    }

    /**
     * @api {post} /elogistics/updateDispatchInfo Update Dispatch Info
     * @apiVersion 1.0.0
     * @apiGroup Elogistics
     *
     * @apiParam {string} invoice_id Invoice ID
     * @apiParam {number} parcel_id Parcel ID
     * @apiParam {number} delv_mode_id Delivery Mode Id
     * @apiParam {number} parcel_size_id Parcek Size Id
     * @apiParam {number} delivery_window Delivery Window
     * @apiParam {string} [username] Cashier Name. Option, default is 'MIS'
     *
     * @apiParamExample {json} Request Example
     *  {
     *      "invoice_id": "HIB20752",
     *      "parcel_id": 3,
     *      "delv_mode_id": "NID",
     *      "parcel_size_id": 1,
     *      "delivery_window": -2,
     *      "username": "3810",
     *  }
     *
     * @apiSuccessExample success
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Success update dispatch info",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample server error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Whoops! There was a problem processing your parcel. Please try again."
     *  }
     *
     */
    public function updateDispatchInfo()
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return $this->_errorWithInfo("Request code is error, please check your request code.");
        }
        $input = json_decode(file_get_contents('php://input'), true);
        $invoice_id = $input['invoice_id'];
        if (is_null($invoice_id)) {
            return $this->_validateError("invoice_id required");
        }
        $parcel_id = $input['parcel_id'];
        if (is_null($parcel_id)) {
            return $this->_validateError("parcel_id required");
        }
        $invoice_exists = $this->elogistics_model->checkParcelListExists($invoice_id, $parcel_id);
        if (!$invoice_exists) {
            return $this->_errorWithInfo("Sorry, invoice_id or parcel_id invalid");
        }
        $delv_mode = $input['delv_mode_id'];
        if (is_null($delv_mode)) {
            return $this->_validateError("delv_mode_id required");
        }
        $parcel_size_id = $input['parcel_size_id'];
        if (is_null($parcel_size_id)) {
            return $this->_validateError("parcel_size_id required");
        }
        $delivery_window = $input['delivery_window'];
        if (is_null($delivery_window)) {
            return $this->_validateError("delivery_window required");
        }
        $username = isset($input['username']) ? $input['username'] : 'MIS';
        $res = $this->elogistics_model->updateDispatchInfo($invoice_id, $parcel_id, $delv_mode, $parcel_size_id, $delivery_window, $username);
        if ($res) {
            return $this->_successWithInfo("Success update dispatch info");
        } else {
            return $this->_errorWithInfo("Whoops! There was a problem processing your parcel. Please try again.");
        }
    }

    /**
     * @api {get} /elogistics/printLabel/{invoiceId}/{parcelId} Print Label
     * @apiVersion 1.0.0
     * @apiGroup Elogistics
     *
     * @apiParam {string} invoiceId Invoice Id
     * @apiParam {string} parcelId Parcel Id
     *
     * @apiSuccessExample Success print label
     * HTTP/1.1 200 OK
     *  {
     *      "info": "Print label success.",
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample Label print error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Label print error"
     *  }
     */
    public function printLabel($invoiceId = null, $parcelId = null)
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return $this->_errorWithInfo("Request code is error, please check your request code.");
        }
        if (is_null($invoiceId) || is_null($parcelId) || !is_numeric($parcelId)) {
            return $this->_validateError("invoice_id and parcel_id required");
        }
        $res = $this->elogistics_model->printLabel($invoiceId, $parcelId);
        if ($res) {
            return $this->_successWithInfo("Print label success.");
        } else {
            return $this->_errorWithInfo("Label print error");
        }
    }

    /**
     * @api {get} /elogistics/getDriverName/{invoiceId} Driver Name
     * @apiVersion 1.0.0
     * @apiGroup Elogistics
     *
     * @apiParam {string} invoiceId Invoice Id
     *
     * @apiSuccessExample Success get driver name
     * HTTP/1.1 200 OK
     *  {
     *      "data": {
     *          "default": "Royston",
     *          "all_driver": [
     *              "Alvin Tan",
     *              "Bhoomi",
     *              "Carlos",
     *              "Faizal",
     *              "Kay Shan",
     *              "Lim Boon Chew",
     *              "Low Siow Chye",
     *              "Royston",
     *              "Sarsi",
     *              "Sellappa",
     *              "Sudagar",
     *              "Yunos Ismail"
     *          ]
     *      },
     *      "status": "success",
     *      "status_code": 200
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Request code is error, please check your request code."
     *  }
     *
     * @apiErrorExample invoice invalid
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, invoice invalid"
     *  }
     *
     * @apiErrorExample delv mode error
     *  {
     *      "status": "error",
     *      "status_code": 404,
     *      "message": "Sorry, Delv mode must NID."
     *  }
     */
    public function getDriverName($invoiceId = null)
    {
        $code = $this->input->get('code', TRUE);
        if ($code !== $this->token) {
            return $this->_errorWithInfo("Request code is error, please check your request code.");
        }
        if (is_null($invoiceId)) {
            return $this->_validateError("invoice_id required");
        }
        $invoice_exists = $this->elogistics_model->checkInvoiceExists($invoiceId);
        if ($invoice_exists) {
            $res = $this->elogistics_model->IsNID($invoiceId);
            if ($res) {
                $data = $this->elogistics_model->getDriverName($invoiceId);
                return $this->_successWithData($data);
            } else {
                return $this->_errorWithInfo("Sorry, Delv mode must NID.");
            }
        } else {
            return $this->_errorWithInfo("Sorry, invoice invalid");
        }
    }

    public function _success()
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                'status' => 'success',
                'status_code' => 200
            )));
    }

    public function _successWithData($data)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                'data' => $data,
                'status' => 'success',
                'status_code' => 200
            )));
    }

    public function _successWithInfo($info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                'info' => $info,
                'status' => 'success',
                'status_code' => 200
            )));
    }

    public function _error()
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(array(
                'status' => 'error',
                'status_code' => 404
            )));
    }

    public function _validateError($errors)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(422)
            ->set_output(json_encode(array(
                'status' => 'validate error',
                'status_code' => 422,
                'errors' => $errors
            )));
    }

    public function _errorWithInfo($info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(array(
                'status' => 'error',
                'status_code' => 404,
                'message' => $info
            )));
    }

    public function _errorWithCodeAndInfo($code, $info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output(json_encode(array(
                'status' => 'error',
                'status_code' => $code,
                'message' => $info
            )));
    }

}
