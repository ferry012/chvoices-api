<?php

!defined('BASEPATH') OR ( 'No direct script access allowed');

class Rn_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }

    public function paging($page = NULL, $limit = NULL) {
        $page = $this->input->get('page');
        $limit = $this->input->get('limit');
        if (!empty($page) && !is_numeric($page))
            $this->invalid_params('page');
        if (!empty($limit) && !is_numeric($limit))
            $this->invalid_params('limit');
        if (!isset($page) || empty($page) || $page <= 0)
            $page = DEFAULT_PAGE;
        if (!isset($limit) || empty($limit) || $limit <= 0)
            $limit = DEFAULT_LIMIT;
        return array(
            "page" => $page,
            "limit" => $limit
        );
    }

    public function get_all_return_note_list(array $params) {
        $status_level = (isset($params["status_level"]) && $params["status_level"] != NULL) ? $params["status_level"] : "1";
        $sql_search = '';
        $sql_search.= (isset($params["search"]) && $params["search"] != NULL) ? " AND (pms_rn_list.trans_id LIKE '%" . $params["search"] . "%' OR pms_rn_list.buyer_id LIKE '%" . $params["search"] . "%')" : "";
        $sql_search.= (isset($params["search_to"]) && $params["search_to"] != NULL) ? " AND pms_rn_list.submit_date <= '" . date('Y-m-d H:i:s',strtotime($params["search_to"] . "+1 days")) . "'" : "";
        $sql_search.= (isset($params["search_from"]) && $params["search_from"] != NULL) ? " AND pms_rn_list.submit_date >= '" . date("Y-m-d H:i:s", strtotime(convert_to_mysql_time($params["search_from"]))) . "'" : "";
        $sql = "SELECT 
                    pms_rn_list.trans_id,
                    (SELECT status_desc FROM sys_status_list WHERE status_level = pms_rn_list.status_level AND ref_id = 'rn_list') status_desc,
                    TO_CHAR(pms_rn_list.submit_date,'DD-MM-YYYY') submit_date,--CONVERT(VARCHAR(10), CAST(pms_rn_list.submit_date AS DATE), 105) AS submit_date,
                    pms_rn_list.buyer_id,
                    TO_CHAR(pms_rn_list.collection_date,'DD-MM-YYYY') collection_date,--CONVERT(VARCHAR(10), CAST(pms_rn_list.collection_date AS DATE), 105) AS collection_date,
                    pms_rn_list.debit_note,
                    pms_rn_list.supp_notes,
                    pms_rn_list.remarks,
                    pms_rn_list.doc_ref,
                    pms_rn_list.receipt_id,
                    pms_rn_list.approve_by, 
                    TO_CHAR(pms_rn_list.approve_date,'DD-MM-YYYY') approve_date,--CONVERT(VARCHAR(10), CAST(pms_rn_list.approve_date AS DATE), 105) AS approve_date,    
                    pms_rn_list.created_by, 
                    (SELECT usr_name FROM sys_usr_list WHERE usr_id=pms_rn_list.created_by) as created_by_name, 
                    pms_rn_list.loc_id, 
                    pms_rn_list.supp_id, 
                    pms_rn_list.supp_name, 
                    pms_rn_list.coy_id,
                    (SELECT COUNT(*) FROM pms_rn_item R WHERE R.trans_id = pms_rn_list.trans_id  ) AS line_no,
                    (SELECT l.brand_id FROM pms_rn_item R, ims_item_list L WHERE R.item_id=L.item_id AND R.trans_id=pms_rn_list.trans_id ORDER BY R.created_on DESC LIMIT 1) AS remark_brand,
                    case
                        when rn_type ='CX' then 'Consignment Return'
                        else 
                            case when replacement_ind='N' then 'Return for CN due to Faulty' when replacement_ind='Y' then 'Exchange' when replacement_ind='R' then 'Return for CN due to SOR' when replacement_ind='S' then 'Service' when replacement_ind='C' then 'Consignment'	end	
                    end remark_reason,
                    (SELECT remarks FROM pms_rn_item R WHERE R.trans_id = pms_rn_list.trans_id AND R.remarks!='' LIMIT 1 ) AS item_remarks,
                    (SELECT brand_id FROM pms_rn_item R, ims_item_list I WHERE R.trans_id = pms_rn_list.trans_id AND I.item_id=R.item_id LIMIT 1 ) AS item_brand
                FROM pms_rn_list
                WHERE pms_rn_list.coy_id ='" . Base_Common_Model::COMPANY_ID . "' AND pms_rn_list.supp_id = '" . trim($params["supp_id"]) . "'
                AND pms_rn_list.status_level IN (" . $status_level . ") " . $sql_search . "
                LIMIT 800";
        $result = CI::db()->query($sql)->result_array();
        //echo"<pre>";print_r($this->db->last_query());exit;
        if ($result) {
            return $result;
        }
        return FALSE;
    }

    public function get_all_return_note_items(array $params) {
        $status_level = (isset($params["status_level"]) && $params["status_level"] != NULL) ? $params["status_level"] : "0";
        $sql = "SELECT 
                    pms_rn_item.line_num,
                    pms_rn_item.item_id,
                    pms_rn_item.item_desc,
                    pms_rn_item.trans_qty,
                    pms_rn_item.unit_price,
                    pms_rn_item.remarks
                FROM pms_rn_item
                WHERE pms_rn_item.trans_id = rtrim('" . $params["trans_id"] . "')  
                ORDER BY line_num ASC";
        $result = CI::db()->query($sql)->result_array();
        //echo"<pre>";print_r($this->db->last_query());exit;
        if ($result) {
            return $result;
        }
        return FALSE;
    }

    public function get_debit_note(array $params) {
        $sql = "SELECT
                    TO_CHAR(fin_journal.trans_date,'DD-MM-YYYY') trans_date,--CONVERT(VARCHAR(10), CAST(fin_journal.trans_date AS DATE), 105) AS trans_date,
                    fin_journal.trans_date,
                    fin_journal.doc_ref,
                    fin_journal.trans_id,
                    fin_journal.cust_supp_id,
                    pms_supplier_list.supp_name,
                    coy_address_book.street_line1,coy_address_book.street_line2,coy_address_book.street_line3,coy_address_book.street_line4,coy_address_book.country_id,coy_address_book.postal_code,
                    fin_drcr_note.contact_person,fin_drcr_note.tel_code,fin_drcr_note.fax_code, pms_supplier_list.email_addr,
                    fin_journal.trans_desc,
                    fin_journal.trans_amount,
                    fin_journal.tax_percent
                FROM fin_journal 
                    LEFT JOIN fin_drcr_note ON fin_drcr_note.trans_id=fin_journal.trans_id
                    LEFT JOIN pms_supplier_list ON pms_supplier_list.supp_id=fin_journal.cust_supp_id AND pms_supplier_list.coy_id=fin_journal.coy_id
                    LEFT JOIN coy_address_book ON coy_address_book.ref_type='SUPPLIER' AND coy_address_book.ref_id=fin_journal.cust_supp_id AND coy_address_book.coy_id=fin_drcr_note.coy_id AND coy_address_book.addr_type=fin_drcr_note.addr_type
                WHERE fin_journal.trans_id LIKE '".$params["search"]."' 
                LIMIT 1";
        $result = CI::db()->query($sql)->row();
        //echo"<pre>";print_r($this->db->last_query());exit;
        if ($result) {
            return $result;
        }
        return FALSE;
    }

    public function update_return_note_date(array $params) {
        $sql = "UPDATE pms_rn_list
                  SET collection_date='" . $params["collection_date"] . "', 
                      modified_by='" . $params["modified_by"] . "', modified_on='" . $params["modified_on"] . "'
                  WHERE trans_id IN ('" . $params["trans_id"] . "');  ";

        $result = CI::db()->query($sql);
        return $result;
    }

    public function update_return_note_suppnote(array $params) {
        $sql = "UPDATE pms_rn_list
                  SET supp_notes='" . $params["supp_notes"] . "', 
                      modified_by='" . $params["modified_by"] . "', modified_on='" . $params["modified_on"] . "'
                  WHERE trans_id IN ('" . $params["trans_id"] . "');  ";

        $result = CI::db()->query($sql);
        return $result;
    }

    public function get_rn_collectdate($doc_ref) {
        $sql = "select requested_date as doc_date from pms_rn_list where trans_id='".$doc_ref."';  ";
        $result = CI::db()->query($sql)->result_array();
        return ($result) ? $result : FALSE;
    }

    public function CallSP_email(array $params) {
        $result = $this->_SendEmail($params);
        return $result;
    }

}

?>
