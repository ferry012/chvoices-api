<table id="datatable" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No</th> 
            <th>Item ID</th>
            <th>Description</th>
            <th>Qty</th>
            <th>Remarks</th> 
        </tr>
    </thead>
    <tbody>
        <?php if ($results): ?>
            <?php foreach ($results as $result): ?>
                <?php
                $line_num = isset($result["line_num"]) ? $result["line_num"] : "";
                $item_id = isset($result["item_id"]) ? $result["item_id"] : "";
                $item_desc = isset($result["item_desc"]) ? $result["item_desc"] : "";
                $trans_qty = isset($result["trans_qty"]) ? $result["trans_qty"] : "";
                $remarks = isset($result["remarks"]) ? $result["remarks"] : "";
                ?>
                <tr>
                    <td><?php echo $line_num; ?></td>
                    <td><?php echo $item_id; ?></td>
                    <td><?php echo $item_desc; ?></td>
                    <td><?php echo display_number($trans_qty); ?></td>
                    <td><?php echo $remarks; ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>