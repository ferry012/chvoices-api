<section class="clearfix content" id="Return_Note_Coll">
    <?php echo theme_view('general/search_bar_filter'); ?>
    <div class="row" id="UpdateCollectionDate_Div">
        <div class="col-xs-12">
            <div class="box box-danger " >
                <div class="box-header hidden">
                    <h3 class="box-title">RN Details </h3>
                </div>
                <div class="box-body"> 
                    <div class="col-sm-5">
                        <div id="newcodate_div">
                            <div class="col-sm-12">
                                <p><span><b>Update Collection Date: <span id="sel_num"></span></b></span></p>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group date datepicker_future" style="margin-bottom:4px"> 
                                    <input placeholder="Collection date" type="text" class="form-control" name="new_codate" id="new_codate" value="" >
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <span id="UpdateCollectionDate_Resp" style="display:block"></span>
                                </div>
                            </div>
                            <div class="col-sm-6" style="margin-top:0px">
                                <button class="btn btn-success" id="UpdateCollectionDate_Btn"><i class="fa fa-send"></i> Update Selected</button>
                            </div>  
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="col-sm-12">
                            <p><span id="RMAfiles_Resp"><b>Supporting Documents</b></span>&nbsp;</p>
                        </div>
                        <div class="col-sm-12">
                            <div id="myUploadFiles"></div> 
                            <div id="rn_pdf_div" style="float:right"></div>
                        </div>
                    </div> 
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row" id="listing_rn">
        <div class="col-xs-12">
            <div class="box">
                <div class="hidden box-header">
                    <h3 class="box-title">RN Listings</h3>
                </div>
                <div class="box-body">                    
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="no-sort"><span class="fa fa-check-square-o" id="sel_all_btn" title="Select all on this page"></span></th>
                                <th>RN #</th> 
                                <th class="">Submit Date</th>
                                <th class="h-mobile no-sort">Buyer</th>
                                <th class="chv-green-gradient">Collection Date</th>
                                <th class="h-mobile no-sort" style="max-width:30%">Remarks<br>Notes to buyer</th>
                                <th class="h-mobile no-sort">Brand</th>
                                <th class="h-mobile no-sort">Doc Ref</th>
                                <th class="h-mobile no-sort">Receipt ID</th>
                                <th class="h-mobile no-sort">Line</th>
                                <th class="no-sort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($results): $k = 0; ?>
                                <?php foreach ($results as $result): ?>
                                    <?php
                                    $line_no = isset($result["line_no"]) ? trim($result["line_no"]) : 0;
                                    $trans_id = isset($result["trans_id"]) ? trim($result["trans_id"]) : "";
                                    $status_desc = isset($result["status_desc"]) ? $result["status_desc"] : "";
                                    $submit_date = isset($result["submit_date"]) ? $result["submit_date"] : "";
                                    $buyer_id = isset($result["buyer_id"]) ? $result["buyer_id"] : "";
                                    $loc_id = isset($result["loc_id"]) ? $result["loc_id"] : "";
                                    $created_by = isset($result["created_by"]) ? $result["created_by"] : "";
                                    $collection_date = (isset($result["collection_date"]) && strtotime($result["collection_date"]) > 631123200 ) ? $result["collection_date"] : "";
                                    $debit_note = isset($result["debit_note"]) ? $result["debit_note"] : "";
                                    $doc_ref = isset($result["doc_ref"]) ? $result["doc_ref"] : "";
                                    $receipt_id = isset($result["receipt_id"]) ? $result["receipt_id"] : "";
                                    $approve_by = isset($result["approve_by"]) ? $result["approve_by"] : "";
                                    $approve_date = (isset($result["approve_date"]) && strtotime($result["approve_date"]) > 631123200 ) ? $result["approve_date"] : "";
                                    $supp_notes = isset($result["supp_notes"]) ? $result["supp_notes"] : "";
                                    $remarks = isset($result["remarks"]) ? $result["remarks"] : "";
                                    $fullnotes = ($remarks != "") ? '<span id="RemarkShow_'.$trans_id.'">' . $remarks . '</span><br>' : '';
                                    $fullnotes.= ($supp_notes != "") ? '<span id="NoteShow_'.$trans_id.'"><i>Notes:</i> ' . $supp_notes . '</span>' : '<span id="NoteShow_'.$trans_id.'"></span>';
                                    $fullnotes.= ($result["item_remarks"] != "") ? '<!--<br>' . $result["item_remarks"] . '-->' : '';
                                    $item_brand = isset($result["item_brand"]) ? $result["item_brand"] : "";
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" class="cb" id="<?php echo 'cb_' . $k; ?>" name="inv_selected[]" value="<?php echo $trans_id; ?>" ></td>
                                        <td><a href="<?php echo base_url("rn/rn_export/rn/".$trans_id) ; ?>" class="popuplink"><?php echo $trans_id; ?></a></td>
                                        <td data-sort="<?php echo strtotime($submit_date); ?>"><span title="Submitted on <?php echo $submit_date; ?>"><?php echo $submit_date; ?></span></td>
                                        <td class="h-mobile"><?php echo $buyer_id; ?></td>
                                        <td data-sort="<?php echo strtotime($collection_date); ?>" class="chv-green-gradient field-selection cbdate" id="<?php echo 'cbdate_' . $k; ?>"><span id="<?php echo 'codate_' . $k; ?>"><?php echo $collection_date; ?></span></td> 
                                        <td class="h-mobile"><div style="width:300px"><?php echo $fullnotes; ?></div></td>
                                        <td class="h-mobile"><?php echo $item_brand; ?></td> 
                                        <td class="h-mobile"><?php echo $doc_ref; ?></td>
                                        <td class="h-mobile"><?php echo $receipt_id; ?></td>
                                        <td class="h-mobile"><?php echo $line_no; ?></td>
                                        <td>
                                            <input type="hidden" id="CodateOld_<?php echo $trans_id; ?>" value="<?php echo $collection_date; ?>" >
                                            <input type="hidden" id="Note_<?php echo $trans_id; ?>" value="<?php echo $supp_notes; ?>" >
                                            <input type="hidden" id="Buyerid_<?php echo $trans_id; ?>" value="<?php echo $buyer_id; ?>" >
                                            <input type="hidden" id="Createdby_<?php echo $trans_id; ?>" value="<?php echo $created_by; ?>" >
                                            <input type="hidden" id="Locid_<?php echo $trans_id; ?>" value="<?php echo $loc_id; ?>" >
                                            <a class="btn btn-info btn-sm ViewGRNFull" id="<?php echo $trans_id; ?>" id2="<?php echo 'dt_' . $k; ?>" href="#">Items</a>
                                            <a class="btn btn-info btn-sm ViewNotebuyer" id="<?php echo $trans_id; ?>" data-toggle="modal" data-target="#myNotebuyerModal">Notes</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $k++;
                                endforeach;
                                ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="listing_rn_div">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">RN Items <span id="rn_no"></span></h3>
                    <span id="ItembtnSpan"></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="datatable_div">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>PO ID</th> 
                                <th>Item ID</th>
                                <th>Item Description</th>
                                <th>GRN Qty</th>
                                <th>Unit Price</th>
                                <th class="no-sort">Disc %</th>
                                <th>Amount</th>
                                <th class="no-sort">Tax Amt</th>
                                <th>Supp DO<br>Recv. Date</th>
                                <th class="no-sort">Tax<br>Curr</th> 
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    
    <span class="pull-right" id="ViewGRNFullCloseSpan"><a href="#" id="ViewGRNFullClose" class="btn btn-default text-bold"><i class="fa fa-arrow-left"></i> Back to Return Notes</a></span>
    
</section>

<div class="modal fade" id="myNotebuyerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Email Notes To Buyer</h4>
            </div>
            <div class="modal-body">
                <p><b>Return Note # <span id="rn_no_span"></span></b></p>
                <form role="form" id="Invoice_submit_match" 
                      action="<?php echo base_url("rn/buyernotes"); ?>" method="post" enctype="multipart/form-data">
                    <div class="hidden visible-xs visible-sm">
                        <b>RN Remarks:</b><br>
                        <div id="seeremarks"></div>
                        <hr>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" name="rn_no" id="rn_no" value="">
                            <input type="hidden" name="loc_id" id="loc_id" value="">
                            <input type="hidden" name="buyer_id" id="buyer_id" value="">
                            <input type="hidden" name="created_by" id="created_by" value="">
                            <textarea name="buyernotes" id="buyernotes" class="form-control" placeholder="Enter notes here..." required=""></textarea>
                        </div>
                        <div id="submit_response">
                            Your message will be saved and sent to the Buyer.
                        </div>
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="myNotebuyerSender" class="btn btn-primary"><i class="fa fa-send"></i> Send</button>
            </div>
        </div>
    </div>
</div>