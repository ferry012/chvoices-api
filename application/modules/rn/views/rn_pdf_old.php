<?php

ob_start();
tcpdf();

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        $this->SetFont('helvetica', '', 9);
        $headerData = $this->getHeaderData();
        $this->writeHTML($headerData['string']);
    }

    // Page footer
    public function Footer() {
        $this->SetY(-50);
        $this->SetFont('helvetica', '', 9);
        $footer = $this->getFooterString();
        $footer .= ''; //'<table style="padding: 5px;"><tr><td align=left></td><td align=center></td><td align="right">Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages() . '</td></tr></table>';
        
        $this->writeHTML($footer);
    }

    public $isLastPage = false;
    public function lastPage($resetmargins = false) {
        $this->setPage($this->getNumPages(), $resetmargins);
        $this->isLastPage = true;
    }

}

$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, 75, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(true, 65);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);

$barcode_params = $obj_pdf->serializeTCPDFtagParameters(array($rninfo['trans_id'], 'C128', '', '', 63, 5, 0.4, array('position'=>'S', 'border'=>false, 'padding'=>0, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4), 'N'));
$barcode = '<tcpdf method="write1DBarcode" params="'.$barcode_params.'" />';

$logo = 'http://chvoices.challenger.sg/assets/images/logo.png'; // base_url('assets/images/logo.png');
$CustomHeader = '<table style="font-size:9px;">
    <tr>
        <td style="width: 25%; text-align: left;">
            <div>
                <img style="width: 130px; height: auto;" src="' . $logo . '"/><br/>
                <span><br>Co. Reg. No.: '.$ctl_addr["coy_reg_code"].'</span> 
            </div>
        </td>
        <td style="width: 40%; text-align: left; margin-left:10px;">
            <table width="100%">
                <tr><td>&nbsp;<br><span style="font-size:10px"><b>'.$ctl_addr["coy_name"].'</b></span></td></tr>
                <tr><td>'.$ctl_addr["street_line1"].'</td></tr>
                <tr><td>'.$ctl_addr["street_line2"].'</td></tr>
                <tr><td>'.$ctl_addr["country_name"].' '.$ctl_addr["postal_code"].'</td></tr>
                <tr><td>Tel: '.$ctl_addr["tel_code"].' Fax: '.$ctl_addr["fax_code"].'</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
        <td style="width: 35%;" align="right">
            <table width="100%">
                <tr><td>
                    <table width="100%" cellspacing="1" cellpadding="1">
                        <tr><td align="right" width="45"><b>DATE:</b></td><td width="130" style="border:1px solid #888;"><span>'.$rninfo['submit_date'].'</span></td></tr>
                        <tr><td align="right"><b>CURR.:</b></td><td style="border:1px solid #888"><span>SGD</span></td></tr>
                        <tr><td align="right"><b>R.N. NO.:</b></td><td style="border:1px solid #888"><span>'.$rninfo['trans_id'].'</span></td></tr>
                    </table>
                </td></tr>
                <tr><td align="right">&nbsp;<br><span style="font-weight:bold;">RETURN NOTE &nbsp;&nbsp;</span></td></tr>
                <tr><td align="right">'.$barcode.'</td></tr>
            </table>
        </td>
    </tr>
</table>

<table style="padding: 5px;font-size: 9px;">
    <tr>
        <td style="border: 1px solid black; width: 270px; height: 90px;"><span><b><i>To:</i></b> '.$rninfo["supp_name"].'</span>
            <div>'.nl2br($rninfo_supp_add).'</div>
        </td>
        <td style="border: 1px solid black; width: 270px; height: 90px;"><span><b><i>Collect At:</i></b> Challenger Technologies Limited</span>
            <div>'.nl2br($rninfo_coll_add).'</div>
        </td>
    </tr>
</table>

<table style="paddding: 5px;margin-top:10px">
    <tr><td colspan="4">&nbsp;</td></tr>
    <tr style="background-color:#DDD;">
        <th style="width: 30px;  border: 1px solid black;text-align: center; font-size: 10px;">Item</th>
        <th style="width: 220px; border: 1px solid black; text-align: center; font-size: 10px;">Description</th>
        <th style="width: 90px; border: 1px solid black;text-align: center; font-size: 10px;">Quantity</th>
        <th style="width: 100px; border: 1px solid black;text-align: center; font-size: 10px;">Unit Price</th>
        <th style="width: 100px; border: 1px solid black;text-align: center; font-size: 10px;">Amount</th>
    </tr>
</table>';

$CustomFooter = '<table style="padding: 5px;">
                <tr>
                    <td align="center"><b><u>Lunch hour from 12.30 to 1.30pm. Pls avoid.</u></b></td>
                </tr>
            </table>
            <table cellpadding="5" cellspacing="2">
                <tr>
                    <td style="border: 1px solid black; width: 160px; height: auto; font-size: 9px;">
<b>Requested By:</b><br>'.$rninfo["created_by_name"].'<br>
<b>Approved By:</b><br>'.$rninfo["approve_by"].'<br>
<b>Status:</b> '.$rninfo["status_desc"].'
                    </td>
                    <td style="border: 1px solid black; width: 210px; height: auto; font-size: 9px;">
NOTE: WAREHOUSE OPERATION HOURS<br>COLLECTION TIME: MONDAY - FRIDAY<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; AVOID LUNCH HOUR<br>
* NO COLLECTION OF GOODS ON<br>&nbsp;&nbsp;&nbsp;SATURDAY, SUNDAY & PUBLIC HOLIDAY
                    </td>
                    <td style="border: 0px solid white; width: 170px; height: auto; font-size: 9px;">
<b>Received By:</b><br><br><br><br><br>
<table style="border-top:1px solid #888"><tr><td><b>Name, Signature, IC No. or Company Stamp and Date</b></td></tr></table>
                    </td>
                </tr>
            </table>';

$total_price=0;
ob_start();
?>
<table style="paddding: 5px;margin-top:10px;font-size: 10px;">
    <tr><td colspan="5">&nbsp;</td></tr>
    <?php if ($items): ?>
        <?php foreach ($items as $result): ?>
            <?php
            $line_num = isset($result["line_num"]) ? $result["line_num"] : "";
            $item_id = isset($result["item_id"]) ? $result["item_id"] : "";
            $item_desc = isset($result["item_desc"]) ? $result["item_desc"] : "";
            $trans_qty = isset($result["trans_qty"]) ? $result["trans_qty"] : "";
            $unit_price = isset($result["unit_price"]) ? $result["unit_price"] : "";
            $remarks = isset($result["remarks"]) ? $result["remarks"] : "";
            $total_price += ($trans_qty * $unit_price);
            ?>
            <tr style="margin-top:3px">
                <td style="width: 30px;text-align: left; font-size: 10px;"><?php echo $line_num; ?></td>
                <td style="width: 220px;  text-align: left; font-size: 10px;"><?php echo $item_desc; ?><br><?php echo $item_id; ?><br><?php echo $remarks; ?></td>
                <td style="width: 90px;text-align: right; font-size: 10px;"><?php echo display_number($trans_qty); ?></td>
                <td style="width: 100px;text-align: right; font-size: 10px;"><?php if ($rntype=='rn') echo price_no_symbol($unit_price); ?></td>
                <td style="width: 100px;text-align: right; font-size: 10px;"><?php if ($rntype=='rn') echo price_no_symbol($trans_qty * $unit_price); ?></td>
            </tr> 
        <?php endforeach; ?>
    <?php endif; ?> 
    <tr><td colspan="5">&nbsp;</td></tr>
</table>
<table>
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr>
        <td colspan="3" style="width:320"></td>
        <td style="width:110;text-align: center;"><span style="font-size:14px"><b><?php if ($rntype=='rn') echo 'TOTAL'; ?></b></span></td>
        <td height="18" <?php if ($rntype=='rn') echo 'style="width:110;background-color: #F4F4F4; text-align: center; border: 1px solid black;"'; ?>>
            <span style="font-size:14px"><?php if ($rntype=='rn') echo price_no_symbol($total_price); ?></span>
        </td>
    </tr>  
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr>
        <td colspan="5" style="height: 30px; "><table><tr><td width="60"><b>Doc Ref:</b></td><td><?php echo nl2br($rninfo['doc_ref']); ?></td></tr></table></td>
    </tr>
    <tr>
        <td colspan="5" style="height: 30px; "><table><tr><td width="60"><b>Remarks:</b></td><td><?php echo nl2br($rninfo['remarks']); ?></td></tr></table></td>
    </tr>
    <tr>
        <td colspan="5" style="height: 30px; "><br><b><u>TERMS AND CONDITIONS:</u><br>
Vendor must verify the Unit Cost and Quantity stated in Return Note.<br>
All unit cost stated in Return Note (RN) will be based on LAST PURCHASED COST OR LATEST PRICE PROTECTED COST.<br>
No Amendment is allowed once Goods are collected and Return Note is signed<br>
Vendor to collect the return stock within 7 days from date of Return Note. Failing which Challenger reserve the Right to dispose the above Goods at our Discretion.<br>
Challenger will issue Debit Note once the RN is collected and shall proceed to contra/offset with payment. Vendor do not need to issue Credit Note of Challenger.
        </b></td>
    </tr>
</table>
<?php
$content = ob_get_contents();
ob_end_clean();

$obj_pdf->setHeaderData($ln = '', $lw = 0, $ht = '', $CustomHeader, $tc = array(0, 0, 0), $lc = array(0, 0, 0));
$obj_pdf->SetHeaderMargin(5);
$obj_pdf->setPrintHeader(true);
$obj_pdf->setFooterData(array(0, 0, 0), array(0, 0, 0), $CustomFooter);
$obj_pdf->SetFooterMargin(20);
$obj_pdf->setPrintFooter(true);
$obj_pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->AddPage(); 
$obj_pdf->writeHTML($content, true, false, true, false, '');
if (ceil($obj_pdf->GetY()) >= 90) {
    $obj_pdf->AddPage();
}

$lastPage = $obj_pdf->getPage();
$obj_pdf->deletePage($lastPage);
$obj_pdf->Output('output.pdf', 'I');








