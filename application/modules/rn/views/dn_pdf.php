<?php

ob_start();
tcpdf();

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        $this->SetFont('helvetica', '', 9);
        $headerData = $this->getHeaderData();
        $this->writeHTML($headerData['string']);
    }

    // Page footer
    public function Footer() {
        $this->SetY(-50);
        $this->SetFont('helvetica', '', 9);
        $footer = $this->getFooterString();
        $footer .= '<table style="padding: 5px;"><tr><td align=left></td><td align=center></td><td align="right">Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages() . '</td></tr></table>';
        
//        if ($this->isLastPage) {
//            // Show on footer of the last page only
//            $this->SetY(-230);
//            $footer .= 'Show on last page footer only';
//        }
        
        $this->writeHTML($footer);
    }

    public $isLastPage = false;
    public function lastPage($resetmargins = false) {
        $this->setPage($this->getNumPages(), $resetmargins);
        $this->isLastPage = true;
    }

}

$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, 75, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(true, 65);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);

$barcode = '';
//$barcode_params = $obj_pdf->serializeTCPDFtagParameters(array($rninfo['trans_id'], 'C128', '', '', 63, 5, 0.4, array('position'=>'S', 'border'=>false, 'padding'=>0, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4), 'N'));
//$barcode = '<tcpdf method="write1DBarcode" params="'.$barcode_params.'" />';

$ToAdd = ($dninfo['street_line1']!='') ? $dninfo['street_line1'] : '' ;
$ToAdd.= ($dninfo['street_line2']!='') ? "<br>".$dninfo['street_line2'] : '' ;
$ToAdd.= ($dninfo['street_line3']!='') ? "<br>".$dninfo['street_line3'] : '' ;
$ToAdd.= ($dninfo['street_line4']!='') ? "<br>".$dninfo['street_line4'] : '' ;
$ToAdd.= ($dninfo['country_id']=='SG') ? "<br>Singapore " : "<br>".$dninfo['country_id'] ;
$ToAdd.= ($dninfo['postal_code']!='') ? $dninfo['postal_code'] : '' ;

$logo = 'http://chvoices.challenger.sg/assets/images/logo.png'; // base_url('assets/images/logo.png');
$CustomHeader = '<table style="font-size:9px;">
    <tr>
        <td style="width: 25%; text-align: left;">
            <div>
                <img style="width: 130px; height: auto;" src="' . $logo . '"/><br/> 
            </div>
        </td>
        <td style="width: 40%; text-align: left; margin-left:10px;">
            <table width="100%">
                <tr><td>&nbsp;<br><span style="font-size:10px"><b>'.$ctl_addr["coy_name"].'</b></span></td></tr>
                <tr><td>'.$ctl_addr["street_line1"].'</td></tr>
                <tr><td>'.$ctl_addr["street_line2"].'</td></tr>
                <tr><td>'.$ctl_addr["country_name"].' '.$ctl_addr["postal_code"].'</td></tr>
                <tr><td>Tel: '.$ctl_addr["tel_code"].' Fax: '.$ctl_addr["fax_code"].'</td></tr>
                <tr><td>ROC No.: '.$ctl_addr["coy_reg_code"].'</td></tr>
                <tr><td>GST No.: '.$ctl_addr["tax_reg_code"].'</td></tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
        <td style="width: 35%;" align="right">
            <table width="100%">
                <tr><td>
                    <table width="100%" cellspacing="1" cellpadding="1">
                        <tr><td align="right" width="50"><b>DATE:</b></td><td width="130" style="border:1px solid #888;"><span>'.$dninfo['trans_date'].'</span></td></tr>
                        <tr><td align="right"><b>OUR REF.:</b></td><td style="border:1px solid #888"><span>'.$dninfo['doc_ref'].'</span></td></tr>
                        <tr><td align="right"><b>DN NO.:</b></td><td style="border:1px solid #888"><span>'.$dninfo['trans_id'].'</span></td></tr>
                    </table>
                </td></tr>
                <tr><td align="right">&nbsp;<br><span style="font-weight:bold;">TAX INVOICE / DEBIT NOTE &nbsp;&nbsp;</span></td></tr>
                <tr><td align="right">'.$barcode.'</td></tr>
            </table>
        </td>
    </tr>
</table>

<table style="padding: 5px;font-size: 9px;">
    <tr>
        <td style="border: 1px solid black; width: 270px; height: 90px;"><span><b><i>To:</i></b> '.$dninfo["supp_name"].'</span>
            <div>'.nl2br($ToAdd).'</div>
        </td>
        <td style="border: 0px solid white; width: 270px; height: 90px;">
            <span><b>Attn:</b> '.$dninfo["contact_person"].'</span><br>
            <span><b>Tel:</b> '.$dninfo["tel_code"].'</span><br>
            <span><b>Fax:</b> '.$dninfo["fax_code"].'</span><br>
            <span><b>Email:</b> '.$dninfo["email_addr"].'</span><br>
        </td>
    </tr>
</table>

<table style="paddding: 5px;margin-top:10px">
    <tr><td colspan="4">&nbsp;</td></tr>
    <tr style="background-color:#DDD;">
        <th style="width: 410px; border: 1px solid black; text-align: center; font-size: 10px;">Description</th>
        <th style="width: 130px; border: 1px solid black;text-align: center; font-size: 10px;">Amount</th>
    </tr>
</table>';

$CustomFooter = '<table cellpadding="5" cellspacing="2">
                <tr>
                    <td style="border: 0px solid white; width: 350px; height: auto; font-size: 9px;"><b>1. Cheque should be crossed and made payable to:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$ctl_addr["coy_name"].'</b><br><br>2. Please quote \'OUR REF.\' when making payment.<br><br>3. Interest will be charged at 1.5% per month on overdue account.</td>
                    <td align="right" style="border: 0px solid white; width: 190px; height: auto; font-size: 9px;"><b>This is a computer generated document. No signature required.</b></td>
                </tr>
            </table>';

$beforegst = sprintf('%0.2f', $dninfo["trans_amount"] / ( (100+$dninfo["tax_percent"])/100) );
$gstamount = $dninfo["trans_amount"] - $beforegst;
ob_start();
?>
<table style="padding: 5px;margin-top:10px;font-size: 10px;">
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr>
        <td style="width: 410px; text-align: left; font-size: 10px;"><?php echo nl2br($dninfo["trans_desc"]); ?></td>
        <td style="width: 130px; text-align: right; font-size: 10px;"><?php echo $beforegst; ?></td>
    </tr>
    <tr><td colspan="5">&nbsp;</td></tr>
</table>
<table style="padding: 5px">
    <tr>
        <td style="width: 420px; text-align: right; font-size: 10px;"><b>Add GST (<?php echo $dninfo["tax_percent"]; ?>%):</b></td>
        <td style="width: 120px; text-align: right; font-size: 10px;"><?php echo $gstamount; ?></td>
    </tr>
    <tr>
        <td style="width: 420px; text-align: right; font-size: 10px;"><b>Total Amount:</b></td>
        <td style="width: 120px; text-align: right; font-size: 10px;"><?php echo $dninfo["trans_amount"]; ?></td>
    </tr>
    <tr><td colspan="5">&nbsp;</td></tr> 
</table> 

<?php
$content = ob_get_contents();
ob_end_clean();

$obj_pdf->setHeaderData($ln = '', $lw = 0, $ht = '', $CustomHeader, $tc = array(0, 0, 0), $lc = array(0, 0, 0));
$obj_pdf->SetHeaderMargin(5);
$obj_pdf->setPrintHeader(true);
$obj_pdf->setFooterData(array(0, 0, 0), array(0, 0, 0), $CustomFooter);
$obj_pdf->SetFooterMargin(20);
$obj_pdf->setPrintFooter(true);
$obj_pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->AddPage(); 
$obj_pdf->writeHTML($content, true, false, true, false, '');
if (ceil($obj_pdf->GetY()) >= 90) {
    $obj_pdf->AddPage();
}

$lastPage = $obj_pdf->getPage();
$obj_pdf->deletePage($lastPage);
$obj_pdf->Output('output.pdf', 'I');








