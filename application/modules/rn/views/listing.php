<section class="content clearfix" id="Return_Note_Coll">
    <?php echo theme_view('general/search_bar_filter'); ?>
    
    <div class="row" id="UpdateCollectionDate_Div">
        <div class="col-xs-12">
            <div class="box box-danger " >
                <div class="box-header hidden">
                    <h3 class="box-title">RN Details</h3>
                </div>
                <div class="box-body"> 
                    <div class="col-sm-5">
                        <div id="debitnote_div">
                            <div class="col-sm-12">
                                <p><span><b>Return Note: <span id="sel_num"></span></b></span></p>
                            </div>
                            <div class="col-sm-6 hidden">
                                <div class="input-group"> 
                                <input placeholder="" type="text" class="form-control" name="debitnote_field" id="debitnote_field" disabled value="" >
                                <span class="input-group-addon"></span>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="col-sm-12">
                            <p><span id="RMAfiles_Resp"><b>Supporting Documents</b></span>&nbsp;</p>
                        </div>
                        <div class="col-sm-12" style="min-height:50px">
                            <div id="myUploadFiles"></div> 
                            <div id="rn_pdf_div" style="float:right"></div>
                        </div>
                    </div> 
                </div> 
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row" id="listing_rn">
        <div class="col-xs-12">
            <div class="box">
                <div class="hidden box-header">
                    <h3 class="box-title">RN Listings</h3>
                </div>
                <div class="box-body">                    
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="no-sort"><span class="fa fa-check-square-o" id="sel_all_btn" title="Select all on this page"></span></th>
                                <th>RN #</th> 
                                <th class="">Submit Date</th>
                                <th class="no-sort">Buyer</th>
                                <th class="no-sort" style="max-width:30%">Remarks<br>Notes to buyer</th>
                                <th>Brand</th>
                                <th>Doc Ref</th>
                                <th>Receipt ID</th>
                                <th class="no-sort">Debit Note</th>
                                <th class="no-sort">Approve By/Date</th>
                                <th class="no-sort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($results): $k = 0; ?>
                                <?php foreach ($results as $result): ?>
                                    <?php
                                    $trans_id = isset($result["trans_id"]) ? trim($result["trans_id"]) : "";
                                    $status_desc = isset($result["status_desc"]) ? $result["status_desc"] : "";
                                    $submit_date = isset($result["submit_date"]) ? $result["submit_date"] : "";
                                    $buyer_id = isset($result["buyer_id"]) ? $result["buyer_id"] : "";
                                    $collection_date = (isset($result["collection_date"]) && strtotime($result["collection_date"]) > 631123200 ) ? $result["collection_date"] : "";
                                    $debit_note = isset($result["debit_note"]) ? $result["debit_note"] : "";
                                    $doc_ref = isset($result["doc_ref"]) ? $result["doc_ref"] : "";
                                    $receipt_id = isset($result["receipt_id"]) ? $result["receipt_id"] : "";
                                    $approve_by = isset($result["approve_by"]) ? $result["approve_by"] : "";
                                    $approve_date = (isset($result["approve_date"]) && strtotime($result["approve_date"]) > 631123200 ) ? $result["approve_date"] : "";
                                    $supp_notes = isset($result["supp_notes"]) ? $result["supp_notes"] : "";
                                    $remarks = isset($result["remarks"]) ? $result["remarks"] : "";
                                    $fullnotes = ($remarks != "") ? '' . $remarks . '<br>' : '';
                                    $fullnotes.= ($supp_notes != "") ? '<i>Notes:</i> ' . $supp_notes . '' : '';
                                    $fullnotes.= ($result["item_remarks"] != "") ? '<!--<br>' . $result["item_remarks"] . '-->' : '';
                                    $item_brand = isset($result["item_brand"]) ? $result["item_brand"] : "";
                                    ?>
                                    <tr>
                                        <td class=""><input type="checkbox" class="cb" id="<?php echo 'cb_' . $k; ?>" name="inv_selected[]" value="<?php echo $trans_id; ?>" ></td>
                                        <td><?php echo $trans_id; ?></td>
                                        <td data-sort="<?php echo strtotime($submit_date); ?>"><span title="Submitted on <?php echo $submit_date; ?>"><?php echo $submit_date; ?></span></td>
                                        <td><?php echo $buyer_id; ?></td>
                                        <td class="h-mobile"><div style="width:300px"><?php echo $fullnotes; ?></div></td>
                                        <td><?php echo $item_brand; ?></td>
                                        <td><?php echo $doc_ref; ?></td>
                                        <td><?php echo $receipt_id; ?></td> 
                                        <td><a href="<?php echo base_url("rn/dn_pdf/".$debit_note) ; ?>" class="popuplink"><?php echo $debit_note; ?></a></td>
                                        <td data-sort="<?php echo strtotime($approve_date); ?>">
                                            <?php echo $approve_by; ?><br>
                                            <?php echo $approve_date; ?>
                                        </td> 
                                        <td>
                                            <input type="hidden" id="Debitnote_<?php echo $trans_id; ?>" value="<?php echo $debit_note; ?>" >
                                            <input type="hidden" id="Note_<?php echo $trans_id; ?>" value="<?php echo $supp_notes; ?>" >
                                            <a class="btn btn-info btn-sm ViewGRNFull" id="<?php echo $trans_id; ?>" id2="<?php echo 'dt_' . $k; ?>" href="#">Items</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $k++;
                                endforeach;
                                ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="listing_rn_div">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">RN Items <span id="rn_no"></span></h3>
                    <span id="ItembtnSpan"></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="datatable_div">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>PO ID</th> 
                                <th>Item ID</th>
                                <th>Item Description</th>
                                <th>GRN Qty</th>
                                <th>Unit Price</th>
                                <th class="no-sort">Disc %</th>
                                <th>Amount</th>
                                <th class="no-sort">Tax Amt</th>
                                <th>Supp DO<br>Recv. Date</th>
                                <th class="no-sort">Tax<br>Curr</th> 
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    
    <span class="pull-right" id="ViewGRNFullCloseSpan"><a href="#" id="ViewGRNFullClose" class="btn btn-default text-bold"><i class="fa fa-arrow-left"></i> Back to Return Notes</a></span>
    
</section>
