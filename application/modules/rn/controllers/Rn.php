<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Rn extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('rn/Rn_model');
        $this->load->model('login/Login_model');
        $this->load->library('session');
        $this->_checkLogged();
    }

    public function collection() {
        $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : date('d-m-Y',strtotime("-1 month"));
        $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : date('d-m-Y');
        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
            $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : '';
            $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : '';
        }
        if (isset($data["search_from"])) {
            $params["search_from"] = $data["search_from"];
        }
        if (isset($data["search_to"])) {
            $params["search_to"] = $data["search_to"];
        }
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        $output = $this->Rn_model->get_all_return_note_list($params);
        //echo "<pre>";print_r($output);exit;
        $data["results"] = $output;
        $data["page_title"] = 'RN Collection Date';
        $data["sidebar"] = 'Return Note';
        Template::set($data);
        Template::render();
    }

    public function buyernotes() {
        $supp_id = $this->session->userdata('cm_supp_id');
        $buyernotes = $this->input->post('buyernotes');
        $rn_no = $this->input->post('rn_no');
        $buyer_id = $this->input->post('buyer_id');
        $loc_id = $this->input->post('loc_id');
        $created_by = $this->input->post('created_by');
        
        if ($buyernotes != "") {
            $params["supp_notes"] = $buyernotes;
            $params["trans_id"] = $rn_no;
            $params["modified_by"] = $this->session->userdata('cm_user_id');
            $params["modified_on"] = date("Y-m-d H:i:s");
            $output = $this->Rn_model->update_return_note_suppnote($params);

            $supp_name = $this->session->userdata('cm_supp_name');
            $supp_email = $this->session->userdata('email_addr3');
            $buyer_email = $this->Login_model->get_sysusr_email($buyer_id);
            $loc_email = ($loc_id != '') ? $this->Login_model->get_sysusr_email($loc_id,'ims_location_list') : '';
            $creator_email = ($created_by != '') ? $this->Login_model->get_sysusr_email($created_by) : '';
            $creator_email = (in_array($creator_email, [$buyer_email,$loc_email])) ? '' : $creator_email;

            $params["to"] = implode_email([$loc_email,$buyer_email]);
            $params["cc"] = implode_email([$supp_email,$creator_email]);
            $params["from"] = $this->session->userdata('email_addr');
            $params["subject"] = "Notes for RN " . $rn_no;
            $params["message"] = $buyernotes." \n\n - ".$supp_name." (".trim($supp_id).")";
            $output = $this->Rn_model->CallSP_email($params);

            //echo '' . $rn_no . ' (' . $buyernotes . '/' . $supp_id . ') <pre>'.print_r($params,true).'</pre>';
            if ($output)
                echo '<span class="text-success">Message sent to buyer.</pre>';
            else
                echo '<span class="text-danger">Fail to deliver message.</span>';
        }
        else {
            echo '<span class="text-danger">No message.</span>';
        }
    }

    public function updatedate() {
        $supp_id = $this->session->userdata('cm_supp_id');
        $new_codate = $this->input->post('new_codate');
        $new_trans_id = $this->input->post('new_trans_id');
        $new_trans_id_str = implode("','", $new_trans_id);

        if (strtotime($new_codate) > time() - 86400) {
            $params["collection_date"] = date("Y-m-d H:i:s", strtotime($new_codate));
            $params["trans_id"] = $new_trans_id_str;
            $params["modified_by"] = $this->session->userdata('cm_user_id');
            $params["modified_on"] = date("Y-m-d H:i:s");
            $output = $this->Rn_model->update_return_note_date($params);

            echo '<span class="text-success">UPDATE DATE ' . $new_codate . '</a> (' . $new_trans_id_str . ')';
        } else {
            echo '<span class="text-danger">Invalid date.</span>';
        }
    }

    public function get_rn_files($rn) {

        $directory = FILE_SERVER . 'pdf/RN/';
        $supp_id = $this->session->userdata('cm_supp_id');

        $doc_date = $this->Rn_model->get_rn_collectdate($rn);
        $file_prefixdate = (strtotime($doc_date[0]['doc_date'])>=START_PREFIXDATE) ? date(SUPP_INV_PREFIXDATE, strtotime($doc_date[0]['doc_date'])) : '';
        $file_prefix = $file_prefixdate.$rn;
//        foreach (glob("$directory*$file_prefix*.*") as $key => $file) {
//            $uploadedfiles[] = basename($file);
//        }
        if (file_exists($directory . $file_prefix . '.pdf'))
            $uploadedfiles[] = array("RN", $file_prefix . '.pdf' );
        if (file_exists($directory .'RCPT/Rcpt_'. $file_prefix . '.pdf'))
            $uploadedfiles[] = array("RCPT", 'Rcpt_'.$file_prefix . '.pdf' );
        if (file_exists($directory .'RCPT/Receipt_'. $file_prefix . '.pdf'))
            $uploadedfiles[] = array("RCPT", 'Receipt_'.$file_prefix . '.pdf' );
        if (file_exists($directory .'SVC/Svc_'. $file_prefix . '.pdf'))
            $uploadedfiles[] = array("SVC", 'Svc_'.$file_prefix . '.pdf' );
        if (file_exists($directory .'RMA/Rma_'. $file_prefix . '.pdf'))
            $uploadedfiles[] = array("RMA", 'Rma_'.$file_prefix . '.pdf' );
        if (file_exists($directory .'EMAIL/Email_'. $file_prefix . '.pdf'))
            $uploadedfiles[] = array("EMAIL", 'Email_'.$file_prefix . '.pdf' );
        //echo "<pre>"; print_r($uploadedfiles); echo "</pre>"; exit;

        if (count($uploadedfiles) > 0) {

            foreach ($uploadedfiles as $file) {
                $filename = $file[1];
                $filetype = $file[0];
                //if (strpos('-' . $filename, $file_prefix)) {
                    $filetypepath = ($filetype!='RN') ? $filetype.'/' : '' ;
                    echo '<div style="float:left;text-align:center;padding:0px 15px"><a title="' . $filename . '" href="' . base_url("login/displayfile/pdf/" . base64_encode($directory . $filetypepath . $filename)) . '" class="popuplink"><span class="fa-2x fa fa-file-pdf-o"></a><br>' . $filetype . '</div>';
                    echo '<input type="hidden" name="uploads[]" value="' . $filename . '">';
                //}
            }
        } else {
            echo '<div style="float:left;text-align:center;padding:0px 15px"><a title="#" class="popuplink"><span class="fa-2x fa fa-file-pdf-o" style="color:#BBB"></a><br>NO FILE</div>';
        }
    }

    public function listing() {
        set_time_limit(180);

        $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : date('d-m-Y',strtotime("-1 month"));
        $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : date('d-m-Y');
        if ($this->input->get("search")) {
            $params["search"] = $this->input->get("search");
            $params["search_from"] = $data["search_from"] = ($this->input->get("search_from")) ? $this->input->get("search_from") : '';
            $params["search_to"] = $data["search_to"] = ($this->input->get("search_to")) ? $this->input->get("search_to") : '';
        }
        if (isset($data["search_from"])) {
            $params["search_from"] = $data["search_from"];
        }
        if (isset($data["search_to"])) {
            $params["search_to"] = $data["search_to"];
        }
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        $params["status_level"] = 2;
        $output = $this->Rn_model->get_all_return_note_list($params);
        //echo "<pre>";print_r($output);exit;
        $data["results"] = $output;
        $data["page_title"] = 'RN Info';
        $data["sidebar"] = 'Return Note';
        Template::set($data);
        Template::render();
    }

    public function items($trans_id = NULL) {
        $params["supp_id"] = $this->session->userdata('cm_supp_id');
        $params["trans_id"] = isset($trans_id) ? $trans_id : NULL;
        $output = $this->Rn_model->get_all_return_note_items($params); 
        $data["results"] = $output;
        $data["page_title"] = 'Return Note';
        $data["sidebar"] = 'Return Note';

        $this->load->view('items', $data);
    }

    public function rn_export($rntype='rn',$rn_no) {
        ini_set('max_execution_time',300);
        
        $ids = isset($rn_no) ? explode(',',$rn_no) : ''; 
        $supp_id = $this->session->userdata('cm_supp_id');
        
        $ctl_addr = $this->Login_model->get_coy_address("CTL","CTL","full");
        $rninfo_supp_add = $this->Login_model->get_coy_address( $supp_id , "CTL" , "address", "SUPPLIER");
                
        foreach ($ids as $id) {
            if ($id!="") {
                $rn_no = $id;
                $params["search"] = isset($rn_no) ? $rn_no : NULL;
                $params["status_level"] = '0,1,2';
                $params["supp_id"] = $supp_id;
                $rninfo = $this->Rn_model->get_all_return_note_list($params); 
                $dataset["rninfo"] = $rninfo[0];

                $params_i["supp_id"] = $this->session->userdata('cm_supp_id');
                $params_i["trans_id"] = isset($rn_no) ? $rn_no : NULL;
                $items = $this->Rn_model->get_all_return_note_items($params_i);  
                $dataset["items"] = $items;
                
                $dataset["rninfo_supp_add"] = ($dataset["rninfo"]["supp_id"]!=$supp_id) ? $this->Login_model->get_coy_address( $dataset["rninfo"]["supp_id"] , $dataset["rninfo"]["coy_id"] , "address", "SUPPLIER") : $rninfo_supp_add;
                $dataset["rninfo_coll_add"] = $this->Login_model->get_coy_address( $dataset["rninfo"]["loc_id"] , $dataset["rninfo"]["coy_id"] , "address");
                $dataset["ctl_addr"] = $ctl_addr;
                
                $batchinfo[] = $dataset;
            }
        }
        
        if ($this->input->get("debug")==1) {
            print_r($ids);
            print_r($batchinfo);
            exit;
        }
        
        if ($rntype=='csv') {
            // Prepares CSV file for user to download on request 
            $handle_path = FCPATH . 'assets/uploads/';
            $uploadedcsv = $params["supp_id"] . '_rnexport.csv';
            $fp = fopen($handle_path . $uploadedcsv, 'w');
            fputcsv($fp, array('RN No','RN Date','Collection Date','Doc Ref','Remarks','Loc ID','Item Line','Item ID','Description','Qty Order','Unit Price','Remarks')); 

            foreach ($batchinfo as $result) {
                foreach ($result["items"] as $result_lines) {
                        $a = array();
                        $a[] = $result["rninfo"]["trans_id"];
                        $a[] = $result["rninfo"]["submit_date"];
                        $a[] = $result["rninfo"]["collection_date"];
                        $a[] = $result["rninfo"]["doc_ref"];
                        $a[] = $result["rninfo"]["remarks"];
                        $a[] = $result["rninfo"]["loc_id"];
                        $a[] = $result_lines["line_num"];
                        $a[] = $result_lines["item_id"];
                        $a[] = $result_lines["item_desc"];
                        $a[] = display_number($result_lines["trans_qty"]);
                        $a[] = $result_lines["unit_price"];
                        $a[] = $result_lines["remarks"];
                        fputcsv($fp, $a);
                }
            }
            fclose($fp);

            if (file_exists($handle_path . $uploadedcsv)) {
                redirect(base_url("assets/uploads/".$uploadedcsv));
            }
        }
        else {
            $this->load->helper('pdf_helper');
            $data["batchinfo"] = $batchinfo;
            $data["rntype"] = $rntype;
            $data['po_output'] = "";
            $data['pdf_title'] = "RN";
            $data['email_view'] = "pdf"; 
            $this->load->view('rn/rn_pdf', $data);
        }
        
    }

    public function dn_pdf($dn_no) {
        ini_set('max_execution_time',300);
        
        $params["search"] = isset($dn_no) ? $dn_no : NULL;
        //$params["supp_id"] = $this->session->userdata('cm_supp_id');
        $dninfo = $this->Rn_model->get_debit_note($params); 
        $data["dninfo"] = (array) $dninfo;
        
        $data["ctl_addr"] = $this->Login_model->get_coy_address("CTL","CTL","full");
        
        $this->load->helper('pdf_helper'); 
        $this->load->view('rn/dn_pdf', $data);
    }

}
