<?php

/* YS
 * 19-12-2017
 */
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Cherps extends Front_Controller {

    private $allowed_pages;

    public function __construct() {
        parent::__construct();

        //ini_set('display_errors', 1);
        $this->load->model('cherps/Cherps_model');
        $this->load->model('cherps/Pogrn_model');
        $this->load->model('po2/Po2_model');
        $this->load->model('login/Login_model');

        $this->allowed_pages = ['mis','pos','page','invdocdate','document_upload', 
        'userAccess', 'sendEmail', 'searchStaff', 'changeLocation', 'setNewStaff', 
        'setResignStaff','sendSMS','setNewPwd', 'requestAccess', 'revokeAccess', 'setAccess'];
    }

    public function index($code){
        $list = json_decode(base64_decode(urldecode($code)) ,true);

        if (!is_array($list)) {

            if (in_array($code,$this->allowed_pages)){
                $this->$code();
                return;
            }

            // o2o_url_key
            $keys = $this->Cherps_model->get_url_key($code);
            if ($keys['ref_type']=='VRC')
                $url = base_url('vrc/export/pdf/'.base64_encode(trim($keys['ref_id'])).'/'.urlencode(trim($keys['supp_id'])).'?cherps='.time());

            redirect($url);
        }

        if (ENVIRONMENT!="production" && $list['t']<=(time()-604800)){
            // Test environment, url expired. Try new URL
            echo 'Link has expired.<br>Please use new link: ' . $this->Cherps_model->build_url($list['m'], $list['u'], $list['i'], $list['s']);
            return;
        }

        if ($list['m']=='popdf' && $list['t']>(time()-604800)) {
            $url = base_url('po2/export/pdf/'.trim($list['i']).'/'.urlencode(trim($list['u'])).'?cherps='.time());
            redirect($url);
        }

        $otp_check = true; //$this->otp( $list['u'], trim($list['i']) );

        if ($otp_check && $list['m']=='po') {
            $po = $this->Cherps_model->get_po_list(trim($list['i']));
            if ($po && $po['status_level']==$list['s']) {
                $this->po(trim($po['po_id']), trim($po['supp_id']), $list['u']);
            }
            else if ($po && $po['status_level']>0) {
                // Show approved page
                $data['subject'] = 'PO Approved';
                $data['message'] = 'PO '.trim($po['po_id']).' has been approved by<br>'.trim($po['approve_by_name']).' on '.date('d M Y', strtotime($po['approve_date'])).'.';
                $this->page($data);
                return;
            }
            else {
                // Show end page
                $this->page();
                return;
            }
        }
        else if ($otp_check && $list['m']=='pr') {
            $pr = $this->Cherps_model->get_pr_list(trim($list['i']));
            if ($pr && $pr['status_level']==$list['s']) {
                $this->pr(trim($pr['pr_id']), trim($pr['supp_id']), $list['u']);
            }
            else if ($pr && $pr['status_level']>0) {
                // Show approved page
                $data['subject'] = 'PR Approved';
                $data['message'] = 'PR '.trim($pr['pr_id']).' has been approved by<br>'.trim($pr['approve_by_name']).' on '.date('d M Y', strtotime($pr['approve_date'])).'.';
                $this->page($data);
                return;
            }
            else {
                // Show end page
                $this->page();
                return;
            }
        }
    }

    private function page($data=[]){

        $view = $this->input->get('view');
        if ($view == 'capitastar') {

            if ($this->input->post('save')) {
                $usr_id = $this->input->post('cherps_usr');
                $usr_pwd = $this->input->post('cherps_pwd');
                $usr_login = $this->Cherps_model->usr_login($usr_id,$usr_pwd);
                if ($usr_login){
                    $cpls['email'] = $this->input->post('cpls_usr');
                    $cpls['password'] = $this->input->post('cpls_pwd');
                    $params['sys_id'] = 'API';
                    $params['set_code'] = 'CAPITASTAR_ACCT';
                    $params['set_default'] = strtoupper($this->input->post('cherps_locid'));
                    $params['remarks'] = base64_encode(json_encode($cpls));
                    $params['created_by'] = $usr_id;
                    $this->Cherps_model->upd_b2b_setting($params);
                    $data['message'] = 'Information saved successfully.';
                }
                else {
                    $data['message'] = 'Fail to save information. Please try again.';
                }
            }

            Template::set_theme('cherps_mobile');
            Template::set_view('b2b_capitastar');
            Template::set($data);
            Template::render();
            return;
        }

        if ($view== 'hachi_pocinvoicefix') {

            if ($this->input->post('save')) {
                $usr_id = $this->input->post('cherps_usr');
                $usr_pwd = $this->input->post('cherps_pwd');
                $usr_login = $this->Cherps_model->usr_login($usr_id,$usr_pwd);
                if ($usr_login){
                    $cpls['invoice_id'] = strtoupper($this->input->post('invoice_id'));
                    $cpls['pay_mode'] = $this->input->post('pay_mode');
                    $cpls['trans_amount'] = $this->input->post('trans_amount');
                    $cpls['trans_ref'] = $this->input->post('trans_ref');
                    $url = $this->Cherps_model->upd_hachi_pay($cpls,$usr_id);
                    $data['message'] = 'Invoice Updated - <a href="'.$url.'" target="_blank">'.$url.'</a>';
                }
                else {
                    $data['message'] = 'Fail to save information. Please try again.';
                }
            }

            Template::set_theme('cherps_mobile');
            Template::set_view('hachi_pocinvoicefix');
            Template::set($data);
            Template::render();
            return;
        }

        if ($view== 'cb_parcel') {

            if ($this->input->post('save')) {
                $usr_login = $this->Cherps_model->cb_parcel($this->input->post('invoice_id'));
                $data['message'] = $usr_login;
            }

            Template::set_theme('cherps_mobile');
            Template::set_view('cb_parcel');
            Template::set($data);
            Template::render();
            return;
        }

        if ($view== 'cb_rerouting') {

            if ($this->input->post('save')) {

                $ok = $this->Cherps_model->cb_rerouting($this->input->post('invoice_id'), $this->input->post('line_num'), $this->input->post('loc_id'));
                if ($ok)
                    $data['message'] = 'Info saved. Please wait half hour for Transfer to be generated.';
                else
                    $data['message'] = 'Fail!! Please contact Software for manual routing.';
            }

            Template::set_theme('cherps_mobile');
            Template::set_view('cb_rerouting');
            Template::set($data);
            Template::render();
            return;
        }

        if ($view== 'cb_logistics') {

            if ($this->input->post('saveassign')) {
                $cb = $this->Cherps_model->cb_logistics();
                $data['message'] = $cb;
            }

            if ($this->input->post('saveoptimo')) {
                $this->load->model('api/Optimoroute_model');
                $date = date('Y-m-d', strtotime('tomorrow'));
                $cb = $this->Optimoroute_model->auto($date);
                $data['message'] = $cb;
            }

            if ($this->input->post('opendo')) {
                $id = $_POST['opendo_id'];
                $k = $this->Cherps_model->get_document('hachi-do',$id,'NID','HCL');
                redirect($k);
                
                $cb = $this->Cherps_model->cb_logistics_getrefkey($id);
                if ($cb)
                    redirect('http://chvoices.challenger.sg/cherps/invoice_redirect/hachi-do/'. trim($_POST['opendo_id']) .'/'. $cb[0]['delv_mode_id'] .'/HCL');
                else
                    redirect('https://www.hachi.tech/hachi-delivery-order-url/invoice/'. $id .'/NID/HCL');
            }

            Template::set_theme('cherps_mobile');
            Template::set_view('cb_logistics');
            Template::set($data);
            Template::render();
            return;
        }

        $data['layout'] = 'general';
        Template::set_theme('cherps_mobile');
        Template::set_view('general');
        Template::set($data);
        Template::render();

    }

    private function po($po_id,$supp_id,$usr_id) {

        if ( $this->input->post('action')=='REJECT' || $this->input->post('action')=='APPROVE' ) {

            $prm['po_id'] = $po_id;
            $prm['current_rev'] = $this->input->post('current_rev');
            $prm['approve_by'] = $usr_id;
            $prm['status_level'] = ($this->input->post('action')=='APPROVE') ? 1 : 0 ;
            $rtn = $this->Cherps_model->po_set_status($prm);
            if ($rtn) {

                $po = $this->Cherps_model->get_po_list(trim($prm['po_id']));
                $po_id_rev = ($po['current_rev']>0) ? $po_id.'R'.$po['current_rev'] : $po_id;
                // Prepares email addresses
                if (ENVIRONMENT!="production") {
                    $po['created_by'] = 'yongsheng@challenger.sg';
                    $po['buyer_id'] = '';
                    $po['requester_id'] = '';
                }
                $email = filter_var($po['buyer_id'], FILTER_VALIDATE_EMAIL) ? str_replace(';','',$po['buyer_id']).'; ' : '';
                $email.= filter_var($po['requester_id'], FILTER_VALIDATE_EMAIL) ? str_replace(';','',$po['requester_id']).'; ' : '';
                $email.= filter_var($po['created_by'], FILTER_VALIDATE_EMAIL) ? str_replace(';','',$po['created_by']).'; ' : '';

                if ($this->input->post('action')=='APPROVE') {
                    $data['subject'] = 'PO update successful';
                    $data['message'] = 'PO No. ' . $po_id_rev . ' has been successfully approved.';

                    $path = '';
                    $subj = 'Approved Purchase Order: '.$po_id_rev.' for (' . trim($po['supp_id']) . ')';
                    $msg = "Purchase Order: ".$po_id_rev." to: ".$po['supp_name']."\n\nAPPROVED\nby ".$po['approve_by_name']."";
                    $msg.= "\n\nDownload PDF PO (valid for 48 hrs):\n";
                    $msg.= $this->Cherps_model->build_url('popdf', trim($po['supp_id']), trim($po['po_id']),1);

                    // Attempt send via API if needed
                    $sendapi = $this->Pogrn_model->transmitPoVendor($po_id, array("supp_id"=>$supp_id) );
                }
                else {
                    $data['subject'] = 'PO update successful';
                    $data['message'] = 'PO No. ' . $po_id_rev . ' has been successfully rejected.';

                    $path = '';
                    $subj = 'Rejected Purchase Order: '.$po_id_rev.' for (' . trim($po['supp_id']) . ')';
                    $msg = "Purchase Order: ".$po_id_rev." to: ".$po['supp_name']."\n\nREJECTED\nby ".$po['approve_by_name']."";
                    $msg.= "\n\nReason:\n";
                    $msg.= $this->input->post('reject_reason');
                }

                // Trigger email
                $email = ($email=='') ? 'ys.challenger@gmail.com' : $email;
                $data['email'] = $this->Cherps_model->email_po($po_id, $email, $subj, $msg, $path, $usr_id);

            }
            else {
                $data['subject'] = 'Please contact administrator';
                $data['message'] = 'We encounter an error when trying to update PO '.$prm['po_id'].' to status '. $prm['status_level'].'.';
            }

            // Show end page
            $this->page($data);

        }
        else {

            // Retreive PO
            $params["ids"] = array($po_id);
            $params["supp_id"] = 'CHERPS';
            $poinfo = $this->Po2_model->pdf_info($params);

            $data['po'] = $poinfo;
            $data['usr_id'] = $usr_id;

            $data['usr_limit'] = $this->Cherps_model->get_pms_limit($usr_id);

            $data["page_title"] = 'Approve PO';
            Template::set_theme('cherps_mobile');
            Template::set_view('quickview_po');
            Template::set($data);
            Template::render();

        }
    }

    private function pr($id,$supp_id,$usr_id) {

        if ( $this->input->post('action')=='REJECT' || $this->input->post('action')=='APPROVE' ) {

            $prm['pr_id'] = $id;
            $prm['current_rev'] = $this->input->post('current_rev');
            $prm['approve_by'] = $usr_id;
            $prm['status_level'] = ($this->input->post('action')=='APPROVE') ? 1 : 0 ;
            $rtn = $this->Cherps_model->pr_set_status($prm);
            if ($rtn) {

                $po = $this->Cherps_model->get_pr_list(trim($prm['pr_id']));
                $po_id_rev = ($po['current_rev']>0) ? $id.'R'.$po['current_rev'] : $id;
                // Prepares email addresses
                if (ENVIRONMENT!="production") {
                    $po['created_by'] = 'yongsheng@challenger.sg';
                    $po['buyer_id'] = '';
                    $po['requester_id'] = '';
                }
                $email = filter_var($po['buyer_id'], FILTER_VALIDATE_EMAIL) ? str_replace(';','',$po['buyer_id']).'; ' : '';
                $email.= filter_var($po['requester_id'], FILTER_VALIDATE_EMAIL) ? str_replace(';','',$po['requester_id']).'; ' : '';
                $email.= filter_var($po['created_by'], FILTER_VALIDATE_EMAIL) ? str_replace(';','',$po['created_by']).'; ' : '';

                if ($this->input->post('action')=='APPROVE') {
                    $data['subject'] = 'PR update successful';
                    $data['message'] = 'PR No. ' . $po_id_rev . ' has been successfully approved.';

                    $path = '';
                    $subj = 'Approved PR: '.$po_id_rev.' for (' . trim($po['supp_id']) . ')';
                    $msg = "Purchase Request: ".$po_id_rev." to: ".$po['supp_name']."\n\nAPPROVED\nby ".$po['approve_by_name']."";
                    //$msg.= "\n\nDownload PDF PO (valid for 48 hrs):\n";
                    //$msg.= $this->Cherps_model->build_url('popdf', trim($po['supp_id']), trim($po['po_id']),1);
                }
                else {
                    $data['subject'] = 'PR update successful';
                    $data['message'] = 'PR No. ' . $po_id_rev . ' has been rejected.';

                    $path = '';
                    $subj = 'Rejected PR: '.$po_id_rev.' for (' . trim($po['supp_id']) . ')';
                    $msg = "Purchase Request: ".$po_id_rev." to: ".$po['supp_name']."\n\nREJECTED\nby ".$po['approve_by_name']."";
                    $msg.= "\n\nReason:\n";
                    $msg.= $this->input->post('reject_reason');
                }

                // Trigger email
                $email = ($email=='') ? 'ys.challenger@gmail.com' : $email;
                $data['email'] = $this->Cherps_model->email_po($id, $email, $subj, $msg, $path, $usr_id);

            }
            else {
                $data['subject'] = 'Please contact administrator';
                $data['message'] = 'We encounter an error when trying to update PR '.$prm['po_id'].' to status '. $prm['status_level'].'.';
            }

            // Show end page
            $this->page($data);

        }
        else {

            // Retreive PR
            $params["ids"] = array($id);
            $params["supp_id"] = 'CHERPS';
            $prinfo = $this->Po2_model->pr_pdf_info($params);

            $data['pr'] = $prinfo;
            $data['usr_id'] = $usr_id;
            //$data['usr_limit'] = $this->Cherps_model->get_pms_limit($usr_id);

            $data["page_title"] = 'Approve PR';
            Template::set_theme('cherps_mobile');
            Template::set_view('quickview_pr');
            Template::set($data);
            Template::render();

        }
    }

    // Update fin_journal doc_date & filename
    public function invdocdate(){

        if (!challengerIpAddr()) {
            $msg = 'Unauthorized access to ' . base_url('cherps/invdocdate') . ' from ' . getRealIpAddr(15);
            $call = $this->Api_model->cherps_email($msg, 'Unauthorized access to fx cherps/invdocdate', 'yongsheng@challenger.sg');

            $result= array(
                "code" => '-1',
                "messages" => 'Unauthorized access location. Please make sure you are at a whitelisted IP location.'
            );
            print_r($data = json_encode($result));
            return;
        }

        $this->load->model('invoice/Invoice_model');

        $steps = array();

        $directory =  FILE_SERVER . '/pdf/SUPP_INV/';
        $supp_id = $this->input->get('supp_id'); //$supp_id = $this->session->userdata('cm_supp_id');
        $inv = $this->input->get('doc_ref');
        $new_date = $this->input->get('doc_date');

        if (!$supp_id || !$inv || !$new_date) {
            if ($this->input->get('reply')=="T") {
                echo '<FORM action=""><table>
                        <tr><td></td><td><select name="module"><option value="SUPP_INV">Supplier Invoice</option><option value="VRC">VRC</option></td></tr>
                        <tr><td>Supp ID:</td><td><input name="supp_id"></td></tr>
                        <tr><td>Doc Ref:</td><td><input name="doc_ref"></td></tr>
                        <tr><td>New Date:</td><td><input name="doc_date" placeholder="YYYY-MM-DD"></td></tr>
                        <tr><td></td><td><input type="submit" value="Submit"><input type="hidden" name="reply" value="T"></td></tr>
                      </table></FORM>';
                return;
            }
            else {
                $result= array(
                    "code" => '-1',
                    "messages" => 'Missing parameters.'
                );
                print_r($data = json_encode($result));
                return;
            }
        }

        $old_date = $this->Invoice_model->get_pi_docdate($inv,$supp_id);
        $oldfile_prefixdate = (strtotime($old_date[0]['doc_date'])>=START_PREFIXDATE) ? date(SUPP_INV_PREFIXDATE, strtotime($old_date[0]['doc_date'])) : '';
        $oldfile_prefix = $oldfile_prefixdate.$supp_id.'_'.$inv;

        if (count($old_date)==1 && strtotime($old_date[0]['doc_date'])>946616400) {
            $steps[] = 'Found 1 submitted invoice '. $inv . ' for ' . $supp_id;

            $newfile_prefixdate = date(SUPP_INV_PREFIXDATE, strtotime($new_date));
            $newfile_prefix = $newfile_prefixdate . $supp_id . '_' . $inv;

            $ext = '.pdf';
            if (file_exists($directory . $oldfile_prefix . $ext)) {
                $old_path = $directory . $oldfile_prefix . $ext;
                $new_path = $directory . $newfile_prefix . $ext;
                exec( "sudo mv ".$old_path." ".$new_path . ' 2>&1', $output);
                $steps[] = "Renamed document ".$newfile_prefix.$ext."";
            }
            $ext = '-INV.pdf';
            if (file_exists($directory . $oldfile_prefix . $ext)) {
                $old_path = $directory . $oldfile_prefix . $ext;
                $new_path = $directory . $newfile_prefix . $ext;
                exec( "sudo mv ".$old_path." ".$new_path . ' 2>&1', $output);
                $steps[] = "Renamed document ".$newfile_prefix.$ext."";
            }
            $ext = '-DO.pdf';
            if (file_exists($directory . $oldfile_prefix . $ext)) {
                $old_path = $directory . $oldfile_prefix . $ext;
                $new_path = $directory . $newfile_prefix . $ext;
                exec( "sudo mv ".$old_path." ".$new_path . ' 2>&1', $output);
                $steps[] = "Renamed document ".$newfile_prefix.$ext."";
            }

            $new_date_sql = date("Y-m-d H:i:s", strtotime($new_date));
            $sql = $this->Invoice_model->upd_pi_docdate($old_date[0]['trans_id'], $new_date_sql);
            if ($sql) {
                $steps[] = "Updated date for document ".$old_date[0]['trans_id']." ";
            }

        }
        else {
            $steps[] = 'No submitted invoice '. $inv . ' for ' . $supp_id;
        }

        if ($this->input->get('reply')=="T") {
            foreach ($steps as $step) {
                echo '- ' . $step . '<br>';
            }
        }
        else {
            $result= array(
                "code" => (count($steps)>1) ? 1 : 0,
                "steps" => $steps
            );
            print_r($data = json_encode($result));
        }
        return;
    }

    // API use by CHERPS 2.0
    public function invoice_redirect($doc,$id,$v1='',$v2=''){
        $this->load->model('cherps/Cherps_model');
        $url = $this->Cherps_model->get_document($doc,$id,$v1,$v2);
        redirect($url);
    }

    // API for web services to upload Doc
    public function document_upload(){

        if ($this->input->get_request_header('X_AUTHORIZATION') != 'A01BC7A80FF27ADB1D181A026A3814FE'){
            echo json_encode(["code" => 0,"msg" => "Unauthorized access"]);
            return;
        }

        $this->load->model('api/Dfs_model');

        $json = file_get_contents('php://input');
        $data = json_decode($json, true);
        $uploadpath = $data['uploadpath'];
        $filename = $data['filename'];
        $content = base64_decode($data['content']);

        $result = $this->Dfs_model->createAndUpload($uploadpath,$filename,$content);
        echo json_encode($result);

    }

    public function otp($usr_id='',$id='') {

        $msg='';

        if ($this->input->post('submit_otp')) {
            // Check OTP
            $usr_id = $this->input->post('usr_id');
            $id = $this->input->post('id');
            $params['usr_id'] = $usr_id;
            $params['po_id'] = $id;
            $params['otp'] = $this->input->post('cherps_otp');
            $result = $this->Cherps_model->otp_check($params);
            if ($result){
                $this->session->set_userdata('otp_approval', [$usr_id,time()] );
            }
            redirect($this->input->post('curr_url'));
        }
        if ($this->input->post('send_new_otp')) {
            // Send OTP
            $usr_id = $this->input->post('usr_id');
            $id = $this->input->post('id');
            $params['usr_id'] = $usr_id;
            $params['po_id'] = $id;
            $result = $this->Cherps_model->otp_send($params);
            if ($result)
                echo json_encode(array('code'=>'1','msg'=>'An OTP has been sent to your device.'));
            else
                echo json_encode(array('code'=>'0','msg'=>'Fail OTP.'));

            redirect($this->input->post('curr_url'));
        }

        $otp = $this->session->userdata('otp_approval');
        if ( isset($otp) && $otp[0]==$usr_id && $otp[1]>(time()-600) ) {
            return true;
        }
        else {

            if (!($this->session->userdata('otp_memory'))){
                // New session, send OTP
                $params['usr_id'] = $usr_id;
                $params['po_id'] = $id;
                $result = $this->Cherps_model->otp_send($params);
                if ($result)
                    $msg = 'An OTP has been sent to your device.';
            }

            $data['msg'] = $msg;
            $data['curr_url'] = current_full_url();
            $data['usr_id'] = $usr_id;
            $data['id'] = $id;
            $data['layout'] = 'general';
            Template::set_theme('cherps_mobile');
            Template::set_view('otp');
            Template::set($data);
            Template::render();

            return false;
        }

    }

    // To manage users (not used)
    public function mis(){ // User mgmt

        $adm_id = $this->input->post('adm_id');
        if ($adm_id) {
            if ($this->Cherps_model->usr_verifyadmin($adm_id)) {
                $adm_id = $this->input->post('adm_id');
                $this->session->set_userdata('otp_loginid', $adm_id);
            }
        }

        if (!($this->session->userdata('otp_loginid'))) {
            $data['subject'] = 'MIS Login';
            $data['message'] = 'Use your User ID to login <br><br>';
            $data['message'].= '<input type="text" name="adm_id" value="" size="6" style="background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:18px;height:48px;width:80%;text-align:center;letter-spacing:5px;" onfocus="this.select();">';
            $data['formurl'] = base_url('cherps/mis');
            $this->page($data);
            return;
        }

        $adm_id = $this->session->userdata('otp_loginid');
        $otp_check = true;// $this->otp($adm_id,'MIS-'.rand(1111,9999) );

        if ($otp_check) {
            if ($this->Cherps_model->usr_verifyadmin($adm_id)) {
                $usr_id = $this->input->get('usr');
                $coy_id = ($this->input->get('coy')) ? $this->input->get('coy') : 'CTL';
                $data['usr_id'] = $usr_id;
                $data['coy_id'] = $coy_id;
                $data['adm_id'] = $adm_id;

                $data["page_title"] = 'User Administration';
                Template::set_theme('cherps_mobile');
                Template::set_view('user');
                Template::set($data);
                Template::render();
            }
            else {
                $data['subject'] = 'MIS Login';
                $data['message'] = 'The User is not authorized MIS Admin Staff.';
                $this->page($data);
                return;
            }
        }

    }

    // To manage POS trans (not used)
    public function pos(){ // POS tx mgmt

        if ($this->input->post('adm_id')) {
            $adm_id = $this->input->post('adm_id');
            if ($this->Cherps_model->usr_verifyadmin($adm_id)) {
                $this->session->set_userdata('otp_loginid', $adm_id);
            }
        }

        if (!($this->session->userdata('otp_loginid'))) {
            $data['subject'] = 'MIS Login';
            $data['message'] = 'Use your User ID to login <br><br>';
            $data['message'].= '<input type="text" name="adm_id" value="" size="6" style="background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:18px;height:48px;width:80%;text-align:center;letter-spacing:5px;" onfocus="this.select();">';
            $data['formurl'] = base_url('cherps/pos');
            $this->page($data);
            return;
        }

        $adm_id = $this->session->userdata('otp_loginid');
        $otp_check = true;// $this->otp($adm_id,'MIS-'.rand(1111,9999) );

        if ($otp_check) {
            if ($this->Cherps_model->usr_verifyadmin($adm_id)) {

                $trans_id = $this->input->get('trans_id');

                if ($this->input->get('upd')=='cash-alipay'){
                    $upd_id = $this->input->get('id');
                    $transinfo = $this->Cherps_model->upd_pos_trans('cash-alipay',$trans_id,$upd_id);
                }

                $transinfo = $this->Cherps_model->get_pos_trans('summary',$trans_id);
                $data['trans'] = $transinfo['trans'];
                $data['pay'] = $transinfo['pay'];

                $data["page_title"] = 'POS Transaction Checker';
                Template::set_theme('cherps_mobile');
                Template::set_view('pos');
                Template::set($data);
                Template::render();
            }
        }

    }

    /*
     * Depreciated (i think?)
     */

    public function approval_inv_transaction($id)
    {
        $check_auth = $this->_check_auth();
        if ($check_auth['status'] === 'error'){
            return $this->_errorWithCodeAndInfo('500', $check_auth['message']);
        }
        if (is_null($id) || empty($id)) {
            return $this->_errorWithInfo("Invoice ID required.");
        }
        $result = $this->Cherps_model->approval_inv_transaction($id);
        $result = json_decode($result);
        if ($result->status_code == 200) {
            return $this->_successWithInfo($result->info);
        } else {
            return $this->_errorWithInfo($result->message);
        }
    }

    public function _check_auth()
    {
        $key_code = 'Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjEAA';
        if (empty($this->input->get()['code']) && empty($this->input->request_headers()['Authorization'])) {
            return array('status'=>'error','message'=>'Auth code is empty');
        } else {
            if (!empty($this->input->request_headers()['Authorization'])) {
                $jwt = explode(" ", $this->input->request_headers()['Authorization']);
                if ($jwt[0] !== 'Bearer' || $jwt[1] !== $key_code){
                    return array('status'=>'error','message'=>'Auth header invalid');
                }
            }
            else if (!empty($this->input->get()['code']) && $this->input->get()['code'] !== $key_code) {
                return array('status'=>'error','message'=>'Key code invalid');
            }
        }
        return array('status'=>'success');
    }

    public function _errorWithInfo($info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(array(
                'status' => 'error',
                'status_code' => 404,
                'message' => $info
            )));
    }

    public function _errorWithCodeAndInfo($code, $info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output(json_encode(array(
                'status' => 'error',
                'status_code' => $code,
                'message' => $info
            )));
    }

    public function _successWithInfo($info)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                'info' => $info,
                'status' => 'success',
                'status_code' => 200
            )));
    }

    public function _successWithData($data)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                'data' => $data,
                'status' => 'success',
                'status_code' => 200
            )));
    }

    public function _validateError($errors)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(422)
            ->set_output(json_encode(array(
                'status' => 'validate error',
                'status_code' => 422,
                'errors' => $errors
            )));
    }

    //user access function
    public function userAccess(){
        $this->load->model('Sss_model');
        $this->Sss_model->setStaffId();
        $staff_profile = $this->_getStaffId();
        if($staff_profile['errMsg'] == NULL){
            $this->load->view('user_access_header', $staff_profile);
            $this->load->view('user_access');
        }else{
            echo "
            <meta charset = 'utf-8'> 
             <meta name='viewport' content='width=device-width, initial-scale=1.0'>
             <title>User Not Found</title> 
             <div style='height:300px;'></div>
             <div style='text-align:center;'>".$staff_profile['errMsg'].
             "<h2>oops!</h2><h2>Looks like you are not allowed to enter.</h2>
             <h2>Try refresh?</h2></div>
             ";
        }
    }

    public function sendEmail(){
        defined('BASEPATH') OR exit('No direct script access allowed');
        $this->load->helper('url');

        if($this->input->post() != NULL){
            $postData = $this->input->post("email");
            $this->load->model('Email');
            $this->Email->sendEmail($postData, "");
        }
    }

    public function sendSMS(){
        defined('BASEPATH') OR exit('No direct script access allowed');
        $this->load->helper('url');

        // $postData = $this->input->post("email");
        // $recipient = "+65".$postData;
        $recipient = "+6591731528";
        $this->load->model('Email');
        $this->Email->sendSMS($recipient);

    }

    public function searchStaff(){
        defined('BASEPATH') OR exit('No direct script access allowed');
        $this->load->model('Sss_model');
        $this->load->helper('url');

        if($this->input->post() != NULL){
            $usr_id = $this->input->post("usr_id");
            $user = $this->Sss_model->getUser($usr_id);
            $user['type'] = $this->input->post("type");
            $staff_profile['userExisted'] = $user;
            $this->load->view('user_access_header', $this->_getStaffId());
            $this->load->view('user_access', $staff_profile);
        }
    }

    public function changeLocation(){
        $this->load->model('Sss_model');
        $this->load->helper('url');

        if($this->input->post() != NULL){
            $postData["loc_id"] = $this->input->post("loc_id");
            $postData["usr_id"] = $this->input->post("usr_id");
            $this->Sss_model->changeLocation($postData);
        }
        
    }

    public function setNewStaff(){
        $this->load->model('Sss_model');
        $this->load->helper('url');

        if($this->input->post() != NULL){
            $postData["coy_id"] = $this->input->post("coy_id");
            $postData["usr_id"] = $this->input->post("usr_id");
            $postData["loc_id"] = $this->input->post("loc_id");
            $postData["usr_name"] = $this->input->post("usr_name");
            $postData["email_addr"] = $this->input->post("email_addr");
            $postData["join_date"] = $this->input->post("join_date");
            $this->Sss_model->setNewStaff($postData);
        }
    }

    public function setResignStaff(){
        $this->load->model('Sss_model');
        $this->load->helper('url');

        if($this->input->post() != NULL){
            $postData['usr_id'] = $this->input->post("usr_id");
            $postData["resign_date"] = $this->input->post("resign_date");
            $this->Sss_model->setResignStaff($postData);
        }
    }

    public function setNewPwd(){
        $this->load->helper('url');

        if($this->input->post() != NULL){
            $this->load->model('Sss_model');
            $this->load->helper('url');

            $postData = $this->input->post("email_verification");
            $data = $this->_getStaffId();
            $data['sms'] = "+65".$this->input->post("sms");
            if(trim(strtolower($data['email_addr'])) == $postData){
                $this->Sss_model->setNewPwd($data);
            }else{
                echo "
                <meta charset = 'utf-8'> 
                 <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                 <title>User Access Request</title> 
                 <div style='height:300px;'></div>
                 <div style='text-align:center;'><h2>Your E-Mail is not matched!</h2></div>
                 <div style='text-align:center;'><h2>Please enter your own email address.</h2></div>
                 <div style='text-align:center;'>
                 <button style='width:50%;padding:15px 32px;font-size:20px;'>
                 <a style='text-decoration: none;' href='/cherps/userAccess'>Home</a>
                 </button></div>
                 ";
            }
        }
    }

    public function requestAccess(){
        $this->load->model('Sss_model');
        $this->load->helper('url');

        if($this->input->post() != NULL){
            $postData['coy_id'] = 'CTL';
            $postData["usr_id"] = $this->input->post("usr_id");
            $postData["selected"] = $this->input->post("selected");
            // foreach($postData["selected"] as $row){
            //     echo "<h1>".$row."</h1>";
            // }
            $this->Sss_model->requestAccess($postData);
        } 
    }

    public function revokeAccess(){
        $this->load->model('Sss_model');
        $this->load->helper('url');

        if($this->input->post() != NULL){
            $postData['coy_id'] = 'CTL';
            $postData["usr_id"] = $this->input->post("usr_id");
            $postData["selected"] = $this->input->post("selected");
            // foreach($postData["selected"] as $row){
            //     echo "<h1>".$row."</h1>";
            // }
            $this->Sss_model->revokeAccess($postData);
        } 
    }

    public function setAccess($postData){
        $this->load->model('Sss_model');
        $this->load->helper('url');

        echo "<h1>".$postData['selected']."</h1>";
        echo "<h1>".$postData['usr_id']."</h1>";
        echo "<h1>".$postData['coy_id']."</h1>";
        echo "<h1>".$postData['approver']."</h1>";

        // $this->Sss_model->setAccess($postData);
    }
}
