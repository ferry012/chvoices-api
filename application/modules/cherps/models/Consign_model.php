<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Consign_model extends Base_Common_Model {

    private $cherps_usr;

    public function __construct() {
        parent::__construct();
        $this->load->model('cherps/Pogrn_model');

        $this->cherps_usr = 'cherps2pg';
    }

    // chvoices.challenger.sg/api/challenger_transfer
    public function createTransfer() {

        $postbody = trim(file_get_contents('php://input'));
        $data = json_decode($postbody,1);

        $status_level = $data['status_level'];
        if ($status_level == 4) {
            // Make sure all items have enough stocks
            foreach ($data['items'] as $k => $orderitem) {
                $sql = "select item_id, sum(qty_on_hand - qty_reserved) qty_available from ims_inv_physical where coy_id='" . $data['coy_id'] . "' and item_id='" . $orderitem['item_id'] . "' and loc_id='" . $data['loc_id'] . "' group by coy_id,item_id";
                $res = $this->db->query($sql)->row_array();
                if ($res['qty_available'] < $orderitem['qty_invoiced']) {
                    $status_level = 1;
                }
            }
        }

        $moveparam = array();
        $moveparam['coy_id'] = $data['coy_id'];
        $moveparam['move_type'] = $data['move_type'];
        $moveparam['status_level'] = $status_level;
        $moveparam['loc_id'] = $data['loc_id'];
        $moveparam['loc_to'] = $data['loc_to'];
        $moveparam['remarks'] = $data['remarks'];
        $moveparam['requester_id'] = $this->cherps_usr;
        $moveparam['created_by'] = $this->cherps_usr;
        $moveparam["items"] = array();
        foreach ($data['items'] as $k => $orderitem) {
            $moveparam["items"][] = array(
                "item_id" => trim($orderitem['item_id']),
                "item_desc" => $orderitem['item_desc'],
                "trans_qty" => $orderitem['qty_invoiced'],
                "ref_id" => $data['invoice_id'],
                "ref_num" => $orderitem['line_num'],
                "movement_ind" => 'T'
            );
        }
        $transfer = $this->Pogrn_model->genImsmove($moveparam);
        $trans_id = $transfer["trans_id"];
        $movement_log[] = $transfer;

        if ($status_level == 4) {
            // Perform the movement if status_level is completed
            foreach ($data['items'] as $k => $orderitem) {
                $params = [];
                $params['coy_id'] = $data['coy_id'];
                $params['item_id'] = trim($orderitem['item_id']);
                $params['loc_id'] = $data['loc_id'];
                $params['loc_to'] = $data['loc_to'];
                $params['trans_qty'] = $orderitem['qty_invoiced'];
                $params['trans_type'] = 'ST';
                $params['trans_id'] = $trans_id;
                $params["created_by"] = $this->cherps_usr;
                $movement = $this->Pogrn_model->moveInvphysical($params);
                $movement_log = array_merge($movement_log, $movement);
            }
        }

        return array(
            "code" => ($transfer) ? '1' : '0',
            "stats" => ["Documents" => count($movement_log)],
            "messages" => $movement_log,
            "trans_id" => $trans_id,
            "status_level" => $status_level
        );
    }

    // chvoices.challenger.sg/api/challenger_po/generate/XXX
    public function generateSmsTransId($invoice_id='', $params=[]) {

        ini_set('max_execution_time', '300');
        $this->load->model('cherps/Consign_model');

        if ($invoice_id=='RD') {
            $sql = "SELECT distinct i.invoice_id, i.inv_loc, i.loc_id, l.inv_type
                        FROM sms_invoice_item i
                        JOIN sms_invoice_list l ON i.coy_id=l.coy_id and i.invoice_id=l.invoice_id
                      WHERE i.coy_id='CTL' and i.status_level > 0 and i.inv_loc <> i.loc_id and (i.trans_id = '' OR i.trans_id IS NULL)
                      AND l.inv_type='RD'
                      AND created_on < now() + interval '-2 minute' ";
        }
        else if ($invoice_id=='') {
            $sql = "SELECT distinct invoice_id, inv_loc, loc_id FROM sms_invoice_item 
                      WHERE coy_id='CTL' and status_level > 0 and inv_loc <> loc_id and (trans_id = '' OR trans_id IS NULL)
                      AND inv_loc in ('IM-C','VEN-C','DSH-C')
                      AND created_on < now() + interval '-5 minute' ";
        }
        else {
            $sql = "SELECT distinct i.invoice_id, i.inv_loc, i.loc_id, l.inv_type
                        FROM sms_invoice_item i
                        JOIN sms_invoice_list l ON i.coy_id=l.coy_id and i.invoice_id=l.invoice_id
                      WHERE i.coy_id='CTL' and i.status_level > 0 and i.inv_loc <> i.loc_id and (i.trans_id = '' OR i.trans_id IS NULL)
                      AND i.invoice_id = '$invoice_id'
                    UNION ALL 
                    SELECT distinct trans_id, 'IM-C', 'RE', 'PS'
                        FROM pos_transaction_item WHERE coy_id='CTL' and status_level > 0
                      AND item_id in (select item_id from ims_item_list where coy_id='CTL' and inv_dim4 like '%-ESD%') 
                      AND trans_id = '$invoice_id' ";
        }
        $res = $this->db->query($sql)->result_array();

        $results = [];
        $performed = ['IMC' => [], 'VEN' => [], 'TRF' => []];
        foreach ($res as $row) {
            $id = trim($row['invoice_id']);
            $inv_type = trim($row['inv_type']);

            if ( trim($row['inv_loc']) == 'IM-C' && !in_array($id, $performed['IMC'])) {
                $results[] = $this->convertImc($id);
                $performed['IMC'][] = $id;
            }
            else if ( (trim($row['inv_loc']) == 'VEN-C' || trim($row['inv_loc']) == 'DSH-C') && !in_array($id, $performed['VEN'])) {
                //$results[] = $this->convertVenDsh($id);
                $performed['VEN'][] = $id;
            }
            else if (!in_array($id, $performed['TRF']) && $inv_type=='RD') {
                $results[] = $this->generateTransfer($id);
                $performed['TRF'][] = $id;
            }
        }

        return array(
            "orders" => $results
        );
    }

    // Non-consign, can get from own inventory
    public function generateTransfer($invoice_id) {

        $return_msg = '';
        $return_stats = array("ST Required"=>0, "ST Generated"=>0);

        // Pick the items with different inv_loc & loc_id
        $sql = "SELECT l.coy_id, l.invoice_id, l.inv_type, i.line_num, i.inv_loc, i.loc_id, i.item_id, i.item_desc, i.qty_invoiced, i.ref_id
                FROM sms_invoice_item i 
                JOIN sms_invoice_list l ON i.coy_id=l.coy_id and i.invoice_id=l.invoice_id
                WHERE i.coy_id='CTL' and i.status_level > 0 and i.inv_loc <> i.loc_id and (i.trans_id = '' OR i.trans_id IS NULL)
                    AND l.inv_type in ('HI','RD','OS')
                    AND i.inv_loc in (select loc_id from ims_location_list where coy_id=i.coy_id and loc_type in ('RE','RS','TS','OF','WH','HT','SR'))
                    AND i.invoice_id='$invoice_id' 
                ORDER BY i.inv_loc, i.loc_id";
        $res = $this->db->query($sql)->result_array();

        // Location pairing
        $resLocations = []; $lastrow = '';
        foreach ($res as $row) {
            $thisrow = trim($row['inv_loc']) .'-'. trim($row['loc_id']);
            $resLocations[$thisrow][] = $row;
            $lastrow = $thisrow;
        }

        // Issue transfers
        foreach ($resLocations as $orderitems) {

            $return_stats['ST Required']++;
            $order = $orderitems[0];

            $invoice_doc_status = 1;
            $invoice_doc_id = NULL;
            $move_type = "ST";
            if ($order['inv_type'] == "HI")
                $move_type = "HC";
            if ($order['inv_type'] == "RD")
                $move_type = "RD";
            if ($order['inv_type'] == "OS") {
                $move_type = "RD";
                $invoice_doc_status = 4;
                $invoice_doc_id = $order['invoice_id'];
            }

            $remarks = '';
            if (isset($order['invoice_id']) && $order['invoice_id']!='')
                $remarks .= $order['invoice_id'] . ' ';
//            if (isset($order['ref_id']) && $order['ref_id']!='')
//                $remarks .= '/ ' . $order['ref_id'] . ' ';

            $moveparam = array();
            $moveparam['coy_id'] = $order['coy_id'];
            $moveparam['move_type'] = $move_type;
            $moveparam['status_level'] = $invoice_doc_status;
            $moveparam['loc_id'] = $order['inv_loc'];
            $moveparam['loc_to'] = $order['loc_id'];
            $moveparam['remarks'] = $remarks;
            $moveparam['requester_id'] = $this->cherps_usr;
            $moveparam['created_by'] = $this->cherps_usr;
            $moveparam["items"] = array();
            foreach ($orderitems as $k => $orderitem) {
                $moveparam["items"][] = array(
                    "item_id" => trim($orderitem['item_id']),
                    "item_desc" => $orderitem['item_desc'],
                    "trans_qty" => $orderitem['qty_invoiced'],
                    "ref_id" => $orderitem['invoice_id'],
                    "ref_num" => $orderitem['line_num'],
                    "movement_ind" => 'T'
                );
            }
            $move = $this->Pogrn_model->genImsmove($moveparam);
            if ($move["code"]==1) {
                $return_stats['ST Generated']++;
                $return_msg .= 'ST '.$move["trans_id"].'. ';

                // 2.2 Update sms/pos
                foreach ($orderitems as $k => $orderitem) {
                    $delvparam = array();
                    $delvparam['created_by'] = $this->cherps_usr;
                    $delvparam['txn_type'] = "SMS";
                    $delvparam['coy_id'] = $orderitem['coy_id'];
                    $delvparam['txn_id'] = trim($orderitem['invoice_id']);
                    $delvparam['line_num'] = $orderitem['line_num'];
                    $delvparam['po_id'] = $move["trans_id"];
                    $delv = $this->Pogrn_model->updDelvtrans($delvparam);
                }
            }
            else {
                $return_msg .= 'ST Failed. ';
            }

        }

        return array(
            "code" => (($return_stats['ST Required']-$return_stats['ST Generated'])>0) ? '0' : '1',
            "stats" => $return_stats,
            "messages" => $return_msg
        );
    }


    public function convertVenDsh($id='') {

        return array(
            "code" => '0',
            "stats" => '',
            "messages" => "$id PO Doc X"
        );

    }

    public function convertImc($id=''){

        $this_supp_id = 'ER0001';
        $this_inv_loc = 'IM-C';
        $this_buyer_id = 'angie-lee';
        $esd_types = ['1-MS-ESD','2-OTHER-ESD'];

        $return_stats = array("PO Generated"=>0, "PO Transmitted"=>0);

        // 1.0 Get all IM-C items (ESD & NON-ESD)
        $invoices = $this->getTransItemsConv_Er0001($id, $this_supp_id, $this_inv_loc, 'CTL');
        $invoices[] = array("txn_type"=>'',"txn_id"=>''); // Add empty row to complete loop
        $ord = array();

        $orders = array(); $last_order = ''; $last_item_type = '';
        foreach ($invoices as $inv) {

            // Rearrange the items into processing sets - ESD (1 PO per line item), NON-ESD (1 PO all tx items)
            $ord_key = trim($inv['txn_type']).trim($inv['txn_id']);
            if ( $last_order != $ord_key || $last_item_type != $inv["txn_item_type"] || in_array($inv['txn_item_type'], $esd_types) ) {
                // Save previous $ord into $orders
                if ($last_order!='')
                    $orders[] = $ord;

                // New Order ID
                $last_order = $ord_key;
                $last_item_type = (in_array($ord['txn_item_type'],$esd_types)) ? '' : $inv["txn_item_type"];

                $ord = array();
                $ord["coy_id"] = $inv["coy_id"];
                $ord["txn_type"] = $inv["txn_type"];
                $ord["txn_item_type"] = $inv["txn_item_type"];
                $ord["txn_id"] = $inv["txn_id"];
                $ord["txn_date"] = $inv["txn_date"];
                $ord["trans_type"] = $inv["trans_type"];
                $ord["trans_loc"] = $inv['trans_loc'];

                $ord["tel_code"] = $inv["tel_code"];
                $ord["cust_name"] = $inv["cust_name"];
                $ord["cust_emailaddr"] = $inv["cust_emailaddr"];

                $ord["items"] = array();
            }

            $orditem = array();
            $orditem["line_num"] = $inv['line_num'];
            $orditem["item_id"] = $inv['item_id'];
            $orditem["item_desc"] = $inv['item_desc'];
            $orditem["long_desc"] = $inv['long_desc'];
            $orditem["txn_qty"] = $inv['txn_qty'];
            $orditem["unit_price"] = $inv['unit_price'];
            $orditem["status_level"] = $inv['status_level'];
            $orditem["inv_dim4"] = $inv['inv_dim4'];
            $ord["items"][] = $orditem;

        }
        //print_r($orders); exit;

        if ( count($orders)>0 ) {
            foreach ($orders as $order) {

                // 2.1 Generate PO
                $params = array();
                $params["emailPo"] = true; // Email notify supplier once po generated
                $params["sendapiPo"] = true; // Trigger api to supplier once po generated

                $params["coy_id"] = $order['coy_id'];
                $params["trans_type"] = $order['trans_type'];
                $params["loc_id"] = $order['trans_loc'];
                $params["buyer_id"] = $this_buyer_id;
                $params["supp_id"] = $this_supp_id;
                $params["status_level"] = (in_array($order['txn_item_type'],$esd_types)) ? 4 : 1;
                $params["tax_id"] = (in_array($order['txn_item_type'],$esd_types)) ? 'SG7' : '';
                $params["remarks"] = 'For Invoice: ' . $order['txn_id'];
                $params["requester_id"] = 'cherps'; //$this->cherps_usr;
                $params["approve_by"] = 'cherps'; //$this->cherps_usr;
                $params["approve_date"] = date('Y-m-d H:i:s');
                $params["created_by"] = $this->cherps_usr;
                $params["items"] = array();
                foreach ($order["items"] as $orderitem) {
                    $params["items"][] = array(
                        "ref_id" => $order['txn_id'],
                        "ref_num" => $orderitem['line_num'],
                        "item_id" => $orderitem['item_id'],
                        "qty_order" => $orderitem['txn_qty'],
                        "qty_received" => (in_array($order['txn_item_type'],$esd_types)) ? $orderitem['txn_qty'] : 0,
                        "bom_ind" => ''
                    );
                }
                $emailPo = (in_array($order['txn_item_type'],$esd_types)) ? false : true;
                $po = $this->Pogrn_model->genPo($params, $emailPo);
                if ($po["code"]==1) {

                    $order['po_id'] = $po["po_id"];
                    $return_stats['PO Generated']++;
                    $return[$order['po_id']] = ''.$order['txn_type'].' ' . trim($order['txn_id']) . ' ('.count($params["items"]).' '.$order['txn_item_type'].' item). ' ;
                    $return[$order['po_id']].= 'PO '. $order['po_id'] .'. ' ;

                    // 2.2 Update sms/pos
                    foreach ($order["items"] as $orderitem) {
                        $delvparam = array();
                        $delvparam['created_by'] = $this->cherps_usr;
                        $delvparam['txn_type'] = $order['txn_type'];
                        $delvparam['coy_id'] = $order['coy_id'];
                        $delvparam['txn_id'] = trim($order['txn_id']);
                        $delvparam['line_num'] = $orderitem['line_num'];
                        $delvparam['po_id'] = $order['po_id'];
                        if (in_array($order['txn_item_type'],$esd_types)) {
                            $delvparam['do_id'] = trim($order['txn_id']) . '-1';
                            $delvparam['delv_date'] = date('Y-m-d H:i:s');
                            $delvparam['qty_picked'] = $orderitem['txn_qty'];
                        }
                        $delv = $this->Pogrn_model->updDelvtrans($delvparam);

                        if ($order['txn_type'] == "SMS") {
                            // Clear reserve

                        }

                    }

                    // 2.3 Do GRN & IMSINV if ESD item (delivered)
                    if (in_array($order['txn_item_type'],$esd_types)) {
                        $grnparam = array();
                        $grnparam['coy_id'] = $order['coy_id'];
                        $grnparam['po_id'] = $order['po_id'];
                        $grnparam['loc_id'] = $order['trans_loc'];
                        $grnparam['supp_id'] = $this_supp_id;
                        $grnparam['supp_do'] = 'B2B-'.trim($order['trans_loc']);
                        $grnparam['order_type'] = 'I';
                        $grnparam['created_by'] = $this->cherps_usr;
                        $grnparam['status_level'] = 0;
                        $grnparam["items"] = array();
                        foreach ($order["items"] as $orderitem) {
                            $grnparam["items"][] = array(
                                //"ref_num" => $orderitem['line_num'],
                                "item_id" => trim($orderitem['item_id']),
                                "grn_qty" => $orderitem['txn_qty']
                            );
                        }
                        $grn = $this->Pogrn_model->genGrn($grnparam, $order['txn_id']);
                        $return[$order['po_id']] .= ($grn["code"]==1) ? 'GRN '.$grn["grn_id"].'. ' : 'GRN Failed. ';
                        if (ENVIRONMENT!="production"){
                            foreach($grn["inv_trans"] as $grn_inv_trans) {
                                $return[$order['po_id']] .= 'INV '.$grn_inv_trans['trans_id'].'. ';
                            }
                        }
                    }


                    $return[$order['po_id']] .= 'sendapiPo '.$po['sendapiPo'].'. ';
                    $return[$order['po_id']] .= 'emailPo '.$po['emailPo'].'. ';
                    if ($po['sendapiPo']=="1"){
                        $return_stats['PO Transmitted']++;
                    }


                }

            }
        }


        return array(
            "code" => (($return_stats['PO Generated']-$return_stats['PO Transmitted'])>0) ? '0' : '1',
            "stats" => $return_stats,
            "messages" => $return
        );

    }

    private function getTransItemsConv_Er0001($id='', $sql_supp_id, $sql_inv_loc='VEN-C', $sql_coy_id='CTL') {

        $sql_where_pos = ($id=='') ? " AND itm.created_on < now() + interval '-5 minute' " : " AND itm.trans_id='$id' " ;
        $sql_where_sms = ($id=='') ? " AND itm.created_on < now() + interval '-5 minute' " : " AND itm.invoice_id='$id' " ;
        $sql = "SELECT itm.coy_id,   
				'SMS' txn_type,   
				 (
						CASE WHEN ( select inv_dim4 from ims_item_list where coy_id = itm.coy_id and item_id = itm.item_id) like '%MS-ESD%' THEN '1-MS-ESD'
						WHEN ( select inv_dim4 from ims_item_list where coy_id = itm.coy_id and item_id = itm.item_id) like '%ESD%' THEN '2-OTHER-ESD'
						ELSE '0-NON-ESD' END
				) txn_item_type,
				rtrim(lst.invoice_id) txn_id,
				lst.invoice_date txn_date,
				'HP' trans_type,
				'HCL' trans_loc,
				lst.tel_code,
				lst.cust_name,
				lst.email_addr cust_emailaddr, 
				itm.line_num,   
				itm.item_id,
				itm.qty_invoiced txn_qty,
				itm.item_desc,   
				(SELECT long_desc FROM ims_item_list WHERE coy_id=itm.coy_id AND item_id=itm.item_id LIMIT 1) long_desc,
				itm.status_level,   
				itm.do_id,   
				itm.trans_id,   
				itm.delv_date,   
				itm.created_by,   
				itm.created_on,   
				itm.modified_by,   
				itm.modified_on,
				(select inv_dim4 from ims_item_list where coy_id = itm.coy_id and item_id = itm.item_id) inv_dim4,
				(select unit_price from ims_item_price prc 
				where prc.coy_id = '$sql_coy_id' and prc.item_id = itm.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '$sql_supp_id' and prc.eff_from <= now() and prc.eff_to >= now() LIMIT 1) *
				(case 
				(select curr_id from ims_item_price prc where prc.coy_id = '$sql_coy_id' and prc.item_id = itm.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '$sql_supp_id' and prc.eff_from <= now() and prc.eff_to >= now() LIMIT 1)
				when 'SGD' then 1 else (select exc.exchg_rate / exc.exchg_unit from ims_item_price prc, coy_foreign_exchg exc where prc.coy_id = '$sql_coy_id' and prc.item_id = itm.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '$sql_supp_id' and prc.eff_from <= now() and prc.eff_to >= now() and prc.coy_id = exc.coy_id and prc.curr_id = exc.curr_id and exc.eff_from <= now() and exc.eff_to >= now() LIMIT 1) 
				end) unit_price
		FROM sms_invoice_item  itm, sms_invoice_list lst
		WHERE ( itm.coy_id = '$sql_coy_id' ) AND (lst.coy_id = itm.coy_id) and (lst.invoice_id = itm.invoice_id) and
				( itm.inv_loc = '$sql_inv_loc' ) AND ( lst.status_level > 0 ) AND ( itm.status_level > 0 ) AND ( itm.trans_id = '' or itm.trans_id is null ) $sql_where_sms
UNION ALL
SELECT itm.coy_id,
				'POS' txn_type, 
				(
						CASE WHEN ( select inv_dim4 from ims_item_list where coy_id = itm.coy_id and item_id = itm.item_id) like '%MS-ESD%' THEN '1-MS-ESD'
						WHEN ( select inv_dim4 from ims_item_list where coy_id = itm.coy_id and item_id = itm.item_id) like '%ESD%' THEN '2-OTHER-ESD'
						ELSE '0-NON-ESD' END
				) txn_item_type,
				rtrim(lst.trans_id) txn_id,  
				lst.trans_date txn_date,  
				'IP' trans_type,
				lst.loc_id trans_loc,
				lst.tel_code,
				lst.cust_name,
				lst.email_addr cust_emailaddr,  
				itm.line_num,   
				itm.item_id,   
				itm.trans_qty txn_qty,
				itm.item_desc,   
			  (SELECT long_desc FROM ims_item_list WHERE coy_id=itm.coy_id AND item_id=itm.item_id LIMIT 1) long_desc,
				itm.status_level,   
				'' do_id,   
				itm.trans_id,   
				itm.modified_on delv_date,   
				itm.created_by,   
				itm.created_on,   
				itm.modified_by,   
				itm.modified_on,
				( select inv_dim4 from ims_item_list where coy_id = itm.coy_id and item_id = itm.item_id) inv_dim4,
				(select unit_price from ims_item_price prc where prc.coy_id = '$sql_coy_id' and prc.item_id = itm.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '$sql_supp_id' and prc.eff_from <= now() and prc.eff_to >= now() LIMIT 1) *
				(case (select curr_id from ims_item_price prc where prc.coy_id = '$sql_coy_id' and prc.item_id = itm.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '$sql_supp_id' and prc.eff_from <= now() and prc.eff_to >= now() LIMIT 1) when 'SGD' then 1
				else (select exc.exchg_rate / exc.exchg_unit from ims_item_price prc, coy_foreign_exchg exc 
				where prc.coy_id = '$sql_coy_id' and prc.item_id = itm.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '$sql_supp_id' and prc.eff_from <= now() and prc.eff_to >= now() and 
				prc.coy_id = exc.coy_id and prc.curr_id = exc.curr_id and exc.eff_from <= now() and exc.eff_to >= now() LIMIT 1) end) unit_price
		FROM pos_transaction_item  itm, pos_transaction_list lst
		WHERE ( itm.coy_id = '$sql_coy_id' ) AND (lst.coy_id = itm.coy_id) and (lst.trans_id = itm.trans_id) and
				( lst.status_level > 0 ) AND ( itm.status_level = 1 ) AND
				( select inv_dim4 from ims_item_list where coy_id = '$sql_coy_id' and item_id = itm.item_id) like '%-ESD%' $sql_where_pos 
ORDER BY txn_type,txn_id,txn_item_type DESC;";
        //echo "<pre>" . $sql;exit;
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

}
?>