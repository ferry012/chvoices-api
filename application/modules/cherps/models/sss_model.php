<?php 
   Class Sss_model extends Base_Common_Model { 
	
      Public function __construct() { 
         parent::__construct(); 
      } 
      public function test(){
        echo "Hello from test function in sss_model!<br>";
        // $sql = "";
        // $result = $this->db->query($sql)->result();
        // foreach($result as $row){
        //     $data = $row->$rowInTable;
        // }

      }

      public function getUser($usr_id){
         $this->load->helper(array('cookie', 'url')); 
         // $usr_id = $params['usr_id'];
         // $type = $params['type'];
        $sql = "SELECT c.coy_id,trim(c.usr_id) as usr_id,c.staff_id,s.usr_name, s.email_addr,c.loc_id,c.fin_dim1,c.fin_dim2,c.fin_dim3,
        case when c.security_level = 0 then 'Staff/Cashier'
             when c.security_level = 1 then 'Supervisor'
             when c.security_level = 2 then 'Dept HOD/Retail Manager'
             when c.security_level = 3 then 'Director/Operation Mgr'
             when c.security_level = 4 then 'COO/CFO/CIO'
             when c.security_level = 5 then 'CEO'
        end as security_level
        FROM coy_usr_list c LEFT JOIN sys_usr_list s ON c.staff_id = s.usr_id
        WHERE c.coy_id = 'CTL' AND c.usr_id = '".$usr_id."' AND c.fin_dim2 != 'RESIGNED' AND s.revoked_by = ''";
        $result = $this->db->query($sql)->result();
        foreach($result as $row){
            $user['usr_id'] = $row->usr_id;
            $user['usr_name'] = $row->usr_name;
            $user['loc_id'] = $row->loc_id;
            $user['security_level'] = $row->security_level;
            $user['email_addr'] = $row->email_addr;
            $user['errMsg'] = "";
        }

        if($result == null){
         $user['usr_id'] = $usr_id;
         $user['errMsg'] = "Staff ID not existed.";
        }

        $sql = "SELECT u.usr_id, u.grp_id, l.grp_desc FROM sss_grp_usr_list u LEFT JOIN sss_grp_list l
        ON u.coy_id = l.coy_id AND u.grp_id = l.grp_id WHERE u.coy_id='CTL' AND u.usr_id='".$user['usr_id']."'";
        $result = $this->db->query($sql)->result();

        $user['access'] = $result;
      //   foreach($result as $row){
      //       $user['access'] = $row->grp_id
      //   }

        return $user;
      }

      public function setStaffId(){
         $this->load->helper('url');
         $this->load->helper(array('cookie', 'url')); 

         if($this->uri->segment(3) != NULL){
            // get staff id from URL
             $staff_id = $this->uri->segment(3);
             //store user profile in cookie
             set_cookie($name='usr_profile',$value = $staff_id,'3600');
         }
     }

     public function getFin_dim1($loc_id){
         //get fin_dim1 value
         $sql = "select distinct fin_dim1 from coy_usr_list where coy_id = 'CTL' AND fin_dim1 like '%$loc_id%'";
         $result = $this->db->query($sql)->result();
         foreach($result as $row){
            return $row->fin_dim1;
        }
     }

      public function changeLocation($params){
         $this->load->model('Email');
         $recipients = "";
         $usr_id = $params['usr_id'];
         $loc_id = $params['loc_id'];
         $fin_dim1 = $this->getFin_dim1($loc_id);

         $sql = "update coy_usr_list set loc_id = '$loc_id', fin_dim1 = '$fin_dim1'
         where coy_id = 'CTL' AND usr_id = '$usr_id'";
         $this->db->query($sql);
         
         $data = $this->getUser($usr_id);
         $requester = $this->getUser(get_cookie('usr_profile'));
         $recipients = $requester['email_addr'];//.";hr_team@challenger.sg;"; // .";$loc_id.".ic@challenger.sg";
         $data['subject'] = "Ops App - Change Location.";
         $data['body'] = "Hi ".$requester['usr_name'].",<br/><br/>
         Staff: ".$data['usr_name']." - ".$data['usr_id']."<br/>
         New location: <b>".$data['loc_id']."</b><br/><br/>
         Above staff location has been updated.<br/>
         Please let your staff to re-login to CHERPS / Logistics Apps upon receive this confirmation email.<br/><br/>
         Hi HR Team,<br/>Please assist to change above staff location as well.<br/>
         Thanks!<br/><br/>
         Please do not reply to this email.<br/>
         Email James if you have any questions<br/><br/>Regards<br/>Software Team";
         $this->Email->sendEmail($recipients, $data);
      }

      public function setNewStaff($postData){
         $this->load->model('Email');
         $fin_dim1 = $this->getFin_dim1($postData['loc_id']);

         //add into sys_usr_list
         $sys_sql = "INSERT INTO sys_usr_list
         SELECT '".$postData['usr_id']."','".$postData['usr_name']."','".$postData['email_addr']."','','e0e82096539b0cc375886605e60b30e8',
         current_timestamp,'','1899-12-31 22:55:25','1899-12-31 22:55:25','".$postData['join_date']."'";
         $this->db->query($sys_sql);

         //add into coy_usr_list
         $coy_sql = "INSERT INTO coy_usr_list
         SELECT '".$postData['coy_id']."','".$postData['usr_id']."','Y','".$postData['usr_id']."','0','N','".$postData['loc_id']."','".$fin_dim1."','','','3393',
         current_timestamp,'3393',current_timestamp";
         $this->db->query($coy_sql);
         
         //assign permission (if any)

         $recipients = "james.chen@challenger.sg"; // .";$loc_id.".ic@challenger.sg";
         $data = $this->getUser($postData['usr_id']);
         $data['email_addr'] = $postData['email_addr'];
         $data['subject'] = "Ops App - New Staff";
         $requester = $this->getUser(get_cookie('usr_profile')); 
         $data['body'] = "Hi ".$requester['usr_name'].",<br/><br/>
         Staff: ".$data['usr_name']." - ".$data['usr_id']."<br/>
         Location: <b>".$data['loc_id']."</b><br/><br/>
         Above staff account has been created.<br/>
         Please let your staff to login to CHERPS to change password upon receive this confirmation email.<br/>
         There is no permission assigned, please let RM/OM to decide what role will they be assigned<br/><br/>
         <b>Please do not reply to this email.<br/>
         Email James if you have any questions</b><br/><br/>Regards<br/>Software Team";
         $this->Email->sendEmail($recipients, $data);
      }

      public function setResignStaff($postData){
         $this->load->model('Email');
         $data = $this->getUser($postData['usr_id']);
         $sysSql = "UPDATE sys_usr_list SET sec_pass = '', revoked_by = '3393', 
         revoked_date=current_timestamp, resigned_date='".$postData['resign_date']." 00:00:00' WHERE usr_id= '".$postData['usr_id']."'";
         $this->db->query($sysSql);
         $coySql = "UPDATE coy_usr_list SET sel_coy='' WHERE usr_id= '".$postData['usr_id']."'";
         $this->db->query($coySql);
         $sssSql = "DELETE FROM sss_grp_usr_list WHERE usr_id= '".$postData['usr_id']."'";
         $this->db->query($sssSql);
         $sssSql = "DELETE FROM sss_usr_menu_list WHERE usr_id= '".$postData['usr_id']."'";
         $this->db->query($sssSql);
         $sssSql = "DELETE FROM sss_usr_setting_list WHERE usr_id= '".$postData['usr_id']."'";
         $this->db->query($sssSql);
         $sssSql = "DELETE FROM sss_usr_type_list WHERE usr_id= '".$postData['usr_id']."'";
         $this->db->query($sssSql);
         // Disable staff member in crm_member_list table (vClub database), if mbr_type = ‘S’+staff_id exists.

         $requester = $this->getUser(get_cookie('usr_profile'));
         //$recipients = "allretails@challenger.sg;all.ubistaff@challenger.sg"; // .";$loc_id.".ic@challenger.sg";
         $recipients = "james.chen@challenger.sg";
         $data['email_addr'] = ";;;;;;;;,,,,,;;;,;,;,;";
         $data['subject'] = "Ops App - Staff Resignation";
         $data['body'] = "<style>table, th, td {width: 40%; text-align: center; border: 1px solid black;border-collapse: collapse;}</style>
         Hi All,<br/><br/>
         Please be informed that the below <b>".$data['loc_id']."</b> staff have left the company with effect from <b>".$postData['resign_date']."</b>.<br/><br/>
         <table><tr style='background-color:#C0C0C0'><th>Employee Code</th><th>Full Name</th></tr>
         <tr><td>".$data['usr_id']."</td><td>".$data['usr_name']."</td></tr></table><br/>
         He is not authorized to perform any form of transaction on behalf of the company with immediate effect.<br/><br/>
         Kindly disseminate this information to your team.<br/><br/>
         The staff cherps access has been revoked, <b>MIS Team</b> please help to deactivate email account upon receive this email.<br/><br/>
         Regards<br/>
         ".$requester['usr_name']."";
         $this->Email->sendEmail($recipients, $data);
      }

      public function setNewPwd($postData){
         // $postData['usr_id']
         $num = mt_rand(1000, 9999);
         $otp = md5("*".$num);
         $sysSql = "UPDATE sys_usr_list SET sec_pass='".$otp."', pwd_changed=current_timestamp WHERE usr_id= '".$postData['usr_id']."'";
         $this->db->query($sysSql);

         $this->load->model('Email');
         $recipients = $postData['usr_email'];
         $postData['subject'] = "Ops App - Reset Password";
         $postData['body'] = "Hi ".$postData['usr_name'].",<br/><br/>
         Your cherps password has been updated!<br/>
         An SMS has been sent to the phone number provided.<br/>
         Please login to cherps to update your password ASAP unpon receive this email. <br/><br/>
         If is not requested by you, please contact MIS immediately.<br/><br/>
         <b>Please do not reply to this email.</b><br/><br/>
         Regards<br/>
         Software Team";
         $data = "Your new default CHERPS password is: *".$num;
         $phoneNum = $postData['sms'];
         $this->Email->sendEmail($recipients, $postData);
         $this->Email->sendSMS($phoneNum, $data);
      }

      public function requestAccess($postData){
         $this->load->model('Email');

         $requester = $this->getUser($postData['usr_id']);
         $sysSql = "SELECT trim(grp_id) grp_id, grp_desc FROM sss_grp_list WHERE coy_id = 'CTL'
         AND grp_id  IN (SELECT grp_id FROM sss_grp_usr_list WHERE coy_id = 'CTL' AND usr_id = '".$postData['usr_id']."')";
         $grp_list = $this->db->query($sysSql)->result();
         
         $current_grp = "<div id='currentAccess'>
         <style>table, th, td {table-layout:fixed; width: 60%; text-align: left; border: 1px solid black;border-collapse: collapse;}</style>
         <table><caption style=text-align:left;><b>Current Permission: </b></caption><br/>
         <tr><th>Group_ID</th><th>Description</th></tr>";
         foreach($grp_list as $row){
            $current_grp.= "<tr><td>".$row->grp_id."</td>";
            $current_grp.= "<td>".$row->grp_desc."</td></tr>";
         }
         $current_grp.= "</table><br/><br/>";

         $request_grp = "<div id='requestAccess'>
         <style>table, th, td {table-layout:fixed; width: 60%; text-align: left; border: 1px solid black;border-collapse: collapse;}</style>
         <table><caption style=text-align:left;color:red;><b>Request Permission: </b></caption><br/>
         <tr><th>Group_ID</th><th>Description</th></tr>";

         foreach($postData['selected'] as $grp_id){
            $request_grp.= "<tr><td>".$grp_id."</td>";

            $sql="SELECT grp_desc FROM sss_grp_list WHERE coy_id = '".$postData['coy_id']."' 
            AND grp_id = '".$grp_id."'";
            $result = $this->db->query($sql)->result();
            foreach($result as $row){
               $request_grp.= "<td>".$row->grp_desc."</td></tr>";
            }
         }
         $request_grp.= "</table><br/>";
         $approver = $this->getUser('3393');
         $recipients = $approver['email_addr'];
         $data['subject'] = "Ops App - Request Access";
         $data['email_addr'] = $requester['email_addr'];
         $data['body'] = "Hi ,<br/>
         Your staff: ".$requester['usr_name']." - ".$requester['usr_id']."<br/>
         is requesting for cherps access, his current permission as per below table.<br/>
         ".$current_grp."<br/>
         He is requesting for below access:<br/>
         ".$request_grp."
         <!--<form method='post' action='/cherps/setAccess'>
            <input type='text' id='coy_id' name='coy_id' style=display:none; value='".$postData['coy_id']."' />
            <input type='text' id='usr_id' name='usr_id' style=display:none; value='".$postData['usr_id']."' />
            <input type='text' id='approver' name='approver' style=display:none; value='".$$approver['usr_id']."' />
            <input type='text' id='selected' name='selected' style=display:none; value='".$postData['selected']."' />
            <spam><input type='submit' value='Approved'/></spam>  
            <spam><input type='submit' value='Denied'/></spam>
         </form>-->
         Please proceed to Ops App to approve / Deny.
         <br/><br/>
         If is not requested by the staff, please contact MIS immediately.<br/><br/>
         <b>Please do not reply to this email.</b><br/><br/>

         Regards<br/>
         Software Team";
         $this->Email->sendEmail($recipients, $data);
         $this->Sss_model->setAccess($recipients, $data);
         //send API to ops app notification
      }

      public function setAccess($postData){
         // insert data to sss_grp_usr_list
         // foreach($postData['selected'] as $grp_id){
         //    $sssSql = "INSERT INTO sss_grp_usr_list 
         //    SELECT '".$postData['coy_id']."','".$grp_id."','".$postData['usr_id']."'";
         //    $this->db->query($sssSql);
         // }
      }

      public function revokeAccess($postData){
         
      }
   }
?> 