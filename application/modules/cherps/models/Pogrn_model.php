<?php

/* YS
 * 2018-05-25
 */
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Pogrn_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
    }

    // chvoices.challenger.sg/api/challenger_po/transmit/XXX
    public function transmitPoVendor($po_id, $params=[]) {
        // Trigger PO to supplier via API (selected suppliers only)
        if (trim($params['supp_id'])=='ER0001') {
            $this->load->model('cherps_supplier/Er0001_model');
            $resp = $this->Er0001_model->send_order($po_id);
        }
        else if (trim($params['supp_id'])=='SH0004') {
            $this->load->model('cherps_supplier/Sh0004_model');
            $resp = $this->Sh0004_model->send_order($po_id);
        }
        else if (trim($params['supp_id'])=='LS0003') {
            $this->load->model('cherps_supplier/Ls0003_model');
            $resp = $this->Ls0003_model->send_order($po_id);
        }
        else if (trim($params['supp_id'])=='BL0001') {
            $this->load->model('cherps_supplier/Bl0001_model');
            $resp = $this->Bl0001_model->send_order($po_id);
        }
        return array("code"=>$resp['code'], "msg"=>$resp['msg']);
    }

    public function genPo($params, $emailPo=false){

        $po_id = (isset($params["po_id"]) && strlen($params["po_id"])>=10) ? $params["po_id"] : $this->getRefId(trim($params["trans_type"]).trim($params["loc_id"]), trim($params["trans_type"]));

        if (!isset($params["emailPo"])) {
            $params["emailPo"] = $emailPo;
        }

        // Get location info
        $location = $this->getLocInfo($params["loc_id"],$params["coy_id"]);

        // Get supplier info
        $sql = "SELECT supp_id, supp_name, supp_addr, contact_person, tel_code, fax_code, payment_id, delv_term_id, delv_location, delv_mode_id, email_addr, email_addr3,
                      tax_id, curr_id, (SELECT tax_percent FROM coy_tax_list WHERE coy_id = pms_supplier_list.coy_id AND tax_id = pms_supplier_list.tax_id) as tax_percent,
                      coy_id, (select coy_name from sys_coy_list where coy_id=pms_supplier_list.coy_id) coy_name
                    FROM pms_supplier_list
                    WHERE coy_id='".$params["coy_id"]."' AND supp_id='".$params["supp_id"]."'
                    ORDER BY modified_by DESC limit 1;";
        $supplier = $this->db->query($sql)->row_array();

        $ins = array();
        $ins["coy_id"] = $params["coy_id"];
        $ins["po_id"] = $po_id;
        $ins["po_type"] = $params["trans_type"];
        $ins["po_date"] = date('Y-m-d H:i:s');
        $ins["buyer_id"] = $params["buyer_id"];
        $ins["requester_id"] = $params["requester_id"];

        $ins["tax_id"] = ($params["tax_id"]!='') ? $params["tax_id"] : $supplier["tax_id"];
        $ins["tax_percent"] = ($params["tax_id"]!='') ? $this->getTaxinfo($params["tax_id"], 'tax_percent') : $supplier["tax_percent"];
        if ($ins["tax_id"] == "PRE") {
            // Check with ims_item_pgs where included_ind='N', set to SG7
            $item_ids = [];
            foreach ($params['items'] as $item) {
                $item_ids[] = $item["item_id"];
            }

            if ($this->getNonPrescribeGds($item_ids)) {
                $ins["tax_id"] = 'SG7';
                $ins["tax_percent"] = $this->getTaxinfo($ins["tax_id"], 'tax_percent');
            }
        }

        $ins["supp_id"] = $supplier["supp_id"];
        $ins["supp_name"] = $supplier["supp_name"];
        $ins["supp_addr"] = $supplier["supp_addr"];
        $ins["tel_code"] = $supplier["tel_code"];
        $ins["fax_code"] = $supplier["fax_code"];
        $ins["contact_person"] = $supplier["contact_person"];
        $ins["payment_id"] = $supplier["payment_id"];
        $ins["delv_term_id"] = $supplier["delv_term_id"];
        $ins["delv_location"] = $supplier["delv_location"];
        $ins["delv_mode_id"] = $supplier["delv_mode_id"];
        $ins["curr_id"] = $supplier["curr_id"];
        $ins["email_addr"] = $supplier["email_addr"];

        $ins["loc_id"] = $location["loc_id"];
        $ins["loc_name"] = $location["loc_name"];
        $ins["loc_addr"] = $location["loc_addr"];
        $ins["status_level"] = $params["status_level"];
        $ins["remarks"] = $params["remarks"];
        $ins["approve_by"] = $params["approve_by"];
        $ins["approve_date"] = $params["approve_date"];
        $ins["created_by"] = $params["created_by"];
        $ins["created_on"] = date('Y-m-d H:i:s');
        $ins["modified_by"] = $params["created_by"];
        $ins["modified_on"] = date('Y-m-d H:i:s');
        $result1 = $this->db->insert('pms_po_list', $ins);

        $ins_num = 0;
        $paramitems = array();
        foreach ($params['items'] as $item) {
            $ins_num++;

            // Get item master info
            $sql = "SELECT item_id,item_desc,item_type,uom_id,long_desc,fin_dim2,fin_dim3,
                           (select unit_price from ims_item_price prc where prc.coy_id=ims_item_list.coy_id and prc.item_id = ims_item_list.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '".$params["supp_id"]."' and prc.eff_from <= now() and prc.eff_to >= now() limit 1) *
                            (case (select curr_id from ims_item_price prc where prc.coy_id=ims_item_list.coy_id and prc.item_id = ims_item_list.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '".$params["supp_id"]."' and prc.eff_from <= now() and prc.eff_to >= now() limit 1) when 'SGD' then 1 
                            else (select exc.exchg_rate / exc.exchg_unit from ims_item_price prc, coy_foreign_exchg exc where prc.coy_id = ims_item_list.coy_id and prc.item_id = ims_item_list.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '".$params["supp_id"]."' and prc.eff_from <= now() and prc.eff_to >= now() and prc.coy_id = exc.coy_id and prc.curr_id = exc.curr_id and exc.eff_from <= now() and exc.eff_to >= now() limit 1) end)
                            unit_price
                        FROM ims_item_list 
                        WHERE (coy_id='".$params["coy_id"]."') AND (item_id='".$item["item_id"]."');";
            $imsitem = $this->db->query($sql)->row_array();

            if (isset($item['loc_id']) && $item['loc_id']!='') {
                $location = $this->getLocInfo($item["loc_id"],$params["coy_id"]);
            }

            $ins = array();
            $ins["coy_id"] = $params["coy_id"];
            $ins["po_id"] = $po_id;
            $ins["status_level"] = $params["status_level"];

            $ins["line_num"] = $ins_num;
            $ins["qty_order"] = $item["qty_order"];
            $ins["ref_id"] = $item["ref_id"];
            $ins["ref_num"] = $item["ref_num"];
            $ins["bom_ind"] = $item["bom_ind"];

            $ins["item_id"] = $imsitem["item_id"];
            $ins["item_desc"] = $imsitem["item_desc"];
            $ins["uom_id"] = $imsitem["uom_id"];
            $ins["long_desc"] = $imsitem["long_desc"];
            $ins["fin_dim1"] = $location["fin_dim1"];
            $ins["fin_dim2"] = $imsitem["fin_dim2"];
            $ins["fin_dim3"] = $imsitem["fin_dim3"];
            $ins["unit_price"] = $imsitem["unit_price"];

            $ins["created_by"] = $params["created_by"];
            $ins["created_on"] = date('Y-m-d H:i:s');
            $ins["modified_by"] = $params["created_by"];
            $ins["modified_on"] = date('Y-m-d H:i:s');

            $ins["qty_received"] = (isset($item["qty_received"])) ? $item["qty_received"] : 0;
            $result2 = $this->db->insert('pms_po_item', $ins);

            if ($result2) {
                $paramitems[] = array("item_id"=>$imsitem["item_id"], "item_desc"=>$imsitem["item_desc"], "qty_order"=>$item["qty_order"], "long_desc"=>$imsitem["long_desc"]);
            }
        }

        if ($result1 && $result2) {

            $emailPo=0;
            $sendapiPo=0;
            $sendapiMsg='';


            if (isset($params["sendapiPo"]) && $params["sendapiPo"]==true && $params["status_level"]>=1) {
                // Send PO API (for approved orders)
                $sendapi = $this->transmitPoVendor($po_id, array("supp_id"=>$supplier["supp_id"]) );
                $sendapiPo = $sendapi['code'];
                $sendapiMsg = $sendapi['msg'];
            }

            if (isset($params["emailPo"]) && $params["emailPo"]==true && $params["status_level"]>=1) {
                // Notify supplier (for approved orders)
                $sendemailparam = array();
                $sendemailparam["po_id"] = $po_id;
                $sendemailparam["supp_id"] = $supplier["supp_id"];
                $sendemailparam["supp_name"] = $supplier["supp_name"];
                $sendemailparam["coy_name"] = $supplier["coy_name"];
                $sendemailparam["email_addr3"] = $supplier['email_addr3'];
                $sendemailparam["remarks"] = $sendapiMsg;
                foreach ($paramitems as $orderitem) {
                    $sendemailparam["items"][] = array(
                        "item_id" => trim($orderitem['item_id']),
                        "item_desc" => $orderitem["item_desc"],
                        "qty_order" => $orderitem["txn_qty"]
                    );
                }
                $sendemail = $this->emailPo($po_id, $sendemailparam, $emailPo);
                $emailPo = 1;
            }

            return array("code"=>1, "po_id"=>trim($po_id), "line_count"=>count($params['items']), "msg"=>'PO '.trim($po_id).' created', "emailPo"=>$emailPo, "sendapiPo"=>$sendapiPo );
        }
        else {
            return array("code"=>0, "msg"=>'Fail to create PO' );
        }

    }

    public function genRn($params){

        $rn_id = (isset($params["rn_id"]) && strlen($params["rn_id"])>10) ? $params["rn_id"] : $this->getRefId('RN'.'R'.trim($params["trans_type"]).trim($params["loc_id"]), '', "PMS", $params["coy_id"]);

        // Get supplier info
        $sql = "SELECT supp_id, supp_name, supp_addr, contact_person, tel_code, fax_code, payment_id, delv_term_id, delv_location, delv_mode_id, email_addr, email_addr3,
                      tax_id, curr_id, (SELECT tax_percent FROM coy_tax_list WHERE coy_id = pms_supplier_list.coy_id AND tax_id = pms_supplier_list.tax_id) as tax_percent,
                      coy_id, (select coy_name from sys_coy_list where coy_id=pms_supplier_list.coy_id) coy_name
                    FROM pms_supplier_list
                    WHERE coy_id='".$params["coy_id"]."' AND supp_id='".$params["supp_id"]."'
                    ORDER BY modified_by DESC limit 1;";
        $supplier = $this->db->query($sql)->row_array();

        $ins = array();
        $ins["coy_id"] = $params["coy_id"];
        $ins["trans_id"] = $rn_id;
        $ins["rn_type"] = $params["trans_type"];
        $ins["requested_date"] = date('Y-m-d H:i:s');
        $ins["loc_id"] = $params["loc_id"];
        $ins["status_level"] = ($params["status_level"]) ? $params["status_level"] : 0;
        $ins["replacement_ind"] = ($params["replacement_ind"]) ? $params["replacement_ind"] : 'N';

        $ins['buyer_id'] = $params["buyer_id"];
        $ins['submit_by'] = $params["submit_by"];
        $ins['submit_date'] = date('Y-m-d H:i:s');
        $ins['approve_by'] = $params["approve_by"];
        $ins['approve_date'] = date('Y-m-d H:i:s');
        $ins['collection_date'] = date('Y-m-d H:i:s');
        $ins['collected_on'] = date('Y-m-d H:i:s');
        $ins['reset_by'] = '';
        $ins['reset_date'] = date('Y-m-d H:i:s');
        $ins["reset_notes"] = '';

        $ins["supp_id"] = $supplier["supp_id"];
        $ins["supp_name"] = $supplier["supp_name"];
        $ins["curr_id"] = $supplier["curr_id"];
        $ins["tax_id"] = ($params["tax_id"]!='') ? $params["tax_id"] : $supplier["tax_id"];
        $ins["tax_percent"] = ($params["tax_id"]!='') ? $this->getTaxinfo($params["tax_id"], 'tax_percent') : $supplier["tax_percent"];
        $ins["supp_notes"] = '';
        $ins["remarks"] =  ($params["remarks"]) ? $params["remarks"] : '';
        $ins["debit_note"] = '';

        $ins["receipt_id"] = $params["receipt_id"];
        $ins["doc_ref"] = '';
        $ins["ref_id"] = '';

        $ins["created_by"] = $params["created_by"];
        $ins["created_on"] = date('Y-m-d H:i:s');
        $ins["modified_by"] = $params["created_by"];
        $ins["modified_on"] = date('Y-m-d H:i:s');
        $result1 = $this->db->insert('pms_rn_list', $ins);

        $ins_num = 0;
        $paramitems = array();
        foreach ($params['items'] as $item) {
            $ins_num++;

            // Get item master info
            $sql = "SELECT item_id,item_desc,item_type,uom_id,long_desc,fin_dim2,fin_dim3,
                        (select unit_price 
                            from ims_item_price prc
                            where prc.coy_id=ims_item_list.coy_id and prc.item_id = ims_item_list.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '".$params["supp_id"]."' and prc.eff_from <= now() and prc.eff_to >= now()
                            ) *
                        (case (select curr_id from ims_item_price prc where prc.coy_id=ims_item_list.coy_id and prc.item_id = ims_item_list.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '".$params["supp_id"]."' and prc.eff_from <= now() and prc.eff_to >= now() limit 1)
                            when 'SGD' then 1
                            else (
                                select exc.exchg_rate / exc.exchg_unit
                                from ims_item_price prc, coy_foreign_exchg exc
                                where prc.coy_id = ims_item_list.coy_id and prc.item_id = ims_item_list.item_id and prc.price_type = 'SUPPLIER' and prc.ref_id = '".$params["supp_id"]."'
                                  and prc.eff_from <= now() and prc.eff_to >= now() and prc.coy_id = exc.coy_id and prc.curr_id = exc.curr_id and exc.eff_from <= now() and exc.eff_to >= now() limit 1)
                            end) unit_price
                        FROM ims_item_list
                        WHERE (coy_id='".$params["coy_id"]."') AND (item_id='".$item["item_id"]."');";
            $imsitem = $this->db->query($sql)->row_array();

            $ins = array();
            $ins["coy_id"] = $params["coy_id"];
            $ins["trans_id"] = $rn_id;
            $ins["status_level"] = $params["status_level"];

            $ins["line_num"] = $ins_num;
            $ins["ref_id"] = $item["ref_id"];
            $ins["ref_rev"] = $item["ref_rev"];
            $ins["ref_num"] = $item["ref_num"];

            $ins["trans_qty"] = $item["trans_qty"];
            $ins["unit_price"] = ($item["unit_price"]) ? $item["unit_price"] : $imsitem["unit_price"];
            $ins["item_id"] = $imsitem["item_id"];
            $ins["item_desc"] = $imsitem["item_desc"];

            $ins['remarks'] = '';
            $ins["initial_date"] = date('Y-m-d H:i:s');
            $ins["created_by"] = $params["created_by"];
            $ins["created_on"] = date('Y-m-d H:i:s');
            $ins["modified_by"] = $params["created_by"];
            $ins["modified_on"] = date('Y-m-d H:i:s');
            $result2 = $this->db->insert('pms_rn_item', $ins);

            if ($result2) {
                $paramitems[] = array("item_id"=>$imsitem["item_id"], "item_desc"=>$imsitem["item_desc"], "trans_qty"=>$item["trans_qty"], "unit_price"=>$imsitem["unit_price"]);
            }
        }

        if ($result1 && $result2) {
            return array("code"=>1, "trans_id"=>trim($rn_id), "line_count"=>count($params['items']), "msg"=>'RN '.trim($rn_id).' created');
        }
        else {
            return array("code"=>0, "msg"=>'Fail to create RN' );
        }

    }

    public function genGrn($params, $stk_deduct=NULL){
        // Get PO details
        $sql = "SELECT l.po_id,l.current_rev,l.po_type,l.loc_id,l.curr_id,
                    i.line_num,i.item_id,i.item_desc,i.qty_order,i.unit_price,i.disc_percent,
                    fin_dim1, fin_dim2, fin_dim3
                FROM pms_po_list l, pms_po_item i
                WHERE l.po_id=i.po_id and l.current_rev=l.rev_num and l.current_rev=i.rev_num
                    and l.coy_id='".$params["coy_id"]."' and l.po_id='".$params["po_id"]."' ;";
        $poitems = $this->db->query($sql)->result_array();

        $poitems_byline = array(); $poitems_byitem = array();
        foreach ($poitems as $item) {
            $poitems_byline[trim($item['line_num'])] = $item;
            $poitems_byitem[trim($item['item_id'])] = $item;
        }

        // Check that all submitted items are valid in PO
        $check_errors = array();
        foreach ($params['items'] as $k=>$par) {
            if (isset($par['ref_num']) && $par['ref_num']>0) {
                // Make sure ref_num is that item & qty sufficient
                if (!isset($poitems_byline[$par['ref_num']]))
                    $check_errors[] = 'PO '.$params["po_id"].' Submitted ref_num '.$par['ref_num'].' is not valid';
                else if ($poitems_byline[$par['ref_num']]['item_id']!=$par['item_id'])
                    $check_errors[] = 'PO '.$params["po_id"].' Submitted ref_num '.$par['ref_num'].' is not the same item';
                else if ($poitems_byline[$par['ref_num']]['qty_order']<$par['grn_qty'])
                    $check_errors[] = 'PO '.$params["po_id"].' Submitted ref_num '.$par['ref_num'].' exceed ordered quantity';
                else {
                    // If all OK, input details into param
                    $params['items'][$k]['rev_num'] = $poitems_byline[$par['ref_num']]['current_rev'];
                    $params['items'][$k]['ref_num'] = $poitems_byline[$par['ref_num']]['line_num'];
                    $params['items'][$k]['item_id'] = $poitems_byline[$par['ref_num']]['item_id'];
                    $params['items'][$k]['item_desc'] = $poitems_byline[$par['ref_num']]['item_desc'];
                    $params['items'][$k]['unit_price'] = $poitems_byline[$par['ref_num']]['unit_price'];
                    $params['items'][$k]['disc_percent'] = $poitems_byline[$par['ref_num']]['disc_percent'];
                }
            }
            else {
                // Tally by item_id that qty sufficient
                if (!isset($poitems_byitem[$par['item_id']]))
                    $check_errors[] = 'PO '.$params["po_id"].' Submitted item_id '.$par['item_id'].' is not valid';
                else if ($poitems_byitem[$par['item_id']]['qty_order']<$par['grn_qty'])
                    $check_errors[] = 'PO '.$params["po_id"].' Submitted item_id '.$par['item_id'].' exceed ordered quantity';
                else {
                    // If all OK, input ref_num to params for grn processing
                    $params['items'][$k]['rev_num'] = $poitems_byitem[$par['ref_num']]['current_rev'];
                    $params['items'][$k]['ref_num'] = $poitems_byitem[$par['item_id']]['line_num'];
                    $params['items'][$k]['item_id'] = $poitems_byitem[$par['item_id']]['item_id'];
                    $params['items'][$k]['item_desc'] = $poitems_byitem[$par['item_id']]['item_desc'];
                    $params['items'][$k]['unit_price'] = $poitems_byitem[$par['item_id']]['unit_price'];
                    $params['items'][$k]['disc_percent'] = $poitems_byitem[$par['item_id']]['disc_percent'];
                }
            }
        }

        if (count($check_errors)>0) {
            // There's error, we are not going to do anything
            return NULL;
        }
        else {
            // Generate GRN
            $loc_id = trim($poitems[0]["loc_id"]);
            $trans_type = trim($poitems[0]["po_type"]);

            $grn_doc_prefix = 'G' . $trans_type . $loc_id;
            $grn_id = $this->getRefId($grn_doc_prefix, "GRN" . $grn_doc_prefix, "PMS", "CTL", "Ym", 15);

            $ins = array();
            $ins["coy_id"] = $params["coy_id"];
            $ins["grn_id"] = $grn_id;
            $ins["supp_id"] = $params["supp_id"];
            $ins["supp_do"] = $params["supp_do"];
            $ins["date_received"] = date('Y-m-d H:i:s');
            $ins["order_type"] = $params["order_type"];
            $ins["curr_id"] = $poitems[0]['curr_id'];
            $ins["po_type"] = $poitems[0]["po_type"];
            $ins["po_id"] = $params["po_id"];
            $ins["rev_num"] = $poitems[0]["current_rev"];
            $ins["loc_id"] = $params["loc_id"];
            $ins["created_by"] = $params["created_by"];
            $ins["created_on"] = date('Y-m-d H:i:s');
            $ins["modified_by"] = $params["created_by"];
            $ins["modified_on"] = date('Y-m-d H:i:s');
            $ins["status_level"] = $params["status_level"];
            $result1 = $this->db->insert('pms_grn_list', $ins);

            $ins_num = 0;
            $inv_trans = array();
            foreach ($params['items'] as $k=>$par) {

                if (isset($par['loc_id']) && $par['loc_id']!='') {
                    $loc_id = $par['loc_id'];
                }

                $ins_num++;
                $item_id = trim($par["item_id"]);

                $ins = array();
                $ins["coy_id"] = $params["coy_id"];
                $ins["grn_id"] = $grn_id;
                $ins["line_num"] = $ins_num;
                $ins["ref_id"] = $params["po_id"];
                $ins["ref_rev"] = $poitems[0]["current_rev"];
                $ins["ref_num"] = $par["ref_num"];
                $ins["item_id"] = $par["item_id"];
                $ins["item_desc"] = $par["item_desc"];
                $ins["grn_qty"] = $par["grn_qty"];
                $ins["unit_price"] = $par["unit_price"];
                $ins["disc_percent"] = $par["disc_percent"];
                $ins["initial_date"] = date('Y-m-d H:i:s');
                $ins["created_by"] = $params["created_by"];
                $ins["created_on"] = date('Y-m-d H:i:s');
                $ins["modified_by"] = $params["created_by"];
                $ins["modified_on"] = date('Y-m-d H:i:s');
                $ins["status_level"] = $params["status_level"];
                $ins["trans_id"] = '';
                $result2 = $this->db->insert('pms_grn_item', $ins);


                // Every item received, add to ims_inv_transaction
                $invtransparams['coy_id'] = $params["coy_id"];
                $invtransparams['trans_type'] = $trans_type;
                $invtransparams['movement_ind'] = '+';
                $invtransparams['ref_id2'] = $grn_id;
                $invtransparams['loc_id'] = $loc_id;
                $invtransparams['loc_id2'] = $loc_id;
                $invtransparams['po_id'] = trim($params["po_id"]);
                $invtransparams['po_rev_num'] = $par["rev_num"];
                $invtransparams['po_line_num'] = $par["ref_num"];
                $invtransparams['item_id'] = $item_id;
                $invtransparams['inv_qty'] = $par["grn_qty"];
                $invtransparams['inv_val'] = $par["grn_qty"] * $par["unit_price"];
                $invtransparams['fin_dim1'] = $poitems_byitem[$item_id]['fin_dim1'];
                $invtransparams['fin_dim2'] = $poitems_byitem[$item_id]['fin_dim2'];
                $invtransparams['fin_dim3'] = $poitems_byitem[$item_id]['fin_dim3'];
                $invtransparams['supp_csg'] = 'N';
                $invtransparams['created_by'] = $params["created_by"];
                $invtransparams['coy_id'] = $params["coy_id"];
                $inv_trans[] = $this->addInvtrans($invtransparams);

                if ($stk_deduct != NULL) {
                    // Stock deduction invoice available - Remove item from inventory
                    $invtransparams['trans_type'] = (substr($stk_deduct,0,2)=="HI") ? "HI" : "PS";
                    $invtransparams['movement_ind'] = '-';
                    $invtransparams['ref_id2'] = $stk_deduct;
                    $invtransparams['loc_id'] = $loc_id;
                    $invtransparams['loc_id2'] = $loc_id;
                    $invtransparams['po_id'] = trim($params["po_id"]);
                    $invtransparams['po_rev_num'] = $par["rev_num"];
                    $invtransparams['po_line_num'] = $par["ref_num"];
                    $invtransparams['item_id'] = $item_id;
                    $invtransparams['inv_qty'] = $par["grn_qty"];
                    $invtransparams['inv_val'] = $par["grn_qty"] * $par["unit_price"];
                    $invtransparams['fin_dim1'] = $poitems_byitem[$item_id]['fin_dim1'];
                    $invtransparams['fin_dim2'] = $poitems_byitem[$item_id]['fin_dim2'];
                    $invtransparams['fin_dim3'] = $poitems_byitem[$item_id]['fin_dim3'];
                    $invtransparams['supp_csg'] = 'N';
                    $invtransparams['created_by'] = $params["created_by"];
                    $invtransparams['coy_id'] = $params["coy_id"];
                    $inv_trans[] = $this->addInvtrans($invtransparams);
                }
                else {
                    // Add qty_on_hand to ims_inv_physical
                    $invtransparams['item_id'] = $item_id;
                    $invtransparams['loc_id'] = $loc_id;
                    $invtransparams['po_id'] = trim($params["po_id"]);
                    $invtransparams['po_rev_num'] = $par["rev_num"];
                    $invtransparams['po_line_num'] = $par["ref_num"];
                    $invtransparams['qty_on_hand'] = $par["grn_qty"];
                    $invtransparams['unit_price'] = $par["unit_price"];
                    $invtransparams['supp_csg'] = 'N';
                    $invtransparams['created_by'] = $params["created_by"];
                    $invtransparams['coy_id'] = $params["coy_id"];
                    $inv_trans[] = $this->addInvphysical($invtransparams);
                }

            }

            if ($result1 && $result2) {
                return array("code"=>1, "grn_id"=>trim($grn_id), "line_count"=>count($params['items']), "msg"=>'GRN '.trim($grn_id).' created', "inv_trans"=>$inv_trans );
            }
            else {
                return array("code"=>0, "msg"=>'Fail to create GRN' );
            }

        }

    }

    public function genImsmove($params){

        // Get item details
        $itemdetails = [];
        $sql_items = [];
        foreach ($params['items'] as $k=>$par)
            $sql_items[] = $par["item_id"];
        $sql = "SELECT ims_item_list.coy_id, ims_item_list.item_id, fin_dim1, fin_dim2, fin_dim3, MAX(item_desc) item_desc, MAX(uom_id) uom_id, AVG(unit_cost) unit_cost, MAX(supp_csg) supp_csg, '' bom_ind
                FROM ims_item_list
                LEFT JOIN ims_inv_physical ON ims_item_list.coy_id=ims_inv_physical.coy_id AND ims_item_list.item_id=ims_inv_physical.item_id
                WHERE ims_item_list.coy_id='CTL' and ims_item_list.item_id in ('" . implode("','", $sql_items) . "')
                GROUP BY ims_item_list.coy_id, ims_item_list.item_id, fin_dim1, fin_dim2, fin_dim3;";
        $itemdetail1 = $this->db->query($sql)->result_array();
        foreach ($itemdetail1 as $itemdetail2) {
            $item_id = trim($itemdetail2["item_id"]);
            $itemdetails[$item_id] = $itemdetail2;
        }

        // Generate ST
        $stkmove_id_prefix = trim($params["loc_id"]) . trim($params["loc_to"]);
        $trans_id = $this->getRefId($stkmove_id_prefix, "MOVE_TYPE" . $stkmove_id_prefix, "IMS", "CTL", "Ym", "15");

        $ins = array();
        $ins["coy_id"] = $params["coy_id"];
        $ins["trans_id"] = $trans_id;
        $ins["move_type"] = $params["move_type"];
        $ins["status_level"] = $params["status_level"];
        $ins["loc_id"] = $params["loc_id"];
        $ins["loc_to"] = $params["loc_to"];
        $ins["requester_id"] = $params['requester_id'];
        $ins["remarks"] = $params['remarks'];
        $ins["created_by"] = $params["created_by"];
        $ins["created_on"] = date('Y-m-d H:i:s');
        $ins["modified_by"] = $params["created_by"];
        $ins["modified_on"] = date('Y-m-d H:i:s');
        $result1 = $this->db->insert('ims_inv_movement', $ins);

        $ins_num = 0;
        $inv_trans = array();
        foreach ($params['items'] as $k=>$par) {

            $ins_num++;
            $item_id = trim($par["item_id"]);

            $ins = array();
            $ins["coy_id"] = $params["coy_id"];
            $ins["trans_id"] = $trans_id;
            $ins["movement_ind"] = 'T'; // $params["move_type"];
            $ins["line_num"] = ($k + 1);
            $ins["ref_id"] = $par["ref_id"];
            $ins["ref_num"] = $par["ref_num"];
            $ins["item_id"] = $item_id;
            $ins["trans_qty"] = $par["trans_qty"];

            $ins["item_desc"] = ($itemdetails[$item_id]) ? $itemdetails[$item_id]["item_desc"] : $par["item_desc"];
            $ins["uom_id"] = ($itemdetails[$item_id]) ? $itemdetails[$item_id]["uom_id"] : '';
            $ins["unit_cost"] = ($itemdetails[$item_id] && $itemdetails[$item_id]["unit_cost"]) ? $itemdetails[$item_id]["unit_cost"] : 0;
            $ins["supp_csg"] = ($itemdetails[$item_id] && $itemdetails[$item_id]["supp_csg"]) ? $itemdetails[$item_id]["supp_csg"] : '';
            $ins["bom_ind"] = ($itemdetails[$item_id]) ? $itemdetails[$item_id]["bom_ind"] : '';

            $ins["created_by"] = $params["created_by"];
            $ins["created_on"] = date('Y-m-d H:i:s');
            $ins["modified_by"] = $params["created_by"];
            $ins["modified_on"] = date('Y-m-d H:i:s');
            $result2 = $this->db->insert('ims_inv_movement_item', $ins);

        }

        if ($result1 && $result2) {
            return array("code"=>1, "trans_id"=>trim($trans_id), "line_count"=>count($params['items']), "msg"=>'ST '.trim($trans_id).' created' );
        }
        else {
            return array("code"=>0, "msg"=>'Fail to create ST' );
        }
    }

    public function addInvtrans($params){
        $loc_id2 = ($params['loc_id2']) ? $params['loc_id2'] : $params['loc_id'];
        $stkmove_id_prefix = trim($params["loc_id"]) . trim($loc_id2);
        $trans_id = $this->getRefId($stkmove_id_prefix, "STK_MOVE" . $stkmove_id_prefix, "IMS", "CTL", "y", "15");

        if (empty($params["po_rev_num"]))
            $params["po_rev_num"] = 0;

        $sql = "INSERT INTO ims_inv_transaction
                              (coy_id,trans_id,trans_type,status_level,trans_date,
                                  movement_ind,item_id,loc_id, 
                                  inv_qty,inv_value,initial_date,
                                  bin_id,lot_id,supp_csg, 
                                  ref_id,ref_rev,ref_num, ref_id2,
                                  fin_dim1,fin_dim2,fin_dim3,promo_id,
                                  created_by,created_on,modified_by,modified_on)
                            VALUES (
                                '".$params["coy_id"]."','$trans_id','".$params['trans_type']."',0,now(),
                                    '".$params["movement_ind"]."','".$params["item_id"]."','".$params["loc_id"]."',
                                    '".$params["inv_qty"]."','".$params["inv_val"]."', cast(now() as date), 
                                    '','','".$params["supp_csg"]."', 
                                    '".$params["po_id"]."','".$params["po_rev_num"]."','".$params["po_line_num"]."', '".$params["ref_id2"]."',
                                    '".$params['fin_dim1']."','".$params['fin_dim2']."','".$params['fin_dim3']."','',
                                    '".$params['created_by']."',now(),'".$params['created_by']."',now() ); ";
        $sqlexec = $this->db->query($sql);
        if ($sqlexec) {
            return array(
                "trans_table" => 'ims_inv_transaction',
                "item_id" => $params["item_id"],
                "trans_id" => $trans_id,
                "trans_type" => $params['trans_type'],
                "trans_date" => date('Y-m-d'),
                "movement_ind" => $params["movement_ind"],
                "ref_id1" => $params["po_id"],
                "ref_id2" => $params["ref_id2"]
            );
        }
        else {
            return NULL;
        }
    }

    public function addInvphysical($params){

        $sql = "SELECT MAX(line_num) max_line_num, MIN(line_num) min_line_num FROM ims_inv_physical WHERE coy_id='".$params["coy_id"]."' and item_id='".$params["item_id"]."' and loc_id='".$params["loc_id"]."'";
        $linenum = $this->db->query($sql)->row_array();

        $sql_linenum = ($linenum['min_line_num']!=1) ? '1' : $linenum['max_line_num']+1 ;
        $sql = "INSERT INTO ims_inv_physical (coy_id,item_id,loc_id,line_num, unit_cost,qty_on_hand,qty_reserved,supp_csg,
                        trans_date,initial_date, ref_id,ref_rev,ref_num, created_by,created_on,modified_by,modified_on)
                  VALUES (
                        '".$params["coy_id"]."','".$params["item_id"]."','".$params["loc_id"]."', $sql_linenum, 
                        '".$params["unit_price"]."','".$params["qty_on_hand"]."',0,'".$params["supp_csg"]."',
                        now(),CAST(now() AS DATE), '".$params["po_id"]."','". (($params["po_rev_num"]) ? $params["po_rev_num"] : '0') ."','".$params["po_line_num"]."', 
                        '".$params['created_by']."',now(),'".$params['created_by']."',now() ); ";
        $sqlexec = $this->db->query($sql);
        if ($sqlexec) {
            return array(
                "trans_table" => 'ims_inv_physical',
                "item_id" => $params["item_id"],
                "trans_id" => trim($params["coy_id"])."-".trim($params["item_id"])."-".trim($params["loc_id"])."-".$sql_linenum,
                "ref_id" => $params["po_id"],
            );
        }
        else {
            return NULL;
        }
    }

    public function moveInvphysical($params) {

        $move_db_trans = [];
        $move_physical = [];

        // Check if there's enough stock
        $sql = "SELECT ims_inv_physical.coy_id,ims_inv_physical.item_id,ims_inv_physical.loc_id,ims_inv_physical.line_num,
                        ims_inv_physical.qty_on_hand,ims_inv_physical.qty_reserved,
                        ims_inv_physical.unit_cost,ims_inv_physical.supp_csg,ims_inv_physical.initial_date,
                        ims_inv_physical.ref_id,ims_inv_physical.ref_rev,ims_inv_physical.ref_num,
                        ims_item_list.fin_dim1,ims_item_list.fin_dim2,ims_item_list.fin_dim3
                FROM ims_inv_physical
                LEFT JOIN ims_item_list ON ims_inv_physical.coy_id=ims_item_list.coy_id and ims_inv_physical.item_id=ims_item_list.item_id
                WHERE ims_inv_physical.coy_id='".$params['coy_id']."' and ims_inv_physical.item_id='".$params['item_id']."' and loc_id='".$params['loc_id']."'
                ORDER BY trans_date";
        $ori_physical = $this->db->query($sql)->result_array();
        $bal_qty = $params['trans_qty'];
        foreach ($ori_physical as $row) {
            $row_qty = $row['qty_on_hand'] - $row['qty_reserved'];
            if ($bal_qty > 0 && $row_qty > 0 && $row_qty >= $bal_qty) {
                $row['trans_qty'] = $bal_qty;
                $row['trans_bal'] = $row['qty_on_hand'] - $bal_qty;
                $move_physical[] = $row;
                $bal_qty = 0;
            }
            else if ($bal_qty > 0 && $row_qty > 0 && $row_qty < $bal_qty) {
                $row['trans_qty'] = $row_qty;
                $row['trans_bal'] = $row['qty_on_hand'] - $row_qty;
                $move_physical[] = $row;
                $bal_qty = $bal_qty - $row_qty;
            }
        }

        if ($bal_qty > 0 || count($move_physical) == 0) {
            // Cannot fulfill this inventory movement, do not do anything
            $move_physical = [];
            return NULL;
        }

        foreach ($move_physical as $phy) {

            // ims_inv_transaction remove from original
            $invtransparams = [];
            $invtransparams['coy_id'] = $params['coy_id'];
            $invtransparams['trans_type'] = $params['trans_type'];
            $invtransparams['movement_ind'] = '-';
            $invtransparams['ref_id2'] = $params['trans_id'];
            $invtransparams['loc_id'] = $params['loc_id'];
            $invtransparams['loc_id2'] = $params['loc_to'];
            $invtransparams['po_id'] = $phy['ref_id'];
            $invtransparams['po_rev_num'] = $phy['ref_rev'];
            $invtransparams['po_line_num'] = $phy['ref_num'];
            $invtransparams['item_id'] = $phy['item_id'];
            $invtransparams['inv_qty'] = $phy['trans_qty'];
            $invtransparams['inv_val'] = $phy["trans_qty"] * $phy["unit_cost"];
            $invtransparams['fin_dim1'] = $phy['fin_dim1'];
            $invtransparams['fin_dim2'] = $phy['fin_dim2'];
            $invtransparams['fin_dim3'] = $phy['fin_dim3'];
            $invtransparams['supp_csg'] = $phy['supp_csg'];
            $invtransparams['created_by'] = $params["created_by"];
            $move_db_trans[] = $this->addInvtrans($invtransparams);

            // ims_inv_physical minus qty or remove line
            if ($phy['qty_reserved'] == 0 && $phy['trans_bal'] == 0) {
                $sql = "DELETE FROM ims_inv_physical WHERE coy_id='".$phy['coy_id']."' AND item_id='".$phy['item_id']."' AND loc_id='".$phy['loc_id']."' AND line_num='".$phy['line_num']."' ";
                $this->db->query($sql);

                $move_db_trans[] = array(
                    "trans_table" => 'ims_inv_physical',
                    "item_id" => $phy["item_id"],
                    "trans_id" => trim($phy["coy_id"])."-".trim($phy["item_id"])."-".trim($phy["loc_id"])."-".$phy['line_num'],
                    "ref_id" => $params["po_id"],
                    "action" => 'DELETE'
                );
            }
            else {
                $sql = "UPDATE ims_inv_physical SET qty_on_hand = '".$phy['trans_bal']."', modified_by='".$phy['created_by']."', modified_on=NOW()
                        WHERE coy_id='".$phy['coy_id']."' AND item_id='".$phy['item_id']."' AND loc_id='".$phy['loc_id']."' AND line_num='".$phy['line_num']."' ";
                $this->db->query($sql);

                $move_db_trans[] = array(
                    "trans_table" => 'ims_inv_physical',
                    "item_id" => $phy["item_id"],
                    "trans_id" => trim($phy["coy_id"])."-".trim($phy["item_id"])."-".trim($phy["loc_id"])."-".$phy['line_num'],
                    "ref_id" => $params["po_id"],
                    "action" => 'UPDATE'
                );
            }

            // ims_inv_transaction add to destination
            $invtransparams = [];
            $invtransparams['coy_id'] = $params['coy_id'];
            $invtransparams['trans_type'] = $params['trans_type'];
            $invtransparams['movement_ind'] = '+';
            $invtransparams['ref_id2'] = $params['trans_id'];
            $invtransparams['loc_id'] = $params['loc_to'];
            $invtransparams['loc_id2'] = $params['loc_to'];
            $invtransparams['po_id'] = $phy['ref_id'];
            $invtransparams['po_rev_num'] = $phy['ref_rev'];
            $invtransparams['po_line_num'] = $phy['ref_num'];
            $invtransparams['item_id'] = $phy['item_id'];
            $invtransparams['inv_qty'] = $phy['trans_qty'];
            $invtransparams['inv_val'] = $phy["trans_qty"] * $phy["unit_cost"];
            $invtransparams['fin_dim1'] = $phy['fin_dim1'];
            $invtransparams['fin_dim2'] = $phy['fin_dim2'];
            $invtransparams['fin_dim3'] = $phy['fin_dim3'];
            $invtransparams['supp_csg'] = $phy['supp_csg'];
            $invtransparams['created_by'] = $params["created_by"];
            $move_db_trans[] = $this->addInvtrans($invtransparams);

            // ims_inv_physical insert new row
            $invtransparams = [];
            $invtransparams['item_id'] = $phy['item_id'];
            $invtransparams['loc_id'] = $params['loc_to'];
            $invtransparams['po_id'] = $phy['ref_id'];
            $invtransparams['po_rev_num'] = $phy["rev_num"];
            $invtransparams['po_line_num'] = $phy["ref_num"];
            $invtransparams['qty_on_hand'] = $phy['trans_qty'];
            $invtransparams['unit_price'] = $phy["unit_cost"];
            $invtransparams['supp_csg'] = $phy["supp_csg"];
            $invtransparams['created_by'] = $params["created_by"];
            $invtransparams['coy_id'] = $params["coy_id"];
            $move_db_trans[] = $this->addInvphysical($invtransparams);

        }

        return $move_db_trans;
    }

    public function updDelvtrans($delvparam){
        if ($delvparam['txn_type']=="SMS") {
            if (isset($delvparam['do_id'])){
                // PO generated & Delievered
                $sql = "UPDATE sms_invoice_item
                        SET trans_id='" . $delvparam['po_id'] . "', do_id='".$delvparam['do_id']."', delv_date=now(), qty_picked='".$delvparam['qty_picked']."',
                            status_level=2, modified_by='" . $delvparam['created_by'] . "', modified_on=now()
                        WHERE coy_id='" . $delvparam['coy_id'] . "' and invoice_id='" . $delvparam['txn_id'] . "' and line_num='" . $delvparam['line_num'] . "' ";
                $res = $this->db->query($sql);
            }
            else {
                // PO generated
                $sql = "UPDATE sms_invoice_item
                        SET trans_id='" . $delvparam['po_id'] . "', modified_by='" . $delvparam['created_by'] . "', modified_on=now()
                        WHERE coy_id='" . $delvparam['coy_id'] . "' and invoice_id='" . $delvparam['txn_id'] . "' and line_num='" . $delvparam['line_num'] . "' ";
                $res = $this->db->query($sql);
            }
        }
        else if ($delvparam['txn_type']=="POS") {
            if (isset($delvparam['do_id'])) {
                $sql = "UPDATE pos_transaction_item
                    SET status_level=2, modified_by='" . $delvparam['created_by'] . "', modified_on=now()
                    WHERE coy_id='" . $delvparam['coy_id'] . "' and trans_id='" . $delvparam['txn_id'] . "' and line_num='" . $delvparam['line_num'] . "' ";
                $res = $this->db->query($sql);
            }
        }
        return $res;
    }

    // $this->getRefId($stkmove_id_prefix, "STK_MOVE", "IMS", "CTL");
    public function getRefId($doc_prefix, $trans_prefix = 'IP', $sys_id = 'PMS', $coy_id = 'CTL', $doc_date="y", $doc_length=13){
        $prefix_yr = ($doc_date!='') ? date($doc_date) : '';
        $trans_prefix = $trans_prefix . $prefix_yr;

        $is_exists = $this->db->query("select 1 from sys_trans_list where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '" . $trans_prefix . "'")->num_rows();
        if ($is_exists === 0) {
            $this->db->query("insert into sys_trans_list (coy_id, sys_id, trans_prefix, next_num, sys_date) VALUES ('$coy_id', '$sys_id', '". $trans_prefix ."', 1, now())");
        }
        $next_num = $this->db->query("select next_num from sys_trans_list where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '" . $trans_prefix . "'")->result_array();
        $this->db->query("update sys_trans_list set next_num = " . $next_num[0]['next_num'] . "+1, sys_date = now() where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '" . $trans_prefix . "'");

        $po_prefix = $doc_prefix . $prefix_yr;
        $po_id = $po_prefix . sprintf("%0" . (string)($doc_length - strlen($po_prefix)) . "s", $next_num[0]['next_num']);
        return $po_id;
    }

    public function getLocInfo($loc_id, $coy='CTL'){
        $sql = "SELECT loc_id,loc_name,loc_addr,fin_dim1 
                    FROM ims_location_list  
                    WHERE (coy_id='".$coy."') AND (loc_id='".$loc_id."') limit 1;";
        $location = $this->db->query($sql)->row_array();
        return $location;
    }

    public function getNonPrescribeGds($items, $coy='CTL'){
        $sql = "select i.item_id,i.item_desc,
                    (CASE WHEN pi.include_ind='N' OR p4.include_ind='N' OR p3.include_ind='N' OR p2.include_ind='N' THEN 'N' ELSE '' END) included_ind
                    from ims_item_list i
                    left join ims_item_pgs pi ON pi.coy_id=i.coy_id and pi.item_id=i.item_id and pi.include_ind='N'
                    left join ims_item_pgs p2 ON p2.coy_id=i.coy_id and p2.inv_dim2=i.inv_dim2 and p2.include_ind='N'
                    left join ims_item_pgs p3 ON p3.coy_id=i.coy_id and p3.inv_dim3=i.inv_dim3 and p3.include_ind='N'
                    left join ims_item_pgs p4 ON p4.coy_id=i.coy_id and p4.inv_dim4=i.inv_dim4 and p4.include_ind='N'
                    where i.coy_id='".$coy."' AND i.item_id in ('" . implode("','", $items). "')";
        $results = $this->db->query($sql)->row_array();
        return ($results) ? true : false;
    }

    public function getTaxinfo($tax_id, $return_field='', $coy='CTL'){
        $sql = "SELECT tax_id,tax_percent FROM coy_tax_list WHERE coy_id = '$coy' AND tax_id = '$tax_id' ORDER BY modified_on desc limit 1 ";
        $res = $this->db->query($sql)->row_array();
        if ($return_field=='')
            return $res;
        else
            return $res[$return_field];
    }

    public function emailPo($po_id, $params=[], $emailPo=8) {
        // $emailPo: 0 - none, 1 - vendor only, 2 - mrd only, 8 - vendor & mrd

        $email_subj = 'New Purchase Order: '.trim($params["po_id"]).' for ('.trim($params["supp_id"]).') from '.$params["coy_name"];

        $email_msg = 'New Purchase Order: '.trim($params["po_id"]).' to: '.$params["supp_name"];
        $email_msg.= '<br>Please confirm the PO Delivery Date and Quantity within 48 hours via CHVOICES portal.<br>';
        foreach($params["items"] as $emailitem) {
            $email_msg .= '<br>' . $emailitem["item_id"] . ' ' . $emailitem["item_desc"];
        }
        $email_msg.= '<br><br><br>Please DO NOT send PO copy or Tax Invoice when deliver stock to store.';
        if (isset($params['remarks']))
            $email_msg.= '<br>' . $params['remarks'];

        $email_rec = ($emailPo==8 || $emailPo==1) ? $params["email_addr3"] : '';
        $email_cc = ($emailPo==8 || $emailPo==2) ? 'mrd.hc@challenger.sg; kunalan@challenger.sg; jenny@challenger.sg;' : '';
        if (ENVIRONMENT!="production") {
            // Overwrite for testing
            $email_rec = 'yongsheng@challenger.sg';
            $email_cc = '';
        }

        $this->cherps_email($email_subj,$email_msg,$email_rec,$email_cc);
    }

    public function cherps_email($subject,$message,$email='yongsheng@challenger.sg',$email_cc='') {
        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = $email_cc;
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML'; 
        $rtn = $this->_SendEmail($params);
    }

}

?>
