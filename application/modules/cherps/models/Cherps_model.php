<?php

/* YS
 * 19-12-2017
 */
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Cherps_model extends Base_Common_Model {

    private $apiurl;

    public function __construct() {
        parent::__construct();

        if (ENVIRONMENT=="production"){
            $this->apiurl = 'https://www.hachi.tech/api/';
        }
        else {
            $this->apiurl = 'https://web3.sghachi.com/api/';
        }
    }

    public function build_url($mode,$usr_id,$id,$state) {
        $uri = urlencode( base64_encode( json_encode( array(
            "m"=>$mode,
            "u"=>$usr_id,
            "i"=>trim($id),
            "s"=>$state,
            "t"=>time()
        ) ) ) );
        $url = base_url('cherps/'.$uri);
        return $url;
    }

    public function pn_notify($sbj, $url, $usr_id, $msg=''){
        // Call push notification API
//        $q['staffid'] = $usr_id;
//        $q['ponum'] = $sbj;
//        $q['url'] = $url;
//        $q['title'] = $sbj; //'Purchase Order #'.$sbj.' approval';
//        $q['msg'] = $msg; //'PO requires approval, please check';
//        $call = $this->_doCurlOTP('chstaff2/pushNotifyPO', json_encode($q));
//        $resp = json_decode($call);
//        $code = $resp->code;

        $q['staffid'] = trim($usr_id);
        $q['url'] = $url;
        $q['subject'] = $sbj; //'Purchase Order #'.$sbj.' approval';
        $q['msg'] = $msg; //'PO requires approval, please check';
        $call = $this->_doChugoPush(json_encode($q));
        $resp = json_decode($call);
        $code = $resp->status;

        if ($code==1) {
            return true;
        }
        else {
            log_message('debug', "PN_NOTIFY ($usr_id,$sbj), $call" );

            if ($this->input->get('debug')) {
                echo $call.'<hr>';
            }
            return false;
        }
    }

    public function otp_check($params){
        // Call external API to check otp
        $otp = $this->session->userdata('otp_memory');
        if ($otp[0]==$params['otp']) {
            $this->session->unset_userdata('otp_memory');
            $q['staffid'] = $params['usr_id'];
            $q['ponum'] = $params['po_id'];
            $call = $this->_doCurlOTP('chstaff2/releasePOOTP', json_encode($q));
            $resp = json_decode($call);
            return ($resp->code == 1) ? true : false;
        }
        else {
            return false;
        }
    }

    public function otp_send($params){
        // Call external API to send new otp
        $q['staffid'] = $params['usr_id'];
        $q['ponum'] = $params['po_id'];
        $call = $this->_doCurlOTP('chstaff2/getPOOTP', json_encode($q));
        $resp = json_decode($call);
        $respresult = json_decode($resp->result);

        // Save the latest OTP to session
        if ($respresult->otp != '') {
            $this->session->set_userdata('otp_memory', [$respresult->otp, time()]);
            return true;
        }
        else {
            return false;
        }
    }

    private function _doCurlOTP($uri,$query) {
        $headers[] = "X_AUTHORIZATION:  NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiurl.$uri);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    private function _doChugoPush($query) {
        $url = 'https://chugo-api.challenger.sg/api/notification/pushNotificationPOPR/8KGqbFVswr';
        $ch = curl_init();

        $headers[] = "Content-Type:  application/json";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    public function po_set_status($params){
        if ($params['status_level']==1){
            $sql = 'UPDATE pms_po_item SET status_level=?, modified_by=?, modified_on=NOW() WHERE po_id=? and rev_num=?';
            $res = $this->db->query($sql, [ $params['status_level'],$params['approve_by'],$params['po_id'],$params['current_rev'] ] );
            if ($res) {
                $sql = 'UPDATE pms_po_list SET approve_by=?, approve_date=NOW(), status_level=?, modified_by=?, modified_on=NOW() WHERE po_id=? and rev_num=?';
                $res = $this->db->query($sql, [$params['approve_by'], $params['status_level'], $params['approve_by'], $params['po_id'], $params['current_rev']]);
            }
        }
        else {
            $sql = 'UPDATE pms_po_list SET approve_by=?, modified_by=?, modified_on=NOW() WHERE po_id=? and rev_num=?';
            $res = $this->db->query($sql, [ $params['approve_by'],$params['approve_by'],$params['po_id'],$params['current_rev'] ] );
        }
        return ($res) ? true : false;
    }

    public function get_po_list($po_id){
        $sql = 'select
                    RTRIM(po_id) po_id,rev_num,current_rev,po_date,status_level, RTRIM(supp_id) supp_id ,supp_name,loc_id,delv_date, approve_date,
                    (select usr_name from sys_usr_list where usr_id=pms_po_list.approve_by limit 1) approve_by_name,
                    (select email_addr from sys_usr_list where usr_id=pms_po_list.approve_by limit 1) approve_by,
                    (select email_addr from sys_usr_list where usr_id=pms_po_list.buyer_id limit 1) buyer_id,
                    (select email_addr from sys_usr_list where usr_id=pms_po_list.requester_id limit 1) requester_id, 
                    (select email_addr from sys_usr_list where usr_id=pms_po_list.created_by limit 1) created_by
                from pms_po_list 
                where po_id=? and rev_num=current_rev';
        $res = $this->db->query($sql, [ $po_id ] )->row_array();
        return $res;
    }

    public function pr_set_status($params){
        if ($params['status_level']==1){
            $sql = 'UPDATE pms_pr_item SET status_level=?, modified_by=?, modified_on=NOW() WHERE pr_id=? and rev_num=?';
            $res = $this->db->query($sql, [ $params['status_level'],$params['approve_by'],$params['pr_id'],$params['current_rev'] ] );
            if ($res) {
                $sql = 'UPDATE pms_pr_list SET approve_by=?, approve_date=NOW(), status_level=?, modified_by=?, modified_on=NOW() WHERE pr_id=? and rev_num=?';
                $res = $this->db->query($sql, [$params['approve_by'], $params['status_level'], $params['approve_by'], $params['pr_id'], $params['current_rev']]);
            }
        }
        else {
            $sql = 'UPDATE pms_pr_list SET approve_by=?, modified_by=?, modified_on=NOW() WHERE pr_id=? and rev_num=?';
            $res = $this->db->query($sql, [ $params['approve_by'],$params['approve_by'],$params['pr_id'],$params['current_rev'] ] );
        }
        return ($res) ? true : false;
    }

    public function get_pr_list($po_id){
        $sql = 'select
                    RTRIM(pr_id) pr_id,rev_num,current_rev,pr_date,status_level, RTRIM(supp_id) supp_id ,supp_name,loc_id,delv_date, approve_date,
                    (select usr_name from sys_usr_list where usr_id=pms_pr_list.approve_by limit 1) approve_by_name,
                    (select email_addr from sys_usr_list where usr_id=pms_pr_list.approve_by limit 1) approve_by,
                    (select email_addr from sys_usr_list where usr_id=pms_pr_list.buyer_id limit 1) buyer_id,
                    (select email_addr from sys_usr_list where usr_id=pms_pr_list.requester_id limit 1) requester_id,
                    (select email_addr from sys_usr_list where usr_id=pms_pr_list.created_by limit 1) created_by
                from pms_pr_list
                where pr_id=? and rev_num=current_rev';
        $res = $this->db->query($sql, [ $po_id ] )->row_array();
        return $res;
    }

    public function get_url_key($key){
        $sql = "select ref_type,ref_id,
                    CASE WHEN ref_type='VRC' THEN (select supp_id from pms_claim_list WHERE trans_id=ref_id limit 1) 
                         WHEN ref_type='PO' THEN (select supp_id from pms_po_list WHERE po_id=ref_id limit 1) 
                    END supp_id
                from o2o_url_key where ref_key = ?; ";
        $res = $this->db->query($sql, [ $key ] )->row_array();
        return $res;
    }

    public function get_pms_limit($usr_id){
        $sql = "select po_limit,
                        concat ( coalesce((select usr_name from sys_usr_list where usr_id=alt_approver1),'') , 
                            (CASE WHEN alt_approver2!='' THEN ', ' ELSE '' END) , 
                            coalesce((select usr_name from sys_usr_list where usr_id=alt_approver2),'') ) alt_approvers
                    from pms_buyer_limit where coy_id='CTL' and buyer_id = ? ";
        $res = $this->db->query($sql, [ $usr_id ] )->row_array();
        return $res;
    }

    public function email_po($po_id,$email,$subject,$message,$attach,$usr_id){

        $params["from"] = 'chvoices@challenger.sg';
        $params["to"] = $email;
        $params["cc"] = '';
        $params["subject"] = $subject;
        $params["message"] = $message;
        //$params["attachment"] = $attach;
        $rtn = $this->_SendEmail($params);

//        $params['email'] = $email;
//        $params['subject'] = $subject;
//        $params['message'] = $message;
//        $params['attach'] = $attach;
//        $rtn = $this->_SendEmailbySmtp($params);

        $created_by = $usr_id;
        $q_attach = ($attach!='') ? " (Attach: $attach)" : '' ;
        if ($rtn) {
            $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,url_link,msg_request,status_code,status_text,msg_response,created_by,created_on,modified_by,modified_on)
                        VALUES ( 'CTL','PO','$po_id','mailto: $email','$subject $q_attach',1,'OK','{\"sender-ip\":\"".substr(getRealIpAddr(),0,15)."\"}','$created_by',NOW(),'$created_by',NOW() ); ";
            $query = $this->db->query($sql);
        }
        else {
            $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,url_link,msg_request,status_code,status_text,msg_response,created_by,created_on,modified_by,modified_on)
                        VALUES ( 'CTL','PO','$po_id','mailto: $email','$subject $q_attach',0,'FAIL','{\"sender-ip\":\"".substr(getRealIpAddr(),0,15)."\"}','$created_by',NOW(),'$created_by',NOW() ); ";
            $query = $this->db->query($sql);
        }

        return $rtn;
    }

    public function usr_get_detail($usr_id,$coy_id){

        $sql = "select rtrim(usr_id) usr_id,usr_name,email_addr,mobile_phone,
                    case when sec_pass <> '' then 'Y' else 'N' end pwd_saved, pwd_changed,
                    revoked_by,revoked_date,resigned_date,join_date,
                    upper(right(sec_pass,10)) [pin]
                from sys_usr_list where usr_id = ?";
        $sys = $this->db->query($sql, [ $usr_id ] )->row_array();

        $sql = "select usr_id,staff_id,staff_name,preferred_name,birth_date,
						block_no+' '+street_name+' '+unit_house_no+' ('+postal_code+')' as full_addr,
						contact1,email_address,
						zone,loc_id,dept_desc,sub_dept,job_designation,
						date_joined,resign_date,status_level
					 from coy_employee_list where usr_id = ?";
        $emp = $this->db->query($sql, [ $usr_id ] )->row_array();

        $sql = "select coy_id,rtrim(staff_id) staff_id,sel_coy,security_level,shift_id,loc_id,fin_dim1,fin_dim2,fin_dim3 
                from coy_usr_list where usr_id = ?";
        $coy = $this->db->query($sql, [ $usr_id ] )->result_array();

        $sql = "select -- (CASE WHEN grp_desc like '%admin%' then 0 when grp_desc like '%manager%' then 1 when grp_desc like '%supervisor%' then 2 when grp_desc like '%user%' then 8 else 6 end) sn, 
                    rtrim(g.grp_id) grp_id,g.grp_desc,
					CASE WHEN r.usr_id IS NOT NULL THEN 'Y' ELSE 'N' END [status_level],
					rtrim(coalesce(r.coy_id,'')) [grant_coy], 
					rtrim(coalesce(r.usr_id,'')) [grant_ref]
				from sss_grp_list g 
				left join (
					select coy_id,grp_id,usr_id from sss_grp_usr_list where usr_id = ? and coy_id = ?
				) r ON r.coy_id=g.coy_id and r.grp_id=g.grp_id
				where g.coy_id = ?
				order by status_level DESC, grp_desc";
        $grp = $this->db->query($sql, [ $usr_id,$coy_id,$coy_id ] )->result_array();

        $sql = "select m.sys_id,m.func_num,m.menu_num,m.menu_desc, 
                  CASE WHEN r.usr_id IS NOT NULL THEN 'Y' ELSE 'N' END [status_level], 
                  coalesce([grant_type],'') [grant_type], 
                  rtrim(coalesce(r.coy_id,'')) [grant_coy], 
                  rtrim(coalesce(r.usr_id,'')) [grant_ref]
                from sys_menu_list m
				left join (
                    select 'USR' [grant_type],coy_id,usr_id,sys_id,func_num,menu_num from sss_usr_menu_list 
                      where usr_id = ? and coy_id = ?
                    UNION
                    select 'GRP' [grant_type],coy_id,grp_id,sys_id,func_num,menu_num from sss_grp_menu_list 
                      where rtrim(coy_id)+rtrim(grp_id) in (select rtrim(coy_id)+rtrim(grp_id) from sss_grp_usr_list where usr_id = ? and coy_id = ?)
                ) r on m.sys_id=r.sys_id and m.func_num=r.func_num and m.menu_num=r.menu_num
                ORDER BY sys_id,func_num,menu_num";
        $menu = $this->db->query($sql, [ $usr_id,$coy_id,$usr_id,$coy_id ] )->result_array();

        $sql = "select sys_id,type_code,rtrim(type_data) type_data, 'USR' [grant_type], rtrim(coalesce(coy_id,'')) [grant_coy], rtrim(coalesce(usr_id,'')) [grant_ref] from sss_usr_type_list where usr_id = ? and coy_id = ?
                UNION
                select sys_id,type_code,rtrim(type_data) type_data, 'GRP' [grant_type], rtrim(coalesce(coy_id,'')) [grant_coy], rtrim(coalesce(grp_id,'')) [grant_ref] from sss_grp_type_list where rtrim(coy_id)+rtrim(grp_id) in (select rtrim(coy_id)+rtrim(grp_id) from sss_grp_usr_list where usr_id = ? and coy_id = ?)";
        $type = $this->db->query($sql, [ $usr_id,$coy_id,$usr_id,$coy_id ] )->result_array();

        $sql = "select sys_id,set_code,rtrim(set_data) set_data, 'USR' [grant_type], rtrim(coalesce(coy_id,'')) [grant_coy], rtrim(coalesce(usr_id,'')) [grant_ref] from sss_usr_setting_list where usr_id = ? and coy_id = ?
                UNION
                select sys_id,set_code,rtrim(set_data) set_data, 'GRP' [grant_type], rtrim(coalesce(coy_id,'')) [grant_coy], rtrim(coalesce(grp_id,'')) [grant_ref] from sss_grp_setting_list where rtrim(coy_id)+rtrim(grp_id) in (select rtrim(coy_id)+rtrim(grp_id) from sss_grp_usr_list where usr_id = ? and coy_id = ?)";
        $setting = $this->db->query($sql, [ $usr_id,$coy_id,$usr_id,$coy_id ] )->result_array();

        $rtn = array(
            'sys' => $sys,
            'emp' => $emp,
            'coy' => $coy,
            'grp' => $grp,
            'menu' => $menu,
            'types' => $type,
            'settings' => $setting
        );
        return (!empty($sys)) ? $rtn : NULL ;
    }

    public function usr_adminstration($usr_id, $admin_id) {

        $action = $this->input->post('action');
        $coy_id = $this->input->post('coy');

        $actions = array();
        if ($action=="coy_loc") {
            $loc_id = $this->input->post('loc');
            $sql = "UPDATE coy_usr_list SET loc_id='$loc_id', fin_dim1=(select fin_dim1 from ims_location_list where coy_id='$coy_id' and loc_id='$loc_id'), modified_by=?, modified_on=NOW() 
                      WHERE coy_id = ? and usr_id = ?; ";
            $query = $this->db->query($sql, [$admin_id,$coy_id,$usr_id] );
            if ($query){
                $actions[] = "Company location updated to $loc_id.";
            }
        }
        if ($action=="sss_grp") {
            $submit = $this->input->post('submit');
            $grp_id = $this->input->post('grp');
            if ($submit=="APPROVE") {
                $sql = "INSERT INTO sss_grp_usr_list VALUES ('$coy_id','$grp_id','$usr_id');
                        INSERT INTO temp_sss_usr_log values ('$coy_id','APPROVE','MIS_ADM Add Rights','$usr_id','$grp_id','',?,NOW()); ";
                $query = $this->db->query($sql, [$admin_id]);
                if ($query) {
                    $actions[] = "Added to group $grp_id.";
                }
            }
            else if ($submit=="REVOKE") {
                $sql = "DELETE FROM sss_grp_usr_list WHERE coy_id='$coy_id' AND grp_id='$grp_id' AND usr_id='$usr_id';
                        INSERT INTO temp_sss_usr_log values ('$coy_id','REVOKE','MIS_ADM Remove Rights','$usr_id','$grp_id','',?,NOW()); ";
                $query = $this->db->query($sql, [$admin_id]);
                if ($query) {
                    $actions[] = "Added to group $grp_id.";
                }
            }
        }

        return $actions;
    }

    public function usr_verifyadmin($usr_id,$pin='',$grp='MIS_ADM'){
        if ($pin!='') {
            $sql = "select rtrim(u.usr_id) usr_id from sys_usr_list u join sss_grp_usr_list g on g.usr_id=u.usr_id where g.grp_id='$grp' and u.email_addr = ? and RIGHT(u.sec_pass,10) = ?; ";
            $rtn = $this->db->query($sql, [$usr_id, $pin])->row_array();
        }
        else {
            $sql = "select rtrim(u.usr_id) usr_id from sys_usr_list u join sss_grp_usr_list g on g.usr_id=u.usr_id where g.grp_id='$grp' and u.usr_id = ?; ";
            $rtn = $this->db->query($sql, [$usr_id])->row_array();
        }
        return $rtn;
    }

    public function usr_login($usr_id,$usr_pwd){
        $sql = "SELECT DISTINCT b.usr_id,b.sec_pass,c.*,upper(job_designation) as position,d.tel_code
                    FROM sys_usr_list b 
                    INNER JOIN coy_usr_list a on a.usr_id = b.usr_id 
                    INNER JOIN coy_employee_list c on b.usr_id = c.usr_id
                    LEFT JOIN (SELECT loc_id,max(tel_code)tel_code FROM ims_location_list GROUP BY loc_id) d ON c.loc_id = d.loc_id
                    WHERE (coalesce(c.email_address,'') = ? or a.staff_id = ? or a.usr_id = ?) and c.coy_id = 'CTL'";
        $rtn = $this->db->query($sql, [$usr_id,$usr_id,$usr_id])->row_array();
        if ($rtn['sec_pass']==md5($usr_pwd)) {
            return $rtn;
        }
        else {
            return NULL;
        }
    }

    public function upd_b2b_setting($params){
        $sql = "SELECT * FROM b2b_setting_list WHERE sys_id='".$params['sys_id']."' AND set_code='".$params['set_code']."' AND set_default='".$params['set_default']."' ";
        $rtn = $this->db->query($sql)->row_array();
        if ($rtn){
            $sql = "UPDATE b2b_setting_list 
                        SET remarks='".$params['remarks']."', modified_by='".$params['created_by']."', modified_on=NOW()
                        WHERE sys_id='".$params['sys_id']."' AND set_code='".$params['set_code']."' AND set_default='".$params['set_default']."' ";
            $rtnx = $this->db->query($sql);
        }
        else {
            $sql = "INSERT INTO b2b_setting_list (sys_id,set_code,set_default,set_desc,remarks,created_by,created_on,modified_by,modified_on)
                      VALUES ('".$params['sys_id']."','".$params['set_code']."','".$params['set_default']."','Account','".$params['remarks']."', '".$params['created_by']."',NOW(),'".$params['created_by']."',NOW() );";
            $rtnx = $this->db->query($sql);
        }

        // Update back to cherps1
        $cherps_mssql = $this->load->database('cherps_hachi', TRUE);
        $sql = "SELECT * FROM b2b_setting_list WHERE sys_id='".$params['sys_id']."' AND set_code='".$params['set_code']."' AND set_default='".$params['set_default']."' ";
        $rtn = $cherps_mssql->query($sql)->row_array();
        if ($rtn){
            $sql = "UPDATE b2b_setting_list 
                        SET remarks='".$params['remarks']."', modified_by='".$params['created_by']."', modified_on=GETDATE()
                        WHERE sys_id='".$params['sys_id']."' AND set_code='".$params['set_code']."' AND set_default='".$params['set_default']."' ";
            $rtnx1 = $cherps_mssql->query($sql);
        }
        else {
            $sql = "INSERT INTO b2b_setting_list (sys_id,set_code,set_default,set_desc,remarks,created_by,created_on,modified_by,modified_on)
                      VALUES ('".$params['sys_id']."','".$params['set_code']."','".$params['set_default']."','Account','".$params['remarks']."', '".$params['created_by']."',GETDATE(),'".$params['created_by']."',GETDATE() );";
            $rtnx1 = $cherps_mssql->query($sql);
        }

        return $rtnx;
    }

    public function upd_hachi_pay($param,$usr_id){
        $invoice_id = $param['invoice_id'];
        $sql = "update sms_invoice_list set status_level=1 where coy_id='CTL' and invoice_id='$invoice_id' and status_level=0; ";
        $this->db->query($sql);
        $sql = "update sms_invoice_item set status_level=1 where coy_id='CTL' and invoice_id='$invoice_id' and status_level=0; ";
        $this->db->query($sql);

        if ($param['pay_mode']!='') {
            $sql = "SELECT count(*) num FROM sms_payment_list WHERE coy_id='CTL' and invoice_id='$invoice_id'";
            $res = $this->db->query($sql)->result_array();
            $num = isset($res[0]['num']) ? $res[0]['num'] + 1 : 1;

            $fin_dim2 = $param['pay_mode'];
            $trans_amount = $param['trans_amount'];
            $pay_mode = ($fin_dim2 == 'NETS') ? 'NETS' : 'CARD';
            $trans_ref1 = ($pay_mode == 'NETS') ? '0000000000000000000' : 'XXXXXXXXXXXX' . substr($param['trans_ref'], 0, 4);
            $trans_ref2 = ($pay_mode == 'NETS') ? '00000000' : 'XXXX';
            $trans_ref3 = ($pay_mode == 'NETS') ? $param['trans_ref'] : substr($param['trans_ref'], 5, 6);
            $sql = "INSERT INTO sms_payment_list
                    SELECT 'CTL','$invoice_id',$num,'$pay_mode',$trans_amount,'$trans_ref1','$trans_ref2','$trans_ref3','','','','$fin_dim2','',1,'$usr_id',NOW(),'$usr_id',NOW()";
            $this->db->query($sql);
        }

        $sql = "insert into o2o_url_key values ('HI','$invoice_id', NEWID() ) ";
        $this->db->query($sql);

        $sql = "select 'https://www.hachi.tech/invoice/hachi-cherps-url/'+ref_key url from o2o_url_key where ref_id='$invoice_id' and ref_type='HI'";
        $rtn = $this->db->query($sql)->row_array();

        return $rtn['url'];
    }

    public function get_pos_trans($mode,$trans_id){
        if ($mode=="summary"){
            $sql = "select l.coy_id,rtrim(l.trans_id) trans_id,l.trans_date,l.email_addr,i.line_num,rtrim(i.item_id) item_id,i.item_desc,i.trans_qty, m.item_type, m.inv_dim4,
                        o.lot_id,o.string_udf3,o.datetime_udf1
                    from pos_transaction_list l
                    join pos_transaction_item i on l.coy_id=i.coy_id and l.trans_id=i.trans_id and i.status_level>=0
                    left join pos_transaction_lot o on o.coy_id=i.coy_id and o.trans_id=i.trans_id and o.line_num=i.line_num
                    join ims_item_list m on m.coy_id=i.coy_id and m.item_id=i.item_id
                    where l.coy_id='CTL' and l.trans_id='".strtoupper($trans_id)."' and l.status_level>=0
                    ORDER BY i.line_num";
            $res = $this->db->query($sql)->result_array();
            if ($res){
                $sql = "select rtrim(pay_mode) pay_mode,line_num,trans_amount,fin_dim2 from pos_payment_list where coy_id='CTL' and trans_id='".strtoupper($trans_id)."'";
                $pay = $this->db->query($sql)->result_array();
                return array("trans"=>$res,"pay"=>$pay);
            }
            return NULL;
        }
        if ($mode=="blackhawk"){
            $sql = "select trans_id,created_on send_on,lot_id card_no,resp_code,case when resp_code='00' then 'ACTIVATED' else 'ERROR: '+resp_code END resp_msg from b2b_chitunes_message where lot_id='".trim($trans_id)."' order by created_on";
            $history = $this->db->query($sql)->result_array();
            return $history;
        }
        if ($mode=="ponotes"){
            $pos = strpos($trans_id,'***');
            $id1 = substr($trans_id,0,$pos);
            $id2 = substr($trans_id,($pos+3));
            $sql = "select invoice_id trans_id,modified_on trigger_on,item_id,left(item_info1,10)+'.. ' product_key,email_addr cust_email from b2b_po_notes where invoice_id='$id1' and item_id='$id2' order by created_on";
            $history = $this->db->query($sql)->result_array();
            return $history;
        }
    }
    public function upd_pos_trans($mode,$trans_id,$id){
        if ($mode=="cash-alipay") {
            $sql = "UPDATE pos_payment_list set pay_mode='I-ALIPAY',fin_dim2='ALIPAY',trans_ref1='MANUAL' 
                      WHERE trans_id='$trans_id' and line_num=$id and pay_mode='6-Cash'";
            $history = $this->db->query($sql);
        }
    }

    public function cb_parcel($id){

        $sql = "select invoice_id,line_num,item_id,item_desc,qty_invoiced,
                       qty_picked,coalesce(pick_date,'') pick_date, (case when modified_on=pick_date then (select usr_name from sys_usr_list where usr_id= sms_invoice_item.modified_by) else '' end) pick_by,
                       inv_loc,loc_id,trans_id, delv_mode_id,
                    coalesce((select TOP 1 (select usr_name from sys_usr_list where usr_id= pl.created_by) from b2b_parcel_list pl where sms_invoice_item.invoice_id = pl.invoice_id order by created_on desc),'') parcel_list_created_by,
                    coalesce((select TOP 1 created_on from b2b_parcel_list where sms_invoice_item.invoice_id = b2b_parcel_list.invoice_id order by created_on desc),'') parcel_list_created_on,
                    coalesce((select top 1 tracking_id from b2b_parcel_list where sms_invoice_item.invoice_id = b2b_parcel_list.invoice_id order by created_on desc),'') parcel_list_tracking_id,
                    coalesce((select sum(qty_packed) from b2b_parcel_item where sms_invoice_item.invoice_id = b2b_parcel_item.invoice_id),'') parcel_item_total_qty
                from sms_invoice_item
                where invoice_id='$id'
                order by sms_invoice_item.line_num";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }

    public function cb_rerouting($inv,$line,$loc) {
        $sql = "select trans_id,status_level,qty_picked,item_id,qty_invoiced from sms_invoice_item where coy_id='CTL' and invoice_id='$inv' and line_num='$line'; ";
        $res = $this->db->query($sql)->result_array();
        if ($res){
            if (trim($res[0]['trans_id'])=='' && $res[0]['status_level']>=0 && $res[0]['status_level']==1 && $res[0]['qty_picked']==0 ) {

                $sql = "update sms_invoice_item set inv_loc='$loc',modified_on=now(),modified_by='CHVOICES-RR' where coy_id='CTL' and invoice_id='$inv' and line_num='$loc'";
                $r = $this->db->query($sql);
                if ($r) {

                    $item = $res[0]['item_id'];
                    $qty = $res[0]['qty_invoiced'];
                    $sql = "select line_num from ims_inv_physical 
                                where coy_id='CTL' and item_id='$item' and loc_id='$loc' and qty_on_hand>qty_reserved 
                                order by trans_date limit 1";
                    $item_line = $this->db->query($sql)->result_array();

                    $iline = $item_line[0]['line_num'];
                    $sql = "update ims_inv_physical set qty_reserved = qty_reserved + $qty 
                                where coy_id='CTL' and item_id='$item' and loc_id='$loc' line_num='$iline'";
                    $this->db->query($sql);

                    return true;
                }

            }
        }

        return false;
    }

    public function cb_logistics(){
        $sql = "EXEC sp_b2b_delivery_planning;";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function cb_logistics_getrefkey($id){
        $sql = "select i.invoice_id, max(i.delv_mode_id) delv_mode_id, o.ref_key
                from sms_invoice_item i join o2o_url_key o on i.invoice_id=o.ref_id
                where invoice_id='$id'
                group by i.invoice_id, o.ref_key;";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_document($doc,$id,$v1='',$v2=''){


        if ($doc=="hachi-do") {
            if ($v1=="NID") {
                return "https://www.hachi.tech/pdf/hachi-do/". trim($id) ."";
            }
            else {
                return "https://www.hachi.tech/pdf/hachi-do/". trim($id) ."/". trim($v1) ."/".$v2;
            }
        }
        else {
            //return "http://www.hachi.tech/invoice/trusted/". trim($id) ."?keycode=fc745c4606d817b70e862079920aca1d";
            $s = md5($id.'@Challenger.'.substr($id, -3));
            return "http://www.hachi.tech/invoice/trusted/". trim($id) ."?signature=" . $s;
        }


        $cherps_mssql = $this->load->database('cherps_hachi', TRUE);

        $key = $id;
        // http://chvoices-test.challenger.sg/cherps/invoice_redirect/hachi-refund/HIC37711
        // http://chvoices-test.challenger.sg/cherps/invoice_redirect/hachi-exchange/HIC37711
        // http://chvoices-test.challenger.sg/cherps/invoice_redirect/hachi-invoice/HIC37711
        // http://chvoices-test.challenger.sg/cherps/invoice_redirect/hachi-do/HIC37711/NID/HCL

        $sql = "select ref_key from o2o_url_key where ref_id='$id' ";
        $result = $cherps_mssql->query($sql)->result_array();

        if ($result) {
            $key = $result[0]['ref_key'];

            $sql = "select * from sms_invoice_item where rtrim(invoice_id)=rtrim('$id') ";
            $result = $this->db->query($sql)->result_array();

//            $sql = "delete from sms_invoice_item where invoice_id='$id'";
//            $cherps_mssql->query($sql);
            foreach($result as $ins) {
                $ins['code_send'] = date('Y-m-d H:i:s', strtotime($ins['code_send']) );
                $ins['delv_date'] = date('Y-m-d H:i:s', strtotime($ins['delv_date']) );
                $ins['posted_on'] = date('Y-m-d H:i:s', strtotime($ins['posted_on']) );
                $ins['created_on'] = date('Y-m-d H:i:s', strtotime($ins['created_on']) );
                $ins['modified_on'] = date('Y-m-d H:i:s', strtotime($ins['modified_on']) );
                $ins['pick_date'] = (isset($ins['pick_date'])) ? date('Y-m-d H:i:s', strtotime($ins['pick_date']) ) : '1900-01-01';
                if ($doc == 'hachi-do' && $v1== 'DSH') {
                    if ($ins['delv_mode_id'] == $v1) {
                        $ins['qty_picked'] = $ins['qty_invoiced'];
                        $ins['pick_date'] = date('Y-m-d H:i:s', strtotime('today'));
                    }
                }
                $cherps_mssql->where('invoice_id', $ins['invoice_id'])->where('line_num', $ins['line_num'])->update('sms_invoice_item', $ins);
            }
        }
        else {
            $doc = '';
        }

        if ($doc=="hachi-refund") {
            return "https://www.hachi.tech/hachi-refund-invoice-url/invoice/" . $key;
        }
        else if ($doc=="hachi-exchange") {
            return "https://www.hachi.tech/hachi-exchange-invoice-url/invoice/" . $key;
        }
        else if ($doc=="hachi-invoice") {
            return "https://www.hachi.tech/invoice/hachi-cherps-url/" . $key;
        }
        else if ($doc=="hachi-do") {
            return "https://www.hachi.tech/hachi-delivery-order-url/invoice/" . $key .'/'. $v1 .'/' . $v2;
        }
        else {
            return 'https://www.hachi.tech/';
        }
    }

    public function approval_inv_transaction($id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

}

?>
