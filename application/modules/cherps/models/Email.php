<?php 
   Class Email extends Base_Common_Model { 
	
      Public function __construct() { 
         parent::__construct(); 
      } 
      public function sendEmail($recipients, $data){
         $postData = ["recipients"=> $recipients,
         "copy_recipients"=> $data['email_addr'].";james.chen@challenger.sg",
         // "copy_recipients"=> "james.chen@challenger.sg",
         "subject"=> $data['subject'],
         "body"=> $data['body'],
         ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://ctlmailer.api.valueclub.asia/send',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode($postData),
            CURLOPT_HTTPHEADER => array(
                'Authorization: 8f413c39b4d1b4e7c584943df22e7a8c'
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo "
        <meta charset = 'utf-8'> 
         <meta name='viewport' content='width=device-width, initial-scale=1.0'>
         <title>User Access Request</title> 
         <div style='height:300px;'></div>
         <div style='text-align:center;'><h2>Your request is completed!</h2></div>
         <div style='text-align:center;'>
         <button style='width:50%;padding:15px 32px;font-size:20px;' 
         onclick=\"document.location='/cherps/userAccess'\">Home</button></div>
         ";
      }

      public function sendSMS($recipients, $data) {
        //  $sub = "";
        //  $a = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        //  for ($sub = '', $i = 0, $z = strlen($a)-1; $i != 4; $x = rand(0,$z), $sub .= $a{$x}, $i++);
        //  $otp = mt_rand(100000, 999999);
         $postData = ["recipients"=> $recipients,
         "from_address"=> "CHERPS",
         "subject"=> $data,
         ];

         $curl = curl_init();
 
         curl_setopt_array($curl, array(
             CURLOPT_URL => 'https://ctlmailer.api.valueclub.asia/sms',
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => '',
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 0,
             CURLOPT_FOLLOWLOCATION => true,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => 'POST',
             CURLOPT_POSTFIELDS =>json_encode($postData),
             CURLOPT_HTTPHEADER => array(
                 'Authorization: 8f413c39b4d1b4e7c584943df22e7a8c'
             ),
         ));
         
         $response = curl_exec($curl);

         curl_close($curl);
      }
   } 
?>
