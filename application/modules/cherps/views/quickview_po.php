<section class="content clearfix">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">

                    <div class="col-xs-12 no-padding clearfix">

                        <div class="col-xs-4 text-grey">PO. No.:</div>
                        <div class="col-xs-8 no-padding text-bold"><?php echo $po[0]['po_id']; ?></div>

                        <div class="col-xs-4 text-grey">Date:</div>
                        <div class="col-xs-8 no-padding text-bold"><?php echo $po[0]['po_date']; ?></div>

                        <div class="col-xs-4 text-grey">Curr:</div>
                        <div class="col-xs-3 no-padding text-bold"><?php echo $po[0]['curr_id']; ?></div>

                        <div class="col-xs-2 text-grey">Term:</div>
                        <div class="col-xs-3 text-bold"><?php echo $po[0]['payment_id']; ?></div>

                        <div class="col-xs-4 text-grey">Deliver to:</div>
                        <div class="col-xs-8 no-padding text-bold"><?php echo $po[0]['loc_id']; ?> <!--<?php echo $po[0]['delv_date']; ?>--></div>

                    </div>
                    <div class="col-xs-12 no-padding clearfix" style="margin-top:20px;padding:5px 0px !important;border-top:1px solid #CCC;border-bottom:1px solid #CCC;">

                        <div class="col-xs-3 text-grey">Vendor:</div>
                        <div class="col-xs-9 text-bold"><?php echo $po[0]['supp_id']; ?><br><?php echo $po[0]['supp_name']; ?></div>

                    </div>

                </div>
            </div>
            <div class="box">
                <div class="box-body">


                    <div class="col-xs-12 no-padding clearfix m-t-10">

                        <div class="col-xs-7 text-bold text-blue">
                            <span>Item ID/Desc</span>
                            <span class="pull-right">Unit Price</span>
                        </div>
                        <div class="col-xs-5 text-bold text-blue">
                            <span>Qty</span>
                            <span class="pull-right">Amount</span>
                        </div>

                        <?php $cT = 0; ?>
                        <?php foreach ($po as $porow):?>
                            <?php $cT += $porow['unit_price']*$porow['qty_order']; ?>

                            <div class="col-xs-7 text-bold p-t-5">
                                <span><?php echo $porow['item_id']; ?></span>
                                <span class="pull-right"><?php echo number_format($porow['unit_price'],2); ?></span>
                            </div>
                            <div class="col-xs-5 text-bold p-t-5">
                                <span><?php echo number_format($porow['qty_order']); ?></span>
                                <span class="pull-right"><?php if ($porow['curr_id']=='SGD') echo '$ '; ?><?php echo number_format($porow['unit_price']*$porow['qty_order'],2); ?></span>
                            </div>
                            <div class="col-xs-12 chv-outline-bottom">
                                <span><?php echo $porow['item_desc']; ?><br><?php echo nl2br($porow['long_desc']); ?></span>
                            </div>
                        <?php endforeach; ?>

                        <div class="col-xs-12 chv-outline-bottom p-5">
                            <span class="pull-right text-red text-bold">TOTAL: <?php if ($porow['curr_id']=='SGD') echo '$ '; ?><?php echo number_format($cT,2); ?></span>
                        </div>

                    </div>

                    <div class="col-xs-12 no-padding clearfix">

                        <div class="col-xs-4">Remarks:</div>
                        <div class="col-xs-8"><?php
                            $reg_exUrl = "/((http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)/";
                            $remark = preg_replace($reg_exUrl, ' <a href="${1}" target="_blank">[OPEN LINK HERE]</a> ', $po[0]['remarks']);
                            echo $remark;
                        ?>&nbsp;</div>
                        <div class="m-t-20 clearfix">&nbsp;</div>

                        <div class="col-xs-4">Buyer:</div>
                        <div class="col-xs-8"><?php echo $po[0]['buyer_id_usr']; ?></div>
                        <div class="clearfix"></div>
                        <div class="col-xs-4">Requested by:</div>
                        <div class="col-xs-8"><?php echo $po[0]['requester_id_usr']; ?></div>
                        <div class="clearfix"></div>
                        <div class="col-xs-4">Order/Issue by:</div>
                        <div class="col-xs-8"><?php echo $po[0]['created_by_usr']; ?></div>

                    </div>
                </div>
            </div>

            <div style="height:80px;">&nbsp;</div>
        </div>
    </div>

    <?php if ($po[0]['status_level']==0): ?>
    <div class="fixed-bottom chv-white-bg" style="height:90px;">
        <div class="col-xs-12 text-center">
            <span class="text-center text-red text-bold f-md">TOTAL: <span class="f-lg"><?php if ($porow['curr_id']=='SGD') echo '$ '; ?><?php echo number_format($cT,2); ?></span></span>
        </div>
        <div class="col-xs-12 text-center">
            <?php if ($cT <= $usr_limit['po_limit']): ?>
            <form action="<?php echo current_full_url(); ?>" method="post">
                <input type="hidden" name="po_id" value="<?php echo $po[0]['po_id']; ?>">
                <input type="hidden" name="current_rev" value="<?php echo $po[0]['current_rev']; ?>">
                <input type="hidden" name="usr_id" value="<?php echo $usr_id; ?>">

                <!-- <button name="action" value="REJECT" class="btn btn-grey w-40" onclick="return confirm('Are you sure to reject this PO?')">Reject</button> -->
                <a href="#" data-toggle="modal" data-target="#RejectPoModal" class="btn btn-grey w-40">Reject</a>

                <button name="action" value="APPROVE" class="btn btn-green m-10 w-40">Approve</button>
            </form>
            <?php elseif ($usr_limit['po_limit']==NULL OR $usr_limit['po_limit']<0): ?>
                <span class="text-center">You are not authorized to perform PO Approval.</span>
            <?php else: ?>
                <span class="text-center">PO Amount exceeded the Approver Limit!
                    <br>Alternate Approvers: <?php echo $usr_limit['alt_approvers']; ?></span>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>

</section>

<div class="modal fade" id="RejectPoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Reject PO</h4>
            </div>
            <form action="<?php echo current_full_url(); ?>" method="post">
                <input type="hidden" name="po_id" value="<?php echo $po[0]['po_id']; ?>">
                <input type="hidden" name="current_rev" value="<?php echo $po[0]['current_rev']; ?>">
                <input type="hidden" name="usr_id" value="<?php echo $usr_id; ?>">

                <div class="modal-body">
                    <p>Please enter reason (if any):</p>
                    <textarea name="reject_reason" style="width:100%;height:50px;"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-grey" data-dismiss="modal">Cancel</button>
                    <button name="action" value="REJECT" class="btn btn-default w-40">Reject</button>
                </div>

            </form>
        </div>
    </div>
</div>