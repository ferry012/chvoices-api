<section id="cherps_user" class="content clearfix">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <h4>Retrieve User Profile:</h4>
                    <div class="text-center">
                        <form method="get" action="">
                            <input type="text" name="usr" value="" placeholder="User ID" size="6" style="background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:18px;height:48px;width:80%;text-align:center;" onfocus="this.select();">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div id="usr_sys" class="col-xs-12 no-padding clearfix"></div>
                    <div id="usr_emp" class="col-xs-12 no-padding clearfix" style="margin-top:20px;padding:5px 0px !important;border-top:1px solid #CCC;border-bottom:1px solid #CCC;"></div>
                </div>
            </div>

            <div class="box">
                <div class="box-body">
                    <div class="col-xs-12 pull-right"><h4>Companies:</h4></div>
                    <div id="usr_coy" class="col-xs-12 no-padding clearfix"></div>
                </div>
            </div>

            <div class="box">
                <div class="box-body">
                    <div class="col-xs-12 pull-right">
                        <h4>User Groups:
                            <span class="pull-right">
                                <a href="#" data-toggle="modal" data-target="#modal_grp" class="btn btn-blue btn-sm"><b>+</b> Add</a>
                            </span>
                        </h4>
                    </div>
                    <div id="usr_grp" data-title="User Groups:" class="col-xs-12 no-padding clearfix"></div>
                </div>
            </div>

            <div class="box">
                <div class="box-body">
                    <div class="col-xs-12 pull-right"><h4>Cherps Menu:</h4></div>
                    <div id="usr_menu" class="col-xs-12 no-padding clearfix"></div>
                </div>
            </div>

            <div class="box">
                <div class="box-body">
                    <div class="col-xs-12 pull-right"><h4>Granted Data Types:</h4></div>
                    <div id="usr_types" class="col-xs-12 no-padding clearfix"></div>
                </div>
            </div>

            <div class="box">
                <div class="box-body">
                    <div class="col-xs-12 pull-right"><h4>Account Default Settings:</h4></div>
                    <div id="usr_settings" class="col-xs-12 no-padding clearfix"></div>
                </div>
            </div>
        </div>
    </div>



</section>

<div class="modal fade" id="modal_grp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add to New Group</h4>
            </div>
            <form action="<?php echo base_url('api/challenger_mis/'.$usr_id); ?>" method="post">
                <input type="hidden" name="by" value="yongsheng@challenger.sg">
                <input type="hidden" name="pin" value="86EB33C5CC">
                <input type="hidden" name="coy" value="CTL">
                <input type="hidden" name="action" value="sss_grp">

                <div class="modal-body text-center">
                    <select id="sel_grp" name="grp" class="form-control"></select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-grey" data-dismiss="modal">Cancel</button>
                    <button name="submit" value="APPROVE" class="btn btn-default w-40">Add Group</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_coy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Company</h4>
            </div>
            <form action="<?php echo base_url('api/challenger_mis/'.$usr_id); ?>" method="post">
                <input type="hidden" name="by" value="yongsheng@challenger.sg">
                <input type="hidden" name="pin" value="86EB33C5CC">
                <input type="hidden" name="coy" value="CTL">
                <input type="hidden" name="action" value="coy_loc">

                <div class="modal-body text-center">
                    <input name="loc" class="form-control" maxlength="3" size="8">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-grey" data-dismiss="modal">Cancel</button>
                    <button name="action" value="REJECT" class="btn btn-default w-40">Add Group</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script>
    var USR_ID = '<?php echo $usr_id; ?>';
    var COY_ID = '<?php echo $coy_id; ?>';

    <?php if ($usr_id): ?>
    <?php else: /* NEW FORM VIEW */ ?>

    <?php endif; ?>

</script>
