<section class="content clearfix">

    <h4>Check Hachi Parcel</h4>
    <form method="post" action="<?php echo base_url('cherps/page?view=cb_parcel') ; ?>">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <h5>Hachi Invoice</h5>
                        <p>Enter Hachi Invoice number.</p>
                        <div class="formfield">Invoice ID:<br><input name="invoice_id" value="HIC" required></div>

                        <hr>
                        <div class="formfield">
                            <input class="btn btn-green" type="submit" name="save" value="Check Invoice 4D">
                        </div>
                    </div>
                </div>
                <?php if ($message && count($message)>1): ?>
                    <div class="box">
                        <div class="box-body">
                            <div>
                                <table border="0">
                                <?php
                                foreach ($message as $msg) {
                                    foreach ($msg as $mk=>$mm) {
                                        echo '<tr><td align="right">' . $mk . '</td><td>&nbsp; : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>' . $mm . '</td></tr>';
                                    }
                                    echo '<tr><td colspan="3"><hr></td></tr>';
                                }
                                ?>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </form>

</section>

<style>
    input {background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:14px;width:60%;}
    h5 {font-weight:bold}
    .formfield {padding:5px}
</style>