<?php
$subject = (!isset($subject)) ? 'Page Expired' : $subject;
$message = (!isset($message)) ? 'The link you are using has expired. Please request for a new link to view the content.' : $message;
$formurl = (!isset($formurl)) ? base_url('cherps/otp') : $formurl;
?>
<section class="content clearfix">
    <form action="<?php echo $formurl; ?>" method="post" autocomplete="off">

        <div class="text-center">
            <h3 class="box-title f-lg"><?php echo $subject; ?></h3>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box-body text-center">

                    <p><?php echo $message; ?></p>
                    <hr>
                    <p><i>Please close this window.</i></p>

                </div>
            </div>
        </div>

        <div class="fixed-bottom text-center"> </div>

    </form>
</section>