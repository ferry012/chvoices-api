<section class="content clearfix">

    <h4>Logistics</h4>
        <div class="row">
            <div class="col-xs-12">

                <form method="post" action="<?php echo base_url('cherps/page?view=cb_logistics') ; ?>">
                <div class="box">
                    <div class="box-body">
                        <h5>Deliver Assignment</h5>
                        <?php if ($message): ?>
                            <p><?php echo '<b>' . $message[0]['assigned_qty'] . '</b> Orders has been assigned in this batch'; ?></p>
                        <?php endif; ?>
                        <hr>
                        <div class="formfield">
                            <input class="btn btn-green" type="submit" name="saveassign" value="Assign Now">
                        </div>
                    </div>
                </div>
                </form>

                <form method="post" action="<?php echo base_url('cherps/page?view=cb_logistics') ; ?>" target="OPENDO">
                <div class="box">
                    <div class="box-body">
                        <h5>Retreive DO</h5>
                        <hr>
                        <div class="formfield">
                            <input name="opendo_id" value="" placeholder="Invoice ID" onfocus="this.value=''" required style="margin-bottom: 10px;"> &nbsp;
                            <input class="btn btn-green" type="submit" name="opendo" value="Trigger Now">
                        </div>
                    </div>
                </div>
                </form>

                <form method="post" action="<?php echo base_url('cherps/page?view=cb_logistics') ; ?>">
                <div class="box">
                    <div class="box-body">
                        <h5>Trigger to OptimoRoute</h5>
                        <?php if ($message): ?>
                            <p><?php echo '<b>' . $message['success'] . '/' . $message['total'] . '</b> Orders triggered in this batch'; ?></p>
                        <?php endif; ?>
                        <hr>
                        <div class="formfield">
                            <input class="btn btn-green" type="submit" name="saveoptimo" value="Trigger Now">
                        </div>
                    </div>
                </div>
                </form>

            </div>
        </div>

</section>

<style>
    input {background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:14px;width:60%;}
    h5 {font-weight:bold}
    .formfield {padding:5px}
</style>