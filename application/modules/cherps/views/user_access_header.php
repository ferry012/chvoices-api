<!DOCTYPE html> 
<html lang = "en"> 

   <head> 
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <link rel = "stylesheet" type = "text/css" href = "/assets/css/style.css">
      <script src="/assets/js/userAccess.js"></script>
      <meta charset = "utf-8"> 
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>User Access Request</title> 
   </head>

   <body id="body"> 
      <div id="header">
         <div id="title">
               <div id='usr_name'>
               <?php 
               echo $usr_name."</div>";
               echo "<div id='loc_id'>".$loc_id."</div>";
               echo "<div id='security_level'>".$security_level."</div>";
               ?><hr/>
         </div>
      </div>