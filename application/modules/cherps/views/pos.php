<section id="cherps_user" class="content clearfix">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <h4>Retrieve Transaction:</h4>
                    <div class="text-center">
                        <form method="get" action="">
                            <input type="text" name="trans_id" value="" placeholder="Trans ID" size="6" style="background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:18px;height:48px;width:80%;text-align:center;" onfocus="this.select();">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="col-xs-12 no-padding clearfix">
                        <table width="100%">
                            <tr>
                                <td>Trans: <strong><?php echo strtoupper($this->input->get('trans_id')); ?></strong></td>
                                <td class="pull-right"><?php echo $trans[0]['trans_date']; ?></td>
                            </tr>
                        </table><br>
                        <table width="100%" border="1">
                            <tr>
                                <td>Item</td>
                                <td></td>
                                <td>Qty</td>
                                <td>Remarks</td>
                            </tr>
                            <?php foreach ($trans as $tx): ?>
                                <tr>
                                    <td><?php echo $tx['item_id']; ?></td>
                                    <td><?php echo $tx['item_desc']; ?></td>
                                    <td><?php echo intval($tx['trans_qty']); ?></td>
                                    <td align="right"><?php
                                        if ($tx['item_type']=='G' || $tx['item_type']=='H') {
                                            echo '<a href="#" class="open_gourl" data-gotype="BLACKHAWK" data-gourl="api/challenger_txn/blackhawk/'.$tx['lot_id'].'">Details</a>';
                                        }
                                        else if (strpos('-'.$tx['inv_dim4'], 'ESD')) {
                                            echo '<a href="#" class="open_gourl" data-gotype="MSESD" data-gourl="api/challenger_txn/ponotes/'.$tx['trans_id'].'***'.$tx['item_id'].'">Details</a>';
                                        }
                                    ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                        <br>
                        <b>Payment:</b>
                        <table width="100%">
                            <?php $pay_alipay=0; ?>
                            <?php foreach ($pay as $pp): ?>
                                <tr>
                                    <td><?php echo $pp['pay_mode']; ?></td>
                                    <td><?php echo '$'.$pp['trans_amount']; ?></td>
                                    <td><?php echo $pp['fin_dim2']; ?></td>
                                    <td align="right"><?php
                                        if ( strtoupper($pp['pay_mode'])=='I-ALIPAY') {
                                            echo '<a href="#" class="open_gourl" data-gotype="ALIPAY" data-gourl="api/alipay/check/' . $tx['trans_id'] . '">ALIPAY</a>';
                                            $pay_alipay++;
                                        }
                                        if ( strtoupper($pp['pay_mode'])=='6-CASH') {
                                            echo '<a href="'.base_url('cherps/pos?trans_id='.trim($tx['trans_id']).'&upd=cash-alipay&id='.$pp['line_num']).'" onclick="return confirm(\'Make sure you have check for valid ALIPAY transaction before you switch!\')" >Switch to Alipay</a>';
                                        }
                                    ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <?php if ($pay_alipay==0): ?>
                                <tr>
                                    <td align="center" colspan="4"><br>
                                        No confirmed ALIPAY recorded. <?php echo '<a href="#" class="open_gourl" data-gotype="ALIPAY" data-gourl="api/alipay/check/' . strtoupper($this->input->get('trans_id')) . '">Query at Alipay Server</a>'; ?>.
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_grp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Item Details</h4>
            </div>
            <div class="modal-body" id="show_gourl">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-grey" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(".open_gourl").click(function () {
            var gotype = $(this).data('gotype');
            var gourl = $(this).data('gourl');
            var fullurl = 'https://chvoices.challenger.sg/' + gourl;

            $("#modal_grp").modal('show');
            $("#show_gourl").html('Retrieving info from <a href="'+fullurl+'" target="_blank">'+fullurl+'</a>');
            $.getJSON(fullurl , function(data) {
                var tbl_body = "";
                if (gotype=='ALIPAY') {
                    tbl_body += 'ID: '+ data.trans_id +'<br>';
                    tbl_body += 'STATUS-1: '+ data.result2 +'<br>';
                    tbl_body += 'STATUS-2: '+ data.result1 +'<br>';
                    tbl_body += 'AMOUNT: '+ data.payment_amount +'<br>';
                }
                else {
                    $.each(data, function () {
                        $.each(this, function (k, v) {
                            tbl_body += '<b>' + k + '</b>: ' + v + '<br>';
                        })
                        tbl_body += "<hr>";
                    })
                }

                // MESSAGING
                if (gotype=='MSESD')
                    tbl_body += '<br>If product key is not generated, you must check with Ingram. Update b2b_po_notes table to re-send to customer.';
                if (gotype=='BLACKHAWK')
                    tbl_body += '<br>If card is not activated yet, you can trigger the activation attempt at https://chvoices.challenger.sg/api/blackhawk/{trans_id}';

                $("#show_gourl").html(tbl_body);
            });


        });
    });
</script>