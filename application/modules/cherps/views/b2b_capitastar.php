<section class="content clearfix">

    <h4>Mall Login Details</h4>
    <form method="post" action="<?php echo base_url('cherps/page?view=capitastar') ; ?>">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php if ($message): ?>
                        <div class="text-center"><?php echo $message; ?></div>
                    <?php else: ?>
                    <h5>CHERPS Login</h5>
                    <p>Please login to your CHERPS account for authentication.</p>
                    <div class="formfield">User ID: <br><input name="cherps_usr" required></div>
                    <div class="formfield">Password: <br><input name="cherps_pwd" type="password" required></div>

                    <hr>

                    <h5>Mall Account</h5>
                    <p>Enter the account information to be saved.</p>
                    <div class="formfield">Mall Landlord:<br><input name="cpls_mall" value="CAPITAMALL" required></div>
                    <div class="formfield">Location ID (i.e. BF/IMM/PS/etc):<br><input name="cherps_locid" maxlength="5" required></div>
                    <div class="formfield">Capitastar Login ID:<br><input name="cpls_usr" required></div>
                    <div class="formfield">Capitastar Password:<br><input name="cpls_pwd" type="password" required></div>

                    <hr>
                    <div class="formfield">
                        <input class="btn btn-green" type="submit" name="save" value="Save Login">
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    </form>

</section>

<style>
    input {background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:14px;width:60%;}
    h5 {font-weight:bold}
    .formfield {padding:5px}
</style>
