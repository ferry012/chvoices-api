<section class="content clearfix">
    <form action="<?php echo base_url('cherps/otp'); ?>" method="post" autocomplete="off">

        <div class="text-center">
            <h3 class="box-title f-xl">CHERPS OTP</h3>
        </div>

        <div class="row">
            <div class="col-xs-12">
                    <div class="box-body text-center">

                        <div class="col-xs-10 col-xs-push-1">The content you are viewing is protected. An OTP has been sent to the intended user. Please enter the OTP to view the content.</div>
                        <div class="col-xs-12 m-t-20">
                            <input type="hidden" name="curr_url" value="<?php echo $curr_url; ?>">
                            <input type="hidden" name="usr_id" value="<?php echo $usr_id; ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <input type="text" name="cherps_otp" value="" size="6" style="background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:18px;height:48px;width:80%;text-align:center;letter-spacing:5px;" onfocus="this.select();">

                            <br><br>
                            <input class="btn btn-white" type="submit" name="send_new_otp" value="Resend">

                            <p><?php if ($this->input->get('debug')==1) print_r($this->session->userdata()); ?></p>
                        </div>
                    </div>
            </div>
        </div>

        <div class="fixed-bottom text-center">
            <input class="btn btn-green m-10 w-80" type="submit" name="submit_otp" value="SUBMIT OTP">
        </div>

    </form>
</section>