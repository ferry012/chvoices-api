<section class="content clearfix">

    <h4>HACHI Invoice Fix</h4>
    <form method="post" action="<?php echo base_url('cherps/page?view=hachi_pocinvoicefix') ; ?>">
    <div class="row">
        <div class="col-xs-12">
            <?php if ($message): ?>
                <div class="box">
                    <div class="box-body">
                        <div class="text-center"><?php echo $message; ?></div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="box">
                <div class="box-body">
                    <h5>CHERPS Login</h5>
                    <p>Your CHERPS login to authenticate your identity.</p>
                    <div class="formfield">User ID: <br><input name="cherps_usr" required></div>
                    <div class="formfield">Password: <br><input name="cherps_pwd" type="password" required></div>

                    <hr>

                    <h5>Invoice Details</h5>
                    <p>Submit below form to insert payment info & generate invoice link. Leave payment info empty to generate link without amending payment info.</p>
                    <div class="formfield">Invoice ID:<br><input name="invoice_id" value="HIA" required></div>
                    <div class="formfield">Payment Mode:<br>
                        <select name="pay_mode">
                            <option value="">NONE</option>
                            <option value="NETS">NETS (Ref: 000xxx)</option><option value="AMEX">AMEX (Ref: 4 Digits)</option>
                            <option value="DBS">DBS (Ref: CardNo-Ref 1234-XXXXXX)</option><option value="UOB">UOB (Ref: CardNo-Ref 1234-XXXXXX)</option>
                        </select></div>
                    <div class="formfield">Amount:<br><input name="trans_amount"></div>
                    <div class="formfield">Ref:<br><input name="trans_ref"></div>

                    <hr>
                    <div class="formfield">
                        <input class="btn btn-green" type="submit" name="save" value="Update & Check Invoice">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>

</section>

<style>
    input {background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:14px;width:60%;}
    h5 {font-weight:bold}
    .formfield {padding:5px}
</style>