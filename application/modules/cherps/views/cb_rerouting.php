<section class="content clearfix">

    <h4>Hachi Invoice Reroute</h4>
    <form method="post" action="<?php echo base_url('cherps/page?view=cb_rerouting') ; ?>">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php if ($message): ?>
                            <div class="text-center text-red"><?php echo $message; ?></div>
                        <?php endif ?>

                        <h5>Hachi Invoice - Manual Rerouting</h5>
                        <p>This only update invoice location & reserve qty from outlet. Transfer will be created later.</p>

                        <div class="formfield">Invoice ID:<br><input name="invoice_id" value="HIC" required></div>

                        <div class="formfield">Item Line Num:<br><input name="line_num" value="" required></div>

                        <div class="formfield">From Loc ID:<br><input name="loc_id" value="" required></div>

                        <hr>
                        <div class="formfield">
                            <input class="btn btn-green" type="submit" name="save" value="Save Rerouting">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</section>

<style>
    input {background-color:#EFEFEF;border:1px solid #DFDFDF;font-size:14px;width:60%;}
    h5 {font-weight:bold}
    .formfield {padding:5px}
</style>