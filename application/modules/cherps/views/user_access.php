<?php
   $locationList ="<label for='location'>New Location: <br/></label>
      <select name='loc_id' id='loc_id'><option value='313'>313</option>
         <option value='AA'>AA</option><option value='AMK'>AMK</option><option value='BF'>BF</option>
         <option value='BM'>BM</option><option value='BP'>BP</option><option value='BS'>BS</option><option value='CM'>CM</option>
         <option value='CO'>COM</option><option value='CP'>CP</option><option value='CW'>CW</option><option value='CY'>CY</option>
         <option value='DE'>DE</option><option value='EP'>EP</option><option value='GWC'>GWC</option><option value='HL'>HL</option>
         <option value='HM'>HM</option><option value='IM'>IMM</option><option value='J8'>J8</option><option value='JC'>JC</option>
         <option value='JE'>JEM</option><option value='JL'>JL</option><option value='JP'>JP</option><option value='L1'>L1</option>
         <option value='MB'>MB</option><option value='NEX'>NEX</option><option value='NP'>NP</option><option value='PG'>PG</option>
         <option value='PLQ'>PLQ</option><option value='PP'>PP</option><option value='PS'>PS</option><option value='RC'>RC</option>
         <option value='SC'>SC</option><option value='SM'>SM</option><option value='SP'>SP</option><option value='SS'>SS</option>
         <option value='TB'>TB</option><option value='TM'>TM</option><option value='TP'>TP</option><option value='VC'>VC</option>
         <option value='WCP'>WCP</option><option value='WG'>WG</option><option value='WP'>WP</option>
         <option value='WS'>WS</option><option value='YT'>YT</option><option value='UBI'>UBI</option>
      </select><br/>";
   $hideRequest = "<script>document.getElementById('request').innerHTML=''</script>";
   $sysSql = "SELECT trim(grp_id) grp_id, grp_desc FROM sss_grp_list WHERE coy_id = 'CTL'
   AND grp_id NOT IN ('USR_ADM','TRS_User','TRS','SYS_ADM','MIS_ADM')
   AND grp_id NOT IN (SELECT grp_id FROM sss_grp_usr_list WHERE coy_id = 'CTL' AND usr_id = '".$userExisted['usr_id']."')";
   $grp_list = $this->db->query($sysSql)->result();
?>

<!-- main content here -->
<div id="home">
   <!-- footer message here -->
   <button id='btnHome' onclick="document.location='/cherps/userAccess'">Home</button>
</div>

<div id="main">
   <!-- change location -->
   <div id="request">
      <div id="newAccess">      
         <button id="btnNewAccess" onclick="checkUser('A')">Cherps Permission</button>
         <br/><br/>
      </div>
      <div id="changeLoc">
         <button id="btnChangeLoc" onclick="checkUser('C')">Change Location</button>
         <br/><br/>
      </div>
      <div id="newStaff">
         <button id="btnNewStaff" onclick="checkUser('N')">New Staff</button>
         <br/><br/>
      </div>
      <div id="resignStaff">
         <button id="btnResignStaff" onclick="checkUser('R')">Deactivate Staff</button>
         <br/><br/>
      </div>
      <div id="resetPwd">
         <button id="btnResetPwd" onclick="checkUser('P')">Reset Password</button>
         <br/><br/>
      </div>
      <div id="showForm"></div>
      <!-- <div id="errMsg"></div> -->
   </div>

   <br/>
   <div id="details">
      <?php
         if($userExisted['errMsg'] != ""){
            if($userExisted['type'] == 'C' || $userExisted['type'] == 'R' || $userExisted['type'] == 'A'
               || $userExisted['type'] == 'P'){
               echo $hideRequest."<div id='errMsg' style=color:red; font-size:30px;>".$userExisted['errMsg']."</div>";
            }else if($userExisted['type'] == 'N'){ //N = new staff
               echo "<div>Please fill in new staff details below:<br/><br/>";
              echo $hideRequest."<div id='setStaff'>
               <!-- <form method='post' action='<?=base_url();?>'> -->
               <form method='post' action='/cherps/setNewStaff'>
                  <label style=text-align:left;>Company ID:<br/></label>
                  <select name='coy_id' id='coy_id'>
                  <option value='CTL'>CTL</option>
                  </select><br/><br/>
                  ".$locationList."<br/>
                  <label>Staff ID:<br/></label>
                  <input name='usr_id' id='usr_id' type='text' value='".$userExisted['usr_id']."' required /><br/><br/>
                  <label>Staff Name:</label><br/>
                  <input name='usr_name' id='usr_name' type = 'text' placeholder='Your Full Name' required /><br/><br/>
                  <label>Email Address:<br/></label>
                  <input name='email_addr' id='email_addr' type = 'email' placeholder='Email Address' required /><br/><br/>
                  <label>Joined Date:<br/></label>
                  <input name='join_date' id='join_date' type = 'date' required /><br/><br/>
                  <input name='btnSubmit' id='btnSubmit' type ='submit' value='Submit' />
               </form></div>";
            }
         }else{
            echo "<div id='usr_name'>".$userExisted['usr_name']."<br/>";
            echo $userExisted['loc_id']."<br/>".$userExisted['security_level']."</div><br/>";
            if($userExisted['type'] == 'C'){ // C = change location
               echo $hideRequest."<div id='newLoc'>
               <form action = '/cherps/changeLocation' method='post'>
                  <input name='usr_id' id='usr_id' type='text' style=display:none; value='".$userExisted['usr_id']."'/>
                  <div id='locationList'>".$locationList."</div><br/>
                  <input type='submit' value='Submit'/>
               </form></div>";
            }if($userExisted['type'] == 'R'){ // R = Resign staff
               echo $hideRequest."<div id='resignStaff'>
               <!-- <form method='post' action='<?=base_url();?>'> -->
               <form method='post' action='/cherps/setResignStaff' onsubmit=\"return confirm('are u sure?')\">
                  <label>Resigned Date</label>
                  <input name='usr_id' id='usr_id' type='text' style=display:none; value='".$userExisted['usr_id']."'/>
                  <input name='resign_date' type='date' required /><br/><br/>
                  <input name='btnSubmit' type='submit' value='Submit' />
               </form>
               </div>";
            }else if($userExisted['type'] == 'A'){ // A = Access / Permission
               echo $hideRequest."<spam><button id='btnAssignAccess'>Grant</button></spam>
               <spam><button id='btnRevokeAccess'>Revoke</button></spam><br/><br/>";
               //revoke access
               echo"<div id='revokeAccess'>
               <!-- <form method='post' action='<?=base_url();?>'> -->
               <form method='post' action='/cherps/revokeAccess'>
               <input name='usr_id' id='usr_id' type='text' style=display:none; value='".$userExisted['usr_id']."'/>
               <label><b>Choose Permission to revoke:</b> </label><br/>
               <input id='usrInput' type='text' placeholder='Search..' onkeyup='searchGrp()'/>
               <div name='grp_id' id='grp_id'>
               <select name='selected[]' id='selected' multiple size=5 required>";
               $count = 1;
               foreach($userExisted['access'] as $row){
                  if($count % 2 == 0){
                     echo "<option style=background-color:#E6E6FA; value='".$row->grp_id."'>".$count.". ".$row->grp_desc."</option>";
                  }else{
                     echo "<option value='".$row->grp_id."'>".$count.". ".$row->grp_desc."</option>";
                  }
                  $count++;
               }
               echo "</select></div><br/>
               <input id='btnSubmit' type='submit' value='Submit'>
               </form></div>";

               //assign access
               echo "<div id='assignAccess'>
               <style>table, th, td {table-layout:fixed; width: 100%; text-align: left; border: 1px solid black;border-collapse: collapse;}</style>
               <table><caption style=text-align:left;><b>Current Permission: </b></caption><br/>
               <tr><th>Group_ID</th><th>Description</th></tr>";
               foreach($userExisted['access'] as $row){
                  echo "<tr><td>".$row->grp_id."</td>";
                  echo "<td>".$row->grp_desc."</td></tr>";
               }
               echo "</table><br/>
               <!-- <form method='post' action='<?=base_url();?>'> -->
               <form method='post' action='/cherps/requestAccess'>
               <input name='usr_id' id='usr_id' type='text' style=display:none; value='".$userExisted['usr_id']."'/>
               <label><b>New role:</b> </label><br/>
               <input id='usrInput' type='text' placeholder='Search..' onkeyup='searchGrp()'/>
               <div name='grp_id' id='grp_id'>
               <select name='selected[]' id='selected' multiple size=10 required>";
               $count = 1;
               foreach($grp_list as $row){
                  if($count % 2 == 0){
                     echo "<option style=background-color:#E6E6FA; value='".$row->grp_id."'>".$count.". ".$row->grp_desc."</option>";
                  }else{
                     echo "<option value='".$row->grp_id."'>".$count.". ".$row->grp_desc."</option>";
                  }

                  $count++;
               }

               echo "</select></div><br/>
               <input id='btnSubmit' type='submit' value='Submit'>
               </form></div>
               <script>
                  $('option').mousedown(function(e) {
                     e.preventDefault();
                     $(this).prop('selected', !$(this).prop('selected'));
                     return false;
                  });

                  function searchGrp(){
                     var input = document.getElementById('usrInput');
                     var filter = input.value.toLowerCase();
                     var div = document.getElementById('grp_id');
                     var option = div.getElementsByTagName('option');
                     for(i=0; i<(option.length-1); i++){
                        value = option[i].textContent || option[i].innerText;
                        if(value.toLowerCase().indexOf(filter) > -1){
                           option[i].style.display='';
                        }else{
                           option[i].style.display='none';
                        }
                     }
                  }
               </script>
               ";
            }else if($userExisted['type'] == 'N'){
               echo $hideRequest."<div style=color:red; font-size:30px;>Staff already existed</div>";
            }
         }
      ?>
   </div>
</div>



</body>
</html>
