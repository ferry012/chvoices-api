<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Sa0006_model extends Base_Common_Model {

    public function __construct() {
        parent::__construct();
        $this->fixed_supp_id = 'SA0006';

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->apiurl = 'https://esb1.tradeshift.com/rest/challenger_prod/message.xml';
            $this->username = 'challenger_prod';
            $this->password = 'challenger2019';
        }
        else {
            $this->apiurl = 'https://esb1.tradeshift.com/rest/challenger/message.xml';
            $this->username = 'challenger_sand';
            $this->password = 'challenger2019';
        }
    }

    /**
     * @api {post} /api/challenger_po/vrc/:transID?code=Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjE SAMSUNG
     * @apiVersion 1.0.0
     * @apiGroup SA0006
     *
     * @apiSuccessExample Update Success
     * HTTP/1.1 200 OK
     *  {
     *      "code": 1,
     *      "message": "Update success",
     *  }
     *
     * @apiErrorExample Update Error
     *  {
     *      "code": 0,
     *      "message": "Update error",
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "return": "0",
     *      "message": "Request code is error, please check your request code.",
     *      "po_id": "IPVC190066393"
     *  }
     */
    public function send_vrc($trans_id){

        $data = $this->_getData($trans_id);
        $xml = $this->_doXml($data);
        $res = $this->_doCurl($xml, $trans_id, $data);
        if ($res['code'] == 1) {
            $result = array(
                "code" => 1,
                "message" => "Update success"
            );
        } else {
            $result = array(
                "code" => 0,
                "message" => "Update error"
            );
        }

        return $result;
    }

    private function _getData($trans_id){

        // Query data from DB

        $sql = "SELECT f.trans_id, to_char(f.trans_date, 'YYYY-MM-DD') AS trans_date, f.trans_desc, f.trans_amount, f.tax_percent, f.curr_id, s.coy_name, s.coy_reg_code, t.days_due, p.email_addr, c.street_line1, f.cust_supp_name, f.cust_supp_id, f.exchg_unit
                        FROM fin_journal f
                        LEFT JOIN sys_coy_list s ON s.coy_id = f.coy_id 
                        LEFT JOIN coy_address_book c ON c.ref_id = f.cust_supp_id AND c.ref_type = 'SUPPLIER'
                        LEFT JOIN pms_supplier_list p ON p.supp_id = f.cust_supp_id 
                            JOIN com_payment_term as t ON p.payment_id = t.payment_id
                        WHERE f.sys_id = 'APS' AND f.coy_id = 'CTL' AND f.trans_id = '$trans_id'";
        return $query = $this->db->query($sql)->result_array();
    }

    private function _doXml($data){
        // Format the XML

        if (trim($data[0]['cust_supp_id']) == 'SA0006'){
            $buyer_code = 'SAPL-SET';
            $address = '30 Pasir Panjang Road #17-31 Mapletree Business City';
            $post_code = '117440';
        } else {
            $buyer_code = 'SAPL-DS';
            $address = '3 Church Street, #26-01 Samsung Hub';
            $post_code = '049483';
        }

        $tax_amount = number_format($data[0]['trans_amount']*($data[0]['tax_percent']/(100+$data[0]['tax_percent'])), 2, '.', '');

        //print_r($tax_amount);
        $InvoiceNumber = $data[0]['trans_id'];
        $InvoiceDate = $data[0]['trans_date'];
        $SupplierCompanyName = trim($data[0]['coy_name']);
        $SupplierBRN = $data[0]['coy_reg_code'];
        $BuyerName = $data[0]['cust_supp_name'];
        $BuyerAddressStreetName = $data[0]['street_line1'];
        $Description = trim($data[0]['trans_desc']);
        $amount = number_format($data[0]['trans_amount'], 2, '.', '');
        $UnitPrice = $amount - $tax_amount;
        $Tax = $data[0]['tax_percent'];
        $quantity = $data[0]['exchg_unit'];
        $Subtotal = number_format($amount*$quantity, 2, '.', '');
        $GrandTotal = number_format($amount*$quantity, 2, '.', '');
        $PaymentTerms = $data[0]['days_due'];
        $Currency = $data[0]['curr_id'];
        $Email = $data[0]['email_addr'];


        $xml='
<?xml version="1.0" encoding="UTF-8"?>
<Invoice
    xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"
    xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"
    xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
    xmlns:ccts="urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2"
    xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"
    xmlns:ns7="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"
    xmlns:sdt="urn:oasis:names:specification:ubl:schema:xsd:SpecializedDatatypes-2"
    xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2">
    <ext:UBLExtensions>
        <ext:UBLExtension>
            <ext:ExtensionURI>urn:oasis:names:specification:ubl:dsig:enveloped</ext:ExtensionURI>
            <ext:ExtensionContent>
                <sig:UBLDocumentSignatures
                    xmlns:sig="urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2">
                    <sac:SignatureInformation
                        xmlns:sac="urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2">
                        <cbc:ID>urn:oasis:names:specification:ubl:signatures:1</cbc:ID>
                        <Signature
                            xmlns="http://www.w3.org/2000/09/xmldsig#">
                            <SignedInfo>
                                <CanonicalizationMethod
                                        Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></CanonicalizationMethod>
                                <SignatureMethod
                                        Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"></SignatureMethod>
                                <Reference URI="">
                                    <Transforms>
                                        <Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116">
                                            <XPath
                                                xmlns:sig="urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2">
                                                count(ancestor-or-self::sig:UBLDocumentSignatures |
                                                here()/ancestor::sig:UBLDocumentSignatures[1]) &gt;
                                                count(ancestor-or-self::sig:UBLDocumentSignatures)
                                            
                                            
                                            </XPath>
                                        </Transform>
                                    </Transforms>
                                    <DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></DigestMethod>
                                    <DigestValue>47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=</DigestValue>
                                </Reference>
                            </SignedInfo>
                            <SignatureValue>WA7kGkeg3TQeT2CI+G74HYN8FDcQQhsXLFuxR+y+t1qzdSzwHK6a442pg45WUXdYU6IOY/sNAMsC
                                VpTzpv6wFFcyZ33Kl3UeszVD/27u0qW8WF2o+2wZruSAyhXq2Rtzp8m9PJnyEReQqdTAjZEnkwOT
                                Ud+3PBiW2pp+tsBkexo=
                            </SignatureValue>
                            <KeyInfo>
                                <KeyValue>
                                    <RSAKeyValue>
                                        <Modulus>
                                            qjPnoh/BgvN22UWUVcwVYr9xWj49ffp2obvmR5WttIJssS5ZbCYOxjIjO3gIcNAu6NLFn5gpsp95
                                            FPNY1JDGII1qPnp9zyI6HKyA3yb5Vq9ONm2cLRfOz2zrvPdG+38ZLMzHe1rLALXEoIqfJWWt3u2B
                                            UvWP+h5ZYzm8px1gmJM=
                                        </Modulus>
                                        <Exponent>AQAB</Exponent>
                                    </RSAKeyValue>
                                </KeyValue>
                                <X509Data>
                                    <X509Certificate>
                                        MIICATCCAWoCCQCo1AOqHHrvcDANBgkqhkiG9w0BAQUFADBFMQswCQYDVQQGEwJBVTETMBEGA1UE
                                        CBMKU29tZS1TdGF0ZTEhMB8GA1UEChMYSW50ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMB4XDTEwMDQw
                                        OTA5MTkyN1oXDTI5MTIyNTA5MTkyN1owRTELMAkGA1UEBhMCQVUxEzARBgNVBAgTClNvbWUtU3Rh
                                        dGUxITAfBgNVBAoTGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDCBnzANBgkqhkiG9w0BAQEFAAOB
                                        jQAwgYkCgYEAqjPnoh/BgvN22UWUVcwVYr9xWj49ffp2obvmR5WttIJssS5ZbCYOxjIjO3gIcNAu
                                        6NLFn5gpsp95FPNY1JDGII1qPnp9zyI6HKyA3yb5Vq9ONm2cLRfOz2zrvPdG+38ZLMzHe1rLALXE
                                        oIqfJWWt3u2BUvWP+h5ZYzm8px1gmJMCAwEAATANBgkqhkiG9w0BAQUFAAOBgQARLOs0egYgj7q7
                                        mN0uthdbzAEg75Ssgh4JuOJ3iXI/sbqAIQ9uwsLodo+Fkpb5AiLlNFu7mCZXG/SzAAO3ZBLAWy4S
                                        KsXANu2/s6U5ClYd93HoZwzXobKb+2+aMf7KiAg1wHPUcyKx2c5nplgqQ7Hwldk9S9yzaRsYEGWT
                                        +xpSUA==
                                    </X509Certificate>
                                </X509Data>
                            </KeyInfo>
                        </Signature>
                    </sac:SignatureInformation>
                </sig:UBLDocumentSignatures>
            </ext:ExtensionContent>
        </ext:UBLExtension>
    </ext:UBLExtensions>
    <cbc:UBLVersionID>2.0</cbc:UBLVersionID>
    <cbc:CustomizationID>urn:tradeshift.com:ubl-2.0-customizations:2010-06</cbc:CustomizationID>
    <cbc:ProfileID schemeAgencyID="CEN/ISSS WS/BII" schemeID="CWA 16073:2010" schemeVersionID="1">
        urn:www.cenbii.eu:profile:bii04:ver1.0
    </cbc:ProfileID>
    <cbc:ID>'.$InvoiceNumber.'</cbc:ID>
    <cbc:IssueDate>'.$InvoiceDate.'</cbc:IssueDate>
    <cbc:InvoiceTypeCode listAgencyID="6" listID="UN/ECE 1001 Subset" listVersionID="D08B">380</cbc:InvoiceTypeCode>
    <cbc:Note>CHALLENGER TECHNOLOGIES LTD
        UOB BANK
        A/c no.: 401-319-538-7
    </cbc:Note>
    <cbc:DocumentCurrencyCode>'.$Currency.'</cbc:DocumentCurrencyCode>
    <cbc:AccountingCost>SAPL-SET</cbc:AccountingCost>
    <cac:AccountingSupplierParty>
        <cac:Party>
            <cac:PartyIdentification>
                <cbc:ID schemeID="SG:UEN">'.$SupplierBRN.'</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name>'.$SupplierCompanyName.'</cbc:Name>
            </cac:PartyName>
            <cac:PostalAddress>
                <cbc:AddressFormatCode listAgencyID="6" listID="UN/ECE 3477" listVersionID="D08B">5
                </cbc:AddressFormatCode>
                <cbc:StreetName>1 UBI LINK CHALLENGER TECHUB</cbc:StreetName>
                <cbc:BuildingNumber></cbc:BuildingNumber>
                <cbc:CityName>SINGAPORE</cbc:CityName>
                <cbc:PostalZone>408553</cbc:PostalZone>
                <cac:Country>
                    <cbc:IdentificationCode>SG</cbc:IdentificationCode>
                </cac:Country>
            </cac:PostalAddress>
        </cac:Party>
    </cac:AccountingSupplierParty>
    <cac:AccountingCustomerParty>
        <cac:Party>
            <cac:PartyIdentification>
                <cbc:ID schemeID="TS:GLI" schemeName="Tradeshift ID">com.samsung:SAPL</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyIdentification>
                <cbc:ID schemeID="SG:GSTN">'.$buyer_code.'</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name>'.$BuyerName.'</cbc:Name>
            </cac:PartyName>
            <cac:PostalAddress>
                <cbc:AddressFormatCode listAgencyID="6" listID="UN/ECE 3477" listVersionID="D08B">5
                </cbc:AddressFormatCode>
                <cbc:StreetName>'.$address.'</cbc:StreetName>
                <cbc:BuildingNumber></cbc:BuildingNumber>
                <cbc:CityName>SINGAPORE</cbc:CityName>
                <cbc:PostalZone>'.$post_code.'</cbc:PostalZone>
                <cac:Country>
                    <cbc:IdentificationCode>SG</cbc:IdentificationCode>
                </cac:Country>
            </cac:PostalAddress>
            <cac:PartyTaxScheme>
                <cbc:CompanyID schemeID="SG:GSTN">200610380D</cbc:CompanyID>
                <cac:TaxScheme>
                    <cbc:Name>VAT</cbc:Name>
                </cac:TaxScheme>
            </cac:PartyTaxScheme>
            <cac:Contact>
                <cbc:ID>'.$Email.'</cbc:ID>
            </cac:Contact>
        </cac:Party>
    </cac:AccountingCustomerParty>
    <cac:Delivery></cac:Delivery>
    <cac:TaxTotal>
        <cbc:TaxAmount currencyID="SGD">'.$tax_amount.'</cbc:TaxAmount>
        <cac:TaxSubtotal>
            <cbc:TaxableAmount currencyID="SGD">'.$amount.'</cbc:TaxableAmount>
            <cbc:TaxAmount currencyID="SGD">'.$tax_amount.'</cbc:TaxAmount>
            <cac:TaxCategory>
                <cbc:ID schemeAgencyID="6" schemeID="UN/ECE 5305" schemeVersionID="D08B">S</cbc:ID>
                <cbc:Percent>'.$Tax.'</cbc:Percent>
                <cac:TaxScheme>
                    <cbc:ID schemeAgencyID="6" schemeID="UN/ECE 5153 Subset" schemeVersionID="D08B">VAT</cbc:ID>
                    <cbc:Name>SG GST '.$Tax.'%</cbc:Name>
                </cac:TaxScheme>
            </cac:TaxCategory>
        </cac:TaxSubtotal>
    </cac:TaxTotal>
    <cac:LegalMonetaryTotal>
        <cbc:LineExtensionAmount currencyID="SGD">'.$UnitPrice.'</cbc:LineExtensionAmount>
        <cbc:TaxExclusiveAmount currencyID="SGD">'.$UnitPrice.'</cbc:TaxExclusiveAmount>
        <cbc:TaxInclusiveAmount currencyID="SGD">'.$amount.'</cbc:TaxInclusiveAmount>
        <cbc:PayableAmount currencyID="SGD">'.$GrandTotal.'</cbc:PayableAmount>
    </cac:LegalMonetaryTotal>
    <cac:InvoiceLine>
        <cbc:ID>1</cbc:ID>
        <cbc:InvoicedQuantity unitCode="EA">'.$quantity.'</cbc:InvoicedQuantity>
        <cbc:LineExtensionAmount currencyID="SGD">'.$UnitPrice.'</cbc:LineExtensionAmount>
        <cac:TaxTotal>
            <cbc:TaxAmount currencyID="SGD">'.$tax_amount.'</cbc:TaxAmount>
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="SGD">'.$amount.'</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="SGD">'.$tax_amount.'</cbc:TaxAmount>
                <cbc:CalculationSequenceNumeric>1</cbc:CalculationSequenceNumeric>
                <cac:TaxCategory>
                    <cbc:ID schemeAgencyID="6" schemeID="UN/ECE 5305" schemeVersionID="D08B">'.$PaymentTerms.'</cbc:ID>
                    <cbc:Percent>7</cbc:Percent>
                    <cac:TaxScheme>
                        <cbc:ID schemeAgencyID="6" schemeID="UN/ECE 5153 Subset" schemeVersionID="D08B">VAT</cbc:ID>
                        <cbc:Name>SG GST '.$Tax.'%</cbc:Name>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        </cac:TaxTotal>
        <cac:Item>
            <cbc:Description>'.$Description.'</cbc:Description>
            <cbc:Name>'.$Description.'</cbc:Name>
            <cac:SellersItemIdentification>
                <cbc:ID schemeAgencyID="9" schemeID="GTIN"></cbc:ID>
            </cac:SellersItemIdentification>
        </cac:Item>
        <cac:Price>
            <cbc:PriceAmount currencyID="SGD">'.$amount.'</cbc:PriceAmount>
            <cbc:BaseQuantity unitCode="EA">'.$quantity.'</cbc:BaseQuantity>
            <cbc:OrderableUnitFactorRate>1</cbc:OrderableUnitFactorRate>
        </cac:Price>
    </cac:InvoiceLine>
</Invoice>
        ';


        return $xml;

    }

    private function _doCurl($xml, $trans_id, $data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiurl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($ch);
        curl_close($ch);

        $rtn = ($return!="") ? true : false;

        if ($rtn) {
            $res = array(
                "code" => 1,
                "status" => "OK",
                "message" => "success"
            );
        }
        else {
            $res = array(
                "code" => 0,
                "status" => "XOK",
                "message" => "error"
            );
        }
        // Save to DB
        $sql_ip = getRealIpAddr(15);
        $msg_request = $xml; //'Samsung Tradeshift: ' + $trans_id + ' for (' + $data[0]['cust_supp_id'] + ')';
        $rtn_code = $res['code'];
        $rtn_msg = $res['status'];
        $output = $return; //$res['message'];
        $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,url_link,msg_request,status_code,status_text,msg_response,created_by,created_on,modified_by,modified_on)
                        VALUES ('CTL','CL','".$trans_id."','$this->apiurl','$msg_request','$rtn_code','$rtn_msg','$output','$sql_ip',now(),'$sql_ip',now() ); ";
        $query = $this->db->query($sql);
        return $res;
    }

}


?>
