<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Sh0004_model extends Base_Common_Model {

    private $xml_receipient;
    private $fixed_supp_id;
    private $send;

    public function __construct() {
        parent::__construct();

        // DECLARE const in this API
        if (ENVIRONMENT=="production") {
            $this->xml_receipient = 'yongsheng@challenger.sg';
            $this->fixed_supp_id = 'SH0004';
            $this->send = true;
        }
        else {
            $this->xml_receipient = 'yongsheng@challenger.sg';
            $this->fixed_supp_id = 'SH0004';
            $this->send = false;
        }
    }

    public function send_order($po_id){

        $data = $this->ordexp_po_get($po_id);
        $xml = $this->xml($data);

        $supp_id = trim($data[0]['supp_id']);
        $email = 'yongsheng@challenger.sg, edi-inbound@sennheiser.com ';

        $handle_path = FCPATH . 'assets/uploads/';
        $uploaded = $supp_id . '_PO_'.trim($po_id).'.xml';
        file_put_contents($handle_path.$uploaded, $xml);

        if ($this->send) {
            if ($this->ordexp_email($po_id, $email, 'Sennheiser PO ' . $po_id, 'Download PO Details from XML', $handle_path . $uploaded)) {
                return array('code' => '01', 'msg' => $po_id . ' Email sent');
            } else {
                return array('code' => '-1', 'msg' => $po_id . ' Email failed');
            }
        }
        else {
            return array('code' => '-1', 'msg' => $po_id . ' Simulated. No email was sent.');
        }

    }

    public function ordexp_email($po_id,$email,$subject,$message,$attach){
        $params['email'] = $email;
        $params['subject'] = $subject;
        $params['message'] = $message;
        $params['attach'] = $attach;
        $rtn = $this->_SendEmailbySmtp($params);

        $supp_id = $this->fixed_supp_id;
        if ($rtn) {
            $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,url_link,msg_request,status_code,status_text,msg_response,created_by,created_on,modified_by,modified_on)
                        VALUES ( 'CTL','PO','$po_id','mailto: $email','$subject (Attach: $attach)',1,'OK','','$supp_id',now(),'$supp_id',now() ); ";
            $query = $this->db->query($sql);
        }
        else {
            $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,url_link,msg_request,status_code,status_text,msg_response,created_by,created_on,modified_by,modified_on)
                        VALUES ( 'CTL','PO','$po_id','mailto: $email','$subject (Attach: $attach)',0,'FAIL','','$supp_id',now(),'$supp_id',now() ); ";
            $query = $this->db->query($sql);
        }

        return $rtn;
    }

//    public function phpmailer($po_id,$email,$subject,$message,$attach){
//        $this->load->library('email');
//
//        $result = $this->email
//            ->from('mis@challenger.sg')
//            ->reply_to('mis@challenger.sg')
//            ->to($email)
//            ->subject($subject)
//            ->message($message)
//            ->attach($attach)
//            ->send();
//
//        return $result;
//    }

    public function ordexp_po_get($po_id){
        $sql = "SELECT
                    pms_po_list.po_id,pms_po_list.po_date,pms_po_list.supp_id,pms_po_list.approve_date,
                    pms_po_list.loc_id,pms_po_list.loc_addr,pms_po_list.loc_name,pms_po_list.tel_code,
                    c.street_line1,c.street_line2,c.country_id,c.postal_code,c.addr_text,
                    pms_po_list.delv_date,
                    pms_po_item.line_num,pms_po_item.item_id,pms_po_item.item_desc,pms_po_item.qty_order,pms_po_item.unit_price,pms_po_item.long_desc
                FROM pms_po_item
                    LEFT JOIN pms_po_list ON pms_po_list.po_id = pms_po_item.po_id
                    LEFT JOIN coy_address_book c ON c.coy_id=pms_po_list.coy_id and c.ref_type='LOCATION' and c.ref_id=pms_po_list.loc_id and c.addr_type=pms_po_list.loc_addr
                WHERE pms_po_item.po_id = rtrim('$po_id') and pms_po_list.supp_id in ('".$this->fixed_supp_id."')
                    AND pms_po_item.rev_num = pms_po_list.current_rev
                    AND pms_po_list.rev_num = pms_po_list.current_rev 
                    AND pms_po_item.coy_id ='CTL'
                ORDER BY line_num ASC";
        $res = $query = $this->db->query($sql)->result_array();
        return $res;
    }

    public function xml($data){

        $calc_totalamt = 0; $xml_productlines='';
        foreach ($data as $row) {
            $calc_totalamt += floatval($row['qty_order'] * $row['unit_price']);
            $xml_productlines .= '
<ProductLineItem>
    <comments><FreeFormText>'.trim($row['long_desc']).'</FreeFormText></comments>
    <GlobalProductUnitOfMeasureCode>pcs</GlobalProductUnitOfMeasureCode>
    <isDropShip><AffirmationIndicator>No</AffirmationIndicator></isDropShip>
    <LineNumber>'.intval($row['line_num']).'</LineNumber>
    <OrderQuantity><requestedQuantity><ProductQuantity>'.intval($row['qty_order']).'</ProductQuantity></requestedQuantity></OrderQuantity>
    <OrderShippingInformation>
        <CarrierInformation>
            <accountIdentifier><ProprietaryReferenceIdentifier>Challenger</ProprietaryReferenceIdentifier></accountIdentifier>
            <GlobalCarrierCode>None</GlobalCarrierCode></CarrierInformation>
        <GlobalFreeOnBoardCode>Origin</GlobalFreeOnBoardCode>
        <GlobalShipmentTermsCode>DEL</GlobalShipmentTermsCode>
        <GlobalShippingServiceLevelCode>Standard Service</GlobalShippingServiceLevelCode>
        <SpecialHandlingInstruction><specialHandlingText><FreeFormText>None</FreeFormText></specialHandlingText></SpecialHandlingInstruction>
    </OrderShippingInformation>
    <ProductIdentification>
        <PartnerProductIdentification>
            <GlobalPartnerClassificationCode>End User</GlobalPartnerClassificationCode>
            <ProprietaryProductIdentifier>'.trim($row['item_id']).'</ProprietaryProductIdentifier>
        </PartnerProductIdentification>
        <PartnerProductIdentification>
            <GlobalPartnerClassificationCode>Manufacturer</GlobalPartnerClassificationCode>
            <ProprietaryProductIdentifier></ProprietaryProductIdentifier>
        </PartnerProductIdentification>
        <PartnerProductIdentification>
            <GlobalPartnerClassificationCode>Manufacturer</GlobalPartnerClassificationCode>
            <ProprietaryProductIdentifier>'.trim($row['item_desc']).'</ProprietaryProductIdentifier>
        </PartnerProductIdentification>
    </ProductIdentification>
    <requestedEvent><TransportationEvent><DateStamp>20160219Z</DateStamp><GlobalTransportEventCode>Dock</GlobalTransportEventCode></TransportationEvent></requestedEvent>
    <requestedUnitPrice><FinancialAmount><GlobalCurrencyCode>SGD</GlobalCurrencyCode><MonetaryAmount>'.floatval($row['unit_price']).'</MonetaryAmount></FinancialAmount></requestedUnitPrice>
    <totalLineItemAmount><FinancialAmount><GlobalCurrencyCode>SGD</GlobalCurrencyCode><MonetaryAmount>'.floatval($row['qty_order'] * $row['unit_price']).'</MonetaryAmount></FinancialAmount></totalLineItemAmount>
</ProductLineItem>';
        }

        $xml_pofinancialamt ='<FinancialAmount><GlobalCurrencyCode>SGD</GlobalCurrencyCode><MonetaryAmount>'.floatval($calc_totalamt).'</MonetaryAmount></FinancialAmount>';
        $xml_podocnumber = '<ProprietaryDocumentIdentifier>'.trim($data[0]['po_id']).'</ProprietaryDocumentIdentifier>';
        $xml_posupplier = '<GlobalBusinessIdentifier>'.trim($data[0]['supp_id']).'</GlobalBusinessIdentifier><GlobalSupplyChainCode>Electronic Components</GlobalSupplyChainCode>';
        $xml_podocdatetime = '<DateTimeStamp>'. date('Ymd\THis\Z',strtotime($data[0]['approve_date'])) .'</DateTimeStamp>'; //20160219T165604Z

        $xml_shipto = '<shipTo>
    <PartnerDescription>
        <BusinessDescription>
            <GlobalBusinessIdentifier>595136763</GlobalBusinessIdentifier>
            <businessName><FreeFormText>CHALLENGER TECHNOLOGIES LIMITED</FreeFormText></businessName>
        </BusinessDescription>
        <GlobalPartnerClassificationCode>End User</GlobalPartnerClassificationCode>
        <PhysicalLocation>
            <GlobalLocationIdentifier>'.trim($data[0]['loc_id']).'</GlobalLocationIdentifier>
            <PhysicalAddress>
                <addressLine1><FreeFormText>'.str_replace('&','',trim($data[0]['street_line1'])).'</FreeFormText></addressLine1>
                <addressLine2><FreeFormText>'.str_replace('&','',trim($data[0]['street_line2'])).'</FreeFormText></addressLine2>
                <cityName><FreeFormText></FreeFormText></cityName>
                <GlobalCountryCode>'.trim($data[0]['country_id']).'</GlobalCountryCode>
                <regionName><FreeFormText>'.trim($data[0]['addr_text']).'</FreeFormText></regionName>
            </PhysicalAddress>
        </PhysicalLocation>
    </PartnerDescription>
</shipTo>';

        $xml_accountdesc = '<AccountDescription>
    <accountName><FreeFormText>CHALLENGER TECHNOLOGIES LIMITED</FreeFormText></accountName>
    <AccountNumber>595136763</AccountNumber>
    <billTo>
        <PartnerDescription>
            <BusinessDescription>
                <GlobalBusinessIdentifier>595136763</GlobalBusinessIdentifier>
                <businessName><FreeFormText>CHALLENGER TECHNOLOGIES LIMITED</FreeFormText></businessName>
            </BusinessDescription>
            <GlobalPartnerClassificationCode>End User</GlobalPartnerClassificationCode>
            <ContactInformation><telephoneNumber><CommunicationsNumber>65 63189800</CommunicationsNumber></telephoneNumber></ContactInformation>
            <PhysicalLocation>
                <PhysicalAddress>
                    <addressLine1><FreeFormText>ATTN: ACCOUNTS PAYABLE</FreeFormText></addressLine1>
                    <addressLine2><FreeFormText>1 Ubi Link</FreeFormText></addressLine2>
                    <addressLine3><FreeFormText>Challenger TecHub</FreeFormText></addressLine3>
                    <cityName><FreeFormText></FreeFormText></cityName>
                    <GlobalCountryCode>SG</GlobalCountryCode>
                </PhysicalAddress>
            </PhysicalLocation>
        </PartnerDescription>
    </billTo>
</AccountDescription>';


        $xml='<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE Pip3A4PurchaseOrderRequest SYSTEM "3A4_MS_V02_02_PurchaseOrderRequest.dtd">
<Pip3A4PurchaseOrderRequest>
    <fromRole>
        <PartnerRoleDescription>
            <ContactInformation><contactName><FreeFormText>WONG TEEK HOK</FreeFormText></contactName><EmailAddress></EmailAddress><telephoneNumber><CommunicationsNumber></CommunicationsNumber></telephoneNumber></ContactInformation>
            <GlobalPartnerRoleClassificationCode>Purchase Order Provider</GlobalPartnerRoleClassificationCode>
            <PartnerDescription><BusinessDescription><GlobalBusinessIdentifier>595136763</GlobalBusinessIdentifier><GlobalSupplyChainCode>Electronic Components</GlobalSupplyChainCode></BusinessDescription><GlobalPartnerClassificationCode>Retailer</GlobalPartnerClassificationCode></PartnerDescription>
        </PartnerRoleDescription>
    </fromRole>
    <GlobalDocumentFunctionCode>Request</GlobalDocumentFunctionCode>
    <PurchaseOrder>
        '.$xml_accountdesc.'
        <FinancingTerms>
            <GlobalFinanceTermsCode>Terms established/determined by TPA</GlobalFinanceTermsCode>
            <PaymentTerms><GlobalPaymentConditionCode>45 Days</GlobalPaymentConditionCode><netTermsDays><CountableAmount>45</CountableAmount></netTermsDays><percentDue><PercentAmount>100</PercentAmount></percentDue></PaymentTerms>
        </FinancingTerms>
        <GlobalPurchaseOrderTypeCode>Replenishment</GlobalPurchaseOrderTypeCode>
        <isDropShip><AffirmationIndicator>No</AffirmationIndicator></isDropShip>
        '.$xml_productlines.'
        '.$xml_shipto.'
        <totalAmount>'.$xml_pofinancialamt.'</totalAmount>
    </PurchaseOrder>
    <thisDocumentGenerationDateTime>'.$xml_podocdatetime.'</thisDocumentGenerationDateTime>
    <thisDocumentIdentifier>'.$xml_podocnumber.'</thisDocumentIdentifier>
    <toRole>
        <PartnerRoleDescription>
            <GlobalPartnerRoleClassificationCode>Seller</GlobalPartnerRoleClassificationCode>
            <PartnerDescription>
                <BusinessDescription>'.$xml_posupplier.'</BusinessDescription>
                <GlobalPartnerClassificationCode>Manufacturer</GlobalPartnerClassificationCode>
            </PartnerDescription>
        </PartnerRoleDescription>
    </toRole>
</Pip3A4PurchaseOrderRequest>';


        return $xml;

    }

}

?>
