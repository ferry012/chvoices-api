<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Bl0001_model extends Base_Common_Model {

    private $fixed_supp_id;
    private $apiurl;
    private $apitoken;

    public function __construct() {

        parent::__construct();
        $this->fixed_supp_id = 'BL0001';

        if (ENVIRONMENT=="production") {
            $this->apiurl = 'http://121.200.244.125:8338/api/v1/Sales/ConfirmCHPO';
            $this->apitoken = "27d41b313ca44c15b75ef53b68551700.public";
        }
        else {
            $this->apiurl = 'http://121.200.244.125:8344/api/v1/Sales/ConfirmCHPO';
            $this->apitoken = "27d41b313ca44c15b75ef53b68551700.public";
        }

        $this->load->model('po2/Po2_model');

    }

    public function send_order($po_id){

        $data = $this->ordexp_po_get($po_id);
        $jsondata = $this->to_json($data);

        $supp_id = trim($data[0]['supp_id']);
        $result = $this->ordexp_api($supp_id,$po_id,$jsondata);

        if ( $result["code"] ) {

            // Read the data and confirm the PO
            $response = json_decode($result['data'], true);

            $confirmParam = [];
            $confirmParam["coy_id"] = $data[0]['coy_id'];
            $confirmParam["po_id"] = $data[0]['po_id'];
            $confirmParam["rev_num"] = $data[0]['rev_num'];
            $confirmParam['delv_date'] = date("Y-m-d H:i:s", strtotime($response['response']['delv_date']));
            foreach ($response['response']['items'] as $r=>$row) {
                $confirmParam['items'][] = [
                    'line_num'      => $row['line_num'],
                    'qty_ordered'   => $row['qty_order'],
                    'new_qty'       => $row['qty_confirmed'],
                ];
            }
            $po_confirm = $this->Po2_model->do_po_confirmation($confirmParam);

            if ($po_confirm['return']!='1') {
                // Fail to confirm BL0001 PO
                $subject = 'API Fail - BL0001 PO Confirmation';
                $message = 'PO ' . $confirmParam["po_id"] . ' is confirmed by Vendor but confirmation failed.\n\n' . json_encode($po_confirm);
                $email = 'yongsheng@challenger.sg;';
                $this->cherps_email($subject,$message,$email);
            }

            return array('code'=>'01','msg'=>$po_id . ' API sent - ' . $result["response"]);
        }
        else {
            return array('code'=>'-1','msg'=>$po_id . ' API failed - ' . $result["response"] );
        }

    }

    public function ordexp_api($supp_id,$po_id,$jdata){

        $headers[] = "Content-Type: application/json";
        $headers[] = "Authorization: " . $this->apitoken;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiurl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        if (curl_errno($ch)) {
            // this would be your first hint that something went wrong
            die('Couldn\'t send request: ' . curl_error($ch));
        } else {
            // check the HTTP status code of the request
            $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($resultStatus == 200) {
                // everything went better than expected
            } else {
                // the request did not complete as expected. common errors are 4xx
                // (not found, bad request, etc.) and 5xx (usually concerning
                // errors/exceptions in the remote script execution)

                die('Request failed: HTTP status code: ' . $resultStatus);
            }
        }

        curl_close($ch);

        if ($output) {
            $outresp = json_decode($output,true);

            $sqlparam['status_code'] = ($outresp['isSuccess']) ? 1 : 0;
            $sqlparam['status_code_sql'] = ($outresp['isSuccess']) ? 1 : 0;
            $sqlparam['status_text'] = ($outresp['isSuccess']) ? 'OK' : 'FAIL';
            $sqlparam['message'] = $outresp['returnMessage'];

            if ( strpos($outresp['returnMessage'],'have already confirmed') > 0) {
                // This PO is confirmed, log as status_code=1
                $sqlparam['status_code_sql'] = 1;
            }

        }
        else {
            $sqlparam['status_code'] = 0;
            $sqlparam['status_code_sql'] = 0;
            $sqlparam['status_text'] = 'FAIL';
            $sqlparam['message'] = 'No response';
        }
        $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,
                                                  url_link,msg_request,
                                                  status_code,status_text,msg_response,
                                                  created_by,created_on,modified_by,modified_on)
                        VALUES ( 'CTL','PO','$po_id',
                                  '".$this->apiurl."','".$jdata."',
                                  ".$sqlparam['status_code_sql'].",'".$sqlparam['status_text']."','".$output."',
                                  '$supp_id',NOW(),'$supp_id',NOW() ); ";
        $query = $this->db->query($sql);

        return array("code" => $sqlparam['status_code'], "response" => $sqlparam['message'], "data" => $output);
    }

    public function ordexp_po_get($po_id){
        $sql = "SELECT
                    pms_po_list.coy_id,pms_po_list.po_id,pms_po_list.rev_num,pms_po_list.po_date,pms_po_list.supp_id,pms_po_list.approve_date,pms_po_list.delv_date,
                    pms_po_list.loc_id,pms_po_list.loc_addr,pms_po_list.loc_name,pms_po_list.tel_code,
                    c.street_line1,c.street_line2,c.country_id,c.postal_code,c.addr_text,
                    pms_po_list.delv_date,
                    pms_po_item.line_num,pms_po_item.item_id,pms_po_item.item_desc,pms_po_item.qty_order,pms_po_item.unit_price,LTRIM(RTRIM(pms_po_item.long_desc)) long_desc
                FROM pms_po_item
                    LEFT JOIN pms_po_list ON pms_po_list.po_id = pms_po_item.po_id
                    LEFT JOIN coy_address_book c ON c.coy_id=pms_po_list.coy_id and c.ref_type='LOCATION' and c.ref_id=pms_po_list.loc_id and c.addr_type=pms_po_list.loc_addr
                WHERE pms_po_item.po_id = rtrim('$po_id') and pms_po_list.supp_id in ('".$this->fixed_supp_id."')
                    AND pms_po_item.rev_num = pms_po_list.current_rev
                    AND pms_po_list.rev_num = pms_po_list.current_rev 
                    AND pms_po_item.coy_id ='CTL'
                ORDER BY line_num ASC";
        return $query = $this->db->query($sql)->result_array();
    }

    public function to_json($data){

        $jsonarr = $data;// [];
        $items = [];
        foreach($data as $d) {
            $items[] = [
                'line_num'      => $d['line_num'],
                'item_id'       => $d['item_id'],
                'item_desc'     => $d['item_desc'],
                'qty_order'     => $d['qty_order'],
                'unit_price'    => $d['unit_price'],
                'long_desc'     => $d['long_desc'],
            ];

            $jsonarr = [
                'po_id'     => $d['po_id'],
                'po_date'   => $d['po_date'],
                'delv_date' => $d['delv_date'],
                'loc_id'    => $d['loc_id'],
                'supp_id'   => (isset($d['supp_id'])) ? $d['supp_id'] : ''
            ];
        }

        $jsonarr['Items'] = $items;

        return json_encode($jsonarr);

    }

    public function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {

        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = '';
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }

}

?>
