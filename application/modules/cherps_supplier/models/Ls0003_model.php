<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Ls0003_model extends Base_Common_Model {

    private $fixed_supp_id;
    private $apiurl;
    private $apitoken;

    public function __construct() {
        parent::__construct();
        $this->fixed_supp_id = 'LS0003';

        // DECLARE const in this API
//        if (ENVIRONMENT=="production") {
//            $this->apiurl = 'https://sparkosb.logitech.com/POInboundIntSvc/ProxyServices/POInboundIntPS';
//            $this->apitoken = 'c3J2LWNvcnAtTG9naVdEVXNlcjpMb2dpdGVjaDEyMyE=';
//        }
//        else {
//            $this->apiurl = 'https://b2btest.logitech.com:7451/POInboundIntSvc/ProxyServices/POInboundIntPS';
//            $this->apitoken = 'YXBpaW50dXNyOkxvZ2lBUElJbnRTdmMh';
//        }

        // Since Feb 2021, change new API
        if (ENVIRONMENT=="production") {
            $this->apiurl = 'https://eaiprod.logitech.com:8450/POInboundIntSvc/ProxyServices/POInboundIntPS';
            $this->apitoken = "srv-corp-LogiWDUser:Logitech123!";
        }
        else {
            $this->apiurl = 'https://b2btest.logitech.com:8450/POInboundIntSvc/ProxyServices/POInboundIntPS';
            $this->apitoken = "srv-corp-LogiWDUser:Logitech123!";
        }
    }

    public function send_order($po_id){

        $data = $this->ordexp_po_get($po_id);
        $xml = $this->xml($data);

        $supp_id = trim($data[0]['supp_id']);
        $result = $this->ordexp_api($supp_id,$po_id,$xml);

        if ( $result["code"] ) {
            return array('code'=>'01','msg'=>$po_id . ' API sent - ' . $result["response"] );
        }
        else {
            return array('code'=>'-1','msg'=>$po_id . ' API failed - ' . $result["response"] );
        }

    }

    public function ordexp_api($supp_id,$po_id,$xml){

        $headers[] = "Content-Type: text/XML";
        //$headers[] = "Authorization: Basic " . $this->apitoken;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiurl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERPWD, $this->apitoken);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $output = curl_exec($ch);

        if (curl_errno($ch)) {
            // this would be your first hint that something went wrong
            die('Couldn\'t send request: ' . curl_error($ch));
        } else {
            // check the HTTP status code of the request
            $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($resultStatus == 200) {
                // everything went better than expected
            } else {
                // the request did not complete as expected. common errors are 4xx
                // (not found, bad request, etc.) and 5xx (usually concerning
                // errors/exceptions in the remote script execution)

                die('Request failed: HTTP status code: ' . $resultStatus);
            }
        }

        curl_close($ch);

        $rtn = ($output!="") ? true : false;

        if ($rtn) {
            $sqlparam['status_code'] = 1;
            $sqlparam['status_text'] = 'OK';
        }
        else {
            $sqlparam['status_code'] = 0;
            $sqlparam['status_text'] = 'FAIL';
        }
        $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,
                                                  url_link,msg_request,
                                                  status_code,status_text,msg_response,
                                                  created_by,created_on,modified_by,modified_on)
                        VALUES ( 'CTL','PO','$po_id',
                                  '".$this->apiurl."','".$xml."',
                                  ".$sqlparam['status_code'].",'".$sqlparam['status_text']."','".$output."',
                                  '$supp_id',NOW(),'$supp_id',NOW() ); ";
        $query = $this->db->query($sql);

        return array("code" => $rtn, "response" => $output);
    }

    public function ordexp_po_get($po_id){
        $sql = "SELECT
                    pms_po_list.po_id,pms_po_list.po_date,pms_po_list.supp_id,pms_po_list.approve_date,pms_po_list.delv_date,
                    pms_po_list.loc_id,pms_po_list.loc_addr,pms_po_list.loc_name,pms_po_list.tel_code,
                    c.street_line1,c.street_line2,c.country_id,c.postal_code,c.addr_text,
                    pms_po_list.delv_date,
                    pms_po_item.line_num,pms_po_item.item_id,pms_po_item.item_desc,pms_po_item.qty_order,pms_po_item.unit_price,LTRIM(RTRIM(pms_po_item.long_desc)) long_desc
                FROM pms_po_item
                    LEFT JOIN pms_po_list ON pms_po_list.po_id = pms_po_item.po_id
                    LEFT JOIN coy_address_book c ON c.coy_id=pms_po_list.coy_id and c.ref_type='LOCATION' and c.ref_id=pms_po_list.loc_id and c.addr_type=pms_po_list.loc_addr
                WHERE pms_po_item.po_id = rtrim('$po_id') and pms_po_list.supp_id in ('".$this->fixed_supp_id."')
                    AND pms_po_item.rev_num = pms_po_list.current_rev
                    AND pms_po_list.rev_num = pms_po_list.current_rev 
                    AND pms_po_item.coy_id ='CTL'
                ORDER BY line_num ASC";
        return $query = $this->db->query($sql)->result_array();
    }

    public function xml($data){

        $DateTimeStampMSP = date('d-M-Y H:i:s',strtotime($data[0]['po_date'])); //'12-Feb-2018 01:28:14';
        $OrderNumber = trim($data[0]['po_id']);
        $DocumentId = strtotime($data[0]['po_date']);
        $loc_id = trim($data[0]['loc_id']);
        $OrderRequestDateValues = date('Ymd',strtotime($data[0]['po_date']));
        $RequestedDeliverByDate = date('Ymd',strtotime($data[0]['delv_date']));

        $PurchaseOrderDetail = '';
        foreach ($data as $row) {
            $PurchaseOrderDetail .= '
			<ns:ItemDetail>
				<ns:LineItemNum>'.$row['line_num'].'</ns:LineItemNum>
				<ns:Quantity>'.$row['qty_order'].'</ns:Quantity>
				<ns:UnitOfMeasurement><ns:UOMCoded>EA</ns:UOMCoded></ns:UnitOfMeasurement>
				<ns:LineRequestDate>'.$RequestedDeliverByDate.'</ns:LineRequestDate>
				<ns:ItemIdentifiers>
					<ns:PartNumbers>
						<ns:BuyerPartNumber>
							<ns:PartNum>
								<ns:PartID>'.trim($row['item_id']).'</ns:PartID>
							</ns:PartNum>
						</ns:BuyerPartNumber>
						<ns:SellerPartNumber>
							<ns:PartNum>
								<ns:PartID>'.$row['long_desc'].'</ns:PartID>
							</ns:PartNum>
						</ns:SellerPartNumber>
					</ns:PartNumbers>
				</ns:ItemIdentifiers>
				<ns:UnitPrice>'.$row['unit_price'].'</ns:UnitPrice>
			</ns:ItemDetail>';
        }

        $xml='<?xml version="1.0" encoding="UTF-8"?>
<ns:LogiPurchaseOrder
	xmlns:ns="http://www.logitech.com/retail/1.0">
	<ns:MSPHeader>
		<ns:PartnerName>Challenger</ns:PartnerName>
		<ns:PartnerEnvelopeId>Challenger</ns:PartnerEnvelopeId>
		<ns:LogitechEnvelopeId>024237877</ns:LogitechEnvelopeId>
		<ns:EDIStandard>EDIFACT</ns:EDIStandard>
		<ns:EDIVersion>3</ns:EDIVersion>
		<ns:PartnerGroupId>Challenger</ns:PartnerGroupId>
		<ns:LogitechGroupId>024237877</ns:LogitechGroupId>
		<ns:GroupType>PO</ns:GroupType>
		<ns:GroupVersionRelease>97A</ns:GroupVersionRelease>
		<ns:InterfaceName>EDI 850 PO INBOUND</ns:InterfaceName>
		<ns:Direction>Inbound</ns:Direction>
		<ns:DateTimeStampMSP>'.$DateTimeStampMSP.'</ns:DateTimeStampMSP>
		<ns:DocumentId>'.$DocumentId.'</ns:DocumentId>
		<ns:DocumentType>ORDERS</ns:DocumentType>
	</ns:MSPHeader>
	<ns:PurchaseOrder>
		<ns:PurchaseOrderHeader>
			<ns:OrderNumber>'.$OrderNumber.'</ns:OrderNumber>
			<ns:OracleSysReference>'.$OrderNumber.'</ns:OracleSysReference>
			<ns:OrderCurrency>
				<ns:Currency>
					<ns:CurrencyCoded>SGD</ns:CurrencyCoded>
				</ns:Currency>
			</ns:OrderCurrency>
			<ns:OrderRequestDate>
				<ns:OrderRequestDateQualifer>4</ns:OrderRequestDateQualifer>
				<ns:OrderRequestDateValues>'.$OrderRequestDateValues.'</ns:OrderRequestDateValues>
			</ns:OrderRequestDate>
			<ns:OrderDates>
				<ns:RequestedDeliverByDate>'.$RequestedDeliverByDate.'</ns:RequestedDeliverByDate>
			</ns:OrderDates>
			<ns:OrderParty>
				<ns:ShipToParty>
					<ns:Party>
						<ns:PartyNameAddress>
							<ns:ExternalAddressID>'.$loc_id.'</ns:ExternalAddressID>
						</ns:PartyNameAddress>
					</ns:Party>
				</ns:ShipToParty>
			</ns:OrderParty>
		</ns:PurchaseOrderHeader>
		<ns:PurchaseOrderDetail>
		    '.$PurchaseOrderDetail.'
		</ns:PurchaseOrderDetail>
	</ns:PurchaseOrder>
</ns:LogiPurchaseOrder>';


        return $xml;

    }

}

?>
