<?php
!defined('BASEPATH') OR ( 'No direct script access allowed');

class Er0001_model extends Base_Common_Model {

    private $apiurl;
    private $supp_id;
    private $loc_id;

    public function __construct() {
        parent::__construct();
        $this->supp_id = 'ER0001';
        $this->loc_id = 'IM-C';
        $this->token = 'Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjE';

        if (ENVIRONMENT=="production") {
            $this->apiurl = 'https://mercury.ingrammicro.com/SeeburgerHTTP/HTTPController?HTTPInterface=syncReply'; // Production
        }
        else {
            $this->apiurl = 'https://venus.ingrammicro.com/SeeburgerHTTP/HTTPController?HTTPInterface=syncReply'; // Testing
        }
    }

    public function send_order($po_id){

        $data = $this->ordexp_po_get($po_id);

        if (trim($data[0]['supp_id'])==$this->supp_id) {
            $curlquery = $this->_doParams($data);
            $transmit = $this->_doCurl($curlquery, $po_id);

            if ($transmit['code']) {
                return array('code' => '1', 'msg' => 'XML '.$po_id.' sent.');
            } else {
                return array('code' => '-1', 'msg' => 'XML '.$po_id.' failed - ' . strip_tags($transmit["response"]));
            }
        }
        else {
            return array('code' => '-9', 'msg' => $po_id . ' ('.trim($data[0]['supp_id']).') does not belong to Supplier '.$this->supp_id.'.');
        }

    }

    public function ordexp_po_get($po_id){
        $sql = "SELECT
                    (
                        CASE WHEN ( select inv_dim4 from ims_item_list where coy_id = pms_po_item.coy_id and item_id = pms_po_item.item_id) like '%MS-ESD%' THEN '1-MS-ESD'
                        WHEN ( select inv_dim4 from ims_item_list where coy_id = pms_po_item.coy_id and item_id = pms_po_item.item_id) like '%XBOX-ESD%' THEN '1-MS-ESD'
                        WHEN ( select inv_dim4 from ims_item_list where coy_id = pms_po_item.coy_id and item_id = pms_po_item.item_id) like '%ESD%' THEN '2-OTHER-ESD'
                        ELSE '0-NON-ESD' END
                    ) txn_item_type,
                    rtrim(pms_po_item.ref_id) txn_id,
                    pms_po_list.po_id,pms_po_list.po_date,pms_po_list.supp_id,pms_po_list.approve_date,pms_po_list.delv_date,
                    pms_po_list.loc_id,pms_po_list.loc_addr,pms_po_list.loc_name,pms_po_list.tel_code,
                    c.street_line1,c.street_line2,c.country_id,c.postal_code,c.addr_text,
                    pms_po_list.delv_date,
                    pms_po_item.line_num,pms_po_item.item_id,pms_po_item.item_desc,pms_po_item.qty_order,pms_po_item.unit_price,LTRIM(RTRIM(pms_po_item.long_desc)) long_desc,
                    (
                        CASE WHEN EXISTS(select tel_code from pos_transaction_list where trans_id=pms_po_item.ref_id) THEN (select tel_code from pos_transaction_list where trans_id=pms_po_item.ref_id limit 1)
                        ELSE (coalesce((select tel_code from sms_invoice_list where invoice_id=pms_po_item.ref_id limit 1),'')) END
                    ) tel_code,
                    (
                        CASE WHEN EXISTS(select cust_name from pos_transaction_list where trans_id=pms_po_item.ref_id) THEN (select cust_name from pos_transaction_list where trans_id=pms_po_item.ref_id limit 1)
                        ELSE (coalesce((select cust_name from sms_invoice_list where invoice_id=pms_po_item.ref_id limit 1),'')) END
                    ) cust_name,
                    (
                        CASE WHEN EXISTS(select email_addr from pos_transaction_list where trans_id=pms_po_item.ref_id) THEN (select email_addr from pos_transaction_list where trans_id=pms_po_item.ref_id limit 1)
                        ELSE (coalesce((select email_addr from sms_invoice_list where invoice_id=pms_po_item.ref_id limit 1),'')) END
                    ) cust_emailaddr
                FROM pms_po_item
                    LEFT JOIN pms_po_list ON pms_po_list.po_id = pms_po_item.po_id
                    LEFT JOIN coy_address_book c ON c.coy_id=pms_po_list.coy_id and c.ref_type='LOCATION' and c.ref_id=pms_po_list.loc_id and c.addr_type=pms_po_list.loc_addr
                WHERE pms_po_item.po_id = rtrim('$po_id')
                    AND pms_po_item.rev_num = pms_po_list.current_rev
                    AND pms_po_list.rev_num = pms_po_list.current_rev 
                    AND pms_po_item.coy_id ='CTL'
                ORDER BY line_num ASC";
        //echo "<pre>" . $sql;exit;
        return $query = $this->db->query($sql)->result_array();
    }

    private function _doCurl($query, $po_id) {
        $url = $this->apiurl;
        $headers[] = "Accept-Encoding:gzip,deflate";
        $headers[] = "Content-Type:application/xml";
        $headers[] = "Connection:Keep-Alive";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        curl_close($ch);

        if (strpos($output,"SUCCESS")>0) {
            $rtn = TRUE;
            $rtn_code = 1;
            $rtn_msg = "OK";
        }
        else {
            $rtn = FALSE;
            $rtn_code = 0;
            $rtn_msg = "XOK";
        }

        // Save to DB
        $sql_ip = getRealIpAddr(15);
        $sql = "INSERT INTO b2b_po_message (coy_id,trans_type,trans_id,url_link,msg_request,status_code,status_text,msg_response,created_by,created_on,modified_by,modified_on)
                        VALUES ('CTL','PO','".$po_id."','$url','$query','$rtn_code','$rtn_msg','$output','$sql_ip',now(),'$sql_ip',now() ); ";
        $query = $this->db->query($sql);

        return array(
            "code" => $rtn_code,
            "msg" => $rtn_msg,
            "response" => $output
        );
    }

    private function _doParams($params){

        $param = $params[0];
        $param['cust_name'] = ($param['cust_name']=='') ? 'Challenger' : $param['cust_name'] ;
        $param['tel_code'] = ($param['tel_code']=='') ? '63189800' : $param['tel_code'] ;
        $param['cust_emailaddr'] = ($param['cust_emailaddr']=='') ? 'mis@challenger.sg' : $param['cust_emailaddr'] ;

        $txn_id = trim($param['po_id']);
        $txn_date = date('Y-m-d H:i:s',strtotime($param['po_date']));

        if ( $param['txn_item_type'] == '1-MS-ESD' ) {
            $usr_name = 'SG_MSESD_101083';
            $usr_pwd = (ENVIRONMENT=="production") ? 'CHALLENGER12' : '';
            $shiptodetails_id = '244895';

            $processing_037 = '<TextValue TextType="Z037">MSESD</TextValue>';

            $param['cust_name'] = 'LOO LEONG THYE';
            $param['tel_code'] = '63189800' ;
            $param['cust_emailaddr'] = (ENVIRONMENT=="production") ? 'mrd.hc@challenger.sg' : 'yongsheng@challenger.sg' ;

            $shipto['custname1'] = 'LOO LEONG THYE';
            $shipto['custname2'] = '';
            $shipto['addr1'] = '1 Ubi Link';
            $shipto['addr2'] = '';
            $shipto['postal'] = '408553';
            $shipto['country'] = 'Singapore';
            $shipto['city'] = 'SG';
        }
        else if ( $param['txn_item_type'] == '2-OTHER-ESD' ){
            $usr_name = 'SG_101083'; // Non-MSESD orders – user name can be SG_101083 OR SG_CHALLENGER
            $usr_pwd = 'Challenger123';
            $shiptodetails_id = '244895';

            $processing_037 = '<TextValue TextType="Z037">XML</TextValue>';

            $shipto['custname1'] = $param['cust_name'];
            $shipto['custname2'] = '';
            $shipto['addr1'] = '1 Ubi Link';
            $shipto['addr2'] = '';
            $shipto['postal'] = '408553';
            $shipto['country'] = 'Singapore';
            $shipto['city'] = 'SG';
        }
        else {
            $usr_name = 'SG_101083'; // Non-MSESD orders – user name can be SG_101083 OR SG_CHALLENGER
            $usr_pwd = 'Challenger123';
            $shiptodetails_id = '101083';

            $processing_037 = '<TextValue TextType="Z037">XML</TextValue>';

            $param['cust_name'] = 'LOO LEONG THYE';
            $param['tel_code'] = '63189800' ;
            $param['cust_emailaddr'] = (ENVIRONMENT=="production") ? 'mrd.hc@challenger.sg' : 'yongsheng@challenger.sg' ;

            $shipto['custname1'] = 'CHALLENGER TECHNOLOGIES PTE LTD';
            $shipto['custname2'] = $param['loc_id'];
            $shipto['addr1'] = $param['street_line1'];
            $shipto['addr2'] = $param['street_line2'];
            $shipto['postal'] = $param['postal_code'];
            $shipto['country'] = 'Singapore';
            $shipto['city'] = 'SG';
        }

        // Replace sensitive words (JEM)
        $shipto['custname1'] = str_replace('JEM','J.E.M',$shipto['custname1']);
        $shipto['custname2'] = str_replace('JEM','J.E.M',$shipto['custname2']);
        $shipto['addr1'] = str_replace('JEM','J.E.M',$shipto['addr1']);
        $shipto['addr2'] = str_replace('JEM','J.E.M',$shipto['addr2']);

        $xml = '<?xml version="1.0" encoding="iso-8859-1"?>';
        $xml .= '<BusinessTransactionRequest xmlns="http://www.ingrammicro.com/pcg/in/OrderCreateRequest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
        $xml .= '<RequestPreamble><SenderID>Reseller</SenderID><ReceiverID>Ingram-Micro</ReceiverID><TransactionClassifier>2.0</TransactionClassifier>';
        $xml .= '<TransactionID>' . $txn_id . '-'. count($params) .'2</TransactionID>';
        $xml .= '<UserName>' . $usr_name . '</UserName><UserPassword>' . $usr_pwd . '</UserPassword>';
        $xml .= '<CountryCode>SG</CountryCode><TransactionMode>ASYNC</TransactionMode></RequestPreamble>';

        $xml .= '<OrderCreateRequest><CustomerPurchaseOrderDetails>';
        $xml .= '<PurchaseOrderNumber>' . $txn_id . '</PurchaseOrderNumber>';
        $xml .= '<PurchaseOrderDate>' . $txn_date . '</PurchaseOrderDate>';
        $xml .= '</CustomerPurchaseOrderDetails>';

        $xml .= '<ShipToDetails>';
        $xml .= '<ID>' . $shiptodetails_id . '</ID>';
        $xml .= '<Address><Name1>' . $shipto['custname1'] . '</Name1><Name2>'. $shipto['custname2'] .'</Name2><CareOf>Care of</CareOf><HouseNumber>00</HouseNumber>';
        $xml .= '<Address1>'.$shipto['addr1'].'</Address1><Address2>'.$shipto['addr2'].'</Address2><City>'.$shipto['country'].'</City><PostalCode>'.$shipto['postal'].'</PostalCode><CountryCode>'.$shipto['city'].'</CountryCode>';
        $xml .= '<PhoneNumber>' . $param['tel_code'] . '</PhoneNumber><Email>' . $param['cust_emailaddr'] . '</Email></Address>';
        $xml .= '</ShipToDetails>';

        $xml .= '<ShippingDetails><CarrierCode>460029</CarrierCode>';
        $xml .= '<RequestedDeliveryDate>' . $txn_date . '</RequestedDeliveryDate>';
        $xml .= '</ShippingDetails>';

        $xml .= '<ProcessingFlags><BackOrderFlag>Y</BackOrderFlag><ShipCompleteFlag>Y</ShipCompleteFlag></ProcessingFlags>';
        $xml .= '<HeaderText>';
        $xml .= '<TextValue TextType="Z001">WILL CALL ' . $param['txn_id'] . '</TextValue>';
        $xml .= '<TextValue TextType="Z012">WILL CALL ' . $param['txn_id'] . '</TextValue>';
        $xml .= $processing_037;
        $xml .= '</HeaderText>';

        foreach ($params as $par) {
            $xml .= '<LineDetails>';
            $xml .= '<CustomerLineNumber>' . $par['line_num'] . '</CustomerLineNumber>';
            $xml .= '<IngramPartNumber></IngramPartNumber>';
            $xml .= '<VendorPartNumber>'. $par['long_desc'] .'</VendorPartNumber>';
            $xml .= '<CustomerPartNumber>'. $par['item_id'] .'</CustomerPartNumber>';
            $xml .= '<EANOrUPCCode></EANOrUPCCode>';
            $xml .= '<RequestedQuantity UnitOfMeasure="EA">'. intval($par['qty_order']) .'</RequestedQuantity>';
            $xml .= '<RequestedPrice>'. sprintf('%01.2f',$par['unit_price']) .'</RequestedPrice>';

            $xml .= '<EndUserDetails>';
            $xml .= '<EndUserPurchaseOrderDetails>';
            $xml .= '<PurchaseOrderNumber>' . $param['txn_id'] . '</PurchaseOrderNumber>';
            $xml .= '<PurchaseOrderDate>' . $txn_date . '</PurchaseOrderDate>';
            $xml .= '</EndUserPurchaseOrderDetails>';
            $xml .= '<CompanyName>CHALLENGER TECHNOLOGIES PTE LTD</CompanyName>';
            $xml .= '<Address><Name1>' . $shipto['custname1'] . '</Name1><Name2>'. $shipto['custname2'] .'</Name2><CareOf>Care of</CareOf><HouseNumber>00</HouseNumber>';
            $xml .= '<Address1>'.$shipto['addr1'].'</Address1><Address2>'.$shipto['addr2'].'</Address2><City>'.$shipto['country'].'</City><PostalCode>'.$shipto['postal'].'</PostalCode><CountryCode>'.$shipto['city'].'</CountryCode>';
            $xml .= '<PhoneNumber></PhoneNumber><Email>' . $param['cust_emailaddr'] . '</Email></Address>';
            $xml .= '<EndUserContactDetails>';
            $xml .= '<Name>' . $param['cust_name'] . '</Name>';
            $xml .= '<PhoneNumber>' . $param['tel_code'] . '</PhoneNumber>';
            $xml .= '<Email>' . $param['cust_emailaddr'] . '</Email>';
            $xml .= '</EndUserContactDetails>';
            $xml .= '</EndUserDetails>';

            $xml .= '<LineText>';
            $xml .= '<TextValue TextType="Z021">EUCONTACT:' . $param['cust_name'] . '|EUPHONE:' . $param['tel_code'] . '|EUEMAIL:' . $param['cust_emailaddr'] . '</TextValue>';
            $xml .= '</LineText>';
            $xml .= '</LineDetails>';
        }

        $xml .= '</OrderCreateRequest></BusinessTransactionRequest>';

        return $xml;
    }

    public function cherps_email($subject,$message,$email='yongsheng@challenger.sg') {

        $message_nl = str_replace(['\n','<br>'],'
',$message);

        $params['from'] = 'chvoices@challenger.sg';
        $params['to'] = $email;
        $params['cc'] = '';
        $params['subject'] = $subject;
        $params['message'] = $message_nl;
        $params['body_format'] = 'HTML';
        $rtn = $this->_SendEmail($params);
    }

    /***
     *  INGRAM INVENTORY SYNC
     */

    public function inventory_sync(){

        if ($this->input->get('debug')==1) {
            ob_start();
        }

        $inv_dim12 = 'IMC' . date('Ymd');

        // Get the items from DB
        if ($this->input->get('item_id')) {
            $sql_item = explode(',', $this->input->get('item_id') );
            $sql_where = " and i.item_id in ('". implode("','",$sql_item) ."') ";
        }
        else {
            $sql_where = " and i.status_level>0 and i.inv_dim12<>'$inv_dim12' ";
        }
        $sql = "SELECT RTRIM(i.item_id) item_id,p.modified_on,p.updated_on
                FROM ims_item_list i 
                LEFT JOIN ims_inv_physical p ON i.coy_id=p.coy_id and i.item_id=p.item_id and p.loc_id=i.inv_type
                WHERE i.coy_id = 'CTL' and i.inv_type = '". $this->loc_id ."' $sql_where
                ORDER BY coalesce(p.updated_on, current_date), i.item_id LIMIT 50";
        $item_array = $this->db->query($sql)->result_array();
        $item_ids = array();
        foreach ($item_array as $item_array_pcs) {
            $item_ids[] = $item_array_pcs['item_id'];
        }

        if ($this->input->get('debug')==1) {
            echo "<pre>" . print_r($item_array,true) ."</pre><hr>";
            echo "<pre>" . print_r($item_ids,true) ."</pre><hr>";
            ob_flush();
        }
        if (count($item_ids)==0) {
            return array('code'=>0,'msg'=>'No items found');
        }

        // CURL Query
        $transaction_id = 'BATCH_' . date('Ymd') . "001";
        $username = INGRAM_USER; //"SG_101083";
        $password = INGRAM_PASS; //"Challenger123";
        $url = INGRAM_URL1; //"https://mercury.ingrammicro.com:8447/SeeburgerHTTP/HTTPController?HTTPInterface=syncReply";
        $this->load->helper('date');
        $now = date('Y-m-d\TH:i:s');
        $TotalQty = 3;
        $item_xml = "";
        foreach ($item_array as $item) {
            $item_xml .= '<Item><CustomerPartNumber>'.$item['item_id'].'</CustomerPartNumber><RequestedQuantity UnitOfMeasure = "EA">1</RequestedQuantity></Item>';
        };

        $action = 'POST';
        $xml = <<<EOD
<?xml version = "1.0" encoding = "UTF-8"?>
<BusinessTransactionRequest xmlns = "http://www.ingrammicro.com/pcg/in/PriceAndAvailibilityRequest">
<RequestPreamble>
<SenderID>Challenger</SenderID>
<ReceiverID>XYZ</ReceiverID>
<TransactionClassifier>2.0</TransactionClassifier>
<TransactionID>$transaction_id</TransactionID>
<TimeStamp>$now</TimeStamp>
<UserName>$username</UserName>
<UserPassword>$password</UserPassword>
<CountryCode>SG</CountryCode>
</RequestPreamble>
<PriceAndAvailabilityRequest>
<PriceAndAvailabilityPreference>$TotalQty</PriceAndAvailabilityPreference>
$item_xml
</PriceAndAvailabilityRequest>
</BusinessTransactionRequest>
EOD;

        // SAMPLE OUTPUT (WILL BE OVERWRITTEN BY CURL CALL)
        $output = '<?xml version="1.0" encoding="UTF-8"?>
<BusinessTransactionResponse xmlns="http://www.ingrammicro.com/pcg/out/PriceAndAvailabilityResponse">
   <ResponsePreamble>
      <SenderID>XYZ</SenderID>
      <ReceiverID>Challenger</ReceiverID>
      <TransactionClassifier>2.0</TransactionClassifier>
      <TransactionID>BATCH_20190329001</TransactionID>
      <TimeStamp>2019-03-29T00:44:09</TimeStamp>
      <ReturnCode>10001</ReturnCode>
      <ReturnMessage>Transaction Partially Successful</ReturnMessage>
   </ResponsePreamble>
   <PriceAndAvailabilityResponse>
      <ItemDetails>
         <Status StatusCode="10001" StatusDescription="Item details were retrieved successfully"/>
         <IngramPartNumber IngramPartDescription="GL504GV-ES107T">4412767</IngramPartNumber>
         <VendorPartNumber>90NR01X1-M01960</VendorPartNumber>
         <CustomerPartNumber>0192876243107</CustomerPartNumber>
         <EANOrUPCCode>192876243107</EANOrUPCCode>
         <VendorName VendorNumber="100088">ASUS GLOBAL PTE LTD</VendorName>
         <RequestedQuantity UnitOfMeasure="EA">1</RequestedQuantity>
         <SKUAttributes>
            <IsAvailable>Y</IsAvailable>
            <IsStockable>Y</IsStockable>
            <IsDiscontinued>N</IsDiscontinued>
            <IsSubstitutedDetails>
               <IsSubstituted>N</IsSubstituted>
            </IsSubstitutedDetails>
            <IsSoldSeparately>Y</IsSoldSeparately>
            <IsBillOfMaterial>N</IsBillOfMaterial>
            <HasPromotions>N</HasPromotions>
            <HasQuantityBreaks>N</HasQuantityBreaks>
            <EndUserRequired>N</EndUserRequired>
         </SKUAttributes>
         <PricingDetails>
            <CurrencyCode>SGD</CurrencyCode>
            <UnitNetPrice>2381,00</UnitNetPrice>
            <SalesExTax>2381,00</SalesExTax>
         </PricingDetails>
         <AvailabilityDetails>
            <Plant>
               <PlantID PlantDescription="IM Singapore">SG01</PlantID>
               <AvailableQuantity UnitOfMeasure="EA">22</AvailableQuantity>
               <StorageLocation>
                  <StorageLocationID StorageLocationDescription="Standard">1000</StorageLocationID>
                  <AvailableQuantity UnitOfMeasure="EA">22</AvailableQuantity>
               </StorageLocation>
            </Plant>
         </AvailabilityDetails>
      </ItemDetails>
      <ItemDetails>
         <Status StatusCode="20004" StatusDescription="ERROR: Data issues were encountered while processing your request, details are:   Entered Customer Part Number not Found in Database for Item No: 000008"/>
      </ItemDetails>
   </PriceAndAvailabilityResponse>
</BusinessTransactionResponse>';

        if ($this->input->get('debug')==1) {
            echo "<pre>" . print_r($url,true) ."</pre><br>";
            echo "<pre>" . print_r($xml,true) ."</pre><hr>";
            ob_flush();
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml'));
        curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        $output = curl_exec($ch);
        if (curl_error($ch)) {
            $error_msg = curl_error($ch);
            echo "<pre>" . print_r($error_msg,true) ."</pre><hr>";
            return array('code'=>0,'msg'=>'Fail to call Ingram API','error'=>$error_msg);
        }
        curl_close($ch);


        if ($this->input->get('debug')==1) {
            echo "<pre>" . print_r($output,true) ."</pre><hr>";
            ob_flush();
        }

        // Extract QOH from response
        $item_inventory_synced = array();
        $xml_array = simplexml_load_string($output);
        $xml_array = json_decode(json_encode($xml_array), true);

        if ($this->input->get('debug')==1) {
            echo "<pre>" . print_r($xml_array,true) ."</pre><hr>";
            ob_flush();
        }

        $rows_replied = 0;
        $items0 = (isset($xml_array["PriceAndAvailabilityResponse"]["ItemDetails"])) ? $xml_array["PriceAndAvailabilityResponse"]["ItemDetails"] : [];
        if (array_key_exists('AvailabilityDetails', $items0)) {
            $items[] = $items0;
        } else {
            $items = $items0;
        }
        for ($i = 0; $i < count($items); $i++) {
            $rows_replied++;
            if (gettype($items[$i]) == 'array' && array_key_exists("AvailabilityDetails", $items[$i])) {
                $plant = $items[$i]["AvailabilityDetails"]["Plant"];
                if (isset($plant[0]["PlantID"])) {
                    if ($plant[0]["PlantID"] == "SG01") {
                        $item_inventory_synced[] = array('item_id'=>$items[$i]["CustomerPartNumber"], 'qty_on_hand'=>$plant[0]["StorageLocation"]["AvailableQuantity"]);
                    }
                }
                if (isset($plant["PlantID"])) {
                    if ($plant["PlantID"] == "SG01") {
                        $item_inventory_synced[] = array('item_id'=>$items[$i]["CustomerPartNumber"], 'qty_on_hand'=>$plant["StorageLocation"]["AvailableQuantity"]);
                    }
                }
            }
        }

        if ($this->input->get('debug')==1) {
            echo "<pre>" . print_r($item_inventory_synced,true) ."</pre><hr>";
            ob_flush();
        }

        // Update/Insert to ims_inv_physical
        $coy_id = 'CTL';
        //$loc_id = 'IM-C;
        $sync_ids = array();
        foreach ($item_inventory_synced as $sync_item) {
            $sync_ids[] = $sync_item['item_id'];
            $item_id = $sync_item['item_id'];
            $qty_on_hand = $sync_item['qty_on_hand'];
            $sql = "
                    INSERT INTO ims_inv_physical (coy_id,item_id,loc_id,line_num,trans_date,qty_on_hand,qty_reserved,created_by,supp_csg)
                    (SELECT coy_id,item_id,inv_type,1,now(),0,0,'cherps2pg','Y' FROM ims_item_list
                    WHERE coy_id = '$coy_id' and inv_type = '". $this->loc_id ."' and item_id = '$item_id') 
                ON conflict (coy_id, item_id, line_num, loc_id) 
                DO 
                    UPDATE SET qty_on_hand = $qty_on_hand,trans_date=now(),modified_on=now(),updated_on=now(),modified_by='cherps2pg'";
            $result = $this->db->query($sql);

            if ($this->input->get('debug')==1) {
                echo "<pre>" . print_r($sql,true) ."</pre><hr>";
                ob_flush();
            }
        }

        // Update records so it will not loop today
        $sql1 = "UPDATE ims_item_list SET inv_dim12='' WHERE inv_type='". $this->loc_id ."' and inv_dim12 LIKE 'IMC%' and item_id in ('". implode("','",$item_ids) ."'); ";
        $result = $this->db->query($sql1);

        $sql_items = array_diff($item_ids,$sync_ids);
        if (count($sql_items)>0) {
            $sql1 = "UPDATE ims_item_list SET inv_dim12='$inv_dim12'
                    WHERE inv_type='". $this->loc_id ."' and item_id in ('". implode("','",$sql_items) ."');  ";
            $result = $this->db->query($sql1);

            // $sql2 = "UPDATE ims_inv_physical SET updated_on=now() WHERE loc_id='" . $this->loc_id . "' and item_id in ('" . implode("','", $sql_items) . "'); ";
            // $result = $this->db->query($sql2);
        }

        if ($this->input->get('debug')==1) {
            echo "<pre>" . print_r($sql,true) ."</pre><pre>" . print_r($sql1,true) ."</pre><pre>" . print_r($sql2,true) ."</pre><hr>";

            echo "<pre>Total Updated Records: " . count($item_inventory_synced) ."</pre>";
            echo "<pre>" . print_r($item_ids,true) ."</pre><hr>";
            ob_flush();
            ob_end_flush();
        }

        return array(
            'code'  => 1,
            'msg'   => count($item_ids) . ' Items found | ' .
                $rows_replied . ' Read from response | ' .
                count($item_inventory_synced).' Inventory updated'
        );

    }

    /**
     * @api {post} /api/challenger_po/confirmation/:supply_name?code=Jpc3MiOiJDU01lcmNoYW50IiwiZXhwIjoyNTA5NzgzNjM3LCJ1c2VySWQiOjE PO Confirm
     * @apiVersion 1.0.0
     * @apiGroup ER0001
     *
     * @apiSuccessExample PO Confirmed Success
     * HTTP/1.1 200 OK
     *  {
     *      "return": "1",
     *      "message": "PO Confirmed",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiSuccessExample Revise PO Confirmed Success
     * HTTP/1.1 200 OK
     *  {
     *      "return": "1",
     *      "message": "Revise PO Confirmed",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample Request code is error
     *  {
     *      "return": "0",
     *      "message": "Request code is error, please check your request code.",
     *      "po_id": "IPVC190066393"
     *  }
     *
     *
     * @apiErrorExample Require requested quantity
     *  {
     *      "return": "0",
     *      "message": "Whoops, no requested quantity found.",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample Require customer line number
     *  {
     *      "return": "0",
     *      "message": "Whoops, no customer line number found.",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample Require confirmed quantity
     *  {
     *      "return": "0",
     *      "message": "Whoops, no confirmed quantity found.",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample Require delivery date
     *  {
     *      "return": "0",
     *      "message": "Whoops, no delivery date found.",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample Require customer part number
     *  {
     *      "return": "0",
     *      "message": "Whoops, no customer part number found.",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample item_id or item_quantity not match
     *  {
     *      "return": "0",
     *      "message": "Whoops, we can not find item_id of 0193905227426 or item quantity is not match our record.",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample No valid PO
     *  {
     *      "return": "0",
     *      "message": "No valid PO",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample Invalid date
     *  {
     *      "return": "0",
     *      "message": "Invalid date 2019-08-08",
     *      "po_id": "IPVC190066393"
     *  }
     *
     * @apiErrorExample New Qty cannot be higher than Qty Order
     *  {
     *      "return": "0",
     *      "message": "New Qty cannot be higher than Qty Order",
     *      "po_id": "IPVC190066393"
     *  }
     *
     */
    public function read_xml($supplier) {
        $code = $this->input->get('code', TRUE);
        $return = [];
        if ($code !== $this->token) {
            $return = array('return'=>'0','message'=>'Request code is error, please check your request code.');
        } else {
            $supp = is_empty($supplier) ? $supplier : 'ER0001';
            $data = file_get_contents('php://input');
            $xml = simplexml_load_string($data);
            $po_id = (string)$xml->OrderStatusResponse->StatusHeader->CustomerPurchaseOrderDetails->PurchaseOrderNumber;
            $po_items = CI::db()->query("SELECT coy_id, rev_num, line_num, rtrim(item_id) as item_id, qty_order FROM pms_po_item WHERE po_id = '$po_id' AND rev_num = (SELECT current_rev FROM pms_po_list WHERE po_id='$po_id') ORDER BY line_num limit 1")->result_array();
            $po_count = CI::db()->query("SELECT count(*) as po_count FROM pms_po_item WHERE po_id = '$po_id' AND rev_num = (SELECT current_rev FROM pms_po_list WHERE po_id='$po_id') limit 1")->row_array();
            $po_count = $po_count['po_count'];
            $coy_id = $po_items[0]['coy_id'];
            $rev_num = $po_items[0]['rev_num'];

            $items = $xml->OrderStatusResponse->StatusDetails->OrderDetails->LineDetails;
            $changed = 0;
            $count = 0;
            $item_qty = array();

            foreach ($items as $item) {
                if (isset($item->OrderQuantityDetails->RequestedQuantity)){
                    $req_quantity = (int)$item->OrderQuantityDetails->RequestedQuantity;
                }
                else {
                    $return = array('return'=>'0','message'=>'Whoops, no requested quantity found.', 'po_id'=>$po_id);
                }

                if (isset($item->CustomerLineNumber)){
                    $line_num = (int)$item->CustomerLineNumber;
                }
                else {
                    $return = array('return'=>'0','message'=>'Whoops, no customer line number found.', 'po_id'=>$po_id);
                }

                if (isset($item->EstimatedDeliveryDetails->EstimatedDeliveryLine->ConfirmedQuantity)){
                    $conf_quantity = (int)$item->EstimatedDeliveryDetails->EstimatedDeliveryLine->ConfirmedQuantity;
                }
                else {
                    $return = array('return'=>'0','message'=>'Whoops, no confirmed quantity found.', 'po_id'=>$po_id);
                }

                if (isset($item->EstimatedDeliveryDetails->EstimatedDeliveryLine->DeliveryDate)){
                    $delv_date = (string)$item->EstimatedDeliveryDetails->EstimatedDeliveryLine->DeliveryDate;
                }
                else {
                    $return = array('return'=>'0','message'=>'Whoops, no delivery date found.', 'po_id'=>$po_id);
                    return array([
                        'return'=>0,
                        'message'=>'Whoops, no delivery date found.',
                        'po_id'=>$po_id
                    ]);
                }

                if (isset($item->CustomerPartNumber)){
                    $item_id = (string)$item->CustomerPartNumber;
                }
                else {
                    $return = array('return'=>'0','message'=>'Whoops, no customer part number found.', 'po_id'=>$po_id);
                    return array([
                        'return'=>0,
                        'message'=>'Whoops, no customer part number found.',
                        'po_id'=>$po_id
                    ]);
                }

                if ($line_num == $po_items[$count]['line_num'] && $item_id == $po_items[$count]['item_id']){
                    if ($req_quantity != $conf_quantity) {
                        $changed++;
                    }
                    if ($req_quantity < $conf_quantity) {
                        $changed = -10000;
                    }
                    $item_qty[$line_num] = $conf_quantity;
                    $item_id[$line_num] = (string)$item->CustomerPartNumber;
                } else {
                    return array([
                        'return'=>0,
                        'message'=>'Whoops, we can not find item id of ' .$item_id. ' or item quantity is not match our record.',
                        'po_id'=>$po_id
                    ]);
                }
                $count++;
            }
            if (strtotime($delv_date) > time() - 86400) {
                $params["delv_date"] = date("Y-m-d H:i:s", strtotime($delv_date));
                $params["po_id"] = $po_id;
                $this->update_po_list($params);
            }
            if (count($items)==0 || $changed < 0 || strtotime($delv_date) < time() - 86400) {
                if (count($items)==0) {
                    $return = array('return'=>'0','message'=>'No valid PO', 'po_id'=>$po_id);
                } elseif (strtotime($delv_date) < time() - 86400) {
                    $return = array('return'=>'0','message'=>'Invalid date ' . $delv_date, 'po_id'=>$po_id);
                } else {
                    $return = array('return'=>'0','message'=>'New Qty cannot be higher than Qty Order', 'po_id'=>$po_id);
                }
            }
            elseif ($changed==0) {
                $params_list = array(
                    'supp_id' => $supp,
                    'status_level'=> '2',
                    'coy_id'=> $coy_id,
                    'po_id'=> $po_id,
                    'rev_num'=> $rev_num
                );
                $this->update_po_list($params_list);
                $params_item = array(
                    'supp_id' => $supp,
                    'status_level'=> '1',
                    'coy_id'=> $coy_id,
                    'po_id'=> $po_id,
                    'rev_num'=> $rev_num
                );
                $this->update_po_item($params_item);
                $return = array('return'=>'1','message'=>'PO Confirmed', 'po_id'=>$po_id);
            }
            else {
                // Changes in Qty. Revise this PO
                $params = array(
                    'supp_id' => $supp,
                    'coy_id'=> $coy_id,
                    'po_id'=> $po_id,
                    'rev_num'=> $rev_num
                );
                $this->revise_po($params, $item_qty);

                $return = array('return'=>'1','message'=>'Revise PO Confirmed', 'po_id'=>$po_id);
            }
        }

        // :: After confirmed actions ::
        if ($return['return']=='1') {
            if (substr($po_id,0,2)=="HP" && $return['message']=='Revise PO Confirmed') {
                // If revised & po_type=HP, trigger email
                $email_param["from"] = "chvoices@challenger.sg";
                $email_param["to"] = "li.liangze@challenger.sg; ";
                $email_param["subject"] = "PO revised by supplier - " . $po_id;
                $email_param["message"] = "PO ".$po_id." revised by " . $supp;
                $this->Po2_model->CallSP_email($email_param);
            }
        }

        echo json_encode($return);
    }

    public function update_po_list($params) {
        $supp_id = $params['supp_id'];
        $sql_set = (isset($params["delv_date"])) ? "delv_date='".$params["delv_date"]."', " : '' ;
        $sql_set.= (isset($params["status_level"])) ? "status_level='".$params["status_level"]."', " : '' ;
        $sql_whr = (isset($params["coy_id"])) ? " AND coy_id='".$params["coy_id"]."' " : '' ;
        $sql_whr.= (isset($params["rev_num"])) ? " AND rev_num='".$params["rev_num"]."' " : '' ;
        $sql = "UPDATE pms_po_list SET " . $sql_set
            . "modified_by='" . $supp_id . "',"
            . "modified_on='" . date("Y-m-d H:i:s") . "'"
            . " WHERE po_id IN ('" . $params["po_id"] . "') " . $sql_whr;
        $result = CI::db()->query($sql);
        return $result;
    }

    public function update_po_item($params) {
        $supp_id = $params['supp_id'];
        $sql_set = (isset($params["qty_order"])) ? "delv_date='".$params["qty_order"]."', " : '' ;
        $sql_set.= (isset($params["status_level"])) ? "status_level='".$params["status_level"]."', " : '' ;
        $sql_whr = (isset($params["coy_id"])) ? " AND coy_id='".$params["coy_id"]."' " : '' ;
        $sql_whr.= (isset($params["rev_num"])) ? " AND rev_num='".$params["rev_num"]."' " : '' ;
        $sql_whr.= (isset($params["line_num"])) ? " AND line_num='".$params["line_num"]."' " : '' ;
        $sql = "UPDATE pms_po_item SET " . $sql_set
            . "modified_by='" . $supp_id . "',"
            . "modified_on='" . date("Y-m-d H:i:s") . "'"
            . " WHERE po_id IN ('" . $params["po_id"] . "') " . $sql_whr;
        $result = CI::db()->query($sql);
        return $result;
    }

    public function revise_po($params, $po_qty) {

        //  Prepares SQL query here and execute.
        //    If no items in po_qty, will insert header & set as cancelled (-1)
        //    Else will copy header & valid items, set as confirmed (2)

        $sql_linenum = "";
        foreach ($po_qty as $line=>$qty)
            $sql_linenum[] = $line;

        $supp_id = $params['supp_id'];
        $temp_table = '#temp_po_' . rand(1000,9999);
        $new_status = (count($po_qty)==0) ? "-1" : "2" ;

        $sql = "
            UPDATE pms_po_list SET current_rev='".($params["rev_num"]+1)."',status_level='-2',modified_by='".$supp_id."',modified_on=now() 
                WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."'
            SELECT * INTO ".$temp_table." FROM pms_po_list 
                WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num='".$params["rev_num"]."'
            UPDATE ".$temp_table." SET rev_num='".($params["rev_num"]+1)."',status_level='$new_status',modified_by='".$supp_id."',modified_on=now()
            INSERT INTO pms_po_list
                SELECT * FROM ".$temp_table."
            DROP TABLE ".$temp_table."
            ";
        if (count($po_qty)>0) {
            $sql.= "
                UPDATE pms_po_item SET status_level='-2',modified_by='".$supp_id."',modified_on=now() 
                    WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num='".$params["rev_num"]."'
                SELECT * INTO ".$temp_table."I FROM pms_po_item 
                    WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num='".$params["rev_num"]."'
                        AND line_num IN (". implode(',',$sql_linenum) .")
                UPDATE ".$temp_table."I SET rev_num='".($params["rev_num"]+1)."',status_level='1',modified_by='".$supp_id."',modified_on=now()
                INSERT INTO pms_po_item
                    SELECT * FROM ".$temp_table."I
                DROP TABLE ".$temp_table."I
                ";
            foreach ($po_qty as $line=>$qty) {
                $sql.= "
                    UPDATE pms_po_item SET qty_order=$qty
                        WHERE coy_id='".$params["coy_id"]."' AND po_id='".$params["po_id"]."' AND rev_num='".($params["rev_num"]+1)."' AND line_num=$line
                    ";
            }
        }
        $result = CI::db()->query($sql);
        return $result;
    }

}
?>
