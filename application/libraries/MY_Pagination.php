<?php
/**
 * Challenger
 *
 * @package    Challenger
 * @author     GUO ZHIMING
 * @filesource MY_Pagination.php
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Pagination extends CI_Pagination {

    public function __construct($params = array())
    {

        $params['reuse_query_string'] = true;
        $params['enable_query_strings'] = true;
        $params['use_page_numbers'] = true;
        $params['page_query_string'] = true;
        $params['query_string_segment'] = 'page';
        $params['base_url'] = current_url();
        $params['per_page'] = 20;
        $params['full_tag_open'] = '<ul class="pagination">';
        $params['full_tag_close'] = '</ul>';
        $params['first_link'] = '&laquo; First';
        $params['first_tag_open'] = '<li class="prev page">';
        $params['first_tag_close'] = '</li>';
        $params['last_link'] = 'Last &raquo;';
        $params['last_tag_open'] = '<li class="next page">';
        $params['last_tag_close'] = '</li>';
        $params['next_link'] = '>';
        $params['next_tag_open'] = '<li class="next page">';
        $params['next_tag_close'] = '</li>';
        $params['prev_link'] = '<';
        $params['prev_tag_open'] = '<li class="prev page">';
        $params['prev_tag_close'] = '</li>';
        $params['cur_tag_open'] = '<li class="active"><a href="">';
        $params['cur_tag_close'] = '</a></li>';
        $params['num_tag_open'] = '<li class="page">';
        $params['num_tag_close'] = '</li>';
        $params['anchor_class'] = 'follow_link';
        
        parent::__construct($params);    
    }
    
}
