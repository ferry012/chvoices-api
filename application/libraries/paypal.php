<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Paypal {
	public function __construct() {
		// require_once APPPATH . 'config/paypal.php';
		require_once APPPATH . '../vendor/autoload.php';
	}
	function createPayPalClient() {
		$ci = & get_instance ();

		$PayPalConfig = array (
				'Sandbox' => $ci->config->item ( 'sandbox' ),
				'APIUsername' => $ci->config->item ( 'api_username' ),
				'APIPassword' => $ci->config->item ( 'api_password' ),
				'APISignature' => $ci->config->item ( 'api_signature' ),
				'PrintHeaders' => $ci->config->item ( 'print_headers' ),
				'LogResults' => $ci->config->item ( 'log_results' ),
				'LogPath' => $ci->config->item ( 'log_path' )
		);

		$PayPal = new angelleye\PayPal\PayPal ( $PayPalConfig );

		return $PayPal;
	}

	// function directPayment() {
	// 	$PayPal = $this->createPayPalClient ();
	//
	// 	// ipaddress - Required. IP address of the payer's browser.
	// 	// returnfmfdetails - Flag to determine whether you want the results returned by FMF. 1 or 0. Default is 0.
	// 	$DPFields = array (
	// 			'paymentaction' => 'Sale',
	// 			'ipaddress' => $_SERVER ['REMOTE_ADDR'],
	// 			'returnfmfdetails' => '1'
	// 	);
	//
	// 	// Required. Type of credit card. Visa, MasterCard, Discover, Amex, Maestro, Solo. If Maestro or Solo, the currency code must be GBP. In addition, either start date or issue number must be specified.
	// 	// Required. Credit card number. No spaces or punctuation.
	// 	// Required. Credit card expiration date. Format is MMYYYY
	// 	// Requirements determined by your PayPal account settings. Security digits for credit card.
	// 	// Month and year that Maestro or Solo card was issued. MMYYYY
	// 	// Issue number of Maestro or Solo card. Two numeric digits max.
	// 	$CCDetails = array (
	// 			'creditcardtype' => 'MasterCard',
	// 			'acct' => '5522340006063638',
	// 			'expdate' => '022016',
	// 			'cvv2' => '456',
	// 			'startdate' => '',
	// 			'issuenumber' => ''
	// 	);
	//
	// 	// Email address of payer.
	// 	// Unique PayPal customer ID for payer.
	// 	// Status of payer. Values are verified or unverified
	// 	// Payer's business name.
	// 	$PayerInfo = array (
	// 			'email' => 'sandbox@angelleye.com',
	// 			'payerid' => '',
	// 			'payerstatus' => '',
	// 			'business' => ''
	// 	);
	//
	// 	// Payer's salutation. 20 char max.
	// 	// Payer's first name. 25 char max.
	// 	// Payer's middle name. 25 char max.
	// 	// Payer's last name. 25 char max.
	// 	// Payer's suffix. 12 char max.
	// 	$PayerName = array (
	// 			'salutation' => '',
	// 			'firstname' => 'Tester',
	// 			'middlename' => '',
	// 			'lastname' => 'Testerson',
	// 			'suffix' => ''
	// 	);
	//
	// 	// Required. First street address.
	// 	// Second street address.
	// 	// Required. Name of City.
	// 	// Required. Name of State or Province.
	// 	// Required. Country code.
	// 	// Required. Postal code of payer.
	// 	// Phone Number of payer. 20 char max.
	// 	$BillingAddress = array (
	// 			'street' => '707 W. Bay Drive',
	// 			'street2' => '',
	// 			'city' => 'Largo',
	// 			'state' => 'FL',
	// 			'countrycode' => 'US',
	// 			'zip' => '33770',
	// 			'phonenum' => ''
	// 	);
	//
	// 	// Required if shipping is included. Person's name associated with this address. 32 char max.
	// 	// Required if shipping is included. First street address. 100 char max.
	// 	// Second street address. 100 char max.
	// 	// Required if shipping is included. Name of city. 40 char max.
	// 	// Required if shipping is included. Name of state or province. 40 char max.
	// 	// Required if shipping is included. Postal code of shipping address. 20 char max.
	// 	// Required if shipping is included. Country code of shipping address. 2 char max.
	// 	// Phone number for shipping address. 20 char max.
	// 	$ShippingAddress = array (
	// 			'shiptoname' => '',
	// 			'shiptostreet' => '',
	// 			'shiptostreet2' => '',
	// 			'shiptocity' => '',
	// 			'shiptostate' => '',
	// 			'shiptozip' => '',
	// 			'shiptocountrycode' => '',
	// 			'shiptophonenum' => ''
	// 	);
	//
	// 	// Required. Total amount of order, including shipping, handling, and tax.
	// 	// Required. Three-letter currency code. Default is USD.
	// 	// Required if you include itemized cart details. (L_AMTn, etc.) Subtotal of items not including S&H, or tax.
	// 	// Total shipping costs for the order. If you specify shippingamt, you must also specify itemamt.
	// 	// Total handling costs for the order. If you specify handlingamt, you must also specify itemamt.
	// 	// Required if you specify itemized cart tax details. Sum of tax for all items on the order. Total sales tax.
	// 	// Description of the order the customer is purchasing. 127 char max.
	// 	// Free-form field for your own use. 256 char max.
	// 	// Your own invoice or tracking number
	// 	// An ID code for use by 3rd party apps to identify transactions.
	// 	// URL for receiving Instant Payment Notifications. This overrides what your profile is set to use.
	// 	$PaymentDetails = array (
	// 			'amt' => '3.00',
	// 			'currencycode' => 'USD',
	// 			'itemamt' => '',
	// 			'shippingamt' => '',
	// 			'handlingamt' => '',
	// 			'taxamt' => '',
	// 			'desc' => 'Testing Payments Pro DESC Field',
	// 			'custom' => 'TEST',
	// 			'invnum' => '',
	// 			'buttonsource' => '',
	// 			'notifyurl' => ''
	// 	);
	//
	// 	$OrderItems = array ();
	//
	// 	// Item Name. 127 char max.
	// 	// Item description. 127 char max.
	// 	// Cost of individual item.
	// 	// Item Number. 127 char max.
	// 	// Item quantity. Must be any positive integer.
	// 	// Item's sales tax amount.
	// 	// eBay auction number of item.
	// 	// eBay transaction ID of purchased item.
	// 	// eBay order ID for the item.
	// 	$Item = array (
	// 			'l_name' => 'Test Widget',
	// 			'l_desc' => 'This is a test widget description.',
	// 			'l_amt' => '1.00',
	// 			'l_number' => 'ABC-123',
	// 			'l_qty' => '1',
	// 			'l_taxamt' => '',
	// 			'l_ebayitemnumber' => '',
	// 			'l_ebayitemauctiontxnid' => '',
	// 			'l_ebayitemorderid' => ''
	// 	);
	// 	array_push ( $OrderItems, $Item );
	//
	// 	// Item Name. 127 char max.
	// 	// Item description. 127 char max.
	// 	// Cost of individual item.
	// 	// Item Number. 127 char max.
	// 	// Item quantity. Must be any positive integer.
	// 	// Item's sales tax amount.
	// 	// eBay auction number of item.
	// 	// eBay transaction ID of purchased item.
	// 	// eBay order ID for the item.
	// 	$Item = array (
	// 			'l_name' => 'Test Widget 2',
	// 			'l_desc' => 'This is a test widget description.',
	// 			'l_amt' => '2.00',
	// 			'l_number' => 'ABC-XYZ',
	// 			'l_qty' => '1',
	// 			'l_taxamt' => '',
	// 			'l_ebayitemnumber' => '',
	// 			'l_ebayitemauctiontxnid' => '',
	// 			'l_ebayitemorderid' => ''
	// 	);
	// 	array_push ( $OrderItems, $Item );
	//
	// 	$PayPalRequestData = array (
	// 			'DPFields' => $DPFields,
	// 			'CCDetails' => $CCDetails,
	// 			'PayerInfo' => $PayerInfo,
	// 			'PayerName' => $PayerName,
	// 			'BillingAddress' => $BillingAddress,
	// 			'PaymentDetails' => $PaymentDetails,
	// 			'OrderItems' => $OrderItems
	// 	);
	// 	$PayPalResult = $PayPal->DoDirectPayment ( $PayPalRequestData );
	// 	$_SESSION ['transaction_id'] = isset ( $PayPalResult ['TRANSACTIONID'] ) ? $PayPalResult ['TRANSACTIONID'] : '';
	//
	// 	echo '<pre />';
	// 	print_r ( $PayPalResult );
	// }

	function setExpressCheckOut($checkout) {
		$ci = & get_instance ();
		$PayPal = $this->createPayPalClient ();

		// A timestamped token, the value of which was returned by a previous SetExpressCheckout call.
		// The expected maximum total amount the order will be, including S&H and sales tax.
		// Required. URL to which the customer will be returned after returning from PayPal. 2048 char max.
		// Required. URL to which the customer will be returned if they cancel payment on PayPal's site.
		// URL to which the callback request from PayPal is sent. Must start with https:// for production.
		// An override for you to request more or less time to be able to process the callback request and response. Acceptable range for override is 1-6 seconds. If you specify greater than 6 PayPal will use default value of 3 seconds.
		// The version of the Instant Update API you're using. The default is the current version.
		// The value 1 indicates that you require that the customer's shipping address is Confirmed with PayPal. This overrides anything in the account profile. Possible values are 1 or 0.
		// The value 1 indicates that on the PayPal pages, no shipping address fields should be displayed. Maybe 1 or 0.
		// The value 1 indicates that the customer may enter a note to the merchant on the PayPal page during checkout. The note is returned in the GetExpresscheckoutDetails response and the DoExpressCheckoutPayment response. Must be 1 or 0.
		// The value 1 indicates that the PayPal pages should display the shipping address set by you in the SetExpressCheckout request, not the shipping address on file with PayPal. This does not allow the customer to edit the address here. Must be 1 or 0.
		// Locale of pages displayed by PayPal during checkout. Should be a 2 character country code. You can retrive the country code by passing the country name into the class' GetCountryCode() function.
		// Sets the Custom Payment Page Style for payment pages associated with this button/link.
		// URL for the image displayed as the header during checkout. Max size of 750x90. Should be stored on an https:// server or you'll get a warning message in the browser.
		// Sets the border color around the header of the payment page. The border is a 2-pixel permiter around the header space. Default is black.
		// Sets the background color for the header of the payment page. Default is white.
		// Sets the background color for the payment page. Default is white.
		// This is a custom field not included in the PayPal documentation. It's used to specify whether you want to skip the GetExpressCheckoutDetails part of checkout or not. See PayPal docs for more info.
		// Email address of the buyer as entered during checkout. PayPal uses this value to pre-fill the PayPal sign-in page. 127 char max.
		// Type of checkout flow. Must be Sole (express checkout for auctions) or Mark (normal express checkout)
		// Type of PayPal page to display. Can be Billing or Login. If billing it shows a full credit card form. If Login it just shows the login screen.
		// Type of channel. Must be Merchant (non-auction seller) or eBayItem (eBay auction)
		// The URL on the merchant site to redirect to after a successful giropay payment. Only use this field if you are using giropay or bank transfer payment methods in Germany.
		// The URL on the merchant site to redirect to after a canceled giropay payment. Only use this field if you are using giropay or bank transfer methods in Germany.
		// The URL on the merchant site to transfer to after a bank transfter payment. Use this field only if you are using giropay or bank transfer methods in Germany.
		// A label that overrides the business name in the PayPal account on the PayPal hosted checkout pages. 127 char max.
		// Merchant Customer Service number displayed on the PayPal Review page. 16 char max.
		// Enable gift message widget on the PayPal Review page. Allowable values are 0 and 1
		// Enable gift receipt widget on the PayPal Review page. Allowable values are 0 and 1
		// Enable gift wrap widget on the PayPal Review page. Allowable values are 0 and 1.
		// Label for the gift wrap option such as "Box with ribbon". 25 char max.
		// Amount charged for gift-wrap service.
		// Enable buyer email opt-in on the PayPal Review page. Allowable values are 0 and 1
		// Text for the survey question on the PayPal Review page. If the survey question is present, at least 2 survey answer options need to be present. 50 char max.
		// Enable survey functionality. Allowable values are 0 and 1
		// The unique identifier provided by eBay for this buyer. The value may or may not be the same as the username. In the case of eBay, it is different. 255 char max.
		// The user name of the user at the marketplaces site.
		// Date when the user registered with the marketplace.
		// Whether the merchant can accept push funding. 0 = Merchant can accept push funding : 1 = Merchant cannot accept push funding.
		$SECFields = array (
				'token' => '',
				//'maxamt' => '200.00',
				'returnurl' => base_url ( 'payment/paypal_return' ),
				'cancelurl' => base_url ( 'payment/paypal_cancel' ),
				'callback' => '',
				'callbacktimeout' => '',
				'callbackversion' => '',
				'reqconfirmshipping' => '0',
				'noshipping' => '1',
				'allownote' => '0',
				'addroverride' => '1',
				'localecode' => '',
				'pagestyle' => '',
				'hdrimg' => '',
				'hdrbordercolor' => '',
				'hdrbackcolor' => '',
				'payflowcolor' => '',
				'skipdetails' => '',
				'email' => '',
				'solutiontype' => 'Sole',
				'landingpage' => (empty($checkout['landing_page'])?'Billing':'Login'),
				'channeltype' => '',
				'giropaysuccessurl' => '',
				'giropaycancelurl' => '',
				'banktxnpendingurl' => '',
				'brandname' => 'Hachi.sg Pte Ltd',
				'customerservicenumber' => '',
				/*'giftmessageenable' => '0',
				'giftreceiptenable' => '0',
				'giftwrapenable' => '0',
				'giftwrapname' => 'Box with Ribbon',
				'giftwrapamount' => '2.50',
				'buyeremailoptionenable' => '1',
				'surveyquestion' => '',
				'surveyenable' => '1',
				'buyerid' => '',
				'buyerusername' => '',
				'buyerregistrationdate' => '2012-07-14T00:00:00Z',
				'allowpushfunding' => ''*/
		);

		// Basic array of survey choices. Nothing but the values should go in here.
		$SurveyChoices = array (
				'Yes',
				'No'
		);

		$Payments = array ();

		// Required. The total cost of the transaction to the customer. If shipping cost and tax charges are known, include them in this value. If not, this value should be the current sub-total of the order.
		// A three-character currency code. Default is USD.
		// Required if you specify itemized L_AMT fields. Sum of cost of all items in this order.
		// Total shipping costs for this order. If you specify SHIPPINGAMT you mut also specify a value for ITEMAMT.
		// If true, the insurance drop-down on the PayPal review page displays the string 'Yes' and the insurance amount. If true, the total shipping insurance for this order must be a positive number.
		// Total handling costs for this order. If you specify HANDLINGAMT you mut also specify a value for ITEMAMT.
		// Required if you specify itemized L_TAXAMT fields. Sum of all tax items in this order.
		// Description of items on the order. 127 char max.
		// Free-form field for your own use. 256 char max.
		// Your own invoice or tracking number. 127 char max.
		// URL for receiving Instant Payment Notifications
		// Required if shipping is included. Person's name associated with this address. 32 char max.
		// Required if shipping is included. First street address. 100 char max.
		// Second street address. 100 char max.
		// Required if shipping is included. Name of city. 40 char max.
		// Required if shipping is included. Name of state or province. 40 char max.
		// Required if shipping is included. Postal code of shipping address. 20 char max.
		// Required if shipping is included. Country code of shipping address. 2 char max.
		// Phone number for shipping address. 20 char max.
		// Note to the merchant. 255 char max.
		// The payment method type. Specify the value InstantPaymentOnly.
		// How you want to obtain the payment. When implementing parallel payments, this field is required and must be set to Order.
		// A unique identifier of the specific payment request, which is required for parallel payments.
		// A unique identifier for the merchant. For parallel payments, this field is required and must contain the Payer ID or the email address of the merchant.
		$Payment = array (
				'amt' => $checkout['amount'],
				'currencycode' => 'SGD',
				'itemamt' => $checkout['amount']-8.00,
				'shippingamt' => '8.00',
				/*'insuranceoptionoffered' => '',
				'handlingamt' => '',
				'taxamt' => '5.00',*/
				// 'custom' => $checkout['description'],
				'invnum' => $checkout['transaction_id'],
				//'notifyurl' => '',
				'shiptoname' => $checkout['ship_first_name'],
				'shiptostreet' => $checkout['ship_street_line1'],
				'shiptostreet2' => $checkout['ship_street_line2'],
				'shiptocity' => 'Sinagpore',
				'shiptostate' => 'Sinagpore',
        'shiptocountrycode' => $checkout['ship_country_id'],
				'shiptozip' => $checkout['ship_postal_code'],
				'shiptocountry' => 'Sinagpore',
				'shiptophonenum' => $checkout['ship_contact_no']
				/*'notetext' => 'This is a test note before ever having left the web site.',
				'allowedpaymentmethod' => '',
				'paymentaction' => 'Sale',*/
				/*'paymentrequestid' => '',
				'sellerpaypalaccountid' => ''*/
		);

		$PaymentOrderItems = array ();

		// Item name. 127 char max.
		// Item description. 127 char max.
		// Cost of item.
		// Item number. 127 char max.
		// Item qty on order. Any positive integer.
		// Item sales tax
		// URL for the item.
		// One of the following values: Digital, Physical
		// The weight value of the item.
		// The weight unit of the item.
		// The height value of the item.
		// The height unit of the item.
		// The width value of the item.
		// The width unit of the item.
		// The length value of the item.
		// The length unit of the item.
		// Auction item number.
		// Auction transaction ID number.
		// Auction order ID number.
		// The unique identifier provided by eBay for this order from the buyer. These parameters must be ordered sequentially beginning with 0 (for example L_EBAYITEMCARTID0, L_EBAYITEMCARTID1). Character length: 255 single-byte characters
		$Item = array (
				//'name' => 'Transaction ID '.$checkout['transaction_id'],
				'name' => 'Item 1',
        //'desc' => $checkout['itemList'],
				'amt' => $checkout['amount'] - 8.00,
				//'number' => '123',
				'qty' => '1',
				//'taxamt' => '',
				//'itemurl' => 'http://www.angelleye.com/products/123.php',
				/*'itemcategory' => '',
				'itemweightvalue' => '',
				'itemweightunit' => '',
				'itemheightvalue' => '',
				'itemheightunit' => '',
				'itemwidthvalue' => '',
				'itemwidthunit' => '',
				'itemlengthvalue' => '',
				'itemlengthunit' => '',
				'ebayitemnumber' => '',
				'ebayitemauctiontxnid' => '',
				'ebayitemorderid' => '',
				'ebayitemcartid' => ''*/
		);
		array_push ( $PaymentOrderItems, $Item );

		$Payment ['order_items'] = $PaymentOrderItems;
		array_push ( $Payments, $Payment );

		// The unique identifier provided by eBay for this buyer. The value may or may not be the same as the username. In the case of eBay, it is different. Char max 255.
		// The username of the marketplace site.
		// The registration of the buyer with the marketplace.
		$BuyerDetails = array (
				'buyerid' => '',
				'buyerusername' => '',
				'buyerregistrationdate' => ''
		);

		// For shipping options we create an array of all shipping choices similar to how order items works.
		$ShippingOptions = array ();

		// Shipping option. Required if specifying the Callback URL. true or false. Must be only 1 default!
		// Shipping option name. Required if specifying the Callback URL. 50 character max.
		// Shipping option label. Required if specifying the Callback URL. 50 character max.
		// Shipping option amount. Required if specifying the Callback URL.
		$Option = array (
				'l_shippingoptionisdefault' => '',
				'l_shippingoptionname' => '',
				'l_shippingoptionlabel' => '',
				'l_shippingoptionamount' => ''
		);
		array_push ( $ShippingOptions, $Option );

		$BillingAgreements = array ();

		// Required. Type of billing agreement. For recurring payments it must be RecurringPayments. You can specify up to ten billing agreements. For reference transactions, this field must be either: MerchantInitiatedBilling, or MerchantInitiatedBillingSingleSource
		// Required for recurring payments. Description of goods or services associated with the billing agreement.
		// Specifies the type of PayPal payment you require for the billing agreement. Any or IntantOnly
		// Custom annotation field for your own use. 256 char max.
		$Item = array (
				'l_billingtype' => 'MerchantInitiatedBilling',
				'l_billingagreementdescription' => 'Billing Agreement',
				'l_paymenttype' => 'Any',
				'l_billingagreementcustom' => ''
		);
		//array_push ( $BillingAgreements, $Item );

		$PayPalRequest = array (
				'SECFields' => $SECFields,
				'SurveyChoices' => $SurveyChoices,
				//'BillingAgreements' => $BillingAgreements,
				'Payments' => $Payments
		);

		$_SESSION ['SetExpressCheckoutResult'] = $PayPal->SetExpressCheckout ( $PayPalRequest );

		if(empty($checkout['no_redirect'])) {
			redirect($_SESSION ['SetExpressCheckoutResult'] ['REDIRECTURL']);
		}	else {
			return $_SESSION ['SetExpressCheckoutResult'] ['REDIRECTURL'];
		}

		/*echo '<a href="' . $_SESSION ['SetExpressCheckoutResult'] ['REDIRECTURL'] . '">Click here to continue.</a><br /><br />';
		echo '<pre />';
		print_r ( $_SESSION ['SetExpressCheckoutResult'] );*/
	}

	function doExpressCheckOut($checkout) {
		$PayPal = $this->createPayPalClient ();

		/*
		 * Here we call GetExpressCheckoutDetails to obtain payer information from PayPal
		 */
		$GECDResult = $PayPal->GetExpressCheckoutDetails ( $_SESSION ['SetExpressCheckoutResult'] ['TOKEN'] );

		// Required. A timestamped token, the value of which was returned by a previous SetExpressCheckout call.
		// Required. Unique PayPal customer id of the payer. Returned by GetExpressCheckoutDetails, or if you used SKIPDETAILS it's returned in the URL back to your RETURNURL.
		// Flag to indicate whether you want the results returned by Fraud Management Filters or not. 1 or 0.
		// The gift message entered by the buyer on the PayPal Review page. 150 char max.
		// Pass true if a gift receipt was selected by the buyer on the PayPal Review page. Otherwise pass false.
		// The gift wrap name only if the gift option on the PayPal Review page was selected by the buyer.
		// The amount only if the gift option on the PayPal Review page was selected by the buyer.
		// The buyer email address opted in by the buyer on the PayPal Review page.
		// The survey question on the PayPal Review page. 50 char max.
		// The survey response selected by the buyer on the PayPal Review page. 15 char max.
		// The payment method type. Specify the value InstantPaymentOnly.
		// ID code for use by third-party apps to identify transactions in PayPal.
		$DECPFields = array (
				'token' => $_SESSION ['SetExpressCheckoutResult'] ['TOKEN'],
				'payerid' => $GECDResult ['PAYERID'],
				'returnfmfdetails' => '1',
				'giftmessage' => '',
				'giftreceiptenable' => '',
				'giftwrapname' => '',
				'giftwrapamount' => '',
				'buyermarketingemail' => '',
				'surveyquestion' => '',
				'surveychoiceselected' => '',
				'allowedpaymentmethod' => '',
				'buttonsource' => ''
		);

		$Payments = array ();

		// Required. The total cost of the transaction to the customer. If shipping cost and tax charges are known, include them in this value. If not, this value should be the current sub-total of the order.
		// A three-character currency code. Default is USD.
		// Required if you specify itemized L_AMT fields. Sum of cost of all items in this order.
		// Total shipping costs for this order. If you specify SHIPPINGAMT you mut also specify a value for ITEMAMT.
		// If true, the insurance drop-down on the PayPal review page displays the string 'Yes' and the insurance amount. If true, the total shipping insurance for this order must be a positive number.
		// Total handling costs for this order. If you specify HANDLINGAMT you mut also specify a value for ITEMAMT.
		// Required if you specify itemized L_TAXAMT fields. Sum of all tax items in this order.
		// Description of items on the order. 127 char max.
		// Free-form field for your own use. 256 char max.
		// Your own invoice or tracking number. 127 char max.
		// URL for receiving Instant Payment Notifications
		// Required if shipping is included. Person's name associated with this address. 32 char max.
		// Required if shipping is included. First street address. 100 char max.
		// Second street address. 100 char max.
		// Required if shipping is included. Name of city. 40 char max.
		// Required if shipping is included. Name of state or province. 40 char max.
		// Required if shipping is included. Postal code of shipping address. 20 char max.
		// Required if shipping is included. Country code of shipping address. 2 char max.
		// Phone number for shipping address. 20 char max.
		// Note to the merchant. 255 char max.
		// The payment method type. Specify the value InstantPaymentOnly.
		// How you want to obtain the payment. When implementing parallel payments, this field is required and must be set to Order.
		// A unique identifier of the specific payment request, which is required for parallel payments.
		// A unique identifier for the merchant. For parallel payments, this field is required and must contain the Payer ID or the email address of the merchant.
		$Payment = array (
				'amt' => $GECDResult['AMT'],
				'currencycode' => 'SGD',
				'itemamt' => $GECDResult['AMT'],
				/*'shippingamt' => '15.00',
				'insuranceoptionoffered' => '',
				'handlingamt' => '',
				'taxamt' => '5.00',*/
				// 'desc' => $GECDResult['CUSTOM'],
				// 'custom' => $GECDResult['CUSTOM'],
				'invnum' => $GECDResult['INVNUM'],
				/*'notifyurl' => '',
				'shiptoname' => '',
				'shiptostreet' => '',
				'shiptostreet2' => '',
				'shiptocity' => '',
				'shiptostate' => '',
				'shiptozip' => '',
				'shiptocountry' => '',
				'shiptophonenum' => '',
				'notetext' => 'This is a test note before ever having left the web site.',
				'allowedpaymentmethod' => '',*/
				'paymentaction' => 'Sale',
				/*'paymentrequestid' => '',
				'sellerpaypalaccountid' => ''*/
		);


		$PaymentOrderItems = array ();

		// Item name. 127 char max.
		// Item description. 127 char max.
		// Cost of item.
		// Item number. 127 char max.
		// Item qty on order. Any positive integer.
		// Item sales tax
		// URL for the item.
		// The weight value of the item.
		// The weight unit of the item.
		// The height value of the item.
		// The height unit of the item.
		// The width value of the item.
		// The width unit of the item.
		// The length value of the item.
		// The length unit of the item.
		// Auction item number.
		// Auction transaction ID number.
		// Auction order ID number.
		// The unique identifier provided by eBay for this order from the buyer. These parameters must be ordered sequentially beginning with 0 (for example L_EBAYITEMCARTID0, L_EBAYITEMCARTID1). Character length: 255 single-byte characters
		$Item = array (
		'name' => 'Transaction ID '.$GECDResult['INVNUM'],
		// 'desc' => $GECDResult['CUSTOM'],
		'amt' => $GECDResult['AMT'],
		//'number' => '123',
		'qty' => '1',
		//'taxamt' => '',
		//'itemurl' => 'http://www.angelleye.com/products/123.php',
		/*'itemcategory' => '',
		'itemweightvalue' => '',
		'itemweightunit' => '',
		'itemheightvalue' => '',
		'itemheightunit' => '',
		'itemwidthvalue' => '',
		'itemwidthunit' => '',
		'itemlengthvalue' => '',
		'itemlengthunit' => '',
		'ebayitemnumber' => '',
		'ebayitemauctiontxnid' => '',
		'ebayitemorderid' => '',
		'ebayitemcartid' => ''*/
		);
		array_push ( $PaymentOrderItems, $Item );

		$Payment ['order_items'] = $PaymentOrderItems;
		array_push ( $Payments, $Payment );

		// Describes how the options that were presented to the user were determined. values are: API - Callback or API - Flatrate.
		// The Yes/No option that you chose for insurance.
		// Is true if the buyer chose the default shipping option.
		// The shipping amount that was chosen by the buyer.
		// Is true if the buyer chose the default shipping option...?? Maybe this is supposed to show the name..??
		$UserSelectedOptions = array (
				'shippingcalculationmode' => '',
				'insuranceoptionselected' => '',
				'shippingoptionisdefault' => '',
				'shippingoptionamount' => '',
				'shippingoptionname' => ''
		);

		$PayPalRequest = array (
				'DECPFields' => $DECPFields,
				'Payments' => $Payments
		);

		$_SESSION ['PayPalResult'] = $PayPal->DoExpressCheckoutPayment ( $PayPalRequest );
		/*echo '<pre />';
		print_r ( $_SESSION ['PayPalResult'] );*/

		$GECDResult = $PayPal->GetExpressCheckoutDetails ( $_SESSION ['SetExpressCheckoutResult'] ['TOKEN'] );
		/*echo '<b>GetExpressCheckoutDetails</b><br /><pre>';
		print_r ( $GECDResult );
		echo '<br /><br /></pre>';*/

		return $GECDResult;
	}
}
