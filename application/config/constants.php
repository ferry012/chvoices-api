<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
//defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('HACHI_COY_ID', 'HSG');
define('CTL_COY_ID', 'CTL');

define('DEFAULT_PER_PAGE', 20);

define('HACHI_SELF_COLLECTION_LOC_LIST', "'TP','NEX','BJ','JEM','JP','VC'");

define('ADDRESS_PRIMARY', '0-PRIMARY');
define('ADDRESS_DELIVERY', '2-DELIVERY');

define('CTL_MEMBER_TRANSFER_DAYS', 1);
define('HACHI_MEMBERSHIP_ABOUT_EXPIRY_DAY', 60);
define('HACHI_MEMBERSHIP_ITEM_ID', '!MEMBER-HACHI');
define('HACHI_MEMBERSHIP_YEAR', 1);
define('HACHI_MEMBER_REG_URL','register');
define('HACHI_VERSION', 2);

define('APPLY_GST', 1.07);
define('FREE_SHIPPING_COST', 88);
define('STANDARD_SHIPPING_RATE', 8);
define('POSTAGE_SHIPPING_RATE', 1.2);

define('PAYPAL_SANDBOX_MODE', TRUE);

define('REGEX_PASSWORD', '/^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{6,}$/');

define('CART_ID','cart_id');
define('CART_HOLDER', 'cart');
define('CART_EXPIRY', 259200); //3 days
//define('CART_URL','cart/my_cart');
define('CART_URL','cart'); //change to new cart design
define('CART_SUMMARY_ORDER_URL','cart/cart_summary');
define('CART_PAYMENT_URL','payment/gateway');

define('EMARSYS_USERNAME', 'hachi001');
define('EMARSYS_SECRET', 'HZyv6cG90hDXJiNkH76X');


define('EMARSYS_MS_TPL_ACC_CREATED','member_registration');
define('EMARSYS_MS_TPL_RESET_PW_LINK','forgot_password');
define('EMARSYS_MS_TPL_PW_CHANGED','change_password');
define('EMARSYS_UPDATE_PROFILE','updated_profile');
define('EMARSYS_ORDER_CONFIRMATION','order_confirmation');
define('EMARSYS_ORDER_DISPATCHED_NNV','order_dispatched_nnv');
define('EMARSYS_ORDER_DISPATCHED_NZY','order_dispatched_nzy');
define('EMARSYS_ORDER_COLLECTION','order_collection');
define('EMARSYS_ORDER_COLLECTION_REMINDER','order_collection_reminder');
define('EMARSYS_PRE_ORDER_HUAWEI','preorder_huaweip9');
define('EMARSYS_PRE_ORDER_CEE','preorder_cee');
define('EMARSYS_AUCTION_WINNER','auction_winner');

define('EMARSYS_STUDENT_MEMBERSHIP','student_membership');
define('EMARSYS_WISHLIST_NOTIFY','notify_me');

define('EMARSYS_STORE_DO_FORM','store_do_form');
define('EMARSYS_ORDER_DISPATCHED_STAGING','order_dispatched_staging');
define('EMARSYS_ORDER_CONFIRMATION_STAGING','order_confirmation_staging');


define('TRACKING_URL_NNV','https://track.aftership.com/ninjavan/');
define('TRACKING_URL_NZY','http://sg.zyllem.com/Track/');


define('PROCESS_EMARSYS_ID','Emarsys');
define('PROCESS_ACC_ID','Account');
define('PROCESS_REGISTER_ID','Register');
//define('PROCESS_ID','');

define('REQUEST_GET', 'GET');
define('REQUEST_POST', 'POST');
define('REQUEST_PUT', 'PUT');
define('REQUEST_DELETE', 'DELETE');

// Alpha payment config
define('HO_APC_AUTO_SUBMIT', TRUE);
define('HO_APC_PAYMENT_GATEWAY_URL', 'https://hub.sg.alphapaymentscloud.com/pp/b955a73b-6581-412d-81f8-008700a4a1db');
define('HO_APC_CALLBACK_SUCCESS_URL', 'https://www.hachi.tech/cart/apc_success');
define('HO_APC_CALLBACK_CANCEL_URL', 'https://www.hachi.tech/cart/apc_cancel');
define('HO_APC_MERCHANT_ID', 'HACHISG001');
define('HO_APC_USER_ID', 'Challenger01');
define('HO_APC_AMOUNT_RATE', 1000);
define('HO_APC_CURRENCY', 'SGD');
define('HO_APC_LANG', 'EN');
define('HO_APC_METHOD', '');

// Payment
define('HO_PAYMENT_LOCK', 10); // In mins
define('HO_PAYMENT_KEY', '68d97b710e3c04af0546dcfb711c10ca');

define('HO_ENETS_PAYMENT_GATEWAY_URL','http://payment.hachi.tech/payment/method');
define('HO_ENETS_AMOUNT_RATE', 100);

define('HO_ENABLE_POC', TRUE);

define('SUPP_INV_PREFIXDATE', 'Ym-');
define('START_PREFIXDATE', 1522511909);