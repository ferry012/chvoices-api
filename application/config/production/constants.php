<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/* Paging */

define('DEFAULT_PAGE', 1);
define('DEFAULT_LIMIT', 10);

/* define API BASE URL */

define('API_BASE_LINK', 'localhost/riiko/challenger_api/index.php/');

/*
  The 'IS_AJAX' constant allows for a quick simple test as to whether the
  current request was made with XHR.
 */
define('YES_FLAG', 'Y');
define('NO_FLAG', 'N');
define('ACTIVE', '1');
define('ACCEPT', '2');
define('NOT_ACTIVE', '-1');
define('PENDING','0');
define('CANCELLED', '-2');
define('REMOVED', '-3');
define('REJECTED', '-4');
define('PRODUCT_IMAGE_FOLDER', 'http://localhost:8080/projects/andios_web/public/uploads/product_images/');
define('F_YES', 'F_YES');
define('F_NO', 'F_NO');

$ajax_request = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? TRUE : FALSE;
define('IS_AJAX', $ajax_request);
unset($ajax_request);

define('ELASTICACHE_URL', 'app-cache.7waan3.0001.apse1.cache.amazonaws.com');
define('ELASTICACHE_PORT', 11211);
define('ELASTICACHE_EXPIRY', 900); // in seconds

define('LOGIN_TYPE_FACEBOOK', 'facebook');
define('LOGIN_TYPE_SINGPASS', 'singpass');
define('LOGIN_TYPE_NORMAL', 'normal');
define('LOGIN_TYPE_TWITTER', 'twitter');

define('STATUS_CODE', 'status_code');
define('MESSAGE', 'message');
define('RESULTS', 'results');
define('ACCESS_TOKEN', 'access_token');
define('FOLDER', 'folder');
define('RESULTSTAGS', 'resultTags');
define('TOTALTAGS', 'totalTags');
define('ERROR', 'error');

// API HTTP STATUS CODES
define('X_AUTHORIZATION', 'X-authorization');

define('SSC_HEADER_SUCCESS', 200); // when the request is successful

define('SSC_HEADER_PARAMETER_MISSING_INVALID', 400); // when required params are invalid or missing
define('SSC_HEADER_UNAUTHORIZED', 401); // when the provided authentication details doesnt have access to a resource
define('SSC_HEADER_FORBIDDEN', 403); // when authentication details not provided to access resource
define('SSC_HEADER_NOT_FOUND', 404); // when resource is not found

define('SSC_HEADER_INTERNAL_SERVER_ERROR', 500); // when something unexpected happened on the server


define('MAX_LIMIT', 100);
define('TOTAL', 'total');

define('PAGE', "page");
define('LIMIT', "limit");
define('OFFSET', "offset");

define('MOBILE',                        'mobile');
define('PUSH_NOTIFICATION',             'push_notification');

define('IMAGE_TYPE_ERROR', "Unknown image type");
define('IMAGE_SIZE_ERROR', "Invalid image size");

define('ELASTICACHE_EXPIRY_ONE_MINUTE', 60);
define('ELASTICACHE_EXPIRY_ONE_HOUR', 3600);
define('ELASTICACHE_EXPIRY_ONE_DAY', 86400);
define('SMALL', "s");
define('MEDIUM', "m");
define('BIG', "b");
define('ORIGINAL', "o");

define('S3_BASE_URL', 'https://s3-ap-southeast-1.amazonaws.com/');
//define('IMAGE_URL', 'http://ctl-public-test.s3-website-ap-southeast-1.amazonaws.com/');
define('IMAGE_URL', 'https://cdn.hachi.tech/');

define('DELAY_IN_MICRO', 100000);

define('HACHI_MEMBER_CODE', 'M');
define('LITE_MEMBER_CODE', 'LM');

//define('FILE_SERVER', FCPATH . 'cherpsfile/cherpsfile/');
//define('PDF_SERVER', FCPATH . 'cherpsfile/cherpsfile/chvoices/STAGING/');
//define('CHVOICES_SERVER', FCPATH . 'cherpsfile/cherpsfile/chvoices/');
define('FILE_SERVER', '/mnt/cherpsfile/');
define('PDF_SERVER', '/mnt/cherpsfile/PDF/');
define('CHVOICES_SERVER', '/mnt/cherpsfile/chvoices/');

//Ingram
define('INGRAM_PASS',"Challenger123");
define('INGRAM_USER',"SG_101083");
define('INGRAM_URL1',"https://mercury.ingrammicro.com:8447/SeeburgerHTTP/HTTPController?HTTPInterface=syncReply");

/* End of file constants.php */
/* Location: ./application/config/constants.php */
