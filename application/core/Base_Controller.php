<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Base_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
    }

    /**
     * @return array with page and limit of items\
     */
    protected function _filter_with_pagination($target, $page, $limit) {
        if ($target == NULL || !is_array($target)) {
            return array();
        }
        $results = array_chunk($target, $limit);

        if (isset($results[$page - 1])) { // -1 since array starts from 1
            return $results[$page - 1];
        } else {
            return array();
        }
    }
    
    protected function _checkLogged() {
        if ($this->session->userdata('cm_logged_in') != 1) {
            redirect('login/login');
        } 
    }

}

/* End of file Base_Controller.php */
/* Location: ./application/core/Base_Controller.php */