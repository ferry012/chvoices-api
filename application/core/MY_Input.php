<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Input extends CI_Input
{

    public function is($methods)
    {
        if (!is_array($methods)) {
            $methods = (array)$methods;
        }

        foreach ($methods as $method) {
            if (strtolower($method) == $this->method()) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Fetch an item from the GET array
     *
     * @param    mixed $index     Index for item to be fetched from $_GET
     * @param    bool  $xss_clean Whether to apply XSS filtering
     *
     * @return    mixed
     */
    public function get($index = NULL, $default_value = NULL, $xss_clean = NULL)
    {
        $value = $this->_fetch_from_array($_GET, $index, $xss_clean);

        return isset($value) ? $value : $default_value;
    }

    /**
     * Fetch an item from the POST array
     *
     * @param    mixed $index     Index for item to be fetched from $_POST
     * @param    bool  $xss_clean Whether to apply XSS filtering
     *
     * @return    mixed
     */
    public function post($index = NULL, $default_value = NULL, $xss_clean = NULL)
    {
        $value = $this->_fetch_from_array($_POST, $index, $xss_clean);

        return isset($value) ? $value : $default_value;
    }

}