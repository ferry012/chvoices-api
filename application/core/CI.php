<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Static CodeIgniter Object
 */
class CI
{

    private function __construct()
    {
    }

    private static $i;

    private static function instance()
    {
        if (!self::$i) {
            self::$i =& get_instance();
        }

        return self::$i;
    }

    public static function __callStatic($method, $parameters)
    {
        self::instance();
        if (isset(self::$i->$method)) {
            return self::$i->$method;
        } else {
            self::$i->$method = new $method;

            return self::$i->$method;
        }

    }

}