<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Front_Controller extends Base_Controller {

    public function __construct() {

        parent::__construct();

        CI::load()->library(['assets', 'template', 'user_agent', 'session']);
        CI::load()->driver('cache', ['adapter' => 'apc', 'backup' => 'file']);
        CI::load()->database('pg92');
        CI::load()->model([
        ]);

        Template::set_theme('default');



        $this->init();
    }

    public function init() {
        $this->ref_from = $this->input->get('refFrom', NULL, TRUE);
        $this->redirect_url = urldecode($this->input->get('redirectURL', NULL, TRUE));
        $arr_avoid_track_url = ['register', 'login', 'logout', 'account-convert', 'cart/convert_cart', 'login/forgot_password', 'reset-password', 'vc-reset-password'];

        if (!$this->input->is_ajax_request() && $this->input->is('get')) {
            $curr_url = current_url();
            if (!in_array($this->uri->uri_string, $arr_avoid_track_url) && CI::session()->userdata('redirect_back') != $curr_url) {
                CI::session()->set_userdata('redirect_back', $curr_url);
            }
        }
    }

    public function alertSuccess($msg) {
        $this->alert($msg, 'alert-success');
    }

    protected function alert($msg, $class) {

        $arr['class'] = $class;
        $arr['message'] = $msg;

        CI::session()->set_flashdata('flash_alert', $arr);
    }

    public function alertInfo($msg) {
        $this->alert($msg, 'alert-info');
    }

    public function alertWarning($msg) {
        $this->alert($msg, 'alert-warning');
    }

    public function alertDanger($msg) {
        $this->alert($msg, 'alert-danger');
    }

    protected function _sendOrderConfirmationEmail($trans_id, $email = '') {

        $info = CI::AccountModel()->get_transaction_info(trim($trans_id));

        if ($info === NULL) {
            exit;
        }

        CI::load()->library('encryption');

//        $country_id_b = $info->bill_country_id;
//        $country_id_s = $info->ship_country_id;
//        $country_name_b = CI::AccountModel()->get_country_name(trim($country_id_b));
//        $country_name_s = CI::AccountModel()->get_country_name(trim($country_id_s));

        $date = display_date_format($info->trandate);

//        $bAddr = $info->bill_street_line1 . " " .$info->bill_street_line2 . "<br>" . $country_name_b->country_name . "  " . $info->bill_postal_code;
//        $sAddr = $info->ship_street_line1 . " " . $info->ship_unit_no . "<br>" . $country_name_s->country_name . " " . $info->ship_postal_code;

        $encrypt_trans_id = base64url_encode(CI::encryption()->encrypt($trans_id));

        $url = 'invoice/external/' . $encrypt_trans_id;
        $email = !empty($email) ? $email : Customer::getEmail();
        $user_info = CI::AccountModel()->get_customer_info($info->mbr_id);
        $first_name = trim($user_info->first_name);

        $email_data = [
            'firstname' => is_empty($first_name),
            'email' => $email,
            'InvoiceNo' => trim($info->invoice_no),
            'TransactionDate' => is_empty($date),
            'OrderDetailsURL' => $url,
            'deliverycode' => $info->delv_code,
        ];

        $email_result = $this->_sendEmarsysMailStream(EMARSYS_ORDER_CONFIRMATION, $email_data);

        $recipient_id = $email_result['result'][0]['recipient_id'];
        $error = $email_result['result'][0]['error'];
        if (!empty($email_result) && $error != '') {
            $status = 'Failed';
        } elseif ((!empty($email_result) && $recipient_id != '')) {
            $status = 'Sent';
            $error = 'Recipient id >> ' . $recipient_id;
        } else {
            $status = 'Failed';
        }


        $sql_new = "insert into o2o_email_invoice_log (email_type,invoice_id,user_email,status,error) values (?,?,?,?,?)";
        $query_new = $this->db->query($sql_new, array(EMARSYS_ORDER_CONFIRMATION, $info->invoice_no, $email, $status, $error));
    }

    protected function _sendEmarsysMailStream($template_id, array $content, array $attachments = [], $method = REQUEST_POST) {
        if (empty($template_id)) {
            return;
        }

        $data['fields'] = $content;

        $send_data['recipients'][0] = [
            'fields' => $content,
        ];

        if (count($attachments) > 0) {
            $data['attachments'] = $attachments;
        }

        $send_data['recipients'][0] = $data;

        $api = new SuiteApi(EMARSYS_USERNAME, EMARSYS_SECRET);

        $path = 'mailstream/' . $template_id;

        $result = $api->send($method, $path, json_encode($send_data));
        //debug($content);EXIT;
        //$log = new stdClass();

        if (isset($result) && $result['result'][0]['error'] != '') {
            // @todo
            // record error log
            $process_id = PROCESS_EMARSYS_ID;
            $error = $result['result'][0]['error'];
            $format = 'Template id: %s Error: %s';
            $remarks = '';

            switch ($template_id) {
                case EMARSYS_MS_TPL_ACC_CREATED:
                    $remarks = sprintf($format, EMARSYS_MS_TPL_ACC_CREATED, $error);
                    break;
                case EMARSYS_MS_TPL_PW_CHANGED:
                    $remarks = sprintf($format, EMARSYS_MS_TPL_PW_CHANGED, $error);
                    break;
                case EMARSYS_MS_TPL_RESET_PW_LINK:
                    $remarks = sprintf($format, EMARSYS_MS_TPL_RESET_PW_LINK, $error);
                    break;
                case EMARSYS_ORDER_CONFIRMATION:
                    $remarks = sprintf($format, EMARSYS_ORDER_CONFIRMATION, $error);
                    break;
                case EMARSYS_ORDER_DISPATCHED_NNV:
                    $remarks = sprintf($format, EMARSYS_ORDER_DISPATCHED_NNV, $error);
                    break;
                case EMARSYS_ORDER_DISPATCHED_NZY:
                    $remarks = sprintf($format, EMARSYS_ORDER_DISPATCHED_NZY, $error);
                    break;
                case EMARSYS_ORDER_COLLECTION:
                    $remarks = sprintf($format, EMARSYS_ORDER_COLLECTION, $error);
                    break;
                case EMARSYS_UPDATE_PROFILE:
                    $remarks = sprintf($format, EMARSYS_UPDATE_PROFILE, $error);
                    break;
            }


            if (!empty($remarks)) {
                record_to_log($process_id, $remarks, Customer::getUserId());
            }
        }

        //if (isset($result) && $result['result'][0]['error'] != '') {
        //    $status = 'Failed';
        //    $error = $result['result'][0]['error'];
        //} else {
        //    $status = 'Sent';
        //    $error = NULL;
        //}
        //$log->email_type = EMARSYS_UPDATE_PROFILE;
        //$log->user_email = $content['email'];
        //$log->status = ;
        //$log->error = ;
        //$sql_new = "insert into o2o_email_invoice_log (email_type,user_email,status,error) values (?,?,?,?)";
        //$query_new = $this->db->query($sql_new, array(EMARSYS_UPDATE_PROFILE, $member->email_addr, $status, $error));

        return $result;
    }

    protected function _sendMemberRegistrationEmail($first_name = '', $last_name = '', $dob = '', $email = '', $contact_number = '', $expiry = '') {

        $data = [
            'FirstName' => $first_name,
            'LastName' => $last_name,
            'DateOfBirth' => $dob,
            'EMAIL' => $email,
            'ContactNumber' => $contact_number,
            'MemberExpiryDate' => $expiry,
        ];

        $result = $this->_sendEmarsysMailStream(EMARSYS_MS_TPL_ACC_CREATED, $data);
        $error = $result['result'][0]['error'];
        if (isset($result) && $error != '') {
            $status = 'Failed';
        } else {
            $status = 'Sent';
        }
        $sql_new = "insert into o2o_email_invoice_log (email_type,user_email,status,error) values (?,?,?,?)";
        $query_new = $this->db->query($sql_new, array(EMARSYS_MS_TPL_ACC_CREATED, $email, $status, $error));
    }

    protected function _sendNewMemberEmail($first_name = '', $last_name = '', $email = '', $birth_day = '', $birth_month = '', $phone = '') {

        $data = [
            'inp_1' => $first_name,
            'inp_2' => $last_name,
            'inp_3' => $email,
            'inp_4' => $birth_day,
            'inp_5' => $birth_month,
            'inp_6' => $phone,
        ];

        $url = 'https://suite11.emarsys.net/u/register.php?CID=557573554&f=2281&p=2&a=r&SID=&el=&llid=&counted=&c=&optin=y&interest[]=&' . http_build_query($data);

        file_get_contents($url);
    }

    protected function _sendValueClubMemberEmail($first_name = '', $last_name = '', $email = '') {

        $data = [
            'inp_1' => $first_name,
            'inp_2' => $last_name,
            'inp_3' => $email,
        ];

        $url = 'https://suite11.emarsys.net/u/register.php?CID=557573554&f=2282&p=2&a=r&SID=&el=&llid=&counted=&c=&optin=y&interest[]=&' . http_build_query($data);
        file_get_contents($url);
    }

    protected function getPage() {
        $page = $this->input->get('page');

        if (empty($page)) {
            return DEFAULT_PAGE;
        }

        return $page;
    }

    protected function getLimit() {
        $limit = $this->input->get('limit');

        if (empty($limit)) {
            return DEFAULT_LIMIT;
        }

        return $limit;
    }

    protected function getPerPage() {
        $per_page = CI::input()->get('PerPage');

        if (isset($per_page) && in_array($per_page, [40, 60, 80])) {
            CI::session()->set_userdata('per_page', $per_page);
        } elseif ($per_page == DEFAULT_PER_PAGE) {
            CI::session()->unset_userdata('per_page');
        } elseif (CI::session()->has_userdata('per_page')) {
            $per_page = CI::session()->userdata('per_page');
        } else {
            $per_page = DEFAULT_PER_PAGE;
        }

        return $per_page;
    }

    protected function _redirectBack() {
        $redirect = '';

        if (CI::session()->has_userdata('redirect_back')) {
            $redirect = CI::session()->userdata('redirect_back');
        }

        redirect($redirect);
    }

    public function _checkAccess(array $params) {
        $this->db->select('pms_supp_usr_list.supp_id,'
                . 'pms_supp_usr_list.supp_usr_id,'
                . 'pms_supp_usr_list.po,'
                . 'pms_supp_usr_list.rn,'
                . 'pms_supp_usr_list.invoice,'
                . 'pms_supp_usr_list.inventory,'
                . 'pms_supp_usr_list.status_level');
        $this->db->where('pms_supp_usr_list.status_level', ACTIVE);
        if (isset($params["supp_usr_id"]) && $params["supp_usr_id"] != NULL) {
            $this->db->where('pms_supp_usr_list.supp_usr_id', $params["supp_usr_id"]);
        }
        $result = $this->db->get('pms_supp_usr_list')->result();
        if (empty($result)) {
            $data["error_message"] = "No access this page!";
            $this->session->set_flashdata('error_message', $data["error_message"]);
            redirect(base_url('home/index'), 'refresh');
        }
    }

    public function _getStaffId(){
        $this->load->helper(array('cookie', 'url')); 
        $this->load->helper('url');
        $this->load->model('Sss_model');

       return $staff_profile = $this->Sss_model->getUser(get_cookie('usr_profile'));
    }
}

/* End of file Front_Controller.php */
/* Location: ./application/core/Front_Controller.php */
