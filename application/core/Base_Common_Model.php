<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Base_Common_Model extends Base_Model {

    const NO_RECORDS_FOUND = 'No records found.';
    const COMPANY_ID = 'CTL';
    const COMMON_PASSWORD = 'ch@408553';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function common_add($tableName, $data) {
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    public function common_edit($tableName, $key, $value, $data) {
        $data['updated_at'] = date("Y-m-d H:i:s");

        $this->db->where($key, $value);
        if ($this->db->update($tableName, $data)) {
            return $this->db->affected_rows();
        } else {
            return FALSE;
        }
    }

    public function common_edit_without_date($tableName, $key, $value, $data) {
        $this->db->where($key, $value);
        if ($this->db->update($tableName, $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function common_select_all($tableName) {
        return $this->db->get_where($tableName, array('deleted_at' => NULL))->result();
    }

    public function common_select_where($tableName, $key, $value) {
        return $this->db->get_where($tableName, array($key => $value, 'deleted_at' => NULL))->result();
    }

    public function common_select_row($tableName, $key, $value) {
        return $this->db->get_where($tableName, array($key => $value, 'deleted_at' => NULL))->row();
    }

    public function common_select_without_date($tableName, $key, $value) {
        return $this->db->get_where($tableName, array($key => $value))->result();
    }

    public function common_select_limit($tableName, $key, $value, $limit, $offset) {
        return $this->db->get_where($tableName, array($key => $value, 'deleted_at' => NULL), $limit, $offset)->result();
    }

    public function common_select_limit_total($tableName, $key, $value) {
        return $this->db->get_where($tableName, array($key => $value, 'deleted_at' => NULL))->num_rows();
    }

    public function common_delete($tableName, $key, $value) {
        $this->db->where($key, $value);
        if ($this->db->update($tableName, array('deleted_at' => date("Y-m-d H:i:s")))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function common_permanent_delete($tableName, $key, $value) {
        $this->db->where($key, $value);

        if ($this->db->delete($tableName)) {
            return $this->db->affected_rows();
        } else {
            return FALSE;
        }
    }

    /**
     * @return array with page and limit of items\
     */
    protected function _filter_with_pagination($target, $page, $limit) {
        if ($target == NULL || !is_array($target)) {
            return array();
        }
        $results = array_chunk($target, $limit);

        if (isset($results[$page - 1])) { // -1 since array starts from 1
            return $results[$page - 1];
        } else {
            return array();
        }
    }

    protected function _upload($file, $name = '', $path = "uploads", $filetypes = "jpg|gif|jpeg|png|pdf|csv", $prefix = '') {
        $this->load->library('upload');
        //$name = time();
        if ($name == '')
            $name = $prefix . $_FILES[$file]['name'];
        //$path = FCPATH . '/uploads/product_images/';
        $config = array();
        $config['file_name'] = $name;
        $config['upload_path'] = FCPATH . '/' . $path;
        $config['overwrite'] = TRUE;
        $config['allowed_types'] = $filetypes;
//        $config['max_size'] = '2000';
//        $config['max_width'] = '2000';
//        $config['max_height'] = '2000';
//        $config['image_library'] = 'gd2';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($file)) {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        } else {
            $data = $this->upload->data();
            $result = array('upload_data' => $data);
            return $result;
        }
    }
    
    protected function _CopyFileServer($old_path, $new_path){
        
        $rtn = false;
        if (ENVIRONMENT == 'production' || ENVIRONMENT == 'staging') {
            exec( "sudo cp ".$old_path." ".$new_path . ' 2>&1', $output);
            //log_message('debug', "sudo cp ".$old_path." ".$new_path . ' 2>&1'.print_r($output,true) ); 
            if (empty($output))
                return true;
            else
                return false;
        }
        else {
            $rtn = copy($old_path, $new_path);
        }
        return $rtn;
    }

    protected function _SendEmail(array $params) {
        // from;to;cc;subject;message;attachment

        $from = (isset($params["from"]) && $params["from"] != NULL) ? $params["from"] : "";
        $to = (isset($params["to"]) && $params["to"] != NULL) ? $params["to"] : "";
        $cc = (isset($params["cc"]) && $params["cc"] != NULL) ? $params["cc"] : ""; 
        $subject = (isset($params["subject"]) && $params["subject"] != NULL) ? $params["subject"] : "";
        $message = (isset($params["message"]) && $params["message"] != NULL) ? $params["message"] : "";
        $attachment = (isset($params["attachment"]) && $params["attachment"] != NULL) ? $params["attachment"] : "";

        if (ENVIRONMENT == 'production') {
            $sql = "exec sp_SysSendDBmail
                        @from_address = ?,
                        @recipients = ?,
                        @copy_recipients = ?,
                        @subject = ?,
                        @body = ?,
                        @file_attachments = ? ";

            $result = $this->load->database('cherps_hachi', TRUE)->query($sql, [$from, $to, $cc, $subject, $message, $attachment]);
        } else {  
            $message .= "\n\n\n ================ \n PLEASE IGNORE THIS MESSAGE. THIS MESSAGE WAS SENT FROM CHVOICES TEST SYSTEM.";
            $sql = "exec [10.0.50.19].[cherps].[dbo].[sp_SysSendDBmail]
                        @from_address = ?,
                        @recipients = ?,
                        @copy_recipients = ?,
                        @subject = ?,
                        @body = ?,
                        @file_attachments = ? ";

            $result = $this->load->database('cherps_hachi', TRUE)->query($sql, [$from, $to, $cc, $subject, $message, $attachment]);
        }

        return $result;
    }
    
    protected function _SendEmailbySmtp($params){

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => '10.0.80.125', //?exchange.challenger.sg
            'smtp_port' => 25, //?25
            'smtp_user' => '', //? online@challenger.sg
            'smtp_pass' => '', //?challenger99
            'mailtype' => 'html'
        );

        // Set our default values
        foreach (array('email' => 'yongsheng@challenger.sg', 'subject' => 'Chvoices Message', 'message' => "Message deliver on ".date('d M Y'), 'attach' => "") as $key => $val) {
            if ( ! isset($params[$key])) {
                $params[$key] = $val;
            }
        }

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('mis@challenger.sg');
        $this->email->to($params['email']);
        $this->email->cc($params['cc']);
        $this->email->subject($params['subject']);
        $this->email->message($params['message']);
        if ($params['attach']!='')
            $this->email->attach($params['attach']);
        if($this->email->send()) {
            return true;
        }
        else {
            //show_error($this->email->print_debugger());
            return false;
        }

    }

    protected function _SysNewDocId($coy_id,$sys_id,$doc_type,$doc_prefix,$len=15){
        // Depends on different doc_type, will have different trans_prefix
        $trans_prefix = $doc_type.$doc_prefix;
        $is_exists = $this->db->query("select 1 from sys_trans_list where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '$trans_prefix'")->num_rows();
        if ($is_exists === 0) {
            $this->db->query("insert into sys_trans_list (coy_id, sys_id, trans_prefix, next_num, sys_date) VALUES ('$coy_id', '$sys_id', '$trans_prefix', 1, now())");
        }
        $next_num = $this->db->query("select next_num from sys_trans_list where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '$trans_prefix'")->result_array();
        $this->db->query("update sys_trans_list set next_num = " . $next_num[0]['next_num'] . "+1 where coy_id = '$coy_id' and sys_id = '$sys_id' and trans_prefix = '$trans_prefix'");
        $doc_id = $doc_prefix . sprintf("%0" . (string)( $len - strlen($doc_prefix)) . "s", $next_num[0]['next_num']);
        return $doc_id;
    }

}

/* End of file client_model.php */
/* Location: ./application/models/client_model.php */
