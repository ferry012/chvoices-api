<!DOCTYPE html>
<?php $prefix = (ENVIRONMENT=="production") ? "P202011" : "t".time() ; ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Challenger - CHVOICES - <?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/skins/_all-skins.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datepicker/datepicker3.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url("assets/css/index.css?".$prefix); ?>">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini fixed ">
        <!-- Site wrapper -->
        <div class="wrapper">
            <?php echo theme_view('general/header'); ?>
            <?php echo theme_view('general/sidebar'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="/*min-width:768px*/">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <input type="hidden" id="web_url" value="<?php echo base_url(); ?>">
                    <?php echo theme_view('general/alert'); ?>

                    <h1>
                        <?php echo $page_title; ?>
                    </h1>
                    <ol class="breadcrumb hidden">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Examples</a></li>
                        <li class="active">Blank page</li>
                    </ol> 
                </section>
                <?php echo Template::content(); ?>
                
                <div class="modal fade" id="UserActivityLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">CHVOICES Activity Log</h4>
                            </div>
                            <div class="modal-body">
                                <pre id="UserActivityLog_TEXT" style="width:100%;max-height:500px;overflow:auto;height:100%;background-color:#F7F7F7;border: 1px solid #EEE;padding:3px;font-size:12px"></pre>
                            </div>
                            <div class="modal-footer">
                                <a href="#" class="btn btn-default MailActivityLog">Report Log</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo theme_view('general/footer'); ?>
            <div class="control-sidebar-bg"></div>
            
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/slimScroll/jquery.slimscroll.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/fastclick/fastclick.js"); ?>"></script>
        <script src="<?php echo base_url("assets/dist/js/app.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/dist/js/demo.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/datepicker/bootstrap-datepicker.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/jquery.nicefileinput.min.js?".$prefix); ?>"></script>
        <script src="<?php echo base_url("assets/js/jquery.browser.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/index.js?".$prefix); ?>"></script>

        <script src="<?php echo base_url("assets/js/jquery-validation/jquery.validate.min.js"); ?>"></script>

        <script src="<?php echo base_url("assets/js/base_forms_validation.js"); ?>"></script>
        
        <?php echo Assets::js(); ?> 
    </body>
</html>