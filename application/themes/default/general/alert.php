<?php
$success_message = $this->session->flashdata('success_message');
$warning_message = $this->session->flashdata('warning_message');
$error_message = $this->session->flashdata('error_message');
?>
<?php if (isset($success_message) && !empty($success_message)): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-check-circle"></i>
        <b><?php echo $success_message; ?></b>
    </div>
<?php endif; ?>

<?php if (isset($warning_message) && !empty($warning_message)): ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-warning"></i>
        <b><?php echo $warning_message; ?></b>
    </div>
<?php endif; ?>

<?php if (isset($error_message) && !empty($error_message)): ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fa fa-bell"></i>
        <b><?php echo $error_message; ?></b>
    </div>
<?php endif; ?>