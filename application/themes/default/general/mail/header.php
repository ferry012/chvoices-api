<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?php echo $subject; ?></title>
<style>
* {font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;}
img {max-width: 600px;width: 100%;}
body {-webkit-font-smoothing: antialiased;height: 100%;-webkit-text-size-adjust: none;width: 100% !important;margin: 0px; padding: 0px;}
a {color: #348eda;}
.btn-primary {Margin-bottom: 10px;width: auto !important;}
.btn-primary td {background-color: #348eda;border-radius: 25px;font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;font-size: 14px;text-align: center;vertical-align: top;}
.btn-primary td a {background-color: #348eda;border: solid 1px #348eda;border-radius: 25px;border-width: 10px 20px;display: inline-block;color: #ffffff;cursor: pointer;font-weight: bold;line-height: 2;text-decoration: none;}
.btn-aclick {font-size:18px;font-family:Helvetica,Arial,sans-serif;font-weight:normal;color:#ffffff;text-decoration:none;border-radius:3px;display:inline-block;background:#8cc541;border-color:#8cc541;border-style:solid;border-width:15px 25px}
.last {margin-bottom: 0;}
.first {margin-top: 0;}
.padding {padding: 10px 0;}
table {width:100%}
h1,h2,h3 {color: #111111;font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;font-weight: 200;line-height: 1.2em;margin: 40px 0 10px;}
h1 {font-size: 36px;}h2 {font-size: 28px;}h3 {font-size: 22px;}
p,ul,ol {font-size: 14px;font-weight: normal;margin-bottom: 10px;}
ul li,ol li {margin-left: 5px;list-style-position: inside;}
.container {clear: both !important;display: block !important;Margin: 0 auto !important;max-width: 600px !important;}
.body-wrap .container {padding: 20px;}
.content {display: block;margin: 0 auto;max-width: 600px;}
.content table {width: 100%; min-width:100%}
</style>
</head>
<body>
<!-- body -->
<table style="padding:10px 10px 3px 10px;width:100%;text-align:center;background-color:#FFF;" bgcolor="#FFF"><tr><td>
    <a href="<?php echo site_url(); ?>"><img src="<?php echo IMAGE_URL."assets/images/andios_logo_email.png"; ?>" border="0" alt="Andios Pte Ltd" style="width:250px;height:110px"></a>
</td></tr></table>
<table style="padding:5px;width:100%;background-color:#f6f6f6" bgcolor="#f6f6f6">
  <tr>
    <td></td>
    <td style="border: 1px solid #f0f0f0;" bgcolor="#FFFFFF">
      <!-- content -->
      <div class="content" style="background-color:#FFF;padding:5px;">
