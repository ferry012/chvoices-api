<?php

$menu_list = array();
$menu_list[0] = array(
    'id' => 'dashboard',
    'header' => 'Dashboard',
    'icon' => 'fa fa-th',
    'url' => base_url("home/index"),
    'submenu' => []
);
$menu_list[1] = array(
    'id' => 'Purchase Order',
    'header' => 'Purchase Order',
    'icon' => 'ion ion-archive',
    'url' => '#',
    'submenu' => array(
        array('url'=> base_url("po2/confirmation"), 'header'=> 'PO Confirmation'),
        array('url'=> base_url("po2/listing"), 'header'=> 'Reprint PO'),
        array('url'=> base_url("po2/pr_listing"), 'header'=> 'View CR')
    )
);
$menu_list[2] = array(
    'id' => 'Invoice',
    'header' => 'Invoice',
    'icon' => 'ion ion-filing',
    'url' => '#',
    'submenu' => array(
        array('url'=> base_url("invoice/submit_new_match"), 'header'=> 'Invoice Matching (Single)'),
        array('url'=> base_url("invoice/submit_batch"), 'header'=> 'Invoice Matching (Batch)'),
        array('url'=> base_url("invoice/listing"), 'header'=> 'Invoice Info'),
        array('url'=> base_url("invoice/update_tax"), 'header'=> 'Invoice Tax Update'),
    )
);
$menu_list[3] = array(
    'id' => 'Return Note',
    'header' => 'Return Note',
    'icon' => 'ion ion-clipboard',
    'url' => '#',
    'submenu' => array(
        array('url'=> base_url("rn/collection"), 'header'=> 'RN Collection Date'),
        array('url'=> base_url("rn/listing"), 'header'=> 'RN Info')
    )
);
$menu_list[4] = array(
    'id' => 'Claims',
    'header' => 'Claims',
    'icon' => 'ion ion-ios-calculator',
    'url' => '#',
    'submenu' => array(
        array('url'=> base_url("vrc/listing"), 'header'=> 'Claims (VRC)')
    )
);
$menu_list[5] = array(
    'id' => 'Inventory',
    'header' => 'Inventory',
    'icon' => 'ion ion-ios-box-outline',
    'url' => '#',
    'submenu' => array(
        array('url'=> base_url("inventory/listings"), 'header'=> 'Inventory Listings'),
        //array('url'=> base_url("inventory/history"), 'header'=> 'Update History')
    )
);
$menu_list[12] = array(
    'id' => 'Promotion',
    'header' => 'Promotion',
    'icon' => 'ion ion-clipboard',
    'url' => '#',
    'submenu' => array(
        array('url'=> base_url("evoucher/check_voucher"), 'header'=> 'Issue VC Voucher'),
        //array('url'=> base_url("inventory/history"), 'header'=> 'Update History')
    )
);

$access_control = $this->session->userdata('r_access_control');
foreach ($menu_list as $k=>$menu_item) {
    if ($access_control[$k]=='Y')
        $rights[] = $menu_item;
}
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">

                <img src="<?php echo base_url("assets/images/fpo_avatar.png"); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('cm_supp_name'); ?></p>
                <p><?php echo $this->session->userdata('cm_supp_id'); ?></p>
            </div>
        </div>
        <?php if (ENVIRONMENT!="production" ): ?>
            <div class="sidebar-form alert alert-warning alert-dismissable">
                <p class="text-bold">TEST PORTAL</p>
                <p>Please use the live system <a href="https://chvoices.challenger.sg" style="color:#FFF">http://chvoices.challenger.sg</a> for actual transactions.</p>
            </div>
        <?php endif; ?>

        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?php foreach ($rights as $menu): ?>
                <li class="
                        <?php if (count($menu['submenu'])>0) echo "treeview"; ?>
                        <?php if ($sidebar == $menu['id']) echo "active"; ?>
                    ">
                    <a href="<?php echo $menu['url']; ?>">
                        <i class="<?php echo $menu['icon']; ?>"></i> <span><?php echo $menu['header']; ?></span>
                        <span class="pull-right-container">
                            <?php if (count($menu['submenu'])>0): ?>
                            <i class="fa fa-angle-left pull-right"></i>
                            <?php else: ?>
                            <small class="label pull-right bg-green"></small>
                            <?php endif; ?>
                        </span>
                    </a>
                    <?php if (count($menu['submenu'])>0): ?>
                    <ul class="treeview-menu">
                        <?php foreach ($menu['submenu'] as $submenu): ?>
                            <li><a href="<?php echo $submenu['url']; ?>"><i class="fa fa-circle-o"></i> <?php echo $submenu['header']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
        
        <div style="z-index:999;color:#777;width:100%;padding:3px;background-color:#1a2226;cursor:pointer; position:absolute;left:0px;bottom:0px;text-align:center" class="OpenActivityLog">Activity Log</div>
        
    </section>
</aside>
