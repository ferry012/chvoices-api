<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Ch</b>V</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>CH</b>VOICES</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="toggle-arrow-menu" data-toggle="offcanvas" role="button">
          <i id="toggle-arrow-menu" class="fa fa-arrow-left text-gray" title="Toggle side menu Show/Hide" style="font-weight:normal; padding: 12px 0px 0px 10px;font-size:20px"></i>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url("assets/images/fpo_avatar.png"); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs">My Account</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url("assets/images/fpo_avatar.png"); ?>" class="img-circle" alt="User Image">
                <p>
                  <?php echo $this->session->userdata('r_acct_name'); ?>
                  <small><?php echo $this->session->userdata('r_acct_email'); ?></small>
                </p>
              </li> 
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                    <a href="<?php echo base_url("home/account"); ?>" class="btn btn-default btn-flat" style="color:#333 !important;background-color:#F4F4F4 !important;border-color:#DDD !important">Change Password</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url("login/logout"); ?>" class="btn btn-default btn-flat" style="color:#333 !important;background-color:#F4F4F4 !important;border-color:#DDD !important">Sign out</a>
                </div>
              </li>
            </ul>
          </li> 
          <!-- Control Sidebar Toggle Button -->
          <li class="hidden">
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>