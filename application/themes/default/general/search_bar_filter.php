<?php
if ($this->input->get("search_from") != NULL) {
    $search_from = ($this->input->get("search_from") != NULL) ? $this->input->get("search_from") : "";
} else {
    $search_from = date('d-m-Y', strtotime('-30 days'));
}
if ($this->input->get("search_to") != NULL) {
    $search_to = ($this->input->get("search_to") != NULL) ? $this->input->get("search_to") : "";
} else {
    $search_to = date('d-m-Y', strtotime('+0 days'));
}
?>
<form class="form-horizontal" id="search_bar" action="" role="form" method="get" >
    <div class="col-md-2"></div>
    <div class="form-group col-md-8">
        <div class="col-md-3">
            <div class="input-group date  datepicker"> 
                <input placeholder="From" type="text" class="form-control" name="search_from" id="search_from" value="<?php echo $search_from; ?>" >
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group date  datepicker"> 
                <input placeholder="To" type="text" class="form-control" name="search_to" id="search_to" value="<?php echo $search_to; ?>" >
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group" style="margin-top:4px">

                <?php if (isset($search_bar_options) && in_array('show_confirmed',$search_bar_options)): ?>
                <div style="float:left; margin-right: 10px;margin-top: 6px;">
                    <input type="checkbox" id="show_confirmed" name="show_confirmed" value="1" <?php if (isset($_GET['show_confirmed'])) echo "checked"; ?>> <label for="show_confirmed" style="font-weight:normal">Show Confirmed</label>
                </div>
                <?php endif; ?>

                <div style="display:none; float:left"><input type="text" class="form-control w-100" name="search" id="search" placeholder="Search..." value="<?php echo $search_value; ?>"/></div>
                <div style="float:left"><button id="search_btn" class="btn btn-default"><i class="fa fa-search"></i> Retrieve Records</button></div>
            </div>
        </div> 
    </div>
</form>