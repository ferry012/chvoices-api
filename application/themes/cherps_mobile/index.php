<!DOCTYPE html>
<?php $prefix = (ENVIRONMENT=="production") ? "P20180823" : "t".time() ; ?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CHERPS Mobile - <?php echo $page_title; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/skins/_all-skins.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datepicker/datepicker3.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/index.css?".$prefix); ?>">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div id="body-div">
<!-- Site wrapper
<div class="hidden text-center center-absolute"><h5><i>Only available on Mobile view.</i></h5></div>
<div class="wrapper">

<div class="hidden visible-sm visible-md visible-lg text-center center-absolute"><h5><i>Only available on Mobile view.</i></h5></div>
<div class="wrapper hidden-sm hidden-md hidden-lg">

-->
<div class="center-absolute text-right " style="z-index:-1;text-align:right !important"><h5><i>Best view on Mobile.</i></h5></div>
<div class="wrapper">


<?php if ($layout=='general'): ?>

    <header class="main-header">
        <!-- Logo -->
        <div href="#" class="logo" style="height:220px;background-color:#FFF;padding:0px; width:100%">
            <div style="height:150px;background-color:#3c8dbc"></div>
            <div style="margin-top:-60px;text-align:center;z-index: 2000;"><img src="<?php echo base_url('assets/images/cherps-otp.gif'); ?>"></div>
        </div>
    </header>
    <div class="content-wrapper" style="margin-top:90px;background-color:#FFF;">
        <?php echo Template::content(); ?>
    </div>
    <div class="control-sidebar-bg"></div>

<?php else: ?>

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo" style="width:100%">
            <span class="logo-mini">CH<b>ERP</b>S</span>
            <span class="logo-lg">CH<b>ERP</b>S</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="hidden navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="toggle-arrow-menu" data-toggle="offcanvas" role="button">
                <i id="toggle-arrow-menu" class="fa fa-arrow-left text-gray" title="Toggle side menu Show/Hide" style="font-weight:normal; padding: 12px 0px 0px 10px;font-size:20px"></i>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url("assets/images/fpo_avatar.png"); ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs">My Account</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo base_url("assets/images/fpo_avatar.png"); ?>" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo $this->session->userdata('cm_supp_name'); ?>
                                    <small><?php echo $this->session->userdata('email_addr'); ?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?php echo base_url("home/account"); ?>" class="btn btn-default btn-flat" style="color:#333 !important;background-color:#F4F4F4 !important;border-color:#DDD !important">Change Password</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo base_url("login/logout"); ?>" class="btn btn-default btn-flat" style="color:#333 !important;background-color:#F4F4F4 !important;border-color:#DDD !important">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="/*min-width:768px*/">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <input type="hidden" id="web_url" value="<?php echo base_url(); ?>">
            <?php echo theme_view('general/alert'); ?>

            <h1>
                <?php echo $page_title; ?>
            </h1>
            <ol class="breadcrumb hidden">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>
        <?php echo Template::content(); ?>

    </div>
    <div class="control-sidebar-bg"></div>

<?php endif; ?>

</div>
<!-- ./wrapper -->

<style>
h1,h2,h3 {
    color:#12699F !important;
}
</style>
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/slimScroll/jquery.slimscroll.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/fastclick/fastclick.js"); ?>"></script>
<script src="<?php echo base_url("assets/dist/js/app.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/dist/js/demo.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/datepicker/bootstrap-datepicker.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/jquery.nicefileinput.min.js?".$prefix); ?>"></script>
<script src="<?php echo base_url("assets/js/jquery.browser.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/index.js?".$prefix); ?>"></script>
<script src="<?php echo base_url("assets/js/cherps_mobile.js?".$prefix); ?>"></script>

<?php echo Assets::js(); ?>
</div></body>
</html>