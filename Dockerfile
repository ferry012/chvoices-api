# Source: https://github.com/Microsoft/mssql-docker/blob/master/oss-drivers/msphpsql/Dockerfile (Modified for PHP7.2)
FROM public.ecr.aws/n2w8l8l1/ctl-basedocker
LABEL maintainer="yongsheng@challenger.sg"

#RUN rm -rfv /var/lib/apt/lists/*
RUN apt-get update -y


RUN apt-get install -y php7.2-pgsql
RUN echo "extension=pgsql.so" >> /etc/php/7.2/apache2/php.ini
RUN apt-get install -y php7.2-redis
RUN apt-get install php7.2-curl
# RUN apt-get install -y php7.2-bcmath
# RUN apt-get install -y php7.2-gmp
# RUN echo "extension=php_gmp.so" >> /etc/php/7.2/apache2/php.ini

# RUN echo "log_errors = On" >> /etc/php/7.2/apache2/php.ini
# RUN echo "error_log = /dev/stdout" >> /etc/php/7.2/apache2/php.ini
RUN rm -f /var/log/apache2/error.log
RUN ln -s /dev/stdout /var/log/apache2/error.log

###
### WORKING DIR
###

WORKDIR /var/www/html
COPY ./ /var/www/html/
COPY ./docker/vhost.conf /etc/apache2/sites-enabled/000-default.conf 

RUN rm /var/www/html/index.html

# RUN ./version-test.sh

RUN chown www-data -R /var/www/html/*
RUN chmod 775 -R *
 
# RUN apt-get -y install curl gnupg 
# RUN apt-get -y install gpg

# RUN chown www-data -R /var/www/html/*
# RUN chmod 777 -R /var/www/html/*
# RUN chmod 777 -R *


EXPOSE 80