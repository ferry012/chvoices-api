# CHVOICES
### Challenger Vendor Order & Invoice Confirmation System

https://chvoices.challenger.sg/

### apidoc
````
// http://chvoices-test.challenger.sg/assets/apidoc/
apidoc -f .php -i ./application -o assets/apidoc/
````

### docker run -v $(pwd):/var/www/html -p 8090:80 chvoices    


---
if you are working in CI 3.x and just upgraded your server php version to php 7.x

Go to system/libraries/Session/session.php at Line no 281 and replace ini_set('session.name', $params['cookie_name']); by ini_set('session.id', $params['cookie_name']);